# Begin ~/.bashrc
# Baseado Beyond Linux From Scratch-8.2
# Escrito por Unicode Tec <unicodetec@gmail.com>
# by 64728912024

export PATH=:/usr/sbin:/opt/jdk/bin:/sbin:$PATH
export XORG_PREFIX="/usr"
export XORG_CONFIG="--prefix=$XORG_PREFIX --sysconfdir=/etc \
    --localstatedir=/var --disable-static"
export QT5PREFIX=/usr
export KF5_PREFIX=/usr

if [ -d "$HOME/bin" ]; then
    PATH="$HOME/bin:$PATH"
fi

alias ls='ls --color=auto'
alias grep='grep --color=auto'

# Set up user specific i18n variables
export LANG=pt_BR.utf8

NORMAL="\[\e[0m\]"
RED="\[\e[1;31m\]"
GREEN="\[\e[1;32m\]"
if [[ $EUID == 0 ]] ; then
  PS1="$RED\u [ $NORMAL\w$RED ]# $NORMAL"
else
  PS1="$GREEN\u [ $NORMAL\w$GREEN ]\$ $NORMAL"
fi

unset RED GREEN NORMAL

# End ~/.bashrc
