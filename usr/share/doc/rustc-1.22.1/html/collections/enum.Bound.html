<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="generator" content="rustdoc">
    <meta name="description" content="API documentation for the Rust `Bound` enum in crate `collections`.">
    <meta name="keywords" content="rust, rustlang, rust-lang, Bound">

    <title>collections::Bound - Rust</title>

    <link rel="stylesheet" type="text/css" href="../normalize.css">
    <link rel="stylesheet" type="text/css" href="../rustdoc.css">
    <link rel="stylesheet" type="text/css" href="../main.css">
    

    <link rel="shortcut icon" href="https://doc.rust-lang.org/favicon.ico">
    
</head>
<body class="rustdoc enum">
    <!--[if lte IE 8]>
    <div class="warning">
        This old browser is unsupported and will most likely display funky
        things.
    </div>
    <![endif]-->

    

    <nav class="sidebar">
        <a href='../collections/index.html'><img src='https://www.rust-lang.org/logos/rust-logo-128x128-blk-v2.png' alt='logo' width='100'></a>
        <p class='location'>Enum Bound</p><div class="block items"><ul><li><a href="#variants">Variants</a></li><li><a href="#implementations">Trait Implementations</a></li></ul></div><p class='location'><a href='index.html'>collections</a></p><script>window.sidebarCurrent = {name: 'Bound', ty: 'enum', relpath: ''};</script><script defer src="sidebar-items.js"></script>
    </nav>

    <nav class="sub">
        <form class="search-form js-only">
            <div class="search-container">
                <input class="search-input" name="search"
                       autocomplete="off"
                       placeholder="Click or press ‘S’ to search, ‘?’ for more options…"
                       type="search">
            </div>
        </form>
    </nav>

    <section id='main' class="content">
<h1 class='fqn'><span class='in-band'>Enum <a href='index.html'>collections</a>::<wbr><a class="enum" href=''>Bound</a></span><span class='out-of-band'><span class='since' title='Stable since Rust version 1.17.0'>1.17.0</span><span id='render-detail'>
                   <a id="toggle-all-docs" href="javascript:void(0)" title="collapse all docs">
                       [<span class='inner'>&#x2212;</span>]
                   </a>
               </span><a class='srclink' href='../src/alloc/lib.rs.html#240-250' title='goto source code'>[src]</a></span></h1>
<pre class='rust enum'>pub enum Bound&lt;T&gt; {
    Included(T),
    Excluded(T),
    Unbounded,
}</pre><div class='docblock'><p>An endpoint of a range of keys.</p>

<h1 id='examples' class='section-header'><a href='#examples'>Examples</a></h1>
<p><code>Bound</code>s are range endpoints:</p>

<pre class="rust rust-example-rendered">
<span class="attribute">#![<span class="ident">feature</span>(<span class="ident">collections_range</span>)]</span>

<span class="kw">use</span> <span class="ident">std</span>::<span class="ident">collections</span>::<span class="ident">range</span>::<span class="ident">RangeArgument</span>;
<span class="kw">use</span> <span class="ident">std</span>::<span class="ident">collections</span>::<span class="ident">Bound</span>::<span class="kw-2">*</span>;

<span class="macro">assert_eq</span><span class="macro">!</span>((..<span class="number">100</span>).<span class="ident">start</span>(), <span class="ident">Unbounded</span>);
<span class="macro">assert_eq</span><span class="macro">!</span>((<span class="number">1</span>..<span class="number">12</span>).<span class="ident">start</span>(), <span class="ident">Included</span>(<span class="kw-2">&amp;</span><span class="number">1</span>));
<span class="macro">assert_eq</span><span class="macro">!</span>((<span class="number">1</span>..<span class="number">12</span>).<span class="ident">end</span>(), <span class="ident">Excluded</span>(<span class="kw-2">&amp;</span><span class="number">12</span>));</pre>

<p>Using a tuple of <code>Bound</code>s as an argument to <a href="btree_map/struct.BTreeMap.html#method.range"><code>BTreeMap::range</code></a>.
Note that in most cases, it&#39;s better to use range syntax (<code>1..5</code>) instead.</p>

<pre class="rust rust-example-rendered">
<span class="kw">use</span> <span class="ident">std</span>::<span class="ident">collections</span>::<span class="ident">BTreeMap</span>;
<span class="kw">use</span> <span class="ident">std</span>::<span class="ident">collections</span>::<span class="ident">Bound</span>::{<span class="ident">Excluded</span>, <span class="ident">Included</span>, <span class="ident">Unbounded</span>};

<span class="kw">let</span> <span class="kw-2">mut</span> <span class="ident">map</span> <span class="op">=</span> <span class="ident">BTreeMap</span>::<span class="ident">new</span>();
<span class="ident">map</span>.<span class="ident">insert</span>(<span class="number">3</span>, <span class="string">&quot;a&quot;</span>);
<span class="ident">map</span>.<span class="ident">insert</span>(<span class="number">5</span>, <span class="string">&quot;b&quot;</span>);
<span class="ident">map</span>.<span class="ident">insert</span>(<span class="number">8</span>, <span class="string">&quot;c&quot;</span>);

<span class="kw">for</span> (<span class="ident">key</span>, <span class="ident">value</span>) <span class="kw">in</span> <span class="ident">map</span>.<span class="ident">range</span>((<span class="ident">Excluded</span>(<span class="number">3</span>), <span class="ident">Included</span>(<span class="number">8</span>))) {
    <span class="macro">println</span><span class="macro">!</span>(<span class="string">&quot;{}: {}&quot;</span>, <span class="ident">key</span>, <span class="ident">value</span>);
}

<span class="macro">assert_eq</span><span class="macro">!</span>(<span class="prelude-val">Some</span>((<span class="kw-2">&amp;</span><span class="number">3</span>, <span class="kw-2">&amp;</span><span class="string">&quot;a&quot;</span>)), <span class="ident">map</span>.<span class="ident">range</span>((<span class="ident">Unbounded</span>, <span class="ident">Included</span>(<span class="number">5</span>))).<span class="ident">next</span>());</pre>
</div><h2 id='variants' class='variants small-section-header'>
                   Variants<a href='#variants' class='anchor'></a></h2>
<span id="variant.Included" class="variant small-section-header"><a href="#variant.Included" class="anchor field"></a><span id='Included.v' class='invisible'><code>Included(T)</code></span></span><div class='docblock'><p>An inclusive bound.</p>
</div><span id="variant.Excluded" class="variant small-section-header"><a href="#variant.Excluded" class="anchor field"></a><span id='Excluded.v' class='invisible'><code>Excluded(T)</code></span></span><div class='docblock'><p>An exclusive bound.</p>
</div><span id="variant.Unbounded" class="variant small-section-header"><a href="#variant.Unbounded" class="anchor field"></a><span id='Unbounded.v' class='invisible'><code>Unbounded</code></span></span><div class='docblock'><p>An infinite endpoint. Indicates that there is no bound in this direction.</p>
</div>
            <h2 id='implementations' class='small-section-header'>
              Trait Implementations<a href='#implementations' class='anchor'></a>
            </h2>
        <h3 id='impl-Copy' class='impl'><span class='in-band'><code>impl&lt;T&gt; <a class="trait" href="../core/marker/trait.Copy.html" title="trait core::marker::Copy">Copy</a> for <a class="enum" href="../collections/enum.Bound.html" title="enum collections::Bound">Bound</a>&lt;T&gt; <span class="where fmt-newline">where<br>&nbsp;&nbsp;&nbsp;&nbsp;T: <a class="trait" href="../core/marker/trait.Copy.html" title="trait core::marker::Copy">Copy</a>,&nbsp;</span></code><a href='#impl-Copy' class='anchor'></a></span><span class='out-of-band'><div class='ghost'></div><a class='srclink' href='../src/alloc/lib.rs.html#239' title='goto source code'>[src]</a></span></h3>
<div class='impl-items'></div><h3 id='impl-Clone' class='impl'><span class='in-band'><code>impl&lt;T&gt; <a class="trait" href="../core/clone/trait.Clone.html" title="trait core::clone::Clone">Clone</a> for <a class="enum" href="../collections/enum.Bound.html" title="enum collections::Bound">Bound</a>&lt;T&gt; <span class="where fmt-newline">where<br>&nbsp;&nbsp;&nbsp;&nbsp;T: <a class="trait" href="../core/clone/trait.Clone.html" title="trait core::clone::Clone">Clone</a>,&nbsp;</span></code><a href='#impl-Clone' class='anchor'></a></span><span class='out-of-band'><div class='ghost'></div><a class='srclink' href='../src/alloc/lib.rs.html#239' title='goto source code'>[src]</a></span></h3>
<div class='impl-items'><h4 id='method.clone' class="method"><span id='clone.v' class='invisible'><code>fn <a href='../core/clone/trait.Clone.html#tymethod.clone' class='fnname'>clone</a>(&amp;self) -&gt; <a class="enum" href="../collections/enum.Bound.html" title="enum collections::Bound">Bound</a>&lt;T&gt;</code></span><span class='out-of-band'><div class='ghost'></div><a class='srclink' href='../src/alloc/lib.rs.html#239' title='goto source code'>[src]</a></span></h4>
</div><h3 id='impl-PartialEq&lt;Bound&lt;T&gt;&gt;' class='impl'><span class='in-band'><code>impl&lt;T&gt; <a class="trait" href="../core/cmp/trait.PartialEq.html" title="trait core::cmp::PartialEq">PartialEq</a>&lt;<a class="enum" href="../collections/enum.Bound.html" title="enum collections::Bound">Bound</a>&lt;T&gt;&gt; for <a class="enum" href="../collections/enum.Bound.html" title="enum collections::Bound">Bound</a>&lt;T&gt; <span class="where fmt-newline">where<br>&nbsp;&nbsp;&nbsp;&nbsp;T: <a class="trait" href="../core/cmp/trait.PartialEq.html" title="trait core::cmp::PartialEq">PartialEq</a>&lt;T&gt;,&nbsp;</span></code><a href='#impl-PartialEq&lt;Bound&lt;T&gt;&gt;' class='anchor'></a></span><span class='out-of-band'><div class='ghost'></div><a class='srclink' href='../src/alloc/lib.rs.html#239' title='goto source code'>[src]</a></span></h3>
<div class='impl-items'><h4 id='method.eq' class="method"><span id='eq.v' class='invisible'><code>fn <a href='../core/cmp/trait.PartialEq.html#tymethod.eq' class='fnname'>eq</a>(&amp;self, __arg_0: &amp;<a class="enum" href="../collections/enum.Bound.html" title="enum collections::Bound">Bound</a>&lt;T&gt;) -&gt; bool</code></span><span class='out-of-band'><div class='ghost'></div><a class='srclink' href='../src/alloc/lib.rs.html#239' title='goto source code'>[src]</a></span></h4>
<h4 id='method.ne' class="method"><span id='ne.v' class='invisible'><code>fn <a href='../core/cmp/trait.PartialEq.html#method.ne' class='fnname'>ne</a>(&amp;self, __arg_0: &amp;<a class="enum" href="../collections/enum.Bound.html" title="enum collections::Bound">Bound</a>&lt;T&gt;) -&gt; bool</code></span><span class='out-of-band'><div class='ghost'></div><a class='srclink' href='../src/alloc/lib.rs.html#239' title='goto source code'>[src]</a></span></h4>
</div><h3 id='impl-Debug' class='impl'><span class='in-band'><code>impl&lt;T&gt; <a class="trait" href="../collections/fmt/trait.Debug.html" title="trait collections::fmt::Debug">Debug</a> for <a class="enum" href="../collections/enum.Bound.html" title="enum collections::Bound">Bound</a>&lt;T&gt; <span class="where fmt-newline">where<br>&nbsp;&nbsp;&nbsp;&nbsp;T: <a class="trait" href="../collections/fmt/trait.Debug.html" title="trait collections::fmt::Debug">Debug</a>,&nbsp;</span></code><a href='#impl-Debug' class='anchor'></a></span><span class='out-of-band'><div class='ghost'></div><a class='srclink' href='../src/alloc/lib.rs.html#239' title='goto source code'>[src]</a></span></h3>
<div class='impl-items'><h4 id='method.fmt' class="method"><span id='fmt.v' class='invisible'><code>fn <a href='../collections/fmt/trait.Debug.html#tymethod.fmt' class='fnname'>fmt</a>(&amp;self, __arg_0: &amp;mut <a class="struct" href="../collections/fmt/struct.Formatter.html" title="struct collections::fmt::Formatter">Formatter</a>) -&gt; <a class="enum" href="../core/result/enum.Result.html" title="enum core::result::Result">Result</a>&lt;(), <a class="struct" href="../collections/fmt/struct.Error.html" title="struct collections::fmt::Error">Error</a>&gt;</code></span><span class='out-of-band'><div class='ghost'></div><a class='srclink' href='../src/alloc/lib.rs.html#239' title='goto source code'>[src]</a></span></h4>
<div class='docblock'><p>Formats the value using the given formatter.</p>
</div></div><h3 id='impl-Hash' class='impl'><span class='in-band'><code>impl&lt;T&gt; <a class="trait" href="../core/hash/trait.Hash.html" title="trait core::hash::Hash">Hash</a> for <a class="enum" href="../collections/enum.Bound.html" title="enum collections::Bound">Bound</a>&lt;T&gt; <span class="where fmt-newline">where<br>&nbsp;&nbsp;&nbsp;&nbsp;T: <a class="trait" href="../core/hash/trait.Hash.html" title="trait core::hash::Hash">Hash</a>,&nbsp;</span></code><a href='#impl-Hash' class='anchor'></a></span><span class='out-of-band'><div class='ghost'></div><a class='srclink' href='../src/alloc/lib.rs.html#239' title='goto source code'>[src]</a></span></h3>
<div class='impl-items'><h4 id='method.hash' class="method"><span id='hash.v' class='invisible'><code>fn <a href='../core/hash/trait.Hash.html#tymethod.hash' class='fnname'>hash</a>&lt;__HT&gt;(&amp;self, __arg_0: &amp;mut __HT) <span class="where fmt-newline">where<br>&nbsp;&nbsp;&nbsp;&nbsp;__HT: <a class="trait" href="../core/hash/trait.Hasher.html" title="trait core::hash::Hasher">Hasher</a>,&nbsp;</span></code></span><span class='out-of-band'><div class='ghost'></div><a class='srclink' href='../src/alloc/lib.rs.html#239' title='goto source code'>[src]</a></span></h4>
</div><h3 id='impl-Eq' class='impl'><span class='in-band'><code>impl&lt;T&gt; <a class="trait" href="../core/cmp/trait.Eq.html" title="trait core::cmp::Eq">Eq</a> for <a class="enum" href="../collections/enum.Bound.html" title="enum collections::Bound">Bound</a>&lt;T&gt; <span class="where fmt-newline">where<br>&nbsp;&nbsp;&nbsp;&nbsp;T: <a class="trait" href="../core/cmp/trait.Eq.html" title="trait core::cmp::Eq">Eq</a>,&nbsp;</span></code><a href='#impl-Eq' class='anchor'></a></span><span class='out-of-band'><div class='ghost'></div><a class='srclink' href='../src/alloc/lib.rs.html#239' title='goto source code'>[src]</a></span></h3>
<div class='impl-items'></div></section>
    <section id='search' class="content hidden"></section>

    <section class="footer"></section>

    <aside id="help" class="hidden">
        <div>
            <h1 class="hidden">Help</h1>

            <div class="shortcuts">
                <h2>Keyboard Shortcuts</h2>

                <dl>
                    <dt>?</dt>
                    <dd>Show this help dialog</dd>
                    <dt>S</dt>
                    <dd>Focus the search field</dd>
                    <dt>&larrb;</dt>
                    <dd>Move up in search results</dd>
                    <dt>&rarrb;</dt>
                    <dd>Move down in search results</dd>
                    <dt>&#9166;</dt>
                    <dd>Go to active search result</dd>
                    <dt>+</dt>
                    <dd>Collapse/expand all sections</dd>
                </dl>
            </div>

            <div class="infos">
                <h2>Search Tricks</h2>

                <p>
                    Prefix searches with a type followed by a colon (e.g.
                    <code>fn:</code>) to restrict the search to a given type.
                </p>

                <p>
                    Accepted types are: <code>fn</code>, <code>mod</code>,
                    <code>struct</code>, <code>enum</code>,
                    <code>trait</code>, <code>type</code>, <code>macro</code>,
                    and <code>const</code>.
                </p>

                <p>
                    Search functions by type signature (e.g.
                    <code>vec -> usize</code> or <code>* -> vec</code>)
                </p>
            </div>
        </div>
    </aside>

    

    <script>
        window.rootPath = "../";
        window.currentCrate = "collections";
    </script>
    <script src="../main.js"></script>
    <script defer src="../search-index.js"></script>
</body>
</html>