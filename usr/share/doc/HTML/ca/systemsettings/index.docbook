<?xml version="1.0" ?>
<!DOCTYPE book PUBLIC "-//KDE//DTD DocBook XML V4.5-Based Variant V1.1//EN" "dtd/kdedbx45.dtd" [
  <!ENTITY % addindex "IGNORE">
  <!ENTITY % Catalan  "INCLUDE">
]>
<book id="systemsettings" lang="&language;">

<bookinfo>
<title
>El manual de l'&systemsettings;</title>

<authorgroup>
<author
>&Richard.Johnson; &Richard.Johnson.mail; </author>
&traductor.Antoni.Bella; 
</authorgroup>

<copyright>
<year
>2007</year>
<holder
>&Richard.Johnson;</holder>
</copyright>
<legalnotice
>&FDLNotice;</legalnotice>

<date
>24 de gener de 2017</date>
<releaseinfo
>Plasma 5.9</releaseinfo>

<abstract>
<para
>Aquesta documentació descriu el sistema de configuració i el centre d'administració pel vostre escriptori. </para>
</abstract>

<keywordset>
<keyword
>KDE</keyword>
<keyword
>Sistema</keyword>
<keyword
>Arranjament</keyword>
<keyword
>configuració</keyword>
<keyword
>administració</keyword>
<keyword
>config</keyword>
<keyword
>admin</keyword>
</keywordset>

</bookinfo>

<chapter id="introduction">
<title
>Introducció</title>

<para
>L'&systemsettings; proporciona a l'usuari una manera centralitzada i convenient per a configurar tots els ajustaments del vostre escriptori. </para>

<para
>L'&systemsettings; es compon de múltiples mòduls. Cada mòdul és una aplicació independent, però, l'&systemsettings; els organitza en un sol lloc. </para>

<tip>
<para
>Cada mòdul a l'&systemsettings; es pot executar de forma individual </para>
<para
>Vegeu la secció titulada <link linkend="run-modules-individually"
>Executa mòduls individuals d'&systemsettings;</link
> per a més informació. </para>
</tip>

<para
>L'&systemsettings; aplega tots els mòduls de configuració en diverses categories: <itemizedlist>
<listitem
><para
><link linkend="appearance"
>Aparença</link
></para
></listitem>
<listitem
><para
><link linkend="workspace"
>Espai de treball</link
></para
></listitem>
<listitem
><para
><link linkend="personalization"
>Personalització</link
></para
></listitem>
<listitem
><para
><link linkend="network"
>Xarxa</link
></para
></listitem>
<listitem
><para
><link linkend="hardware"
>Maquinari</link
></para
></listitem>
</itemizedlist>
</para>

<para
>Els mòduls que componen &systemsettings; es troben sota una de les anteriors categories, el qual facilita localitzar el mòdul de configuració correcte. </para>

</chapter>

<chapter id="using-kapp">
<title
>Ús de l'&systemsettings;</title>

<para
>En aquesta secció es detalla l'ús de l'&systemsettings;. Per a obtenir informació sobre cada mòdul individual, si us plau, consulteu <link linkend="modules"
>Mòduls de &systemsettings;</link
>. </para>

<sect1 id="starting">
<title
>Comença amb &systemsettings;</title>

<para
>L'&systemsettings; es pot iniciar en una d'aquestes tres maneres: </para>

<orderedlist>
<listitem>
<para
>Seleccionant <menuchoice
><guimenu
>Arranjament</guimenu
><guimenuitem
>Arranjament del sistema</guimenuitem
></menuchoice
> des del <guilabel
>Menú d'aplicacions</guilabel
>. </para>
</listitem>
<listitem>
<para
>Prement <keycombo
>&Alt;<keycap
>F2</keycap
></keycombo
> o <keycombo
>&Alt;<keycap
>Espai</keycap
></keycombo
> s'obrirà el diàleg de &krunner;. Escriviu <command
>systemsettings5</command
> i premeu &Intro;. </para>
</listitem>
<listitem>
<para
>Escriviu <command
>systemsettings5 &amp;</command
> a qualsevol indicatiu d'ordres. </para>
</listitem>
</orderedlist>

<para
>Tots tres mètodes són equivalents i produeixen el mateix resultat. </para>
</sect1>

<sect1 id="screen">
<title
>La pantalla de l'&systemsettings;</title>

<para
>En iniciar l'&systemsettings;, us apareixerà una finestra, la qual es divideix en dues parts funcionals. </para>

<para
>A la part superior es troba una barra d'eines. Aquesta proporciona a l'usuari la capacitat de tornar a la vista principal des de l'interior d'un mòdul mitjançant la vista <guibutton
>Tot l'arranjament</guibutton
>. També podeu trobar un menú <guimenu
>Ajuda</guimenu
>, així com un botó <guibutton
>Configura</guibutton
> que proporciona un diàleg amb els ajustaments de vista alternatius. </para>
<para
>Per a cercar alguna cosa a tots els mòduls, comenceu a escriure paraules clau en el camp de cerca a la dreta de la barra d'eines a la vista <guibutton
>Tot l'arranjament</guibutton
>. Quan comenceu a escriure, apareixerà una llista de temes coincidents. Seleccioneu-ne un i només estaran habilitats els grups amb ajustaments per a aquesta paraula clau, els altres restaran de color gris.</para>
<para
>Quan la icona de la finestra té el focus, podeu escriure la primera lletra del nom per a qualsevol mòdul o grup de mòduls per a seleccionar-lo. En escriure de nou aquesta lletra, la selecció es mourà a la següent coincidència. </para>
<para
>Per sota de la barra d'eines es troba una vista d'icones dels diferents mòduls individuals o grups de mòduls que componen &systemsettings; aplegats per diferents categories. Per omissió, si el ratolí es queda uns quants segons sobre una icona, apareixerà un consell, explicant el propòsit del mòdul o mòduls en aquest grup. </para>
</sect1>

<sect1 id="general">
<title
>Les categories i els mòduls de l'&systemsettings;</title>
<para
>Un breu resum de totes les categories i els seus mòduls:</para>

<variablelist>
<varlistentry id="appearance">
<term
>Aparença</term>
<listitem>
<para
><itemizedlist>
<listitem
><para
>Tema de l'espai de treball (Aspecte i comportament, Tema d'escriptori, Tema de cursor, Pantalla de presentació) </para
></listitem>
<listitem
><para
>Colors </para
></listitem>
<listitem
><para
>Tipus de lletra (Tipus de lletra, Gestió dels tipus de lletra) </para
></listitem>
<listitem
><para
>Icones (Icones, Emoticones) </para
></listitem>
<listitem
><para
>Estil de les aplicacions (Decoracions de les finestres, Estil dels estris, Estil de les aplicacions GNOME (GTK)) </para
></listitem>
</itemizedlist
></para>
</listitem>
</varlistentry>

<varlistentry id="workspace">
<term
>Espai de treball</term>
<listitem>
<para
><itemizedlist>
<listitem
><para
>Comportament de l'escriptori (Espai de treball, Efectes de l'escriptori, Vores de la pantalla, Bloqueig de la pantalla, Escriptoris virtuals, Activitats) </para
></listitem>
<listitem
><para
>Gestió de les finestres (Comportament de les finestres, Commutador de tasques, Scripts de KWin, Regles de les finestres) </para
></listitem>
<listitem
><para
>Dreceres (Dreceres estàndard, Dreceres globals, Dreceres web, Dreceres personalitzades) </para
></listitem>
<listitem
><para
>Engegada i aturada (Pantalla d'inici de sessió (SDDM), Inici automàtic, Serveis en segon pla, Sessió de l'escriptori, Pantalla de presentació del sistema) </para
></listitem>
<listitem
><para
>Cerca (Cerca de Plasma, Cerca de fitxers) </para
></listitem>
</itemizedlist
></para>
</listitem>
</varlistentry>

<varlistentry id="personalization">
<term
>Personalització</term>
<listitem>
<para
><itemizedlist>
<listitem
><para
>Detalls del compte (Cartera del KDE, Gestor d'usuaris) </para
></listitem>
<listitem
><para
>Arranjament regional (Traduccions, Format, Corrector ortogràfic, Data i hora) </para
></listitem>
<listitem
><para
>Notificacions (Notificacions, Altres notificacions) </para
></listitem>
<listitem
><para
>Aplicacions (Aplicacions per defecte, Associacions de fitxers, Ubicacions, Reacció en iniciar) </para
></listitem>
<listitem
><para
>Accessibilitat </para
></listitem>
</itemizedlist
></para>
</listitem>
</varlistentry>

<varlistentry id="network">
<term
>Xarxa</term>
<listitem>
<para
><itemizedlist>
<listitem
><para
>Connexions </para
></listitem>
<listitem
><para
>Preferències (Intermediari, Preferències de connexió, Preferències SSL, Memòria cau, Galetes, Identificació del navegador) </para
></listitem>

<listitem
><para
>Connectivitat (Comparticions de Windows) </para
></listitem>

<listitem
><para
>Bluetooth (Dispositius, Adaptadors, Arranjament avançat) </para
></listitem>
</itemizedlist
></para>
</listitem>
</varlistentry>

<varlistentry id="hardware">
<term
>Maquinari</term>
<listitem>
<para
><itemizedlist>
<listitem
><para
>Dispositius d'entrada (Teclat, Ratolí, Palanca de control, Ratolí tàctil) </para
></listitem>
<listitem
><para
>Pantalla i monitor (Pantalles, Compositor, Gamma) </para
></listitem>
<listitem
><para
>Multimèdia (Volum de l'àudio, Àudio i vídeo) </para
></listitem>
<listitem
><para
>Gestió de l'energia (Estalvi de l'energia, Arranjament d'activitat, Arranjament avançat) </para
></listitem>
<listitem
><para
>Emmagatzematge extraïble (Accions de dispositiu, Dispositius extraïbles) </para
></listitem>
</itemizedlist
></para>
</listitem>
</varlistentry>

</variablelist>

<tip
><para
>Utilitzeu el camp de cerca a la vista <guibutton
>Tot l'arranjament</guibutton
> per a trobar tots els mòduls coincidents per a una determinada paraula clau. Comenceu escrivint una paraula clau per obrir una llista de paraules clau i només estaran habilitats els mòduls que hi coincideixin.</para
></tip>
</sect1>
<sect1 id="exiting">
<title
>Sortir de l'&systemsettings;</title>

<para
>Es pot sortir de l'&systemsettings; en una d'aquestes dues maneres: </para>

<orderedlist>
<listitem>
<para
>Prement <keycombo
>&Ctrl;<keycap
>Q</keycap
></keycombo
> en el teclat. </para>
</listitem>
<listitem>
<para
>Fent clic al botó <guibutton
>Surt</guibutton
> situat a la barra d'eines. </para>
</listitem>
</orderedlist>
</sect1>

<sect1 id="configuring">
<title
>Configuració de l'&systemsettings;</title>

<para
>La icona <guiicon
>Configura</guiicon
> a la barra d'eines us permet canviar alguns paràmetres de l'&systemsettings;. Podeu canviar de <guilabel
>Vista d'icones</guilabel
> (vista per omissió) a la <guilabel
>Vista en arbre clàssica</guilabel
>. </para>

<para
>També podeu desactivar els consells detallats desmarcant <guilabel
>Mostra consells emergents detallats</guilabel
>. A partir del qual només rebreu consells normals i no el contingut d'un grup de mòduls. </para>
</sect1>

<sect1 id="run-modules-individually">
<title
>Executa mòduls de l'&systemsettings; de forma individual</title>

<para
>Els mòduls individuals es poden executar sense executar &systemsettings; mitjançant l'ordre <command
>kcmshell5</command
> des de la línia d'ordres. Escriviu <command
>kcmshell5 --list</command
> per a veure una llista dels mòduls disponibles a &systemsettings;. </para>
<para
>A més, podeu iniciar el KRunner i començar a escriure paraules clau. Quan comenceu a escriure, apareixerà una llista de mòduls coincidents amb &systemsettings;. </para>
</sect1>
</chapter>

<chapter id="modules">
<title
>Els mòduls de l'&systemsettings;</title>

<para
>Per tal que sigui el més fàcil possible, l'&systemsettings; disposa les opcions organitzades en cinc categories. Dins de cada categoria, hi ha icones aplegades en subcategories. Cada icona s'anomena un mòdul. En fer doble clic sobre la icona d'un mòdul, us apareixeran les opcions del mòdul a la finestra principal. </para>

<para
>Cada mòdul disposa d'algun o de tots els següents botons: </para>

<variablelist>
<varlistentry>
<term
>Ajuda</term>
<listitem>
<para
>Aquest botó proporcionarà una ajuda específica per al mòdul actual. En fer clic al botó s'obrirà el &khelpcenter; en una nova finestra amb informació detallada sobre el mòdul. </para>
</listitem>
</varlistentry>

<varlistentry>
<term
>Per omissió</term>
<listitem>
<para
>En fer clic en aquest botó es restaurarà el mòdul en qüestió als valors per omissió. Heu de fer clic a <guibutton
>Aplica</guibutton
> per a desar les opcions. </para>
</listitem>
</varlistentry>

<varlistentry>
<term
>Inicialitza</term>
<listitem>
<para
>Aquest botó <quote
>Restableix</quote
> el mòdul als ajustaments anteriors. </para>
</listitem>
</varlistentry>

<varlistentry>
<term
>Aplica</term>
<listitem>
<para
>Fent clic en aquest botó es desaran totes les modificacions. Si heu canviat quelcom, fent clic a <guibutton
>Aplica</guibutton
> es causarà que els canvis tinguin efecte. </para>
</listitem>
</varlistentry>
</variablelist>

<note>
<para
>Haureu de fer <quote
>Inicialitza</quote
> o <quote
>Aplica</quote
> els canvis abans de canviar a un altre mòdul. </para>
<para
>Si voleu canviar sense desar o restablir les opcions, se us demanarà si voleu desar els canvis, o descartar-los. </para>
</note>

</chapter>

<chapter id="credits">

<title
>Crèdits i llicència</title>

<para
>&systemsettings; </para>
<para
>Copyright del programa 2007 Benjamin C. Meyer. </para>
<para
>Copyright del programa 2009 Ben Cooksley. </para>
<para
>Col·laboradors: <itemizedlist>
<listitem>
<para
>Will Stephenson <email
>wstepheson@kde.org</email
> </para>
</listitem>
<listitem>
<para
>Michael D. Stemle, Jr. <email
>manchicken@notsosoft.net</email
> </para>
</listitem>
<listitem>
<para
>Matthias Kretz <email
>kretz@kde.org</email
> </para>
</listitem>
<listitem>
<para
>&Daniel.Molkentin; &Daniel.Molkentin.mail; </para>
</listitem>
<listitem>
<para
>&Matthias.Elter; &Matthias.Elter.mail; </para>
</listitem>
<listitem>
<para
>Frans Englich <email
>englich@kde.org</email
> </para>
</listitem>
<listitem>
<para
>Michael Jansen <email
>kde@michael-jansen.biz</email
> </para>
</listitem>
</itemizedlist>
</para>

<para
>Copyright de la documentació &copy; 2008 &Richard.Johnson; &Richard.Johnson.mail; </para>

<para
>Traductor/Revisor de la documentació: &credits.Antoni.Bella;</para
> 
&underFDL; &underGPL; </chapter>

&documentation.index;
</book>

<!--
Local Variables:
mode: xml
sgml-minimize-attributes:nil
sgml-general-insert-case:lower
sgml-indent-step:0
sgml-indent-data:nil
End:

vim:tabstop=2:shiftwidth=2:expandtab
kate: space-indent on; indent-width 2; tab-width 2; indent-mode none;
-->
