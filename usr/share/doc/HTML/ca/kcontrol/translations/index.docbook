<?xml version="1.0" ?>
<!DOCTYPE article PUBLIC "-//KDE//DTD DocBook XML V4.5-Based Variant V1.1//EN"
"dtd/kdedbx45.dtd" [
<!ENTITY % addindex "IGNORE">
<!ENTITY % Catalan  "INCLUDE"
> <!-- change language only here -->
]>

<article id="translations" lang="&language;">
<articleinfo>
<title
>Idioma</title>
<authorgroup>
<author
>&Mike.McBride; &Mike.McBride.mail;</author>
<author
>&Krishna.Tateneni; &Krishna.Tateneni.mail;</author>
&traductor.Antoni.Bella; 
 </authorgroup>

	  <date
>18 de maig de 2015</date>
	  <releaseinfo
>Plasma 5.3</releaseinfo>

	  <keywordset>
		<keyword
>KDE</keyword>
		<keyword
>Arranjament del sistema</keyword>
		<keyword
>configuració regional</keyword>
		<keyword
>país</keyword>
		<keyword
>idioma</keyword>
		<keyword
>traducció</keyword>
		<keyword
>Idioma</keyword>
	  </keywordset>
</articleinfo>

<para
>En aquesta pàgina podreu establir els idiomes en els quals serà mostrat l'espai de treball i les aplicacions del &kde;. </para>

<para
>L'espai de treball i les aplicacions del &kde; estan escrites en anglès americà i són traduïdes a molts idiomes diferents per equips de voluntaris. Aquestes traduccions han d'estar instal·lades abans que pugueu optar per utilitzar-les. La llista <guilabel
>Idiomes disponibles</guilabel
>, mostra els noms de l'idioma localitzat per a les traduccions instal·lades i disponibles al vostre sistema. Si l'idioma que voleu utilitzar no apareix en aquesta llista, haureu d'instal·lar-lo amb el mètode habitual per al vostre sistema. </para>
<note
><para
>Assegureu-vos que heu instal·lat els paquets d'idiomes o traduccions del &kde; per als idiomes que voleu emprar.</para>
<para
>Com el &kde; aprofita les biblioteques &Qt;, necessitareu les traduccions de les &Qt; per als idiomes seleccionats, per tal de tenir una &IGU; totalment localitzada.</para
></note>
<!--FIXME 
Toooltip in GUI about available languages is wrong as well
-->

<para
>La llista <guilabel
>Idiomes preferits</guilabel
>, mostra els noms de l'idioma localitzat que s'han d'utilitzar quan es mostri l'espai de treball i les aplicacions del &kde;. Atès que no tots els espais de treball &kde; i les seves aplicacions poden ser traduïdes a tots els idiomes, es mira de trobar la traducció adequada, treballant en l'ordre preferent a la llista fins que es troba una traducció. Si cap dels idiomes preferits disposa d'una traducció requerida, llavors s'utilitzarà l'original en anglès americà. </para>

<para
>Podeu afegir un idioma a la llista <guilabel
>Idiomes preferits</guilabel
>, seleccionant-lo a <guilabel
>Idiomes disponibles</guilabel
> i fent clic al botó de fletxa «Afegeix». Si teniu activat el quadre de llista, només haureu d'escriure la primera lletra del vostre idioma desitjat. Escriviu la primera lletra de nou per a recórrer totes les entrades coincidents en la llista. </para>
<para
>Podeu llevar un idioma de la llista <guilabel
>Idiomes preferits</guilabel
>, seleccionant-lo i després fent clic al botó de fletxa Suprimeix. Podeu canviar l'ordre de preferència en la llista <guilabel
>Idiomes preferits</guilabel
>, seleccionant un idioma i fent clic als botons de fletxa Amunt o Avall. </para>

<para
>Només els idiomes llistats a <guilabel
>Idiomes preferits</guilabel
> i <guilabel
>Idiomes disponibles</guilabel
> s'oferiran com a opcions per a <guilabel
>Idioma primari</guilabel
> i <guilabel
>Idioma alternatiu</guilabel
> al diàleg <guilabel
>Canvia l'idioma de l'aplicació</guilabel
> del menú <guimenu
>Ajuda</guimenu
>. </para>

<note>
<para
>L'idioma i els formats són paràmetres independents. El canvi de l'idioma <emphasis
>no</emphasis
> canviarà automàticament l'arranjament per als nombres, moneda, &etc; al del país o regió corresponent. </para>
</note>

</article>
