<?xml version="1.0" ?>
<!DOCTYPE article PUBLIC "-//KDE//DTD DocBook XML V4.5-Based Variant V1.1//EN"
"dtd/kdedbx45.dtd" [
<!ENTITY % addindex "IGNORE">
<!ENTITY % Catalan  "INCLUDE"
> <!-- change language only here -->
]>

<article id="kcmsmserver" lang="&language;">
<articleinfo>
<title
>Gestió de sessions</title>
<authorgroup>
<author
>&Jost.Schenck; &Jost.Schenck.mail;</author>
&traductor.Antoni.Bella; 
</authorgroup>

<date
>8 d'abril de 2015</date>
<releaseinfo
>Plasma 5.3</releaseinfo>

<keywordset>
<keyword
>KDE</keyword>
<keyword
>KControl</keyword>
<keyword
>sessió</keyword>
<keyword
>Arranjament del sistema</keyword>
</keywordset>
</articleinfo>

<sect1 id="sessions">
<title
>Gestió de sessions</title>

<sect2 id="sessions-use">
<title
>Ús</title>

<para
>En aquest mòdul de l'&systemsettings; podreu configurar el gestor de sessions i sortida del &kde;.</para>

<para
>La gestió de sessions es refereix a la capacitat del &kde; per a desar l'estat de les aplicacions i finestres quan sortiu del &kde; i restaurar-les quan hi torneu a accedir.</para>

<sect3 id="sessions-general">
<title
>General</title>
<variablelist>
<varlistentry>
<term
><guilabel
>Confirma la sortida</guilabel
></term>
<listitem>
<para
>Si aquesta opció ha estat seleccionada, en sortir del &kde; se us mostrarà un diàleg per a la seva confirmació. En aquest diàleg podeu escollir si desitgeu restaurar la vostra sessió actual quan la inicieu la propera vegada.</para>
</listitem>
</varlistentry>
<varlistentry>
<term
><guilabel
>Ofereix les opcions d'aturada</guilabel
></term>
<listitem>
<para
>Si està marcada aquesta opció, el &kde; mostrarà la possibilitat d'escollir entre diverses opcions d'apagada. Això inclou finalitzar la sessió actual (l'opció usada si les opcions d'apagada no estan disponibles), l'apagada de l'ordinador o reiniciar-lo.</para>
</listitem>
</varlistentry>
</variablelist>
</sect3>

<sect3 id="sessions-default-leave-options">
<title
>Opció de sortida per omissió</title>
<para
>Finalment podeu configurar què haurà de passar per omissió quan sortiu del &kde;. Aquestes opcions no són possibles en tots els sistemes operatius i requereixen l'ús del &kdm; com al vostre gestor d'accés.</para>

<para
>Les opcions disponibles són autoexplicatives. Si teniu cap dubte, deixeu l'opció preestablerta. Són les següents:</para>

<itemizedlist>
<listitem
><para
><guilabel
>Finalitza la sessió actual</guilabel
> (aquesta és per omissió)</para
></listitem>
<listitem
><para
><guilabel
>Apaga l'ordinador</guilabel
></para
></listitem>
<listitem
><para
><guilabel
>Torna a engegar l'ordinador</guilabel
></para
></listitem>
</itemizedlist>
</sect3>

<sect3 id="sessions-on-login">
<title
>En connectar-se</title>
<para
>Podeu escollir una de les tres opcions per a com s'haurà d'accedir al &kde;:</para>

<variablelist>
<varlistentry>
<term
><guilabel
>Restaura la sessió prèvia</guilabel
></term>
<listitem
><para
>Si seleccioneu aquesta opció, el &kde; desarà l'estat de la vostra sessió actual quan en sortiu. El &kde; la restaurarà en el següent accés, de manera que pugueu continuar amb la vostra tasca amb l'escriptori en el mateix estat en el qual el vàreu deixar.</para>
</listitem>
</varlistentry>
<varlistentry>
<term
><guilabel
>Restaura la sessió desada manualment</guilabel
></term>
<listitem
><para
>En comptes de restaurar el &kde; l'estat en el qual estava quan vàreu accedir-hi l'última vegada, es restaurarà a un estat específic que hàgiu desat manualment.</para>
<para
>Si aquesta opció està marcada, el menú d'inici ofereix un element addicional <menuchoice
><guimenu
>Sortida</guimenu
><guimenuitem
>Desa la sessió</guimenuitem
></menuchoice
>. </para
></listitem>
</varlistentry>
<varlistentry>
<term
><guilabel
>Comença amb una sessió buida</guilabel
></term>
<listitem>
<para
>Si seleccioneu aquesta opció, el &kde; mai restaurarà les sessions que hàgiu desat.</para>
</listitem>
</varlistentry>
</variablelist>
<para
>Finalment podeu introduir un llistat d'aplicacions separades per dos punts (<literal
>:</literal
>) o coma (<literal
>,</literal
>) que no es desaran en les sessions, de manera que en accedir a una nova sessió no es restauraran. Per exemple, <userinput
>xterm:konsole</userinput
> o <userinput
>xterm,konsole</userinput
>.</para>
<!-- does this need the full path here? -->
</sect3>

</sect2>

</sect1>

</article>

