<?xml version="1.0" ?>
<!DOCTYPE article PUBLIC "-//KDE//DTD DocBook XML V4.5-Based Variant V1.1//EN"
"dtd/kdedbx45.dtd" [
<!ENTITY % addindex "IGNORE">
<!ENTITY % Catalan  "INCLUDE"
> <!-- change language only here -->
]>

<article id="screenlocker" lang="&language;">
<articleinfo>
<title
>Bloqueig de la pantalla</title>
<authorgroup>
<author
>&Mike.McBride; &Mike.McBride.mail;</author>
&traductor.Antoni.Bella; 
</authorgroup>
<!--FIXME code in workspace/kscreenlocker - docbook in workspace/plasma-desktop -->
<date
>19 de setembre de 2016</date>
<releaseinfo
>Plasma 5.8</releaseinfo>

<keywordset>
<keyword
>KDE</keyword>
<keyword
>Arranjament del sistema</keyword>
<keyword
>bloqueig de la pantalla</keyword>
<keyword
>Bloqueig de la pantalla</keyword>
</keywordset>
</articleinfo>
<!--https://bugs.kde.org/show_bug.cgi?id=317156 does this effect screenlock in kf5?-->
<para
>Emprant aquest mòdul, podreu determinar quant de temps haurà de passar abans que s'activi el bloquejador de la pantalla, i afegir o treure la protecció amb contrasenya.</para>

<para
>A la part superior hi ha una casella de selecció per tenir blocada la pantalla <guilabel
>Bloqueja la pantalla automàticament després de</guilabel
>, i un botó de selecció de valors que determina el període d'inactivitat abans d'iniciar-lo. En aquest quadre podeu introduir qualsevol nombre positiu de minuts.</para>

<para
>A sota hi ha un botó de selecció de valors anomenat <guilabel
>Cal contrasenya després del bloqueig</guilabel
>. Quan premeu una tecla o feu clic amb un botó del ratolí es desactivarà el bloquejador de la pantalla després del temps establert al botó de selecció de valors i tornareu al vostre treball, haureu d'introduir una contrasenya. La contrasenya utilitzada és la mateixa que heu emprat per iniciar la sessió al sistema. </para>

<para
>El bloqueig de la pantalla manualment provoca immediatament la protecció amb contrasenya.</para>

<para
>La drecera per omissió <keycombo action="simul"
>&Ctrl;&Alt;<keycap
>L</keycap
></keycombo
> proporciona una forma ràpida per bloquejar la pantalla manualment.</para>

<para
>Marqueu <guilabel
>Bloqueja la pantalla en reprendre</guilabel
> si voleu un sistema protegit amb contrasenya quan desperti de la suspensió.</para>

<para
>Si voleu canviar el fons per a l'estat de bloqueig, canvieu-lo a la pestanya <guilabel
>Fons d'escriptori</guilabel
>.</para>

<para
>Podeu seleccionar un <guilabel
>Color llis</guilabel
> com a tipus de fons d'escriptori.</para>

<para
>De manera alternativa, podeu utilitzar una sola imatge o un passi de diapositives amb imatges d'una carpeta o carregar nous fons d'escriptori des d'Internet.</para>

</article>
