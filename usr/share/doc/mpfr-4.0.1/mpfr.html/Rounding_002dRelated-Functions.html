<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents how to install and use the Multiple Precision
Floating-Point Reliable Library, version 4.0.1.

Copyright 1991, 1993-2018 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document under
the terms of the GNU Free Documentation License, Version 1.2 or any later
version published by the Free Software Foundation; with no Invariant Sections,
with no Front-Cover Texts, and with no Back-Cover Texts.  A copy of the
license is included in GNU Free Documentation License. -->
<!-- Created by GNU Texinfo 6.5, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Rounding-Related Functions (GNU MPFR 4.0.1)</title>

<meta name="description" content="How to install and use GNU MPFR, a library for reliable multiple precision
floating-point arithmetic, version 4.0.1.">
<meta name="keywords" content="Rounding-Related Functions (GNU MPFR 4.0.1)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<link href="index.html#Top" rel="start" title="Top">
<link href="Concept-Index.html#Concept-Index" rel="index" title="Concept Index">
<link href="MPFR-Interface.html#MPFR-Interface" rel="up" title="MPFR Interface">
<link href="Miscellaneous-Functions.html#Miscellaneous-Functions" rel="next" title="Miscellaneous Functions">
<link href="Integer-and-Remainder-Related-Functions.html#Integer-and-Remainder-Related-Functions" rel="prev" title="Integer and Remainder Related Functions">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<a name="Rounding_002dRelated-Functions"></a>
<div class="header">
<p>
Next: <a href="Miscellaneous-Functions.html#Miscellaneous-Functions" accesskey="n" rel="next">Miscellaneous Functions</a>, Previous: <a href="Integer-and-Remainder-Related-Functions.html#Integer-and-Remainder-Related-Functions" accesskey="p" rel="prev">Integer and Remainder Related Functions</a>, Up: <a href="MPFR-Interface.html#MPFR-Interface" accesskey="u" rel="up">MPFR Interface</a> &nbsp; [<a href="Concept-Index.html#Concept-Index" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<a name="index-Rounding-mode-related-functions"></a>
<a name="Rounding_002dRelated-Functions-1"></a>
<h3 class="section">5.11 Rounding-Related Functions</h3>

<dl>
<dt><a name="index-mpfr_005fset_005fdefault_005frounding_005fmode"></a>Function: <em>void</em> <strong>mpfr_set_default_rounding_mode</strong> <em>(mpfr_rnd_t <var>rnd</var>)</em></dt>
<dd><p>Set the default rounding mode to <var>rnd</var>.
The default rounding mode is to nearest initially.
</p></dd></dl>

<dl>
<dt><a name="index-mpfr_005fget_005fdefault_005frounding_005fmode"></a>Function: <em>mpfr_rnd_t</em> <strong>mpfr_get_default_rounding_mode</strong> <em>(void)</em></dt>
<dd><p>Get the default rounding mode.
</p></dd></dl>

<dl>
<dt><a name="index-mpfr_005fprec_005fround"></a>Function: <em>int</em> <strong>mpfr_prec_round</strong> <em>(mpfr_t <var>x</var>, mpfr_prec_t <var>prec</var>, mpfr_rnd_t <var>rnd</var>)</em></dt>
<dd><p>Round <var>x</var> according to <var>rnd</var> with precision <var>prec</var>, which
must be an integer between <code>MPFR_PREC_MIN</code> and <code>MPFR_PREC_MAX</code>
(otherwise the behavior is undefined).
If <var>prec</var> is greater or equal to the precision of <var>x</var>, then new
space is allocated for the significand, and it is filled with zeros.
Otherwise, the significand is rounded to precision <var>prec</var> with the given
direction. In both cases, the precision of <var>x</var> is changed to <var>prec</var>.
</p>
<p>Here is an example of how to use <code>mpfr_prec_round</code> to implement
Newton&rsquo;s algorithm to compute the inverse of <var>a</var>, assuming <var>x</var> is
already an approximation to <var>n</var> bits:
</p><div class="example">
<pre class="example">  mpfr_set_prec (t, 2 * n);
  mpfr_set (t, a, MPFR_RNDN);         /* round a to 2n bits */
  mpfr_mul (t, t, x, MPFR_RNDN);      /* t is correct to 2n bits */
  mpfr_ui_sub (t, 1, t, MPFR_RNDN);   /* high n bits cancel with 1 */
  mpfr_prec_round (t, n, MPFR_RNDN);  /* t is correct to n bits */
  mpfr_mul (t, t, x, MPFR_RNDN);      /* t is correct to n bits */
  mpfr_prec_round (x, 2 * n, MPFR_RNDN); /* exact */
  mpfr_add (x, x, t, MPFR_RNDN);      /* x is correct to 2n bits */
</pre></div>

<p>Warning! You must not use this function if <var>x</var> was initialized
with <code>MPFR_DECL_INIT</code> or with <code>mpfr_custom_init_set</code>
(see <a href="Custom-Interface.html#Custom-Interface">Custom Interface</a>).
</p></dd></dl>

<dl>
<dt><a name="index-mpfr_005fcan_005fround"></a>Function: <em>int</em> <strong>mpfr_can_round</strong> <em>(mpfr_t <var>b</var>, mpfr_exp_t <var>err</var>, mpfr_rnd_t <var>rnd1</var>, mpfr_rnd_t <var>rnd2</var>, mpfr_prec_t <var>prec</var>)</em></dt>
<dd><p>Assuming <var>b</var> is an approximation of an unknown number
<var>x</var> in the direction <var>rnd1</var> with error at most two to the power
E(b)-<var>err</var> where E(b) is the exponent of <var>b</var>, return a non-zero
value if one is able to round correctly <var>x</var> to precision <var>prec</var>
with the direction <var>rnd2</var> assuming an unbounded exponent range, and
0 otherwise (including for NaN and Inf).
In other words, if the error on <var>b</var> is bounded by two to the power
<var>k</var> ulps, and <var>b</var> has precision <var>prec</var>,
you should give <var>err</var>=<var>prec</var>−<var>k</var>.
This function <strong>does not modify</strong> its arguments.
</p>
<p>If <var>rnd1</var> is <code>MPFR_RNDN</code> or <code>MPFR_RNDF</code>,
the error is considered to be either
positive or negative, thus the possible range
is twice as large as with a directed rounding for <var>rnd1</var> (with the
same value of <var>err</var>).
</p>
<p>When <var>rnd2</var> is <code>MPFR_RNDF</code>, let <var>rnd3</var> be the opposite direction
if <var>rnd1</var> is a directed rounding, and <code>MPFR_RNDN</code>
if <var>rnd1</var> is <code>MPFR_RNDN</code> or <code>MPFR_RNDF</code>.
The returned value of <code>mpfr_can_round (b, err, rnd1, MPFR_RNDF, prec)</code>
is non-zero iff after
the call <code>mpfr_set (y, b, rnd3)</code> with <var>y</var> of precision <var>prec</var>,
<var>y</var> is guaranteed to be a faithful rounding of <var>x</var>.
</p>
<p>Note: The <a href="Rounding-Modes.html#ternary-value">ternary value</a> cannot be determined in general with this
function. However, if it is known that the exact value is not exactly
representable in precision <var>prec</var>, then one can use the following
trick to determine the (non-zero) ternary value in any rounding mode
<var>rnd2</var> (note that <code>MPFR_RNDZ</code> below can be replaced by any
directed rounding mode):
</p><div class="example">
<pre class="example">if (mpfr_can_round (b, err, MPFR_RNDN, MPFR_RNDZ,
                    prec + (rnd2 == MPFR_RNDN)))
  {
    /* round the approximation 'b' to the result 'r' of 'prec' bits
       with rounding mode 'rnd2' and get the ternary value 'inex' */
    inex = mpfr_set (r, b, rnd2);
  }
</pre></div>
<p>Indeed, if <var>rnd2</var> is <code>MPFR_RNDN</code>, this will check if one can
round to <var>prec</var>+1 bits with a directed rounding:
if so, one can surely round to nearest to <var>prec</var> bits,
and in addition one can determine the correct ternary value, which would not
be the case when <var>b</var> is near from a value exactly representable on
<var>prec</var> bits.
</p>
<p>A detailed example is available in the <samp>examples</samp> subdirectory,
file <samp>can_round.c</samp>.
</p></dd></dl>

<dl>
<dt><a name="index-mpfr_005fmin_005fprec"></a>Function: <em>mpfr_prec_t</em> <strong>mpfr_min_prec</strong> <em>(mpfr_t <var>x</var>)</em></dt>
<dd><p>Return the minimal number of bits required to store the significand of
<var>x</var>, and 0 for special values, including 0.
</p></dd></dl>

<dl>
<dt><a name="index-mpfr_005fprint_005frnd_005fmode"></a>Function: <em>const char *</em> <strong>mpfr_print_rnd_mode</strong> <em>(mpfr_rnd_t <var>rnd</var>)</em></dt>
<dd><p>Return a string (&quot;MPFR_RNDD&quot;, &quot;MPFR_RNDU&quot;, &quot;MPFR_RNDN&quot;, &quot;MPFR_RNDZ&quot;,
&quot;MPFR_RNDA&quot;) corresponding to the rounding mode <var>rnd</var>, or a null pointer
if <var>rnd</var> is an invalid rounding mode.
</p></dd></dl>

<dl>
<dt><a name="index-mpfr_005fround_005fnearest_005faway"></a>Macro: <em>int</em> <strong>mpfr_round_nearest_away</strong> <em>(int (<var>foo</var>)(mpfr_t, type1_t, ..., mpfr_rnd_t), mpfr_t <var>rop</var>, type1_t <var>op</var>, ...)</em></dt>
<dd><p>Given a function <var>foo</var> and one or more values <var>op</var> (which may be
a <code>mpfr_t</code>, a <code>long</code>, a <code>double</code>, etc.), put in <var>rop</var>
the round-to-nearest-away rounding of <code><var>foo</var>(<var>op</var>,...)</code>.
This rounding is defined in the same way as round-to-nearest-even,
except in case of tie, where the value away from zero is returned.
The function <var>foo</var> takes as input, from second to
penultimate argument(s), the argument list given after <var>rop</var>,
a rounding mode as final argument,
puts in its first argument the value <code><var>foo</var>(<var>op</var>,...)</code> rounded
according to this rounding mode, and returns the corresponding ternary value
(which is expected to be correct, otherwise <code>mpfr_round_nearest_away</code>
will not work as desired).
Due to implementation constraints, this function must not be called when
the minimal exponent <code>emin</code> is the smallest possible one.
This macro has been made such that the compiler is able to detect
mismatch between the argument list <var>op</var>
and the function prototype of <var>foo</var>.
Multiple input arguments <var>op</var> are supported only with C99 compilers.
Otherwise, for C89 compilers, only one such argument is supported.
</p>
<p>Note: this macro is experimental and its interface might change in future
versions.
</p><div class="example">
<pre class="example">unsigned long ul;
mpfr_t f, r;
/* Code that inits and sets r, f, and ul, and if needed sets emin */
int i = mpfr_round_nearest_away (mpfr_add_ui, r, f, ul);
</pre></div>
</dd></dl>

<hr>
<div class="header">
<p>
Next: <a href="Miscellaneous-Functions.html#Miscellaneous-Functions" accesskey="n" rel="next">Miscellaneous Functions</a>, Previous: <a href="Integer-and-Remainder-Related-Functions.html#Integer-and-Remainder-Related-Functions" accesskey="p" rel="prev">Integer and Remainder Related Functions</a>, Up: <a href="MPFR-Interface.html#MPFR-Interface" accesskey="u" rel="up">MPFR Interface</a> &nbsp; [<a href="Concept-Index.html#Concept-Index" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
