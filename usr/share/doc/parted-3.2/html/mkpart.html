<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Copyright (C) 1999-2014 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts, and with no Back-Cover
Texts.  A copy of the license is included in the section entitled "GNU
Free Documentation License". -->
<!-- Created by GNU Texinfo 6.5, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>mkpart (Parted User&rsquo;s Manual)</title>

<meta name="description" content="mkpart (Parted User&rsquo;s Manual)">
<meta name="keywords" content="mkpart (Parted User&rsquo;s Manual)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<link href="index.html#Top" rel="start" title="Top">
<link href="Concept-index.html#Concept-index" rel="index" title="Concept index">
<link href="Command-explanations.html#Command-explanations" rel="up" title="Command explanations">
<link href="name.html#name" rel="next" title="name">
<link href="mklabel.html#mklabel" rel="prev" title="mklabel">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<a name="mkpart"></a>
<div class="header">
<p>
Next: <a href="name.html#name" accesskey="n" rel="next">name</a>, Previous: <a href="mklabel.html#mklabel" accesskey="p" rel="prev">mklabel</a>, Up: <a href="Command-explanations.html#Command-explanations" accesskey="u" rel="up">Command explanations</a> &nbsp; [<a href="Concept-index.html#Concept-index" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<a name="mkpart-1"></a>
<h4 class="subsection">2.4.5 mkpart</h4>
<a name="index-mkpart_002c-command-description"></a>
<a name="index-command-description_002c-mkpart"></a>

<dl>
<dt><a name="index-mkpart"></a>Command: <strong>mkpart</strong> <em>[<var>part-type</var> <var>fs-type</var> <var>name</var>] <var>start</var> <var>end</var></em></dt>
<dd>
<p>Creates a new partition, <em>without</em> creating a new file system on
that partition.  This is useful for creating partitions for file systems
(or LVM, etc.) that Parted doesn&rsquo;t support.  You may specify a file
system type, to set the appropriate partition code in the partition
table for the new partition.  <var>fs-type</var> is required for data
partitions (i.e., non-extended partitions).  <var>start</var> and <var>end</var>
are the offset from the beginning of the disk, that is, the &ldquo;distance&rdquo;
from the start of the disk.
</p>
<p><var>part-type</var> is one of &lsquo;<samp>primary</samp>&rsquo;, &lsquo;<samp>extended</samp>&rsquo; or &lsquo;<samp>logical</samp>&rsquo;,
and may be specified only with &lsquo;<samp>msdos</samp>&rsquo; or &lsquo;<samp>dvh</samp>&rsquo; partition tables.
A <var>name</var> must be specified for a &lsquo;<samp>gpt</samp>&rsquo; partition table.
Neither <var>part-type</var> nor <var>name</var> may be used with a &lsquo;<samp>sun</samp>&rsquo;
partition table.
</p>
<p><var>fs-type</var> must be one of these supported file systems:
</p><ul>
<li> ext2
</li><li> fat16, fat32
</li><li> hfs, hfs+, hfsx
</li><li> linux-swap
</li><li> NTFS
</li><li> reiserfs
</li><li> ufs
</li><li> btrfs
</li></ul>

<p>For example, the following creates a logical partition that will contain
an ext2 file system.  The partition will start at the beginning of the disk,
and end 692.1 megabytes into the disk.
</p>
<div class="example">
<pre class="example">(parted) <kbd>mkpart logical 0.0 692.1</kbd>
</pre></div>

<p>Now, we will show how to partition a low-end flash
device (&ldquo;low-end&rdquo;, as of 2011/2012).
For such devices, you should use 4MiB-aligned partitions<a name="DOCF2" href="#FOOT2"><sup>2</sup></a>.
This command creates a tiny place-holder partition at the beginning, and
then uses all remaining space to create the partition you&rsquo;ll actually use:
</p>
<div class="example">
<pre class="example">$ <kbd>parted -s /dev/sdX -- mklabel msdos \</kbd>
<kbd>    mkpart primary fat32 64s 4MiB \</kbd>
<kbd>    mkpart primary fat32 4MiB -1s</kbd>
</pre></div>

<p>Note the use of &lsquo;<samp>--</samp>&rsquo;, to prevent the following &lsquo;<samp>-1s</samp>&rsquo; last-sector
indicator from being interpreted as an invalid command-line option.
The above creates two empty partitions.  The first is unaligned and tiny,
with length less than 4MiB.
The second partition starts precisely at the 4MiB mark
and extends to the end of the device.
</p>
<p>The next step is typically to create a file system in the second partition:
</p>
<div class="example">
<pre class="example">$ <kbd>mkfs.vfat /dev/sdX2</kbd>
</pre></div>


</dd></dl>

<div class="footnote">
<hr>
<h4 class="footnotes-heading">Footnotes</h4>

<h3><a name="FOOT2" href="#DOCF2">(2)</a></h3>
<p>Cheap flash drives will be with us for a long time to
come, and, for them, 1MiB alignment is not enough.
Use at least 4MiB-aligned partitions.
For details, see Arnd Bergman&rsquo;s article,
<a href="http://http://lwn.net/Articles/428584/">http://http://lwn.net/Articles/428584/</a> and its many comments.</p>
</div>
<hr>
<div class="header">
<p>
Next: <a href="name.html#name" accesskey="n" rel="next">name</a>, Previous: <a href="mklabel.html#mklabel" accesskey="p" rel="prev">mklabel</a>, Up: <a href="Command-explanations.html#Command-explanations" accesskey="u" rel="up">Command explanations</a> &nbsp; [<a href="Concept-index.html#Concept-index" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
