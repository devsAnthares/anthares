<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Copyright (C) 1999-2014 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts, and with no Back-Cover
Texts.  A copy of the license is included in the section entitled "GNU
Free Documentation License". -->
<!-- Created by GNU Texinfo 6.5, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Running Parted (Parted User&rsquo;s Manual)</title>

<meta name="description" content="Running Parted (Parted User&rsquo;s Manual)">
<meta name="keywords" content="Running Parted (Parted User&rsquo;s Manual)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<link href="index.html#Top" rel="start" title="Top">
<link href="Concept-index.html#Concept-index" rel="index" title="Concept index">
<link href="Using-Parted.html#Using-Parted" rel="up" title="Using Parted">
<link href="Invoking-Parted.html#Invoking-Parted" rel="next" title="Invoking Parted">
<link href="Partitioning.html#Partitioning" rel="prev" title="Partitioning">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<a name="Running-Parted"></a>
<div class="header">
<p>
Next: <a href="Invoking-Parted.html#Invoking-Parted" accesskey="n" rel="next">Invoking Parted</a>, Previous: <a href="Partitioning.html#Partitioning" accesskey="p" rel="prev">Partitioning</a>, Up: <a href="Using-Parted.html#Using-Parted" accesskey="u" rel="up">Using Parted</a> &nbsp; [<a href="Concept-index.html#Concept-index" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<a name="Using-GNU-Parted"></a>
<h3 class="section">2.2 Using GNU Parted</h3>
<a name="index-modes-of-use"></a>

<p>Parted has two modes: command line and interactive.  Parted should
always be started with:
</p>
<div class="example">
<pre class="example"># <kbd>parted <var>device</var></kbd>
</pre></div>

<p>where <var>device</var> is the hard disk device to edit.  (If you&rsquo;re
lazy and omit the DEVICE argument, Parted will attempt to guess which
device you want.)
</p>
<p>In command line mode, this is followed by one or more commands.  For
example:
</p>
<div class="example">
<pre class="example"># <kbd>parted /dev/sda mklabel gpt mkpart P1 ext3 1MiB 8MiB </kbd>
</pre></div>

<p>Options (like <kbd>--help</kbd>) can only be specified on the
command line.
</p>
<p>In interactive mode, commands are entered one at a time at a prompt, and
modify the disk immediately.  For example:
</p>
<div class="example">
<pre class="example">(parted) <kbd>mklabel gpt</kbd>
(parted) <kbd>mkpart P1 ext3 1MiB 8MiB </kbd>
</pre></div>

<p>Unambiguous abbreviations are allowed.  For example, you can
type &ldquo;p&rdquo; instead of &ldquo;print&rdquo;, and &ldquo;u&rdquo; instead of &ldquo;units&rdquo;.
Commands can be typed either in English, or your native language (if
your language has been translated).  This may create ambiguities.
Commands are case-insensitive.
</p>
<p>Numbers indicating partition locations can be whole numbers or decimals.
The suffix selects the unit, which may be one of those described in
<a href="unit.html#unit">unit</a>, except CHS and compact.  If no suffix is given, then the default
unit is assumed.  Negative numbers count back from the end of the disk,
with &ldquo;-1s&rdquo; indicating the sector at the end of the disk.
Parted will compute sensible
ranges for the locations you specify (e.g. a range of +/- 500 MB when you
specify the location in &ldquo;G&rdquo;). Use the sector unit &ldquo;s&rdquo; to specify exact
locations.  With parted-2.4 and newer,
IEC binary units like &ldquo;MiB&rdquo;, &ldquo;GiB&rdquo;, &ldquo;TiB&rdquo;, etc., specify
exact locations as well.
See <a href="unit.html#IEC-binary-units">IEC binary units</a>.
</p>
<p>If you don&rsquo;t give a parameter to a command, Parted will prompt you for it.
For example:
</p>
<div class="example">
<pre class="example">(parted) <kbd>mklabel</kbd>
New disk label type? <kbd>gpt</kbd>
</pre></div>

<p>Parted will always warn you before doing something that is potentially
dangerous, unless the command is one of those that is inherently
dangerous (viz., rm, mklabel and mkpart).
Since many partitioning systems have complicated constraints, Parted will
usually do something slightly different to what you asked.  (For example,
create a partition starting at 10.352Mb, not 10.4Mb)
If the calculated values differ too much, Parted will ask you for
confirmation.
</p>

<hr>
<div class="header">
<p>
Next: <a href="Invoking-Parted.html#Invoking-Parted" accesskey="n" rel="next">Invoking Parted</a>, Previous: <a href="Partitioning.html#Partitioning" accesskey="p" rel="prev">Partitioning</a>, Up: <a href="Using-Parted.html#Using-Parted" accesskey="u" rel="up">Using Parted</a> &nbsp; [<a href="Concept-index.html#Concept-index" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
