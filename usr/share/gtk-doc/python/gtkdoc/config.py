version = "1.27"

# tools
dblatex = ''
fop = ''
highlight = '/usr/bin/highlight'
highlight_options = '--syntax=$SRC_LANG --out-format=xhtml -f --class-name=gtkdoc '
pkg_config = '/usr/bin/pkg-config'
xsltproc = '/usr/bin/xsltproc'

# configured directories
prefix = '/usr'
datarootdir = "${prefix}/share".replace('${prefix}', prefix)
datadir = "${datarootdir}".replace('${datarootdir}', datarootdir)

exeext = ''
