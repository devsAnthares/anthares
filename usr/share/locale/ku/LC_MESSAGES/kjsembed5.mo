��    '      T  5   �      `  Z   a  *   �     �                :     O     i  >   o     �  9   �     �                @     Y     v     �     �     �     �     �       !   $     F     e     }     �     �     �     �     �       (   )     R  >   e  &   �  &   �  6  �     )
  (   1
     Z
      v
      �
     �
  %   �
     �
  A     !   I  B   k     �     �     �     �  "        6  !   V  &   x  !   �     �     �     �  %        =  !   Y  )   {     �  '   �  !   �  "        2     E  2   e     �  2   �  *   �  *                           %   &                                      	         "   
                                               '             !            $         #                   %1 is 'the slot asked for foo arguments', %2 is 'but there are only bar available'%1, %2. %1 is not a function and cannot be called. %1 is not an Object type '%1' is not a valid QLayout. '%1' is not a valid QWidget. Action takes 2 args. ActionGroup takes 2 args. Alert Bad event handler: Object %1 Identifier %2 Method %3 Type: %4. Call to '%1' failed. Call to method '%1' failed, unable to get argument %2: %3 Confirm Could not construct value Could not create temporary file. Could not open file '%1' Could not open file '%1': %2 Could not read file '%1' Failed to create Action. Failed to create ActionGroup. Failed to create Layout. Failed to create Widget. Failed to load file '%1' File %1 not found. First argument must be a QObject. Incorrect number of arguments. Must supply a filename. Must supply a layout name. Must supply a valid parent. Must supply a widget name. No classname specified No classname specified. No such method '%1'. Not enough arguments. There was an error reading the file '%1' Wrong object type. but there is only %1 available but there are only %1 available include only takes 1 argument, not %1. library only takes 1 argument, not %1. Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2010-08-13 15:45+0200
Last-Translator: Erdal Ronahî <erdal.ronahi@nospam.gmail.com>
Language-Team: Kurdish Team http://pckurd.net
Language: ku
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Virtaal 0.6.1
X-Launchpad-Export-Date: 2007-11-26 09:44+0000
X-Poedit-Language: Kurdish
X-Poedit-Country: Kurdistan
X-Poedit-SourceCharset: utf-8
 %1, %2. %1 ne fonksiyonek e û nayê gazîkirin. %1 ne cureyekî bireseran e '%1' ne QLayout'eke derbasdar e. '%1' ne QWidget'eke derbasdar e. Çalakî 2 argûmanan distîne. KomaÇalakiyan 2 argûmanan distîne. Şiyarî Xebatkera bûyerên nebaş: Hêman %1 Nasker %2 Metod %3 Cure: %4 Gazîkirina '%1 'ê bi ser neket. Gazîkirina metoda '%1' têkçû, nekarî hevoksaziya %2 bîne: %3 Piştrastkirin Nirx nehat serastkirin Pelê demdemî nayê çêkirin. Vekirina pelê '%1' biserneket Vekirina pelê '%1' biserneket: %2 Xwendina pelê '%1' biserneket. Afirandina çalakiyê biserneket. Afirandina koma çalakiyan biserneket. Afirandina Mizanpajê biserneket. Afirandina Widget biserneket. Barkirina pelê '%1' biserneket Pelê %1 nehate dîtin. Divê argûmenta yekemîn QObject be. Hejmara çewt a argûmetan. Divê navê pelî bê diyarkirin. Divê navê bicîbûnekê bê diyarkirin. Divê çavkanî bê diyarkirin. Divê navê parçeyekê bê diyarkirin. Navê sinifê nehatiye diyarkirin Navê sinifê nehatiye diyarkirin. Şewaza '%1' tune. Argûmanên heyî têrê nakin. Dema pelê '%1' hat xwendin çewtiyek derket holê Cureyê hêmanan çêewt e. lê li vir tenê %1 heye lê li vir tenê %1 henin include tenê argunamekê distîne, ne %1. library tenê argunamekê distîne, ne %1. 