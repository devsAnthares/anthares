��          �      l      �  3   �  $     �   :     �  4   �     �  #        =  �   E  �   �  +   �     �     �     �  1     (   3     \  �   s  $   �  (     �  F  2     	   A     K     W  6   d     �  )   �     �  �   �  �   �	  5   v
     �
     �
     �
     �
  .        1     H  *   T  /                                                      	   
                                            1 action for this device %1 actions for this device @info:status Free disk space%1 free Accessing is a less technical word for Mounting; translation should be short and mean 'Currently mounting this device'Accessing... All devices Click to access this device from other applications. Click to eject this disc. Click to safely remove this device. General It is currently <b>not safe</b> to remove this device: applications may be accessing it. Click the eject button to safely remove this device. It is currently <b>not safe</b> to remove this device: applications may be accessing other volumes on this device. Click the eject button on these other volumes to safely remove this device. It is currently safe to remove this device. Most Recent Device No Devices Available Non-removable devices only Open auto mounter kcmConfigure Removable Devices Open popup when new device is plugged in Removable devices only Removing is a less technical word for Unmounting; translation shoud be short and mean 'Currently unmounting this device'Removing... This device is currently accessible. This device is not currently accessible. Project-Id-Version: plasma_applet_devicenotifier
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2016-07-02 11:54+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 1 åtgärd för enheten %1 åtgärder för enheten %1 ledigt Ansluter... Alla enheter Klicka för att komma åt enheten från andra program. Klicka för att mata ut skivan. Klicka för säker urkoppling av enheten. Allmänt Det är för närvarande <b>inte säkert</b> att ta bort enheten: det kan finnas program som använder den. Klicka på utmatningsknappen för säker borttagning av enheten. Det är för närvarande <b>inte säkert</b> att ta bort enheten: det kan finnas program som använder andra volymer på enheten. Klicka på utmatningsknappen för de andra volymerna för säker borttagning av enheten. Det är för närvarande säkert att ta bort enheten. Senaste enhet Inga tillgängliga enheter Bara fasta enheter Anpassa flyttbara enheter Öppna meddelanderuta när en ny enhet ansluts Bara flyttbara enheter Tar bort... Enheten är för närvarande tillgänglig. Enheten är för närvarande inte tillgänglig. 