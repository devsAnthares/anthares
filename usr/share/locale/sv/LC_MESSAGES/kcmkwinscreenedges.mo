��    %      D  5   l      @     A     E     G  	   Y     c     |     �     �     �     �     �     �            S   ,  w   �     �  M         [     |  ?   �     �  	   �     �     
     #  %   2     X     e  F   �  #   �     �  ]     R   f     �     �  �  �     �	     �	     �	     �	     �	     �	     �	  	   �	      
      
     9
     T
     r
     �
  W   �
  V   �
     F  5   U     �     �  <   �     �               0     >  ,   S     �     �  X   �     �       U   (  B   ~     �     �               !      "          #   %                                                                            $                          	                
                            ms % %1 - All Desktops %1 - Cube %1 - Current Application %1 - Current Desktop %1 - Cylinder %1 - Sphere &Reactivation delay: &Switch desktop on edge: Activation &delay: Active Screen Corners and Edges Activity Manager Always Enabled Amount of time required after triggering an action until the next trigger can occur Amount of time required for the mouse cursor to be pushed against the edge of the screen before the action is triggered Application Launcher Change desktop when the mouse cursor is pushed against the edge of the screen EMAIL OF TRANSLATORSYour emails Lock Screen Maximize windows by dragging them to the top edge of the screen NAME OF TRANSLATORSYour names No Action Only When Moving Windows Open krunnerRun Command Other Settings Quarter tiling triggered in the outer Show Desktop Switch desktop on edgeDisabled Tile windows by dragging them to the left or right edges of the screen Toggle alternative window switching Toggle window switching Trigger an action by pushing the mouse cursor against the corresponding screen edge or corner Trigger an action by swiping from the screen edge towards the center of the screen Window Management of the screen Project-Id-Version: kcmkwinscreenedges
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-30 03:13+0100
PO-Revision-Date: 2018-01-02 19:01+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
  ms % %1 - Alla skrivbord %1 - Kub %1 - Aktuellt program %1 - Aktuellt skrivbord %1 - Cylinder %1 - Klot Återaktiveringsför&dröjning: Byt &skrivbord vid kant: Aktiveringsför&dröjning: Aktiva skärmhörn och kanter Aktivitetshanterare Alltid aktiverad Hur lång tid som krävs efter en åtgärd har påbörjats innan nästa kan påbörjas. Hur lång tid som muspekaren måste hållas mot skärmkanten innan åtgärden utförs. Starta program Byt skrivbord när muspekaren hålls mot skärmkanten stefan.asserhall@bredband.net Lås skärmen Maximera fönster genom att dra dem till skärmens överkant Stefan Asserhäll Ingen åtgärd Bara när fönster flyttas Kör kommando Andra inställningar Fjärdedels sida-vid-sida utlöst i de yttre Visa skrivbord Inaktiverad Lägg fönster sida vid sida genom att dra dem till skärmens vänster- eller högerkant Ändra alternativt fönsterbyte Ändra fönsterbyte Utför en åtgärd genom att trycka muspekaren mot motsvarande skärmkant eller hörn Utför en åtgärd genom att svepa från skärmens kant mot mitten Fönsterhantering av skärmen 