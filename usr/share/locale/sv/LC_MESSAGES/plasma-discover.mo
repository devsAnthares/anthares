��    d      <  �   \      �     �     �     �     �     �  5   �  #   �  E   	  E   _	  >   �	     �	     �	  7   
     E
     _
     p
     �
     �
     �
     �
     �
     �
          	          4     B     W     i     n  	   u          �     �  !   �  F   �     #     @     Q  <   c     �     �  *   �     �      �            	        &     6  	   >     H     X     _      h     �  
   �     �     �     �     �  
   �  F     :   K     �     �     �     �     �     �  8   �     �       	     
   "     -     =     F     W     l     r     �     �     �     �     �     �     �  &   �     "  
   >     I     Y     y     �     �     �     �  $   �  �  �     �     �     �     �     �  =   �  ,     F   A  F   �  ?   �       
   %  E   0     v     �      �     �     �     �       	              5     >     Q     f     |     �     �     �  	   �     �     �     �  *     F   ;     �     �     �  4   �     �       )        7  !   F     h  	   u          �  
   �     �     �     �     �  %   �      �           -     K     ]     p     w  N   �  <   �  	              '     /     B     J  K   Z     �     �     �     �     �     �     �     �               =     E     b     w  "   �  	   �     �     �     �     �     �               %     .     ?     Z  &   r         J   `   :       	   W                 [   .   E         )      (   R   M   '   O   $       7   Y   4                -                    9       8   #   S       0   P       B      
   +                         L   >              N   @      *   D      F   /   ;       5       2   ^      U               ?   6       <   V   ,       c       \   Z      G   1                  d       C   Q   &       A          _   b       =   T      !   3             a   K           ]              H      %      I   X          "    
Also available in %1 %1 %1 (%2) %1 (Default) <b>%1</b> by %2 <em>%1 out of %2 people found this review useful</em> <em>Tell us about this review!</em> <em>Useful? <a href='true'><b>Yes</b></a>/<a href='false'>No</a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'><b>No</b></a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'>No</a></em> @infoFetching updates @infoFetching... @infoIt is unknown when the last check for updates was @infoLooking for updates @infoNo updates @infoNo updates are available @infoShould check for updates @infoThe system is up to date @infoUpdates @infoUpdating... Accept Add Source... Addons Aleix Pol Gonzalez An application explorer Apply Changes Available backends:
 Available modes:
 Back Cancel Category: Checking for updates... Comment too long Comment too short Compact Mode (auto/compact/full). Could not close the application, there are tasks that need to be done. Could not find category '%1' Couldn't open %1 Delete the origin Directly open the specified application by its package name. Discard Discover Display a list of entries with a category. Extensions... Failed to remove the source '%1' Featured Help... Homepage: Improve summary Install Installed Jonathan Thomas Launch License: List all the available backends. List all the available modes. Loading... Local package file to install Make default More Information... More... No Updates Open Discover in a said mode. Modes correspond to the toolbar buttons. Open with a program that can deal with the given mimetype. Proceed Rating: Remove Resources for '%1' Review Reviewing '%1' Running as <em>root</em> is discouraged and unnecessary. Search Search in '%1'... Search... Search: %1 Search: %1 + %2 Settings Short summary... Show reviews (%1)... Size: Sorry, nothing found... Source: Specify the new source for %1 Still looking... Summary: Supports appstream: url scheme Tasks Tasks (%1%) TransactioName TransactionStatus%1 %2 Unable to find resource: %1 Update All Update Selected Update section nameUpdate (%1) Updates Version: unknown reviewer updates not selected updates selected © 2010-2016 Plasma Development Team Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:02+0100
PO-Revision-Date: 2018-01-17 22:07+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 
Också tillgänglig via %1 %1 %1 (%2) %1 (förval) <b>%1</b> av %2 <em>%1 av %2 personer ansåg att omdömet var till nytta</em> <em>Informera oss om det här omdömet.</em> <em>Nyttigt? <a href='true'><b>Ja</b></a>/<a href='false'>Nej</a></em> <em>Nyttigt? <a href='true'>Ja</a>/<a href='false'><b>Nej</b></a></em> <em>Nyttigt? <a href='true'>Ja</a>/<a href='false'>Nej</a></em> Hämtar uppdateringar Hämtar... Det är inte känt när senaste kontrollen av uppdateringar utfördes Söker efter uppdateringar Inga uppdateringar Inga uppdateringar tillgängliga Bör kontrollera uppdateringar Systemet är aktuellt Uppdateringar Uppdaterar... Acceptera Lägg till källa... Tillägg Aleix Pol Gonzalez En programutforskare Verkställ ändringar Tillgängliga gränssnitt:
 Tillgängliga lägen:
 Tillbaka Avbryt Kategori: Söker efter uppdateringar... Kommentar för lång Kommentar för kort Kompakt läge (automatiskt/kompakt/fullt). Kunde inte stäng programmet, det finns uppgifter som måste utföras. Kunde inte hitta kategori '%1' Kunde inte öppna %1 Ta bort ursprunget Öppna angivet program direkt enligt dess paketnamn. Kasta Upptäck Visa en lista med poster med en kategori. Utökningar... Misslyckades ta bort källan '%1' Presenterade Hjälp... Hemsida: Förbättra sammanfattning Installera Installerat Jonathan Thomas Starta Licens: Lista alla tillgängliga gränssnitt. Lista alla tillgängliga lägen. Läser in... Lokal paketfil att installera Gör till förval Mer information... Mer... Inga uppdateringar Öppna Upptäck med angivet läge. Lägen motsvarar knapparna i verktygsraden. Öppna med ett program som kan hantera den givna Mime-typen. Fortsätt Betyg: Ta bort Resurser för '%1' Omdöme Recenserar '%1' Att köra som <em>systemadministratör</em> är onödigt och avråds från. Sök Sök i '%1'... Sök... Sök: %1 Sök: %1 + %2 Inställningar Kort sammanfattning... Visa omdömen (%1)... Storlek: Tyvärr hittades ingenting ... Källa: Ange den nya källan för %1 Fortsätter leta ... Sammanfattning: Stöder appstream: webbadresschema Uppgifter Uppgifter (%1 %) %1 %2 Kan inte hitta resurs: %1 Uppdatera alla Uppdatera markerade Uppdatera (%1) Uppdateringar Version: okänd recensent uppdateringar ej markerade uppdateringar markerade © 2010-2016 Plasma-utvecklingsgruppen 