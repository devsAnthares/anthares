��          �            h     i     �  #   �      �      �     �  J        [  &   y     �  T   �       *   $     O  �  ]     	     &  #   :     ^     ~     �  O   �  !     8   #     \  `   n     �      �                                  	                                             
       Also index file content Configure File Search Copyright 2007-2010 Sebastian Trüg Do not search in these locations EMAIL OF TRANSLATORSYour emails Enable File Search File Search helps you quickly locate all your files based on their content Folder %1 is already excluded Folder's parent %1 is already excluded NAME OF TRANSLATORSYour names Not allowed to exclude root folder, please disable File Search if you do not want it Sebastian Trüg Select the folder which should be excluded Vishesh Handa Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2017-10-18 21:53+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Indexera också filinnehåll Anpassa filsökning Copyright 2007-2010 Sebastian Trüg Sök inte på följande platser stefan.asserhall@bredband.net Aktivera filsökning Filsökning hjälper dig att snabbt hitta alla dina filer baserat på innehåll Katalogen %1 är redan undantagen Katalogens överliggande katalog %1 är redan undantagen Stefan Asserhäll Det är inte tillåtet att utesluta rotkatalogen. Inaktivera filsökning om du inte vill ha den. Sebastian Trüg Välj katalogen som ska undantas Vishesh Handa 