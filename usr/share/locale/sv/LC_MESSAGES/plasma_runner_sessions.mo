��          �   %   �      P  �  Q          )  6   9  -   p     �     �     �     �  (   �  d        x     �     �     �     �     �     �                7  "   X     {     �     �  �  �  �  �     [
     p
  5   ~
  3   �
  
   �
     �
            -   '  m   U     �  	   �     �     �  
   �       
     
     	   #  	   -  	   7     A     P     T                               	                                                                      
                        <p>You have chosen to open another desktop session.<br />The current session will be hidden and a new login screen will be displayed.<br />An F-key is assigned to each session; F%1 is usually assigned to the first session, F%2 to the second session and so on. You can switch between sessions by pressing Ctrl, Alt and the appropriate F-key at the same time. Additionally, the KDE Panel and Desktop menus have actions for switching between sessions.</p> Lists all sessions Lock the screen Locks the current sessions and starts the screen saver Logs out, exiting the current desktop session New Session Reboots the computer Restart the computer Shutdown the computer Starts a new session as a different user Switches to the active session for the user :q:, or lists all active sessions if :q: is not provided Turns off the computer User sessionssessions Warning - New Session lock screen commandlock log out log out commandLogout log out commandlogout new session restart computer commandreboot restart computer commandrestart shutdown computer commandshutdown switch user switch user commandswitch switch user commandswitch :q: Project-Id-Version: plasma_runner_sessions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2011-01-11 18:34+0100
Last-Translator: Stefan Asserhall <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=2; plural=n != 1;
 <p>Du har valt att öppna en annan skrivbordssession.<br />Den aktuella sessionen kommer att döljas, och en ny inloggningsskärm visas.<br />En funktionstangent är tilldelad varje session: F%1 tilldelas oftast den första sessionen, F%2 tilldelas den andra, och så vidare. Du kan byta mellan sessioner genom att samtidigt trycka på Ctrl, Alt och en lämplig funktionstangent. Dessutom har KDE:s panel och skrivbordsmenyer alternativ för att byta mellan sessioner.</p> Lista alla sessioner Lås skärmen Låser nuvarande session och startar skärmsläckaren Loggar ut, och avslutar nuvarande skrivbordssession Ny session Startar om datorn Starta om datorn Stäng av datorn Startar en ny session som en annan användare Byter till aktiv session för användaren :q:, eller listar alla aktiva sessioner om :q: inte tillhandahålls Stänger av datorn sessioner Varning: Ny session lås utloggning Logga ut utloggning ny session starta om starta om stäng av byt användare byt byt :q: 