��    
      l      �       �      �            	               .  I   3     }  	   �  �  �     ?     P     b     h     o     �  Q   �     �     �               
   	                           &Trigger word: Add Item Alias Alias: Character Runner Config Code Creates Characters from :q: if it is a hexadecimal code or defined alias. Delete Item Hex Code: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2010-07-30 21:32+0200
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=2; plural=n != 1;
 U&tlösande ord: Lägg till objekt Alias Alias: Inställning av teckenprogram Kod Skapar tecken från :q: om det är en hexadecimal kod eller ett definierat alias. Ta bort objekt Hexkod: 