��          �   %   �      p     q  +   �  .   �  6   �  *     #   D  &   h  /   �  2   �  :   �  0   -  3   ^  ;   �  K   �  -     $   H  '   m     �     �     �     �     �  #        1     O     c     |  �  �     5     >  !   Z  +   |  ,   �     �     �      	  !   0	  2   R	  $   �	  &   �	  6   �	  Z   
  %   c
     �
     �
     �
     �
     �
     �
     �
          )     @     M     \                                                    	   
                                                                    @action:buttonCommit @info:statusAdded files to SVN repository. @info:statusAdding files to SVN repository... @info:statusAdding of files to SVN repository failed. @info:statusCommit of SVN changes failed. @info:statusCommitted SVN changes. @info:statusCommitting SVN changes... @info:statusRemoved files from SVN repository. @info:statusRemoving files from SVN repository... @info:statusRemoving of files from SVN repository failed. @info:statusReverted files from SVN repository. @info:statusReverting files from SVN repository... @info:statusReverting of files from SVN repository failed. @info:statusSVN status update failed. Disabling Option "Show SVN Updates". @info:statusUpdate of SVN repository failed. @info:statusUpdated SVN repository. @info:statusUpdating SVN repository... @item:inmenuSVN Add @item:inmenuSVN Commit... @item:inmenuSVN Delete @item:inmenuSVN Revert @item:inmenuSVN Update @item:inmenuShow Local SVN Changes @item:inmenuShow SVN Updates @labelDescription: @title:windowSVN Commit Show updates Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2015-11-08 11:44+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 Arkivera Filer tillagda i SVN-arkiv. Lägger till filer i SVN-arkiv... Tillägg av filer i SVN-arkiv misslyckades. Arkivering av ändringar i SVN misslyckades. Arkiverar ändringar i SVN. Arkiverar ändringar i SVN... Filer borttagna från SVN-arkiv. Tar bort filer från SVN-arkiv... Borttagning av filer från SVN-arkiv misslyckades. Filer återställda från SVN-arkiv. Återställer filer från SVN-arkiv... Återställning av filer från SVN-arkiv misslyckades. Uppdatering av SVN-status misslyckades. Inaktiverar alternativet "Visa SVN-uppdateringar". Uppdatering av SVN-arkiv misslyckades Uppdaterade SVN-arkiv Uppdaterar SVN-arkiv... Lägg till i SVN Arkivera i SVN... Ta bort i SVN SVN återställ Uppdatera från SVN Visa lokala ändringar i SVN Visa SVN-uppdateringar Beskrivning: Arkivera i SVN Visa uppdateringar 