��          �      �           	          &     -     4  
   =     H     Q     Y     i  >   w     �     �     �     �  
   �     �     �     �            U   %  �  {     '     6     J     W  	   _     i  	   r     |     �     �  ;   �     �     �     �       
        $     *     2     E     X  r   _                       	                                                
                                 %1 is running %1 not running &Reset &Start Advanced Appearance Command: Display Execute command Notifications Remaining time left: %1 second Remaining time left: %1 seconds Run command S&top Show notification Show seconds Show title Text: Timer Timer finished Timer is running Title: Use mouse wheel to change digits or choose from predefined timers in the context menu Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2015-04-22 17:20+0200
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 %1 är startad %1 är inte startad Åte&rställ &Starta Avancerat Utseende Kommando: Visning Kör kommando Underrättelser Återstående tid: %1 sekund Återstående tid: %1 sekunder Utför kommando S&toppa Visa underrättelser Visa sekunder Visa titel Text: Stoppur Tidtagning stoppad Tidtagning startad Titel: Använd mushjulet för att ändra siffror eller välja bland fördefinierade tider i den sammanhangsberoende menyn 