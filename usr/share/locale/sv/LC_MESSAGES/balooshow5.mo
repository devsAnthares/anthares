��          �      \      �     �     �  
   �                :      N     o  
   }     �     �     �  	   �  (   �  \   	     f  2   t     �     �  �  �     o     }     �     �     �     �     �     	  
        '     9     Y  
   u  *   �  ]   �     	  0        N     ^     	                                                                                 
             %1 Terms: %2 (c) 2012, Vishesh Handa Baloo Show Device id for the files EMAIL OF TRANSLATORSYour emails File Name Terms: %1 Inode number of the file to show Internal Info Maintainer NAME OF TRANSLATORSYour names No index information found Print internal info Terms: %1 The Baloo data Viewer - A debugging tool The Baloo index could not be opened. Please run "%1" to see if Baloo is enabled and working. The file urls The fileID is not equal to the actual Baloo fileID This is a bug Vishesh Handa Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-28 03:17+0100
PO-Revision-Date: 2018-01-28 22:23+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 %1 termer: %2 © 2012, Vishesh Handa Baloo-visare Enhets-id för filerna stefan.asserhall@bredband.net Filnamnstermer: %1 I-nodnummer för filen att visa Intern information Underhåll Stefan Asserhäll Ingen indexinformation hittades Skriv ut intern information Termer: %1 Baloo-datavisare - ett felsökningsverktyg Baloo-indexet kunde inte öppnas. Kör "%1" för att se om Baloo är aktiverat och fungerade. Filens webbadresser Fil-id är inte lika med verkligt fil-id i Baloo Det är ett fel Vishesh Handa 