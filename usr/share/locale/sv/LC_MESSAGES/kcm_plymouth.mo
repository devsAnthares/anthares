��          �   %   �      @     A  !   Y      {     �      �     �     �  +        =     N     [  (   z     �  ,   �  7   �     !  7   :     r  ]   �  1   �  	   "     ,  "   ?  5   b  �  �     D  $   _     �     �     �      �     �  +        D     Y     f  (   x     �  &   �  8   �     	  6   0	     g	  b   �	  C   �	     +
     8
  %   O
  5   u
                    
         	                                                                                                 Cannot start initramfs. Cannot start update-alternatives. Configure Plymouth Splash Screen Download New Splash Screens EMAIL OF TRANSLATORSYour emails Get New Boot Splash Screens... Initramfs failed to run. Initramfs returned with error condition %1. Install a theme. Marco Martin NAME OF TRANSLATORSYour names No theme specified in helper parameters. Plymouth theme installer Select a global splash screen for the system The theme to install, must be an existing archive file. Theme %1 does not exist. Theme corrupted: .plymouth file not found inside theme. Theme folder %1 does not exist. This module lets you configure the look of the whole workspace with some ready to go presets. Unable to authenticate/execute the action: %1, %2 Uninstall Uninstall a theme. update-alternatives failed to run. update-alternatives returned with error condition %1. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-28 03:05+0200
PO-Revision-Date: 2017-01-07 10:10+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Kan inte starta initramfs. Kan inte starta update-alternatives. Anpassa Plymouth startskärm Ladda ner nya startskärmar stefan.asserhall@bredband.net Hämta nya datorstartskärmar... Initramfs kunde inte köra. Initramfs returnerade med feltillstånd %1. Installera ett tema. Marco Martin Stefan Asserhäll Inget tema angivet i hjälpparametrarna. Plymouth temainstallation Välj global startskärm för systemet Temat att installera, måste vara en befintlig arkivfil. Temat %1 finns inte. Tema skadat: .plymouth-fil hittades inte inne i temat. Temakatalogen %1 finns inte. Modulen låter dig ställa in hela arbetsrymdens utseende, med några färdiga förinställningar. Kunde inte behörighetskontrollera eller utföra åtgärden: %1, %2 Avinstallera Avinstallera ett tema. update-alternatives kunde inte köra. update-alternatives returnerade med feltillstånd %1. 