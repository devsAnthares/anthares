��            )   �      �     �     �     �     �     �     �     �     �  0        3     G     ]     c     o     w     �  
   �     �     �     �     �     �               (     A     I     a     m  �  s       $   '     L     b  	   u       	   �     �  8   �     �  "   �       	        (     0     L     i  %   u     �  "   �     �      �               &     A     J     _     k                                            
                                                                                      	       %1 by %2 Add Custom Wallpaper Add Folder... Add Image... Background: Blur Centered Change every: Directory with the wallpaper to show slides from Download Wallpapers Get New Wallpapers... Hours Image Files Minutes Next Wallpaper Image Open Containing Folder Open Image Open Wallpaper Image Positioning: Recommended wallpaper file Remove wallpaper Restore wallpaper Scaled Scaled and Cropped Scaled, Keep Proportions Seconds Select Background Color Solid color Tiled Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:39+0100
PO-Revision-Date: 2017-11-19 21:07+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 %1 x %2 Lägg till eget skrivbordsunderlägg Lägg till katalog... Lägg till bild... Bakgrund: Suddig Centrerat Ändra var: Katalog med skrivbordsunderlägget att visa bilder från Ladda ner skrivbordsunderlägg Hämta nya skrivbordsunderlägg... timmar Bildfiler minuter Nästa skrivbordsunderlägg Öppna katalog med innehåll Öppna bild Öppna bild för skrivbordsunderlägg Positionering: Rekommenderat skrivbordsunderlägg Ta bort skrivbordsunderlägg Återställ skrivbordsunderlägg Skalat Skalat och beskuret Skalat, behåll proportion sekunder Välj bakgrundsfärg Enkel färg Sida vid sida 