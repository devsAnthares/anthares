��    ?        Y         p     q  !   �  "   �  &   �  ,   �  #   &  &   J  !   q  &   �  '   �  "   �  #     "   )  '   L     t     �  +   �  2   �  
   �     �               ,     3     G  U   O  7   �     �     �     		     	      	     6	     T	     c	     k	  !   �	     �	  	   �	     �	     �	     �	     �	  &   
     /
     N
     U
     p
     v
     ~
     �
  4   �
  $   �
     �
               /     G     ]     w  &   �     �  �  �     q     �     �  	   �     �     �  
   �     �     �     �     �     �     �            
   (  ?   3  D   s  	   �     �  
   �     �     �     �       X     J   u     �     �  
   �     �  )     $   1     V     d     m     �     �  
   �  
   �  %   �     �       3         R     s     {     �     �     �     �  7   �  '   �           5     U     k     z     �     �  '   �     �     $                               1          "   *   )          :             	         ;   !                          9   =   5   /   3                    (         #   4       8       -   6   
   7          %                                         ?       0   &      +          '         .       ,      >       <   2        &Matching window property:  @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Border @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large @item:inlistbox Button size:Large @item:inlistbox Button size:Normal @item:inlistbox Button size:Small @item:inlistbox Button size:Very Large Active Window Glow Add Add handle to resize windows with no border Allow resizing maximized windows from window edges Animations B&utton size: Border size: Button mouseover transition Center Center (Full Width) Class:  Configure fading between window shadow and glow when window's active state is changed Configure window buttons' mouseover highlight animation Decoration Options Detect Window Properties Dialog Edit Edit Exception - Oxygen Settings Enable/disable this exception Exception Type General Hide window title bar Information about Selected Window Left Move Down Move Up New Exception - Oxygen Settings Question - Oxygen Settings Regular Expression Regular Expression syntax is incorrect Regular expression &to match:  Remove Remove selected exception? Right Shadows Tit&le alignment: Title:  Use the same colors for title bar and window content Use window class (whole application) Use window title Warning - Oxygen Settings Window Class Name Window Drop-Down Shadow Window Identification Window Property Selection Window Title Window active state change transitions Window-Specific Overrides Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-13 05:20+0100
PO-Revision-Date: 2017-12-13 18:53+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 &Matchande fönsteregenskap:  Enorm Stor Utan kant Utan sidokant Normal Förstorad Mycket liten Större än enorm Mycket stor Stor Normal Liten Mycket stor Glöd för aktivt fönster Lägg till Lägg till grepp för att ändra storlek på fönster utan kant Tillåt storleksändring av maximerade fönster från fönsterkanter Animering &Knappstorlek: Kantbredd: Knappövergång för mus Centrera Centrera (full bredd) Klass:  Anpassa toning mellan fönsterskugga och glöd när fönstrets aktiva tillstånd ändras Anpassa fönsterknapparnas animerade markering när musen hålls över dem Dekorationsalternativ Detektera fönsteregenskaper Dialogruta Redigera Redigera undantag - Oxygen-inställningar Aktivera eller inaktivera undantaget Undantagstyp  Allmänt Dölj namnlist på fönster Information om valda fönster Vänster Flytta ner Flytta upp Nytt undantag - Oxygen-inställningar Fråga - Oxygen-inställningar Reguljärt uttryck Syntaxen för det reguljära uttrycket är felaktig Reguljärt uttryck a&tt matcha:  Ta bort Ta bort markerat undantag? Höger Skuggor Namn&listjustering: Namn:  Använd samma färg för namnlist och fönsterinnehåll Använd fönsterklass (hela programmet) Använd fönsternamn Varning - Oxygen-inställningar Fönsterklassens namn Fönsterskugga Fönsteridentifikation Val av fönsteregenskap Fönsternamn Fönster aktiva tillståndsövergångar Fönsterspecifika alternativ 