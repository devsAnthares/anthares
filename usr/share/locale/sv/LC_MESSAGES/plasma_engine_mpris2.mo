��          t      �         C        U     i  3   �     �     �  F   �  5   *     `       �  �  G   :     �     �     �     �     �  >   �  4   0     e     �                              	                          
    Attempting to perform the action '%1' failed with the message '%2'. Media playback next Media playback previous Name for global shortcuts categoryMedia Controller Play/Pause media playback Stop media playback The argument '%1' for the action '%2' is missing or of the wrong type. The media player '%1' cannot perform the action '%2'. The operation '%1' is unknown. Unknown error. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-06 03:09+0200
PO-Revision-Date: 2015-02-18 19:08+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 Försök att utföra åtgärden '%1' misslyckades med meddelandet '%2'. Media spela nästa Media spela föregående Mediakontroll Spela/Pausa medieuppspelning Stoppa medieuppspelning Argumentet '%1' för åtgärden '%2' saknas eller har fel typ. Mediaspelaren '%1' kan inte utföra åtgärden '%2'. Åtgärden '%1' är okänd. Okänt fel. 