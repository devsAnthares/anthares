��          �      <      �     �     �     �  +   �  @        L     a     i     w     }     �  
   �     �     �     �     �     �  �  
     �     �     �  +   �  M        `     u     ~  
   �     �     �  
   �     �     �     �     �  $        
         	                                                                                       <a href='%1'>%1</a> Close Copy Automatically: Don't show this dialog, copy automatically. Drop text or an image onto me to upload it to an online service. Error during upload. General History Size: Paste Please wait Please, try again. Sending... Share Shares for '%1' Successfully uploaded The URL was just shared Upload %1 to an online service Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-01 04:04+0100
PO-Revision-Date: 2015-04-25 22:09+0200
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 <a href='%1'>%1</a> Stäng Kopiera automatiskt: Visa inte dialogrutan, kopiera automatiskt. Släpp en text eller en bild här för att ladda upp den till en nättjänst. Fel vid uppladdning. Allmänt Historikstorlek: Klistra in Vänta Försök igen. Skickar... Dela Delade resurser för '%1' Uppladdad med lyckat resultat Webbadressen har just delats Ladda upp %1 till en Internettjänst 