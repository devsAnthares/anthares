��            )   �      �  !   �  "   �  '   �  ,     #   ;  &   _  !   �  &   �  '   �     �                 V   $  1   {     �  *   �     �        
     
   "     -     6     ;     D     T     [     a     g  �  p     -     3     8     D     T  
   [     f     l     ~     �     �     �     �  `   �  7        J  &   e  "   �     �     �     �     �     �     �      	     	  	   	     "	     '	                                                                          
                                                    	           @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Borders @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large Application menu Border si&ze: Buttons Close Close by double clicking:
 To open the menu, keep the button pressed until it appears. Close windows by double clicking &the menu button Context help Drag buttons between here and the titlebar Drop here to remove button Get New Decorations... Keep above Keep below Maximize Menu Minimize On all desktops Search Shade Theme Titlebar Project-Id-Version: kcmkwindecoration
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-04-14 02:49+0200
PO-Revision-Date: 2015-10-06 18:37+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 Enorm Stor Utan kanter Utan sidokanter Normal Förstorad Liten Större än enorm Mycket stor Programmeny Kant&bredd: Knappar Stäng Stäng genom att dubbelklicka:
 För att visa menyn, håll knappen nedtryckt till den dyker upp. S&täng fönster genom att dubbelklicka på menyknappen Sammanhangsberoende hjälp Dra knappar härifrån till namnlisten Släpp här för att ta bort knapp Hämta nya dekorationer... Behåll ovanför Behåll under Maximera Meny Minimera På alla skrivbord Sök Rulla upp Tema Namnlist 