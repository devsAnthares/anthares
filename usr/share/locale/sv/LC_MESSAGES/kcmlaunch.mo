��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  �  K     �  %   �  �    l  �  K   6
     �
     �
     �
  *   �
     �
     �
  %     !   3                                          
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2003-12-06 13:26+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Svenska <sv@li.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.3
Plural-Forms: nplurals=2; plural=n != 1;
  sek Tids&gräns för startunderrättelse: <H1>Underrättelse i aktivitetsfält</H1>
Du kan aktivera en andra metod för underrättelse av start som
används av aktivitetsfältet där en knapp med ett roterande timglas
visas, vilket symboliserar att programmet håller på att startas.
Det kan visa sig att vissa program inte är medvetna om denna typ
av underrättelse, i vilket fall knappen försvinner efter tiden
angiven under "Tidsgräns för startunderrättelse". <h1>Upptagen pekare</h1>
KDE erbjuder en upptagen pekare som startunderrättelse.
För att aktivera upptagen pekare, välj en typ av visuell återkoppling
från kombinationsrutan.
Det kan visa sig att vissa program inte är medvetna om denna typ
av underrättelse. I det fallet slutar pekaren blinka efter tiden
angiven under "Tidsgräns för startunderrättelse". <h1>Gensvar vid start</h1> Här kan du ställa in gensvar vid programstart. Blinkande pekare Studsande pekare &Upptagen pekare Ak&tivera underrättelse i aktivitetsfält Ingen upptagen pekare Passiv upptagen pekare Ti&dsgräns för startunderrättelse: Underrättelse i a&ktivitetsfält 