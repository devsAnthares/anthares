��    
      l      �       �   $   �   %     :   <     w  '   �  -   �     �       '     �  <  $   �  -     /   ;  !   k  0   �  +   �     �  
   
  +            	                            
        Could not detect the file's mimetype Could not find all required functions Could not find the provider with the specified destination Error trying to execute script Invalid path for the requested provider It was not possible to read the selected file Service was not available Unknown Error You must specify a URL for this service Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-08-02 19:47+0200
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=2; plural=n != 1;
 Kunde inte detektera filens Mime-typ Kunde inte hitta alla nödvändiga funktioner Kunde inte hitta leverantören med angivet mål Fel vid försök att köra skript Ogiltig sökväg till den begärda leverantören Det gick inte att läsa den markerade filen Tjänsten är inte tillgänglig Okänt fel Du måste ange en webbadress för tjänsten 