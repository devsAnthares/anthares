��    8      �  O   �      �  A   �          2  /   I  8   y     �     �     �     �     �                      $   ,  	   Q     [  !   q  3   �  	   �     �      �  $   �       *   /     Z  	   _  5   i     �     �  
   �     �     �  	   �          !     @  5   O  3   �  6   �     �  ,   �  /   )	  	   Y	     c	     z	     �	     �	  O   �	  Z   �	  b   J
  	   �
  
   �
  P   �
       �  #  7   �          +  8   A      z     �     �     �     �     �     �  
               !   #     E     M  *   \  0   �  
   �  
   �     �  $   �          "     +     2  .   9     h     �  
   �     �     �     �     �     �     �  A     D   Z  E   �     �  ?   �     2     7     ?     ]     e     m  Z   �  o   �  b   M     �     �  T   �                                                 ,   	          #       )   $                                         +   (   &   1            7   4                         6           8   
   *   .             /       %      3   !           0   2      "            -       '           5        %1 is an external application and has been automatically launched (c) 2009, Ben Cooksley (c) 2017, Marco Martin <i>Contains 1 item</i> <i>Contains %1 items</i> A search yelded no resultsNo items matching your search About %1 About Active Module About Active View About System Settings All Settings Apply Settings Author Back Ben Cooksley Central configuration center by KDE. Configure Configure your system Contains 1 item Contains %1 items Determines whether detailed tooltips should be used Developer Dialog EMAIL OF TRANSLATORSYour emails Expand the first level automatically Frequently used: General config for System SettingsGeneral Help Icon View Internal module representation, internal module model Internal name for the view used Keyboard Shortcut: %1 Maintainer Marco Martin Mathias Soeken Most Used Most used module number %1 NAME OF TRANSLATORSYour names No views found Provides a categorized icons view of control modules. Provides a categorized sidebar for control modules. Provides a classic tree-based view of control modules. Relaunch %1 Reset all current changes to previous values Search through a list of control modulesSearch Search... Show detailed tooltips Sidebar Sidebar View System Settings System Settings was unable to find any views, and hence has nothing to display. System Settings was unable to find any views, and hence nothing is available to configure. The settings of the current module have changed.
Do you want to apply the changes or discard them? Tree View View Style Welcome to "System Settings", a central place to configure your computer system. Will Stephenson Project-Id-Version: systemsettings
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:08+0100
PO-Revision-Date: 2017-10-19 20:57+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 %1 är ett externt program och har startats automatiskt © 2009, Ben Cooksley © 2017, Marco Martin <i>Innehåller 1 objekt</i> <i>Innehåller %1 objekt</i> Inga objekt motsvarar sökningen Om %1 Om aktiv modul Om aktiv vy Om systeminställningar Alla inställningar Verkställ inställningar Upphovsman Tillbaka Ben Cooksley Inställningscentral för av KDE. Anpassa Anpassa datorn Innehåller 1 objekt Innehåller %1 objekt Avgör om detaljerade verktygstips ska användas Utvecklare Dialogruta stefan.asserhall@bredband.net Expandera automatiskt första nivån Oftast använda: Allmänt Hjälp Ikonvy Intern modulrepresentation, intern modulmodell Internt namn på använd vy Snabbtangent: %1 Underhåll Marco Martin Mathias Soeken Oftast använda Oftast använda modul nummer %1 Stefan Asserhäll Några vyer hittades inte Tillhandahåller en kategoriserad ikonvy av inställningsmoduler. Tillhandahåller en kategoriserad sidorad för inställningsmoduler. Tillhandahåller en klassisk trädbaserad vy av inställningsmoduler. Starta om %1 Återställ alla nuvarande ändringar till föregående värden Sök Sök... Visa detaljerade verktygstips Sidorad Sidorad Systeminställningar Systeminställningar kunde inte hitta några vyer, och kan därefter inte visa någonting. Systeminställningar kunde inte hitta några vyer, och därefter är inte någonting tillgängligt att anpassa. Inställningarna i nuvarande modul har ändrats.
Vill du verkställa ändringarna eller kasta dem? Trädvy Vystil Välkommen till "Systeminställningar", en central plats för att anpassa din dator. Will Stephenson 