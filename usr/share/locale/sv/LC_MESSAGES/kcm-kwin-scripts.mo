��          �      �       0  (   1     Z      q     �     �     �     �     �     �  `     ^   u     �  �  �  '   �     �     �     �     �          ,     8     T  "   f  /   �     �        
              	                                    *.kwinscript|KWin scripts (*.kwinscript) Configure KWin scripts EMAIL OF TRANSLATORSYour emails Get New Scripts... Import KWin Script Import KWin script... KWin Scripts KWin script configuration NAME OF TRANSLATORSYour names Placeholder is error message returned from the install serviceCannot import selected script.
%1 Placeholder is name of the script that was importedThe script "%1" was successfully imported. Tamás Krutki Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-19 03:12+0100
PO-Revision-Date: 2017-10-13 16:44+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 *.kwinscript|Kwin-skript (*.kwinscript) Anpassa Kwin-skript stefan.asserhall@bredband.net Hämta nya skript... Importera Kwin-skript Importera Kwin-skript... Kwin-skript Inställning av Kwin-skript Stefan Asserhäll Kan inte importera valt skript.
%1 Skriptet "%1" importerades med lyckat resultat. Tamás Krutki 