��          L      |       �   >   �      �   9   �   A   &  
   h  �  s  H        h     l  9   s     �                                         '%1' needs a password to be accessed. Please enter a password. ... A default name for an action without proper labelUnknown A new device has been detected.<br><b>What do you want to do?</b> Do nothing Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2014-04-27 07:33+0200
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.4
 '%1' behöver ett lösenord för att kommas åt. Skriv in ett lösenord. ... Okänd En ny enhet har detekterats.<br><b>Vad vill du göra?</b> Gör ingenting 