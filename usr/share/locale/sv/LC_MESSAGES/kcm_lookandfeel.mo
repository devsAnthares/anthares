��          �      |      �     �  2        B     b       #   �      �     �  %   �       
   &     1     >     ]  �   }       ]   (     �  p   �  :        P  �  \  ,     E   5  '   {     �     �  ,   �     
     (  2   @  !   s  
   �     �     �  (   �  �   �     �	  b   �	  #    
  �   $
  G   �
  	   �
                                                                      
                	           Apply a look and feel package Command line tool to apply look and feel packages. Configure Look and Feel details Copyright 2017, Marco Martin Cursor Settings Changed Download New Look And Feel Packages EMAIL OF TRANSLATORSYour emails Get New Looks... List available Look and feel packages Look and feel tool Maintainer Marco Martin NAME OF TRANSLATORSYour names Reset the Plasma Desktop layout Select an overall theme for your workspace (including plasma theme, color scheme, mouse cursor, window and desktop switcher, splash screen, lock screen etc.) Show Preview This module lets you configure the look of the whole workspace with some ready to go presets. Use Desktop Layout from theme Warning: your Plasma Desktop layout will be lost and reset to the default layout provided by the selected theme. You have to restart KDE for cursor changes to take effect. packagename Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-27 05:46+0100
PO-Revision-Date: 2017-12-20 19:29+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Applicera ett paket med utseende och känsla Kommandoradsverktyg för att applicera paket med utseende och känsla Anpassa detaljerat utseende och känsla Copyright 2017, Marco Martin Pekarinställningar ändrade Ladda ner nya paket med utseende och känsla stefan.asserhall@bredband.net Hämta nya utseenden... Lista tillgängliga paket med utseende och känsla Verktyg för utseende och känsla Underhåll Marco Martin Stefan Asserhäll Återställ layout för Plasma skrivbord Välj ett övergripande tema för arbetsrymden (inklusive Plasma-tema, färgschema, muspekare, fönster- och skrivbordsbyte, startskärm, låsskärm, etc.) Visa förhandsgranskning Modulen låter dig ställa in hela arbetsrymdens utseende, med några färdiga förinställningar. Använd skrivbordslayout från tema Varning: Plasma-skrivbordets layout kommer att gå förlorad och återställas till standardlayouten som tillhandahålls av valt tema. KDE måste startas om för att ändringarna av pekare ska verkställas. paketnamn 