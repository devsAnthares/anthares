��          �   %   �      P     Q     b     w     �     �     �  
   �     �     �     �     �     �  /        3     Q     p     v     �     �     �     �     �     �     �       �  %     �     �     �          *     8     O     X     a     h  
   n     y  7   �     �     �     �            
        (     B     ]     x     �     �                            
                                                                                             	    %1 file %1 files %1 folder %1 folders %1 of %2 processed %1 of %2 processed at %3/s %1 processed %1 processed at %2/s Appearance Behavior Cancel Clear Configure... Finished Jobs List of running file transfers/jobs (kuiserver) Move them to a different list Move them to a different list. Pause Remove them Remove them. Resume Show all jobs in a list Show all jobs in a list. Show all jobs in a tree Show all jobs in a tree. Show separate windows Show separate windows. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2014-04-08 21:11+0200
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.4
 %1 fil %1 filer %1 katalog %1 kataloger %1 av %2 behandlade %1 av %2 behandlade med %3/s %1 behandlade %1 behandlade med %2/s Utseende Beteende Avbryt Rensa Anpassa... Avslutade jobb Lista med pågående filöverföringar/jobb (kuiserver) Flytta dem till en annan lista Flytta dem till en annan lista. Paus Ta bort dem Ta bort dem. Återuppta Visa alla jobb i en lista Visa alla jobb i en lista. Visa alla jobb i ett träd Visa alla jobb i ett träd. Visa separata fönster Visa separata fönster. 