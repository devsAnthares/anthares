��          �      �       0     1  �  H     �       &         ?     `     n     v     �     �  (   �  �  �     �  �  �     �     �  &   �     �     �     �               $  -   ;         
                 	                                (c) 2002-2006 KDE Team <h1>System Notifications</h1>Plasma allows for a great deal of control over how you will be notified when certain events occur. There are several choices as to how you are notified:<ul><li>As the application was originally designed.</li><li>With a beep or other noise.</li><li>Via a popup dialog box with additional information.</li><li>By recording the event in a logfile without any additional visual or audible alert.</li></ul> Carsten Pfeiffer Charles Samuels Disable sounds for all of these events EMAIL OF TRANSLATORSYour emails Event source: KNotify NAME OF TRANSLATORSYour names Olivier Goffart Original implementation System Notification Control Panel Module Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-09 07:18+0200
PO-Revision-Date: 2017-08-09 22:49+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 © 2002-2006 KDE-gruppen <h1>Systemunderrättelser</h1>Plasma ger dig stor kontroll över hur du kommer att bli underrättad när speciella händelser inträffar. Du kan välja mellan flera olika sätt att bli underrättad på:<ul><li>Så som programmet ursprungligen var konstruerat.</li><li>Med ett pip eller annat ljud.</li><li>Via en dialogruta som visar ytterligare information.</li><li>Genom att spara händelsen i en loggfil utan någon ytterligare visuell eller ljudlig varning.</li></ul> Carsten Pfeiffer Charles Samuels Inaktivera ljud för alla händelserna stefan.asserhall@bredband.net Händelsekälla: KNotify Stefan Asserhäll Olivier Goffart Originalimplementation Inställningsmodul för systemunderrättelser 