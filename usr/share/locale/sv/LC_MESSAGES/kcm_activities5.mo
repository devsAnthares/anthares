��    *      l  ;   �      �     �     �  )   �               /     H     ^  #   ~     �  
   �     �     �  %   �  +        E     Z     m  @   z     �     �     �     �               '     ,     9     ?     _     e  .   m     �  F   �  (   �  	   '  	   1  	   ;  '   E  7   m  "   �  �  �     t	     �	  #   �	  
   �	     �	  	   �	     �	     �	     �	     
     "
     .
     D
  +   S
  /   
     �
     �
     �
  I   �
     0     C  
   P     [     p     �     �     �     �     �     �  
   �  -   �       M   *  0   x  	   �     �     �     �     �     �                   $                                 %                      *                                               	   #   )   "             &           
                        '             (   !    &Do not remember @actionWalk through activities @actionWalk through activities (Reverse) @action:buttonApply @action:buttonCancel @action:buttonChange... @action:buttonCreate @title:windowActivity Settings @title:windowCreate a New Activity @title:windowDelete Activity Activities Activity information Activity switching Are you sure you want to delete '%1'? Blacklist all applications not on this list Clear recent history Create activity... Description: Error loading the QML files. Check your installation.
Missing %1 For a&ll applications Forget a day Forget everything Forget the last hour Forget the last two hours General Icon Keep history Name: O&nly for specific applications Other Privacy Private - do not track usage for this activity Remember opened documents: Remember the current virtual desktop for each activity (needs restart) Shortcut for switching to this activity: Shortcuts Switching Wallpaper for in 'keep history for 5 months'for  unit of time. months to keep the history month  months unlimited number of monthsforever Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-03-29 18:39+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 &Kom inte ihåg Gå igenom aktiviteter Gå igenom aktiviteter (baklänges) Verkställ Avbryt Ändra... Skapa Aktivitetsinställningar Skapa en ny aktivitet Ta bort aktivitet Aktiviteter Aktivitetsinformation Aktivitetsbyte Är du säker på att du vill ta bort '%1'? Svartlista alla program som inte finns i listan Rensa senaste historik Skapa aktivitet... Beskrivning: Fel vid inläsning av QML-filerna. Kontrollera installationen.
Saknar %1. För a&lla program Glöm en dag Glöm allt Glöm senaste timmen Glöm senaste två timmarna Allmänt Ikon Behåll historik Namn: &Bara för specifika program Annan Integritet Privat: Följ inte användning av aktiviteten Kom ihåg öppnade dokument: Kom ihåg aktuellt virtuellt skrivbord för varje aktivitet (kräver omstart) Genväg för att byta till den här aktiviteten: Genvägar Byte Skrivbordsunderlägg för   månad  månader för alltid 