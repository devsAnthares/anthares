��          �      �           	               4  	   I     S      c  "   �      �     �     �  	   �     �               )  
   9     D     S     a     g     {  �  �     0     3     ;     X  
   k     v     �  )   �  "   �     �     �     �       	        $     :  
   M     X     l     �     �  
   �                              	                                     
                                      K (HH:MM) (In minutes - min. 1, max. 600) Activate Night Color Automatic Detect location EMAIL OF TRANSLATORSYour emails Error: Morning not before evening. Error: Transition time overlaps. Latitude Location Longitude NAME OF TRANSLATORSYour names Night Color Night Color Temperature:  Operation mode: Roman Gilg Sunrise begins Sunset begins Times Transition duration and ends Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-12 03:09+0100
PO-Revision-Date: 2017-12-12 19:35+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
  K (TT:MM) (I minuter - min 1, max 600) Aktivera nattfärg Automatisk Detektera plats stefan.asserhall@bredband.net Fel: Morgonen kommer inte före kvällen. Fel: Övergångstider överlappar. Latitud Plats Longitud Stefan Asserhäll Nattfärg Nattfärgtemperatur:  Användningsmetod: Roman Gilg Soluppgång börjar Solnedgång börjar Tider Övergångstid och slutar 