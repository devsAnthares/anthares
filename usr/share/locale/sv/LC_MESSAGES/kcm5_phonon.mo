��    B      ,  Y   <      �  i   �  m        y  b   �     �     	  6        O  7   _     �     �  	   �     �  (   �  )   �  )   (     R     o  Y   u     �     �  �   �      y	  .   �	     �	  
   �	     �	     �	     
     
     !
     5
     B
     J
     c
     r
     w
     �
     �
     �
     �
     �
  	   �
  
   �
     �
     �
  	     
     
   *     5     B  	   `     j     o  
   �  �   �     (  8   8  �   q     �  8   	  ,   B  ,   o  %   �     �  �  �  Q   �  |   �     X  j   t     �     �  8        F  7   U     �     �     �     �  -   �  ,      -   -  $   [     �  W   �     �     �  s        �  0   �     �     �     �            
   &     1     C     S  #   [          �     �     �     �     �     �  
   �     �  
                   8     F     R  	   [     e  	   �     �     �  	   �  �   �     [  E   h  �   �     ;  8   K  4   �  5   �     �     	     $   !   %          ;   ,          B                 8   #                 7   9       +              )   ?   -   <         6                                    *   2       >                1       @       .   5   3   :   A         &      /                4              0      "             =   (   '         	   
           @info User changed Phonon backendTo apply the backend change you will have to log out and back in again. A list of Phonon Backends found on your system.  The order here determines the order Phonon will use them in. Apply Device List To... Apply the currently shown device preference list to the following other audio playback categories: Audio Hardware Setup Audio Playback Audio Playback Device Preference for the '%1' Category Audio Recording Audio Recording Device Preference for the '%1' Category Backend Colin Guthrie Connector Copyright 2006 Matthias Kretz Default Audio Playback Device Preference Default Audio Recording Device Preference Default Video Recording Device Preference Default/Unspecified Category Defer Defines the default ordering of devices which can be overridden by individual categories. Device Configuration Device Preference Devices found on your system, suitable for the selected category.  Choose the device that you wish to be used by the applications. EMAIL OF TRANSLATORSYour emails Failed to set the selected audio output device Front Center Front Left Front Left of Center Front Right Front Right of Center Hardware Independent Devices Input Levels Invalid KDE Audio Hardware Setup Matthias Kretz Mono NAME OF TRANSLATORSYour names Phonon Configuration Module Playback (%1) Prefer Profile Rear Center Rear Left Rear Right Recording (%1) Show advanced devices Side Left Side Right Sound Card Sound Device Speaker Placement and Testing Subwoofer Test Test the selected device Testing %1 The order determines the preference of the devices. If for some reason the first device cannot be used Phonon will try to use the second, and so on. Unknown Channel Use the currently shown device list for more categories. Various categories of media use cases.  For each category, you may choose what device you prefer to be used by the Phonon applications. Video Recording Video Recording Device Preference for the '%1' Category  Your backend may not support audio recording Your backend may not support video recording no preference for the selected device prefer the selected device Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2014-04-08 21:04+0200
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.4
 För att verkställa gränssnittsändringen måste du logga ut och logga in igen. En lista med gränssnitt för Phonon som hittades på systemet. Ordningen här avgör i vilken ordning Phonon använder dem. Använd enhetslista för... Använd enhetsinställningarna som för närvarande visas för följande andra ljuduppspelningskategorier: Inställning av ljudhårdvara Ljuduppspelning Ljuduppspelningsenhetens inställning för kategori '%1' Ljudinspelning Ljudinspelningsenhetens inställning för kategori '%1' Bakgrundsprogram Colin Guthrie Kontakt Copyright 2006 Matthias Kretz Standardinställning av ljuduppspelningsenhet Standardinställning av ljudinspelningsenhet Standardinställning av videoinspelningsenhet Ospecificerad eller standardkategori Lägre prioritet Definierar standardordningen för enheter, som kan överskridas av enskilda kategorier. Enhetsinställning Enhetsinställning Enheter som hittades på systemet, lämpliga för vald kategori. Välj enheten du vill ska användas av programmen. stefan.asserhall@bredband.net Misslyckades ställa in vald ljudutmatningsenhet Center fram Vänster fram Vänster om center fram Höger fram Höger om center fram Maskinvara Oberoende enheter Ingångsnivåer Ogiltig Inställning av ljudhårdvara i KDE Matthias Kretz Mono Stefan Asserhäll Inställningsmodul för Phonon Uppspelning (%1) Högre prioritet Profil Center bak Vänster bak Höger bak Inspelning (%1) Visa avancerade enheter Vänster sida Höger sida Ljudkort Ljudenhet Högtalarplacering och provning Subwoofer Test Testa markerad enhet Testar %1 Ordningen bestämmer vilken utenhet som föredras. Om den första enheten inte kan användas av någon anledning, försöker Phonon använda den andra,
och så vidare. Okänd kanal Använd enhetslistan som för närvarande visas för fler kategorier: Diverse kategorier av användarfall för media. För varje kategori kan du välja vilken enhet du föredrar ska användas av Phonon-program. Videoinspelning Videoinspelningsenhetens inställning för kategori '%1' Gränssnittet kanske inte stöder inspelning av ljud Gränssnittet kanske inte stöder inspelning av video vald enhet föredras inte vald enhet föredras 