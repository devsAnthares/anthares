��          �   %   �      @     A     Z     g          �     �      �     �     �     �            W   2  #   �     �     �     �     �  $        +     J     \     p     �  �  �     U     f     u     �     �     �  R   �          5     B     I     ]  #   q     �     �     �     �     �     �  ;        B     T     l     q                                           	                                                           
                        @title:menuMain Toolbar Clear Search Collapse All Categories Copyright 2009-2018 KDE Current Maintainer David Hubner EMAIL OF TRANSLATORSYour emails Expand All Categories Helge Deller Help button labelHelp Info Center Information Modules Information about current module located in about menuAbout Current Information Module Kaction search labelSearch Modules Main window titleKInfocenter Matthias Elter Matthias Ettrich Matthias Hoelzer-Kluepfel Module help button labelModule Help NAME OF TRANSLATORSYour names Nicolas Ternisien Previous Maintainer Search Bar Click MessageSearch Waldo Bastian Project-Id-Version: kinfocenter
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-08 05:56+0100
PO-Revision-Date: 2018-01-17 22:06+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 Huvudverktygsrad Rensa sökning Dra ihop alla kategorier Copyright 2009-2018 KDE Nuvarande utvecklare David Hubner newzella@linux.nu,pelinsr@algonet.se,awl@hem.passagen.se,d96reftl@dtek.chalmers.se Expandera alla kategorier Helge Deller Hjälp Informationscentral Informationsmoduler Om den aktuella informationsmodulen Sök efter moduler Informationscentral Matthias Elter Matthias Ettrich Matthias Hoelzer-Kluepfel Hjälp om modul Mattias Newzella,Per Lindström,Anders Widell,Magnus Reftel Nicolas Ternisien Föregående utvecklare Sök Waldo Bastian 