��    7      �  I   �      �     �     �     �     �     �     �               $     -     5     H     T      `     �     �     �     �     �     �     �     �                     )     7  	   C  	   M     W     d     j     �     �     �     �     �     �     �     �     �     �  @   �     7     @     \     c     j     r     �     �     �  4   �     �  �  �     �	     �	     �	     �	     �	  	   �	     �	     �	     
     

     
     
  	   +
     5
     S
     Z
  "   m
  	   �
     �
  	   �
     �
     �
     �
     �
               *     <     H     W     c     j     |     �     �     �     �  	   �     �     �     �  
   �  B   �     &      4     U     b  	   d  '   n     �     �     �     �     �     &   0      7                  #                
          	   '   (   +                       6              3      1                   ,   2                  "       *          $      4   )               %      5                     !          /   .   -       % %1 is value, %2 is unit%1 %2 %1: Application Energy Consumption Battery Capacity Charge Percentage Charge state Charging Current Degree Celsius°C Details: %1 Discharging EMAIL OF TRANSLATORSYour emails Energy Energy Consumption Energy Consumption Statistics Environment Full design Fully charged Has power supply Kai Uwe Broulik Last 12 hours Last 2 hours Last 24 hours Last 48 hours Last 7 days Last full Last hour Manufacturer Model NAME OF TRANSLATORSYour names No Not charging PID: %1 Path: %1 Rechargeable Refresh Serial Number Shorthand for WattsW System Temperature This type of history is currently not available for this device. Timespan Timespan of data to display Vendor VoltV Voltage Wakeups per second: %1 (%2%) WattW Watt-hoursWh Yes current power draw from the battery in WConsumption literal percent sign% Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2016-06-17 17:38+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 % %1 %2 %1: Energiförbrukning för program Batteri Kapacitet Laddningsprocent Laddningstillstånd Laddas Ström °C Detaljinformation: %1 Laddas ur stefan.asserhall@bredband.net Energi Energiförbrukning Statistik över energiförbrukning Omgivning Full konstruktion Fulladdat Har kraftaggregat Kai Uwe Broulik Senaste 12 timmarna Senaste två timmarna Senaste 24 timmarna Senaste 48 timmarna Senaste 7 dagarna Senast full Senaste timmen Tillverkare Modell Stefan Asserhäll Nej Laddas inte Process-ID: %1 Sökväg: %1 Uppladdningsbart Uppdatera Serienummer W System Temperatur Historiktypen är för närvarande inte tillgänglig för enheten. Tidsintervall Tidsintervall för data att visa Försäljare V Spänning Antal väckningar per sekund: %1 (%2 %) W Wh Ja Förbrukning % 