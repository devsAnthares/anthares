��    &      L  5   |      P      Q     r     ~  -   �  #   �  #   �  ?     1   S     �     �     �  H   �  L   �     F  V   N  N   �     �  
                   (     >     W  2   _  
   �     �  )   �  8   �  #      .   D  -   s  D   �  6   �  0     A   N  =   �  9   �  �  	  $   �
     �
  #   �
  8   	     B     R     b     y     �     �     �     �     �     �     �                 
   )     4     G     a  	   �  	   �     �  &   �  '   �     �  &     ?   +  )   k  %   �     �     �     �     �     �                  "                            
       $                      !                &                                      %      	           #                                %1 notification %1 notifications %1 of %2 %3 %1 running job %1 running jobs &Configure Event Notifications and Actions... 10 seconds ago, keep short10 s ago 30 seconds ago, keep short30 s ago A button tooltip; expands the item to show detailsShow Details A button tooltip; hides item detailsHide Details Clear Notifications Copy Copy Link Address Either just 1 dir or m of n dirs are being processed1 dir %2 of %1 dirs Either just 1 file or m of n files are being processed1 file %2 of %1 files History How much many bytes (or whether unit in the locale has been copied over total%1 of %2 Indicator that there are more urls in the notification than previews shown+%1 Information Job Failed Job Finished More Options... No new notifications. No notifications or jobs Open... Placeholder is job name, eg. 'Copying'%1 (Paused) Select All Show a history of notifications Show application and system notifications Speed and estimated time to completion%1 (%2 remaining) Track file transfers and other jobs Use custom position for the notification popup minutes ago, keep short%1 min ago %1 min ago notification was added n days ago, keep short%1 day ago %1 days ago notification was added yesterday, keep shortYesterday notification was just added, keep shortJust now placeholder is row description, such as Source or Destination%1: the job, which can be anything, failed to complete%1: Failed the job, which can be anything, has finished%1: Finished Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:07+0100
PO-Revision-Date: 2018-01-14 19:39+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 %1 underrättelse %1 underrättelser %1 av %2 %3 %1  jobb som kör %1  jobb som kör An&passa underrättelser om händelser och åtgärder... för 10 s sedan för 30 s sedan Visa detaljinformation Dölj detaljinformation Rensa underrättelser Kopiera Kopiera länkadress 1 katalog %2 av %1 kataloger 1 fil %2 av %1 filer Historik %1 av %2 +%1 Information Jobb misslyckades Jobb klart Fler alternativ... Inga nya underrättelser. Inga underrättelser eller jobb Öppna... %1 (paus) Markera alla Visa en historik över underrättelser Visa program- och systemunderrättelser %1 (%2 återstår) Följ filöverföringar och andra jobb Använd egen position för meddelanderutan för underrättelser för %1 minut sedan för %1 minuter sedan för %1 dag sedan för %1 dagar sedan Igår just nu %1: %1: Misslyckades %1: Klar 