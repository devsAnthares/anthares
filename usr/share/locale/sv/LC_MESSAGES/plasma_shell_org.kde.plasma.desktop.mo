��    F      L  a   |         
     
     
        "     1     5     I     X     ^  	   m     w  	        �     �     �  
   �     �     �  3   �  	   �     �               $     9     @     G     V     f     m  
        �  2   �  ?   �                    )     2     ?     N     S     a     r     �     �     �     �     �     �  	   �     �     �     �     �  b   �  �   [	     �	     �	  	   
     
     (
  
   8
  	   C
     M
     ]
     e
     k
     }
  �  �
     :     F     Z  "   o     �      �     �  
   �     �     �     �               "     .  
   5     @     I     P     R     Z     l          �     �     �     �     �     �     �     �       =     F   Q     �     �     �     �     �     �     �     �     �               !     0     >     E  
   Q     \     d     j     {     �  b   �  �   �     �     �     �     �     �  	   �     �          1     F     L     `     +       E   C   4       &       D           
   <      *   9           !   F      /   #                A       @   =                     $   ;                  B             :       '           0         1   2                                -   (       6   ?         >              "   .   %   ,              )       8       3   5   	                  7    Activities Add Action Add Spacer Add Widgets... Alt Alternative Widgets Always Visible Apply Apply Settings Apply now Author: Auto Hide Back-Button Bottom Cancel Categories Center Close Concatenation sign for shortcuts, e.g. Ctrl+Shift+ Configure Configure activity Create activity... Ctrl Currently being used Delete Email: Forward-Button Get new widgets Height Horizontal-Scroll Input Here Keyboard shortcuts Layout cannot be changed whilst widgets are locked Layout changes must be applied before other changes can be made Layout: Left Left-Button License: Lock Widgets Maximize Panel Meta Middle-Button More Settings... Mouse Actions OK Panel Alignment Remove Panel Right Right-Button Screen Edge Search... Shift Stop activity Stopped activities: Switch The settings of the current module have changed. Do you want to apply the changes or discard them? This shortcut will activate the applet: it will give the keyboard focus to it, and if the applet has a popup (such as the start menu), the popup will be open. Top Undo uninstall Uninstall Uninstall widget Vertical-Scroll Visibility Wallpaper Wallpaper Type: Widgets Width Windows Can Cover Windows Go Below Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-09 03:10+0200
PO-Revision-Date: 2017-09-09 14:53+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Aktiviteter Lägg till åtgärd Lägg till mellanrum Lägg till grafiska komponenter... Alt Alternativa grafiska komponenter Alltid synlig Verkställ Verkställ inställningar Verkställ nu Upphovsman: Dölj automatiskt Bakåtknapp Längst ner Avbryt Kategorier Centrera Stäng + Anpassa Anpassa aktivitet Skapa aktivitet... Ctrl Används för närvarande Ta bort E-post: Framåtknapp Hämta nya grafiska komponenter Höjd Horisontell rullning Skriv in här Snabbtangenter Layout kan inte ändras medan grafiska komponenter är låsta Layoutändringar måste verkställas innan andra ändringar kan göras Layout: Vänster Vänsterknapp Licens: Lås grafiska komponenter Maximera panel Meta Mittenknapp Fler inställningar... Musåtgärder Ok Paneljustering Ta bort panel Höger Högerknapp Skärmkant Sök... Skift Stoppa aktivitet Stoppade aktiviteter: Byt Inställningarna i nuvarande modul har ändrats. Vill du verkställa ändringarna eller kasta dem? Genvägen aktiverar miniprogrammet: Den ger tangentbordsfokus till det, och om miniprogrammet har en komponent som ska visas (såsom startmenyn), öppnas den. Längst upp Ångra avinstallera Avinstallera Avinstallera grafisk komponent Vertikal rullning Synlighet Skrivbordsunderlägg Typ av skrivbordsunderlägg: Grafiska komponenter Bredd Fönster kan täcka Fönster hamnar under 