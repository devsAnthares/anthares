��          t      �         P     )   b  %   �  @   �     �  V   	     `     {     �     �  �  �     R  ,   Y  2   �  ,   �     �  Q   �  )   Q     {  	   �     �                         	      
                             %1 is '%1 packages to update' and %2 is 'of which %1 is security updates'%1, %2 1 package to update %1 packages to update 1 security update %1 security updates First part of '%1, %2'1 package to update %1 packages to update No packages to update Second part of '%1, %2'of which 1 is security update of which %1 are security updates Security updates available System up to date Update Updates available Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-15 03:15+0100
PO-Revision-Date: 2018-01-15 20:18+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 %1, %2 1 paket att uppdatera %1 paket att uppdatera 1 säkerhetsuppdatering %1 säkerhetsuppdateringar 1 paket att uppdatera %1 paket att uppdatera Inga paket att uppdatera av vilka ett är en säkerhetsuppdatering av vilka %1 är säkerhetsuppdateringar Säkerhetsuppdateringar är tillgängliga Systemet är uppdaterat Uppdatera Uppdateringar tillgängliga 