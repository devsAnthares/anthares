��    _                   C   	  /   M  -   }     �     �     �     �      	     	     1	     >	     J	  
   S	     ^	     g	     p	     �	  	   �	     �	     �	     �	  ,   �	  	    
     

  
   )
     4
     L
     `
     u
     �
     �
     �
     �
     �
  	   �
     �
     �
     
               !     (  	   ;     E     ]  
   r     }     �  
   �     �     �     �  
   �     �     �               $     2     @     R     h     y     �     �     �     �  	   �     �     �     �               4     Q     j     �     �     �     �  	   �     �  ,   �          !     0     @     L     S     b     t     �  #   �     �  �  �     x     �  
   �     �     �  &   �      �          #     +     3     H  
   Q     \  	   b     l          �     �     �     �  4   �  	     "     
   3     >     Q     f     z     �     �     �     �     �     �     �     �     �     �     �            	        )     ?     V     d     z     �      �     �     �  
   �  	   �     �               *     :     J     b     z     �  *   �     �     �     �     �     �          	     $     5     G     b          �     �     �     �  	   �     �  3        5     H     c          �     �     �     �  !   �  0   �               Q         Y   5           %       H       J   !              \   *       @   V   4   [       A   (   
       U   B   ]   ^       $   _         '   >       D   K      -                  ?      ,   O   0       R   8   6   W   2   I   =   X   E   #       N   C       T   G   M          3   "         9             +          S          F           ;   :                    &   <   L          P         7          .       /       1                     )            	   Z       @action opens a software center with the applicationManage '%1'... @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Desktop Add to Favorites Add to Panel (Widget) Align search results to bottom All Applications App name (Generic name)%1 (%2) Applications Apps & Docs Behavior Categories Computer Contacts Description (Name) Description only Documents Edit Application... Edit Applications... End session Expand search to bookmarks, files and emails Favorites Flatten menu to a single level Forget All Forget All Applications Forget All Contacts Forget All Documents Forget Application Forget Contact Forget Document Forget Recent Documents General Generic name (App name)%1 (%2) Hibernate Hide %1 Hide Application Icon: Lock Lock screen Logout Name (Description) Name only Often Used Applications Often Used Documents Often used On All Activities On The Current Activity Open with: Pin to Task Manager Places Power / Session Properties Reboot Recent Applications Recent Contacts Recent Documents Recently Used Recently used Removable Storage Remove from Favorites Restart computer Run Command... Run a command or a search query Save Session Search Search results Search... Searching for '%1' Session Show Contact Information... Show In Favorites Show applications as: Show often used applications Show often used contacts Show often used documents Show recent applications Show recent contacts Show recent documents Show: Shut Down Sort alphabetically Start a parallel session as a different user Suspend Suspend to RAM Suspend to disk Switch User System System actions Turn off computer Type to search. Unhide Applications in '%1' Unhide Applications in this Submenu Widgets Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2017-09-09 14:53+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Hantera '%1'... Välj... Rensa ikon Lägg till på skrivbordet Lägg till i favoriter Lägg till i panel (grafisk komponent) Justera sökresultat längst ner Alla program %1 (%2) Program Program och dokument Beteende Kategorier Dator Kontakter Beskrivning (Namn) Bara beskrivning Dokument Redigera program... Redigera program... Avsluta session Expandera sökning till bokmärken, filer och e-post Favoriter Platta ut menyn till en enda nivå Glöm alla Glöm alla program Glöm alla kontakter Glöm alla dokument Glöm program Glöm kontakt Glöm dokument Glöm senaste dokument Allmänt %1 (%2) Dvala Dölj %1 Dölj program Ikon: Lås Lås skärmen Logga ut Namn (Beskrivning) Bara namn Ofta använda program Ofta använda dokument Ofta använda För alla aktiviteter För aktuell aktivitet Öppna med: Fäst upp i aktivitetshanteraren Platser Strömhantering och session Egenskaper Starta om Senaste program Senaste kontakter Senaste dokument Senast använda Senast använda Flyttbart lagringsmedia Ta bort från favoriter Starta om datorn Kör kommando... Kör ett kommando eller utför en sökning Spara session Sök Sökresultat Sök... Söker efter "%1" Session Visa kontaktinformation... Visa i favoriter Visa program som: Visa ofta använda program Visa ofta använda kontakter Visa ofta använda dokument Visa senaste program Visa senaste kontakter Visa senaste dokument Visa: Stäng av Sortera alfabetiskt Starta en parallell session som en annan användare Gå till viloläge Gå till viloläge i minne Gå till viloläge på disk Byt användare System Systemåtgärder Stäng av datorn Skriv för att söka. Dölj inte längre program i '%1' Dölj inte längre program i den här undermenyn Grafiska komponenter 