��    Q      �  m   ,      �     �     �     �                      .   !  U   P     �     �      �     �  /   �  /   -     ]     i     r  
   ~     �     �  $   �     �     �     �     �     	     	  	   *	     4	  
   F	  7   Q	     �	     �	     �	     �	  	   �	     �	  !   �	     
     
  	   !
      +
     L
     Y
     r
     �
     �
     �
     �
     �
     �
     �
  (   �
       =   '  2   e     �     �  "   �     �       J     1   Y  )   �  (   �  '   �  "     4   )     ^     l     r     {     �     �     �  U   �  N   !  9   p  J   �  �  �     �     �     �     �     �  
   �  
   �     �                ;      L     m  :   y  :   �     �     �               &     9  $   U     z     �     �     �     �     �     �     �     �     �               0     P  	   U     _     h     �     �  	   �  /   �     �          !     8     O  
   T     _     g     }     �  0   �     �     �     �                    +     3     C     J  /   h  -   �  ,   �  (   �  9        V     h  
   n     y     �     �     �  
   �     �     �  "   �        L   )             O                      4              6   $   ?   3         B              (   K   Q               ;       .   '   D   2   -       J   >   !   
   8                       P   5         ,          7   <           F      *   @       H   1                 "          9      +   &   0           /   N          #       E   	             I   A   =   %      G   :   C                   M    &All Desktops &Close &Fullscreen &Move &New Desktop &Pin &Shade 1 = number of desktop, 2 = desktop name&%1 %2 Activities a window is currently on (apart from the current one)Also available on %1 Add To Current Activity All Activities Allow this program to be grouped Alphabetically Always arrange tasks in columns of as many rows Always arrange tasks in rows of as many columns Arrangement Behavior By Activity By Desktop By Program Name Close Window or Group Cycle through tasks with mouse wheel Do Not Group Do Not Sort Filters Forget Recent Documents General Grouping and Sorting Grouping: Highlight windows Icon size: Invalid number of new messages, overlay, keep short— Keep &Above Others Keep &Below Others Keep launchers separate Large Ma&ximize Manually Mark applications that play audio Maximum columns: Maximum rows: Mi&nimize Minimize/Restore Window or Group More Actions Move &To Current Desktop Move To &Activity Move To &Desktop Mute New Instance On %1 On All Activities On The Current Activity On middle-click: Only group when the task manager is full Open groups in popups Open or bring to the front window of media player appRestore Over 9999 new messages, overlay, keep short9,999+ Pause playbackPause Play next trackNext Track Play previous trackPrevious Track Quit media player appQuit Re&size Remove launcher button for application shown while it is not runningUnpin Show all user Places%1 more Place %1 more Places Show only tasks from the current activity Show only tasks from the current desktop Show only tasks from the current screen Show only tasks that are minimized Show progress and status information in task buttons Show tooltips Small Sorting: Start New Instance Start playbackPlay Stop playbackStop The click actionNone Toggle action for showing a launcher button while the application is not running&Pin When clicking it would toggle grouping windows of a specific appGroup/Ungroup Which activities a window is currently onAvailable on %1 Which virtual desktop a window is currently onAvailable on all activities Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2018-01-17 22:07+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 &Alla skrivbord Stän&g &Fullskärmsläge &Flytta &Nytt skrivbord &Fäst upp Rulla &upp &%1 %2 Också tillgänglig för %1 Lägg till i aktuell aktivitet Alla aktiviteter Tillåt att programmet grupperas Alfabetiskt Arrangera alltid uppgifter i kolumner med så många rader Arrangera alltid uppgifter i rader med så många kolumner Arrangemang Beteende Enligt aktivitet Enligt skrivbord Enligt programnamn Stäng fönster eller grupp Gå igenom aktiviteter med mushjulet Gruppera inte Sortera inte Filter Glöm senaste dokument Allmänt Gruppering och sortering Gruppering: Markera fönster Ikonstorlek: — Behåll ovanför &andra &Behåll under andra Behåll startprogram separerade Stor Ma&ximera Manuellt Markera program som spelar ljud Maximalt antal kolumner: Maximalt antal rader: Mi&nimera Minimera eller återställ fönster eller grupp Fler åtgärder Flytta &till aktuellt skrivbord Flytta till &aktivitet Flytta till skrivbor&d Tyst Ny instans För %1 För alla aktiviteter För aktuell aktivitet Vid mittenklick: Gruppera bara när aktivitetshanteraren är full Öppna grupper i dialogrutor Återställ 9,999+ Paus Nästa spår Föregående spår Avsluta Ändra &storlek Ta ner %1 plats till %1 platser till Visa bara aktiviteter från nuvarande aktivitet Visa bara aktiviteter på nuvarande skrivbord Visa bara aktiviteter från nuvarande skärm Visa bara aktiviteter som är minimerade Visa förlopps- och statusinformation i aktivitetsknappar Visa verktygstips Liten Sortering: Starta ny instans Spela Stoppa Ingen &Fäst upp Gruppera eller dela up Tillgänglig för %1 Tillgänglig för alla aktiviteter 