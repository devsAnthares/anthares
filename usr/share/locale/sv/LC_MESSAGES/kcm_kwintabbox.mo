��    "      ,  /   <      �  
   �               ,     >     J  !   V     x     �     �     �     �     �  L   �     #     +     J     Y     u     z     �     �     �     �  	   �     �     �     �  �   �  F   �     �     �     �  �  �     �     �     �     �     �            	        (     <     N     b     s  Q   �     �  *   �     	  !    	  
   B	  
   M	     X	     v	     �	     �	  	   �	     �	     �	     �	  �   �	  I   m
     �
     �
     �
                    !                                            	                                                           "               
                    Activities All other activities All other desktops All other screens All windows Alternative An example Desktop NameDesktop 1 Content Current activity Current application Current desktop Current screen Filter windows by Focus policy settings limit the functionality of navigating through windows. Forward Get New Window Switcher Layout Hidden windows Include "Show Desktop" icon Main Minimization Only one window per application Recently used Reverse Screens Shortcuts Show selected window Sort order: Stacking order The currently selected window will be highlighted by fading out all other windows. This option requires desktop effects to be active. The effect to replace the list window when desktop effects are active. Virtual desktops Visible windows Visualization Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2012-05-23 21:11+0200
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.4
Plural-Forms: nplurals=2; plural=n != 1;
 Aktiviteter Alla andra aktiviteter Alla andra skrivbord Alla andra skärmar Alla fönster Alternativ metod Skrivbord 1 Innehåll Nuvarande aktivitet Nuvarande program Nuvarande skrivbord Nuvarande skärm Filtrera fönster enligt Inställningar av fokuspolicy begränsar funktionen för att gå igenom fönster. Framåt Hämta bytesutläggning för nytt fönster Dolda fönster Inkludera ikonen "Visa skrivbord" Huvudmetod Minimering Bara ett fönster per program Senast använda Omvänd Skärmar Genvägar Visa valt fönster Sorteringsordning: Lagerordning Fönstret som för närvarande är valt markeras genom att alla andra fönster tonas bort. Alternativet kräver skrivbordseffekter för att vara aktivt. Effekt att ersätta listfönstret med när skrivbordseffekter är aktiva. Virtuella skrivbord Synliga fönster Visualisering 