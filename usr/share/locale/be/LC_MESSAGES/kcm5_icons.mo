��    .      �  =   �      �     �     �               $     +  �   F  -   �  r      	   s  	   }     �     �     �     �  
   �     �     �      �               $     1     7     R     _     ~  	   �     �     �     �     �     �     �     �          
  +        B     J     X     `  S   m  )   �     �  �  �     �	     �	     
      
  	   :
     D
  �   _
  <     �   L     �     �  .        B     O     b     {     �  ]   �  -   �     )     I     R     j  4   w  ,   �  )   �  
             $     1     @  "   Z  B   }  F   �  F        N     Z  `   z     �     �     �       �   *  K   �  %                                .          #   )           *      +   -              '       ,   !   &                        $                    
   	             "                           %      (                    &Amount: &Effect: &Second color: &Semi-transparent &Theme (c) 2000-2003 Geert Jansen <qt>Are you sure you want to remove the <strong>%1</strong> icon theme?<br /><br />This will delete the files installed by this theme.</qt> <qt>Installing <strong>%1</strong> theme</qt> A problem occurred during the installation process; however, most of the themes in the archive have been installed Ad&vanced All Icons Antonio Larrosa Jimenez Co&lor: Colorize Confirmation Desaturate Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Effect Parameters Gamma Geert Jansen Icons Icons Control Panel Module Main Toolbar NAME OF TRANSLATORSYour names Name No Effect Panel Preview Remove Theme Set Effect... Setup Active Icon Effect Setup Default Icon Effect Setup Disabled Icon Effect Size: Small Icons The file is not a valid icon theme archive. To Gray To Monochrome Toolbar Torsten Rahn Unable to download the icon theme archive;
please check that address %1 is correct. Unable to find the icon theme archive %1. Use of Icon Project-Id-Version: kcmicons
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-08 06:27+0100
PO-Revision-Date: 2007-08-22 09:26+0000
Last-Translator: Darafei Praliaskouski <komzpa@licei2.com>
Language-Team: Belarusian <i18n@mova.org>
Language: be
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 &Агулам: &Эфект: &Другі колер: &Паўпразрысты &Тэма (c) 2000-2003 Geert Jansen <qt>Вы сапраўды хочаце выдаліць тэму значак <strong>%1</strong>?<br /><br />Вы выдаліце ўсе файлы гэтай тэмы.</qt> <qt>Усталяванне тэмы <strong>%1</strong></qt> Адбылася памылка падчас усталявання; аднак, большасць тэмаў з архіва была ўсталявана Ад&мысловыя Усе значкі Антоніо Ларроса Джыменэз Ко&лер: Фарбаваць Пацверджанне Зацяніць Апісанне Перацягніце сюды ці вызначце самі спасылку на тэму greendeath@mail.ru, ihar.hrachyshka@gmail.com Параметры эфекту Гама Гірт Джэнсэн Значкі Модуль кіравання значкамі KDE Галоўная панель начыння Eugene Zelenko, Ігар Грачышка Назва Няма эфекту Панель Прагляд Выдаліць тэму Усталяваць эфект... Наладзіць эфект для актыўных значак Наладзіць стандартны эфект для значак Наладзіць эфект для неактыўных значак Памер: Маленькія значкі Файл не з'яўляецца нармальным архівам з тэмай значак Шэры Чорна-белы Панель начыння Торстэн Ран Немагчыма запампаваць архіў з тэмай значак;
калі ласка, праверце правільнасць адраса %1. Немагчыма знайсці архіў з тэмай значак %1. Выкарыстанне значкі 