��          D      l       �      �      �   ,   �   c   �   �  P  E   F  "   �  L   �  �   �                          Also allow remote connections Logout canceled by '%1' Restores the saved user session if available The reliable KDE session manager that talks the standard X11R6 
session management protocol (XSMP). Project-Id-Version: ksmserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-05 03:18+0100
PO-Revision-Date: 2007-08-17 10:15+0000
Last-Translator: Darafei Praliaskouski <komzpa@licei2.com>
Language-Team: Belarusian <i18n@mova.org>
Language: be
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Таксама дазволіць аддаленыя злучэнні Выхад скасаваны '%1' Узнаўляе запісаны сеанс, калі ён даступны Надзейны кіраўнік сеансаў KDE які размаўляе на звычайным пратаколу X11R6 
кіравання сеансамі (XSMP). 