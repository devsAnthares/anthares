��    '      T  5   �      `     a  
   g     r     �  /   �  '   �      �  �   	     �     �  9        H     Q  
   `     k     w     �     �  E   �     �     �           #     D  }   J     �     �     �     �          "     1     G     Z     l     x     �     �  �  �     �	     �	  )   �	     �	  e   �	  K   ^
  B   �
  �  �
  '   w     �  \   �          -     G     [     s     �     �  �   �  1   A  +   s  1   �     �     �  �   �     �  '   �       ,        8     R  &   l  ,   �  !   �     �  ;   �     6     K        %   
   "                   !                     	          '      &                  #                              $                                                            msec  pixel/sec &Acceleration delay: &General &Move pointer with keyboard (using the num pad) &Single-click to open files and folders (c) 1997 - 2005 Mouse developers <h1>Mouse</h1> This module allows you to choose various options for the way in which your pointing device works. Your pointing device may be a mouse, trackball, or some other hardware that performs a similar function. Acceleration &profile: Acceleration &time: Activates and opens a file or folder with a single click. Advanced Bernd Gehrmann Brad Hards Brad Hughes Button Order David Faure Dirk A. Mueller Dou&ble-click to open files and folders (select icons on first click) Double click interval: Drag start distance: Drag start time: EMAIL OF TRANSLATORSYour emails Icons If you click with the mouse and begin to move the mouse at least the drag start distance, a drag operation will be initiated. Le&ft handed Ma&ximum speed: Mouse Mouse wheel scrolls by: NAME OF TRANSLATORSYour names Patrick Dowler Pointer acceleration: Pointer threshold: R&epeat interval: Ralf Nolden Re&verse scroll direction Righ&t handed Rik Hemsley Project-Id-Version: kcminput
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-05 03:17+0100
PO-Revision-Date: 2007-08-26 22:21+0300
Last-Translator: Darafei Praliaskouski <komzpa@licei2.com>
Language-Team: Belarusian <i18n@mova.org>
Language: be
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
  мс  пікселяў/с &Затрымка акселерацыі: &Агульныя &Перамяшчаць курсор клавіятурай (лічбавай клавіятурай) &Адзін націск для адкрыцця файлаў і тэчак (c) 1997 - 2005 Распрацоўшчыкі модуля "Мыш" <h1>Мыш</h1> Гэты модуль дазваляе выбраць з некалькіх варыянтаў спосаб працы вашай прылады пазіцыянавання курсору. Гэтай прыладай можа быць мыш, трэкбол ці іншае апаратнае забеспячэнне, якое выконвае падобныя функцыі &Профіль акселерацыі: &Час акселерацыі: Актывізуе і адкрывае файл ці тэчку адным націскам. Асаблівы Вернд Германн Брад Хардс Брэд Хьюджыс Парадак кнопак Дэвід Фор Дзірк А. Мюллер &Двайны націск для адкрыцця файлаў і тэчак (выбар значак адным націскам) Інтэрвал двайнога націску: Адлегласць для працягу: Інтэрвал для перацягвання: ihar.hrachyshka@gmail.com Значкі Калі вы націснулі кнопку мышы і падвінулі мыш як мінімум на гэтую дыстанцыю, будзе пачатая аперацыя перацягвання. Для &ляўшы Най&большая хуткасць: Мыш Колца мышы прамотвае на: Ігар Грачышка Патрык Доўлер Акселерацыя курсора: Адлегласць зрабатвання: Інтэрвал п&аўтору: Ральф Нолдэн Ад&варотны напрамак прамотвання Для &праўшы Рік Хемслі 