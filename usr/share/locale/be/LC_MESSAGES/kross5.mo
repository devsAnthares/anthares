��          �   %   �      @     A     S     Z      c     �     �     �     �  ,   �     �          0     6     <  '   I     q     �     �     �     �     �     �  &   �     �  �    "   �          *     <     M  +   \     �  -   �  Z   �  E   #  ?   i  	   �     �     �  A   �  '        G  1   S     �  0   �     �     �  D   �     ,	                                                                                      
         	                               Add a new script. Add... Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such interpreter "%1" Remove Remove selected script. Run Stop Stop execution of the selected script. Text: Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2009-09-06 15:21+0300
Last-Translator: Darafei Praliaskouski <komzpa@gmail.com>
Language-Team: Belarusian <i18n@mova.org>
Language: be
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Дадаць новы сцэнар Дадаць... Каментар: komzpa@gmail.com Змяніць Змяніць выбраны сцэнар. Змяніць... Выканаць выбраны сцэнар. Немагчыма стварыць сцэнар для інтэрпрытатара '%1'. Немагчыма загрузіць інтэрпрытатар '%1' Немагчыма адкрыць файл сцэнару "%1". Файл: Значка: Інтэрпрытатар: Узровень бяспекі інтэрпрытатара Ruby Дарафей Праляскоўскі Назва: Не існуе інтэрпрытатара "%1" Выдаліць Выдаліць вызначаны сцэнар Выканаць Спыніць Спыніць выкананне выбранага сцэнару. Тэкст: 