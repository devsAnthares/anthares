��          D      l       �      �   
   	       *      �  K  �   C     7     T  Y   r                          <h1>Multiple Desktops</h1>In this module, you can configure how many virtual desktops you want and how these should be labeled. Desktop %1 Desktop %1: Here you can enter the name for desktop %1 Project-Id-Version: kcm_kwindesktop
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-06 05:35+0100
PO-Revision-Date: 2008-02-29 12:53+0200
Last-Translator: Darafei Praliaskouski <komzpa@gmail.com>
Language-Team: Belarusian <i18n@mova.org>
Language: be
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KAider 0.1
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 <h1>Некалькі працоўных сталоў</h1>У гэтым модулі вы можаце наставіць колькі віртуальных сталоў вы хочаце мець і як вы хочаце іх назваць. Працоўны стол %1 Працоўны стол %1: Тут вы можаце вызначыць назву працоўнага стала %1 