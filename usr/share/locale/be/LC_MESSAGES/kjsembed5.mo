��          �   %   �      0     1     F  9   [     �      �     �     �               8     V     o     �     �     �     �     �               5     J  (   `     �  �  �  1   �  $   �  o   �  B   V  :   �  /   �  3     3   8  3   l  @   �  3   �  1   	  3   G	  $   {	  ;   �	  .   �	  2   
  -   >
  .   l
     �
  .   �
  -   �
  0        	                 
                                                                                                         Action takes 2 args. Call to '%1' failed. Call to method '%1' failed, unable to get argument %2: %3 Could not construct value Could not create temporary file. Could not open file '%1' Could not open file '%1': %2 Could not read file '%1' Failed to create Action. Failed to create ActionGroup. Failed to create Layout. Failed to create Widget. Failed to load file '%1' File %1 not found. Incorrect number of arguments. Must supply a filename. Must supply a widget name. No classname specified No classname specified. No such method '%1'. Not enough arguments. There was an error reading the file '%1' Wrong object type. Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2009-09-06 15:21+0300
Last-Translator: Darafei Praliaskouski <komzpa@gmail.com>
Language-Team: Belarusian <i18n@mova.org>
Language: be
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Дзеянне прымае 2 аргументы. Выклік да '%1' схібіў. Выклік да метаду '%1' няўдалы, немагчыма атрымаць аргумент %2: %3 Немагчыма сканструіраваць значэнне Немагчыма стварыць часовы файл. Немагчыма адкрыць файл '%1' Немагчыма адкрыць файл '%1': %2 Немагчыма прачытаць файл '%1' Немагчыма стварыць дзеянне. Немагчыма стварыць групу дзеянняў. Немагчыма стварыць расклад. Немагчыма стварыць віджэт. Немагчыма загрузіць файл '%1' Файл %1 не знойдзены. Нечаканая колькасць аргументаў. Трэба задаць назву файлу. Трэба задаць назву віджэту. Не зададзена назва класу Не зададзена назва класу. Метаду '%1' няма. Недастаткова аргументаў. Памылка чытання файлу '%1' Недапушчальны тып аб'екту. 