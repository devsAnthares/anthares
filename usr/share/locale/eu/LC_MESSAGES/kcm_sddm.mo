��    +      t  ;   �      �  (   �     �  �   �     z     �     �     �     �     �     �     �     �     �     �                @     H     V     d     v     �     �     �     �     �     �          	          .     A     N     ^     s      |  7   �     �     �     �            �       �     �  !   �  
   �     �     	     	     '	  	   6	     @	     _	     r	  
   �	     �	     �	     �	     �	     �	     �	     �	     
     !
     7
  #   Q
     u
     �
     �
     �
     �
     �
     �
  
   �
               .  !   5  B   W     �     �     �     �     �                       %   *   +      !   "                                                            
                (                             )      &      #      '                             $   	    %1 is the name of a session%1 (Wayland) ... @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) Advanced Author Auto &Login Background: Clear Image Commands Could not decompress archive Cursor Theme: Customize theme Default Description Download New SDDM Themes EMAIL OF TRANSLATORSYour emails General Get New Theme Halt Command: Install From File Install a theme. Invalid theme package Load from file... Login screen using the SDDM Maximum UID: Minimum UID: NAME OF TRANSLATORSYour names Name No preview available Reboot Command: Relogin after quit Remove Theme SDDM KDE Config SDDM theme installer Session: The default cursor theme in SDDM The theme to install, must be an existing archive file. Theme Unable to install theme Uninstall a theme. User User: Project-Id-Version: kcm_sddm
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2017-08-17 09:16+0100
Last-Translator: Ander Elortondo <ander.elor@gmail.com>
Language-Team: Basque (https://www.transifex.com/librezale/teams/76827/eu/)
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 %1 (Wayland) ... (Erabilgarri dauden neurriak: %1) Aurreratua Egilea Auto &Saio-hasiera Atzeko planoa: Ezabatu irudia Komandoak Ezin da artxiboa deskonprimitu Kurtsorearen gaia: Gaia pertsonalizatu Lehenetsia Deskribapena Deskargatu SDDM gai berriak ander.elor@gmail.com Orokorra Gai berria lortu Gelditu komandoa: Instalatu fitxategitik Instalatu gaia Gai pakete baloigabea Kargatzen fitxategitik... Saio hasiera pantaila SDDM erabiliz Gehienezko UID: Gutxieneko UID: Ander Elortondo Izena Ez dago aurrebistarik Berrabiarazi komandoa: Berriz saioa-hasi irten ondoren Kendu gaia SDDM KDE ezarpenak SDDM gai instalatzailea Saioa: Kurtsorearen gai lehenetsia SDDMn Instalatzeko gaia, existitzen den fitxategi artxiboa izan behar da Gaia Ezin da instalatu gaia Desinstalatu gaia Erabiltzailea Erabiltzailea: 