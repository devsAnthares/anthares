��          �      �       H     I     i  #   �      �     �     �     �  �        �  ]   �       p   *  :   �  �  �  ,   �     �  '   �               0     =  �   W       r   3      �  w   �  :   ?                     
          	                                  Configure Look and Feel details Cursor Settings Changed Download New Look And Feel Packages EMAIL OF TRANSLATORSYour emails Get New Looks... Marco Martin NAME OF TRANSLATORSYour names Select an overall theme for your workspace (including plasma theme, color scheme, mouse cursor, window and desktop switcher, splash screen, lock screen etc.) Show Preview This module lets you configure the look of the whole workspace with some ready to go presets. Use Desktop Layout from theme Warning: your Plasma Desktop layout will be lost and reset to the default layout provided by the selected theme. You have to restart KDE for cursor changes to take effect. Project-Id-Version: kcm_lookandfeel
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-27 05:46+0100
PO-Revision-Date: 2017-07-19 22:14+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 Konfiguratu itxura eta izaeraren xehetasunak Kurtsore-ezarpenak aldatuta Jaitsi itxura eta izaera pakete berriak Xalba@euskalnet.net Eskuratu itxura berriak... Marco Martin Iñigo Salvador Azurmendi Hautatu zure laneko eremuarentzako gai orokor bat (plasma gaia, kolore eskema, sagu kurtsore, leiho eta mahaigain aldatzaile, plast pantaila, giltzatze pantaila eta abar barne hartuko dituena) Erakutsi aurreikuspegi bat Modulu honek laneko eremu osoaren itxura konfiguratzen uzten dizu erabiltzeko prest dauden aurrezarpen batzurekin. Erabili gaiko mahaigain-diseinua Abisua: zure Plasma mahaigainaren diseinua galdu egingo da eta hautatutako gaiaren diseinu lehenetsira berrezarriko da. KDE berrabiatu behar duzu kurtsoreen aldaketak gauzatzeko. 