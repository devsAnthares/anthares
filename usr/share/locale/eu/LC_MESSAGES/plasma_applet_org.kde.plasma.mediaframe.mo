��          �   %   �      p     q     ~     �     �     �     �     �     �  )   �               !     4     I     ]     m  3   u     �     �  <   �  5     8   T  8   �     �     �     �     �  �  �     �     �     �     �          '     ;     U  ?   ^     �     �     �     �     �            /        M  $   e  >   �  8   �  6   	  6   9	     p	     x	     �	     �	                        
                                                	                                                   Add files... Add folder... Background frame Change picture every Choose a folder Choose files Configure plasmoid... General Left click image opens in external viewer Pad Paths Pause on mouseover Preserve aspect crop Preserve aspect fit Randomize items Stretch The image is duplicated horizontally and vertically The image is not transformed The image is scaled to fit The image is scaled uniformly to fill, cropping if necessary The image is scaled uniformly to fit without cropping The image is stretched horizontally and tiled vertically The image is stretched vertically and tiled horizontally Tile Tile horizontally Tile vertically s Project-Id-Version: plasma_applet_org.kde.plasma.mediaframe
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-16 03:31+0100
PO-Revision-Date: 2017-08-27 09:59+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 Gehitu fitxategiak Gehitu karpeta Atzeko planoko markoa Aldatu irudia hainbestero Hautatu karpeta bat Hautatu fitxategiak Konfiguratu plasmoidea... Orokorra Irudian ezker-klik egiteak kanpoko erakusle batean irekitzen du Jatorrizkoa Bide-izenak Eten sagua gainetik igarotzean Mantendu itxura moztua Mantendu itxura egokitua Elementuak ausaz Luzatu Irudia horizontalki eta bertikalki bikoizten da Irudia ez da eraldatzen Irudia eskalatzen da ondo egokitzeko Irudia uniformeki eskalatzen da egokitzeko, moztu beharra dago Irudia uniformeki eskalatzen da moztu gabe egokitu dadin Irudia horizontalki luzatzen da eta bertikalki lauzatu Irudia bertikalki luzatzen da eta horizontalki lauzatu Lauzatu Lauzatu horizontala Lauzatu bertikalki s 