��    8      �  O   �      �     �     �     �     �     �       +     *   I  +   t  #   �     �  J   �     #     ,     9     @  	   L  H   V     �      �     �  �   �  �   �  M   I     �     �  �   �    >	     K
     b
     o
     �
     �
     �
  1   �
  !   �
  .     0   A      r  :   �  #   �  
   �     �       N     H   b  =   �     �     �             5   1  D   g  D   �  .   �  �        �     �     �     �     �       0   -  0   ^  2   �  #   �     �  [   �     T     ]     q     x  
   �  _   �     �  P        c  �   w  �   I  U        \     b  �   {    5     C     `     u  T   �     �  	   �  2   �  %   /  2   U  1   �  $   �  <   �  $        A     P     a  R   g  R   �  8        F     f     �     �  9   �  O   �  \   @  1   �                  *                 4          )       !                                    -      .      
       0   1   '                         2       /   ,      6   8         &          %   3              $             	   (   "   5                         +      7              #     min  msec &Bell &Keyboard Filters &Lock sticky keys &Use slow keys &Use system bell whenever a key is accepted &Use system bell whenever a key is pressed &Use system bell whenever a key is rejected (c) 2000, Matthias Hoelzer-Kluepfel Activation Gestures All screen colors will be inverted for the amount of time specified below. AltGraph Audible Bell Author Bounce Keys Browse... Click here to choose the color used for the "flash screen" visible bell. Configure &Notifications... EMAIL OF TRANSLATORSYour emails F&lash screen Here you can activate keyboard gestures that turn on the following features: 
Mouse Keys: %1
Sticky keys: Press Shift key 5 consecutive times
Slow keys: Hold down Shift for 8 seconds Here you can activate keyboard gestures that turn on the following features: 
Sticky keys: Press Shift key 5 consecutive times
Slow keys: Hold down Shift for 8 seconds Here you can customize the duration of the "visible bell" effect being shown. Hyper I&nvert screen If the option "Use customized bell" is enabled, you can choose a sound file here. Click "Browse..." to choose a sound file using the file dialog. If this option is checked, KDE will show a confirmation dialog whenever a keyboard accessibility feature is turned on or off.
Ensure you know what you are doing if you uncheck it, as the keyboard accessibility settings will then always be applied without confirmation. KDE Accessibility Tool Locking Keys Matthias Hoelzer-Kluepfel NAME OF TRANSLATORSYour names Notification Press %1 Press %1 while CapsLock and ScrollLock are active Press %1 while CapsLock is active Press %1 while NumLock and CapsLock are active Press %1 while NumLock and ScrollLock are active Press %1 while NumLock is active Press %1 while NumLock, CapsLock and ScrollLock are active Press %1 while ScrollLock is active Slo&w Keys Sound &to play: Super The screen will turn to a custom color for the amount of time specified below. Turn sticky keys and slow keys off after a certain period of inactivity. Turn sticky keys off when two keys are pressed simultaneously Us&e customized bell Use &sticky keys Use &system bell Use bou&nce keys Use gestures for activating sticky keys and slow keys Use system bell whenever a locking key gets activated or deactivated Use system bell whenever a modifier gets latched, locked or unlocked Use the system bell whenever a key is rejected Project-Id-Version: kcmaccess
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-10 03:17+0100
PO-Revision-Date: 2017-08-18 19:25+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=(n != 1);
  min  ms &Kanpaia &Teklatu-iragazkiak Blokeatu tek&la itsaskorrak &Erabili tekla motelak &Erabili sistemaren kanpaia, tekla bat onartzean &Erabili sistemaren kanpaia, tekla bat sakatzean &Erabili sistemaren kanpaia, tekla bat baztertzean (c) 2000, Matthias Hoelzer-Kluepfel Aktibazio-keinuak Pantailako kolore guztiak alderantzikatu egingo dira behean adierazten den denbora-tartean. AltGraph Kanpai entzungarria Egilea Errebote-teklak Arakatu... Egin klik hemen, ageriko kanpaiaren pantaila argiztatzeko erabili nahi den kolorea aukeratzeko. &Jakinarazpenak konfiguratu... koldo.np@euskalnet.net,igaztanaga@gmail.com,marcos@euskalgnu.org,hizpol@ej-gv.es Argi&ztatu pantaila Hemen, eginbide hauek gaituko dituzten teklatuaren keinuak aktiba ditzakezu: 
Saguaren botoiak: %1
Tekla itsaskorra: sakatu MAIUS tekla 5 aldiz jarraian
Tekla motelak: mantendu MAIUS tekla sakatuta 8 segundoan Hemen, eginbide hauek gaituko dituzten teklatuaren keinuak aktiba ditzakezu: 
Tekla itsaskorra: sakatu MAIUS tekla 5 aldiz jarraian
Tekla motelak: mantendu MAIUS tekla sakatuta 8 segundoan Hemen, erakutsiko den "ageriko kanpaia"ren efektuaren iraupena pertsonaliza dezakezu. Hiper &Alderantzikatu pantaila "Erabili kanpai pertsonalizatua" aukera gaiturik badago, soinu-fitxategi bat aukeratu dezakezu hemen. Sakatu "Arakatu...", elkarrizketa-koadroa erabiliz soinu-fitxategi bat aukeratzeko. Aukera hau hautatuz gero, KDEk berrespen-elkarrizketa bat erakutsiko du, teklatuaren irisgarritasun-eginbideak aktibatu edo desaktibatutakoan.
Kontuz ibili aukerekin; hautamarka kentzen baduzu, teklatuaren irisgarritasun-ezarpenak berrespenik gabe aplikatuko dira beti. KDEren irisgarritasun-tresna Tekla blokeatzaileak Matthias Hoelzer-Kluepfel Koldo Navarro,Ion Gaztañaga,Marcos Goyeneche,Hizkuntza Politikarako Sailburuordetza Jakinarazpena Sakatu %1 Sakatu %1, BlokMaius eta BlokKorr aktibo daudenean Sakatu %1, BlokMaius aktibo dagoenean Sakatu %1, BlokZenb eta BlokMaius aktibo daudenean Sakatu %1, BlokZenb eta BlokKorr aktibo daudenean Sakatu %1, BlokZenb aktibo dagoenean Sakatu %1, BlokZenb, BlokMaius eta BlokKorr aktibo daudenean Sakatu %1, BlokKorr aktibo dagoenean Tekla &motelak &Jotzeko soinua: Super Pantailak kolore pertsonalizatua erabiliko du behean adierazitako denbora-tartean. Desaktibatu tekla itsaskorrak eta motelak, jarduerarik gabeko tarte baten ondoren. Itzali tekla itsaskorrak, bi tekla aldi berean sakatzean Erabili &kanpai pertsonalizatua Erabili tekla it&saskorrak Erabili &sistemaren kanpaia Erabili e&rrebote-teklak Erabili keinuak tekla itsaskorrak eta motelak aktibatzeko Erabili sistemaren kanpaia, tekla blokeatzaileak aktibatzean edo desaktibatzean Erabili sistemaren kanpaia, tekla aldatzaile bat atxikitzean, blokeatzean edo desblokeatzean Erabili sistemaren kanpaia, tekla bat baztertzean 