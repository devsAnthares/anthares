��    O      �  k         �     �     �     �  5   �  #     E   A  E   �  >   �       7   #     [     l     �     �     �     �     �     �     �     
	     "	     0	     E	     W	  !   o	  F   �	     �	     �	     
  <   
     U
     ]
  *   f
     �
     �
     �
  	   �
     �
     �
     �
      �
     �
  
        #     A  
   I  F   T  :   �     �     �     �     �     �  8        G  	   Y  
   c     n     ~     �     �     �     �     �     �     �          $     *     6  
   R     ]     m     �     �     �     �  $   �  �  �     �     �     �  8   �  %      J   F  J   �  C   �        F   8       !   �  /   �     �     �                    *     =     [     l     �     �  (   �  7   �          :     H  B   Y     �     �  2   �     �     �  	   �                  
   )  -   4  $   b     �  $   �  
   �     �  G   �  A        ^     j     r     �     �  H   �        	     
         +  	   ;     E     W     q  
   �  &   �     �  
   �  #   �     �               0     @     X     g     t     �     �  "   �     A             O   9   8          4                 2              :          5   ,   =   E      "   B   *   <   '                   @   H                 J   3                            $   0                  F   >   
              .              ;   6   D   M   +   )       #   I      !      -   C         L           G      %      (       	      N       1   &       ?   7      /           K    
Also available in %1 %1 (%2) <b>%1</b> by %2 <em>%1 out of %2 people found this review useful</em> <em>Tell us about this review!</em> <em>Useful? <a href='true'><b>Yes</b></a>/<a href='false'>No</a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'><b>No</b></a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'>No</a></em> @infoFetching updates @infoIt is unknown when the last check for updates was @infoNo updates @infoNo updates are available @infoShould check for updates @infoThe system is up to date @infoUpdates @infoUpdating... Accept Addons Aleix Pol Gonzalez An application explorer Apply Changes Available backends:
 Available modes:
 Checking for updates... Compact Mode (auto/compact/full). Could not close the application, there are tasks that need to be done. Could not find category '%1' Couldn't open %1 Delete the origin Directly open the specified application by its package name. Discard Discover Display a list of entries with a category. Extensions... Help... Install Installed Jonathan Thomas Launch License: List all the available backends. List all the available modes. Loading... Local package file to install More... No Updates Open Discover in a said mode. Modes correspond to the toolbar buttons. Open with a program that can deal with the given mimetype. Rating: Remove Resources for '%1' Review Reviewing '%1' Running as <em>root</em> is discouraged and unnecessary. Search in '%1'... Search... Search: %1 Search: %1 + %2 Settings Short summary... Show reviews (%1)... Sorry, nothing found... Source: Specify the new source for %1 Still looking... Summary: Supports appstream: url scheme Tasks Tasks (%1%) Unable to find resource: %1 Update All Update Selected Update section nameUpdate (%1) Updates unknown reviewer updates not selected updates selected © 2010-2016 Plasma Development Team Project-Id-Version: plasma-discover
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:02+0100
PO-Revision-Date: 2017-08-07 01:01+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 
Erabilgarri dago ere hemen: %1 %1 (%2) <b>%1</b> egilea %2 <em>%2-(e)tik %1-ri izan zaie baliagarri iritzi hau</em> <em>Zer diozu iritzi honi buruz?</em> <em>Baliagarria? <a href='true'><b>Bai</b></a>/<a href='false'>Ez</a></em> <em>Baliagarria? <a href='true'>Bai</a>/<a href='false'><b>Ez</b></a></em> <em>Baliagarria? <a href='true'>Bai</a>/<a href='false'>Ez</a></em> Eguneratzeak eskuratzen Ez da ezagutzen eguneratzeak bilatzeko azken egiaztatzea noiz egin zen Eguneratzerik ez Ez dago eguneratze erabilgarririk Eguneratzeak dauden egiaztatu beharko litzateke Sistema eguneratuta dago Eguneratzeak Eguneratzen... Onartu Gehigarriak Aleix Pol Gonzalez Aplikazioen esploratzaile bat Ezarri aldaketak Bizkarraldeko erabilgarriak:
 Modu erabilgarriak:
 Eguneratzeak egiaztatzen... Modu trinkoa (automatikoa/trinkoa/osoa). Ezin da aplikazioa itxi, burutu beharreko atazak daude. Ez da aurkitu kategoria: '%1' Ezin ireki %1 Ezabatu jatorria Zuzenean ireki adierazitako aplikazioa bere pakete-izenaren bidez. Baztertu Discover Bistaratu kategoria bat duen sarrera-zerrenda bat. Hedapenak... Laguntza... Instalatu Instalatuta Jonathan Thomas Abiarazi Lizentzia: Zerrendatu bizkarraldeko erabilgarri guztiak. Zerrendatu modu erabilgarri guztiak. Zamatzen... Instalatu beharreko fitxategi lokala Gehiago... Eguneratzerik ez Ireki Discover esandako moduan. Moduak tresna-barrako botoiei dagozkie. Ireki emandako mime-motarekin lan egin dezakeen programa batekin. Balorazioa: Ezabatu Honetarako baliabideak: '%1' Iritzia Honi buruz iritzia ematen: '%1' <em>root</em> gisa exekutatzea ez da beharrezkoa ezta gomendagarria ere. Bilatu hemen: '%1'... Bilatu... Bilatu: %1 Bilatu: %1 + %2 Ezarpenak Laburpen motza... Erakutsi iritziak (%1)... Ez da ezer aurkitu... Iturburua: Zehaztu honentzako iturburu berria: %1 Oraindik bilatzen... Laburpena: 'appstream' onartzen du: url eskema Atazak Atazak (%%1) Ezin da aurkitu baliabidea: %1 Eguneratu denak Eguneratu hautatutakoak Eguneratu (%1) Eguneratzeak Iritzi emaile ezezaguna eguneratzeak ez dira hautatu eguneratzeak hautatuta © 2010-2016 Plasma garapen taldea 