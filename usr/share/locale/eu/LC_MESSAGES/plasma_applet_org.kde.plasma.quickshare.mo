��          �      <      �     �     �     �  +   �  @        L     a     i     w     }     �  
   �     �     �     �     �     �  �  
     �     �     �  :     K   F     �     �     �     �     �     �     �  	   �     �          "     =     
         	                                                                                       <a href='%1'>%1</a> Close Copy Automatically: Don't show this dialog, copy automatically. Drop text or an image onto me to upload it to an online service. Error during upload. General History Size: Paste Please wait Please, try again. Sending... Share Shares for '%1' Successfully uploaded The URL was just shared Upload %1 to an online service Project-Id-Version: plasma_applet_org.kde.plasma.quickshare
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-01 04:04+0100
PO-Revision-Date: 2017-11-06 01:05+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 <a href='%1'>%1</a> Itxi Kopiatu automatikoki: Ez erakutsi elkarrizketa-koadro hau, kopiatu automatikoki. Jaregin testua edo irudi bat nire gainean lerroko zerbitzu batera igotzeko. Errorea igo bitartean. Orokorra Historiaren neurria: Itsatsi Itxaron Saiatu berriz. Bidaltzen... Partekatu Honen partekatzeak: '%1' Ondo igo da URLa oraintxe partekatu da Igo %1 lerroko zerbitzu batera 