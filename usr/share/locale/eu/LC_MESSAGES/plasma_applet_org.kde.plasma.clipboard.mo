��          �      L      �     �     �     �     �          )     1     9     P     h     t  K   �     �     �     �     �            �        �               #     C     V     _     h      ~     �     �     �     �     �     �     �     �                                                        
   	                                               Change the barcode type Clear history Clipboard Contents Clipboard history is empty. Clipboard is empty Code 39 Code 93 Configure Clipboard... Creating barcode failed Data Matrix Edit contents Indicator that there are more urls in the clipboard than previews shown+%1 Invoke action QR Code Remove from history Return to Clipboard Search Show barcode Project-Id-Version: plasma_applet_org.kde.clipboard
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-10 03:07+0200
PO-Revision-Date: 2017-11-03 07:59+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 Aldatu barra-kode mota Garbitu historia Arbelaren edukia Arbelaren historia hutsik dago. Arbela hutsik dago 39 kodea 93 kodea Konfiguratu arbela... Barra-kode sorrerak huts egin du Datu matrizea Editatu edukia +%1 Deitu ekintza QR kodea Kendu historiatik Itzuli arbelera Bilatu Erakutsi barra-kodea 