��          �   %   �      `     a     j          �     �     �  0   �     �     �                    &     ;  
   R     ]     r          �     �     �     �     �     �     �       �       �  #   �          #  
   4     ?  F   S     �     �     �     �     �     �     	     (     5  	   N  $   X     }     �  
   �     �  !   �     �     �  	                                                      
                                                                  	       %1 by %2 Add Custom Wallpaper Add Folder... Add Image... Centered Change every: Directory with the wallpaper to show slides from Download Wallpapers Get New Wallpapers... Hours Image Files Minutes Next Wallpaper Image Open Containing Folder Open Image Open Wallpaper Image Positioning: Recommended wallpaper file Remove wallpaper Restore wallpaper Scaled Scaled and Cropped Scaled, Keep Proportions Seconds Select Background Color Tiled Project-Id-Version: plasma_applet_org.kde.image
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:39+0100
PO-Revision-Date: 2017-11-12 01:01+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=(n != 1);
 %1 %2-(e)k egina Gehitu horma-papera pertsonalizatua Gehitu karpeta... Gehitu irudia... Erdiratuta Aldatu hainbestero: Diapositiba gisa erakutsi behar diren horma-paperak dituen direktorioa Jaitsi horma-paperak Eskuratu horma-paper berriak... Ordu Irudi-fitxategiak Minutu Hurrengo horma-paper irudia Ireki hau barnean duen karpeta Ireki irudia Ireki horma-paper irudia Kokatzea: Gomendatutako horma-paper fitxategia Kendu horma-papera Lehengoratu horma-papera Eskalatuta Eskalatuta eta moztuta Eskalatuta, mantendu proportzioak Segundo Hautatu atzeko planoko kolorea Lauzatuta 