��    	      d      �       �   
   �      �      �   	              "     C  "   ]  �  �     S     Z     c     w     �  .   �  +   �  1   �                             	                 Appearance Colors: Display seconds Draw grid Show inactive LEDs: Use custom color for active LEDs Use custom color for grid Use custom color for inactive LEDs Project-Id-Version: plasma_applet_org.kde.plasma_binaryclock
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-31 05:47+0100
PO-Revision-Date: 2017-11-04 12:26+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 Itxura Koloreak Bistarazi segundoak Marraztu sareta Erakutsi LED ez aktiboak: Erabili kolore pertsonalizatuak LED aktiboekin Erabili kolore pertsonalizatuak saretarekin Erabili kolore pertsonalizatuak LED ez aktiboekin 