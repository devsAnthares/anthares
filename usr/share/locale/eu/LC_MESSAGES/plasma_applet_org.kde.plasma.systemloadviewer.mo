��    &      L  5   |      P     Q     l     y     �     �     �     �     �     �     �     �     �  &   �               #     ,     3     ?     M     U     ]     d     s     �     �     �     �     �     �     �     �     �     �  
   �            �       �     �  !         "  
   (     3     7     >     L     `     i     {  '   �     �     �  
   �     �     �     �     �                    -     @     O     X  	   n     x     �     �     �     �     �     �     �     	                           	      #      $                                                    &                                          !          %                            "   
    Abbreviation for secondss Application: Average clock: %1 MHz Bar Buffers: CPU CPU %1 CPU monitor CPU%1: %2% @ %3 Mhz CPU: %1% CPUs separately Cache Cache Dirty, Writeback: %1 MiB, %2 MiB Cache monitor Cached: Circular Colors Compact Bar Dirty memory: General IOWait: Memory Memory monitor Memory: %1/%2 MiB Monitor type: Nice: Set colors manually Show: Swap Swap monitor Swap: %1/%2 MiB Sys: System load Update interval: Used swap: User: Writeback memory: Project-Id-Version: plasma_applet_org.kde.plasma.systemloadviewer
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-29 03:31+0200
PO-Revision-Date: 2017-11-07 01:47+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=(n != 1);
 s Aplikazioa: Erlojuaren batez-bestekoa: %1 MHz Barra Bufferrak: PUZ PUZ %1 PUZ begiralea CPU%1: %%2 @ %3 Mhz CPU: %%1 PUZak banan-banan Cachea Cache zikina, Writeback: %1 MiB, %2 MiB Cache begiralea Cacheratua: Zirkularra Koloreak Barra trinkoa Memoria zikina: Orokorra S/I Itxaronaldia: Memoria Memoria begiralea Memoria: %1/%2 MiB Begirale mota: Adeitsu: Ezarri koloreak eskuz Erakutsi: Trukaketa memoria Trukaketa begiralea Trukaketa: %1/%2 MiB Sistema: Sistemaren zama Eguneratze-tartea: Erabilitako trukaketa: Erabiltzailea: Writeback memoria: 