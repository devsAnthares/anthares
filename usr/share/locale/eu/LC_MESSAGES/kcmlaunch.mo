��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  �  K     �  "     �  %    �  g   D
     �
     �
     �
  #   �
          .  "   G     j                                          
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2014-02-11 08:16+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=(n != 1);
  s &Abiarazte-abisuaren denbora-muga: <h1>Ataza-barraren jakinarazpena</h1> Abiarazte-jakinarazpenen beste modu bat ere gaitu dezakezu:
ataza-barran, harea-erloju bat biraka duen botoi bat agertzen da,
eta hark adierazten du abiarazi duzun aplikazioa kargatzen ari dela.
Baliteke aplikazio batzuek abiarazte-jakinarazpen hori
ez jasotzea. Hala bada, botoia desagertu egingo da 'Abiarazte-abisuaren denbora-muga' atalean adierazitako denbora igarotakoan <h1>Kurtsore lanpetua</h1>
KDEk kurtsore lanpetua eskaintzen du, aplikazioa abiarazi dela jakinarazteko.
Kurtsore lanpetua gaitzeko, hautatu konbinazio-koadroko
ikusizko ohar bat.
Baliteke aplikazio batzuek abiarazteari buruzko jakinarazpen
hori ez jasotzea. Hala bada, kurtsoreak keinu egiteari utziko dio
'Abiarazte-abisuaren denbora-muga' atalean adierazitako denbora igarotakoan. <h1>Abiaraztearen jakinarazpena</h1> Hemen, aplikazioen abiaraztearen jakinarazpena konfigura dezakezu. Kurtsore keinukaria Errebote egiten duen kurtsorea K&urtsore okupatua Gaitu &ataza-barraren jakinarazpena K&urtsore lanpeturik ez Kurtsore lanpetu pasiboa A&biarazte-abisuaren denbora-muga: Ataza-barraren &jakinarazpena 