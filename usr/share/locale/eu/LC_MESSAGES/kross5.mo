��    $      <  5   \      0     1  +   E     q  4   �  &   �     �     �                     5     :     P     X  ,   u  3   �     �     �               !  '   .     V     u     {     �     �     �     �     �     �  &   �       B        b  �  y     *     1     P     `     }     �  	   �  	   �  	   �  =   �     �      	  
   	     )	  4   I	  @   ~	  +   �	  .   �	     
     &
     .
  *   A
  /   l
     �
     �
     �
     �
     �
  	   �
     �
       *   &     Q  =   Y     �                                       
                              !                                  $      #                               	                           "             @info:creditAuthor @info:creditCopyright 2006 Sebastian Sauer @info:creditSebastian Sauer @info:shell command-line argumentThe script to run. @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: application descriptionCommand-line utility to run Kross scripts. application nameKross Project-Id-Version: kross5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2017-08-30 23:29+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=(n != 1);
 Egilea Copyright 2006 Sebastian Sauer Sebastian Sauer Exekutatu beharreko scripta. Orokorra Gehitu script berria. Gehitu... Ezeztatu? Iruzkina: marcos@euskalgnu.org,igaztanaga@gmail.com,xalba@euskalnet.net Editatu Editatu hautatutako script-a. Editatu... Exekutatu hautatutako script-a. Ezin da sortu script-a "%1" interpretatzailearendako Ezin aurkitu "%1" script fitxategia irekitzeko interpretatzailea Ezin izan da kargatu "%1" interpretatzailea "%1" script fitxategia irekitzeak huts egin du Fitxategia: Ikonoa: interpretatzailea: Ruby interpretatzailearen seguritate maila Marcos,Ion Gaztañaga,Iñigo Salvador Azurmendi Izena: "%1" funtziorik ez "%1" interpretatzailerik ez Kendu Kendu hautatutako script-a. Exekutatu "%1" script fitxategia ez dago. Gelditu Gelditu hautatutako script-aren exekuzioa. Testua: Kross scriptak exekutatzeko komando-lerroko baliagarritasuna. Kross 