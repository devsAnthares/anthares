��    :      �  O   �      �     �                    ,     3  �  N  �   )  -   �  )   �  -   	  +   ;	  r   g	  	   �	  	   �	     �	     
     
     
  
   $
     /
     ;
     C
     K
      b
     �
     �
     �
      �
     �
     �
  r   �
  5   ]     �     �     �  	   �     �     �     �  (   �                9     S     n     t  +   �  3   �     �     �     �     �  S     )   _     �  �   �  �  s  	   &  	   0     :     M     [     a  �  |  �   0  ,   �     �  
   �  
   �  S        \     h     v  	   �     �  
   �     �     �  
   �     �  !   �  b        k     �     �  !   �     �      �  �   �  7   p     �  q   �     /     5     B  
   I  
   T     _       %   �  (   �  '   �            5     ,   U     �     �     �     �  K   �  (   �     $  �   6               2                '   (          1      !            8                                       .               5   	   6   $      )       7   4          *                 0   
             :         ,                    +      %      "   -   /   #       9           &      3    &Amount: &Effect: &Second color: &Semi-transparent &Theme (c) 2000-2003 Geert Jansen <h1>Icons</h1>This module allows you to choose the icons for your desktop.<p>To choose an icon theme, click on its name and apply your choice by pressing the "Apply" button below. If you do not want to apply your choice you can press the "Reset" button to discard your changes.</p><p>By pressing the "Install Theme File..." button you can install your new icon theme by writing its location in the box or browsing to the location. Press the "OK" button to finish the installation.</p><p>The "Remove Theme" button will only be activated if you select a theme that you installed using this module. You are not able to remove globally installed themes here.</p><p>You can also specify effects that should be applied to the icons.</p> <qt>Are you sure you want to remove the <strong>%1</strong> icon theme?<br /><br />This will delete the files installed by this theme.</qt> <qt>Installing <strong>%1</strong> theme</qt> @label The icon rendered as activeActive @label The icon rendered as disabledDisabled @label The icon rendered by defaultDefault A problem occurred during the installation process; however, most of the themes in the archive have been installed Ad&vanced All Icons Antonio Larrosa Jimenez Co&lor: Colorize Confirmation Desaturate Description Desktop Dialogs Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Effect Parameters Gamma Geert Jansen Get new themes from the Internet Icons Icons Control Panel Module If you already have a theme archive locally, this button will unpack it and make it available for KDE applications Install a theme archive file you already have locally Main Toolbar NAME OF TRANSLATORSYour names Name No Effect Panel Preview Remove Theme Remove the selected theme from your disk Set Effect... Setup Active Icon Effect Setup Default Icon Effect Setup Disabled Icon Effect Size: Small Icons The file is not a valid icon theme archive. This will remove the selected theme from your disk. To Gray To Monochrome Toolbar Torsten Rahn Unable to download the icon theme archive;
please check that address %1 is correct. Unable to find the icon theme archive %1. Use of Icon You need to be connected to the Internet to use this action. A dialog will display a list of themes from the http://www.kde.org website. Clicking the Install button associated with a theme will install this theme locally. Project-Id-Version: kcmicons
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-08 06:27+0100
PO-Revision-Date: 2014-02-25 21:08+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=(n != 1);
 &Kopurua: &Efektua: &Bigarren kolorea: &Erdi-gardena &Gaia (c) 2000-2003 Geert Jansen <h1>Ikonoak</h1>Modulu honek aukera ematen du mahaigainerako ikonoak aukeratzeko.<p>Ikono-gai bat aukeratzeko, egin klik haren izenean, eta ezarri aldaketa "Aplikatu" botoia sakatuz. Aldaketa ezarri nahi ez baduzu, "Berrezarri" botoia sakatu dezakezu aldaketak baztertzeko.</p><p>"Gai-fitxategia instalatu..." botoia sakatuta, ikono-gai berria instala dezakezu, laukian haren kokalekua idatziz edo kokalekura nabigatuz. Sakatu "Ados" botoia, instalazioa bukatzeko.</p><p>Gaia modulu hau erabiliz instalatu baduzu bakarrik aktibatuko da "Kendu gaia" botoia. Ezin dira kendu aukera honekin globalki instalatutako gaiak.</p><p>Halaber, ikonoei aplikatu beharreko efektuak zehaztu ditzakezu.</p> <qt>Ziur zaude <strong>%1</strong> ikono-gaia ezabatu nahi duzula?<br /><br />Gai honetan instalatutako fitxategiak ezabatu egingo dira.</qt> <qt><strong>%1</strong>gaia instalatzea</qt> Aktibo Desgaituta Lehenetsia Arazo bat gertatu da instalatzean; hala ere, artxiboko gai gehienak instalatu dira. Au&rreratua Ikono guztiak Antonio Larrosa Jimenez Ko&lorea: Koloreztatu Berrespena Desasetu Deskribapena Mahaigaina Elkarrizketa-koadroak Arrastatu edo idatzi gaiaren URLa marcos@euskalgnu.org,juanirigoien@gmail.com,asieriko@gmail.com,xalba@euskalnet.net,hizpol@ej-gv.es Efektuaren parametroak Gamma Geert Jansen Eskuratu gai berriak Internetetik Ikonoak Ikonoen kontrol-paneleko modulua Gai-artxibo bat dagoeneko lokalean baldin badaukazu, botoi horren bidez deskonprimatu egingo da, eta erabilgarri egongo da KDE aplikazioetarako Instalatu lokalean daukazun gaien artxibo-fitxategi bat Tresna-barra nagusia Marcos Goyeneche,Juan Irigoien,Asier Urio Larrea,Iñigo Salvador Azurmendi,Hizkuntza Politikarako Sailburuordetza Izena Efekturik ez Panela Aurrebista Kendu gaia Kendu hautatutako gaia diskotik Efektua ezarri... Ikono-efektu aktiboaren konfigurazioa Ikono-efektu lehenetsiaren konfigurazioa Ikono-efektu desgaituaren konfigurazioa Tamaina: Ikono txikiak Fitxategi hau ez da baliozko ikonoen gai-artxibo bat. Hautatutako gaia ezabatu egingo da diskotik. Grisez Monokromoan Tresna-barra Torsten Rahn Ezin da deskargatu ikonoen gai-artxiboa;
egiaztatu %1 helbidea zuzena dela. Ezin da aurkitu %1 ikonoen gai-artxiboa. Ikonoa erabiltzea Ekintza hau erabiltzeko, Internetera konektatuta egon behar duzu. Elkarrizketa-koadro batek http://www.kde.org webguneko gai-zerrenda bat bistaratutako du. Gai bakoitzarekin lotutako Instalatu botoia sakatuta, gaia lokalean instalatuko du. 