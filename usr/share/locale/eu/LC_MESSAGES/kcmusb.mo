��    N      �  k   �      �  "   �     �     �  8   �  9   -  ,   g      �  2   �  4   �  4     8   R  ?   �  0   �  #   �  /    	  0   P	  #   �	  2   �	  /   �	     
     
     #
     4
     :
     H
  
   `
     k
     t
     �
     �
     �
     �
     �
  }   �
     I     N     U      a     �     �     �     �     �     �     �  	   �     �     �     �          '     A     G     U     t     �     �     �     �     �     �     �     �  	   �  	   �  G   �           ,     ;     C     X     `     p     �     �     �     �  �  �  "   �     �     �  <   �  >     +   J  !   v  4   �  2   �  =      8   >  ?   w  6   �  %   �  3     3   H  %   |  3   �  9   �       	        '     :     A  "   P  
   s     ~     �     �     �     �     �     �  �   �     i     p     w  8   �     �     �     �      �     �          '  
   6     A     J  #   W     {     �     �     �  Q   �          "  
   2     =     K     Q     X     `     o     t  	   �  I   �     �     �  	   �     �            !   0  %   R  %   x     �     �         /   6          A   -   >      B                  5             L              )      %       K         4             3   +             G   ;      $   1   I      &          '   D   0           	   #   !       C      *   <          ,           2   H          @      
       .   N                         F          E             =                 J   8      9   7   :   ?   M           "   (    (c) 2001 Matthias Hoelzer-Kluepfel <b>Manufacturer:</b>  <b>Serial #:</b>  <tr><td><i>Attached Devicenodes</i></td><td>%1</td></tr> <tr><td><i>Bandwidth</i></td><td>%1 of %2 (%3%)</td></tr> <tr><td><i>Channels</i></td><td>%1</td></tr> <tr><td><i>Class</i></td>%1</tr> <tr><td><i>Intr. requests</i></td><td>%1</td></tr> <tr><td><i>Isochr. requests</i></td><td>%1</td></tr> <tr><td><i>Max. Packet Size</i></td><td>%1</td></tr> <tr><td><i>Power Consumption</i></td><td>%1 mA</td></tr> <tr><td><i>Power Consumption</i></td><td>self powered</td></tr> <tr><td><i>Product ID</i></td><td>0x%1</td></tr> <tr><td><i>Protocol</i></td>%1</tr> <tr><td><i>Revision</i></td><td>%1.%2</td></tr> <tr><td><i>Speed</i></td><td>%1 Mbit/s</td></tr> <tr><td><i>Subclass</i></td>%1</tr> <tr><td><i>USB Version</i></td><td>%1.%2</td></tr> <tr><td><i>Vendor ID</i></td><td>0x%1</td></tr> AT-commands ATM Networking Abstract (modem) Audio Bidirectional Boot Interface Subclass Bulk (Zip) CAPI 2.0 CAPI Control CDC PUF Communications Control Device Control/Bulk Control/Bulk/Interrupt Could not open one or more USB controller. Make sure, you have read access to all USB controllers that should be listed here. Data Device Direct Line EMAIL OF TRANSLATORSYour emails Ethernet Networking Floppy HDLC Host Based Driver Hub Human Interface Devices I.430 ISDN BRI Interface Keyboard Leo Savernik Live Monitoring of USB Bus Mass Storage Matthias Hoelzer-Kluepfel Mouse Multi-Channel NAME OF TRANSLATORSYour names No Subclass Non Streaming None Printer Q.921 Q.921M Q.921TM Q.932 EuroISDN SCSI Streaming Telephone This module allows you to see the devices attached to your USB bus(es). Transparent Unidirectional Unknown V.120 V.24 rate ISDN V.42bis Vendor Specific Vendor Specific Class Vendor Specific Protocol Vendor Specific Subclass Vendor specific kcmusb Project-Id-Version: kcmusb
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2014-02-06 07:00+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=(n != 1);
 (c) 2001 Matthias Hoelzer-Kluepfel <b>Fabrikatzailea:</b>  <b>Serie-zk.:</b>  <tr><td><i>Erantsitako gailu-nodoak</i></td><td>%1</td></tr> <tr><td><i>Banda-zabalera:</i></td><td>%1/%2 (% %3)</td></tr> <tr><td><i>Kanalak</i></td><td>%1</td></tr> <tr><td><i>Klasea</i></td>%1</tr> <tr><td><i>Eteteko eskaerak</i></td><td>%1</td></tr> <tr><td><i>Eskaera isokr.</i></td><td>%1</td></tr> <tr><td><i>Paketearen neurri maximoa</i></td><td>%1</td></tr> <tr><td><i>Energia-kontsumoa</i></td><td>%1 mA</td></tr> <tr><td><i>Energia-kontsumoa</i></td><td>autoelikatua</td></tr> <tr><td><i>Produktuaren IDa</i></td><td>0x%1</td></tr> <tr><td><i>Protokoloa</i></td>%1</tr> <tr><td><i>Berrikuspena</i></td><td>%1.%2</td></tr> <tr><td><i>Abiadura</i></td><td>%1 Mbit/s</td></tr> <tr><td><i>Azpiklasea</i></td>%1</tr> <tr><td><i>USB-bertsioa</i></td><td>%1.%2</td></tr> <tr><td><i>Hornitzailearen IDa</i></td><td>0x%1</td></tr> AT komandoak ATM sarea Laburpena (modema) Audioa Noranzko bikoa Abiarazte-interfazearen azpiklasea Bulk (Zip) CAPI 2.0 CAPI kontrola CDC PUF Komunikazioak Kontrol-gailua Kontrola/Bulk Kontrola/Bulk/Eten Ezin ireki USB gidari bat edo gehiago. Egiaztatu hemen zerrendatu beharko liratekeen USB kontrolatzaile guztietan irakurtzeko baimena duzula. Datuak Gailua Zuzeneko linea marcos@euskalgnu.org,xalba@euskalnet.net,hizpol@ej-gv.es Ethernet sarea Disketea HDLC Ostalarian oinarritutako gidaria Kontzentragailua Giza interfazeko gailuak I.430 ISDN BRI Interfazea Teklatua Leo Savernik USB busaren zuzeneko monitorizazioa Biltegiratze masiboa Matthias Hoelzer-Kluepfel Sagua Kanal anitzekoa Marcos Goyeneche,Iñigo Salvador Azurmendi,Hizkuntza Politikarako Sailburuordetza Azpiklaserik ez Streaming-ik ez Bat ere ez Inprimagailua Q.921 Q.921M Q.921TM Q.932 EuroISDN SCSI Streaming-a Telefonoa Modulu honek aukera ematen du USB buse(t)an erantsitako gailuak ikusteko. Gardena Noranzko bakarrekoa Ezezaguna ISDN (V.120 V.24) V.42bis Hornitzailearen espezifikoa Hornitzailearen klase espezifikoa Hornitzailearen protokolo espezifikoa Hornitzailearen azpiklase espezifikoa Hornitzailearen espezifikoa kcmusb 