��    >        S   �      H  	   I  	   S     ]     e     s     y          �     �     �     �     �     �     �  
        "  ?   .     n  >   w  	   �     �     �  S   �      A     b  �  z     	     
	     	  
   '	     2	     B	     R	  
   _	     j	  A   y	     �	     �	  
   �	     �	     �	     
     #
     ,
     ;
     G
     X
     h
     |
     �
     �
     �
     �
     �
     �
               (  T   ;     �  S   �  �  �     �  
   �     �     �     �     �     �     �  *     	   2  '   <      d  +   �  $   �     �     �  ?   �  
   ?  <   J  
   �     �     �  ^   �  R   $      w  �  �     "     +     <     L     `     u     �  
   �     �  :   �     �          "     1  _   >     �     �     �     �     �     �     
          +     G  #   ^     �     �  $   �     �     �       Q        p  G   ~            3          1   6       2   4      !   *         #   7                    (   9          &           :   5          +                    "                        '           -          >   =   /   	      )                ;   
      .      $   %       <   ,                    8      0            [Hidden] &Comment: &Delete &Description: &Edit &File &Name: &New Submenu... &Run as a different user &Sort &Sort all by Description &Sort all by Name &Sort selection by Description &Sort selection by Name &Username: &Work path: (C) 2000-2003, Waldo Bastian, Raffaele Sandrini, Matthias Elter Advanced All submenus of '%1' will be removed. Do you want to continue? Co&mmand: Could not write to %1 Current shortcut &key: Do you want to restore the system menu? Warning: This will remove all custom menus. EMAIL OF TRANSLATORSYour emails Enable &launch feedback Following the command, you can have several place holders which will be replaced with the actual values when the actual program is run:
%f - a single file name
%F - a list of files; use for applications that can open several local files at once
%u - a single URL
%U - a list of URLs
%d - the folder of the file to open
%D - a list of folders
%i - the icon
%m - the mini-icon
%c - the caption General General options Hidden entry Item name: KDE Menu Editor KDE menu editor Main Toolbar Maintainer Matthias Elter Menu changes could not be saved because of the following problem: Menu entry to pre-select Montel Laurent Move &Down Move &Up NAME OF TRANSLATORSYour names New &Item... New Item New S&eparator New Submenu Only show in KDE Original Author Previous Maintainer Raffaele Sandrini Restore to System Menu Run in term&inal Save Menu Changes? Show hidden entries Spell Checking Spell checking Options Sub menu to pre-select Submenu name: Terminal &options: Unable to contact khotkeys. Your changes are saved, but they could not be activated. Waldo Bastian You have made changes to the menu.
Do you want to save the changes or discard them? Project-Id-Version: kmenuedit
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2014-02-09 16:26+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=(n != 1);
 [Ezkutatuta] &Iruzkina: E&zabatu &Deskribapena: &Editatu &Fitxategia I&zena: &Azpimenu berria... Beste &erabiltzaile-izen batekin exekutatu &Ordenatu &Ordenatu denak deskribapenaren arabera &Ordenatu denak izenaren arabera &Ordenatu hautapena deskribapenaren arabera &Ordenatu hautapena izenaren arabera Erabiltzaile-&izena: &Laneko bide-izena: (C) 2000-2003, Waldo Bastian, Raffaele Sandrini, Matthias Elter Aurreratua '%1'(r)en azpimenu guztiak kenduko dira. Jarraitu nahi duzu? &Komandoa: Ezin izan da %1(e)(a)n idatzi Uneko &laster-tekla: Sistemaren menua berrezarri nahi duzu? Abisua: Honek menu pertsonalizatu guztiak kenduko ditu. juanirigoien@irakasle.net,marcos@euskalgnu.org,xalba@euskalnet.net,hizpol@ej-gv.es Gaitu a&biaraztearen informazioa Komandoaren ondoren, benetako programa exekutatutakoan benetako balioekin ordeztuko diren hainbat aldagai egon daitezke:
%f: fitxategi-izen bat
%F: fitxategi-zerrenda bat; erabili hainbat fitxategi lokal aldi berean ireki ditzaketen aplikazioetarako
%u: URL bat
%U: URLen zerrenda bat
%d: ireki beharreko fitxategiaren karpeta
%D: fitxategi-zerrenda bat
%i: ikonoa
%m: miniikonoa
%c: epigrafea Orokorra Aukera orokorrak Sarrera ezkutua Elementuaren izena: KDEren menu-editorea KDEren menu-editorea Tresna-barra nagusia Arduraduna Matthias Elter Ezin izan dira gorde menuko aldaketak, arazo hau dela eta: Aurrez hautatzeko menu-sarrera Montel Laurent Eraman &behera Eraman &gora Juan Irigoien,Marcos Goyeneche,Iñigo Salvador Azurmendi,Hizkuntza Politikarako Sailburuordetza E&lementu berria... Elementu berria Berei&zle berria Azpimenu berria Erakutsi KDEn soilik Jatorrizko egilea Arduradun ohia Raffaele Sandrini Berrezarri sistemaren menua Exekutatu ter&minalean Menuko aldaketak gorde nahi dituzu? Erakutsi sarrera ezkutuak Ortografia-egiaztatzailea Ortografia-egiaztatzailearen aukerak Aurrez hautatzeko azpimenua Azpimenuaren izena: &Terminal-aukerak: Ezin da Khotkeys-ekin kontaktatu. Aldaketak gorde dira, baina ezin dira aktibatu. Waldo Bastian Aldaketak egin dituzu menuan.
Aldaketak gorde edo baztertu nahi dituzu? 