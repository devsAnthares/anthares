��          �      <      �     �     �     �  7  �  �  #  +   �  ^              �     �  x   �  �   %     �               ;     U  �  r     (	     <	     W	    l	    �
  )   �  r   �     ?     X     t  u   }  �   �     �     �  !   �          +                          
       	                                                                    &End current session &Restart computer &Turn off computer <h1>Session Manager</h1> You can configure the session manager here. This includes options such as whether or not the session exit (logout) should be confirmed, whether the session should be restored again when logging in and whether the computer should be automatically shut down after session exit by default. <ul>
<li><b>Restore previous session:</b> Will save all applications running on exit and restore them when they next start up</li>
<li><b>Restore manually saved session: </b> Allows the session to be saved at any time via "Save Session" in the K-Menu. This means the currently started applications will reappear when they next start up.</li>
<li><b>Start with an empty session:</b> Do not save anything. Will come up with an empty desktop on next start.</li>
</ul> Applications to be e&xcluded from sessions: Check this option if you want the session manager to display a logout confirmation dialog box. Conf&irm logout Default Leave Option General Here you can choose what should happen by default when you log out. This only has meaning, if you logged in through KDM. Here you can enter a colon or comma separated list of applications that should not be saved in sessions, and therefore will not be started when restoring a session. For example 'xterm:konsole' or 'xterm,konsole'. O&ffer shutdown options On Login Restore &manually saved session Restore &previous session Start with an empty &session Project-Id-Version: kcmsmserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2014-02-11 01:15+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=(n != 1);
 &Amaitu uneko saioa &Berrabiarazi ordenagailua &Itzali ordenagailua <h1>Saio kudeatzailea</h1>Saio kudeatzailea hemen konfigura dezakezu. Aukeren tartean hauxek aurkituko dituzu: saiotik irtetea baieztatu behar duzun edo ez, saioa berezarri nahi duzun sartzen zaren hurrengo aldian edo konputagailua automatikoki itzaliko den saiotik ateratzean. <ul>
<li><b>Leheneratu aurreko saioa.</b> Exekutatzen diren aplikazio guztiak gordetzen ditu, eta leheneratu egiten ditu abiarazten diren hurrengo aldian</li>
<li><b>Eskuz leheneratu gordetako saioa. </b> Aukera ematen du edozein unetan saioa K-Menu-ko "Gorde saioa" aukeraren bidez gordetzeko. Horrek esan nahi du unean exekutatzen ari diren aplikazioak berriro agertuko direla abiarazten diren hurrengo aldian.</li>
<li><b>Hasi saio huts batekin.</b> Ez du ezer gordetzen. Mahaigain hutsa izango du abiarazten den hurrengo aldian.</li>
</ul> Saioetatik &baztertuko diren aplikazioak: Hautatu aukera hau, saio-kudeatzaileak saiotik irtetea berresteko elkarrizketa-koadro bat bistaratzea nahi baduzu. &Berretsi saioa amaitzea Irteteko aukera lehenetsiak Orokorra Hemen, saioa amaitutakoan zer gertatuko den konfigura dezakezu. Saioa KDMrekin hasi baduzu bakarrik du zentzua honek. Hemen, saioetan gordeko ez diren eta, beraz, saio bat leheneratzean abiaraziko ez diren aplikazio zerrenda bat (koma edo bi puntuen bidez bereizia) sar dezakezu. Adibidez, 'xterm:xconsole' edo 'xterm,konsole'. &Eskaini itzaltzeko aukerak Saioa hastean &Eskuz leheneratu gordetako saioa Leheneratu &aurreko saioa Hasi saio &huts batekin 