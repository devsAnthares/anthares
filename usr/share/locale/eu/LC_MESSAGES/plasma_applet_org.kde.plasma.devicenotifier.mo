��          �      l      �  3   �  $     �   :     �  4   �     �  #        =  �   E  �   �  +   �     �     �     �  1     (   3     \  �   s  $   �  (     �  F  6        D     L     X  4   f  !   �  *   �     �  �   �  �   �	  )   Q
     {
     �
     �
     �
  .   �
       
   5  %   @  %   f                                                   	   
                                            1 action for this device %1 actions for this device @info:status Free disk space%1 free Accessing is a less technical word for Mounting; translation should be short and mean 'Currently mounting this device'Accessing... All devices Click to access this device from other applications. Click to eject this disc. Click to safely remove this device. General It is currently <b>not safe</b> to remove this device: applications may be accessing it. Click the eject button to safely remove this device. It is currently <b>not safe</b> to remove this device: applications may be accessing other volumes on this device. Click the eject button on these other volumes to safely remove this device. It is currently safe to remove this device. Most Recent Device No Devices Available Non-removable devices only Open auto mounter kcmConfigure Removable Devices Open popup when new device is plugged in Removable devices only Removing is a less technical word for Unmounting; translation shoud be short and mean 'Currently unmounting this device'Removing... This device is currently accessible. This device is not currently accessible. Project-Id-Version: plasma_applet_devicenotifier
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2017-07-25 22:04+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=(n != 1);
 ekintza 1 gailu honentzako %1 ekintza gailu honentzako %1 aske Atzitzen... Gailu guztiak Egin klik gailu hau beste aplikazioetatik atzitzeko. Egin klik disko hau kanporatzeko. Egin klik gailu hau era seguruan kentzeko. Orokorra Une honetan <b>ez da segurua</b> gailu hau kentzea: aplikazioak berau atzitzen egon daitezke. Egin klik egotzi botoian gailua era seguruan kentzeko. Une honetan <b>ez da segurua</b> gailu hau kentzea : aplikazioak gailu honetako beste bolumen batzuk atzitzen egon daitezke. Egin klik beste bolumen hauen egotzi botoian gailu hau era seguruan kentzeko. Une honetan segurua da gailu hau kentzea. Konektatutako azken gailua Ez dago gailurik eskuragarri Gailu ez-aldagarriak soilik Konfiguratu gailu aldagarriak Ireki laster-leihoa gailu berria lotzen denean Gailu aldagarriak soilik Kentzen... Gailu hau une honetan atzitu daiteke. Gailu hau une honetan ezin da atzitu. 