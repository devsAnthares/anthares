��    *      l  ;   �      �     �     �  )   �               /     H     ^  #   ~     �  
   �     �     �  %   �  +        E     Z     m  @   z     �     �     �     �               '     ,     9     ?     _     e  .   m     �  F   �  (   �  	   '  	   1  	   ;  '   E  7   m  "   �  �  �     �	     �	  ,   �	     �	     �	  	   �	     �	     �	     
     
  	   .
     8
     W
  "   i
  E   �
     �
     �
  	     J        X     p     �     �     �     �     �     �     �  %   �            ;   !     ]  R   }  (   �     �                         4                   $                                 %                      *                                               	   #   )   "             &           
                        '             (   !    &Do not remember @actionWalk through activities @actionWalk through activities (Reverse) @action:buttonApply @action:buttonCancel @action:buttonChange... @action:buttonCreate @title:windowActivity Settings @title:windowCreate a New Activity @title:windowDelete Activity Activities Activity information Activity switching Are you sure you want to delete '%1'? Blacklist all applications not on this list Clear recent history Create activity... Description: Error loading the QML files. Check your installation.
Missing %1 For a&ll applications Forget a day Forget everything Forget the last hour Forget the last two hours General Icon Keep history Name: O&nly for specific applications Other Privacy Private - do not track usage for this activity Remember opened documents: Remember the current virtual desktop for each activity (needs restart) Shortcut for switching to this activity: Shortcuts Switching Wallpaper for in 'keep history for 5 months'for  unit of time. months to keep the history month  months unlimited number of monthsforever Project-Id-Version: kcm_activities5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2017-08-26 23:01+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 E&z gogoratu Ibilaldia jardueratan zehar Ibilaldia jardueratan zehar (alderantzizkoa) Ezarri Utzi Aldatu... Sortu Jarduera-ezarpenak Sortu jarduera berria Ezabatu jarduera Jarduerak Jarduerari buruzko informazioa Jarduera aldaketa Ziur zaude %1 ezabatu nahi duzula? Zerrenda beltzera gehitu zerrenda honetan ez dauden aplikazio guztiak Garbitu duela gutxiko historia Sortu jarduera... Azalpena: Errorea QML fitxategiak zamatzean. Egiaztatu zure instalazioa.
%1 falta da Aplikazio &guztietarako Ahaztu egun bat Ahaztu guztia Ahaztu azken ordua Ahaztu azken bi orduak Orokorra Ikonoa Mantendu historia Izena: Soi&lik aplikazio zehatz batzuetarako Bestelakoak Pribatutasuna Pribatua - ez egin jarduera honen erabilpenaren jarraipenik Gogoratu irekitako dokumentuak: Gogoratu jarduera bakoitzaren uneko alegiazko mahaigaina (berrabiatu beharra dago) Jarduera honetara aldatzeko lasterbidea: Lasterbideak Aldaketa Horma-papera    hilabetez  hilabetez betirako 