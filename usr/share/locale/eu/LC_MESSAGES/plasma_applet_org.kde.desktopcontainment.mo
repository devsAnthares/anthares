��    ^           �      �     �               !     '     .     :     K     Y     a  /   i  -   �     �     �  
   �  
   �     �     �     	     	     	     "	     /	     4	  
   <	     G	     T	     c	  	   {	     �	     �	     �	     �	     �	     �	  	   �	     �	     �	     
  	   
     
     
     
     "
     '
  	   0
     :
     H
     O
     V
     n
     s
     x
     {
  <   �
     �
     �
     �
     �
     �
                    $  
   8     C     Q     c     r     �  )   �     �     �     �     �     �                       
   2     =     B  	   I     S     Z     _     y     �     �     �  E   �  *   �  �  &     �               *     1     :     H     ]  
   q     |  
   �     �  	   �     �     �     �     �     �     �  	   �     �            
   #  	   .     8     I  "   `     �     �     �  	   �  %   �     �     �                 )     J     Q     a     i  	   p     z  	   �  
   �     �  
   �  	   �  #   �     �  
   �     �     �  I        R     i     �     �     �  	   �     �     �     �     �     �     �           3     T  1   g     �  "   �     �     �     �     �                ,     A  	   O     Y  
   b     m     {  &   �     �     �     �     �  T   �     R     V   1   I   4           5   L          +   J               F       *      H             M       -   ?   O           	   6   R   ;      &       <   @       3         \   P   '      /   B          D   G       2   8   9                $   #          0                  !      Z   7       
       )       S       [   (   .      >      X   A   ,   T       K   :                          %         ^      Q       N   ]   E   =           W                  U      Y   "   C           &Delete &Empty Trash Bin &Move to Trash &Open &Paste &Properties &Refresh Desktop &Refresh View &Reload &Rename @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Align Appearance: Arrange In Arrange in Arrangement: Back Cancel Columns Configure Desktop Custom title Date Default Descending Deselect All Desktop Layout Enter custom title here Features: File name pattern: File types: Filter Folder preview popups Folders First Folders first Full path Got it Hide Files Matching Huge Icon Size Icons Large Left List Location Location: Lock in place Locked Medium More Preview Options... Name None OK Panel button: Press and hold widgets to move them and reveal their handles Preview Plugins Preview thumbnails Remove Resize Restore from trashRestore Right Rotate Rows Search file type... Select All Select Folder Selection markers Show All Files Show Files Matching Show a place: Show files linked to the current activity Show the Desktop folder Show the desktop toolbox Size Small Small Medium Sort By Sort by Sorting: Specify a folder: Text lines Tiny Title: Tool tips Tweaks Type Type a path or a URL here Unsorted Use a custom icon Widget Handling Widgets unlocked You can press and hold widgets to move them and reveal their handles. whether to use icon or list viewView mode Project-Id-Version: plasma_applet_org.kde.desktopcontainment
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:11+0100
PO-Revision-Date: 2017-11-12 00:23+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 &Ezabatu &Hustu zakarrontzia &Bota zakarrontzira &Ireki &Itsatsi &Propietateak F&reskatu mahaigaina F&reskatu ikuspegia &Birzamatu &Berrizendatu Hautatu... Kendu ikonoa Lerrokatu Itxura: Antolatu honela: Antolatu Antolaketa: Atzera Utzi Zutabetan Konfiguratu mahaigaina Titulu pertsonalizatua Data Lehenetsia Beherantz Desautatu guztia Mahaigainaren diseinua Sartu titulu pertsonalizatua hemen Eginbideak: Fitxategi-izenaren eredua: Fitxategi motak: Iragazkia Karpeta-aurreikuspegia azaleratzen da Karpetak lehenengo Karpetak lehenengo Bide-izen osoa Ulertuta Ezkutatu bat datozen fitxategiak Itzela Ikonoen neurria Ikonoak Handia Ezkerrera Zerrenda Kokalekua Kokalekua: Giltzatu lekuan Giltzatuta Euskarria Aurreikuspegiaren aukera gehiago... Izena Bat ere ez Ados Paneleko botoia: Zanpatu trepetak eta eutsi, haiek mugitu eta euren heldulekuak erakusteko Aurreikusteko pluginak Aurreikusteko koadro txikiak Kendu Neurria aldatu Lehengoratu Eskuinera Biratu Errenkadatan Bilatu ...fitxategi mota Hautatu guztia Hautatu karpeta Hautapenaren markatzaileak Erakutsi fitxategi guztiak Erakutsi bat datozen fitxategiak Erakutsi leku bat: Erakutsi uneko jarduerarekin lotutako fitxategiak Erakutsi Mahaigain karpeta Erakutsi Mahaigaineko tresna-kutxa Neurria Txikia txiki ertaina Ordenatu honen arabera Sailkatu ... arabera Sailkatzea: Zehaztu karpeta bat: Testu lerroak Ñimiñoa Titulua: Argibideak Egokitzapenak Mota Idatzi bide-izen bat edo URL bat hemen Sailkatu gabe Erabili ikono pertsonalizatua Trepeten kudeaketa Trepetak giltzatu gabe Trepetak zanpatu eta eutsi ditzakezu, haiek mugitu eta euren heldulekuak erakusteko. Ikuspegi-modua 