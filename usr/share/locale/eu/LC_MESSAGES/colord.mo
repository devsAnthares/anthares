��    2      �  C   <      H     I  2   Q     �  !   �     �     �  
   �  &   �     �     
          +     3     E     V  	   h     r      �     �     �     �     �  '   �                    !     *     A     M     U     [     c  
   k  	   v     �     �     �     �     �     �  (   �     '      >     _     e     k     p     �  �  �     %	  5   +	     a	  "   e	     �	     �	     �	  =   �	     �	     
     
     
     &
     7
     K
     `
  #   n
     �
     �
  '   �
     �
     �
  $   �
  	   "  	   ,     6     >     F     \     n     v     |     �     �     �  	   �     �     �     �  #         $  /   A     q  '   �     �  	   �     �     �     �     !       &                                      )      '            -                         #            /   (   *       .                  +            $   %                         ,      0      1              2   	          "   
    Ambient Authentication is required to use the color sensor CRT Clear any metadata in the profile Color Color Management Colorspace Command not found, valid commands are: Create a color profile Create a device Create a profile Created Debugging Options Deletes a device Deletes a profile Device ID Exit after a small delay Exit after the engine has loaded Filename Find a profile by filename Format Gamma Table Initialize any metadata for the profile Locked Metadata Model Modified Modify a color profile Object Path Options Owner Printer Profile Profile ID Projector Sensor Serial number Sets the copyright string Sets the description string Sets the manufacturer string Sets the model string Show debugging information for all files Show debugging options Show extra debugging information State Title Type Use color sensor Vendor Project-Id-Version: colord
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-08-21 09:32+0100
PO-Revision-Date: 2017-08-21 08:32+0000
Last-Translator: Richard Hughes <richard@hughsie.com>
Language-Team: Basque (http://www.transifex.com/freedesktop/colord/language/eu/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: eu
Plural-Forms: nplurals=2; plural=(n != 1);
 Giroa Autentifikazioa behar da kolore-sentsorea erabiltzeko CRT Garbitu profileko metadatu guztiak Kolorea Kolore-kudeaketa Kolore-espazioa Ez da komandoa aurkitu, komando baliogarriak hurrengoak dira: Sortu kolore-profila Sortu gailua Sortu profila Sortua Arazketa-aukerak Gailua ezabatzen du Profila ezabatzen du Gailuaren IDa Irten atzerapen txiki baten ondoren Irten motorra kargatu ondoren Fitxategi-izena Bilatu profila fitxategi-izenaren bidez Formatua Gamma-taula Hasieratu profileko metadatu guztiak Blokeatua Metadatua Modeloa Aldatua Aldatu kolore-profila Objektuaren bidea Aukerak Jabea Inprimagailua Profila Profilaren IDa Proiektorea Sentsorea Serie-zenbakia Copyright katea ezartzen du Deskribapen-katea ezartzen du Fabrikatzailearen katea ezartzen du Modeloaren katea ezartzen du Erakutsi fitxategi guztien arazketa-informazioa Erakutsi arazketa-aukerak Erakutsi arazketa-informazio gehigarria Egoera Izenburua Mota Erabili kolore-sentsorea Hornitzailea 