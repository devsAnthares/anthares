��          |      �             !  >   #  9   b     �     �  
   �      �  	   �            �   5  �  �     |  ?   ~  ;   �     �          0  #   =  	   a     k  @     �   �                                       	      
              % Battery will be considered critical when it reaches this level Battery will be considered low when it reaches this level Configure Notifications... Critical battery level Do nothing EMAIL OF TRANSLATORSYour emails Hibernate Low battery level NAME OF TRANSLATORSYour names The Power Management Service appears not to be running.
This can be solved by starting or scheduling it inside "Startup and Shutdown" Project-Id-Version: powerdevilglobalconfig
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-11 03:05+0200
PO-Revision-Date: 2014-01-26 22:55+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=(n != 1);
 % Bateria oso gutxi dagoela joko da maila honetara iristen denean Bateria gutxi dagoela joko da maila honetara iristen denean Konfiguratu jakinarazpenak... Bateria-maila oso baxua Ez egin ezer xalba@euskalnet.net,hizpol@ej-gv.es Hibernatu Bateria-maila baxua Iñigo Salvador Azurmendi,Hizkuntza Politikarako Sailburuordetza Itxura denez, energia kudeatzeko sistema ez da exekutatzen ari.
Sistema "Abioa eta itzaltzea" aukeran abiaraziz edo antolatuz konpon daiteke hori. 