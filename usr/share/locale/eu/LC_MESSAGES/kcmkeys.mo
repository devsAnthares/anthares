��          �   %   �      @     A     H     Z  �   i  �   %  
   �     �       2   !     T     Y     j     o     �     �     �     �     �  /   �     "     B  ?   \  =   �  S   �  �  .     �     �       �     �   �     �	     �	     �	  H   
  
   J
     U
     i
     q
     �
     �
     �
     �
  )   �
  -   
  $   8  #   ]  D   �  @   �  \                                                               	                            
                                      &Path: &Standard scheme: All Components Are you sure you want to remove the registered shortcuts for component '%1'? The component and shortcuts will reregister themselves with their default settings when they are next started. Component '%1' is currently active. Only global shortcuts currently not active will be removed from the list.
All global shortcuts will reregister themselves with their defaults when they are next started. Components Current Component Export Scheme... Failed to contact the KDE global shortcuts daemon
 File Import Scheme... Load Load Shortcut Scheme Message: %1
Error: %2 Remove component Reset to defaults Select Shortcut Scheme Select a shortcut scheme file Select one of the standard KDE shortcut schemes Select the Components to Export Set All Shortcuts to None This file (%1) does not exist. You can only select local files. You are about to reset all shortcuts to their default values. Your current changes will be lost if you load another scheme before saving this one Project-Id-Version: kcmkeys
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-03 04:12+0100
PO-Revision-Date: 2014-02-10 23:29+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=(n != 1);
 &Bide-izena: &Eskema estandarra: Osagai guztiak Ziur zaude '%1' osagaiarentzako erregistratuta dauden lasterbideak kendu nahi dituzula? Osagaiak eta lasterbideak balio lehenetsiekin erregistratuko dute berriz beren burua hurrengo aldian abiarazten direnean. '%1' osagaia aktibo dago une honetan. Une honetan aktibo ez dauden lasterbide globalak bakarrik kenduko dira zerrendatik.
Lasterbide global guztiek balio lehenetsiekin erregistratuko dute berriz beren burua hurrengo aldian abiarazten direnean. Osagaiak Uneko osagaia Esportatu eskema... Huts egin du KDE lasterbide globalen daimonarekin harremanetan jartzean
 Fitxategia Inportatu eskema... Kargatu Kargatu lasterbide-eskema Mezua: %1
Errorea: %2 Kendu osagaia Berrezarri lehenetsiak Hautatu lasterbide-eskema Hautatu lasterbide-eskemako fitxategi bat Hautatu KDEren lasterbide-eskema estandar bat Hautatu esportatu beharreko osagaiak Ezarri lasterbide guztiak ezerezera Fitxategia (%1) ez dago. Fitxategi lokalak bakarrik hauta ditzakezu. Lasterbide guztiak haien balio lehenetsietan berrezartzera zoaz. Zure uneko aldaketak galdu egingo dira, eskema hau gorde aurretik beste bat kargatzen baduzu 