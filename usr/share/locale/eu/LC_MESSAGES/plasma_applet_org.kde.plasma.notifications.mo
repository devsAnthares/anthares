��    #      4  /   L            	     *     6  -   U  #   �  #   �  ?   �  1        =     Q  H   V  L   �     �  V   �  N   K     �  
   �     �     �     �     �  2   �     (  )   H  8   r  #   �  .   �  -   �  D   ,  6   q  0   �  A   �  =     9   Y  �  �     h
  
   �
  !   �
  5   �
  
   �
  
   �
               +     B     J     j     �     �     �     �     �     �     �     �            $     4   =     r  C   �  =   �          '     C     H     Q     U     f                                                          "       	                       
             !                                             #           %1 notification %1 notifications %1 of %2 %3 %1 running job %1 running jobs &Configure Event Notifications and Actions... 10 seconds ago, keep short10 s ago 30 seconds ago, keep short30 s ago A button tooltip; expands the item to show detailsShow Details A button tooltip; hides item detailsHide Details Clear Notifications Copy Either just 1 dir or m of n dirs are being processed1 dir %2 of %1 dirs Either just 1 file or m of n files are being processed1 file %2 of %1 files History How much many bytes (or whether unit in the locale has been copied over total%1 of %2 Indicator that there are more urls in the notification than previews shown+%1 Information Job Failed Job Finished No new notifications. No notifications or jobs Open... Placeholder is job name, eg. 'Copying'%1 (Paused) Show a history of notifications Show application and system notifications Speed and estimated time to completion%1 (%2 remaining) Track file transfers and other jobs Use custom position for the notification popup minutes ago, keep short%1 min ago %1 min ago notification was added n days ago, keep short%1 day ago %1 days ago notification was added yesterday, keep shortYesterday notification was just added, keep shortJust now placeholder is row description, such as Source or Destination%1: the job, which can be anything, failed to complete%1: Failed the job, which can be anything, has finished%1: Finished Project-Id-Version: plasma_applet_org.kde.plasma.notifications
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:07+0100
PO-Revision-Date: 2017-11-01 21:48+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 Jakinarazpen %1 %1 jakinarazpen %1 / %2 %3 Ataza %1 martxan %1 ataza martxan &Konfiguratu gertaeren jakinarazpenak eta ekintzak... duela 10 s duela 30 s Erakutsi xehetasunak Ezkutatu xehetasunak Garbitu jakinarazpenak Kopiatu direktorio 1 %2 / %1 direktorio fitxategi 1 %2 / %1 fitxategi Historia %1 / %2 +%1 Informazioa Atazak huts egin du Ataza amaituta Jakinarazpen berririk ez. Ez jakinarazpenik edo atazik Ireki... %1 (Etenda) Erakutsi jakinarazpenen historia bat Erakutsi Aplikazioen jakinarazpenak eta sistemarenak %1 (%2 falta dira) Jarraipena egin fitxategien transferentziei eta beste ataza batzuei Erabili jakinarazpen gainerakorraren kokaleku pertsonalizatua duela minutu %1 duela %1 minutu duela egun %1 duela %1 egun Atzo Oraintxe %1: %1: huts egin du %1: Amaituta 