��    >        S   �      H     I  !   e  "   �  &   �  ,   �  #   �  &   "  !   I  &   k  '   �  "   �  #   �  "     '   $     L     _  +   c  
   �     �     �     �     �     �     �  U   �  7   J     �     �     �     �      �     �     �     	     	  !   &	     H	  	   M	     W	     _	     	     �	  &   �	     �	     �	     �	     
     
     #
     5
  4   =
  $   r
     �
     �
     �
     �
     �
            &   )     P  �  j  !   =     _     f  	   m     w     �     �     �  	   �  
   �     �     �     �  
   �     �     �  :   �  
   7     B     V  (   f  
   �      �     �  q   �  O   6     �      �     �     �  &   �                .     7  (   W  	   �     �     �  $   �     �     �  /   �  -   +     Y     _  	   ~     �     �  	   �  6   �  *   �          +     E     [     z     �     �  .   �  !   �         #          1   &                  	   <          6       =   0   2          +                                 '   ;              5   -             /   !      *               3   )                    
          $             ,   9   8   :           %       "      (       >   4      7      .       &Matching window property:  @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Border @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large @item:inlistbox Button size:Large @item:inlistbox Button size:Normal @item:inlistbox Button size:Small @item:inlistbox Button size:Very Large Active Window Glow Add Add handle to resize windows with no border Animations B&utton size: Border size: Button mouseover transition Center Center (Full Width) Class:  Configure fading between window shadow and glow when window's active state is changed Configure window buttons' mouseover highlight animation Decoration Options Detect Window Properties Dialog Edit Edit Exception - Oxygen Settings Enable/disable this exception Exception Type General Hide window title bar Information about Selected Window Left Move Down Move Up New Exception - Oxygen Settings Question - Oxygen Settings Regular Expression Regular Expression syntax is incorrect Regular expression &to match:  Remove Remove selected exception? Right Shadows Tit&le alignment: Title:  Use the same colors for title bar and window content Use window class (whole application) Use window title Warning - Oxygen Settings Window Class Name Window Drop-Down Shadow Window Identification Window Property Selection Window Title Window active state change transitions Window-Specific Overrides Project-Id-Version: oxygen_kdecoration
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-13 05:20+0100
PO-Revision-Date: 2017-08-27 11:26+0100
Last-Translator: Ander Elortondo <ander.elor@gmail.com>
Language-Team: Basque (https://www.transifex.com/librezale/teams/76827/eu/)
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 &Bat datorren leiho-propietatea:  Itzela Handia Ertzik ez Alboko ertzik ez Arrunta Neurriz kanpokoa Ttipia Erraldoia Oso handia Handia Arrunta Txikia Oso handia Aktibatu leiho dirdira Gehitu Erantsi heldulekua ertzik gabeko leihoen neurria aldatzeko Animazioak B&otoiaren neurria: Ertzen neurria: Botoien sagua gainetik igaro trantsizioa Erdiratuta Erdiratuta (zabalera osoarekiko) Klasea:  Konfiguratu urtze-trantsizioa leihoaren itzala eta distiraren artean leihoaren aktibotasun egoera aldatzen denean Konfiguratu leiho botoien sagua gainetik igarotzerakoan nabarmentzeko animazioa Apaingarrien aukerak Detektatu leihoaren propietateak Elkarrizketa-koadroa Editatu Editatu salbuespena - Oxigen ezarpenak Gaitu/desgaitu salbuespen hau Salbuespen mota Orokorra Ezkutatu leihoaren titulu-barra Hautatutako leihoari buruzko informazioa Ezkerrean Eraman behera Eraman gora Salbuespen berria - Oxigen ezarpenak Galdera - Oxigen ezarpenak Adierazpen erregularra Adierazpen erregularraren sintaxia ez da zuzena Bat etorri be&harreko adierazpen erregularra: Kendu Kendu hautatutako salbuespena? Eskuinean Itzalak Tit&uluaren lerrokatzea: Titulua:  Erabili kolore berdina titulu barran eta leiho edukian Erabili leiho klasea (aplikazio osoarekin) Erabili leiho-titulua Abisua - Oxigen ezarpenak Leiho-klasearen izena Leihoaren goitibeherako itzala Leiho-identifikazioa Leiho-propietateen hautaketa Leihoaren titulua Leihoen egoera aktibo aldaketaren trantsizioak  Leihoaren zuzenketa espezifikoak 