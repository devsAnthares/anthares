��          �      |      �     �                (     I     ]     u     �     �     �     �     �     �     �  �   �     l  #   �  @   �  ?   �     )     -  �  5     �            X        x  "   �  W   �          &  	   2     <     E     W     ^  �   f  $     )   3  E   ]  D   �     �     �     
                                         	                                                   (c) 2002 Daniel Molkentin Daniel Molkentin Description EMAIL OF TRANSLATORSYour emails KDE Service Manager Load-on-Demand Services NAME OF TRANSLATORSYour names Not running Running Service Start Startup Services Status Stop This is a list of available KDE services which will be started on demand. They are only listed for convenience, as you cannot manipulate these services. Unable to contact KDED. Unable to start server <em>%1</em>. Unable to start service <em>%1</em>.<br /><br /><i>Error: %2</i> Unable to stop service <em>%1</em>.<br /><br /><i>Error: %2</i> Use kcmkded Project-Id-Version: kcmkded
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-16 03:36+0100
PO-Revision-Date: 2014-02-18 10:06+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=(n != 1);
 (c) 2002 Daniel Molkentin Daniel Molkentin Deskribapena ziberpunk@ziberghetto.dhis.org,marcos@euskalgnu.org,igaztanaga@gmail.com,hizpol@ej-gv.es KDEren zerbitzu-kudeatzailea Eskatu ahala kargatzeko zerbitzuak Alfredo Beaumont,Marcos Goyeneche,Ion Gaztañaga,Hizkuntza Politikarako Sailburuordetza Ez da exekutatzen ari Exekutatzen Zerbitzua Abiarazi Abioko zerbitzuak Egoera Gelditu Eskatu ahala abiaraziko diren KDEren zerbitzu erabilgarrien zerrenda da hau. Informazioa emateko baino ez dira zerrendatzen zerbitzu horiek, ezin baitituzu manipulatu. Ezin da jarri harremanetan KDEDekin. Ezin da abiarazi <em>%1</em> zerbitzaria. Ezin da abiarazi <em>%1</em> zerbitzua.<br /><br /><i>Errorea: %2</i> Ezin da gelditu <em>%1</em> zerbitzua.<br /><br /><i>Errorea: %2</i> Erabili kcmkded 