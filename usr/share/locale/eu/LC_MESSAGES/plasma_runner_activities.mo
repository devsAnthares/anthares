��          L      |       �      �      �   3   �           "  �  <     �       I        _     q                                         KRunner keywordactivity KRunner keywordactivity :q: Lists all activities currently available to be run. Switch to "%1" Switches to activity :q:. Project-Id-Version: plasma_runner_activities
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2014-01-26 12:34+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=(n != 1);
 jarduera :q: jarduera Unean exekutatzeko erabilgarri dauden jarduera guztiak zerrendatzen ditu. Aldatu hona: "%1" :q: jarduerara aldatzen du. 