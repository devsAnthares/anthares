��          t      �         �     �   �  9   �  7   �  8   �  
   8     C     J     c  
   {  �  �  �   D  1  �  7     A   ;  O   }     �     �  *   �  $        6         
                    	                               Check this option if the application you want to run is a text mode application. The application will then be run in a terminal emulator window. Check this option if you want to run the application with a different user id. Every process has a user id associated with it. This id code determines file access and other permissions. The password of the user is required to do this. Enter the password here for the user you specified above. Enter the user you want to run the application as here. Finds commands that match :q:, using common shell syntax Pass&word: Run %1 Run as a different &user Run in &terminal window User&name: Project-Id-Version: plasma_runner_shell
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-12-17 03:48+0100
PO-Revision-Date: 2014-01-26 01:34+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=(n != 1);
 Markatu aukera hau, testu moduko aplikazio bat exekutatu nahi baduzu. Hala, aplikazioa terminal-emulatzailearen leiho batean exekutatuko da. Markatu aukera hau aplikazioa beste erabiltzaile-identifikatzaile batekin exekutatu nahi baduzu. Prozesu bakoitza erabiltzaile-identifikatzaile batekin lotuta dago. Identifikatzaile-kode horrek fitxategirako sarbidea eta beste baimen batzuk zehazten ditu. Horretarako, erabiltzailearen pasahitza behar da. Idatzi goian adierazi duzun erabiltzailearen pasahitza. Idatzi aplikazioa zer erabiltzaile-izenekin exekutatu nahi duzun. :q:(r)ekin bat datozen komandoak aurkitzen ditu, shell sintaxi arrunta erabiliz &Pasahitza: Exekutatu %1 Beste &erabiltzaile-izen batekin exekutatu Exekutatu &terminalaren leiho batean Erabiltzaile-ize&na: 