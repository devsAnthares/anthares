��    &      L  5   |      P     Q  8   S     �     �     �     �     �     �  3   �  >        N     i     y     �  m   �     �          "     2     7     ?  *   O      z     �     �  "   �     �     �          3     :     H     X     e     �  ;   �     �  �  �     �     �     �     �     �  	   
	     	     '	  	   >	  
   H	     S	     q	     �	     �	     �	     �	     �	     �	  	   �	     �	     �	  8   �	  .   8
     g
     ~
     �
     �
     �
  $   �
     �
     �
          +     <     D     K     Q        #   %                                                              "          $                      
                                           &      !                	        % Accessibility data on volume sliderAdjust volume for %1 Applications Audio Muted Audio Volume Behavior Capture Devices Capture Streams Checkable switch for (un-)muting sound output.Mute Checkable switch to change the current default output.Default Decrease Microphone Volume Decrease Volume Devices General Heading for a list of ports of a device (for example built-in laptop speakers or a plug for headphones)Ports Increase Microphone Volume Increase Volume Maximum volume: Mute Mute %1 Mute Microphone No applications playing or recording audio No output or input devices found Playback Devices Playback Streams Port is unavailable (unavailable) Port is unplugged (unplugged) Raise maximum volume Show additional options for %1 Volume Volume at %1% Volume feedback Volume step: label of device items%1 (%2) label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: plasma_applet_org.kde.plasma.volume
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2017-11-05 15:20+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 % Doitu honen bolumena: %1 Aplikazioak Audioa isilduta Audioaren bolumena Jokabidea Atzemateko gailuak Atzemandako korronteak Isilarazi Lehenetsia Jaitsi mikrofonoaren bolumena Jaitsi bolumena Gailuak Orokorra Atakak Igo mikrofonoaren bolumena Igo bolumena Bolumen handiena: Isilarazi Isilarazi %1 Isilarazi mikrofonoa Ez dago audioa jotzen edo grabatzen ari den aplikaziorik Ez da aurkitu sarrerako edo irteerako gailurik Atzera jotzeko gailuak Atzera jotako korronteak  (ez erabilgarri)  (entxufatu gabe) Igo bolumen handiena Erakutsi honen aukera osagarriak: %1 Bolumena Bolumena % %1(e)an Bolumenaren berrelikadura Bolumen urratsa: %1 (%2) %1: %2 % 100 % %1 