��    D      <  a   \      �     �     �     �  !     "   %  &   H  ,   o  #   �  &   �  !   �  &   	  '   0  "   X  #   {  !   �  "   �  '   �       +     2   <     o  
   �     �     �     �     �     �     �     �     �     	  !   	  +   *	     V	     v	      {	     �	     �	     �	     �	     �	  !   �	     
  	    
     *
     2
     R
     m
  &   �
     �
     �
     �
     �
     �
     �
     �
            $        A     R     l     ~     �     �     �  >   �  �       �     �  !   �     �     �  	                  '  	   7  
   A  
   L     W     ^  
   f     q  
   x     �  :   �  ;   �       
        #     7  
   G      R     s     |     �      �     �  .   �  6   �  &   6     ]  &   e     �     �     �     �     �  (   �  	        '     5  $   A     f     �  /   �  -   �     �     �  	        %  	   -  	   7     A  	   Z  *   d     �     �     �     �     �                :     D   8      1                 )                  .               3   @   4       &      /   >       ?      <   ;             7       (          
                 $                  0   5       A   :   #   2         C   +                        =   '             *                 	          ,   !                             B   9   6       "       %   -     ms % &Matching window property:  @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Border @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large @item:inlistbox Button size:Large @item:inlistbox Button size:Medium @item:inlistbox Button size:None @item:inlistbox Button size:Small @item:inlistbox Button size:Very Large Add Add handle to resize windows with no border Allow resizing maximized windows from window edges Anima&tions duration: Animations B&utton size: Border size: Center Center (Full Width) Class:  Color: Decoration Options Detect Window Properties Dialog Draw a circle around close button Draw separator between Title Bar and Window Draw window background gradient Edit Edit Exception - Breeze Settings Enable animations Enable/disable this exception Exception Type General Hide window title bar Information about Selected Window Left Move Down Move Up New Exception - Breeze Settings Question - Breeze Settings Regular Expression Regular Expression syntax is incorrect Regular expression &to match:  Remove Remove selected exception? Right Shadows Si&ze: Tiny Tit&le alignment: Title:  Use window class (whole application) Use window title Warning - Breeze Settings Window Class Name Window Identification Window Property Selection Window Title Window-Specific Overrides strength of the shadow (from transparent to opaque)S&trength: Project-Id-Version: breeze_kwin_deco
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-09 03:20+0100
PO-Revision-Date: 2018-02-10 17:56+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 ms % &Bat datorren leiho-propietatea:  Itzela Handia Ertzik ez Alboko ertzik ez Arrunta Neurriz kanpoko Ñimiñoa Oso itzela Oso handia Handia Ertaina Bat ere ez Txikia Oso handia Erantsi Erantsi heldulekua ertzik gabeko leihoen neurria aldatzeko Baimendu maximizatutako leihoak ertzetatik neurriz aldatzea Anima&zioen iraupena: Animazioak B&otoiaren neurria: Ertzen neurria: Erdiratuta Erdiratuta (zabalera osoarekiko) Klasea:  Kolorea: Apaingarrien aukerak Detektatu leihoaren propietateak Elkarrizketa-koadroa Marraztu zirkulu bat ixteko botoiaren inguruan Marraztu bereiztekoa Titulu-barra eta leihoaren artean Marraztu leiho-atzealdearen gradientea Editatu Editatu salbuespena - Breeze ezarpenak Gaitu animazioak Gaitu/desgaitu salbuespen hau Salbuespen mota Orokorra Ezkutatu leihoaren titulu-barra Hautatutako leihoari buruzko informazioa Ezkerrera Eraman behera Eraman gora Salbuespen berria - Breeze ezarpenak Galdera - Breeze ezarpenak Adierazpen erregularra Adierazpen erregularraren sintaxia ez da zuzena Bat etorri be&harreko adierazpen erregularra: Kendu Kendu hautatutako salbuespena? Eskuinera Itzalak &Neurria: Ñimiñoa Tit&uluaren lerrokatzea: Titulua:  Erabili leiho klasea (aplikazio osoarekin) Erabili leiho-titulua Abisua - Breeze ezarpenak Leiho-klasearen izena Leiho-identifikazioa Leiho-propietateen hautaketa Leihoaren titulua Leihoen berariazko gainezartzeak Ilun&tasuna: 