��          �      ,      �     �     �     �     �      �            
   4     ?     N     _     y     �     �  $   �     �  �       �  $   �     �       8        V  %   d  
   �     �     �     �  Q   �  !   !     C     _          
                         	                                                Arguments for the module Configuration module to open Daniel Molkentin Do not display main window EMAIL OF TRANSLATORSYour emails Frans Englich List all possible modules Maintainer Matthias Elter Matthias Ettrich Matthias Hoelzer-Kluepfel NAME OF TRANSLATORSYour names No description available Specify a particular language The following modules are available: Waldo Bastian Project-Id-Version: kcmshell
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-07 06:55+0200
PO-Revision-Date: 2014-02-18 08:57+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=(n != 1);
 Modulurako argumentuak Ireki beharreko konfigurazio-modulua Daniel Molkentin Ez bistaratu leiho nagusia marcos@euskalgnu.org,xalba@euskalnet.net,hizpol@ej-gv.es Frans Englich Zerrendatu ahal diren modulu gehienak Arduraduna Matthias Elter Matthias Ettrich Matthias Hoelzer-Kluepfel Marcos Goyeneche,Iñigo Salvador Azurmendi,Hizkuntza Politikarako Sailburuordetza Ez dago deskribapenik erabilgarri Zehaztu hizkuntza jakin bat Modulu hauek daude erabilgarri: Waldo Bastian 