��          �      L      �     �     �  �   �  T   }  )   �  (   �  0   %  $   V  &   {  &   �  %   �  ?   �  F   /     v     �     �     �     �  �  �     �     �  �   �  w   �       	              4     <     I     Q     X     k     �     �     �     �      �                                              
                    	                                  Dim screen by half Dim screen totally Lists screen brightness options or sets it to the brightness defined by :q:; e.g. screen brightness 50 would dim the screen to 50% maximum brightness Lists system suspend (e.g. sleep, hibernate) options and allows them to be activated Note this is a KRunner keyworddim screen Note this is a KRunner keywordhibernate Note this is a KRunner keywordscreen brightness Note this is a KRunner keywordsleep Note this is a KRunner keywordsuspend Note this is a KRunner keywordto disk Note this is a KRunner keywordto ram Note this is a KRunner keyword; %1 is a parameterdim screen %1 Note this is a KRunner keyword; %1 is a parameterscreen brightness %1 Set Brightness to %1 Suspend to Disk Suspend to RAM Suspends the system to RAM Suspends the system to disk Project-Id-Version: plasma_runner_powerdevil
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-26 03:33+0200
PO-Revision-Date: 2014-01-26 02:00+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=(n != 1);
 Ilundu pantaila erdira Ilundu erabat pantaila Pantailaren distira-aukerak zerrendatzen ditu edo :q:(e)k definitutako distira ezartzen du; adibidez, pantailaren distira 50 jarrita, pantailaren distira maximoaren % 50 izango litzateke Sistema egonean uzteko aukerak (adibidez, lo egin, hibernatu) zerrendatzen ditu, eta haiek aktibatzeko aukera ematen du ilundu pantaila hibernatu pantailaren distira lo egin egonean utzi diskora RAMera ilundu pantaila %1 %1 pantailaren distira Ezarri %1(e)ko distira Egonean utzi diskoan Egonean utzi RAMen Sistema RAMen egonean uzten du Sistema diskoan egonean uzten du 