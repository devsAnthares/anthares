��          �            h     i  	   r     |      �  X   �     �       
   #  
   .     9     T     j     q     �  �  �     F     N     Z  K   b  e   �       c      
   �  
   �     �     �     �     �     �                                              
   	                               Accurate Automatic Crisp EMAIL OF TRANSLATORSYour emails Hint: To find out or configure how to activate an effect, look at the effect's settings. Instant NAME OF TRANSLATORSYour names OpenGL 2.0 OpenGL 3.1 Re-enable OpenGL detection Re-use screen content Smooth Smooth (slower) XRender Project-Id-Version: kcmkwincompositing
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2017-08-18 19:27+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 1.5
 Zehatza Automatikoa Zurruna marcos@euskalgnu.org,asieriko@gmail.com,xalba@euskalnet.net,hizpol@ej-gv.es Argibidea: efektu bat nola aktibatu jakiteko, edo hura konfiguratzeko, begiratu efektuen ezarpenetan. Berehalakoa Marcos Goyeneche,Asier Urio Larrea,Iñigo Salvador Azurmendi,Hizkuntza Politikarako Sailburuordetza OpenGL 2.0 OpenGL 3.1 Birgaitu OpenGLren detekzioa Berrerabili pantailako edukia Leuna Leuna (motelagoa) XRender 