��            )   �      �     �  
   �  /   �  -   �          !  
   2     =     J     `  W   i     �  ,   �  	                  !     '     -  
   :     E     W     o     �     �     �     �  1   �       �       �  
   �  
   �               *     <     C     O     j  w   w     �  L        S     \     l     u     }     �     �     �     �     �     �  %   �     	     ,	     L	     h	                                                                                  
                                	                       %1@%2 %2@%3 (%1) @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Favorites All Applications Appearance Applications Applications updated. Computer Drag tabs between the boxes to show/hide them, or reorder the visible tabs by dragging. Edit Applications... Expand search to bookmarks, files and emails Favorites Hidden Tabs History Icon: Leave Menu Buttons Often Used On All Activities On The Current Activity Remove from Favorites Show In Favorites Show applications by name Sort alphabetically Switch tabs on hover Type is a verb here, not a nounType to search... Visible Tabs Project-Id-Version: plasma_applet_org.kde.plasma.kickoff
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2017-10-21 13:12+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 %1@%2 %2@%3 (%1) Hautatu... Berrezarri ikonoa Erantsi gogokoetara Aplikazio guztiak Itxura Aplikazioak Eguneratutako aplikazioak. Ordenagailua Arrastatu fitxak koadro batetik bestera haiek erakutsi/ezkutatzeko, edo fitxa ikusgaien ordena aldatu haiek arrastatuz. Editatu aplikazioak... Zabaldu bilaketa laster-marketara, fitxategietara eta posta-elektronikoetara Gogokoak Ezkutuko fitxak Historia Ikonoa: Irten Menuko botoiak Maiz erabilia Jarduera guztietan Uneko jardueran Ezabatu gogokoen artetik Erakutsi gogokoetan Erakutsi aplikazioak izenaren arabera Sailkatu alfabetikoki Fitxa aldatu gainetik pasatzean Idatzi bilatu beharrekoa... Fitxa ikusgarriak 