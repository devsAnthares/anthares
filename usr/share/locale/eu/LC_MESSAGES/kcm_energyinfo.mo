��    7      �  I   �      �     �     �     �     �     �     �               $     -     5     H     T      `     �     �     �     �     �     �     �     �                     )     7  	   C  	   M     W     d     j     �     �     �     �     �     �     �     �     �     �  @   �     7     @     \     c     j     r     �     �     �  4   �     �  �  �     �	     �	     �	     �	     �	  	   �	     �	     �	  	   
     
     
      
     0
     =
     Q
     Y
  !   k
  	   �
     �
     �
     �
     �
     �
     �
                $     5     A     N     ]     d     ~     �     �     �     �     �     �     �     �     �  E   �     ,  "   ;     ^     k     m     v     �     �     �  	   �     �     &   0      7                  #                
          	   '   (   +                       6              3      1                   ,   2                  "       *          $      4   )               %      5                     !          /   .   -       % %1 is value, %2 is unit%1 %2 %1: Application Energy Consumption Battery Capacity Charge Percentage Charge state Charging Current Degree Celsius°C Details: %1 Discharging EMAIL OF TRANSLATORSYour emails Energy Energy Consumption Energy Consumption Statistics Environment Full design Fully charged Has power supply Kai Uwe Broulik Last 12 hours Last 2 hours Last 24 hours Last 48 hours Last 7 days Last full Last hour Manufacturer Model NAME OF TRANSLATORSYour names No Not charging PID: %1 Path: %1 Rechargeable Refresh Serial Number Shorthand for WattsW System Temperature This type of history is currently not available for this device. Timespan Timespan of data to display Vendor VoltV Voltage Wakeups per second: %1 (%2%) WattW Watt-hoursWh Yes current power draw from the battery in WConsumption literal percent sign% Project-Id-Version: kcm_energyinfo
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2017-03-26 16:05+0200
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 1.5
 % %1 %2 %1: Aplikazioen energia kontsumoa Bateria Gaitasuna Kargaren ehunekoa Kargaren egoera Kargatzen Unekoa °C Xehetasunak: %1 Deskargatzen xalba@euskalnet.net Energia Energia kontsumoa Energia kontsumoaren estatistikak Ingurunea Diseinuko betea Osorik kargatuta Energia hornidura du Kai Uwe Broulik Azken 12 orduetan Azken 2 orduetan Azken 24 orduetan Azken 48 orduetan Azken 7 egunetan Azken betea Azken orduan Fabrikatzailea Eredua Iñigo Salvador Azurmendi Ez Ez da kargatzen ari PID: %1 Bide-izena: %1 Kargagarria Freskatu Serie-zenbakia W Sistema Tenperatura Mota honetako historia ez dago eskuragarri oraingoz gailu honentzako. Denbora tartea Bistaratzeko datuen denbora tartea Hornitzailea V Tentsioa Esnatzeak segundoko: %1 (%2%) W Wh Bai Kontsumoa % 