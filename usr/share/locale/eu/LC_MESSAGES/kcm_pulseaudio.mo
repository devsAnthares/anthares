��    $      <  5   \      0     1     6  )   J     t  $   �  &   �  #   �  !   �  "   !     D     T     f     z     �  J   �     �  L        [     c     k      {     �  
   �     �     �     �     �     �  "   �       )   9  <   c     �  ;   �     �  �       �     �     �     �  *    	  -   +	  #   Y	     }	     �	     �	  
   �	  
   �	     �	     �	  \   �	     ?
  X   _
  	   �
  
   �
     �
     �
     �
     �
     
     $     9     B     I     O     a  *   s  C   �     �     �     �     	                                                                         !       #             "                                        
           $                               100% @info:creditAuthor @info:creditCopyright 2015 Harald Sitter @info:creditHarald Sitter @labelNo Applications Playing Audio @labelNo Applications Recording Audio @labelNo Device Profiles Available @labelNo Input Devices Available @labelNo Output Devices Available @labelProfile: @titlePulseAudio @title:tabAdvanced @title:tabApplications @title:tabDevices Add virtual output device for simultaneous output on all local sound cards Advanced Output Configuration Automatically switch all running streams when a new output becomes available Capture Default Device Profiles EMAIL OF TRANSLATORSYour emails Inputs Mute audio NAME OF TRANSLATORSYour names Notification Sounds Outputs Playback Port Port is unavailable (unavailable) Port is unplugged (unplugged) Requires 'module-gconf' PulseAudio module This module allows to set up the Pulseaudio sound subsystem. label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: kcm_pulseaudio
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-03 03:00+0200
PO-Revision-Date: 2017-08-31 15:42+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 % 100 Egilea Copyright 2015 Harald Sitter Harald Sitter Ez dago audioa jotzen ari den aplikaziorik Ez dago soinua grabatzen ari den aplikaziorik Ez dago gailu-profil erabilgarririk Ez dago sarrera gailurik Ez dago irteera gailurik Profila: PulseAudio Aurreratua Aplikazioak Gailuak Erantsi alegiazko irteera gailua, irteera bertako soinu txartel guztietan aldi beren izateko Irteera konfigurazio aurreratua Automatikoki bideratu martxan dauden korronte guztiak erabilgarri dagoen irteera berrira Kapturatu Lehenetsia Gailu-profilak xalba@euskalnet.net Sarrerak Isilarazi audioa Iñigo Salvador Azurmendi Jakinarazpen soinuak Irteerak Jotzea Ataka  (ez erabilgarri)  (entxufatu gabe) Behar du 'module-gconf' PulseAudio modulua Modulu honek PulseAudio soinu azpisistema ezartzea ahalbidetzen du. %1: %2 % 100 %%1 