��          �      L      �  &   �  +   �       @     	   ]     g     �     �     �     �  (   �     �     �  ,   �               +     7  �  ;  (     +   4     `     i  	   r     |     �     �     �     �  1   �     �     �  :        I     V     k     �        
                          	                                                                  Do you want to suspend to RAM (sleep)? Do you want to suspend to disk (hibernate)? General Heading for list of actions (leave, lock, shutdown, ...)Actions Hibernate Hibernate (suspend to disk) Leave Leave... Lock Lock the screen Logout, turn off or restart the computer No Sleep (suspend to RAM) Start a parallel session as a different user Suspend Switch User Switch user Yes Project-Id-Version: plasma_applet_org.kde.plasma.lock_out
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2017-11-05 14:50+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=(n != 1);
 RAMean egonean utzi nahi duzu (lokartu)? Diskoan egonean utzi nahi duzu (hibernatu)? Orokorra Ekintzak Hibernatu Hibernatu (eseki diskora) Irten Irten... Giltzatu Giltzatu pantaila Saio amaitu, ordenagailua itzali edo berrabiarazi Ez Lotarazi (eseki RAM-era) Abiatu saio paralelo bat erabiltzaile desberdin bat bezala Egonean utzi Aldatu erabiltzailea Aldatu erabiltzailea Bai 