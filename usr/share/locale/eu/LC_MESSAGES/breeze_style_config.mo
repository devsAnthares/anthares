��            )         �     �  "   �     �  !   �  !        4  
   J     U     p     �  !   �     �  0   �  8        ?  !   ]          �     �     �     �                '  
   /  
   :  
   E  &   P     w     �  �  �     V  +   Y     �  '   �  '   �     �  
             .     @  *   ^  '   �  F   �  C   �  (   <	  ,   e	  (   �	  '   �	     �	  -    
     .
  *   ?
     j
     r
     {
  	   �
     �
  5   �
     �
     �
                                                                                                 	                       
                 ms &Keyboard accelerators visibility: &Top arrow button type: Always Hide Keyboard Accelerators Always Show Keyboard Accelerators Anima&tions duration: Animations Bottom arrow button t&ype: Breeze Settings Center tabbar tabs Drag windows from all empty areas Drag windows from titlebar only Drag windows from titlebar, menubar and toolbars Draw a thin line to indicate focus in menus and menubars Draw focus indicator in lists Draw frame around dockable panels Draw frame around page titles Draw frame around side panels Draw slider tick marks Draw toolbar item separators Enable animations Enable extended resize handles Frames General No Buttons One Button Scrollbars Show Keyboard Accelerators When Needed Two Buttons W&indows' drag mode: Project-Id-Version: breeze_style_config
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:42+0200
PO-Revision-Date: 2017-11-01 21:25+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 ms &Teklatuko azeleratzaileen ikusgarritasuna: &Gora geziaren botoi mota: Ezkutatu beti teklatuko azeleratzaileak Erakutsi beti teklatuko azeleratzaileak Anima&zioen iraupena: Animazioak Behera geziaren botoi mo&ta: Breezen ezarpenak Erdiratu fitxa-barrako fitxak Arrastatu leihoak azalera huts guztietatik Arrastatu leihoa titulu-barratik soilik Arrastatu leihoak titulu-barratik, menu-barratik eta tresna-barretatik Marraztu lerro fin bat menuetan eta menu-barretan fokua adierazteko Marraztu fokuaren adierazlea zerrendetan Marraztu markoa panel atrakagarrien inguruan Marraztu markoa orriaren titulu inguruan Marraztu markoa alboko panelen inguruan Marraztu graduatzaile-markak Marraztu tresna-barrako elementuen bereizleak Gaitu animazioak Gaitu tamaina aldatzeko helduleku luzatuak Markoak Orokorra Botoirik ez Botoi bat Labaintzeko-barrak Erakutsi teklatuko azeleratzaileak beharrezkoa denean Bi botoi Le&ihoen arrastatzeko modua: 