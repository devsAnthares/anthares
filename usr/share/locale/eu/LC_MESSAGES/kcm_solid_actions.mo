��    +      t  ;   �      �     �     �     �     �     	  *     *   ;     f     s  �   �  	   �     �     �     �     �     �  <   �            A     P  "   X     {     �     �  r   �  ;   $  
   `     k     �     �     �     �     �     �  #   �  )        8  !   T  &   v  $   �  _   �     "	  �  .	     �
  $        &     7  	   I  /   S  4   �     �     �    �  
   �                     4     O  C   [  #   �     �  
   �  #   �       '        4  u   G  :   �  
   �  @        D     ]     m     �     �     �  0   �  .   �     !      A  ,   b  5   �  e   �     +               $         &            !                                         '      (   #              *          
          %                  )                 	      "                                +    (c) 2009, Ben Cooksley Action icon, click to change it Action name Action name: Add... All of the contained properties must match Any of the contained properties must match Ben Cooksley Cannot be deleted Command that will trigger the action
This can include any or all of the following case insensitive expands:

%f: The mountpoint for the device - Storage Access devices only
%d: Path to the device node - Block devices only
%i: The identifier of the device Command:  Contains Content Conjunction Content Disjunction Device Interface Match Device type: Devices must match the following parameters for this action: EMAIL OF TRANSLATORSYour emails Edit Parameter Edit... Enter the name for your new action Equals Error Parsing Device Conditions Invalid action It appears that the action name, command, icon or condition are not valid.
Therefore, changes will not be applied. It appears that the predicate for this action is not valid. Maintainer NAME OF TRANSLATORSYour names No Action Selected Parameter type: Property Match Remove Reset Parameter Save Parameter Changes Solid Action Desktop File Generator Solid Device Actions Control Panel Module Solid Device Actions Editor The device must be of the type %1 The device property %1 must contain %2 The device property %1 must equal %2 Tool to automatically generate Desktop Files from Solid DeviceInterface classes for translation Value name: Project-Id-Version: kcm_solid_actions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-02-08 04:00+0100
PO-Revision-Date: 2014-02-09 19:07+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=(n != 1);
 (c) 2009, Ben Cooksley Ekintzaren ikonoa, klikatu aldatzeko Ekintzaren izena Ekintzaren izena: Gehitu... Dituen propietate guztiek bat etorri behar dute Dituen propietateetako edozeinek bat etorri behar du Ben Cooksley Ezin da ezabatu Ekintza abiaraziko duen komandoa
Honek maiuskulak/minuskulak bereizten ez dituzten hedapen hauetako bat edo guztiak izan ditzake:

%f: Gailua muntatzeko puntua - Biltegia atzitzeko gailuak soilik
%d: Gailuaren nodora daraman bidea - Blokeko gailuak soilik
%i: Gailuaren identifikatzailea Komandoa:  Hau du: Edukiaren konjuntzioa Edukiaren disjuntzioa Gailu-interfazea bat dator Gailu mota: Gailuek bat etorri behar dute parametro hauekin ekintza honetarako: xalba@euskalnet.net,hizpol@ej-gv.es Editatu parametroa Editatu... Idatzi ekintza berriarentzako izena Berdin Errorea gailuaren baldintzak aztertzean Ekintza baliogabea Badirudi ekintzaren izena, komandoa, ikonoa edo baldintza baliogabeak direla.
Horregatik, aldaketak ez dira ezarriko. Dirudienez, ekintza honetarako predikatua ez da baliozkoa. Arduraduna Iñigo Salvador Azurmendi,Hizkuntza Politikarako Sailburuordetza Ez da ekintzarik hautatu Parametro mota: Propietateen bat-etortzea Kendu Berrezarri parametroa Gorde parametroaren aldaketak Solid ekintzen mahaigaineko fitxategi-sortzailea Solid gailuen ekintzen aginte-paneleko modulua Solid gailuen ekintzen editorea Gailuak %1 motakoa izan behar du %1 gailu-propietateak hau eduki behar du: %2 %1 gailu-propietateak honen berdina izan behar du: %2 Itzulpenerako Solid DeviceInterface klaseetatik mahaigaineko fitxategiak automatikoki sortzeko tresna Balioaren izena: 