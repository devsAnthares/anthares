��          �   %   �      P  �   Q  �     �   �     G     O     S     `     h     n     u     y     �     �     �     �     �  	   �     �     �     �       	          	        &  y  /  �   �  �   �  �   b     	     	     !	     /	     7	     =	     F	     J	  	   `	     j	     �	     �	     �	     �	     �	  !   �	     �	     �	     �	  
   
     
  	   &
                                                                                            	                             
    <qt>Cannot start <i>gpg</i> and check the validity of the file. Make sure that <i>gpg</i> is installed, otherwise verification of downloaded resources will not be possible.</qt> <qt>Cannot start <i>gpg</i> and retrieve the available keys. Make sure that <i>gpg</i> is installed, otherwise verification of downloaded resources will not be possible.</qt> <qt>Cannot start <i>gpg</i> and sign the file. Make sure that <i>gpg</i> is installed, otherwise signing of the resources will not be possible.</qt> Author: BSD Description: Details Error Finish GPL Get Hot New Stuff Install Key used for signing: LGPL License: Opposite to BackNext Password: Reset Select Signing Key Share Hot New Stuff Title: Uninstall Update Username: Version: Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-12 03:00+0100
PO-Revision-Date: 2008-08-06 00:07+0200
Last-Translator: 
Language-Team: Occitan <oc@li.org>
Language: oc
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: KBabel 1.11.4
 <qt>Impossible d'amodar <i>gpg</i> per verificar la validitat d'aqueste fichièr. Asseguratz-vos que lo programa <i>gpg</i> es installat, siquenon la verificacion de las ressorsas telecargadas serà pas possibla.</qt> <qt>Impossible d'amodar <i>gpg</i> per i recuperar la tièra de las claus disponiblas. Asseguratz-vos que lo programa <i>gpg</i> es installat, siquenon la verificacion de las ressorsas telecargadas serà pas possibla.</qt> <qt>Impossible d'amodar <i>gpg</i> per signar lo fichièr. Asseguratz-vos que lo programa <i>gpg</i> es installat, siquenon la signatura de las ressorsas serà pas possibla.</qt> Autor : BSD Descripcion : Detalhs Error Terminar GPL Recuperar de novetats Installar Clau utilizada per signar : LGPL Licéncia : Seguent Mot de pas : Reinicializar Cauisissètz la clau de signatura Partejar de novetats Títol: Desinstallar Actualizar Nom d'utilizaire : Version : 