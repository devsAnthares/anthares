��          D      l       �      �      �   �  �      ;  �  X     	  #     �  2      �                           sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' Enable &taskbar notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2002-10-24 12:08SAST
Last-Translator: Lwandle Mgidlana <lwandle@translate.org.za>
Language-Team: Xhosa <xhosa@translate.org.za>
Language: xh
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.0beta2
Plural-Forms: nplurals=2; plural=n != 1;
  sec &Qala isalathiso sexesha lokuphuma: <H1>Isaziso sebar yomsebenzi</H1>
Ungenza indlela yesibini yesiqalo sesaziso esiza
kusetyenziswa yibar yomsebenzi apho iqhosha elinediski ejikelezayo livela,
libonisa ukuba isicelo sakho esiqaliweyo siyalayisha.
Inokwenzeka, ukuba ezinye izicelo azisilindelanga isiqalo
sesaziso. Kulo mzekelo, iqhosha liyalahleka emva kwexesha 
elinikiweyo kwicandelo 'Isiqalo sesalathiso sexesha lokuphuma' Yenza isaziso se bar y&omsebenzi 