��          �      <      �     �     �     �     �     �  '   �  >        L     f     �     �     �  )   �  7   �  '        B     T  L  s     �  
   �     �     �          ,     C  (   U  5   ~  ;   �  $   �       B   4  !   w  	   �  .   �  ,   �                                       
                      	                                        Current user General Layout Lock Screen New Session Nobody logged in on that sessionUnused Show a dialog with options to logout/shutdown/restartLeave... Show both avatar and name Show full name (if available) Show login username Show only avatar Show only name Show technical information about sessions User logged in on console (X display number)on %1 (%2) User logged in on console numberTTY %1 User name display You are logged in as <b>%1</b> Project-Id-Version: plasma_applet_org.kde.plasma.userswitcher
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-11 03:18+0100
PO-Revision-Date: 2016-04-15 23:42+0200
Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>
Language-Team: Serbian <kde-i18n-sr@kde.org>
Language: sr@ijekavian
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
 Текући корисник Опште Распоред Закључај екран Нова сесија некоришћено Напусти... Прикажи и аватар и име Прикажи цело име (ако је дато) Прикажи пријавно корисничко име Прикажи само аватар Прикажи само име Прикажи техничке податке о сесијама на %1 %2|/|на $[лок %1] %2 ТТИ %1 Приказ корисничког имена Пријављени сте као <b>%1</b> 