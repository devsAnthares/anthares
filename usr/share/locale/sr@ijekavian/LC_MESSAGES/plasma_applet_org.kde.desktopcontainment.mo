��    `        �         (     )     1     B     Q     W     ^     j     {     �     �  /   �  -   �     �     �  
   		  
   	     	     ,	     1	     8	     @	     R	     _	     d	  
   l	     w	     �	     �	     �	  	   �	     �	  	   �	     �	     �	     �	     
     
  	   #
     -
     4
     H
  	   M
     W
     ]
     c
     h
     m
  	   v
     �
     �
     �
     �
     �
     �
     �
     �
  <   �
               /     6     =     X     ^     e     j  
   ~     �     �     �     �     �  )   �               5     :     @     M     U     ]     f  
   x     �     �  	   �     �     �     �     �     �     �     �  E   �  *   A  K  l     �     �     �                    1     J     e     �     �     �     �     �     �     �       
   !     ,     =     N     f  
   �     �     �     �     �     �  3   �     .  %   D     j     |     �  2   �     �     �          #  0   2     c     r     �     �     �     �     �     �      �            '   ,     T     [     j     v  s   �  &     "   .     Q     ^  
   ~  
   �     �     �  #   �     �     �       $   "  2   G     z  [   �  ,   �  ,        >     O     X     a     u     �      �     �     �     �     �             )        D     Y  #   w     �  �   �     <     J       -      M       %   Q           ^              U       L   S   $      .       H       X          Y   8   *         <   &          '       ]   N       3                     `          #   V             /   T   G   9   @   >   =   6   2   O   C   F   ?      0      
      A   E       	      P              _                D          Z               1   R   ,   +                \           I   )   B   K      :   !   W   4          ;               (      5   "       7       [           &Delete &Empty Trash Bin &Move to Trash &Open &Paste &Properties &Refresh Desktop &Refresh View &Reload &Rename @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Align Appearance: Arrange In Arrange in Arrangement: Back Cancel Columns Configure Desktop Custom title Date Default Descending Description Deselect All Desktop Layout Enter custom title here Features: File name pattern: File type File types: Filter Folder preview popups Folders First Folders first Full path Got it Hide Files Matching Huge Icon Size Icons Large Left List Location Location: Lock in place Locked Medium More Preview Options... Name None OK Panel button: Press and hold widgets to move them and reveal their handles Preview Plugins Preview thumbnails Remove Resize Restore from trashRestore Right Rotate Rows Search file type... Select All Select Folder Selection markers Show All Files Show Files Matching Show a place: Show files linked to the current activity Show the Desktop folder Show the desktop toolbox Size Small Small Medium Sort By Sort by Sorting: Specify a folder: Text lines Tiny Title: Tool tips Tweaks Type Type a path or a URL here Unsorted Use a custom icon Widget Handling Widgets unlocked You can press and hold widgets to move them and reveal their handles. whether to use icon or list viewView mode Project-Id-Version: plasma_applet_org.kde.desktopcontainment
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:11+0100
PO-Revision-Date: 2017-12-15 12:38+0100
Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>
Language-Team: Serbian <kde-i18n-sr@kde.org>
Language: sr@ijekavian
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
 &Обриши &Испразни смеће Премести у &смеће &Отвори &Налепи &Својства &Освежи површ &Освежи приказ &Учитај поново &Преименуј Изабери... Очисти иконицу Поравнај Изглед: Распореди по Распоред по Распоред: Назад Одустани колонама Подеси површ посебан наслов датум подразумеван Опадајуће опис Поништи избор Распоред површи Унесите овде посебан наслов Могућности: Образац имена фајла: тип фајла Типови фајлова: Филтер Искакачи прегледа фасцикле прво фасцикле Прво фасцикле пуна путања Разумем Сакриј поклопљене фајлове огромне Величина иконица Иконице велике лијево Списак Локација Локација: Закључај на месту Закључано средње Још опција прегледа... име никакав У реду Панелско дугме: Притисните и држите виџете да их померате и откријете им ручке. Прикључци за преглед Сличице за преглед Уклони Промени величину Врати десно Окрени врстама Потражи тип фајла... Изабери све Избор фасцикле Маркери избора Прикажи све фајлове Прикажи поклопљене фајлове Мјесто: Прикажи фајлове повезана са тренутном активношћу Прикажи фасциклу површи Прикажи алатницу површи величина мале мале Поређај по Поређај по Ређање: Посебна фасцикла: Редови текста сићушне Наслов: Облачићи Штеловања тип Унесите путању или УРЛ непоређано Посебна иконица Управљање виџетима Виџети откључани Можете притиснути и држати виџете да их померате и откријете им ручке. Режим приказа 