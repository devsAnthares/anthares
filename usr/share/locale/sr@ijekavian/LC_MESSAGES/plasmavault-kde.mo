��    ?        Y         p  �  q  }  J  M  �        
   7     B     K     j     �  <   �     �     �  	   �     �  .     %   A     g     }     �     �     �     �     �     �     �     �  4        B  9   T     �     �     �  �   �  !   �  t   �     8     D     a     n     s  	   �     �  -   �     �  B   �  &     ?   B  '   �  )   �  7   �  .     5   ;  +   q     �     �     �  ,   �          -  9   :  V   t  C   �  2       B  �  ]  l  (  *   �!     �!     �!  B   �!  B   *"     m"  H   |"     �"     �"     �"     #  ]   .#  9   �#  %   �#     �#  '   �#  
   #$  (   .$  %   W$     }$  :   �$  
   �$  6   �$  T   	%  "   ^%  l   �%  %   �%     &  
   4&  w  ?&  <   �'  �   �'     �(  4   �(     )     $)  .   3)     b)     r)  L   �)     �)  {   �)  8   h*  d   �*  M   +  K   T+  p   �+  N   ,  ^   `,  I   �,  1   	-  3   ;-     o-  V   �-  '   �-     .  d   %.  �   �.     /     !      	   2   ?   
   /                 +   (   *       1            )   >           8          9   $              7             .       '   6          %   <             3   ,          ;       =   &                              -           #      "          :      5      0                                   4               <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body style=" font-family:'hlv'; font-size:9pt; font-weight:400; font-style:normal;">
<p style="-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><br /></p></body></html> <b>Security notice:</b>
                             According to a security audit by Taylor Hornby (Defuse Security),
                             the current implementation of Encfs is vulnerable or potentially vulnerable
                             to multiple types of attacks.
                             For example, an attacker with read/write access
                             to encrypted data might lower the decryption complexity
                             for subsequently encrypted data without this being noticed by a legitimate user,
                             or might use timing analysis to deduce information.
                             <br /><br />
                             This means that you should not synchronize
                             the encrypted data to a cloud storage service,
                             or use it in other circumstances where the attacker
                             can frequently access the encrypted data.
                             <br /><br />
                             See <a href='http://defuse.ca/audits/encfs.htm'>defuse.ca/audits/encfs.htm</a> for more information. <b>Security notice:</b>
                             CryFS encrypts your files, so you can safely store them anywhere.
                             It works well together with cloud services like Dropbox, iCloud, OneDrive and others.
                             <br /><br />
                             Unlike some other file-system overlay solutions,
                             it does not expose the directory structure,
                             the number of files nor the file sizes
                             through the encrypted data format.
                             <br /><br />
                             One important thing to note is that,
                             while CryFS is considered safe,
                             there is no independent security audit
                             which confirms this. @title:windowCreate a New Vault Activities Backend: Can not create the mount point Can not open an unknown vault. Change Choose the encryption system you want to use for this vault: Choose the used cypher: Close the vault Configure Configure Vault... Configured backend can not be instantiated: %1 Configured backend does not exist: %1 Correct version found Create Create a New Vault... CryFS Device is already open Device is not open Dialog Do not show this notice again EncFS Encrypted data location Failed to create directories, check your permissions Failed to execute Failed to fetch the list of applications using this vault Failed to open: %1 Forcefully close  General If you limit this vault only to certain activities, it will be shown in the applet only when you are in those activities. Furthermore, when you switch to an activity it should not be available in, it will automatically be closed. Limit to the selected activities: Mind that there is no way to recover a forgotten password. If you forget the password, your data is as good as gone. Mount point Mount point is not specified Mount point: Next Open with File Manager Password: Plasma Vault Please enter the password to open this vault: Previous The mount point directory is not empty, refusing to open the vault The specified backend is not available The vault configuration can only be changed while it is closed. The vault is unknown, can not close it. The vault is unknown, can not destroy it. This device is already registered. Can not recreate it. This directory already contains encrypted data Unable to close the vault, an application is using it Unable to close the vault, it is used by %1 Unable to detect the version Unable to perform the operation Unknown device Unknown error, unable to create the backend. Use the default cipher Vaul&t name: Wrong version installed. The required version is %1.%2.%3 You need to select empty directories for the encrypted storage and for the mount point formatting the message for a command, as in encfs: not found%1: %2 Project-Id-Version: plasmavault-kde
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-11 03:19+0100
PO-Revision-Date: 2017-12-17 18:01+0100
Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>
Language-Team: Serbian <kde-i18n-sr@kde.org>
Language: sr@ijekavian
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
 <html><p><br /></p></html> <p><b>Безбедносно обавештење:</b></p><p>Према безбедносној ревизији Тејлора Хорнбија (Дифјуз сикјурити), тренутна изведба енкФС‑а рањива је (или потенцијално рањива) на више врста напада. На пример, нападач са приступом за читање и писање над шифрованим подацима може да снизи сложеност дешифровања за накнадно шифроване податке, тако да то легитимни корисник не примети, или да извуче податке анализом тајминга.</p><p>Ово значи да не би требало да синхронизујете шифроване податке са сервисом складиштења у облаку, као ни у другим ситуацијама где би нападач могао да има чест приступ шифрованим подацима.</p><p>Погледајте <a href='http://defuse.ca/audits/encfs.htm'>defuse.ca/audits/encfs.htm</a> за више детаља.</p> <p><b>Безбедносно обавештење:</b></p><p>КриФС шифрује ваше фајлове тако да их можете безбедно складиштити било где. Добро ради са сервисима у облаку, попут Дропбокса, Ај‑клауда, Вандрајва и других.</p><p>За разлику од неких других решења на основу преклапања фајл система, ово не открива структуру фасцикли, број фајлова, нити њихове величине, кроз формат шифрованих података.</p><p>Имајте у виду да, иако се криФС сматра безбедним, нема независне безбедносне ревизије која би то потврдила.</p> Стварање новог трезора Активности Позадина: Не могу да направим тачку монтирања. Не могу да отворим непознати трезор. Промени Жељени систем шифровања за овај трезор: Избор шифрара: Затвори трезор Подешавање Подеси трезор... Не може да се направи примерак подешене позадине: %1 Подешена позадина не постоји: %1 Нађено добро издање. Направи Направи нови трезор... криФС Уређај је већ отворен. Уређај није отворен. Дијалог Не приказуј више ово обавештење енкФС Локација шифрованих података Не могу да правим фасцикле, проверите дозволе. Не могу да извршим. Не могу да добавим листу програма који користе овај трезор. Не могу да отворим: %1 Принудно затвори Опште Ако ограничите овај трезор само на одређене активности, биће приказан у аплету само док сте у тим активностима. Кад се пребаците у активност у којој не би требало да буде доступан, биће аутоматски затворен. Ограничи на изабране активности: Имајте у виду да заборављена лозинка не може да се поврати. Ако је заборавите, сви подаци су вам пропали. Тачка монтирања Тачка монтирања није задата. Тачка монтирања: Следеће Отвори менаџером фајлова Лозинка: Плазма трезор Унесите лозинку за отварање овог трезора: Претходно Фасцикла за тачку монтирања није празна, одбијам да отворим трезор. Задата позадина није доступна. Постава трезора може да се измени само док је затворен. Трезор је непознат, не могу да га затворим. Трезор је непознат, не могу да га уништим. Овај уређај је већ регистрован. Не могу поново да га направим. Ова фасцикла већ садржи шифроване податке. Не могу да затворим трезор, користи га неки програм. Не могу да затворим трезор, користи га %1. Не могу да откријем издање. Не могу да изведем поступак. Непознат уређај. Непозната грешка, не могу да направим позадину. подразумевани шифрар Име &трезора: Инсталирано погрешно издање. Неопходно је издање %1.%2.%3. И за шифровано складиште и за тачке монтирања морате да бирате празне фасцикле. %1: %2 