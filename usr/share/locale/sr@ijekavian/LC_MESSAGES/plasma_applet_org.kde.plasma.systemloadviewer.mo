��    &      L  5   |      P     Q     l     y     �     �     �     �     �     �     �     �     �  &   �               #     ,     3     ?     M     U     ]     d     s     �     �     �     �     �     �     �     �     �     �  
   �            P       k     m  !   }  
   �     �     �  	   �     �     �     �          %  9   ,     f     |     �     �     �     �  
   �     �     �     �     	     8	  	   O	  $   Y	     ~	     �	     �	     �	     �	  #   �	      
  (   0
     Y
  $   k
                           	      #      $                                                    &                                          !          %                            "   
    Abbreviation for secondss Application: Average clock: %1 MHz Bar Buffers: CPU CPU %1 CPU monitor CPU%1: %2% @ %3 Mhz CPU: %1% CPUs separately Cache Cache Dirty, Writeback: %1 MiB, %2 MiB Cache monitor Cached: Circular Colors Compact Bar Dirty memory: General IOWait: Memory Memory monitor Memory: %1/%2 MiB Monitor type: Nice: Set colors manually Show: Swap Swap monitor Swap: %1/%2 MiB Sys: System load Update interval: Used swap: User: Writeback memory: Project-Id-Version: plasma_applet_org.kde.plasma.systemloadviewer
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-29 03:31+0200
PO-Revision-Date: 2017-03-12 22:21+0100
Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>
Language-Team: Serbian <kde-i18n-sr@kde.org>
Language: sr@ijekavian
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
 s Програм: Просечан такт: %1 MHz траке Бафери: ЦПУ ЦПУ %1 Надзор ЦПУ‑а ЦПУ %1: %2% @ %3 MHz ЦПУ: %1% ЦПУ‑ови засебно Кеш Кеша прљаво, изауписно: %1 MiB, %2 MiB Надзор кеша Кеширано: кружно Боје сажета трака Прљава меморија: Опште У/И: Меморија Надзор меморије Меморија: %1/%2 MiB Тип надзора: Фино: Ручно задавање боја Приказано: Размјена Надзор размјене Размјена: %1/%2 MiB Систем: Оптерећење система Период ажурирања: Искоришћена размјена: Корисник: Изауписна меморија: 