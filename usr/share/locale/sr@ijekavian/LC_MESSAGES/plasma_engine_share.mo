��    
      l      �       �   $   �   %     :   <     w  '   �  -   �     �       '     6  <  @   s  H   �  Y   �  D   W  >   �  7   �  &        :  ;   Z         	                            
        Could not detect the file's mimetype Could not find all required functions Could not find the provider with the specified destination Error trying to execute script Invalid path for the requested provider It was not possible to read the selected file Service was not available Unknown Error You must specify a URL for this service Project-Id-Version: plasma_engine_share
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-12-05 01:23+0100
Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>
Language-Team: Serbian <kde-i18n-sr@kde.org>
Language: sr@ijekavian
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
 Не могу да препознам МИМЕ тип фајла Не могу да нађем све неопходне функције Не могу да нађем добављача са задатим одредиштем Грешка при покушају извршења скрипте Лоша путања за траженог добављача Не могу да читам изабрани фајл Сервис није доступан Непозната грешка Морате задати УРЛ за овај сервис 