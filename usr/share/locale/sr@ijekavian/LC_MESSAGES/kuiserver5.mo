��          �   %   �      P     Q     b     w     �     �     �  
   �     �     �     �     �     �  /        3     Q     p     v     �     �     �     �     �     �     �       ,  %  7   R  O   �     �  '   �          2     R     _     p     �     �     �  H   �  2     4   :     o     �     �     �  %   �  5   �  %   	  5   5	  #   k	  3   �	                            
                                                                                             	    %1 file %1 files %1 folder %1 folders %1 of %2 processed %1 of %2 processed at %3/s %1 processed %1 processed at %2/s Appearance Behavior Cancel Clear Configure... Finished Jobs List of running file transfers/jobs (kuiserver) Move them to a different list Move them to a different list. Pause Remove them Remove them. Resume Show all jobs in a list Show all jobs in a list. Show all jobs in a tree Show all jobs in a tree. Show separate windows Show separate windows. Project-Id-Version: kuiserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-06-30 14:15+0200
Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>
Language-Team: Serbian <kde-i18n-sr@kde.org>
Language: sr@ijekavian
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
 %1 фајл %1 фајла %1 фајлова %1 фајл %1 фасцикла %1 фасцикле %1 фасцикли %1 фасцикла Обрађено %1 од %2 %1 од %2 обрађено при %3/s Обрађено %1 Обрађено %1 при %2/s Изглед Понашање Одустани Очисти Подеси... Завршени послови Списак преноса фајлова и послова у току Премјести их у други списак Премијештање у други списак. Паузирај Уклони их Уклањање. Настави Сви послови у списку Прикажи све послове у списку. Сви послови у стаблу Прикажи све послове у стаблу. Раздвојени прозори Прикажи раздвојене прозоре. 