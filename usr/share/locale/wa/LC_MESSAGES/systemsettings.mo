��    *      l  ;   �      �  A   �     �  /        2     ;     O     a     w     �     �  	   �     �  3   �  	   �     �      �  $      *   E     p  	   u  5        �     �  
   �     �          $  5   3  6   i     �  ,   �  /   �     	        O   0  Z   �  b   �  	   >  
   H  P   S     �  �  �  @   ^
     �
  9   �
     �
     �
       %   <     b     |     �     �     �  4   �  	   �     �  A   �  .   A     p     y     �  <   �  !   �     �     
       ,   !     N  6   d  P   �     �  :   �     3     :     Y  Y   r  ]   �  l   *     �     �  o   �     (            )             &      $   	   %         '                                                !      
       *                     "                                                 (                 #    %1 is an external application and has been automatically launched (c) 2009, Ben Cooksley <i>Contains 1 item</i> <i>Contains %1 items</i> About %1 About Active Module About Active View About System Settings Apply Settings Author Ben Cooksley Configure Configure your system Determines whether detailed tooltips should be used Developer Dialog EMAIL OF TRANSLATORSYour emails Expand the first level automatically General config for System SettingsGeneral Help Icon View Internal module representation, internal module model Internal name for the view used Keyboard Shortcut: %1 Maintainer Mathias Soeken NAME OF TRANSLATORSYour names No views found Provides a categorized icons view of control modules. Provides a classic tree-based view of control modules. Relaunch %1 Reset all current changes to previous values Search through a list of control modulesSearch Show detailed tooltips System Settings System Settings was unable to find any views, and hence has nothing to display. System Settings was unable to find any views, and hence nothing is available to configure. The settings of the current module have changed.
Do you want to apply the changes or discard them? Tree View View Style Welcome to "System Settings", a central place to configure your computer system. Will Stephenson Project-Id-Version: systemsettings
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:08+0100
PO-Revision-Date: 2010-05-11 17:39+0200
Last-Translator: Jean Cayron <jean.cayron@gmail.com>
Language-Team: Walloon <linux-wa@walon.org>
Language: wa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=2; plural=n != 1;
 %1 est on dfoûtrin programe eyet il a stî enondé otomaticmint © 2009, Ben Cooksley <i>I gn a dvins 1 cayet</i> <i>I gn a dvins %1 cayets</i> Åd fwait di %1 Åd fwait do module en alaedje Åd fwait del vuwe en alaedje Åd fwait d' Apontiaedjes do sistinme Mete apontiaedjes en ouve Oteur Ben Cooksley Apontyî Apontyî vosse sistinme Dit s' on s' doet siervi des sipepieusès racsegnes Programeu Purnea di dvize jean.cayron@gmail.com,laurent.hendschel@skynet.be,pablo@walon.org Totafwait mostrer otomaticmint l' prumî livea Djenerå Aidance Vuwe èn imådjetes Riprezintaedje do dvintrin module, modele do dvintrin module Divintrin no pol vuwe d' eployeye Rascourti taprece: %1 Mintneu Mathias Soeken Jean Cayron,Lorint Hendschel,Pablo Saratxaga Nole vuwe di trovêye Dene ene vuwe ène imådjetes des modules di controle. Dene ene vuwe classike des modules di controle båzé soz on coxhlaedje d' åbe. Renonder %1 Rimete tos les candjmints do moumint come il estént dvant Trover Mostrer sipepieusès racsegnes Apontiaedjes do sistinme Apontiaedjes do sistinme n' a nén sepou trover d' vuwe et d' abôrd n' a rén a håyner. Apontiaedjes do sistinme n' a nén sepou trover d' vuwe et d' abôrd i gn a rén az apontyî. Les apontiaedjes do module do moumint ont candjî.
Voloz vs mete en ouve les candjmint oudonbén les rovyî? Vuwe e coxhlaedje Stîle di vuwe Wilicome ås « Apontiaedjes do sistinme », li plaece å mitan pos apontyî l' sistinme di vosse copiutrece. Will Stephenson 