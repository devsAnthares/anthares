��          �            h  &   i  +   �     �  	   �     �     �     �     �     �  (        7  ,   N     {     �  �  �     X  $   x     �  	   �  $   �     �     �     �     �  ,        5  .   R     �     �                                                                  	   
           Do you want to suspend to RAM (sleep)? Do you want to suspend to disk (hibernate)? General Hibernate Hibernate (suspend to disk) Leave Leave... Lock Lock the screen Logout, turn off or restart the computer Sleep (suspend to RAM) Start a parallel session as a different user Suspend Switch user Project-Id-Version: plasma_applet_lockout
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2011-01-05 18:58+0100
Last-Translator: Jean Cayron <jean.cayron@gmail.com>
Language-Team: Walloon <linux@walon.org>
Language: wa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-Language: Walloon
X-Generator: Lokalize 1.0
 Voloz vs mete doirmi sol RAM ? Voloz vs mete fordoirmi sol plake ? Djenerå Fordoirmi Fordoirmi (mete fordoirmi sol plake) End aler End aler... Eclawer Eclawer l' waitroûle Dislodjî, arester ou renonder l' copiutrece Doirmi (mete doirmi sol RAM) Enonder ene session paralele come on ôte uzeu Mete doirmi Discandjî d' uzeu 