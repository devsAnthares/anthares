��    	      d      �       �      �      �           !     >     V  ,   r  c   �  �    #   �      �  	   �     �       &   1  4   X  �   �                                    	          Also allow remote connections Halt Without Confirmation Log Out Log Out Without Confirmation Logout canceled by '%1' Reboot Without Confirmation Restores the saved user session if available The reliable KDE session manager that talks the standard X11R6 
session management protocol (XSMP). Project-Id-Version: ksmserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-05 03:18+0100
PO-Revision-Date: 2011-01-05 18:51+0100
Last-Translator: Jean Cayron <jean.cayron@gmail.com>
Language-Team: Walloon <linux@walon.org>
Language: wa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.0
 Permete les raloyaedjes då lon eto Clôre totoute sins acertinaedje Dislodjî Si dislodjî sins acertinaedje Dislodjaedje rinoncî pa '%1' Renonder l' sistinme sins acertinaedje Rapexhî les sessions di l' uzeu schapêyes, si nd a Li manaedjeu del session di KDE k' on pout conter dssus eyet ki cåze li
protocole sitandård X11R6 di manaedjmint des session (XSMP). 