��    v      �  �   |      �	     �	     �	     
     
     
  
   $
     /
     1
     8
     F
     X
     f
     v
     �
     �
     �
  
   �
     �
  *   �
     �
                    &      /     P  "   c     �     �  
   �     �  !   �     �     �     �     �                 !   #  ?   E      �     �     �     �     �     �     �     �  /   �  3   .     b     |     �  	   �     �     �     �     �  	   �     �     �     	                      A     S  
   d     o     �     �  *   �     �     �     �       
   '  	   2     <     S     Z     l     |     �     �     �     �     �     �     �  '        +  S   0  _   �  �   �     �     �     �     �  T   �     �               "     .     ;     J     X     j     |     �     �     �     �     �     �  �  �     �  	   �  
   �     �  	   �     �     �  	   �     �     �          $     :     ?     U     X     d     r  0   y     �     �     �     �     �     �               %     1  
   4     ?      G     h  	   v     �  
   �     �     �     �  -   �     �     �            
   &     1     E     X     g  
   y     �  -   �  6   �     �                !     *     ;  
   >     I  !   f     �     �     �  4   �     �            $   %  '   J     r  8   �     �     �     �                     -     H     O     [     ^     r     �     �     �     �     �     �  <         =  o   C  t   �  �   (     �  	   �     �     �  w   �     s     �     �     �     �     �     �     �          %     D     V     g     y  
   �     �     %   :       P   =   c   M   J               #   ?       k   4   0            n   /          E   *             $   !       ]   r         v   9   ^      -   )   O   j       @   G          \       [   F       t       >   h   `          S   Q              &   L       +   6                 1   o   5   H          B   "   T   R       a           V   C       .   b   s   A   q      <   	   f   ,   i   U   7   8   u   Y          I   (   Z       
      m         _                     3       ;   g   '      2   e       N       l                               W       X           p   D   d          K    &Autodetect &Delete &Duplicate... &Edit... &New... &Shortcut: 1 Action Action window Activate window:  Active window Active window:  Add a new conditionAnd Allow Merging And_conditionAnd Application: Arguments: Call Change the exported state for the actions. Command/URL Command/URL :  Command/URL: Comment Comment: Condition typeActive Window ... Condition typeAnd Condition typeExisting Window ... Condition typeNot Condition typeOr Conditions Contains Copyright 2008 (c) Michael Jansen D-Bus Command D-Bus:  Delete Desktop Dialog Dock Does Not Contain Does Not Match Regular Expression Don't change the state of exported hotkey actions.Actual State EMAIL OF TRANSLATORSYour emails Edit Edit Gesture Edit... Existing window:  Export Actions Export Group Export Group... Export hotkey actions in enabled state.Enabled Export hotkey actions into disabled  stateDisabled Failed to run qdbusviewer Failed to start service '%1'. Filename Function: Gesture trigger Gestures Global Shortcut Id Import... Input Action: %1 Input Actions Daemon Is Is Not K-Menu Entry KDE Hotkeys Configuration Module KHotkeys file id. Keyboard input:  Maintainer Matches Regular Expression Menu Editor entries Menu entry:  Merge into existing directories on import? Michael Jansen Mouse Gesture Action Mouse button: NAME OF TRANSLATORSYour names New Action New Group No service configured. Normal Not_conditionNot Or_conditionOr Remote application: Remote object: Save changes Select Application ... Send Keyboard Input Settings Shortcut trigger:  Specific window Start the Input Actions daemon on login Test The current action has unsaved changes. If you continue these changes will be lost. This "actions" file has already been imported before. Are you sure you want to import it again? This "actions" file has no ImportId field and therefore it cannot be determined whether or not it has been imported already. Are you sure you want to import it? Timeout: Trigger Trigger When Type Unable to contact khotkeys. Your changes are saved, but they could not be activated. Voice trigger:  Window Window Action Window Data Window Types Window appears Window class: Window disappears Window gets focus Window loses focus Window role: Window simple:  Window title: Window trigger:  action enabledEnabled action nameName Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:42+0200
PO-Revision-Date: 2010-01-22 12:00+0100
Last-Translator: Jean Cayron <jean.cayron@gmail.com>
Language-Team: Walloon <linux-wa@walon.org>
Language: wa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=2; plural=n != 1;
 Deteccion &otomatike &Disfacer &Dobler... &Candjî... &Novea... &Rascourti : 1 Faitindje Purnea do faitindje Disperter purnea :  Purnea en alaedje Purnea en alaedje :  Eyet Permete li rmaxhaedje Et Programe : Årgumints : Houkî Candjî l' estat d' ebagaedje po les faitindjes. Comande/URL Comande/URL :  Comande/URL : Rawete di tecse Rawete di tecse : Purnea en alaedje ... Et Purnea egzistant ... N' est nén Ou Condicions A dvins Copyright 2008 © Michael Jansen Comande D-Bus D-Bus :  Disfacer Sicribanne Divize Dock (sicriftôr) N' a nén dvins Èn corespond nén a l' erîlêye ratourneure Vraiy estat Jean.cayron@gmail.com Candjî Candjî movmint Candjî... Purnea egzistant :  Ebaguer faitindjes Ebaguer groupe Ebaguer groupe... En alaedje Essocté L' enondaedje di qdbusviewer a fwait berwete L' enondaedje do siervice « %1 » a fwait berwete. No do fitchî Fonccion : Clitchete pa movmint Movmints Rascourti globå Id Abaguer... Faitindje èn intrêye : %1 Démon des faitindjes en intrêye Est N' est nén Intrêye di K-Dressêye Module d' apontiaedje di KDE po les Tchôdès tapes Id. do fitchî KTchôdèsTapes Intrêye del taprece :  Mintneu Corespond a l' erîlêye ratourneure Intrêyes di l' aspougneu del dressêye Intrêye el dressêye :  Rimaxhî dins des ridants egzitants oudonbén abaguer ? Michael Jansen Faitindje do movmint del sori Boton del sori : Djan Cayron Novea faitindje Novea groupe Nou siervice d' apontyî. Normå N' est nén Ou Programe då lon : Objet då lon : Schaper candjmints Tchoezi programe ... Evoyî intrêye del taprece Apontiaedjes Clitchete pa rascourti :  Purnea specifike Enonder l' démon des Faitindjes en intrêye a l' elodjaedje Sayî Li faitindje do moumint a des candjmints nén schapés. Si vs tcheryîz pus lon, ces candjmints sront pierdous. Ci fitchî d' « faitindjes » a ddja stî abaguer dins l' tins. Estoz vs seur di voleur l' abaguer cor on côp ? Ci fitchî d' « faitindjes » n' a pont d' tchamp ImportId ey adon dji n' sai nén dire s' il a ddja stî abagué ou nonna.  Estoz vs seur di voleur l' abaguer ? Astådje : Clitchete Clitchete cwand Sôre Dji n' sai nén contacter ktchôdèstapes. Vos candjmints sont schapés mins i n' pôrént nén esse metous en alaedje. Clitchete pal vwès :  Purnea Faitindje do purnea Dinêyes do purnea Sôres di purnea On purnea aparexhe Classe do purnea : On purnea disparexhe On purnea prind l' prumî plan On purnea piede li prumî plan Role do purnea : Simpe purnea :  Tite do purnea : Clitchete pa purnea :  En alaedje No 