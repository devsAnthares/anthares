��            )   �      �  &   �     �     �     �     �      �               .     6  ,   S  3   �     �     �     �     �     �  '        4     S     Y     o     �     �     �     �     �  &   �     �  �  �     �     �     �     �     �  A   �     >     F  
   e     p  :   �  @   �  0     0   7     h  
   q     |  (   �  ,   �     �  *   �  ,   	     :	     A	     X	  +   `	     �	  "   �	     �	                                       
                      	                                                                            @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2012-07-04 17:33+0200
Last-Translator: Jean Cayron <jean.cayron@base.be>
Language-Team: Walloon <linux-wa@walon.org>
Language: wa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-Language: Walloon
 Djenerå Radjouter novea scripe. Radjouter... Rinoncî ? Rawete: laurent.hendschel@skynet.be,pablo@walon.org,jean.cayron@gmail.com Candjî Candjî l' sicripe di tchoezi. Candjî... Enonde l' scripe tchoezi. Nén possibe d' ahiver on scripe po l' eterpreteu « %1 » Nén possibe di trover l' eterpreteu pol fitchî scripe « %1 » Nén possibe di tcherdjî l' eterpreteu « %1 » Nén possibe di drovi l' fitchî scripe « %1 » Fitchî: Imådjete: Eterpreteu: Livea d' såvrité di l' eterpreteu Ruby Lorint Hendschel,Pablo Saratxaga,Djan Cayron No: Cisse fonccion « %1 » n' egzistêye nén Cist eterpreteu n' egzistêye nén: « %1 » Oister Oister scripe tchoezi. Enonder Fitchî scripe « %1 » n' egzistêye nén. Djoker Arestêye l' enondaedje do scripe. Tecse 