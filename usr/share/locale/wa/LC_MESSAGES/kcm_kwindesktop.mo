��    !      $  /   ,      �     �     �  
   o  +   z  
   �     �     �     �      �               7  	   @      J  *   k  H   �     �     �       )     ;   <  	   x     �  (   �     �     �     �          7     L     c  	   ~  �  �     0  �   6     �  ;   �     	     	      %	     F	  ;   Y	  #   �	  3   �	     �	     �	     
  *   
  U   C
     �
     �
     �
  /   �
  O   �
  
   A  4   L  7   �  $   �  #   �  #     "   &     I     ^     x  	   �                         	                                                                                  
                !                                     msec <h1>Multiple Desktops</h1>In this module, you can configure how many virtual desktops you want and how these should be labeled. Animation: Assigned global Shortcut "%1" to Desktop %2 Desktop %1 Desktop %1: Desktop Effect Animation Desktop Names Desktop Switch On-Screen Display Desktop Switching Desktop navigation wraps around Desktops Duration: EMAIL OF TRANSLATORSYour emails Here you can enter the name for desktop %1 Here you can set how many virtual desktops you want on your KDE desktop. Layout NAME OF TRANSLATORSYour names No Animation No suitable Shortcut for Desktop %1 found Shortcut conflict: Could not set Shortcut %1 for Desktop %2 Shortcuts Show desktop layout indicators Show shortcuts for all possible desktops Switch One Desktop Down Switch One Desktop Up Switch One Desktop to the Left Switch One Desktop to the Right Switch to Desktop %1 Switch to Next Desktop Switch to Previous Desktop Switching Project-Id-Version: kcm_kwindesktop
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-06 05:35+0100
PO-Revision-Date: 2010-10-22 18:27+0200
Last-Translator: Jean Cayron <jean.cayron@gmail.com>
Language-Team: Walloon <linux@walon.org>
Language: wa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=2; plural=n != 1;
  mseg <h1>Sacwants scribannes</h1>Divins c' module vos ploz apontyî cobén d' forveyous scribannes vos vloz eyet comint i dvrént esse lomés. Animåcion: Dj' a aroyî l' rascourti globå « %1 » å scribanne %2 Sicribanne %1 Sicribanne %1: Animåcion d' efet do scribanne Nos des scribannes Håynaedje sol waitroûle (OSD) eyet passaedje di scribanne Passaedje d' on scribanne a l' ôte Li naiviaedje emey les scribannes rivént å cmince Sicribannes Durêye: jean.cayron@gmail.com Vos ploz chal intrer l' no do scribanne %1 Vos ploz chal defini cobén d' forveyous scribannes vos vloz so vosse sicribanne KDE. Adjinçmint Djan Cayron Nole animåcion Dji n' a trové nou rascourti pol sicribanne %1 Afontmint di rascourti: dji n' a sepou defini l' rascourti %1 pol sicribanne %2 Rascourtis Mostrer les indicateurs d' adjinçmint d' sicribanne Mostrer les rascourtis po tos les scribannes k' on sait Aler on scribanne pus lon al valêye Aler on scribanne pus lon al copete Aler on scribanne pus lon a hintche Aler on scribanne pus lon a droete Aler å scribanne %1 Aler å scribanne shuvant Aler å scribanne di dvant Passaedje 