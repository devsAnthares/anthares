��          �            x  
   y  	   �     �     �  3   �     �     �                    %     *     F  B   Y     �  �  �  	   �     �     �     �  8   �       %   '     M     a  	   v     �  $   �     �     �  !   �     	                                                  
                        Appearance Browse... Choose an image Fifteen Puzzle Image Files (*.png *.jpg *.jpeg *.bmp *.svg *.svgz) Number color Path to custom image Piece color Show numerals Shuffle Size Solve by arranging in order Solved! Try again. The time since the puzzle started, in minutes and secondsTime: %1 Use custom image Project-Id-Version: plasma_applet_fifteenPuzzle
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-01-12 03:59+0100
PO-Revision-Date: 2017-06-28 17:06+0100
Last-Translator: Vincent Pinon <vpinon@kde.org>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Apparence Parcourir… Choisir une image Jeu de taquin Fichiers d'image (*.png *.jpg *.jpeg *.bmp *.svg *.svgz) Couleur des nombres Emplacement de l'image personnalisée Couleur des pièces Afficher des nombres Mélanger Taille Résoudre en replaçant dans l'ordre Résolu ! Recommencez. Temps : %1 Utiliser une image personnalisée 