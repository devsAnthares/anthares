��    $      <  5   \      0     1  +   E     q  4   �  &   �     �     �                     5     :     P     X  ,   u  3   �     �     �               !  '   .     V     u     {     �     �     �     �     �     �  &   �       B        b  �  y     _     f     �     �  	   �     �  
   �  
   �     �  +   �      	  !   )	     K	  "   W	  E   z	  V   �	  7   
  :   O
  
   �
  	   �
     �
  ,   �
  2   �
       (     *   @  	   k  "   u  	   �  -   �     �  .   �       ?        Q                                       
                              !                                  $      #                               	                           "             @info:creditAuthor @info:creditCopyright 2006 Sebastian Sauer @info:creditSebastian Sauer @info:shell command-line argumentThe script to run. @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: application descriptionCommand-line utility to run Kross scripts. application nameKross Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2014-05-21 16:00+0200
Last-Translator: Sebastien Renard <renard@kde.org>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 1.5
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Auteur Copyright 2006 Sebastian Sauer Sebastian Sauer Script à exécuter. Général Ajouter un nouveau script. Ajouter... Annuler ? Commentaire : jcorn@free.fr,kde@macolu.org,renard@kde.org Édition Modifier le script sélectionné. Modifier... Exécuter le script sélectionné. Il est impossible de créer le script pour l'interpréteur « %1 » Il est impossible de déterminer un interpréteur pour le fichier de script « %1 » Il est impossible de charger l'interpréteur « %1 » Il est impossible d'ouvrir le fichier de script « %1 » Fichier : Icône : Interpréteur : Niveau de sécurité de l'interpréteur Ruby Joëlle Cornavin,Matthieu Robin, Sébastien Renard Nom : Aucune fonction « %1 » d'un tel type Aucun interpréteur  %1 » d'un tel type Supprimer Supprimer le script sélectionné. Exécuter Le fichier de script « %1 » n'existe pas. Arrêter Arrêter l'exécution du script sélectionné. Texte : Application ligne de commande pour exécuter des scripts Kross. Kross 