��          D      l       �      �   6   �   a   �      /  �  ?     9  J   F     �     �                          Applications Finds applications whose name or description match :q: Jump list search result, %1 is action (eg. open new tab), %2 is application (eg. browser)%1 - %2 System Settings Project-Id-Version: plasma_runner_services
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-14 03:09+0100
PO-Revision-Date: 2016-08-31 19:43+0100
Last-Translator: Yoann Laissus <yoann.laissus@gmail.com>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Generator: Lokalize 2.0
 Applications Trouve les applications dont le nom ou la description correspondent à :q: %1 - %2 Configuration du système 