��          �      �       H     I     a      m     �     �     �  
   �     �  &   �          .     L  1   b  �  �     �     �  D   �     �  #     A   =          �  -   �  $   �     �  $     8   ;         	                                    
                    Configure Desktop Theme David Rosca EMAIL OF TRANSLATORSYour emails Get New Themes... Install from File... NAME OF TRANSLATORSYour names Open Theme Remove Theme Theme Files (*.zip *.tar.gz *.tar.bz2) Theme installation failed. Theme installed successfully. Theme removal failed. This module lets you configure the desktop theme. Project-Id-Version: kcm_desktopthemedetails
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-22 05:21+0100
PO-Revision-Date: 2018-01-06 20:35+0800
Last-Translator: Simon Depiets <sdepiets@gmail.com>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Configurer le thème de bureau David Rosca kimael@gmail.com, renard@kde.org, vpinon@kde.org, sdepiets@gmail.com Obtenir de nouveaux thèmes... Installer à partir d'un fichier... Mickaël Sibelle, Sébastien Renard, Vincent Pinon, Simon Depiets Ouvrir un thème Supprimer un thème Fichiers de thème (*.zip *.tar.gz *.tar.bz2) L'installation du thème a échoué. Thème installé avec succès. La suppression du thème a échoué. Ce module vous permet de configurer le thème de bureau. 