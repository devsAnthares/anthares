��    7      �  I   �      �     �     �     �     �     �     �               $     -     5     H     T      `     �     �     �     �     �     �     �     �                     )     7  	   C  	   M     W     d     j     �     �     �     �     �     �     �     �     �     �  @   �     7     @     \     c     j     r     �     �     �  4   �     �  �  �     �	     �	     �	  (   �	     
  	   
     
     ,
  	   ?
     I
     Q
     U
     c
  0   p
     �
     �
  '   �
     �
     �
     
     !     >     N     c     z     �     �     �     �  	   �     �     �               &     /     @  
   M     X     j     l     u  N   �  
   �     �     �     �     �          %     '     *     .     ;     &   0      7                  #                
          	   '   (   +                       6              3      1                   ,   2                  "       *          $      4   )               %      5                     !          /   .   -       % %1 is value, %2 is unit%1 %2 %1: Application Energy Consumption Battery Capacity Charge Percentage Charge state Charging Current Degree Celsius°C Details: %1 Discharging EMAIL OF TRANSLATORSYour emails Energy Energy Consumption Energy Consumption Statistics Environment Full design Fully charged Has power supply Kai Uwe Broulik Last 12 hours Last 2 hours Last 24 hours Last 48 hours Last 7 days Last full Last hour Manufacturer Model NAME OF TRANSLATORSYour names No Not charging PID: %1 Path: %1 Rechargeable Refresh Serial Number Shorthand for WattsW System Temperature This type of history is currently not available for this device. Timespan Timespan of data to display Vendor VoltV Voltage Wakeups per second: %1 (%2%) WattW Watt-hoursWh Yes current power draw from the battery in WConsumption literal percent sign% Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2016-11-23 12:41+0100
Last-Translator: Vincent Pinon <vpinon@kde.org>
Language-Team: French <kde-i18n-doc@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 % %1 %2 %1 :  Consommation d'énergie de l'application Batterie Capacité Pourcentage de charge État de la charge En charge Courant °C Détails : %1 En décharge johan.claudebreuninger@gmail.com, vpinon@kde.org Énergie Consommation d'énergie Statistiques de consommation d'énergie Environnement Capacité d'usine Complètement chargée Adaptateur secteur connecté Kai Uwe Broulik Dernières 12 heures Deux dernières heures Dernières 24 heures Dernières 48 heures Derniers 7 jours Dernière charge complète Dernière heure Fabricant Modèle Johan Claude, Vincent Pinon Non N'est pas en charge PID : %1 Emplacement : %1 Rechargeable Actualiser Numéro de série W Système Température Ce type d'historique n'est pas disponible pour ce périphérique actuellement. Intervalle Intervalle à afficher Vendeur V Tension Réveils par seconde : %1 (%2%) W Wh Oui Consommation % 