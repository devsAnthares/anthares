��    ,      |  ;   �      �  (   �     �  �   �     �     �     �     �     �     �     �     �     �               %     1      J     k     s     �     �     �     �     �     �     �               /     4     I     Y     l     y     �     �      �  7   �                     1     6  �  <     	     $	     (	     D	     \	     d	     k	     �	     �	  	   �	     �	     �	     �	     �	     �	  &   
     ,
  	   E
     O
     i
      ~
     �
     �
  !   �
  "   �
          %     4     E     I     b  "   ~     �     �     �  
   �  *   �  A        Z      a     �     �     �     !                 %   +   ,      "   #                               &                  	                           )                             *      '      $      (                                
    %1 is the name of a session%1 (Wayland) ... @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) @title:windowSelect Image Advanced Author Auto &Login Background: Clear Image Commands Could not decompress archive Cursor Theme: Customize theme Default Description Download New SDDM Themes EMAIL OF TRANSLATORSYour emails General Get New Theme Halt Command: Install From File Install a theme. Invalid theme package Load from file... Login screen using the SDDM Maximum UID: Minimum UID: NAME OF TRANSLATORSYour names Name No preview available Reboot Command: Relogin after quit Remove Theme SDDM KDE Config SDDM theme installer Session: The default cursor theme in SDDM The theme to install, must be an existing archive file. Theme Unable to install theme Uninstall a theme. User User: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2017-12-15 09:04+0100
Last-Translator: Vincent Pinon <vpinon@kde.org>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 %1 (Wayland) ... (Tailles disponibles : %1) Sélectionner une image Avancé Auteur &Connexion Automatique Fond : Effacer l'image Commandes Impossible d'extraire l'archive Thème du curseur : Personnaliser le thème Défaut Description Télécharger de nouveaux thèmes SDDM maxence.ledore@gmail.com Général Obtenir un nouveau thème Commande d'arrêt : Installer à partir d'un fichier Installer un thème. Paquet de thème non valable Charger à partir d'un fichier... Écran de connexion utilisant SDDM UID maximum : UID minimum : Maxence Le Doré Nom Pas d'aperçu disponible Commande de redémarrage : Se reconnecter après déconnexion Supprimer le thème SDDM KDE Config Installateur de thèmes SDDM Session : Le thème par défaut du curseur dans SDDM Le thème à installer, doit être un fichier d'archive existant. Thème Impossible d'installer le thème Désinstaller un thème. Utilisateur Utilisateur 