��    *      l  ;   �      �     �     �     �     �     �     �     �  	   �     	          5     J     W     o     �     �  
   �     �  
   �     �     �     �     �     �     �                    -     C  U   X  Z   �  O   	  D   Y     �     �     �  	   �  	   �     �     �  �  �  
   �     �  !   �      	     	     	     #	  	   ?	     I	     _	     u	     �	     �	     �	  	   �	     �	  	   �	     
     
     
     "
     4
     I
     Q
     j
     r
     v
     �
     �
     �
  )   �
  !     !   6  9   X     �     �     �     �     �  	   �     �                                             &                   	         )          '   "         (                           *   $          #                          %   !      
                            Adapter Add New Device Add New Device... Address Audio Audio device Available devices Bluetooth Bluetooth is Disabled Bluetooth is disabled Bluetooth is offline Browse Files Configure &Bluetooth... Configure Bluetooth... Connect Connected devices Connecting Copy Disconnect Disconnecting Enable Bluetooth File transfer Input Input device Network No No Adapters Available No Devices Found No adapters available No connected devices Notification when the connection failed due to FailedConnection to the device failed Notification when the connection failed due to Failed:HostIsDownThe device is unreachable Notification when the connection failed due to NotReadyThe device is not ready Number of connected devices%1 connected device %1 connected devices Other device Paired Remote Name Send File Send file Trusted Yes Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:01+0100
PO-Revision-Date: 2017-07-03 17:58+0100
Last-Translator: Vincent Pinon <vpinon@kde.org>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Adaptateur Ajouter un périphérique Ajouter nouveau périphérique... Adresse Audio Périphérique audio Périphériques disponibles Bluetooth Bluetooth désactivé Bluetooth désactivé Bluetooth hors-ligne Parcourir les fichiers Configurer &Bluetooth... Configurer Bluetooth... Connecter Périphériques connectés Connexion Copier Déconnecter Déconnexion Activer Bluetooth Transfert de fichier Entrée Périphérique d'entrée Réseau Non Aucun adaptateur disponible Aucun périphérique trouvé Aucun adaptateur disponible Aucun périphérique connecté La connexion au périphérique a échoué Le périphérique est injoignable Le périphérique n'est pas prêt %1 périphérique connecté %1 périphériques connectés Autre périphérique Associé Nom distant Envoyé fichier Envoyer le fichier Approuvé Oui 