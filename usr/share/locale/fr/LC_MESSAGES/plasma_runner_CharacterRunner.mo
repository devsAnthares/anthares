��    
      l      �       �      �            	               .  I   3     }  	   �  �  �     �     �     �     �  -   �     �  Z   �     V     m               
   	                           &Trigger word: Add Item Alias Alias: Character Runner Config Code Creates Characters from :q: if it is a hexadecimal code or defined alias. Delete Item Hex Code: Project-Id-Version: plasma_runner_CharacterRunner
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2013-05-16 16:35+0200
Last-Translator: xavier <xavier.besnard@neuf.fr>
Language-Team: French <kde-i18n-doc@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 1.5
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Mot de déclenchemen&t :  Ajouter un élément Alias Alias : Configuration de l'exécution des caractères Code Crée des caractères depuis :q: s'il s'agit d'un code hexadécimal ou d'un alias défini. Supprimer un élément Code hexa : 