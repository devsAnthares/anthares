��          �      l      �     �     �            
   #     .     :     I     Q     ]     e     v     }     �  #   �     �     �     �     �  
      �       �  %        '     >     C     O     o     ~     �  	   �     �     �     �     �     �     �               ,     A                                                       
   	                                         (Automatic load) Always show all entries Application Status Auto Categories Close popup Communications Entries Extra Items General Hardware Control Hidden Keyboard Shortcut Miscellaneous Name of the system tray entryEntry Show hidden icons Shown Status & Notifications System Services Visibility Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-03 03:15+0100
PO-Revision-Date: 2017-06-28 18:24+0100
Last-Translator: Vincent Pinon <vpinon@kde.org>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
  (Chargement automatique) Toujours afficher toutes les entrées État des applications Auto Catégories Fermer la fenêtre contextuelle Communications Entrées Éléments supplémentaires Général Contrôle du matériel Masqué Raccourcis clavier Divers Entrée Afficher les icônes masquées Affiché État & notifications Services du système Visibilité 