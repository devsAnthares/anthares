��          �   %   �      P     Q     b     w     �     �     �  
   �     �     �     �     �     �  /        3     Q     p     v     �     �     �     �     �     �     �       �  %               5     H     f     r     �     �     �     �     �     �  @   �  "     #   8     \     b     n  	   {  (   �  1   �  '   �  0     &   9  '   `                            
                                                                                             	    %1 file %1 files %1 folder %1 folders %1 of %2 processed %1 of %2 processed at %3/s %1 processed %1 processed at %2/s Appearance Behavior Cancel Clear Configure... Finished Jobs List of running file transfers/jobs (kuiserver) Move them to a different list Move them to a different list. Pause Remove them Remove them. Resume Show all jobs in a list Show all jobs in a list. Show all jobs in a tree Show all jobs in a tree. Show separate windows Show separate windows. Project-Id-Version: kuiserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2013-04-11 08:44+0200
Last-Translator: xavier <ktranslator31@yahoo.fr>
Language-Team: French <kde-i18n-doc@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 1.5
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 %1 fichier %1 fichiers %1 dossier %1 dossiers %1 sur %2 traités %1 sur %2 traités à %3 / s %1 traités %1 traités à %2 / s Présentation Comportement Annuler Effacer Configurer... Travaux terminés Liste des transferts de fichiers et travaux en cours (kuiserver) Les déplacer dans une autre liste Les déplacer dans une autre liste. Pause Les retirer Les retirer. Reprendre Afficher tous les travaux dans une liste Afficher tous les travaux sous forme d'une liste. Afficher tous les travaux dans un arbre Afficher tous les travaux sous forme d'un arbre. Afficher dans des fenêtres séparées Afficher dans des fenêtres séparées. 