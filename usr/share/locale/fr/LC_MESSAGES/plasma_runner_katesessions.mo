��          <      \       p   !   q   3   �      �   �  �   2   �  E   �     ?                   Finds Kate sessions matching :q:. Lists all the Kate editor sessions in your account. Open Kate Session Project-Id-Version: krunner_katesessions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-18 03:08+0100
PO-Revision-Date: 2013-05-16 16:15+0200
Last-Translator: xavier <xavier.besnard@neuf.fr>
Language-Team: French <kde-i18n-doc@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 1.5
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Cherche les sessions de Kate correspondant à :q:. Répertorie toutes les sessions de l'éditeur Kate dans votre compte. Ouvrir une session Kate 