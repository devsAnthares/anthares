��    &      L  5   |      P      Q     r     ~  -   �  #   �  #   �  ?     1   S     �     �     �  H   �  L   �     F  V   N  N   �     �  
                   (     >     W  2   _  
   �     �  )   �  8   �  #      .   D  -   s  D   �  6   �  0     A   N  =   �  9   �  �  	           #  @   0  ;   q     �     �     �     �     �     
          *     G  
   d  	   o     y     }     �     �     �     �     �  	   �            )   '  9   Q     �  3   �  N   �          :     Y     ^     k     q     ~                  "                            
       $                      !                &                                      %      	           #                                %1 notification %1 notifications %1 of %2 %3 %1 running job %1 running jobs &Configure Event Notifications and Actions... 10 seconds ago, keep short10 s ago 30 seconds ago, keep short30 s ago A button tooltip; expands the item to show detailsShow Details A button tooltip; hides item detailsHide Details Clear Notifications Copy Copy Link Address Either just 1 dir or m of n dirs are being processed1 dir %2 of %1 dirs Either just 1 file or m of n files are being processed1 file %2 of %1 files History How much many bytes (or whether unit in the locale has been copied over total%1 of %2 Indicator that there are more urls in the notification than previews shown+%1 Information Job Failed Job Finished More Options... No new notifications. No notifications or jobs Open... Placeholder is job name, eg. 'Copying'%1 (Paused) Select All Show a history of notifications Show application and system notifications Speed and estimated time to completion%1 (%2 remaining) Track file transfers and other jobs Use custom position for the notification popup minutes ago, keep short%1 min ago %1 min ago notification was added n days ago, keep short%1 day ago %1 days ago notification was added yesterday, keep shortYesterday notification was just added, keep shortJust now placeholder is row description, such as Source or Destination%1: the job, which can be anything, failed to complete%1: Failed the job, which can be anything, has finished%1: Finished Project-Id-Version: plasma_applet_notifications
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:07+0100
PO-Revision-Date: 2018-01-12 18:29+0800
Last-Translator: Simon Depiets <sdepiets@gmail.com>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 %1 notification %1 notifications %1 sur %2 %3 Tâche %1 en cours d'exécution %1 tâches en cours d'exécution Configurer la notification d'évènements et les actions… Il y a 10 s Il y a 30 s Afficher les détails Masquer les détails Effacer les notifications Copier Copier l'adresse du lien 1 dossier %2 sur %1 dossiers 1 fichier %2 sur %1 fichiers Historique %1 sur %2 +%1 Information Échec de la tâche Tâche terminée Plus d'options... Aucune nouvelle notification. Aucune notification ou tâche Ouvrir… %1 (en pause) Tout sélectionner Afficher une historique des notifications Affiche les notifications du système et des applications %1 (%2 restant) Suivre les transferts de fichiers et autres tâches Personnaliser l'endroit où les notifications surgissantes doivent apparaître Il y a %1 min Il y a %1 min Il y a %1 jour Il y a %1 jours Hier À l'instant %1 : %1 : Échec %1 : terminé 