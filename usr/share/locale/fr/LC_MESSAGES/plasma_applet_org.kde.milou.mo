��          D      l       �   ;   �   <   �           	  �    X   �  V   W     �     �                          Drag categories to change the order in which results appear Only the selected components are shown in the search results Search Search Results Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2014-02-28 17:24+0100
Last-Translator: Vincent PINON <vincent.pinon@laposte.net>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Déplacez les catégories pour changer l'ordre dans lesquels les résultats apparaissent Seuls les composants sélectionnés sont affichés dans les résultats de la recherche Chercher Résultats de la recherche 