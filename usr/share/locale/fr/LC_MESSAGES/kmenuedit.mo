��    >        S   �      H  	   I  	   S     ]     e     s     y          �     �     �     �     �     �     �  
        "  ?   .     n  >   w  	   �     �     �  S   �      A     b  �  z     	     
	     	  
   '	     2	     B	     R	  
   _	     j	  A   y	     �	     �	  
   �	     �	     �	     
     #
     ,
     ;
     G
     X
     h
     |
     �
     �
     �
     �
     �
     �
               (  T   ;     �  S   �  �  �     �     �  
   �     �  	   �     	            ,   0     ]     d     �  $   �     �     �     �  ?        C  M   K     �     �     �  k   �  >   L  !   �  �  �  	   u          �     �     �     �     �  
   �       X     &   m     �     �     �  =   �          %     6     K     ]     z     �     �     �     �  &   �          1  *   N     y     �     �  p   �     5  X   C            3          1   6       2   4      !   *         #   7                    (   9          &           :   5          +                    "                        '           -          >   =   /   	      )                ;   
      .      $   %       <   ,                    8      0            [Hidden] &Comment: &Delete &Description: &Edit &File &Name: &New Submenu... &Run as a different user &Sort &Sort all by Description &Sort all by Name &Sort selection by Description &Sort selection by Name &Username: &Work path: (C) 2000-2003, Waldo Bastian, Raffaele Sandrini, Matthias Elter Advanced All submenus of '%1' will be removed. Do you want to continue? Co&mmand: Could not write to %1 Current shortcut &key: Do you want to restore the system menu? Warning: This will remove all custom menus. EMAIL OF TRANSLATORSYour emails Enable &launch feedback Following the command, you can have several place holders which will be replaced with the actual values when the actual program is run:
%f - a single file name
%F - a list of files; use for applications that can open several local files at once
%u - a single URL
%U - a list of URLs
%d - the folder of the file to open
%D - a list of folders
%i - the icon
%m - the mini-icon
%c - the caption General General options Hidden entry Item name: KDE Menu Editor KDE menu editor Main Toolbar Maintainer Matthias Elter Menu changes could not be saved because of the following problem: Menu entry to pre-select Montel Laurent Move &Down Move &Up NAME OF TRANSLATORSYour names New &Item... New Item New S&eparator New Submenu Only show in KDE Original Author Previous Maintainer Raffaele Sandrini Restore to System Menu Run in term&inal Save Menu Changes? Show hidden entries Spell Checking Spell checking Options Sub menu to pre-select Submenu name: Terminal &options: Unable to contact khotkeys. Your changes are saved, but they could not be activated. Waldo Bastian You have made changes to the menu.
Do you want to save the changes or discard them? Project-Id-Version: kmenuedit
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2014-07-13 16:39+0200
Last-Translator: Ludovic Grossard
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 1.5
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
  [Masqué]  &Commentaire : &Supprimer &Description : &Modifier &Fichier &Nom : &Nouveau sous-menu... Exécute&r en tant qu'utilisateur différent &Trier Tout trier par de&scription &Trier tout par nom Trier la &sélection par description Trier la &sélection par nom Nom d'&utilisateur : &Dossier de travail : (C) 2000-2003, Waldo Bastian, Raffaele Sandrini, Matthias Elter Avancé Tous les sous-menus de « %1 » seront supprimés. Voulez-vous continuer ? Co&mmande : Impossible d'écrire dans %1 Raccourci &clavier actuel : Voulez-vous vraiment restaurer le menu système ? Attention : les menus personnalisés seront supprimés. duranceau@kde.org, cousin@kde.org, nicolas.ternisien@gmail.com &Activer le témoin de démarrage A la suite de la commande, vous pouvez avoir plusieurs variables qui seront remplacées par les vraies valeurs quand le véritable programme sera lancé :
%f - un simple nom de fichier
%F - une liste de fichiers ; à utiliser pour les applications pouvant ouvrir plusieurs fichiers locaux en une fois
%u - une simple URL
%U - une liste d'URL
%d - le dossier du fichier à ouvrir
%D - une liste de dossiers
%i - l'icône
%m - la mini-icône
%c - le titre Général Options générales Élément caché Nom de l'élément : L'éditeur du menu KDE Éditeur du menu de KDE Barre principale Mainteneur Matthias Elter Les changements de menu n'ont pas pu être enregistrés à cause du problème suivant : Élément de menu à présélectionner Montel Laurent Déplacer vers le &bas Déplacer vers le ha&ut François-Xavier Duranceau, Thibaut Cousin, Nicolas Ternisien Nouvel é&lément... Nouvel élément Nouveau &séparateur Nouveau sous-menu Afficher uniquement dans KDE Auteur originel Mainteneur précédent Raffaele Sandrini Restaurer le menu système Exécuter dans un term&inal Enregistrer les changements du menu ? Afficher les éléments cachés Vérification orthographique Options de la vérification orthographique Sous-menu à présélectionner Nom du sous-menu : &Options du terminal : Impossible de contacter « khotkeys ». Vos changements ont été enregistrés mais impossible de les activer. Waldo Bastian Vous avez modifié le menu.
Voulez-vous enregistrer les changements ou les abandonner ? 