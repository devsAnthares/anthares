��          �      <      �     �     �     �  +   �  @        L     a     i     w     }     �  
   �     �     �     �     �     �  �  
     �             5   !  U   W  "   �  	   �     �     �     �          #     ,     5     N     h  (   �     
         	                                                                                       <a href='%1'>%1</a> Close Copy Automatically: Don't show this dialog, copy automatically. Drop text or an image onto me to upload it to an online service. Error during upload. General History Size: Paste Please wait Please, try again. Sending... Share Shares for '%1' Successfully uploaded The URL was just shared Upload %1 to an online service Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-01 04:04+0100
PO-Revision-Date: 2015-06-13 10:43+0200
Last-Translator: Thomas Vergnaud <thomas.vergnaud@gmx.fr>
Language-Team: French <kde-i18n-doc@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 1.5
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 <a href='%1'>%1</a> Fermer Copier automatiquement : Ne plus afficher ce dialogue. Copier automatiquement. Déposez un texte ou une image sur moi pour le téléverser vers un service en ligne. Erreur pendant le téléversement. Général Taille de l'historique : Coller Veuillez patienter Veuillez réessayer. Envoi… Partager Partages pour « %1 » Téléversé avec succès L'URL vient d'être partagée Téléverser %1 vers un service en ligne 