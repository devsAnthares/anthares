��            )         �     �  "   �     �  !   �  !        4  
   J     U     p     �  !   �     �  0   �  8        ?  !   ]          �     �     �     �                '  
   /  
   :  
   E  &   P     w     �  �  �     p  +   t  2   �  ,   �  -         .  
   H  4   S     �  )   �  9   �  ?   	  ]   B	  L   �	  .   �	  <   
  +   Y
  /   �
  &   �
  7   �
       8   +     d  	   k     u  	   �     �  6   �     �  #   �                                                                                                 	                       
                 ms &Keyboard accelerators visibility: &Top arrow button type: Always Hide Keyboard Accelerators Always Show Keyboard Accelerators Anima&tions duration: Animations Bottom arrow button t&ype: Breeze Settings Center tabbar tabs Drag windows from all empty areas Drag windows from titlebar only Drag windows from titlebar, menubar and toolbars Draw a thin line to indicate focus in menus and menubars Draw focus indicator in lists Draw frame around dockable panels Draw frame around page titles Draw frame around side panels Draw slider tick marks Draw toolbar item separators Enable animations Enable extended resize handles Frames General No Buttons One Button Scrollbars Show Keyboard Accelerators When Needed Two Buttons W&indows' drag mode: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:42+0200
PO-Revision-Date: 2016-11-23 12:05+0100
Last-Translator: Vincent Pinon <vpinon@kde.org>
Language-Team: French <kde-i18n-doc@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
  ms Visibilité des accélérateurs &clavier : Type de bouton pour flèche pointant vers le &haut Toujours masquer les accélérateurs clavier Toujours afficher les accélérateurs clavier Durée des anima&tions : Animations T&ype de bouton pour flèche pointant vers le bas : Paramètres de Breeze Centré les onglets de la barre d'onglets Faire glisser les fenêtres depuis toutes les zones vides Faire glisser les fenêtres uniquement depuis la barre de titre Faire glisser les fenêtres depuis la barre de titre, la barre de menu et les barres d'outils Dessiner un liseré pour signaler le focus dans les menus et barres de menus Afficher l'indicateur de focus dans les listes Dessiner un cadre autour des panneaux pouvant être ancrés. Dessiner un cadre autour des titres de page Dessiner un cadre autour des panneaux latéraux Dessiner des marques pour les curseurs Dessiner les séparateurs d'éléments de barre d'outil Activer les animations Activer la prise en charge de redimensionnement étendue Cadres Général Pas de boutons Un bouton Barres de défilements Afficher les accélérateurs clavier quand nécessaire Deux boutons Mode de glissement des fenêtres : 