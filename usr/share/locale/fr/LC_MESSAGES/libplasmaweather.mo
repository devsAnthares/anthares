��          <      \       p      q   *   �   /   �   �  �   1   �  N     V   Q                   Cannot find '%1' using %2. Connection to %1 weather server timed out. Weather information retrieval for %1 timed out. Project-Id-Version: libplasmaweather
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-13 03:17+0100
PO-Revision-Date: 2013-05-17 10:36+0200
Last-Translator: xavier <xavier.besnard@neuf.fr>
Language-Team: French <kde-i18n-doc@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 1.5
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Impossible de trouver « %1 » à l'aide de %2. Le temps imparti à la connexion au serveur météorologique %1 est dépassé. Le temps imparti à l'extraction des informations météorologiques pour %1 a expiré. 