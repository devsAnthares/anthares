��          �      |      �     �     �     �                 ;  	   \     f     �  &   �     �     �     �  	          �   %  �   �  t   ?  7   �  +   �       �       �  
   �       !     &   1      X     y     �     �  3   �     �            	   2     <  �   M  �   	  �   �	     t
  I   �
     �
                                      
   	                                                         min Act like Always Define a special behavior Don't use special settings EMAIL OF TRANSLATORSYour emails Hibernate NAME OF TRANSLATORSYour names Never shutdown the screen Never suspend or shutdown the computer PC running on AC power PC running on battery power PC running on low battery Shut down Suspend The Power Management Service appears not to be running.
This can be solved by starting or scheduling it inside "Startup and Shutdown" The activity service is not running.
It is necessary to have the activity manager running to configure activity-specific power management behavior. The activity service is running with bare functionalities.
Names and icons of the activities might not be available. This is meant to be: Act like activity %1Activity "%1" Use separate settings (advanced users only) after Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-09 03:03+0200
PO-Revision-Date: 2016-11-23 12:47+0100
Last-Translator: Vincent Pinon <vpinon@kde.org>
Language-Team: French <kde-i18n-doc@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
  min Agir comme Toujours Définir un comportement spécial Ne pas utiliser de réglages spéciaux grossard@kde.org, vpinon@kde.org Veille prolongée Ludovic Grossard, Vincent Pinon Ne jamais éteindre l'écran Ne jamais mettre en veille ou arrêter l'ordinateur PC sur secteur PC sur batterie PC sur batterie faible Éteindre Mettre en veille Le service de gestion de l'alimentation ne semble pas être en cours de fonctionnement.
Ce problème peut être résolu en le démarrant ou en le programmant dans « Démarrage et arrêt » Le service des activités ne fonctionne pas.
Il est nécessaire que le gestionnaire des activités soit en fonctionnement pour configurer le comportement du gestionnaire d'énergie spécifique aux activités. Le service des activités fonctionne avec les fonctionnalités essentielles.
Les noms et icônes des activités ne seront peut-être pas disponibles. Activité « %1 » Utiliser des réglages séparés (utilisateurs expérimentés uniquement) Après 