��            )   �      �     �     �     �     �     �     �     �  
           )   (     R     V     \     c     v     �     �     �  3   �     �       <   #  5   `  8   �  8   �                    /    1     7     O     e     {     �     �     �     �  	   �  .   �     )     1     >  "   N     q     �     �     �  7   �      	  #   	  Q   C	  >   �	  @   �	  @   
  	   V
     `
     v
     �
                                                  
                                                                               	        Add files... Add folder... Background frame Change picture every Choose a folder Choose files Configure plasmoid... Fill mode: General Left click image opens in external viewer Pad Paths Paths: Pause on mouseover Preserve aspect crop Preserve aspect fit Randomize items Stretch The image is duplicated horizontally and vertically The image is not transformed The image is scaled to fit The image is scaled uniformly to fill, cropping if necessary The image is scaled uniformly to fit without cropping The image is stretched horizontally and tiled vertically The image is stretched vertically and tiled horizontally Tile Tile horizontally Tile vertically s Project-Id-Version: plasma_applet_org.kde.plasma.mediaframe
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-16 03:31+0100
PO-Revision-Date: 2018-01-12 18:26+0800
Last-Translator: Simon Depiets <sdepiets@gmail.com>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Ajouter des fichiers… Ajouter un dossier… Cadre d'arrière-plan Changer l'images toutes les Choisir un dossier Choisir des fichiers Configurer le plasmoid… Mode de remplissage : Général Le clic gauche ouvre dans un afficheur externe Remplir Emplacements Emplacements : Pause lors du passage de la souris Rognage conservant le ratio Ajustement conservant le ratio Éléments au hasard Tendre L'image est répétée horizontalement et verticalement L'image n'est pas transformée L'image est étirée pour s'ajuster L'image est étirée uniformément pour tout remplir, avec rognage si nécessaire L'image est étirée uniformément pour s'ajuster sans rognage L'image est étirée horizontalement et répétée verticalement L'image est étirée verticalement et répétée horizontalement Mosaïque Mosaïque horizontale Mosaïque verticale s 