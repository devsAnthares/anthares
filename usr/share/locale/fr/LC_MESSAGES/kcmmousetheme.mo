��          �   %   �      @     A  �   `  m   �  �   P  )   �  `        o     |     �     �     �      �     �     �  '        ,     >     ]     b     s  ?   �  Y   �  +     H   F  �  �     {  �   �  |   8	     �	     �	  s   �	     b
      o
     �
     �
  $   �
     �
     �
        =         X     y     �     �     �  ]   �  l     @   �  J   �                                                 	                                                               
              (c) 2003-2007 Fredrik Höglund <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) @item:inlistbox sizeResolution dependent A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Cursor Theme Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Fredrik Höglund Get new Theme Get new color schemes from the Internet Install from File NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. You have to restart the Plasma session for these changes to take effect. Project-Id-Version: kcminput
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-08-31 19:21+0100
Last-Translator: Yoann Laissus <yoann.laissus@gmail.com>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 (c) 2003-2007 Fredrik Höglund <qt>Voulez-vous vraiment supprimer le thème de curseur <strong>%1</strong> ? <br />Cette action supprimera tous les fichiers installés par ce thème.</qt> <qt>Vous ne pouvez pas supprimer un thème actuellement utilisé.<br />Veuillez tout d'abord en sélectionner un autre.</qt> (Tailles disponibles : %1) Dépendant de la résolution Un thème nommé %1 existe déjà dans votre dossier de thèmes d'icônes. Voulez-vous le remplacer par celui-ci ? Confirmation Paramètres du curseur modifiés Thème de curseur Description Glissez ou saisissez l'URL du thème nicolas.ternisien@gmail.com Fredrik Höglund Obtenir un nouveau thème Obtenez de nouveaux schémas de couleurs à partir d'Internet Installer à partir d'un fichier Nicolas Ternisien Nom Écraser le thème ? Supprimer un thème Le fichier « %1 » ne semble pas correspondre à une archive valable de thème de curseur. Impossible de télécharger l'archive du thème de curseur. Vérifiez que l'adresse « %1 » est correcte. Impossible de trouver l'archive du thème de curseur « %1 ». KDE doit être redémarré pour que ces changements soient pris en compte. 