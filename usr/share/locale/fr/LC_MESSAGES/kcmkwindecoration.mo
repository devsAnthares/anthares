��            )   �      �  !   �  "   �  '   �  ,     #   ;  &   _  !   �  &   �  '   �     �                 V   $  1   {     �  *   �     �        
     
   "     -     6     ;     D     T     [     a     g  �  p     _     g     n     }     �     �  	   �     �     �     �     �     �       o   	  E   y     �  1   �  +   	  $   /	     T	     j	  	   �	     �	     �	     �	  
   �	     �	     �	     �	                                                                          
                                                    	           @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Borders @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large Application menu Border si&ze: Buttons Close Close by double clicking:
 To open the menu, keep the button pressed until it appears. Close windows by double clicking &the menu button Context help Drag buttons between here and the titlebar Drop here to remove button Get New Decorations... Keep above Keep below Maximize Menu Minimize On all desktops Search Shade Theme Titlebar Project-Id-Version: kcmkwindecoration
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-04-14 02:49+0200
PO-Revision-Date: 2015-12-13 16:09+0100
Last-Translator: Sebastien Renard <renard@kde.org>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 1.5
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Énorme Grande Aucune bordure Aucune bordure latérale Normale Démesurée Minuscule Gigantesque Très grande Menu d'application &Taille de la bordure :  Boutons Fermer Ferme les fenêtres avec un double clic
Pour ouvrir le menu, laissez le bouton appuyé jusqu'à son apparition. Fermer les fenêtres en faisant un &double clic sur le bouton de menu Aide contextuelle Faites glisser les boutons vers la barre de titre Faites glisser ici pour supprimer un bouton Obtenir de nouvelles décorations... Conserver au-dessus  Conserver au-dessous  Maximiser Menu Réduire Sur tous les bureaux Rechercher Enrouler Thème Barre de titre 