��          �      l      �  3   �  $     �   :     �  4   �     �  #        =  �   E  �   �  +   �     �     �     �  1     (   3     \  �   s  $   �  (     �  F  I   A     �     �     �  H   �      	  ;   -	  	   i	  �   s	    J
  N   `  !   �     �  (   �  (     E   C  $   �     �  .   �  4   �                                                   	   
                                            1 action for this device %1 actions for this device @info:status Free disk space%1 free Accessing is a less technical word for Mounting; translation should be short and mean 'Currently mounting this device'Accessing... All devices Click to access this device from other applications. Click to eject this disc. Click to safely remove this device. General It is currently <b>not safe</b> to remove this device: applications may be accessing it. Click the eject button to safely remove this device. It is currently <b>not safe</b> to remove this device: applications may be accessing other volumes on this device. Click the eject button on these other volumes to safely remove this device. It is currently safe to remove this device. Most Recent Device No Devices Available Non-removable devices only Open auto mounter kcmConfigure Removable Devices Open popup when new device is plugged in Removable devices only Removing is a less technical word for Unmounting; translation shoud be short and mean 'Currently unmounting this device'Removing... This device is currently accessible. This device is not currently accessible. Project-Id-Version: plasma_applet_notifier
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2016-09-05 22:37+0100
Last-Translator: Thomas Vergnaud <thomas.vergnaud@gmx.fr>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Une seule action pour ce périphérique %1 actions pour ce périphérique %1 libre(s) Accès en cours... Tous les périphériques Cliquez pour accéder à ce périphérique depuis d'autres applications. Cliquez pour éjecter ce disque. Cliquez pour retirer ce périphérique en toute sécurité. Général Il est actuellement <b>déconseillé </b> de retirer ce périphérique : des applications peuvent y accéder en ce moment. Cliquez sur le bouton « Éjecter » pour retirer ce périphérique en toute sécurité. Il est actuellement <b>déconseillé </b> de retirer ce périphérique : des applications peuvent accéder en ce moment à d'autres volumes sur ce périphérique. Cliquez sur le bouton « Éjecter » sur ces autres volumes pour retirer ce périphérique en toute sécurité. Il est actuellement possible de retirer ce périphérique en toute sécurité. Le périphérique le plus récent Aucun périphérique disponible Périphériques non amovibles uniquement Configurer les périphériques amovibles Ouvrir un menu flottant lorsqu'un nouveau périphérique est branché Périphériques amovibles uniquement Suppression en cours... Ce périphérique est actuellement accessible. Ce périphérique n'est pas accessible actuellement. 