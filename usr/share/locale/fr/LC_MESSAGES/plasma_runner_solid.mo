��          �      L      �  m   �     /  "   <  F   _  F   �  F   �  J   4  N     R   �     !  %   2  $   X  #   }  $   �  %   �  &   �  �        �  �  �     �     �  A   �  [     Q   d  M   �  Q   	  a   V	  j   �	     #
     <
     K
     T
     `
     g
     v
     
     �
                                
                          	                                          Close the encrypted container; partitions inside will disappear as they had been unpluggedLock the container Eject medium Finds devices whose name match :q: Lists all devices and allows them to be mounted, unmounted or ejected. Lists all devices which can be ejected, and allows them to be ejected. Lists all devices which can be mounted, and allows them to be mounted. Lists all devices which can be unmounted, and allows them to be unmounted. Lists all encrypted devices which can be locked, and allows them to be locked. Lists all encrypted devices which can be unlocked, and allows them to be unlocked. Mount the device Note this is a KRunner keyworddevice Note this is a KRunner keywordeject Note this is a KRunner keywordlock Note this is a KRunner keywordmount Note this is a KRunner keywordunlock Note this is a KRunner keywordunmount Unlock the encrypted container; will ask for a password; partitions inside will appear as they had been plugged inUnlock the container Unmount the device Project-Id-Version: plasma_runner_solid
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2013-04-26 10:27+0200
Last-Translator: xavier <ktranslator31@yahoo.fr>
Language-Team: French <kde-i18n-doc@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 1.5
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Verrouiller le conteneur Éjecter le média Cherche les périphériques dont le nom correspond à « :q: » Liste tous les périphériques et permet de les monter, de les libérer ou de les éjecter. Liste tous les périphériques pouvant être éjectés et permet de les éjecter. Liste tous les périphériques pouvant être montés et permet de les monter. Liste tous les périphériques pouvant être libérés et permet de les libérer. Liste tous les périphériques chiffrés pouvant être verrouillés et permet de les verrouiller. Liste tous les périphériques déchiffrés pouvant être déverrouillés et permet de les déverrouiller. Monter le périphérique périphérique éjecter verrouiller monter déverrouiller libérer Déverrouiller le conteneur Libérer le périphérique 