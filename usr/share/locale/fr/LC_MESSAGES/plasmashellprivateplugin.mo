��            )   �      �     �     �     �     �     �     �            !   $     F  !   [     }     �     �     �     �     �     �     �     �     �  2     @   >  	     4   �  8   �  $   �          *  �  .  
             0  9   >  /   x     �     �     �  ;   �       '   (     P     X     i     �     �     �      �     �  -   �  /   $	  E   T	  A   �	     �	     �	     �	  4   �	     &
     6
     
                                                             	                                                                       &Execute All Widgets Categories: Desktop Shell Scripting Console Download New Plasma Widgets Editor Executing script at %1 Filters Install Widget From Local File... Installation Failure Installing the package %1 failed. Load New Session Open Script File Output Running Runtime: %1ms Save Script File Screen lock enabled Screen saver timeout Select Plasmoid File Sets the minutes after which the screen is locked. Sets whether the screen will be locked after the specified time. Templates Toolbar Button to switch to KWin Scripting ModeKWin Toolbar Button to switch to Plasma Scripting ModePlasma Unable to load script file <b>%1</b> Uninstallable Use Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-23 03:23+0100
PO-Revision-Date: 2016-11-23 13:59+0100
Last-Translator: Vincent Pinon <vpinon@kde.org>
Language-Team: French <kde-i18n-doc@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 &Exécuter Tous les composants graphiques Catégories : Console de langage de script de l'environnement de bureau Télécharger de nouveaux composants graphiques Éditeur Exécuter le script à %1 Filtres Installer un composant graphique depuis un fichier local... Échec de l'installation L'installation du paquet %1 a échoué. Charger Nouvelle session Ouvrir le fichier de script Sortie En cours d'exécution Exécution : %1 ms Enregistrer le fichier de script Verrouillage d'écran activé Temps d'expiration de l'économiseur d'écran Sélectionner le fichier de composant graphique Définit le nombre de minutes après lequel l'écran est verrouillé. Définit si l'écran sera verrouillé après un temps spécifié. Modèles KWin Plasma Impossible de charger le fichier de script <b>%1</b> Désinstallable Utiliser 