��    =        S   �      8  ;   9     u     �     �     �  6   �  A   
     L     U  $   Y  
   ~     �  #   �     �     �     �     �     �  7        >     [     w  '   �     �     �  $   �  ,   �          3     C     K     ]     y     �     �     �     �     �  �   �  9   �	  "   �	     �	     �	     
     *
     <
     R
     c
  %   u
     �
     �
     �
     �
     �
  
   �
  7        :     T     l     t  �  �  i   x  )   �  '     !   4  &   V     }  !   �     �     �  5   �  	   �  +   �  H   )     r  0   x     �     �     �  ;   �  &   !  *   H     s  L   �  ,   �     �  3     =   D     �     �  	   �  )   �  3   �     
     &  %   3  &   Y     �     �    �  i   �  0   5     f  7   t  (   �  '   �  ,   �  '   *  $   R  /   w     �  .   �  #   �            
     1        O     U     [     c     !          3      0      (                 1      "          =   .   6                                         	      :          5       ,   #   &   7       ;   '             -   
             8                 *           %           <   4   9                )   /   $          2             +       

Choose the previous strip to go to the last cached strip. &Create Comic Book Archive... &Save Comic As... &Strip Number: *.cbz|Comic Book Archive (Zip) @option:check Context menu of comic image&Actual Size @option:check Context menu of comic imageStore current &Position Advanced All An error happened for identifier %1. Appearance Archiving comic failed Automatically update comic plugins: Cache Check for new comic strips: Comic Comic cache: Configure... Could not create the archive at the specified location. Create %1 Comic Book Archive Creating Comic Book Archive Destination: Display error when getting comic failed Download Comics Error Handling Failed adding a file to the archive. Failed creating the file with identifier %1. From beginning to ... From end to ... General Get New Comics... Getting comic strip failed: Go to Strip Information Jump to &current Strip Jump to &first Strip Jump to Strip ... Manual range Maybe there is no Internet connection.
Maybe the comic plugin is broken.
Another reason might be that there is no comic for this day/number/string, so choosing a different one might work. Middle-click on the comic to show it at its original size No zip file is existing, aborting. Range: Show arrows only on mouse over Show comic URL Show comic author Show comic identifier Show comic title Strip identifier: The range of comic strips to archive. Update Visit the comic website Visit the shop &website an abbreviation for Number# %1 days dd.MM.yyyy here strip means comic strip&Next Tab with a new Strip in a range: from toFrom: in a range: from toTo: minutes strips per comic Project-Id-Version: plasma_applet_comic
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-07-18 03:20+0200
PO-Revision-Date: 2015-05-26 17:47+0100
Last-Translator: Maxime Corteel <mcorteel@gmail.com>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 

Sélectionnez la bande dessinée précédente pour aller à la dernière bande dessinée mise en cache. &Créer une archive de bande dessinée... Enregi&strer la bande dessinée sous... Numéro de la &bande dessinée : *.cbz|Archive de bande dessinée (Zip) Taille &actuelle Enregistrer la &position actuelle Avancé Tout Une erreur est apparue pour l'identifiant « %1 ». Apparence Échec de l'archivage de la bande dessinée Mettre à jour automatiquement le module externe de bandes dessinées : Cache Vérifier pour de nouvelles bandes dessinées : Bande dessinée Cache de bandes dessinées : Configurer... Impossible de créer l'archive à l'emplacement spécifié. Créer l'archive de bande dessinée %1 Création d'une archive de bande dessinée Destination : Afficher une image d'erreur quand une bande dessinée ne peut être chargée Télécharger de nouvelles bandes dessinées Gestion des erreurs Échec lors de l'ajout d'un fichier dans l'archive. Échec de création du fichier avec l'identifiant « %1 ». Du début à... De la fin à... Général Obtenir de nouvelles bandes dessinées... Échec du téléchargement de la bande dessinée : Aller à la bande dessinée Informations Aller à la bande dessinée &courante Aller à la &première bande dessinée Aller à la bande dessinée... Intervalle manuel Peut-être n'y a-t-il plus de connexion Internet.
Peut-être que le composant externe de bande dessinée est hors service.
Une autre raison possible pourrait être qu'aucune bande dessinée n'existe pour ce jour / nombre / caractère et le choix d'un autre date pourrait marcher. Cliquez avec le bouton central de la souris sur une bande dessinée pour l'afficher à sa taille initiale Aucun fichier ZIP existant, annulation en cours. Intervalle : Afficher les flèches uniquement au survol de la souris Afficher l'adresse de la bande dessinée Afficher l'auteur de la bande dessinée Afficher l'identifiant de la bande dessinée Afficher le titre de la bande dessinée Identifiant de la bande dessinée : L'intervalle de la bande dessinée à archiver. Mise à jour Visiter le site Internet de la bande dessinée Visiter le site &Web du commerçant # %1 jours jj.mm.aaaa O&nglet suivant avec une nouvelle bande dessinée De : À : minutes images par bande dessinée 