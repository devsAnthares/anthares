��          �      �       0     1  �  H     �       &         ?     `     n     v     �     �  (   �  �  �     �  �  �     �     �  /   �  N   )     x     �  L   �     �     �  5   
	         
                 	                                (c) 2002-2006 KDE Team <h1>System Notifications</h1>Plasma allows for a great deal of control over how you will be notified when certain events occur. There are several choices as to how you are notified:<ul><li>As the application was originally designed.</li><li>With a beep or other noise.</li><li>Via a popup dialog box with additional information.</li><li>By recording the event in a logfile without any additional visual or audible alert.</li></ul> Carsten Pfeiffer Charles Samuels Disable sounds for all of these events EMAIL OF TRANSLATORSYour emails Event source: KNotify NAME OF TRANSLATORSYour names Olivier Goffart Original implementation System Notification Control Panel Module Project-Id-Version: kcmnotify
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-09 07:18+0200
PO-Revision-Date: 2017-09-14 17:31+0100
Last-Translator: Vincent Pinon <vpinon@kde.org>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 (c) 2002-2006 L'équipe KDE <h1>Notifications du système</h1>Plasma permet un large choix de procédés vous informant que certains évènements sont survenus. L'information peut vous parvenir de plusieurs manières : <ul><li>De la façon dont l'application a été nativement conçue.</li> <li>Avec un bip ou un autre son.</li> <li>Par une boîte de dialogue contenant des informations supplémentaires.</li> <li>En enregistrant l'évènement dans un fichier de journal sans autre avertissement visuel ou sonore.</li></ul> Carsten Pfeiffer Charles Samuels Désactiver les sons pour tous ces évènements duranceau@kde.org, cousin@kde.org, nicolas.ternisien@gmail.com, vpinon@kde.org Source de l'évènement : KNotify François-Xavier Duranceau, Thibaut Cousin, Nicolas Ternisien, Vincent Pinon Olivier Goffart Version originale Module de configuration des notifications du système 