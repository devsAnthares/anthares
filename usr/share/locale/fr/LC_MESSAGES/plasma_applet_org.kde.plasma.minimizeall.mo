��          4      L       `      a   *   r   �  �      R  4   i                    Minimize Windows Show the desktop by minimizing all windows Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-04-27 02:55+0200
PO-Revision-Date: 2017-05-04 18:35+0100
Last-Translator: Johan Claude-Breuninger <johan.claudebreuninger@gmail.com>
Language-Team: French <kde-i18n-doc@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 Réduire les fenêtres Affiche le bureau en réduisant toutes les fenêtres 