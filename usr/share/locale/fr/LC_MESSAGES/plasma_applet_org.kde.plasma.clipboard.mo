��          �      L      �     �     �     �     �          )     1     9     P     h     t  K   �     �     �     �     �            �                   5  &   N     u     �     �     �  %   �     �     �                    #     =     X     a                                                  
   	                                               Change the barcode type Clear history Clipboard Contents Clipboard history is empty. Clipboard is empty Code 39 Code 93 Configure Clipboard... Creating barcode failed Data Matrix Edit contents Indicator that there are more urls in the clipboard than previews shown+%1 Invoke action QR Code Remove from history Return to Clipboard Search Show barcode Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-10 03:07+0200
PO-Revision-Date: 2015-03-17 22:26+0100
Last-Translator: Thomas Vergnaud <thomas.vergnaud@gmx.fr>
Language-Team: French <kde-i18n-doc@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 1.5
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Modifie le type de code-barre Effacer l'historique Contenu du presse-papier L'historique du presse-papier est vide Le presse-papier est vide Code 39 Code 93 Configurer le presse-papier La création du code-barre a échoué Code à pixels Éditer le contenu +%1 Invoquer une action Code QR Supprimer de l'historique Retourner au presse-papier Chercher Affiche le code-barre 