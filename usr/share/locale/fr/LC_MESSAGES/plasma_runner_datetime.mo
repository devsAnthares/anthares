��    	      d      �       �      �      �   -        <  -   V  #   �  #   �     �  �  �  	   �     �  6   �     /  6   H          �     �                                   	           Current time is %1 Displays the current date Displays the current date in a given timezone Displays the current time Displays the current time in a given timezone Note this is a KRunner keyworddate Note this is a KRunner keywordtime Today's date is %1 Project-Id-Version: plasma_runner_datetime
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2015-05-26 18:02+0100
Last-Translator: Maxime Corteel <mcorteel@gmail.com>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Generator: Lokalize 2.0
 Il est %1 Affiche la date actuelle Affiche la date actuelle dans un fuseau horaire donné Affiche l'heure actuelle Affiche l'heure actuelle dans un fuseau horaire donné date heure La date d'aujourd'hui est %1 