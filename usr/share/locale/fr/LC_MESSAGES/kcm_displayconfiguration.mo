��    5      �  G   l      �     �     �     �     �     �     �  !   �  '        >     M  -   _     �     �     �     �     �     �     �     �           
     +  	   3     =     N     [  
   i     t     �     �  A   �                     %     2     @     L  <   Z  <   �     �  3   �       �        �     �  ;   �     	     	     *	  %   6	  b   \	  �  �	     �     �     �     �  %   �  )        9  !   X     z     �  6   �     �     �     �               %     ?     K     h  1   u     �     �     �     �     �  
     /        J     c  T   �     �     �     �               .  $   =     b     e     h  S   t     �  �   �     �     �  r   �     @     R  
   k  0   v  z   �             ,      &      3           0   #                 *      /      5       -                                       '   (   !   )   2   4             "          %                         .   +                    1                   
   	         $    %1 Hz &Disable All Outputs &Reconfigure (c), 2012-2013 Daniel Vrátil 90° Clockwise 90° Counterclockwise @title:windowDisable All Outputs @title:windowUnsupported Configuration Active profile Advanced Settings Are you sure you want to disable all outputs? Auto Break Unified Outputs Button Checkbox Combobox Configuration for displays Daniel Vrátil Display Configuration Display: EMAIL OF TRANSLATORSYour emails Enabled Group Box Identify outputs KCM Test App Laptop Screen Maintainer NAME OF TRANSLATORSYour names No Primary Output No available resolutions No kscreen backend found. Please check your kscreen installation. Normal Orientation: Primary display: Radio button Refresh rate: Resolution: Scale Display Scale multiplier, show everything at 1 times normal scale1x Scale multiplier, show everything at 2 times normal scale2x Scale: Scaling changes will come into effect after restart Screen Scaling Sorry, your configuration could not be applied.

Common reasons are that the overall screen size is too big, or you enabled more displays than supported by your GPU. Tab 1 Tab 2 Tip: Hold Ctrl while dragging a display to disable snapping Unified Outputs Unify Outputs Upside Down Warning: There are no active outputs! Your system only supports up to %1 active screen Your system only supports up to %1 active screens Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-24 03:18+0200
PO-Revision-Date: 2017-09-14 17:33+0100
Last-Translator: Vincent Pinon <vpinon@kde.org>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 %1 Hz &Désactiver toutes les sorties &Reconfigurer (c), 2012-2013 Dan Vrátil Pivotée de 90° dans le sens horaire Pivotée de 90° dans le sens antihoraire Désactiver toutes les sorties Configuration non prise en charge Profil actif Configuration avancée Voulez-vous vraiment désactiver toutes les sorties ? Automatique Désynchroniser les sorties Bouton Case à cocher Liste déroulante Configuration des écrans Dan Vrátil Configuration de l'affichage Affichage : jcorn@free.fr, mcorteel@gmail.com, vpinon@kde.org Activé Boîte de groupe Identifier les sorties Application de test de KCM Écran d'ordinateur portable Mainteneur Joëlle Cornavin, Maxime Corteel, Vincent Pinon Aucune sortie principale Aucune résolution disponible Aucun moteur trouvé pour kscreen. Merci de vérifier votre installation de kscreen. Normale Orientation : Affichage principal : Bouton radio Taux de rafraîchissement : Résolution : Mise à l'échelle de l'affichage : 1x 2x Échelle : Les changements de mise à l'échelle ne seront effectifs qu'après un redémarrage Échelle de l'écran Impossible d'appliquer votre configuration.

Les raisons fréquentes sont que la taille globale d'affichage est trop grande, ou que vous avez activé plus d'écrans que ce que votre carte graphique peut prendre en charge. Onglet 1 Onglet 2 Astuce : maintenez la touche « Ctrl » enfoncée en faisant glisser un affichage pour désactiver l'accrochage Sorties unifiées Synchroniser les sorties Retournée Attention : il n'existe aucune sortie active ! Votre système ne peut prendre en charge qu'un écran actif Votre système ne peut prendre en charge que %1 écrans actifs 