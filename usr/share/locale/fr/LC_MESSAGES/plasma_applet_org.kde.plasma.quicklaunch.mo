��          �      <      �     �  <   �  
   �     	          &     3     ?  
   G     R     c     q     }     �     �  
   �     �  �  �     �  M   �  	             "  %   ;     a  	   q     {     �     �     �     �     �          +     =                                                  
   	                                                Add Launcher... Add launchers by Drag and Drop or by using the context menu. Appearance Arrangement Edit Launcher... Enable popup Enter title General Hide icons Maximum columns: Maximum rows: Quicklaunch Remove Launcher Show hidden icons Show launcher names Show title Title Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-02-26 04:09+0100
PO-Revision-Date: 2016-08-31 19:53+0100
Last-Translator: Yoann Laissus <yoann.laissus@gmail.com>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Ajouter un lanceur... Ajouter des lanceurs par glisser-déposer ou en utilisant le menu contextuel. Apparence Disposition Configurer un lanceur... Activer les fenêtres contextuelles  Saisir le titre Général Cacher les icônes Nombre maximal de colonnes : Nombre maximal de lignes : Lancement rapide Supprimer un lanceur Afficher les icônes cachées Afficher le nom des lanceurs  Afficher le titre Titre 