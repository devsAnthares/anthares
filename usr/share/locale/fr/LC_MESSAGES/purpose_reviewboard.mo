��          �      �       H     I     K  	   Z  $   d     �     �  	   �  
   �     �     �  "   �  	     '     �  :     &     (     9  0   L  !   }     �     �     �     �     �  -   �     #  5   8                   
                   	                           / Authentication Base Dir: Could not create the new request:
%1 Could not upload the patch Destination Password: Repository Request Error: %1 Update review User name in the specified service Username: Where this project was checked out from Project-Id-Version: kdevreviewboard
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:19+0100
PO-Revision-Date: 2014-09-02 17:20+0200
Last-Translator: Vincent PINON <vpinon@april.org>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 1.5
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 / Authentification Dossier de base : Impossible de trouver la nouvelle requête :
%1 Impossible d'envoyer le correctif Destination Mot de passe : Dépôt Erreur de requête : %1 Mettre à jour l'analyse Nom de l'utilisateur dans le service indiqué Nom d'utilisateur : L'emplacement depuis lequel ce projet a été extrait 