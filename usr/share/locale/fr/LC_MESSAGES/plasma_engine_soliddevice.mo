��    �      t  �   �      �  
   �  
   �     �     �     �     �     �                         ,     ?     K     O     ]     k     r     w     }     �     �     �     �     �     �     �     �     �     �          &     :     X     `     o     ~     �     �     �     �     �     �     �     �     �     �     �  	   �     �  
   �     �  	          	   $     .     :     K  
   R     ]     m     {     �  
   �     �     �     �  	   �     �     �     �     �     �     �     �     
     !     '  	   -     7     D     J     Z     h     u     |     �     �     �     �  
   �     �     �     �  
   �     �       	        &     .  
   3     >  	   K  
   U     `     e     j     p     u     �     �     �     �     �     �     �     �     �     �     	  	        $     )     :     F     K     S     c     u     |     �     �     �     �  	   �     �     �     �  �  �  
   �     �     �     �     �     �     �                         /     H     a     e     v     �     �     �     �     �     �  	   �     �            	   -     7     E     I  $   `     �  '   �     �     �     �            	         *     0     7     =     E     L     U     a     p     �     �     �  	   �     �     �     �     �       	   #     -     :     R     i     ~     �     �     �  	   �  
   �     �     �     �     �  
   �                2  
   S     ^     e     v     �     �     �     �     �     �     �     �     �       
   #     .     B     Z     f     o     �  
   �     �     �     �     �     �     �     �     �     �          
               /     J     ]     m     �     �     �     �     �  
   �     �     �             
   %     0     B  
   Y     d     p  	   t     ~  	   �  
   �     �     �     �        ^           	   +                y   A       2       n   i       /       �   :       b   �   g       w             B   j   �   1   L   6   <   �   �   8       K   �   ]   E   0   Q   s              D       V      Y      %   G      !   R       X       l   3   '   >          $               ;   O       m   �   q       P   u       |   �       {          @   W   .       f   a                  T   c       (   ?       4       ~      *       [   r   �          I   )      \   d   J   7   p                  -   N            F   `           �   h   H      "   M   C       5   ,       
          k   }   e           =   x   9   z             &              �   _   �   Z       S   o   t   U                           v       #        Accessible Appendable Audio Available Content BD BD-R BD-RE Battery Blank Block Blu Ray Recordable Blu Ray Rewritable Blu Ray Rom Bus CD Recordable CD Rewritable CD Rom CD-R CD-RW Camera Camera Battery Can Change Frequency Capacity Cdrom Drive Charge Percent Charge State Charging Compact Flash DVD DVD Plus Recordable DVD Plus Recordable Duallayer DVD Plus Rewritable DVD Plus Rewritable Duallayer DVD Ram DVD Recordable DVD Rewritable DVD Rom DVD+DL DVD+DLRW DVD+R DVD+RW DVD-R DVD-RAM DVD-RW Data Description Device Device Types Disc Type Discharging Drive Type Emblems Encrypted Encrypted Container File Path File System File System Type Floppy Free Space Free Space Text Fully Charged HD DVD Recordable HD DVD Rewritable HD DVD Rom HDDVD HDDVD-R HDDVD-RW Hard Disk Hotpluggable Icon Ide Ieee1394 Ignored In Use Keyboard Battery Keyboard Mouse Battery Label Major Max Speed Memory Stick Minor Monitor Battery Mouse Battery Not Charging Number Operation result Optical Drive OpticalDisc Other PDA Battery Parent UDI Partition Table Phone Battery Platform Plugged In Portable Media Player Primary Battery Processor Product Raid Read Speed Rechargeable Removable Rewritable Sata Scsi SdMmc Size Smart Media State Storage Access Storage Drive Storage Volume Super Video CD Supported Drivers Supported Media Supported Protocols Tape Temperature Temperature Unit Timestamp Type Type Description UPS Battery UUID Unknown Unknown Battery Unknown Disc Type Unused Usage Usb Vendor Video Blu Ray Video CD Video DVD Write Speed Write Speeds Xd Project-Id-Version: plasma_engine_soliddevice
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-11-23 09:39+0100
Last-Translator: Vincent Pinon <vpinon@kde.org>
Language-Team: French <kde-i18n-doc@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Accessible Écriture à la suite Audio Contenu disponible BD BD-R BD-RE Batterie Vide Bloquer Blu Ray enregistrable Blu Ray ré-inscriptible Blu Ray en lecture seule Bus CD enregistrable CD ré-inscriptible CD en lecture seule CD-R CD-RW Appareil photo Batterie d'appareil photo Fréquence modifiable Capacité Lecteur de CD-ROM Pourcentage de charge État de la charge En charge Compact Flash DVD DVD Plus enregistrable DVD Plus enregistrable double couche DVD Plus ré-inscriptible DVD Plus ré-inscriptible double couche DVD ré-enregistrable DVD enregistrable DVD ré-inscriptible DVD en lecture seule DVD+DL DVD+DL-RW DVD+R DVD+RW DVD-R DVD-RAM DVD-RW Données Description Périphérique Types de périphérique Type de disque En décharge Type de lecteur Emblèmes Chiffré Conteneur chiffré Emplacement de fichier Système de fichiers Type de système de fichiers Disquette Espace libre Texte de l'espace libre Complètement chargée HD DVD enregistrable HD DVD ré-inscriptible HD DVD en lecture seule HD-DVD HD-DVD-R HD-DVD-RW Disque dur Connectable à chaud Icône IDE IEEE1394 Ignoré(e) En cours d'utilisation Batterie de clavier Batterie de clavier et de souris Étiquette Majeur Vitesse maximale Memory Stick Mineur Batterie d'écran Batterie de souris Pas en charge Numéro Résultat de l'opération Lecteur optique Disque optique Autre Batterie d'assistant personnel UDI parent Table de partitions Batterie de téléphone Plate-forme Branché Lecteur multimédia portable Batterie principale Processeur Produit RAID Vitesse de lecture Rechargeable Amovible Ré-inscriptible SATA SCSI SD-MMC Taille Smart Media État Accès au stockage Périphérique de stockage Volume de stockage Super CD vidéo Pilotes pris en charge Média pris en charge Protocoles pris en charge Cassette Température Unité de température Horodatage Type Description du type Batterie non interruptible UUID Inconnu(e) Batterie inconnue Type de disque inconnu Inutilisé Utilisation USB Fabricant Vidéo Blu Ray CD vidéo DVD vidéo Vitesse d'écriture Vitesses d'écriture XD 