��    
      l      �       �   $   �   %     :   <     w  '   �  -   �     �       '     �  <  /     3   E  D   y  4   �  4   �  :   (     c     �  -   �         	                            
        Could not detect the file's mimetype Could not find all required functions Could not find the provider with the specified destination Error trying to execute script Invalid path for the requested provider It was not possible to read the selected file Service was not available Unknown Error You must specify a URL for this service Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2013-04-25 17:43+0200
Last-Translator: xavier <ktranslator31@yahoo.fr>
Language-Team: French <kde-i18n-doc@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 1.5
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Impossible de détecter le type MIME du fichier Impossible de trouver toutes les fonctions requises Impossible de trouver le fournisseur avec la destination spécifiée Erreur lors d'une tentative d'exécution d'un script Emplacement non valable pour le fournisseur demandé Il n'a pas été possible de lire le fichier sélectionné Le service n'est pas disponible Erreur inconnue Vous devez spécifier une URL pour ce service 