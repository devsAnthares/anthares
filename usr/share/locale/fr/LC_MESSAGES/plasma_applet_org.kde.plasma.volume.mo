��    &      L  5   |      P     Q  8   S     �     �     �     �     �     �  3   �  >        N     i     y     �  m   �     �          "     2     7     ?  *   O      z     �     �  "   �     �     �          3     :     H     X     e     �  ;   �     �  �  �     �     �     �  
   �     �     	     	     4	     D	     I	      U	     v	     �	  	   �	     �	  !   �	     �	     �	     �	     �	     
  H   /
  3   x
     �
     �
     �
     �
     �
  ,        B     I  .   Y     �     �     �     �     �        #   %                                                              "          $                      
                                           &      !                	        % Accessibility data on volume sliderAdjust volume for %1 Applications Audio Muted Audio Volume Behavior Capture Devices Capture Streams Checkable switch for (un-)muting sound output.Mute Checkable switch to change the current default output.Default Decrease Microphone Volume Decrease Volume Devices General Heading for a list of ports of a device (for example built-in laptop speakers or a plug for headphones)Ports Increase Microphone Volume Increase Volume Maximum volume: Mute Mute %1 Mute Microphone No applications playing or recording audio No output or input devices found Playback Devices Playback Streams Port is unavailable (unavailable) Port is unplugged (unplugged) Raise maximum volume Show additional options for %1 Volume Volume at %1% Volume feedback Volume step: label of device items%1 (%2) label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2017-10-09 08:48+0100
Last-Translator: Vincent Pinon <vpinon@kde.org>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 % Ajuster le volume de %1 Applications Son coupé Volume audio Comportement Périphériques de capture Flux de capture Muet Par défaut Diminuer le volume du microphone Diminuer le volume Périphériques Général Ports Augmenter le volume du microphone Augmenter le volume Volume maximum : Couper le son Couper le son de %1 Couper le son du microphone Aucune application ne lit ou n'enregistre actuellement du contenu sonore Aucun périphérique d'entrée ou de sortie trouvé Périphériques de lecture Flux de lecture  (indisponible)  (débranché) Augmenter le volume maximum : Afficher les option supplémentaires pour %1 Volume Volume à %1 % Émettre un son lorsque le volume est modifié Pallier de volume : %1 (%2) %1: %2 100 % %1% 