��          �      L      �     �     �  �   �  T   }  )   �  (   �  0   %  $   V  &   {  &   �  %   �  ?   �  F   /     v     �     �     �     �  �  �  .   �  1   �  �   -  J     "   S     v          �  	   �     �     �  %   �     �     	  (   +	     T	  '   s	  1   �	                                              
                    	                                  Dim screen by half Dim screen totally Lists screen brightness options or sets it to the brightness defined by :q:; e.g. screen brightness 50 would dim the screen to 50% maximum brightness Lists system suspend (e.g. sleep, hibernate) options and allows them to be activated Note this is a KRunner keyworddim screen Note this is a KRunner keywordhibernate Note this is a KRunner keywordscreen brightness Note this is a KRunner keywordsleep Note this is a KRunner keywordsuspend Note this is a KRunner keywordto disk Note this is a KRunner keywordto ram Note this is a KRunner keyword; %1 is a parameterdim screen %1 Note this is a KRunner keyword; %1 is a parameterscreen brightness %1 Set Brightness to %1 Suspend to Disk Suspend to RAM Suspends the system to RAM Suspends the system to disk Project-Id-Version: krunner_powerdevil
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-26 03:33+0200
PO-Revision-Date: 2013-04-26 10:47+0200
Last-Translator: xavier <ktranslator31@yahoo.fr>
Language-Team: French <kde-i18n-doc@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 1.5
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Réduire de moitié la luminosité de l'écran Réduire complètement la luminosité de l'écran Affiche les options de réglage de la luminosité de l'écran ou définit la luminosité selon « :q: ». Par exemple, une valeur de luminosité de l'écran à 50 réglera la luminosité maximum de l'écran à 50 %. Affiche les options de mise en veille du système et permet de les activer Régler la luminosité de l'écran hiberner Luminosité de l'écran mettre en veille suspendre vers disque vers mémoire Régler la luminosité de l'écran %1 Luminosité de l'écran %1 Définit la luminosité à %1 Mettre en veille prolongée (sur disque) Mettre en veille (en mémoire) Met le système en veille (en mémoire) Met le système en veille prolongée (sur disque) 