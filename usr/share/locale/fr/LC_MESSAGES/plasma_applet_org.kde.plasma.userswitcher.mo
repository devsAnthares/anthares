��          �      <      �     �     �     �     �     �  '   �  >        L     f     �     �     �  )   �  7   �  '        B     T  �  s     W  	   j     t     �     �  
   �  
   �  '   �  '   �          *     F  ;   `     �     �     �  +   �                                       
                      	                                        Current user General Layout Lock Screen New Session Nobody logged in on that sessionUnused Show a dialog with options to logout/shutdown/restartLeave... Show both avatar and name Show full name (if available) Show login username Show only avatar Show only name Show technical information about sessions User logged in on console (X display number)on %1 (%2) User logged in on console numberTTY %1 User name display You are logged in as <b>%1</b> Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-11 03:18+0100
PO-Revision-Date: 2016-08-31 19:38+0100
Last-Translator: Yoann Laissus <yoann.laissus@gmail.com>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Utilisateur actuel Général Disposition Verrouiller l'écran Nouvelle session Inutilisé Quitter... Afficher aussi bien l'avatar que le nom Afficher le nom complet (si disponible) Afficher le nom d'utilisateur Afficher seulement l'avatar Afficher seulement le nom Afficher des informations techniques à propos des sessions sur %1 (%2) TTY %1 Affichage du nom d'utilisateur Vous êtes connectés en tant que <b>%1</b> 