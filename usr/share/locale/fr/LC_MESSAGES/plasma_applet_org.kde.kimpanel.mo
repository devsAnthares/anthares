��          �      �       0     1     9  
   S     ^     u     �     �     �     �     �     �     �  �  �     �     �  	   �  !   �          &     D     Q     m     �     �     �     	   
                                                  (Empty) @title:windowSelect Font Appearance Configure Input Method Custom Font: Exit Input Method Hide %1 Reload Config Select Font Show Use Default Font Vertical List Project-Id-Version: kimpanel
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-07-18 03:21+0200
PO-Revision-Date: 2015-05-26 17:49+0100
Last-Translator: Maxime Corteel <mcorteel@gmail.com>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 (Vide) Sélectionner la police Apparence Configurer une méthode d'entrée Police personnalisée : Fermer une méthode d'entrée Cacher : %1 Recharger une configuration Sélectionner la police Afficher Utiliser la police par défaut Liste verticale 