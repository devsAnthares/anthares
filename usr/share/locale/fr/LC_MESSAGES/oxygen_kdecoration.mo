��    ?        Y         p     q  !   �  "   �  &   �  ,   �  #   &  &   J  !   q  &   �  '   �  "   �  #     "   )  '   L     t     �  +   �  2   �  
   �     �               ,     3     G  U   O  7   �     �     �     		     	      	     6	     T	     c	     k	  !   �	     �	  	   �	     �	     �	     �	     �	  &   
     /
     N
     U
     p
     v
     ~
     �
  4   �
  $   �
     �
               /     G     ]     w  &   �     �  �  �  5   �     �     �     �                !     -     4     @     M     S     Z     `     l     �  E   �  N   �  
   %     0     E  #   ]     �     �  
   �  O   �  @        D  &   [     �     �  -   �  &   �     �  	     (     +   7  	   c  	   m     w  +   ~  !   �     �  5   �  0     	   J  '   T  	   |     �     �  	   �  P   �  5          6  &   W     ~     �     �  '   �     �  6     "   J     $                               1          "   *   )          :             	         ;   !                          9   =   5   /   3                    (         #   4       8       -   6   
   7          %                                         ?       0   &      +          '         .       ,      >       <   2        &Matching window property:  @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Border @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large @item:inlistbox Button size:Large @item:inlistbox Button size:Normal @item:inlistbox Button size:Small @item:inlistbox Button size:Very Large Active Window Glow Add Add handle to resize windows with no border Allow resizing maximized windows from window edges Animations B&utton size: Border size: Button mouseover transition Center Center (Full Width) Class:  Configure fading between window shadow and glow when window's active state is changed Configure window buttons' mouseover highlight animation Decoration Options Detect Window Properties Dialog Edit Edit Exception - Oxygen Settings Enable/disable this exception Exception Type General Hide window title bar Information about Selected Window Left Move Down Move Up New Exception - Oxygen Settings Question - Oxygen Settings Regular Expression Regular Expression syntax is incorrect Regular expression &to match:  Remove Remove selected exception? Right Shadows Tit&le alignment: Title:  Use the same colors for title bar and window content Use window class (whole application) Use window title Warning - Oxygen Settings Window Class Name Window Drop-Down Shadow Window Identification Window Property Selection Window Title Window active state change transitions Window-Specific Overrides Project-Id-Version: kwin_clients
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-13 05:20+0100
PO-Revision-Date: 2017-12-15 09:03+0100
Last-Translator: Vincent Pinon <vpinon@kde.org>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 &Correspondance avec la propriété de la fenêtre : Énorme Grande Aucune bordure Aucune bordure latérale Normale Démesurée Petite Gigantesque Très grande Grand Normal Petit Très grand Lueur de la fenêtre active Ajouter Ajouter des poignées pour re-dimensionner les fenêtres sans bordure Permettre le redimensionnement des fenêtres maximisées depuis leurs bordures Animations Taille de bo&uton : Taille de la bordure : Transition lors du survol du bouton Centré Centré (sur toute la largeur) Classe :  Configure le fondu entre ombre et lueur de la fenêtre lorsque son état change Configurer l'animation lors du survol des boutons de la fenêtre Options de décoration Détecter les propriétés de fenêtre Boîte de dialogue Modifier Modifier l'exception - Paramètres d'Oxygène Activer / Désactiver cette exception Type d'exception Général Masquer la barre de titre de la fenêtre Informations sur la fenêtre sélectionnée À gauche Descendre Monter Nouvelle exception - Paramètres d'Oxygène Question - Paramètres d'Oxygène Expression rationnelle La syntaxe de l'expression rationnelle est incorrecte Correspondance avec l'expression ra&tionnelle : Supprimer Supprimer l'exception sélectionnée ? À droite Ombres A&lignement du titre : Titre :  Utiliser les mêmes couleurs pour la barre de titre et le contenu de la fenêtre Utiliser la classe de fenêtre (application entière) Utiliser le titre de la fenêtre Avertissement - Paramètres d'Oxygène Nom de la classe de fenêtre Ombre portée de la fenêtre Identification de la fenêtre Sélection des propriétés de fenêtre Titre de la fenêtre Transitions lors des changement d'état de la fenêtre Réglages spécifiques de fenêtre 