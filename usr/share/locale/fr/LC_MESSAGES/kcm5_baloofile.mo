��          �            h     i     �  #   �      �      �     �  J        [  &   y     �  T   �       *   $     O  �  ]  *   <  #   g  #   �  %   �  k   �      A  j   b     �  %   �  F     �   Y     �  3   �     (                            	                                             
       Also index file content Configure File Search Copyright 2007-2010 Sebastian Trüg Do not search in these locations EMAIL OF TRANSLATORSYour emails Enable File Search File Search helps you quickly locate all your files based on their content Folder %1 is already excluded Folder's parent %1 is already excluded NAME OF TRANSLATORSYour names Not allowed to exclude root folder, please disable File Search if you do not want it Sebastian Trüg Select the folder which should be excluded Vishesh Handa Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2018-01-06 20:34+0800
Last-Translator: Simon Depiets <sdepiets@gmail.com>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Indexer également le contenu des fichiers Configurer la recherche de fichiers Copyright 2007-2010 Sebastian Trüg Ne pas chercher dans ces emplacements geoffray.levasseurbrandin@numericable.fr,vpinon@kde.org,johan.claudebreuninger@gmail.com,sdepiets@gmail.com Activer la recherche de fichiers La recherche sur le bureau vous permet de localiser rapidement tous vos fichiers à partir de leur contenu Le dossier %1 est déjà exclu Le dossier parent %1 est déjà exclu Geoffray Levasseur,Vincent Pinon,Johan Claude-Breuninger,Simon Depiets Vous n'avez pas la permission d'exclure le dossier racine, veuillez désactiver la Recherche de fichier si vous ne souhaitez pas l'inclure Sebastian Trüg Sélectionnez les dossiers qui doivent être exclus Vishesh Handa 