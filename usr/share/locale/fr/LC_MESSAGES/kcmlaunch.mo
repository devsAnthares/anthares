��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  �  K     0  #   5  �  Y  �  X	  l        �     �     �  /   �     �     
  #   $  $   H                                          
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2013-04-25 17:40+0200
Last-Translator: xavier <ktranslator31@yahoo.fr>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
  sec &Délai du témoin de démarrage : <H1>Notification de la barre de tâches</H1>
Vous pouvez activer une seconde méthode de notification de 
démarrage utilisée par la barre des tâches. Celle-ci fait apparaître un bouton avec un sablier
tournant, montrant que l'application que vous 
avez lancée est en cours de démarrage.
Il est possible que certaines applications ne sachent pas gérer
cette notification de démarrage. Dans ce cas, le bouton disparaît
après le délai spécifié dans la section « Délai du témoin de démarrage »  <h1>Témoin du curseur</h1>
KDE propose un témoin lié au curseur pour notifier le démarrage des applications.
Pour activer ce témoin, sélectionnez un des modes de retour d'informations visuel 
de la liste déroulante.
Il est possible que certaines applications ne sachent pas gérer cette notification de
démarrage. Dans ce cas, le curseur cesse de clignoter après le temps
spécifié dans la section « Délai du témoin de démarrage » <h1>Notification de lancement</h1> Vous pouvez configurer ici la notification de lancement des applications. Curseur clignotant Curseur rebondissant Tém&oin du curseur Activer la notifica&tion de la barre de tâches Aucun témoin du curseur Témoin du curseur passif Délai d&u témoin de démarrage : &Notification de la barre de tâches 