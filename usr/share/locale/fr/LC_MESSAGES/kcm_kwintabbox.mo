��    "      ,  /   <      �  
   �               ,     >     J  !   V     x     �     �     �     �     �  L   �     #     +     J     Y     u     z     �     �     �     �  	   �     �     �     �  �   �  F   �     �     �     �  �  �  
   �     �     �          ,  
   A     L     U     ]     p     �     �     �  h   �     %	  ;   .	     j	  +   }	  	   �	  
   �	  &   �	     �	     �	     
     
  "   !
     D
     T
  �   g
  T   &     {     �     �                    !                                            	                                                           "               
                    Activities All other activities All other desktops All other screens All windows Alternative An example Desktop NameDesktop 1 Content Current activity Current application Current desktop Current screen Filter windows by Focus policy settings limit the functionality of navigating through windows. Forward Get New Window Switcher Layout Hidden windows Include "Show Desktop" icon Main Minimization Only one window per application Recently used Reverse Screens Shortcuts Show selected window Sort order: Stacking order The currently selected window will be highlighted by fading out all other windows. This option requires desktop effects to be active. The effect to replace the list window when desktop effects are active. Virtual desktops Visible windows Visualization Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2013-04-26 16:07+0200
Last-Translator: xavier <ktranslator31@yahoo.fr>
Language-Team: French <kde-i18n-doc@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 1.5
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Activités Toutes les autres activités Tous les autres bureaux Tous les autres écrans Toutes les fenêtres Alternatif Bureau 1 Contenu Activité courante Application courante Bureau courant Bureau courant Filtre les fenêtres par Les paramètres des règles de focus limitent la fonctionnalité de navigation à travers les fenêtres. En avant Charger une nouvelle disposition de changement de fenêtres Fenêtres cachées Inclure l'icône « Afficher le bureau » Principal Réduction Seulement une fenêtre par application Utilisés récemment En arrière Écrans Raccourcis clavier Affiche la fenêtre sélectionnée Ordre de tri : Ordre d'empilement La fenêtre actuellement sélectionnée sera mise en surbrillance en atténuant les couleurs de toutes les autres fenêtres. Cette option nécessite que les effets de bureau soient activés. L'effet remplaçant la fenêtre de liste lorsque les effets de bureau sont activés. Bureaux virtuels Fenêtres visibles Visualisation 