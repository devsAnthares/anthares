��    :      �  O   �      �  �  �  }  �  M  P  
   �     �     �     �     �  <   �     4     L  	   \     f  .   y  %   �     �     �     �     �               "     @     F  4   ^     �  9   �     �     �  �   �  !   �  t        v     �     �     �     �  	   �     �  B   �  &     ?   E  '   �  )   �  7   �  .     5   >  +   t     �     �     �  ,   �          0  9   =  V   w  C   �  �    �  �  &  �  �  �  
   �"     �"  &   �"  *   �"     #  X   #  "   t#     �#  
   �#     �#  2   �#  '   $     0$     J$     Q$  #   W$  "   {$     �$  "   �$     �$  #   �$  =   �$     2%  H   I%     �%  	   �%    �%  (   �&  �   �&     �'  (   �'     �'     �'  '   �'      (     (  V   (  )   s(  S   �(  4   �(  7   &)  @   ^)  2   �)  =   �)  ;   *  "   L*  $   o*     �*  0   �*  #   �*      +  ?   +  a   W+     �+     8                     2      5   /   3   !          '      %          *      ,   .                 -                 $   
      #   +   )       4                &      6            9           0               	                       1      7   :                               (   "    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body style=" font-family:'hlv'; font-size:9pt; font-weight:400; font-style:normal;">
<p style="-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><br /></p></body></html> <b>Security notice:</b>
                             According to a security audit by Taylor Hornby (Defuse Security),
                             the current implementation of Encfs is vulnerable or potentially vulnerable
                             to multiple types of attacks.
                             For example, an attacker with read/write access
                             to encrypted data might lower the decryption complexity
                             for subsequently encrypted data without this being noticed by a legitimate user,
                             or might use timing analysis to deduce information.
                             <br /><br />
                             This means that you should not synchronize
                             the encrypted data to a cloud storage service,
                             or use it in other circumstances where the attacker
                             can frequently access the encrypted data.
                             <br /><br />
                             See <a href='http://defuse.ca/audits/encfs.htm'>defuse.ca/audits/encfs.htm</a> for more information. <b>Security notice:</b>
                             CryFS encrypts your files, so you can safely store them anywhere.
                             It works well together with cloud services like Dropbox, iCloud, OneDrive and others.
                             <br /><br />
                             Unlike some other file-system overlay solutions,
                             it does not expose the directory structure,
                             the number of files nor the file sizes
                             through the encrypted data format.
                             <br /><br />
                             One important thing to note is that,
                             while CryFS is considered safe,
                             there is no independent security audit
                             which confirms this. Activities Backend: Can not create the mount point Can not open an unknown vault. Change Choose the encryption system you want to use for this vault: Choose the used cypher: Close the vault Configure Configure Vault... Configured backend can not be instantiated: %1 Configured backend does not exist: %1 Correct version found Create CryFS Device is already open Device is not open Dialog Do not show this notice again EncFS Encrypted data location Failed to create directories, check your permissions Failed to execute Failed to fetch the list of applications using this vault Forcefully close  General If you limit this vault only to certain activities, it will be shown in the applet only when you are in those activities. Furthermore, when you switch to an activity it should not be available in, it will automatically be closed. Limit to the selected activities: Mind that there is no way to recover a forgotten password. If you forget the password, your data is as good as gone. Mount point Mount point is not specified Mount point: Next Open with File Manager Password: Previous The mount point directory is not empty, refusing to open the vault The specified backend is not available The vault configuration can only be changed while it is closed. The vault is unknown, can not close it. The vault is unknown, can not destroy it. This device is already registered. Can not recreate it. This directory already contains encrypted data Unable to close the vault, an application is using it Unable to close the vault, it is used by %1 Unable to detect the version Unable to perform the operation Unknown device Unknown error, unable to create the backend. Use the default cipher Vaul&t name: Wrong version installed. The required version is %1.%2.%3 You need to select empty directories for the encrypted storage and for the mount point formatting the message for a command, as in encfs: not found%1: %2 Project-Id-Version: plasmavault-kde
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-11 05:53+0100
PO-Revision-Date: 2017-09-14 17:30+0100
Last-Translator: Vincent Pinon <vpinon@kde.org>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body style=" font-family:'hlv'; font-size:9pt; font-weight:400; font-style:normal;">
<p style="-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><br /></p></body></html> <b>Avertissement de sécurité :</b>
                             D'après un audit de sécurité réalisé par Taylor Hornby (Defuse Security),
                             l'implémentation actuelle de EncFS est vulnérable ou potentiellement vulnérable
                             à de multiples types d'attaques.
                             Par exemple, un attaqueur avec un accès en lecture/écriture
                             à des données chiffrées pourrait diminuer la complexité de déchiffrement
                             pour des données chiffrées par la suite sans que cela ne soit visible pour un utilisateur légitime,
                             ou pourrait utiliser une analyse temporelle pour en déduire des informations.
                             <br /><br />
                             Cela signifie que vous ne devriez pas synchroniser
                             les données chiffrées vers un service de stockage dans le nuage (cloud),
                             ou l'utiliser dans des circonstances où un  attaqueur
                             peut avoir un accès fréquent aux données.
                             <br /><br />
                             Voir <a href='http://defuse.ca/audits/encfs.htm'>defuse.ca/audits/encfs.htm</a> pour plus d'informations. <b>Avertissement de sécurité :</b>
                             CryFS chiffre vos données pour que vous puissiez les stocker partout.
                             Il fonctionne bien avec des services dans le nuage (cloud) comme Dropbox, iCloud, OneDrive et d'autres.
                             <br /><br />
                             Contrairement à d'autres solutions de surcouche de système de fichiers,
                             il n'expose pas la structure des dossiers,
                             le nombre de fichiers ainsi que leurs tailles
                             au travers du format de chiffrement de données.
                             <br /><br />
                             Il est important de noter que,
                             même si CryFS est considéré comme sûr,
                             il n'y a pas eu d'audit indépendant
                             venant le confirmer. Activités Programme principal : Ne peut pas créer le point de montage Ne peut pas ouvrir un coffre-fort inconnu. Modifier Choisissez le système de chiffrement que vous souhaitez utiliser pour ce coffre-fort :  Choisir le code secret utilisé :  Fermer le coffre-fort Configurer Configurer le coffre-fort... Impossible d'instancier le moteur configuré : %1 Le moteur configuré n'existe pas : %1 Version correcte trouvée Créer CryFS Le périphérique est déjà ouvert Le périphérique n'est pas ouvert Dialogue Ne plus afficher cet avertissement EncFS Emplacement des données chiffrées Impossible de créer les dossiers, vérifiez vos permissions. Échec de l'exécution Impossible d'extraire la liste des applications utilisant ce coffre-fort Forcer la fermeture Général Si vous limitez le coffre-fort uniquement à certaines activités, il sera affiché dans l'applet uniquement si vous être dans ces activités. De plus, lorsque vous passez à une activité dans laquelle il ne devrait pas être disponible, il sera automatiquement fermé. Limiter aux activités sélectionnées : Veuillez garder à l'esprit qu'il est impossible de récupérer un mot de passe oublié. Si vous oubliez votre mot de passe vos données seront perdues. Point de montage Le point de montage n'est pas spécifié Point de montage : Suivant Ouvrir avec un gestionnaire de fichiers Mot de passe : Précédent Le dossier de point de montage n'est pas vide, l'ouverture du coffre-fort est refusée Le moteur spécifié n'est pas disponible La configuration du coffre-fort peut être changée uniquement quand il est fermé. Le coffre-fort est inconnu, impossible de le fermer. Le coffre-fort est inconnu, impossible de le détruire. Ce périphérique est déjà déclaré. Impossible de le créer. Ce dossier contient déjà des données chiffrées Impossibe de fermer le coffre-fort, une application l'utilise Impossible de fermer le coffre-fort, il est utilisé par %1 Impossible de détecter la version Impossible de réaliser l'opération Périphérique inconnu Erreur inconnue, impossible de créer le moteur. Utiliser le code secret par défaut Nom du coffre-for&t :  Mauvaise version installée. La version requise est la %1.%2.%3 Vous devez sélectionner des dossiers vides pour le stockage chiffré et pour le point de montage %1 : %2 