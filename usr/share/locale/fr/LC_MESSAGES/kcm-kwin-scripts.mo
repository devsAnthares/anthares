��          �      �       0  (   1     Z      q     �     �     �     �     �     �  `     ^   u     �  �  �  (   �     �          8     W     o     �     �  #   �  1   �  3   
     >        
              	                                    *.kwinscript|KWin scripts (*.kwinscript) Configure KWin scripts EMAIL OF TRANSLATORSYour emails Get New Scripts... Import KWin Script Import KWin script... KWin Scripts KWin script configuration NAME OF TRANSLATORSYour names Placeholder is error message returned from the install serviceCannot import selected script.
%1 Placeholder is name of the script that was importedThe script "%1" was successfully imported. Tamás Krutki Project-Id-Version: kcm-kwin-scripts
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-19 03:12+0100
PO-Revision-Date: 2018-01-18 19:53+0100
Last-Translator: Yoann Laissus <yoann.laissus@gmail.com>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 *.kwinscript|scripts KWin (*.kwinscript) Configurer des scripts KWin jcorn@free.fr,renard@kde.org Obtenir de nouveaux scripts... Importer un script KWin Importer un script KWin... Scripts KWin Configuration du script KWin Joëlle Cornavin, Sébastien Renard Impossible d'importer le script sélectionné.
%1 Le script « %1 » a été importé avec succès. Tamas Krutki 