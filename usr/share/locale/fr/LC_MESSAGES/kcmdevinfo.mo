��    ]           �      �  !   �       	   !     +  S   4  	   �     �     �     �     �     �     �     �     �     �     	  J   $	     o	     }	     �	      �	  	   �	  
   �	     �	     �	     �	     �	     
     
     
  L   
  	   l
  	   v
  
   �
  
   �
  
   �
     �
     �
     �
     �
     �
     �
     �
     �
     	     (  	   +     5     G     S     [     i     m     }     �     �  
   �  	   �     �  
   �     �     �     �     �     �     	          +     ?     \     r     x     |     �  H   �     �     �     �     �     �       
     &        A  "   T     w     �     �     �     �       	     �  (  8   
     C  	   Y     c  $   l  	   �     �     �     �     �     �  	   �     �             "   2  6   U     �     �     �  1   �     �                '     H  
   ^     i     �     �  J   �  	   �  	   �  
   �  
   �  
                  '     6     G     b     p     w  /   �     �     �     �  
   �     �     �          .     C     L     Z     t     �     �     �     �     �     �  !   �  '   �          '     <  &   W     ~     �     �     �     �  B   �     �  
             %     9     S  
   W     b     j     p     v     �     �     �     �     �     �           B   "       (   W   #   $   %            [   @      L   M   '   6       >   T           <   9   G   &           A      .   8   R       7       J             ?   4                   H   -                      F          5   )   N   D       ,       K             3       U   =   
       ;       O   :           +           !   Z   /      V   Y              Q         E       1       X             ]       0      P         \   I          	          C       S   2       *    
Solid Based Device Viewer Module (c) 2010 David Hubner AMD 3DNow ATI IVEC Available space out of total partition size (percent used)%1 free of %2 (%3% used) Batteries Battery Type:  Bus:  Camera Cameras Charge Status:  Charging Collapse All Compact Flash Reader Default device tooltipA Device Device Information Device Listing Whats ThisShows all the devices that are currently listed. Device Viewer Devices Discharging EMAIL OF TRANSLATORSYour emails Encrypted Expand All File System File System Type:  Fully Charged Hard Disk Drive Hotpluggable? IDE IEEE1394 Info Panel Whats ThisShows information about the currently selected device. Intel MMX Intel SSE Intel SSE2 Intel SSE3 Intel SSE4 Keyboard Keyboard + Mouse Label:  Max Speed:  Memory Stick Reader Mounted At:  Mouse Multimedia Players NAME OF TRANSLATORSYour names No No Charge No data available Not Mounted Not Set Optical Drive PDA Partition Table Primary Processor %1 Processor Number:  Processors Product:  Raid Removable? SATA SCSI SD/MMC Reader Show All Devices Show Relevant Devices Smart Media Reader Storage Drives Supported Drivers:  Supported Instruction Sets:  Supported Protocols:  UDI:  UPS USB UUID:  Udi Whats ThisShows the current device's UDI (Unique Device Identifier) Unknown Drive Unused Vendor:  Volume Space: Volume Usage:  Yes kcmdevinfo name of something is not knownUnknown no device UDINone no instruction set extensionsNone platform storage busPlatform unknown battery typeUnknown unknown deviceUnknown unknown device typeUnknown unknown storage busUnknown unknown volume usageUnknown xD Reader Project-Id-Version: kcmdevinfo
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-23 03:11+0200
PO-Revision-Date: 2016-11-23 12:27+0100
Last-Translator: Vincent Pinon <vpinon@kde.org>
Language-Team: French <kde-i18n-doc@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 
Module d'affichage des périphériques fondé sur Solid (c) 2010 David Hubner AMD 3DNow ATI IVEC %1 libre(s) sur %2 (%3% utilisé(s)) Batteries Type de batterie :  Bus :  Appareil photo Appareils photo État de charge : En charge Tout réduire Lecteur « Compact Flash » Un périphérique Informations sur le périphérique Affiche tous les périphériques actuellement listés. Afficheur de périphériques Périphériques En décharge jcorn@free.fr, mcorteel@gmail.com, vpinon@kde.org Chiffré Tout développer Système de fichiers Type de système de fichiers :  Complètement chargé Disque dur Connectable à chaud ? IDE IEEE1394 Affiche des informations sur le périphérique actuellement sélectionné. Intel MMX Intel SSE Intel SSE2 Intel SSE3 Intel SSE4 Clavier Clavier + Souris Étiquette :  Vitesse max. :  Lecteur de cartes mémoire Monté à :  Souris Lecteurs multimédia Joëlle Cornavin, Maxime Corteel, Vincent Pinon Non Aucune charge Aucune donnée disponible Non monté Non défini Lecteur optique Assistant personnel numérique Table des partitions Primaire Processeur %1 Numéro de processeur :  Processeurs Produit :  RAID Amovible ? SATA SCSI Lecteur « SD / MMC » Afficher tous les périphériques Afficher les périphériques pertinents Lecteur « Smart Media » Lecteurs de stockage Pilotes pris en charge :  Jeux d'instructions pris en charge :  Protocoles pris en charge :  UDI :  Onduleur USB UUID : Affiche le UDI (Unique Device Identifier) du périphérique actuel Lecteur inconnu Inutilisé Fabricant :  Taille du volume : Utilisation du volume :  Oui kcmdevinfo Inconnu Aucun Aucun Plate-forme Inconnu Inconnu Inconnu Inconnu Inconnu Lecteur « xD » 