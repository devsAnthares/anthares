��            )   �      �  &   �     �     �     �     �      �               .     6  ,   S  3   �     �     �     �     �     �  '        4     S     Y     o     �     �     �     �     �  &   �     �  �  �     �  6   �     �          )     @     U  [   b     �  g   �  �   ?  �   �  \   f	  U   �	     
     -
  "   D
  u   g
  '   �
       F     L   ]     �  [   �       E   2     x  �   �  
   $                                       
                      	                                                                            @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2012-03-25 21:39+0530
Last-Translator: Sri Ramadoss M <amachu@yavarkkum.org>
Language-Team: Tamil <podhu@madaladal.yavarkkum.org>
Language: ta
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.2
 பொதுவான புதிய நிரலை சேர்க்க. சேர்க்க... இரத்துக? கருத்து: amachu@yavarkkum.org தொகு தேர்வு செய்யப்பட்ட நிரலை தொகுக்க. தொகுக்க... தேர்வு செய்யப்பட்ட நேர்நிரலை இயக்குக. "%1" வரியொடுக்கிக்கான நேர்நிரலை உருவாக்க இயலவில்லை  "%1" நேர்நிரட்கோப்புக்கான வரியொடுக்கிதனை இனங்காண இயலவில்லை "%1" வரியொடுக்கியினை ஏற்ற இயலவில்லை  "%1" நிரட்கோப்பை திறக்க இயலவில்லை கோப்பு: முகவுரு: வரியொடுக்கி: ரூபி வரியொடுக்கியின் பாதுகாப்புத் திறனளவு ம. ஸ்ரீ ராமதாஸ் பெயர்: "%1" என்றொரு செயற்பாடு இல்லை "%1" என்றொரு வரியொடுக்கி இல்லை நீக்குக தேர்வு செய்யப்பட்ட நிரலை அகற்றுக. இயக்குக "%1" நேர் நிரல் கோப்பு இல்லை. நிறுத்துக தேர்வு செய்யப்பட்ட நேர்நிரலின் இயக்கத்தை நிறுத்துக. உரை: 