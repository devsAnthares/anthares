��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  �  K     �  ^   �  $  8  �  ]
  �   '  =   �  4   �  &     _   F  H   �  [   �  �   K  :   �                                          
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2004-08-09 03:53-0800
Last-Translator: Tamil PC <tamilpc@ambalam.com>
Language-Team: Tamil <ta@i18n.kde.org>
Language: ta
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
 நொடி தொடக்கக் குறிப்பு வெளியேற்ற நேரம்: <h1>செயற்பட்டி அறிவிப்பு</h1>
kசாளரமைப்பு பயன்பாடுகள் தொடக்கக் குறிப்பை உணர்த்த செயற்பட்டி அறிவிப்பையும் அளிக்கிறது.
இது ஒரு மனிக்கூண்டின் மூலம் உணர்த்தப்படும்.
ஆனால் சில பயன்பாடுகள் இந்த வசதியைப் பயன்படுத்தாமல் இருக்கலாம்.
அந்நேரங்களில்  குறிப்பிட்ட வெளியேற்ற நேரம் முடிந்தவுடன் இது நின்றுவிடும் <h1>வேலையில் உள்ள இடம் காட்டி</h1>
KDE பயன்பாட்டைத் துவக்குவதற்க்கான அறிவிக்கைத் தரும் வேலையில் உள்ள இடம் காட்டியை வழங்குகிறது.
இந்த வேலையில் உள்ள செயலாக்க, ஏதேனும் ஒரு வகை காட்சிப் பின்னோட்டத்தைக் காம்போ பெட்டியிலிருந்துத் 
தேர்ந்தெடுங்கள்.
சிலப் பயன்பாடுகள் துவக்கத்தைப் பற்றிய விழிப்புணர்வு அறிவிக்கை இல்லாமல் இருக்கலாம்
இந்நிலையில் இடம் காட்டி சிமிட்டுவதை சிறிதுக் காலத்திற்க்குப் பிறகு
"தொடக்கக் குறிப்பு வெளியேற்ற நேரம்" பகுதியில் கொடுக்கப்படும் <h1>ஏவு</h1> நிரல்-ஏவற் செய்திகளை இங்கு வடிவமைக்கலாம். சிமிட்டும் இடங்காட்டி எம்பும் இடஞ்சுட்டி &இயங்கு சுட்டி செயற்பட்டி அறிவிப்பை செயல்படுத்து இடம் காட்டி வேலையில் இல்லை நிலைத்த வேலையில் உள்ள இடம் காட்டி காலங்கடந்ததை குறிப்பெடுக்கும் சுட்டியை துவக்கு. பணிப்பட்டை&அறிவிப்பு 