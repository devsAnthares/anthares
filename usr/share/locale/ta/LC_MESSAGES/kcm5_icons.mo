��    )      d  ;   �      �     �     �     �     �     �     �  -   �  r     	   �  	   �     �     �     �  
   �     �     �      �          "     (     .     I     h  	   m     w     }     �     �     �     �     �     �     �  +         ,     4     B  S   J  )   �     �  ~  �     S     a  0   u  7   �     �     �  j   	    w	     �
  C   �
     �
       -   %  7   S     �  �   �     '  7   6     n     {  W   �  (   �       "   +     N  !   ^  +   �     �  N   �  Z     T   s     �  4   �  k     !   w  $   �  $   �  �   �  �   �  .   [                         %   (          '                                                                             #                              "          )   &      	          $           !          
    &Amount: &Effect: &Second color: &Semi-transparent &Theme (c) 2000-2003 Geert Jansen <qt>Installing <strong>%1</strong> theme</qt> A problem occurred during the installation process; however, most of the themes in the archive have been installed Ad&vanced All Icons Co&lor: Colorize Confirmation Desaturate Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Effect Parameters Gamma Icons Icons Control Panel Module NAME OF TRANSLATORSYour names Name No Effect Panel Preview Remove Theme Set Effect... Setup Active Icon Effect Setup Default Icon Effect Setup Disabled Icon Effect Size: Small Icons The file is not a valid icon theme archive. To Gray To Monochrome Toolbar Unable to download the icon theme archive;
please check that address %1 is correct. Unable to find the icon theme archive %1. Use of Icon Project-Id-Version: kcmicons
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-08 06:27+0100
PO-Revision-Date: 2005-02-14 02:32-0800
Last-Translator: Tamil PC <tamilpc@ambalam.com>
Language-Team:  <tamilpc@ambalam.com>
Language: ta
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
 அளவு: விளைவு: &இரண்டாவது வண்ணம்: குறை-பளிங்குத்தன்மை உருகரு (c) 2000-2003 Geert Jansen <qt><strong>%1</strong> என்ற உருகரு நிறுவப்படுகிறது</qt> நிறுவும்போது பிழை நேர்ந்துள்ளது மன்னிக்கவும். ஆனாலும் காப்பகத்திலுள்ள பல பொருள்கள் நிறுவப்பட்டுள்ளது மேம்பட்டது அனைத்து குறும்படங்களும் நிறம்: வண்ணமாக்கு உறுதிப்படுத்தல் தெவிட்டு நிலையகற்று விபரம் உருகரு இருக்கும் URL-ஐ இழுத்துபோடு அல்ல உள்ளீடு செய் vasee@ieee.org விளைவுப் பரமானங்கள் காமா சின்னங்கள் சிண்ண கட்டுப்பாட்டுப் பலகை கூறு கிறிஸ்டோபர்.மை பெயர் விளைவு இல்லை பலகம் முன்னோட்டம் உருகருவை நீக்கு விளைவு அமை இயங்கு குறும்பட விளைவினை அமை கொடாநிலைக் குறும்பட விளைவினை அமை இயங்காக் குறும்பட விளைவினை அமை அளவு: சிறு குறும்படங்கள் கோப்பு செல்லாத சின்னம் பொருள் காப்பகம். சாம்பலிற்கு மோக்ரோமிற்கு கருவிப்பட்டை சின்னம் பொருள் காப்பகம் படி விறக்கு முடியவில்லை;
 %1 முகவரி சரியாக உள்ளதா என பார்க்கவும். சின்னம் பொருள் காப்பகம் %1 கண்டுபிடிக்கமுடியவில்லை. குறும்படப் பாவனை 