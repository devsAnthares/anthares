��          �   %   �      0     1     B     W     j     �     �  
   �     �     �     �     �     �     �                ,     9     @     X     q     �     �     �  �  �  4   �  .   �  1   �  F   %  '   l  <   �     �     �     �       @   2  i   s  j   �  $   H  .   m  /   �  $   �  v   �  z   h	  |   �	  w   `
  K   �
  K   $                             
                                                                                              	    %1 file %1 files %1 folder %1 folders %1 of %2 processed %1 of %2 processed at %3/s %1 processed %1 processed at %2/s Appearance Behavior Cancel Configure... Finished Jobs Move them to a different list Move them to a different list. Pause Remove them Remove them. Resume Show all jobs in a list Show all jobs in a list. Show all jobs in a tree Show all jobs in a tree. Show separate windows Show separate windows. Project-Id-Version: kuiserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2008-03-19 09:02+0530
Last-Translator: ஆமாச்சு <amachu@ubuntu.com>
Language-Team: தமிழ் <https://lists.ubuntu.com/mailman/listinfo/ubuntu-l10n-tam>
Language: ta
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
 %1 கோப்பு %1 கோப்புகள் %1 அடைவு %1 அடைவுகள் %2 ல் %1 ஆக்கப்பட்டது %3/s தனில் %2 ல் %1 ஆக்கப்பட்டது %1 ஆக்கப்பட்டது %2/s தனில் %1 ஆக்கப்பட்டது தோற்றம் நடத்தை தவிர்க்க வடிவமைக்க... நிறைவேற்றப்பட்ட பணிகள் அவற்றை மற்றொரு பட்டியலுக்கு நகர்த்துக மற்றொரு பட்டியலுக்கு அவற்றை நகர்த்துக. இடைநிறுத்துக அவற்றை அகற்றவும் அவற்றை அகற்றவும். மீளத்துவங்கு பட்டியலில் உள்ள அனைத்து பணிகளையும் காட்டுக பட்டியலில் உள்ள அனைத்து வேலைகளையும் காட்டுக. மரவுருவில் உள்ள அனைத்துப் பணிகளையும் காட்டுக மரவுருவில் உள்ள அனைத்து பணிகளையும் காட்டுக. தனித் தனி சாளரங்களை காட்டுக தனித்தனி சாளரங்களை காட்டுக. 