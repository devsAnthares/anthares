��    E      D  a   l      �  
   �  
   �  
             !     %     9     H     N  	   ]     g  	   o     y     �     �  
   �     �     �  	   �     �     �     �     �     �     �               "     )  
   ;     F  2   Y  ?   �     �     �     �     �     �     �     
               .     <     ?     O     \     b     o  	   {     �     �     �     �  b   �  �   	     �	     �	  	   �	     �	     �	  
   �	  	   �	     	
     
     !
     '
     9
  �  J
     �     �     �                    $     1     8     E  	   R     \  	   i     s     w     ~     �     �     �     �     �     �     �     �     �  	   �     �     �                 0   +  3   \  	   �     �     �     �     �     �     �     �     �     �     
               +     /     <  	   I     S     Y     f     |  E   �  �   �     Y     ]     p     }     �  	   �     �     �     �     �     �     �     *       D   B   3       %       C           
   ;      )   8               E      .   "                @       ?   <                        :                  A             9       &           /         0   1         !                       ,   '       5   >         =              #   -   $   +              (       7       2   4   	                  6    Activities Add Action Add Spacer Add Widgets... Alt Alternative Widgets Always Visible Apply Apply Settings Apply now Author: Auto Hide Back-Button Bottom Cancel Categories Center Close Configure Configure activity Create activity... Ctrl Currently being used Delete Email: Forward-Button Get new widgets Height Horizontal-Scroll Input Here Keyboard shortcuts Layout cannot be changed whilst widgets are locked Layout changes must be applied before other changes can be made Layout: Left Left-Button License: Lock Widgets Maximize Panel Meta Middle-Button More Settings... Mouse Actions OK Panel Alignment Remove Panel Right Right-Button Screen Edge Search... Shift Stop activity Stopped activities: Switch The settings of the current module have changed. Do you want to apply the changes or discard them? This shortcut will activate the applet: it will give the keyboard focus to it, and if the applet has a popup (such as the start menu), the popup will be open. Top Undo uninstall Uninstall Uninstall widget Vertical-Scroll Visibility Wallpaper Wallpaper Type: Widgets Width Windows Can Cover Windows Go Below Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-09 03:10+0200
PO-Revision-Date: 2017-01-24 15:47+0800
Last-Translator: Jeff Huang <s8321414@gmail.com>
Language-Team: Chinese <kde-i18n-doc@kde.org>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 活動 新增動作 新增空白 新增元件... Alt 替代元件 永遠可見 套用 套用設定 立刻套用 作者： 自動隱藏 向後鍵 下 取消 類別 中 關閉 設定 設定活動 建立活動... Ctrl 目前使用中的 刪除 電子郵件： 向前鈕 取得新元件 高度 水平捲軸 在此輸入 鍵盤捷徑 因為元件被鎖定，因此無法變更佈局 佈局的變動必須在其他變動做出前套用 佈局： 左 滑鼠左鍵 授權條款： 鎖定元件 將面板最大化 Meta 滑鼠中鍵 更多設定... 滑鼠動作 確定 面板對齊 移除面板 右 滑鼠右鍵 螢幕邊緣 搜尋... Shift 停止活動 已停止的活動： 切換 目前模組已被變更。您要套用還是要丟棄這些變更？ 啟動此小程式的快捷鍵：它會將鍵盤的焦點交給它，並且如果小程式有彈出式視窗(如開始選單)則也會開啟。 上 復原解除安裝 解除安裝 解除安裝元件 垂直捲軸 可見度 桌布 桌布型態： 元件 寬度 視窗可覆蓋其上 視窗在下面 