��    =        S   �      8  ;   9     u     �     �     �  6   �  A   
     L     U  $   Y  
   ~     �  #   �     �     �     �     �     �  7        >     [     w  '   �     �     �  $   �  ,   �          3     C     K     ]     y     �     �     �     �     �  �   �  9   �	  "   �	     �	     �	     
     *
     <
     R
     c
  %   u
     �
     �
     �
     �
     �
  
   �
  7        :     T     l     t  �  �  8   %     ^     x     �     �     �     �     �     �     �     	       !   #     E     L     h     o  	     *   �     �     �     �  $   �            !   *  (   L     u     �     �     �     �     �     �     �     �            �   *  0   �      �  	     !         B     U     h     ~     �  !   �     �     �     �     �     �  
             #     *     1     6     !          3      0      (                 1      "          =   .   6                                         	      :          5       ,   #   &   7       ;   '             -   
             8                 *           %           <   4   9                )   /   $          2             +       

Choose the previous strip to go to the last cached strip. &Create Comic Book Archive... &Save Comic As... &Strip Number: *.cbz|Comic Book Archive (Zip) @option:check Context menu of comic image&Actual Size @option:check Context menu of comic imageStore current &Position Advanced All An error happened for identifier %1. Appearance Archiving comic failed Automatically update comic plugins: Cache Check for new comic strips: Comic Comic cache: Configure... Could not create the archive at the specified location. Create %1 Comic Book Archive Creating Comic Book Archive Destination: Display error when getting comic failed Download Comics Error Handling Failed adding a file to the archive. Failed creating the file with identifier %1. From beginning to ... From end to ... General Get New Comics... Getting comic strip failed: Go to Strip Information Jump to &current Strip Jump to &first Strip Jump to Strip ... Manual range Maybe there is no Internet connection.
Maybe the comic plugin is broken.
Another reason might be that there is no comic for this day/number/string, so choosing a different one might work. Middle-click on the comic to show it at its original size No zip file is existing, aborting. Range: Show arrows only on mouse over Show comic URL Show comic author Show comic identifier Show comic title Strip identifier: The range of comic strips to archive. Update Visit the comic website Visit the shop &website an abbreviation for Number# %1 days dd.MM.yyyy here strip means comic strip&Next Tab with a new Strip in a range: from toFrom: in a range: from toTo: minutes strips per comic Project-Id-Version: plasma_applet_comic
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-07-18 03:20+0200
PO-Revision-Date: 2015-06-09 17:34+0800
Last-Translator: Franklin
Language-Team: Chinese Traditional <kde-i18n-doc@kde.org>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=1; plural=0;
 

選擇前一則漫畫，進入上次快取的漫畫。 建立漫畫歸檔(&C)... 另存漫畫為(&S)... 篇數(&S)： *.cbz|漫畫歸檔 (Zip) 實際大小(&A) 商店目前位置(&P) 進階 全部 辨識器 %1 發生錯誤。 外觀 歸檔漫畫失敗 自動更新漫畫外掛程式： 快取 檢查有沒有新漫畫： 漫畫 漫畫快取： 設定... 無法在指定的位置上建立歸檔。 建立 %1 漫畫歸檔 建立漫畫歸檔 目的地： 取得漫畫失敗時顯示錯誤： 下載漫畫 錯誤處理 新增檔案到歸檔中時失敗 建立辨識器 %1 的檔案時失敗。 開始於... 結束於... 一般 取得新漫畫... 取得漫畫失敗： 跳到某一篇 資訊 跳到目前這一篇(&C) 跳到第一篇(&F) 跳到其他篇... 手動設定範圍 可能網路連線有問題。
可能漫畫外掛程式有問題。 也可能今天或這個數字/字串沒有漫畫，另外選一個可能就好了。 在漫畫上點儀中鍵可以顯示原始大小 沒有 zip 檔，中止運作。 範圍： 只有停留其上時顯示箭頭 顯示漫畫網址 顯示漫畫作者 顯示漫畫辨識碼 顯示漫畫標題 集數辨識器： 要歸檔的漫畫集數範圍。 更新 造訪漫畫網站 造訪商店網站(&W) # %1  天 dd.MM.yyyy 下一則新漫畫(&N) 從： 到：  分  則/每個漫畫 