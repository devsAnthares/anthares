��          �      �       H     I  
   a     l  .   r     �     �      �  *   �        ,   '  ,   T  0   �     �  �  �     [     x       !   �     �     �     �     �  (   �     #     *  $   /     T     	                                              
              &Lock screen on resume: Activation Error Failed to successfully test the screen locker. Immediately Lock Session Lock screen automatically after: Lock screen when waking up from suspension Re&quire password after locking: Spinbox suffix. Short for minutes min  mins Spinbox suffix. Short for seconds sec  secs The global keyboard shortcut to lock the screen. Wallpaper &Type: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:10+0100
PO-Revision-Date: 2016-09-21 21:38+0800
Last-Translator: Jeff Huang <s8321414@gmail.com>
Language-Team: Chinese <kde-i18n-doc@kde.org>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 回復時鎖定螢幕(&L)： 作用 錯誤 測試螢幕鎖定程式失敗。 立即 鎖定工作階段 多久後鎖定螢幕： 從暫停醒來時鎖定螢幕 鎖定多久後需要輸入密碼(&Q)： 分鐘  秒 鎖定螢幕的全域鍵盤捷徑。 桌布型態(&T)： 