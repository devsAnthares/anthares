��          �   %   �      p     q     ~     �     �     �     �     �     �  )   �               !     4     I     ]     m  3   u     �     �  <   �  5     8   T  8   �     �     �     �     �  �  �     �     �     �     �                    2  0   9     j     n     u     �     �     �     �     �     �       -     9   M  !   �  !   �     �     �     �     �                        
                                                	                                                   Add files... Add folder... Background frame Change picture every Choose a folder Choose files Configure plasmoid... General Left click image opens in external viewer Pad Paths Pause on mouseover Preserve aspect crop Preserve aspect fit Randomize items Stretch The image is duplicated horizontally and vertically The image is not transformed The image is scaled to fit The image is scaled uniformly to fill, cropping if necessary The image is scaled uniformly to fit without cropping The image is stretched horizontally and tiled vertically The image is stretched vertically and tiled horizontally Tile Tile horizontally Tile vertically s Project-Id-Version: plasma_applet_org.kde.plasma.mediaframe
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-16 03:31+0100
PO-Revision-Date: 2016-11-03 19:29+0800
Last-Translator: Jeff Huang <s8321414@gmail.com>
Language-Team: Chinese <kde-i18n-doc@kde.org>
Language: Traditional Chinese
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=1; plural=0;
 新增檔案... 新增資料夾... 背景圖框 變更圖片頻率： 選擇資料夾 選擇檔案 設定 plasmoid... 一般 左鍵點選圖片以在外部檢視器中開啟 Pad 路徑 在滑鼠經過時暫停 保留縱橫比，裁切 保留縱橫比，不裁切 隨機項目 伸縮 圖片水平與垂直複製 圖片不變動 影像大小依視框調整 影像大小依視框調整，超過就裁切 影像大小依視框調整，但是保持比例不裁切 圖片水平伸展並垂直平鋪 圖片垂直伸展並水平平鋪 鋪排 水平平鋪 垂直平鋪 秒 