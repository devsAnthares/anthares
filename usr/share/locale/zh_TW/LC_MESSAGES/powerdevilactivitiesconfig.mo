��          �      |      �     �     �     �                 ;  	   \     f     �  &   �     �     �     �  	          �   %  �   �  t   ?  7   �  +   �       �       �     �     �     �     �  -   �     ,  (   3     \     o     �     �     �     �     �  j   �  s   =  g   �     	  0   %	     V	                                      
   	                                                         min Act like Always Define a special behavior Don't use special settings EMAIL OF TRANSLATORSYour emails Hibernate NAME OF TRANSLATORSYour names Never shutdown the screen Never suspend or shutdown the computer PC running on AC power PC running on battery power PC running on low battery Shut down Suspend The Power Management Service appears not to be running.
This can be solved by starting or scheduling it inside "Startup and Shutdown" The activity service is not running.
It is necessary to have the activity manager running to configure activity-specific power management behavior. The activity service is running with bare functionalities.
Names and icons of the activities might not be available. This is meant to be: Act like activity %1Activity "%1" Use separate settings (advanced users only) after Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-09 03:03+0200
PO-Revision-Date: 2016-09-21 21:36+0800
Last-Translator: Jeff Huang <s8321414@gmail.com>
Language-Team: Chinese <kde-i18n-doc@kde.org>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=1; plural=0;
  分鐘 行為類似 總是 定義特殊行為 不要使用特殊設定 franklin@goodhorse.idv.tw, s8321414@gmail.com 冬眠 Frank Weng (a.k.a. Franklin), Jeff Huang 永不關閉螢幕 永不暫停或關閉電腦 PC 接市電電源 PC 使用電池 PC 電池電力不足 關機 暫停 電源管理服務似乎尚未執行。
您可以在「啟動與關閉」裡啟動或將它加入排程。 此活動服務並未執行。
必須要先執行活動管理員，才能設定活動相關的電源管理行為。 此活動服務正執行中，但是缺少一些功能。
活動的名稱與圖示可能無法使用。 活動 "%1" 使用分別的設定（適合進階使用者） 多久之後： 