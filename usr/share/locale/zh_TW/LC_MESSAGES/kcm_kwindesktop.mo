��    *      l  ;   �      �     �     �     �  
   D  +   O  
   {     �     �     �      �     �     �       	           �   @  e   �  *   D  H   o     �     �     �     �       )     ;   <  	   x     �  (   �     �     �     �          7     L     c  	   ~     �  #   �     �     �  �  �     �
     �
  f   �
  	   #  "   -  	   P     Z     g     z     �     �     �     �  	   �     �  �   �  9   p  (   �  A   �          '     .     <  	   J  %   T  1   z     �     �  !   �     �     
     #     <     U     h     �     �     �     �     �     �        
          *   &      #      '         $                               (            %                       	       !                            )              "                                          msec &Number of desktops: <h1>Multiple Desktops</h1>In this module, you can configure how many virtual desktops you want and how these should be labeled. Animation: Assigned global Shortcut "%1" to Desktop %2 Desktop %1 Desktop %1: Desktop Effect Animation Desktop Names Desktop Switch On-Screen Display Desktop Switching Desktop navigation wraps around Desktops Duration: EMAIL OF TRANSLATORSYour emails Enable this option if you want keyboard or active desktop border navigation beyond the edge of a desktop to take you to the opposite edge of the new desktop. Enabling this option will show a small preview of the desktop layout indicating the selected desktop. Here you can enter the name for desktop %1 Here you can set how many virtual desktops you want on your KDE desktop. KWin development team Layout N&umber of rows: NAME OF TRANSLATORSYour names No Animation No suitable Shortcut for Desktop %1 found Shortcut conflict: Could not set Shortcut %1 for Desktop %2 Shortcuts Show desktop layout indicators Show shortcuts for all possible desktops Switch One Desktop Down Switch One Desktop Up Switch One Desktop to the Left Switch One Desktop to the Right Switch to Desktop %1 Switch to Next Desktop Switch to Previous Desktop Switching Walk Through Desktop List Walk Through Desktop List (Reverse) Walk Through Desktops Walk Through Desktops (Reverse) Project-Id-Version: kcm_kwindesktop
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-06 05:35+0100
PO-Revision-Date: 2014-09-14 09:19+0800
Last-Translator: Franklin
Language-Team: Chinese Traditional <kde-i18n-doc@kde.org>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
dot tw>
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=1; plural=0;
  毫秒 桌面數量(&N)： <h1>多重桌面</h1>此模組讓您設定您要使用多少個虛擬桌面，以及它們的標籤。 動畫： 指定全域捷徑 %1 給桌面 %2 桌面 %1 桌面 %1： 桌面效果動畫 桌面名稱 桌面切換螢幕顯示 桌面切換 桌面循環切換 桌面 期間： franklin@goodhorse.idv.tw 如果您想要鍵盤或使用桌面邊框導覽在您超過桌面邊緣時帶您到對應邊緣的新桌面，請啟用此選項。 開啟此選項將顯示選取桌面佈局的小預覽。 您可以在此輸入桌面 %1 的名稱 您可以設定您要多少個虛擬桌面放在您的 KDE 中。 KWin 開發團隊 佈局 列數(&U)： Franklin Weng 無動畫 找不到適於做桌面 %1 的捷徑 捷徑衝突：無法設定捷徑 %1 給桌面 %2 捷徑 顯示桌面佈局指示器 顯示所有桌面可能的捷徑 切換到下面的桌面 切換到上面的桌面 往左移動一個桌面 往右移動一個桌面 切換到桌面 %1 切換到下一個桌面 切換到上一個桌面 切換 走過桌面清單 走過桌面清單 (反轉) 走過桌面 走過桌面 (反轉) 