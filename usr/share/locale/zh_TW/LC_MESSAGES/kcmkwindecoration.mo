��            )   �      �  !   �  "   �  '   �  ,     #   ;  &   _  !   �  &   �  '   �     �                 V   $  1   {     �  *   �     �        
     
   "     -     6     ;     D     T     [     a     g  �  p     !     (     ,     9     F     M     Z     a  	   n     x     �     �     �  G   �  +   �     !  $   .     S     o     �     �  	   �     �  	   �     �     �     �     �  	   �                                                                          
                                                    	           @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Borders @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large Application menu Border si&ze: Buttons Close Close by double clicking:
 To open the menu, keep the button pressed until it appears. Close windows by double clicking &the menu button Context help Drag buttons between here and the titlebar Drop here to remove button Get New Decorations... Keep above Keep below Maximize Menu Minimize On all desktops Search Shade Theme Titlebar Project-Id-Version: kcmkwindecoration
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-04-14 02:49+0200
PO-Revision-Date: 2016-11-03 16:26+0800
Last-Translator: Jeff Huang <s8321414@gmail.com>
Language-Team: Chinese <kde-i18n-doc@kde.org>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
dot tw>
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=1; plural=0;
 巨大 大 沒有外框 沒有邊框 普通 超過大小 極小 非常巨大 非常大 應用程式選單 邊界大小(&Z)： 按鈕 關閉 雙擊關閉：
 要開啟選單，請按住按鍵直到選單出現。 雙擊視窗選單按鈕時關閉視窗(&T) 內容說明 在這裡跟標題列間拖曳按鈕 丟在這裡以移除按鈕 取得新裝飾... 保持在上 保持在下 最大化 選單 最小化 在所有桌面 搜尋 陰影 主題 標題列 