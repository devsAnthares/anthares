��    "      ,  /   <      �      �          &  -   E  #   s  #   �  ?   �  1   �     -     A  H   F  L   �  V   �  N   3     �  
   �     �     �     �     �  2   �  
     )     8   E  #   ~  .   �  -   �  D   �  6   D  0   {  A   �  =   �  9   ,  �  f     �	     
     &
  "   ?
  	   b
  	   l
     v
     �
     �
     �
     �
     �
     �
     �
     �
     �
               .  	   J     T     f  !   s     �  !   �  '   �  	   �  	   �     	                    )                                                           !       	                      
                                                           "           %1 notification %1 notifications %1 of %2 %3 %1 running job %1 running jobs &Configure Event Notifications and Actions... 10 seconds ago, keep short10 s ago 30 seconds ago, keep short30 s ago A button tooltip; expands the item to show detailsShow Details A button tooltip; hides item detailsHide Details Clear Notifications Copy Either just 1 dir or m of n dirs are being processed1 dir %2 of %1 dirs Either just 1 file or m of n files are being processed1 file %2 of %1 files How much many bytes (or whether unit in the locale has been copied over total%1 of %2 Indicator that there are more urls in the notification than previews shown+%1 Information Job Failed Job Finished No new notifications. No notifications or jobs Open... Placeholder is job name, eg. 'Copying'%1 (Paused) Select All Show application and system notifications Speed and estimated time to completion%1 (%2 remaining) Track file transfers and other jobs Use custom position for the notification popup minutes ago, keep short%1 min ago %1 min ago notification was added n days ago, keep short%1 day ago %1 days ago notification was added yesterday, keep shortYesterday notification was just added, keep shortJust now placeholder is row description, such as Source or Destination%1: the job, which can be anything, failed to complete%1: Failed the job, which can be anything, has finished%1: Finished Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:07+0100
PO-Revision-Date: 2016-12-04 16:27+0800
Last-Translator: Jeff Huang <s8321414@gmail.com>
Language-Team: Chinese <kde-i18n-doc@kde.org>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 %1 個通知 第 %1 個，共 %2 個 %3 %1 個執行中的工作 設定事件通知與動作(&C)... 10 秒前 30 秒前 顯示詳情 隱藏詳情 清除通知 複製 %2 個目錄 / 共 %1 個 %2 個檔案 / 共 %1 個 第 %1 個，共 %2 個 +%1 資訊 工作已失敗 工作已完成 沒有新通知。 沒有通知也沒有工作 開啟... %1【已暫停】 全部選擇 顯示應用程式與系統通知 %1（還有 %2 個） 追蹤檔案傳輸與其他工作 通知彈出式視窗使用自訂位置 %1 分前 %1 天前 昨天 剛才 %1： %1：失敗 %1：已完成 