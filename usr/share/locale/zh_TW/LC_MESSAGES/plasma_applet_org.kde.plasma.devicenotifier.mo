��          �      l      �  3   �  $     �   :     �  4   �     �  #        =  �   E  �   �  +   �     �     �     �  1     (   3     \  �   s  $   �  (     �  F     �  	             +  0   8     i  $   �     �  �   �  �   7	  '   �	     	
     
     2
     K
  *   a
     �
     �
     �
     �
                                                   	   
                                            1 action for this device %1 actions for this device @info:status Free disk space%1 free Accessing is a less technical word for Mounting; translation should be short and mean 'Currently mounting this device'Accessing... All devices Click to access this device from other applications. Click to eject this disc. Click to safely remove this device. General It is currently <b>not safe</b> to remove this device: applications may be accessing it. Click the eject button to safely remove this device. It is currently <b>not safe</b> to remove this device: applications may be accessing other volumes on this device. Click the eject button on these other volumes to safely remove this device. It is currently safe to remove this device. Most Recent Device No Devices Available Non-removable devices only Open auto mounter kcmConfigure Removable Devices Open popup when new device is plugged in Removable devices only Removing is a less technical word for Unmounting; translation shoud be short and mean 'Currently unmounting this device'Removing... This device is currently accessible. This device is not currently accessible. Project-Id-Version: plasma_applet_devicenotifier
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2016-11-03 19:17+0800
Last-Translator: Jeff Huang <s8321414@gmail.com>
Language-Team: Chinese <kde-i18n-doc@kde.org>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=1; plural=0;
 此裝置的 %1 個動作 剩餘 %1 存取中... 所有裝置 點擊以從其它應用程式存取此裝置。 點擊以退出碟片。 點擊以安全地移除此裝置。 一般 目前無法<b>安全地</b>移除此裝置：可能還有應用程式正在使用它。點擊退出鍵可以安全地移除此裝置。 目前無法<b>安全地</b>移除此裝置：可能還有應用程式正在使用此裝置的某些部份。對該部份點擊退出鍵可以安全地移除此裝置。 目前可以安全地移除此裝置。 上次插入的裝置 沒有可用裝置 只有不可移除裝置 設定可移除裝置 當新裝置插入時開啟彈出式通知 只有可移除裝置 移除中... 此裝置目前可存取。 此裝置目前無法存取。 