��          �      �           	          "     4  	   A     K  a   R     �  
   �  	   �     �     �                 3      T     u  	   �     �     �     �  7   �  �  �     �     �     �     �     �     �  W   �     3     J  	   X     b     s     �     �     �     �     �     �  	   �     �  	   �  *                                                                  
                              	           %1 (%2) %1 (long format) %1 (short format) %1 - %2 (%3) &Numbers: &Time: <h1>Formats</h1>You can configure the formats used for time, dates, money and other numbers here. Co&llation and Sorting: Currenc&y: Currency: De&tailed Settings Format Settings Changed Measurement &Units: Measurement Units: Measurement comboboxImperial UK Measurement comboboxImperial US Measurement comboboxMetric No change Numbers: Re&gion: Time: Your changes will take effect the next time you log in. Project-Id-Version: kcmlocale
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-11 03:20+0100
PO-Revision-Date: 2016-11-03 16:18+0800
Last-Translator: Jeff Huang <s8321414@gmail.com>
Language-Team: Chinese <kde-i18n-doc@kde.org>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=1; plural=0;
 %1 (%2) %1 (長格式) %1 (短格式) %1 - %2 (%3) 數字(&N)： 時間(&T)： <h1>格式</h1>您可以在此設定時間、日期、貨幣與其他數值的格式。 整理與排序(&L)： 貨幣(&Y)： 貨幣： 詳細設定(&T) 格式設定已變更 度量衡系統(&U)： 度量衡系統： 英制系統 美式系統 公制 沒有變更 數字： 區域(&G)： 時間： 您的變更會在下次登入時生效。 