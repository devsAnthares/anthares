��    &      L  5   |      P  9   Q     �     �     �     �     �     �  7     	   G     Q     Z  �   t     C     O     b     z     �     �     �     �     �  #   �  %   �          6  �   F  P     o   c  5   �  �   	     �     �     �     �  g   �  )   <	  ?   f	  �  �	     S     d     {     �     �     �     �  	   �  	   �  	   �     �  �     	   �     �     �  	   �     �     �  	   �       	        &     *     .     D  C   T  '   �  )   �  *   �  v        �     �     �  	   �  `   �     +     /               %       
                           #                     !                "       $                            &   	                                                      %1 is remaining time, %2 is percentage%1 Remaining (%2%) %1% Battery Remaining %1%. Charging %1%. Plugged in %1%. Plugged in, not Charging &Configure Power Saving... Battery and Brightness Battery is currently not present in the bayNot present Capacity: Charging Configure Power Saving... Disabling power management will prevent your screen and computer from turning off automatically.

Most applications will automatically suppress power management when they don't want to have you interrupted. Discharging Display Brightness Enable Power Management Fully Charged General Keyboard Brightness Model: No Batteries Available Not Charging Placeholder is battery capacity%1% Placeholder is battery percentage%1% Power management is disabled Show percentage Some Application and n others are currently suppressing PM%2 and %1 other application are currently suppressing power management. %2 and %1 other applications are currently suppressing power management. Some Application is suppressing PM%1 is currently suppressing power management. Some Application is suppressing PM: Reason provided by the app%1 is currently suppressing power management: %2 The battery applet has enabled system-wide inhibition The capacity of this battery is %1%. This means it is broken and needs a replacement. Please contact your hardware vendor for more details. Time To Empty: Time To Full: Used for measurement100% Vendor: Your notebook is configured not to suspend when closing the lid while an external monitor is connected. battery percentage below battery icon%1% short symbol to signal there is no battery curently available- Project-Id-Version: plasma_applet_battery
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-03 03:15+0100
PO-Revision-Date: 2017-03-27 09:19+0800
Last-Translator: Jeff Huang <s8321414@gmail.com>
Language-Team: Chinese <kde-i18n-doc@kde.org>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=1; plural=0;
  還剩 %1 (%2%) 電池電力還剩 %1% %1%。充電中 %1%。已插入 %1%。已插入，未充電 設定省電模式(&C)... 電池與亮度 不存在 容量： 充電中 設定省電模式... 關閉電源管理會避免您的電腦與螢幕自動關閉。

大部份的應用程式，如果不希望您中斷的話，都會自動關閉電源管理。 放電中 顯示亮度 開啟電源管理 已充飽 一般 鍵盤亮度 型號： 沒有可用的電池 未充電 %1% %1% 電源管理已關閉 顯示百分比 %2 與其他 %1 個應用程式目前讓電源管理暫停運作。 %1 目前讓電源管理暫停運作。 %1 目前讓電源管理暫停運作：%2 電池小程式已開啟系統睡眠模式 您的電池容量為 %1%。這表示電池已損壞，需要更換。請聯繫您的硬體廠商詢問更多詳情。 電力剩餘時間： 充到飽所需時間： 100% 廠商： 您的筆記型電腦設定成在外接螢幕時，蓋起上蓋時並不會進入暫停模式。 %1% - 