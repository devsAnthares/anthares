��    B      ,  Y   <      �  i   �  m        y  b   �     �     	  6        O  7   _     �     �  	   �     �  (   �  )   �  )   (     R     o  Y   u     �     �  �   �      y	  .   �	     �	  
   �	     �	     �	     
     
     !
     5
     B
     J
     c
     r
     w
     �
     �
     �
     �
     �
  	   �
  
   �
     �
     �
  	     
     
   *     5     B  	   `     j     o  
   �  �   �     (  8   8  �   q     �  8   	  ,   B  ,   o  %   �     �  �  �  B   �  e   �     C  Q   \     �     �  *   �     �  *        1     8  	   F     P  $   n  $   �  $   �     �     �     �          '  r   :     �  '   �     �     �  	   �       	                  ,  	   9     C     Z  	   i     s     �     �     �  	   �     �     �     �     �     �     �     �  	               	   3     =     D     Z  �   h     �  6     k   ;     �  *   �  -   �  -     $   ;     `     $   !   %          ;   ,          B                 8   #                 7   9       +              )   ?   -   <         6                                    *   2       >                1       @       .   5   3   :   A         &      /                4              0      "             =   (   '         	   
           @info User changed Phonon backendTo apply the backend change you will have to log out and back in again. A list of Phonon Backends found on your system.  The order here determines the order Phonon will use them in. Apply Device List To... Apply the currently shown device preference list to the following other audio playback categories: Audio Hardware Setup Audio Playback Audio Playback Device Preference for the '%1' Category Audio Recording Audio Recording Device Preference for the '%1' Category Backend Colin Guthrie Connector Copyright 2006 Matthias Kretz Default Audio Playback Device Preference Default Audio Recording Device Preference Default Video Recording Device Preference Default/Unspecified Category Defer Defines the default ordering of devices which can be overridden by individual categories. Device Configuration Device Preference Devices found on your system, suitable for the selected category.  Choose the device that you wish to be used by the applications. EMAIL OF TRANSLATORSYour emails Failed to set the selected audio output device Front Center Front Left Front Left of Center Front Right Front Right of Center Hardware Independent Devices Input Levels Invalid KDE Audio Hardware Setup Matthias Kretz Mono NAME OF TRANSLATORSYour names Phonon Configuration Module Playback (%1) Prefer Profile Rear Center Rear Left Rear Right Recording (%1) Show advanced devices Side Left Side Right Sound Card Sound Device Speaker Placement and Testing Subwoofer Test Test the selected device Testing %1 The order determines the preference of the devices. If for some reason the first device cannot be used Phonon will try to use the second, and so on. Unknown Channel Use the currently shown device list for more categories. Various categories of media use cases.  For each category, you may choose what device you prefer to be used by the Phonon applications. Video Recording Video Recording Device Preference for the '%1' Category  Your backend may not support audio recording Your backend may not support video recording no preference for the selected device prefer the selected device Project-Id-Version: kcm_phonon
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2012-03-03 10:19+0800
Last-Translator: Franklin Weng <franklin@mail.everfocus.com.tw>
Language-Team: Chinese Traditional <zh-l10n@linux.org.tw>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=1; plural=0;
 要套用後端介面變更，您必須先登出再重新登入。 在您的系統上找到了一份 Phonon 後端清單。清單中的順序會決定其使用順序。 套用裝置清單到... 將目前顯示的裝置喜好設定清單套用到以下的音效播放類別： 音效硬體設定 音效播放 %1 類別的音效播放裝置喜好設定 音效錄製 %1 類別的音效錄製裝置喜好設定 後端 Colin Guthrie 連接器 Copyright 2006 Matthias Kretz 預設音效播放裝置喜好設定 預設音效錄製裝置喜好設定 預設影像錄製裝置喜好設定 預設/未指定類別 推遲 定義預設的裝置順序。 裝置設定 裝置喜好設定 在您的系統上找到了裝置，可用於選取的類別。請選擇此應用程式要使用哪一個裝置。 franklin@goodhorse.idv.tw 設定選取的音效輸出裝置失敗 前中 前左 前左中 前右 前右中 硬體 獨立裝置 輸入等級 不合法 KDE 音效硬體設定 Matthias Kretz 單聲道 Frank Weng (a.k.a. Franklin) Phonon 設定模組 播放 (%1) 預設 設定檔 後中 後左 後右 錄製 (%1) 顯示進階的裝置 左側 右側 音效卡 音效裝置 喇叭放置與測試 重低音 測試 測試選取的裝置 測試 %1 中 此順序為您想要使用的裝置的順序。如果第一個裝置無法使用，Phonon 會試著使用第二個裝置，依此類推。 未知的聲道 將目前顯示的裝置清單套用到更多類別。 各種類別的媒體使用案例。每個類別您可以選擇 Phonon 應用程式要使用哪個裝置。 影像錄製 %1 類別的影像錄製裝置喜好設定 您的後端介面可能不支援音效錄製 您的後端介面可能不支援影像錄製 此選擇的裝置沒有喜好設定 預設使用選取的裝置 