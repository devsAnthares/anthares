��    &      L  5   |      P     Q     l     y     �     �     �     �     �     �     �     �     �  &   �               #     ,     3     ?     M     U     ]     d     s     �     �     �     �     �     �     �     �     �     �  
   �            �       �     �     �          	               !     /     C  
   L     W  9   ^     �     �     �     �     �  0   �            	        %     8     N     a     n  	   �     �     �     �  	   �     �     �     �     	  -   	                           	      #      $                                                    &                                          !          %                            "   
    Abbreviation for secondss Application: Average clock: %1 MHz Bar Buffers: CPU CPU %1 CPU monitor CPU%1: %2% @ %3 Mhz CPU: %1% CPUs separately Cache Cache Dirty, Writeback: %1 MiB, %2 MiB Cache monitor Cached: Circular Colors Compact Bar Dirty memory: General IOWait: Memory Memory monitor Memory: %1/%2 MiB Monitor type: Nice: Set colors manually Show: Swap Swap monitor Swap: %1/%2 MiB Sys: System load Update interval: Used swap: User: Writeback memory: Project-Id-Version: plasma_applet_systemloadviewer
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-29 03:31+0200
PO-Revision-Date: 2017-03-27 09:31+0800
Last-Translator: Jeff Huang <s8321414@gmail.com>
Language-Team: Chinese <kde-i18n-doc@kde.org>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
dot tw>
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=1; plural=0;
 秒 應用程式： 平均時脈：%1 MHz 條狀 緩衝區： CPU CPU %1 CPU 監視器 CPU%1: %2% @ %3 Mhz CPU: %1% 個別 CPU 快取 尚未寫入及正在寫入磁碟的快取：%1MiB, %2MiB 快取監視器 已快取： 圓形 顏色 簡潔長條 已更改，正等待寫入時碟的記憶體： 一般 輸出入等待： 記憶體 記憶體監視器 記憶體：%1/%2 MiB 監視器型態： 優先權： 手動設定顏色 顯示： 置換空間 置換空間監視器 置換空間：%1/%2 MB 系統： 系統負載 更新間隔： 已使用置換空間： 使用者： 正寫入磁碟但尚未完成的記憶體： 