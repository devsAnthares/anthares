��          4      L       `   $   a   /   �   �  �   ,   �  6   �                    Finds Konsole sessions matching :q:. Lists all the Konsole sessions in your account. Project-Id-Version: plasma_runner_konsolesessions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2009-06-09 15:15+0800
Last-Translator: Frank Weng (a.k.a. Franklin) <franklin at goodhorse dot idv dot tw>
Language-Team: Chinese Traditional <zh-l10n@linux.org.tw>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 0.3
Plural-Forms: nplurals=1; plural=0;
 尋找符合 :q: 的 Konsole 工作階段。 列出您的帳號中的所有 Konsole 工作階段。 