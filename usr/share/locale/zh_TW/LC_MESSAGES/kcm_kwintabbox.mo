��    "      ,  /   <      �  
   �               ,     >     J  !   V     x     �     �     �     �     �  L   �     #     +     J     Y     u     z     �     �     �     �  	   �     �     �     �  �   �  F   �     �     �     �  �  �     �     �     �     �     �     �                    "     8     H     X  ?   n     �     �     �     �      	  	   	  '   	     9	     F	     M	     T	     [	     q	     �	  i   �	  3   �	     ,
     9
  	   F
                    !                                            	                                                           "               
                    Activities All other activities All other desktops All other screens All windows Alternative An example Desktop NameDesktop 1 Content Current activity Current application Current desktop Current screen Filter windows by Focus policy settings limit the functionality of navigating through windows. Forward Get New Window Switcher Layout Hidden windows Include "Show Desktop" icon Main Minimization Only one window per application Recently used Reverse Screens Shortcuts Show selected window Sort order: Stacking order The currently selected window will be highlighted by fading out all other windows. This option requires desktop effects to be active. The effect to replace the list window when desktop effects are active. Virtual desktops Visible windows Visualization Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2012-05-31 13:17+0800
Last-Translator: Franklin Weng <franklin@goodhorse.idv.tw>
Language-Team: Chinese Traditional <zh-l10n@linux.org.tw>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
dot tw>
X-Generator: Lokalize 1.1
Plural-Forms: nplurals=1; plural=0;
 活動 所有其他活動 所有其他桌面 所有其他螢幕 所有視窗 替代 桌面 1 內容 目前活動 目前的應用程式 目前的桌面 目前的螢幕 過濾視窗依據： 焦點政策的設定會限制透過視窗導覽時的功能。 往前 取得新視窗切換器佈局 隱藏視窗 包含「顯示桌面」圖示 主要 最小化 每個應用程式只顯示一個視窗 最近使用 反轉 螢幕 捷徑 顯示選取的視窗 排序順序： 堆疊順序 會用將其它視窗淡出的方式來突顯目前選取的視窗。此功能需要開啟桌面效果。 啟動桌面效果時，取代列出視窗的效果 虛擬桌面 可見視窗 視覺化 