��    +      t  ;   �      �  (   �     �  �   �     z     �     �     �     �     �     �     �     �     �     �                @     H     V     d     v     �     �     �     �     �     �          	          .     A     N     ^     s      |  7   �     �     �     �            �       �     �     �     �     �     �     �     	     	     	     1	     A	     N	     U	     \	  -   u	     �	     �	     �	     �	     �	     �	     �	     
     "
     0
     >
     X
     _
     l
     �
     �
     �
     �
     �
     �
  3   �
     )     0     C  	   Y     c                       %   *   +      !   "                                                            
                (                             )      &      #      '                             $   	    %1 is the name of a session%1 (Wayland) ... @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) Advanced Author Auto &Login Background: Clear Image Commands Could not decompress archive Cursor Theme: Customize theme Default Description Download New SDDM Themes EMAIL OF TRANSLATORSYour emails General Get New Theme Halt Command: Install From File Install a theme. Invalid theme package Load from file... Login screen using the SDDM Maximum UID: Minimum UID: NAME OF TRANSLATORSYour names Name No preview available Reboot Command: Relogin after quit Remove Theme SDDM KDE Config SDDM theme installer Session: The default cursor theme in SDDM The theme to install, must be an existing archive file. Theme Unable to install theme Uninstall a theme. User User: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2017-06-29 21:03+0800
Last-Translator: Jeff Huang <s8321414@gmail.com>
Language-Team: Chinese <kde-i18n-doc@kde.org>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 %1 (Wayland) ... （可用的大小：%1） 進階 作者 自動登入(&L) 背景顏色： 清除影像 指令 無法解壓縮封存檔 游標主題： 自訂主題 預設 描述 下載新的 SDDM 主題 franklin@goodhorse.idv.tw, s8321414@gmail.com 一般 取得新主題 中止指令： 從檔案安裝 安裝主題。 無效的主題包 從檔案載入... SDDM 登入畫面 最大 UID： 最小 UID： Franklin Weng, Jeff Huang 名稱 無法預覽 重新啟動指令： 離開後重新登入 移除主題 SDDM KDE 設定 SDDM 主題安裝程式 工作階段： SDDM 預設游標主題 要安裝的主題，必須為既有的壓縮檔。 主題 無法安裝主題 解除安裝主題。 使用者 使用者： 