��            )         �     �  "   �     �  !   �  !        4  
   J     U     p     �  !   �     �  0   �  8        ?  !   ]          �     �     �     �                '  
   /  
   :  
   E  &   P     w     �  �  �     :     A     [     {     �     �     �     �     �     �  $        7  3   S  3   �  !   �  '   �  !   	  !   '	     I	     _	     {	  !   �	     �	     �	     �	     �	  	   �	     �	     �	     
                                                                                                 	                       
                 ms &Keyboard accelerators visibility: &Top arrow button type: Always Hide Keyboard Accelerators Always Show Keyboard Accelerators Anima&tions duration: Animations Bottom arrow button t&ype: Breeze Settings Center tabbar tabs Drag windows from all empty areas Drag windows from titlebar only Drag windows from titlebar, menubar and toolbars Draw a thin line to indicate focus in menus and menubars Draw focus indicator in lists Draw frame around dockable panels Draw frame around page titles Draw frame around side panels Draw slider tick marks Draw toolbar item separators Enable animations Enable extended resize handles Frames General No Buttons One Button Scrollbars Show Keyboard Accelerators When Needed Two Buttons W&indows' drag mode: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:42+0200
PO-Revision-Date: 2017-11-11 20:28+0800
Last-Translator: Franklin Weng <franklin@goodhorse.idv.tw>
Language-Team: Chinese <zh-l10n@linux.org.tw>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 毫秒 快捷鍵可見度(&K)： 頂端箭頭按鍵型態(&T)： 總是隱藏鍵盤加速器 總是顯示快捷鍵 動畫期間(&T)： 動畫 底端箭頭按鍵型態(&Y)： Breeze 設定 將分頁列的分頁置中 可從所有空白區域拖曳視窗 只從標題列拖曳視窗 可從標題列、選單列與工具列拖曳視窗 畫一條細線在選單與選單列中指示焦點 在清單中繪製焦點指示器 在可嵌入式面板週圍畫上框架 在頁面標題週圍畫上框架 在側邊面板週圍畫上框架 劃出滑動器刻度 畫工具列元件分隔線 開啟動畫 開啟延伸的調整大小元件 框架 一般 沒有按鍵 一個按鍵 捲軸列 需要時顯示鍵盤加速器 兩個按鍵 視窗的拖曳模式(&I)： 