��          �   %   �      @     A  !   Y      {     �      �     �     �  +        =     N     [  (   z     �  ,   �  7   �     !  7   :     r  ]   �  1   �  	   "     ,  "   ?  5   b  �  �     0  #   J     n     �     �     �     �  "   �     
       
   '  $   2     W  *   s  3   �     �  ;   �     %	  6   E	  &   |	     �	     �	  #   �	  ,   �	                    
         	                                                                                                 Cannot start initramfs. Cannot start update-alternatives. Configure Plymouth Splash Screen Download New Splash Screens EMAIL OF TRANSLATORSYour emails Get New Boot Splash Screens... Initramfs failed to run. Initramfs returned with error condition %1. Install a theme. Marco Martin NAME OF TRANSLATORSYour names No theme specified in helper parameters. Plymouth theme installer Select a global splash screen for the system The theme to install, must be an existing archive file. Theme %1 does not exist. Theme corrupted: .plymouth file not found inside theme. Theme folder %1 does not exist. This module lets you configure the look of the whole workspace with some ready to go presets. Unable to authenticate/execute the action: %1, %2 Uninstall Uninstall a theme. update-alternatives failed to run. update-alternatives returned with error condition %1. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-28 03:05+0200
PO-Revision-Date: 2017-06-29 21:03+0800
Last-Translator: Jeff Huang <s8321414@gmail.com>
Language-Team: Chinese <kde-i18n-doc@kde.org>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 無法啟動 initramfs。 無法啟動 update-alternatives。 設定 Plymouth 啟動畫面 下載新啟動畫面 s8321414@gmail.com 取得新啟動畫面... initramfs 執行失敗。 Initramfs 傳回錯誤代碼 %1。 安裝主題。 Marco Martin Jeff Huang 在助手程式中未指定主題。 Plymouth 主題安裝程式 為系統選擇一個全域的啟動畫面 要安裝的主題，必須是既存的壓縮檔。 主題 %1 不存在。 主題已損毀：在主題中找不到 .plymouth 檔案。 主題資料夾 %1 不存在。 這個模組讓您設定整個工作空間的外觀。 無法認證/執行此動作：%1，%2 解除安裝 解除安裝主題。 update-alternatives 執行失敗。 update-alternatives 傳回錯誤代碼 %1。 