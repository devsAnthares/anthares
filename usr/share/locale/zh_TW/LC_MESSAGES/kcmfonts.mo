��    ,      |  ;   �      �     �    �  L   �  ]   ,  /   �     �     �     �     �               (     >     M  I   ^     �     �  *   �     �                /  !   I  "   k     �  6   �  *   �     	     	  #   '	     K	     b	     u	     �	     �	     �	     �	     �	     �	     �	     
  *   *
  *   U
  �  �
     (  �  .  Z   �  i   1  *   �     �     �     �        	             8     N     T  7   n     �     �     �     �     �                 !        >  8   W  6   �  
   �  
   �     �     �     �     �     �  	                        '     +  	   /     9     F            "                             *      '                      &              
   !       )                                     	                     (      #   $   ,         +              %           to  <p>If you have a TFT or LCD screen you can further improve the quality of displayed fonts by selecting this option.<br />Sub-pixel rendering is also known as ClearType(tm).<br /> In order for sub-pixel rendering to work correctly you need to know how the sub-pixels of your display are aligned.</p> <p>On TFT or LCD displays a single pixel is actually composed of three sub-pixels, red, green and blue. Most displays have a linear ordering of RGB sub-pixel, some have BGR.<br /> This feature does not work with CRT monitors.</p> <p>Some changes such as DPI will only affect newly started applications.</p> <p>Some changes such as anti-aliasing or DPI will only affect newly started applications.</p> A non-proportional font (i.e. typewriter font). Ad&just All Fonts... BGR Click to change all fonts Configure Anti-Alias Settings Configure... E&xclude range: Font Settings Changed Font role%1:  Force fonts DPI: Hinting is a process used to enhance the quality of fonts at small sizes. Hinting style: RGB Smallest font that is still readable well. Sub-pixel rendering type: Use a&nti-aliasing: Use anti-aliasingDisabled Use anti-aliasingEnabled Use anti-aliasingSystem Settings Used by menu bars and popup menus. Used by the window titlebar. Used for normal text (e.g. button labels, list items). Used to display text beside toolbar icons. Vertical BGR Vertical RGB abbreviation for unit of points pt font usageFixed width font usageGeneral font usageMenu font usageSmall font usageToolbar font usageWindow title full hintingFull medium hintingMedium no hintingNone no subpixel renderingNone slight hintingSlight use system hinting settingsSystem default use system subpixel settingSystem default Project-Id-Version: kcmfonts
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-24 05:54+0100
PO-Revision-Date: 2017-03-27 09:14+0800
Last-Translator: Jeff Huang <s8321414@gmail.com>
Language-Team: Chinese <kde-i18n-doc@kde.org>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
dot tw>
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=1; plural=0;
  至  <p>如果您使用 TFT 或 LCD 螢幕，您可以選擇此選項以增進顯示字型的品質。<br />子像素成像（Sub-pixel rendering）又叫做 ClearType(tm)。<br />為了使子像素成像正常運作，你必需知道它的顯示方式。</p> <p>在 TFT 或 LCD 顯示一個 pixel，實際是由三個紅、錄、藍的子像素所組成。大部份顯示是 RGB 三線性排列，但亦有 BGR 的方式。</p> <p>有一些像是 DPI 設定上的變更，只會對於新啟動的程式有作用。</p> <p>有一些像是平滑字型或 DPI 設定上的變更，只會對於新啟動的程式有作用。</p> 非調合字型 (例如打字機字型)。 調整所有字型...(&J) BGR 點選以改變所有字型 設定平滑字 設定... 排除字型大小範圍:(&X) 字型設定已改變 %1： 強制指定字型 DPI： 提示（Hinting）是用來加強小字型的質感。 提示風格: RGB 可讀的最小字體 子像素成像型態： 使用平滑字型(&N)： 關閉 開啟 系統設定 用於選單列與彈出選單。 用於視窗標題列。 用於一般文字 (例如按鍵標籤, 列表項目)。 用於工具列圖示以外的文字顯示的字型。 垂直 BGR 垂直 RGB  點  固定寬度 一般 選單 小 工具列 視窗標題 完整 中等 無 無 微小的 系統預設 系統預設值 