��    >        S   �      H     I     R     [     j     |     �  �  �  �   y  -   	  )   3	  -   ]	  +   �	  r   �	  	   *
  	   4
     >
     V
     ^
     g
  
   t
     
     �
     �
     �
      �
     �
     �
     �
      �
            r   :  5   �     �     �                -     L  	   Q     [     a     i  (   v     �     �     �     �     �       +     3   9     m     u     �     �  "   �  S   �  )        9  �   E  �  #     �     �     �     �  
          �  )  {   �  0   ,     ]  	   d     n  Z   u  
   �     �     �                 	   #     -     4  	   ;     E  %   d     �     �     �  !   �     �     �  k   �     X     w     �     �     �     �     �  	   �     �     �     �  !        -     =     V     o     �  	   �  *   �  0   �     �       	             '  D   @  !   �     �  �   �     )            5                        
   8             2   -          $   %      ;   4              <      1   7                 #   6       &       "       	            3      ,            :   (       9                 =   0   '   /              .       +       *                 >         !        &Amount: &Effect: &Second color: &Semi-transparent &Theme (c) 2000-2003 Geert Jansen <h1>Icons</h1>This module allows you to choose the icons for your desktop.<p>To choose an icon theme, click on its name and apply your choice by pressing the "Apply" button below. If you do not want to apply your choice you can press the "Reset" button to discard your changes.</p><p>By pressing the "Install Theme File..." button you can install your new icon theme by writing its location in the box or browsing to the location. Press the "OK" button to finish the installation.</p><p>The "Remove Theme" button will only be activated if you select a theme that you installed using this module. You are not able to remove globally installed themes here.</p><p>You can also specify effects that should be applied to the icons.</p> <qt>Are you sure you want to remove the <strong>%1</strong> icon theme?<br /><br />This will delete the files installed by this theme.</qt> <qt>Installing <strong>%1</strong> theme</qt> @label The icon rendered as activeActive @label The icon rendered as disabledDisabled @label The icon rendered by defaultDefault A problem occurred during the installation process; however, most of the themes in the archive have been installed Ad&vanced All Icons Antonio Larrosa Jimenez Co&lor: Colorize Confirmation Desaturate Description Desktop Dialogs Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Effect Parameters Gamma Geert Jansen Get new themes from the Internet Icons Icons Control Panel Module If you already have a theme archive locally, this button will unpack it and make it available for KDE applications Install a theme archive file you already have locally Install from File Installing icon themes... Jonathan Riddell Main Toolbar NAME OF TRANSLATORSYour names Name No Effect Panel Preview Remove Theme Remove the selected theme from your disk Set Effect... Setup Active Icon Effect Setup Default Icon Effect Setup Disabled Icon Effect Size: Small Icons The file is not a valid icon theme archive. This will remove the selected theme from your disk. To Gray To Monochrome Toolbar Torsten Rahn Unable to create a temporary file. Unable to download the icon theme archive;
please check that address %1 is correct. Unable to find the icon theme archive %1. Use of Icon You need to be connected to the Internet to use this action. A dialog will display a list of themes from the http://www.kde.org website. Clicking the Install button associated with a theme will install this theme locally. Project-Id-Version: kcmicons
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-08 06:27+0100
PO-Revision-Date: 2016-11-03 16:18+0800
Last-Translator: Jeff Huang <s8321414@gmail.com>
Language-Team: Chinese <kde-i18n-doc@kde.org>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
dot tw>
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=1; plural=0;
 程度(&A): 效果(&E): 第二色彩(&S): 半透明(&S) 主題(&T) (c) 2000-2003 Geert Jansen <h1>圖示</h1>這個模組讓您可以選擇桌面的圖示。<p>要選用某個圖示主題，請在該名稱上點一下，並按下下方的「套用」。如果您要反悔您所作的選擇，請按下下方的「重設」來放棄您所作的變更。</p><p>按下「安裝新主題」讓您可以安裝新的圖示主題，只要輸入其位置或是藉由瀏覽得知其位置，再按下「確定」即可完成安裝</p><p>「移除主題」按鈕只有在您選擇了一個您利用本模組所安裝的主題時才可以按。您無法在這裡移除全域安裝的主題</p><p>您也可以指定圖示顯示時的特別效果。</p> <qt>您確定要移除 <strong>%1</strong> 圖示主題？<br /><br />這樣會刪除這個主題所安裝的檔案。</qt> <qt>正在安裝 <strong>%1</strong> 主題</qt> 啟動 已關閉 預設 安裝的過程中發生問題；不過主題集中的大部分檔案都已經安裝了。 進階(&V) 所有的圖示 Antonio Larrosa Jimenez 顏色(&L): 轉成彩色 確認 去飽和 描述 桌面 對話框 拖放或輸入主題的網址 iitze@hotmail.com, s8321414@gmail.com 效果參數 Gamma Geert Jansen 從網際網路上取得新主題 圖示 圖示控制面板模組 如果您已經將主題檔下載到本地端，這個按鍵可以將檔案解壓並安裝到 KDE 系統中 安裝您已下載的主題檔 從檔案安裝 正在安裝圖示主題中... Jonathan Riddell 主工具列 Tsung-Chien Ho, Jeff Huang 名稱 無效果 面板 預覽 移除主題 從磁碟中移除選取的主題 設定效果... 設定啟動圖示效果 設定預設圖示效果 設定關閉圖示效果 大小: 小圖示 此檔案不是合法的圖示主題集。 這會從您的磁碟中移除選取的主題。 轉成灰階 轉為黑白 工具列 Torsten Rahn 無法建立暫存檔。 無法下載此圖示主題集；
請檢查位址 %1 是否正確。 無法找到圖示主題集 %1。 使用圖示 要使用此功能您必須先連線到網際網路上。會有一個對話框顯示出從 http://www.kde.org 中取得的主題清單。點擊「安裝」鍵可以安裝您選取的主題。 