��          <      \       p   !   q   3   �      �   �  �   )   �  <   �                        Finds Kate sessions matching :q:. Lists all the Kate editor sessions in your account. Open Kate Session Project-Id-Version: krunner_katesessions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-18 03:08+0100
PO-Revision-Date: 2009-04-06 11:25+0800
Last-Translator: Frank Weng (a.k.a. Franklin) <franklin at goodhorse dot idv dot tw>
Language-Team: Chinese Traditional <zh-l10n@linux.org.tw>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 0.3
Plural-Forms: nplurals=1; plural=0;
 尋找符合 :q: 的 Kate 工作階段。 列出您的帳號中的所有 Kate 編輯器工作階段。 開啟 Kate 作業階段 