��    ,      |  ;   �      �     �     �  .   �  5        7     >     N     T  	   a     k     �     �     �  1   �     �     �                  '   0     X     [     d  '   k     �     �     �     �  5   �     �          
             $   ,  A   Q  �   �     y     �  C   �  '   �     �       �  "     �	     �	     �	     �	     �	     �	     �	     
     
     
     0
     H
     [
     m
     �
     �
     �
     �
     �
  	   �
     �
     �
     �
     �
                '     4     ;     O     h     o     v     �  	   �     �     �     �     �     �                            #                    '                +   $       &      )                	                                    !   (      "                %                                    
   *       ,        %1% Back Button to change keyboard layoutSwitch layout Button to show/hide virtual keyboardVirtual Keyboard Cancel Caps Lock is on Close Close Search Configure Configure Search Plugins Desktop Session: %1 Different User Keyboard Layout: %1 Logging out in 1 second Logging out in %1 seconds Login Login Failed Login as different user Logout No media playing Nobody logged in on that sessionUnused OK Password Reboot Reboot in 1 second Reboot in %1 seconds Recent Queries Remove Restart Shutdown Shutting down in 1 second Shutting down in %1 seconds Start New Session Suspend Switch Switch Session Switch User Textfield placeholder textSearch... Textfield placeholder text, query specific KRunnerSearch '%1'... This is the first text the user sees while starting in the splash screen, should be translated as something short, is a form that can be seen on a product. Plasma is the project name so shouldn't be translated.Plasma made by KDE Unlock Unlocking failed User logged in on console (X display number)on TTY %1 (Display %2) User logged in on console numberTTY %1 Username Username (location)%1 (%2) Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-09 03:21+0100
PO-Revision-Date: 2017-03-27 09:35+0800
Last-Translator: Jeff Huang <s8321414@gmail.com>
Language-Team: Chinese <kde-i18n-doc@kde.org>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 %1% 返回 切換佈局 虛擬鍵盤 取消 大寫鎖定已開啟 關閉 關閉搜尋 設定 設定搜尋外掛程式 桌面作業階段：%1 不同的使用者 鍵盤佈局：%1 在 %1 秒內登出 登入 登入失敗 登入為不同的使用者 登出 沒有播放任何媒體 未使用 確定 密碼 重新開機 在 %1 秒內重新啟動 最近的查詢 移除 重新啟動 關機 在 %1 秒內關機 開始新的作業階段 暫停 切換 切換工作階段 切換使用者 搜尋... 搜尋「%1」... 由 KDE 製作的 Plasma 解除鎖定 解除鎖定失敗 在 TTY %1（顯示 %2） TTY %1 使用者名稱 %1 (%2) 