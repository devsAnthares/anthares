��          �      �       H     I     i  #   �      �     �     �     �  �        �  ]   �       p   *  :   �  �  �     n     �  !   �  -   �     �            �   *     �  6   �  !   %  e   G  8   �                     
          	                                  Configure Look and Feel details Cursor Settings Changed Download New Look And Feel Packages EMAIL OF TRANSLATORSYour emails Get New Looks... Marco Martin NAME OF TRANSLATORSYour names Select an overall theme for your workspace (including plasma theme, color scheme, mouse cursor, window and desktop switcher, splash screen, lock screen etc.) Show Preview This module lets you configure the look of the whole workspace with some ready to go presets. Use Desktop Layout from theme Warning: your Plasma Desktop layout will be lost and reset to the default layout provided by the selected theme. You have to restart KDE for cursor changes to take effect. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-27 05:46+0100
PO-Revision-Date: 2017-01-23 21:57+0800
Last-Translator: Jeff Huang <s8321414@gmail.com>
Language-Team: Chinese <kde-i18n-doc@kde.org>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 設定外觀與感覺詳情 游標設定已變更 下載新的外觀與感覺套件 franklin@goodhorse.idv.tw, s8321414@gmail.com 取得新外觀... Marco Martin Franklin Weng, Jeff Huang 為您的工作空間選擇一個整體的外觀與感覺主題（包括 Plasma 主題、色彩機制、滑鼠游標、視窗與桌面切換器、啟動畫面、鎖定畫面等等） 顯示預覽 這個模組讓您設定整個工作空間的外觀。 使用主題提供的桌面佈局 警告：您的 Plasma 桌面佈局將會遺失並重置為由選定的主題所提供的預設佈局 您必須重新啟動 KDE 游標的變更才能生效。 