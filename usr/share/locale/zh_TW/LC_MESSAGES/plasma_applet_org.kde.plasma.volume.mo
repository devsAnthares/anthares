��    #      4  /   L           	               $     1     :     J  3   Z  >   �     �     �     �        m        v     �     �     �     �  *   �      �          #  "   4     W     v     �     �     �     �     �     �  ;   �     4  �  J               "     /     <     C     P     ]     d     k     �     �     �  	   �     �     �     �     �     �  *   �     	     :	     G	     T	     g	     w	     �	     �	     �	     �	     �	     �	     �	     �	     #                          
                                    !                 "               	                                                          % Applications Audio Muted Audio Volume Behavior Capture Devices Capture Streams Checkable switch for (un-)muting sound output.Mute Checkable switch to change the current default output.Default Decrease Microphone Volume Decrease Volume Devices General Heading for a list of ports of a device (for example built-in laptop speakers or a plug for headphones)Ports Increase Microphone Volume Increase Volume Maximum volume: Mute Mute Microphone No applications playing or recording audio No output or input devices found Playback Devices Playback Streams Port is unavailable (unavailable) Port is unplugged (unplugged) Raise maximum volume Volume Volume at %1% Volume feedback Volume step: label of device items%1 (%2) label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: plasma_applet_org.kde.plasma.volume
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2017-06-29 21:14+0800
Last-Translator: Jeff Huang <s8321414@gmail.com>
Language-Team: Chinese <kde-i18n-doc@kde.org>
Language: Traditional Chinese
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=1; plural=0;
 % 應用程式 音效靜音 音效音量 行為 抓取裝置 擷取串流 靜音 預設 遞減麥克風音量 遞減音量 裝置 一般 連接埠 增加麥克風音量 遞增音量 最大音量： 靜音 麥克風靜音 沒有正在播放或錄音的應用程式 找不到輸出或輸入裝置 播放裝置 播放串流 （無法使用） （未插入） 提高最大音量 音量 音量在 %1% 音量回饋 音量步進： %1 (%2) %1：%2 100% %1% 