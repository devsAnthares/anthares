��    *      l  ;   �      �     �     �  )   �               /     H     ^  #   ~     �  
   �     �     �  %   �  +        E     Z     m  @   z     �     �     �     �               '     ,     9     ?     _     e  .   m     �  F   �  (   �  	   '  	   1  	   ;  '   E  7   m  "   �  �  �     }	     �	     �	     �	     �	  	   �	     �	     �	     �	     �	     �	     
     
      
  <   ?
     |
     �
  	   �
  E   �
     �
     	          &     ?     X     _     f  	   s     }     �     �  $   �     �  ?   �  !   )     K     R     Y     `     g  	   k                   $                                 %                      *                                               	   #   )   "             &           
                        '             (   !    &Do not remember @actionWalk through activities @actionWalk through activities (Reverse) @action:buttonApply @action:buttonCancel @action:buttonChange... @action:buttonCreate @title:windowActivity Settings @title:windowCreate a New Activity @title:windowDelete Activity Activities Activity information Activity switching Are you sure you want to delete '%1'? Blacklist all applications not on this list Clear recent history Create activity... Description: Error loading the QML files. Check your installation.
Missing %1 For a&ll applications Forget a day Forget everything Forget the last hour Forget the last two hours General Icon Keep history Name: O&nly for specific applications Other Privacy Private - do not track usage for this activity Remember opened documents: Remember the current virtual desktop for each activity (needs restart) Shortcut for switching to this activity: Shortcuts Switching Wallpaper for in 'keep history for 5 months'for  unit of time. months to keep the history month  months unlimited number of monthsforever Project-Id-Version: kcm_activities5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-11-03 16:03+0800
Last-Translator: Jeff Huang <s8321414@gmail.com>
Language-Team: Chinese <kde-i18n-doc@kde.org>
Language: Traditional Chinese
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 不要保留(&D) 瀏覽活動 瀏覽活動（反向） 套用 取消 改變... 建立 活動設定 建立新活動 刪除活動 活動 活動資訊 活動切換 您確定要刪除「%1」嗎？ 將所有不在這份清單上的應用程式列入黑名單 清除最近的歷史 建立活動... 描述： 載入 QML 檔時發生錯誤。請檢查您的安裝。
缺少了 %1 所有應用程式(&L) 忘記今天的 全部忘記 忘記最後一小時的 忘記最後兩小時的 一般 圖示 保留歷史 名稱： 僅指定的應用程式(&N) 其他 隱私 私人活動 - 不要追蹤此活動 記住已開啟的文件： 記住每個活動目前的虛擬桌面（需要重新啟動） 切換到此活動的快捷鍵： 捷徑 切換 桌布 持續 月 不斷地 