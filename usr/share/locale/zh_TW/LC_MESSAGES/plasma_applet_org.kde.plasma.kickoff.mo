��          �   %   �      `     a  
   g  /   r  -   �     �     �  
   �     �     
        W   )     �  ,   �  	   �     �     �     �     �     �  
   �               5     I  1   ^     �  �  �     5  
   ;  	   F     P     ]     s     �     �     �     �  T   �       0   "     S     `     p  	   }     �     �     �     �     �     �  '   �          (                     
                                                                         	                              %1@%2 %2@%3 (%1) @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Favorites All Applications Appearance Applications Applications updated. Computer Drag tabs between the boxes to show/hide them, or reorder the visible tabs by dragging. Edit Applications... Expand search to bookmarks, files and emails Favorites Hidden Tabs History Icon: Leave Menu Buttons Often Used Remove from Favorites Show applications by name Sort alphabetically Switch tabs on hover Type is a verb here, not a nounType to search... Visible Tabs Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2017-06-29 21:12+0800
Last-Translator: Jeff Huang <s8321414@gmail.com>
Language-Team: Chinese <kde-i18n-doc@kde.org>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 %1@%2 %2@%3 (%1) 選擇... 清除圖示 新增到我的最愛 所有應用程式 外觀 應用程式 應用程式已更新 電腦 在框框間拖放分頁以顯示／隱藏它們，或是重新排序可見分頁。 編輯應用程式... 將搜尋擴展到書籤、檔案與電子郵件 我的最愛 隱藏的分頁 歷史紀錄 圖示： 離開 選單按鈕 經常使用 從我的最愛中移除 依名稱顯示應用程式 依字母排序 滑鼠游標置於其上時切換分頁 輸入以搜尋... 可見分頁 