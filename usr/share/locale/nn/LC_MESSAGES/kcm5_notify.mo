��          �      �       0     1  �  H     �       &         ?     `     n     v     �     �  (   �  �  �     �  �  �     �     �  '   �  $   �     �       )        @     P  '   i         
                 	                                (c) 2002-2006 KDE Team <h1>System Notifications</h1>Plasma allows for a great deal of control over how you will be notified when certain events occur. There are several choices as to how you are notified:<ul><li>As the application was originally designed.</li><li>With a beep or other noise.</li><li>Via a popup dialog box with additional information.</li><li>By recording the event in a logfile without any additional visual or audible alert.</li></ul> Carsten Pfeiffer Charles Samuels Disable sounds for all of these events EMAIL OF TRANSLATORSYour emails Event source: KNotify NAME OF TRANSLATORSYour names Olivier Goffart Original implementation System Notification Control Panel Module Project-Id-Version: kcmnotify
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-09 07:18+0200
PO-Revision-Date: 2018-01-25 21:00+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: NorwegianNynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 © 2002–2006 KDE-gruppa <h1>Systemvarslingar</h1>Plasma gjev deg god kontroll over korleis du vert informert når visse hendingar skjer. Du kan velja korleis du skal varslast:<ul><li>Slik programmet opphavleg er sett opp til å varsla deg.</li><li>Med ein pipelyd eller eit anna lydsignal.</li><li>Via eit sprettoppvindauge med tilleggsinformasjon.</li><li>Ved å notera hendinga i ei loggfil – utan noka anna varsling.</li></ul> Carsten Pfeiffer Charles Samuels Slå av lydar for alle desse hendingane gaute@verdsveven.com,karl@huftis.org Hendingskjelde: KNotify Gaute Hvoslef Kvalnes,Karl Ove Hufthammer Olivier Goffart Opphavleg implementasjon Kontrollpanelmodul for systemvarslingar 