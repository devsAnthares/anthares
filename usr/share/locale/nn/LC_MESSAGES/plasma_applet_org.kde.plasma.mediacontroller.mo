��          �            x     y  !   �     �     �     �  @   �     *  $   ?     d     k  "   �     �  %   �     �     �  �       �     �     	          (     @     E  0   K     |  
   �     �     �     �     �     �                        
                                    	                   Artist of the songby %1 Artist of the songby %1 (paused) Choose player automatically General No media playing Open player window or bring it to the front if already openOpen Pause playbackPause Pause playback when screen is locked Paused Play next trackNext Track Play previous trackPrevious Track Quit playerQuit Remaining time for song e.g -5:42-%1 Start playbackPlay Stop playbackStop Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-06 03:09+0200
PO-Revision-Date: 2018-01-25 21:04+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: NorwegianNynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 av %1 av %1 (pause) Vel spelar automatisk Generelt Ingen medium vert spela Opna Pause Set avspeling på pause når skjermen vert låst Pause Neste spor Førre spor Avslutt −%1 Spel Stopp 