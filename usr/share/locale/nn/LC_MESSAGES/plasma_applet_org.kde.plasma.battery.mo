��    &      L  5   |      P  9   Q     �     �     �     �     �     �  7     	   G     Q     Z  �   t     C     O     b     z     �     �     �     �     �  #   �  %   �          6  �   F  P     o   c  5   �  �   	     �     �     �     �  g   �  )   <	  ?   f	    �	     �     �     �     �  %   �          0     E  
   R     ]     c  �   ~     ^     g     x     �     �     �     �     �     �     �     �     �       n     #   ~  '   �  ;   �  �        �     �     �  
   �     �     L     R               %       
                           #                     !                "       $                            &   	                                                      %1 is remaining time, %2 is percentage%1 Remaining (%2%) %1% Battery Remaining %1%. Charging %1%. Plugged in %1%. Plugged in, not Charging &Configure Power Saving... Battery and Brightness Battery is currently not present in the bayNot present Capacity: Charging Configure Power Saving... Disabling power management will prevent your screen and computer from turning off automatically.

Most applications will automatically suppress power management when they don't want to have you interrupted. Discharging Display Brightness Enable Power Management Fully Charged General Keyboard Brightness Model: No Batteries Available Not Charging Placeholder is battery capacity%1% Placeholder is battery percentage%1% Power management is disabled Show percentage Some Application and n others are currently suppressing PM%2 and %1 other application are currently suppressing power management. %2 and %1 other applications are currently suppressing power management. Some Application is suppressing PM%1 is currently suppressing power management. Some Application is suppressing PM: Reason provided by the app%1 is currently suppressing power management: %2 The battery applet has enabled system-wide inhibition The capacity of this battery is %1%. This means it is broken and needs a replacement. Please contact your hardware vendor for more details. Time To Empty: Time To Full: Used for measurement100% Vendor: Your notebook is configured not to suspend when closing the lid while an external monitor is connected. battery percentage below battery icon%1% short symbol to signal there is no battery curently available- Project-Id-Version: plasma_applet_battery
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-03 03:15+0100
PO-Revision-Date: 2017-02-22 20:27+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 %1 att (%2 %) %1 % straum att %1 % – ladar %1 % – kopla til %1 % – kopla til, men ladar ikkje &Set opp straumstyring … Batteri og lysstyrke Ikkje sett i Kapasitet: Ladar Set opp straumstyring … Viss du slår av straumstyringa, vil ikkje skjermen eller maskina kunna slå seg av automatisk når dei ikkje er i bruk.

Dei fleste programma vil automatisk slå av straumstyringa når dei vil at du ikkje skal forstyrrast. Ladar ut Lysstyrke skjerm Bruk straumstyring Ferdig ladd Generelt Tastaturlys Modell: Ingen batteri tilgjengelege Ladar ikkje %1 % %1 % Straumstyringa er avslått Vis prosent %2 og %1 anna program hindrar automatisk straumstyring %2 og %1 andre program hindrar automatisk straumstyring %1 hindrar automatisk straumstyring %1 hindrar automatisk straumstyring: %2 Batterielementet har slått på hindring for heila systemet Batterikapasiteten er %1 %. Dette vil seia at batteriet er øydelagt, og må bytast. Kontakt maskinleverandøren for meir informasjon. Tid til full utlading: Tid til ferdig lada: 100 % Produsent: Maskina di er sett opp til å ikkje gå i kvile- eller dvalemodus når du lukkar att lokket og ein ekstern skjerm er kopla til. %1 % – 