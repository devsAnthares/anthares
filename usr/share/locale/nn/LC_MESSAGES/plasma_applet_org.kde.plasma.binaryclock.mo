��    	      d      �       �   
   �      �      �   	              "     C  "   ]    �  	   �     �  
   �     �     �  '   �  "   �  &                                	                 Appearance Colors: Display seconds Draw grid Show inactive LEDs: Use custom color for active LEDs Use custom color for grid Use custom color for inactive LEDs Project-Id-Version: plasma_applet_binaryclock
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-31 05:47+0100
PO-Revision-Date: 2018-01-24 20:01+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Utsjånad Fargar: Vis sekund Teikn rutenett Vis av-lysdiodar: Bruk sjølvvald farge for på-lysdiodar Bruk sjølvvald farge på rutenett Bruk sjølvvald farge for av-lysdiodar 