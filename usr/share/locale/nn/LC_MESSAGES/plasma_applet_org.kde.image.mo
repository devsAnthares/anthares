��            )   �      �     �     �     �     �     �     �     �     �  0        3     G     ]     c     o     w     �  
   �     �     �     �     �     �               (     A     I     a     m  �  s     g  #   t     �     �  	   �     �  	   �     �  0   �          7     U  
   [     f     m     �     �     �     �     �     �     �                /     P     W  	   j     t                                            
                                                                                      	       %1 by %2 Add Custom Wallpaper Add Folder... Add Image... Background: Blur Centered Change every: Directory with the wallpaper to show slides from Download Wallpapers Get New Wallpapers... Hours Image Files Minutes Next Wallpaper Image Open Containing Folder Open Image Open Wallpaper Image Positioning: Recommended wallpaper file Remove wallpaper Restore wallpaper Scaled Scaled and Cropped Scaled, Keep Proportions Seconds Select Background Color Solid color Tiled Project-Id-Version: KDE 4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:39+0100
PO-Revision-Date: 2018-01-22 21:24+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 «%1» av %2 Legg til sjølvvalt bakgrunnsbilete &Legg til mappe … Legg til bilete … Bakgrunn: Uklar Midtstilt Byt bilete kvar: Mappe med bakgrunnsbilete å visa lysbilete frå Last ned bakgrunnsbilete Hent nye bakgrunnsbilete … Timar Biletfiler Minutt Neste bakgrunnsbilete Opna mappa med innhaldet Opna bilete Opna bakgrunnsbilete Plassering: Anbefalt bakgrunnsbilete Fjern bakgrunnsbilete Gjenopprett bakgrunnsbilete Skalert Skalert og avskore Skalert, behald storleiksforhold Sekund Vel bakgrunnsfarge Einsfarga Jamsides 