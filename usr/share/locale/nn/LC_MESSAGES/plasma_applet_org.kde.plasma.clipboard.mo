��          �      L      �     �     �     �     �          )     1     9     P     h     t  K   �     �     �     �     �            �             $     0     J     f     {     �     �     �     �     �     �     �     �     �          &     +                                                  
   	                                               Change the barcode type Clear history Clipboard Contents Clipboard history is empty. Clipboard is empty Code 39 Code 93 Configure Clipboard... Creating barcode failed Data Matrix Edit contents Indicator that there are more urls in the clipboard than previews shown+%1 Invoke action QR Code Remove from history Return to Clipboard Search Show barcode Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-10 03:07+0200
PO-Revision-Date: 2015-06-06 09:13+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Endra type strekkode Tøm loggen Innhald på utklippstavla Utklippstavleloggen er tom. Utklippstavla er tom Kode 39 Kode 93 Set opp utklippstavla … Klarte ikkje laga strekkode Datamatrise Rediger innhald +%1 Utfør handling QR-kode Fjern frå loggen Tilbake til utklippstavla Søk Vis strekkode 