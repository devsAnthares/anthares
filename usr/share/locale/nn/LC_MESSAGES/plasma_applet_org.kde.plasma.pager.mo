��          �   %   �      0  *   1     \  .   s     �     �     �     �     �     �                 
   $     /     5     =     E     ]     t     �     �     �     �    �  -   �       0   "     S     b     s     �     �     �     �     �     �     �     �  
   �               $     >     X     u     �     �        
                             	                                                                                          %1 Minimized Window: %1 Minimized Windows: %1 Window: %1 Windows: ...and %1 other window ...and %1 other windows Activity name Activity number Add Virtual Desktop Configure Desktops... Desktop name Desktop number Display: Does nothing General Horizontal Icons Layout: No text Only the current screen Remove Virtual Desktop Selecting current desktop: Show Activity Manager... Shows desktop The pager layoutDefault Vertical Project-Id-Version: plasma_applet_pager
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-19 03:29+0200
PO-Revision-Date: 2016-10-25 22:08+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 %1 minimerte vindauge: %1 minimerte vindauge: %1 vindauge: %1 vindauge: … og %1 anna vindauge … og %1 andre vindauge Aktivitetsnamn Aktivitetsnummer Legg til virtuelt skrivebord Set opp skrivebord … Skrivebordsnamn Skrivebordstal Vising: Gjer ikkje noko Generelt Vassrett Ikon Utforming: Ingen tekst Berre gjeldande skjerm Fjern virtuelt skrivebord Vel gjeldande skrivebord: Vis aktivitetshandsamar … Viser skrivebordet Standard Loddrett 