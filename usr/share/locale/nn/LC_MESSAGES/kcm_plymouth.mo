��          �   %   �      @     A  !   Y      {     �      �     �     �  +        =     N     [  (   z     �  ,   �  7   �     !  7   :     r  ]   �  1   �  	   "     ,  "   ?  5   b  �  �  "   H  ,   k     �     �     �     �  "        $     D     X     e  '   y     �  '   �  7   �     	  0   6	     g	  c   �	  1   �	     
     #
  ,   9
  )   f
                    
         	                                                                                                 Cannot start initramfs. Cannot start update-alternatives. Configure Plymouth Splash Screen Download New Splash Screens EMAIL OF TRANSLATORSYour emails Get New Boot Splash Screens... Initramfs failed to run. Initramfs returned with error condition %1. Install a theme. Marco Martin NAME OF TRANSLATORSYour names No theme specified in helper parameters. Plymouth theme installer Select a global splash screen for the system The theme to install, must be an existing archive file. Theme %1 does not exist. Theme corrupted: .plymouth file not found inside theme. Theme folder %1 does not exist. This module lets you configure the look of the whole workspace with some ready to go presets. Unable to authenticate/execute the action: %1, %2 Uninstall Uninstall a theme. update-alternatives failed to run. update-alternatives returned with error condition %1. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-28 03:05+0200
PO-Revision-Date: 2018-01-27 12:01+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Klarte ikkje starta «initramfs». Klarte ikkje starta «update-alternatives». Set opp Plymouth-velkomstbilete Last ned nye velkomstbilete karl@huftis.org Hent nye velkomstbilete … «initramfs» klarte ikkje køyra. «initramfs» gav feilkoden %1. Installer eit tema. Marco Martin Karl Ove Hufthammer Inkje tema definert i hjelpeparametrar. Installering av Plymouth-tema Vel globalt velkomstbilete for systemet Temaet som skal installerast. Det må vera ei arkivfil. Temaet %1 finst ikkje. Ugyldig tema: Manglar .plymouth-fil inni temaet. Temamappa %1 finst ikkje. Med denne modulen kan du setja opp utsjånaden for heile arbeidsflata, med nokre førehandsoppsett. Klarte ikkje autentisera/køyra handlinga: %1, %2 Avinstaller Avinstaller eit tema. «update-alternatives» klarte ikkje køyra. «update-alternatives» gav feilkoden %1. 