��          �      |      �     �  2        B     b       #   �      �     �  %   �       
   &     1     >     ]  �   }       ]   (     �  p   �  :        P  �  \  (   K  G   t     �     �     �  ,        5     E  1   _  !   �     �     �     �  *   �  �   	     �	  b   �	  !   .
  {   P
  I   �
  	                                                                         
                	           Apply a look and feel package Command line tool to apply look and feel packages. Configure Look and Feel details Copyright 2017, Marco Martin Cursor Settings Changed Download New Look And Feel Packages EMAIL OF TRANSLATORSYour emails Get New Looks... List available Look and feel packages Look and feel tool Maintainer Marco Martin NAME OF TRANSLATORSYour names Reset the Plasma Desktop layout Select an overall theme for your workspace (including plasma theme, color scheme, mouse cursor, window and desktop switcher, splash screen, lock screen etc.) Show Preview This module lets you configure the look of the whole workspace with some ready to go presets. Use Desktop Layout from theme Warning: your Plasma Desktop layout will be lost and reset to the default layout provided by the selected theme. You have to restart KDE for cursor changes to take effect. packagename Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-27 05:46+0100
PO-Revision-Date: 2018-01-24 20:17+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Ta i bruk pakke for utsjånad og åtferd Kommandolinjeverktøy for å ta i bruk pakkar for utsjånad og åtferd. Set opp utsjånad og åtferd © 2017 Marco Martin Peikarinnstillingar endra Last ned nye pakkar for utsjånad og åtferd karl@huftis.org Hent nye utsjånadar … Vis tilgjengelege pakkar for utsjånad og åtferd Verktøy for utsjånad og åtferd Vedlikehaldar Marco Martin Karl Ove Hufthammer Tilbakestill oppsett til Plasma-skrivebord Vel eit overordna tema for utsjånad og åtferd på arbeidsbordet (Plasma-tema, fargeoppsett, musepeikarar, vindaugs- og skrivebordsbytar, velkomstbilete, skjermlås osv.) Førehandsvising Med denne modulen kan du setja opp utsjånaden for heile arbeidsflata, med noko førehandsoppsett. Bruk skrivebordsoppsett frå tema Åtvaring: Du vil mista skrivebordsoppsettet i Plasma, då det vil gå tilbake til standardoppsettet frå det valde temaet. Du må starta KDE på nytt for at peikarinnstillingane skal trå i kraft. pakkenamn 