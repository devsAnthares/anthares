��          �            x  
   y  	   �     �     �  3   �     �     �                    %     *     F  B   Y     �  	  �  	   �     �  
   �     �  2   �          '     E     Q     Y     b  (   k     �     �     �     	                                                  
                        Appearance Browse... Choose an image Fifteen Puzzle Image Files (*.png *.jpg *.jpeg *.bmp *.svg *.svgz) Number color Path to custom image Piece color Show numerals Shuffle Size Solve by arranging in order Solved! Try again. The time since the puzzle started, in minutes and secondsTime: %1 Use custom image Project-Id-Version: plasma_applet_fifteenPuzzle
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-01-12 03:59+0100
PO-Revision-Date: 2015-06-06 09:27+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Utsjånad Bla gjennom … Vel bilete Spelet femten Biletfiler (*.png *.jpg *.jpeg *.bmp *.svg *.svgz) Talfarge Adresse til sjølvvalt bilete Brikkefarge Vis tal Stokk om Storleik Løys ved å flytta i rett rekkjefølgje Løyst! Prøv ein gong til. Tid: %1 Bruk sjølvvalt bilete 