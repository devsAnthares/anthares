��          �            x     y  
   �  
   �     �  .   �     �     �     �        *   )      T  ,   u  ,   �  0   �        �    2      
   3  	   >     H  &   M     t     �  	   �     �  C   �  #   �          /  (   ?     h                        
                                            	           &Lock screen on resume: Activation Appearance Error Failed to successfully test the screen locker. Immediately Keyboard shortcut: Lock Session Lock screen automatically after: Lock screen when waking up from suspension Re&quire password after locking: Spinbox suffix. Short for minutes min  mins Spinbox suffix. Short for seconds sec  secs The global keyboard shortcut to lock the screen. Wallpaper &Type: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:10+0100
PO-Revision-Date: 2018-01-27 12:11+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 &Lås skjermen ved vekking frå kvile-/dvalemodus: Låsestart Utsjånad Feil Klarte ikkje å prøva skjermlåsaren. Med ein gong Snøggtast: Lås økt Lås skjermen automatisk etter: Lås skjermen når systemet vert vekt frå kvile- eller dvalemodus. &Krev passord for opplåsing etter:  minutt  minutt  sekund  sekund Global snarvegstast som låser skjermen. &Type bakgrunn: 