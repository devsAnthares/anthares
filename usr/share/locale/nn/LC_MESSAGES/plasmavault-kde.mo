��    ?        Y         p  �  q  }  J  M  �        
   7     B     K     j     �  <   �     �     �  	   �     �  .     %   A     g     }     �     �     �     �     �     �     �     �  4        B  9   T     �     �     �  �   �  !   �  t   �     8     D     a     n     s  	   �     �  -   �     �  B   �  &     ?   B  '   �  )   �  7   �  .     5   ;  +   q     �     �     �  ,   �          -  9   :  V   t  C   �  �    �  �  �  �  O       j"     }"     �"  $   �"  $   �"     �"  7   �"     #     8#     I#     Q#  +   h#  #   �#     �#     �#     �#     �#     �#      $     $     %$     D$     J$  3   b$     �$  <   �$     �$     �$     
%  �   %     �%  ~   &     �&     �&     �&     �&     �&     �&     �&  *   '     1'  B   8'  '   {'  D   �'  2   �'  8   (  A   T(  &   �(  7   �(  .   �(     $)     C)     _)  )   m)     �)     �)  3   �)  U   �)     K*     !      	   2   ?   
   /                 +   (   *       1            )   >           8          9   $              7             .       '   6          %   <             3   ,          ;       =   &                              -           #      "          :      5      0                                   4               <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body style=" font-family:'hlv'; font-size:9pt; font-weight:400; font-style:normal;">
<p style="-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><br /></p></body></html> <b>Security notice:</b>
                             According to a security audit by Taylor Hornby (Defuse Security),
                             the current implementation of Encfs is vulnerable or potentially vulnerable
                             to multiple types of attacks.
                             For example, an attacker with read/write access
                             to encrypted data might lower the decryption complexity
                             for subsequently encrypted data without this being noticed by a legitimate user,
                             or might use timing analysis to deduce information.
                             <br /><br />
                             This means that you should not synchronize
                             the encrypted data to a cloud storage service,
                             or use it in other circumstances where the attacker
                             can frequently access the encrypted data.
                             <br /><br />
                             See <a href='http://defuse.ca/audits/encfs.htm'>defuse.ca/audits/encfs.htm</a> for more information. <b>Security notice:</b>
                             CryFS encrypts your files, so you can safely store them anywhere.
                             It works well together with cloud services like Dropbox, iCloud, OneDrive and others.
                             <br /><br />
                             Unlike some other file-system overlay solutions,
                             it does not expose the directory structure,
                             the number of files nor the file sizes
                             through the encrypted data format.
                             <br /><br />
                             One important thing to note is that,
                             while CryFS is considered safe,
                             there is no independent security audit
                             which confirms this. @title:windowCreate a New Vault Activities Backend: Can not create the mount point Can not open an unknown vault. Change Choose the encryption system you want to use for this vault: Choose the used cypher: Close the vault Configure Configure Vault... Configured backend can not be instantiated: %1 Configured backend does not exist: %1 Correct version found Create Create a New Vault... CryFS Device is already open Device is not open Dialog Do not show this notice again EncFS Encrypted data location Failed to create directories, check your permissions Failed to execute Failed to fetch the list of applications using this vault Failed to open: %1 Forcefully close  General If you limit this vault only to certain activities, it will be shown in the applet only when you are in those activities. Furthermore, when you switch to an activity it should not be available in, it will automatically be closed. Limit to the selected activities: Mind that there is no way to recover a forgotten password. If you forget the password, your data is as good as gone. Mount point Mount point is not specified Mount point: Next Open with File Manager Password: Plasma Vault Please enter the password to open this vault: Previous The mount point directory is not empty, refusing to open the vault The specified backend is not available The vault configuration can only be changed while it is closed. The vault is unknown, can not close it. The vault is unknown, can not destroy it. This device is already registered. Can not recreate it. This directory already contains encrypted data Unable to close the vault, an application is using it Unable to close the vault, it is used by %1 Unable to detect the version Unable to perform the operation Unknown device Unknown error, unable to create the backend. Use the default cipher Vaul&t name: Wrong version installed. The required version is %1.%2.%3 You need to select empty directories for the encrypted storage and for the mount point formatting the message for a command, as in encfs: not found%1: %2 Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-11 05:53+0100
PO-Revision-Date: 2018-01-30 21:57+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body style=" font-family:'hlv'; font-size:9pt; font-weight:400; font-style:normal;">
<p style="-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><br /></p></body></html> <b>Tryggleiksmerknad:</b>
                             Ifølgje ein tryggleiksgjennomgang gjort av Taylor Hornby (Defuse Security),
                             er den gjeldande implementeringa av Encfs sårbar eller potensielt sårbar
                             for fleire typar angrep.
                             For eksempel kan ein angripar med lese- og skriveløyve
                             til krypterte data senka dekrypteringskompleksiteten
                             for seinare krypterte data utan at dette kan oppdagast av ein legitim brukar.
                             Angriparen kan òg bruka tidsmålingsanalysar for å avdekkja informasjon.
                             <br /><br />
                             Dette medfører at du ikkje bør synkronisera dei krypterte dataa
                             med ei skylagringsteneste eller bruka dei i andre samanhengar
                             der ein potensiell angripar hyppig har tilgang til dei.
                             <br /><br />
                             Sjå <a href='http://defuse.ca/audits/encfs.htm'>defuse.ca/audits/encfs.htm</a> for meir informasjon. <b>Tryggleiksmerknad:</b>
                             CryFS krypterer filene dine, slik at du trygt kan lagra dei andre stadar.
                             Det fungerer saman med skytenester som for eksempel Dropbox, iCloud og OneDrive.
                             <br /><br />
                             I motsetnad til nokre krypteringsmetodar som legg
                             seg oppå filsystemet, vil verken mappestrukturen,
                             talet på filer eller filstorleikar vera synlege
                             i det krypterte filformatet.
                             <br /><br />
                             Men merk at sjølv om CryFS-kryptering vert rekna som
                             trygg, er det ikkje gjort nokon uavhengige
                             tryggleiksgjennomgangar som stadfestar dette. Lag nytt datakvelv Aktivitetar Motor: Kan ikkje oppretta monteringspunktet Kan ikkje opna eit ukjend datakvelv. Endra Vel krypteringssystemet du vil bruka for dette kvelvet: Vel chifferet som skal brukast: Lukk datakvelvet Set opp Set opp datakvelv … Den oppsette motoren kan ikkje startast: %1 Set oppsett motoren finst ikkje: %1 Fann rett versjon Lag Lag nytt datakvelv … CryFS Eininga er alt open Eininga er ikkje open Dialogvindauge Ikkje vis denne meldinga igjen EncFS Kryptert dataplassering Klarte ikkje oppretta mapper. Sjekk tilgangsløyva. Klarte ikkje køyra Klarte ikkje henta lista over program som brukar datakvelvet Klarte ikkje opna: %1 Tvangslukk  Generelt Viss du avgrensar datakvelvet til einskildaktivitetar, vert det berre vist i skjermelementet når du er i desse aktivitetane. Når du byter til ein aktivitet som det ikkje skal vera tilgjengeleg i, vert det automatisk lukka. Avgrens til desse aktivitetane: Merk at det ikkje finst nokon måte å gjenoppretta gløymt passord. Viss du gløymer passordet, kan du rekna dataa som tapte. Monteringspunkt Monteringspunkt er ikkje valt Monteringspunkt: Neste Opna i filhandsamar Passord: Plasma-datakvelv Skriv inn passord for å opna datakvelvet: Førre Monteringspunktsmappa er ikkje tom – nektar å opna datakvelvet Den valde motoren er ikkje tilgjengeleg Du kan berre endra innstillingane for datakvelvet når det er lukka. Datakvelvet er ukjent. Kan derfor ikkje lukka det. Datakvelvet er ukjent. Kan derfor ikkje øydeleggja det. Eininga er alt registrert. Kan derfor ikkje oppretta ho på nytt. Denne mappa inneheld alt kryptert data Kan ikkje lukka datakvelvet, då eit program brukar det Kan ikkje lukka datakvelvet, då %1 brukar det Klarte ikkje oppdaga versjonen Kan ikkje utføra operasjon Ukjend eining Ukjend feil. Klarer ikkje oppretta motor. Bruk standardchiffer &Namn på datakvelv: Feil versjon installert. Du treng versjon %1.%2.%3. Du må velja tomme mapper for det krypterte lagringsområdet og for monteringspunktet %1: %2 