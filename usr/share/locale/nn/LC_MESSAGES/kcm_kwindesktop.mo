��    *      l  ;   �      �     �     �     �  
   D  +   O  
   {     �     �     �      �     �     �       	           �   @  e   �  *   D  H   o     �     �     �     �       )     ;   <  	   x     �  (   �     �     �     �          7     L     c  	   ~     �  #   �     �     �  �  �     �
     �
  v     
   �  7   �     �     �     �               <      L  
   m     x        �   �  e   K  ,   �  O   �     .     C     K  &   ]     �  3   �  A   �     
  !     2   9  !   l      �  $   �  #   �     �          '     A     F  '   c     �  !   �        
          *   &      #      '         $                               (            %                       	       !                            )              "                                          msec &Number of desktops: <h1>Multiple Desktops</h1>In this module, you can configure how many virtual desktops you want and how these should be labeled. Animation: Assigned global Shortcut "%1" to Desktop %2 Desktop %1 Desktop %1: Desktop Effect Animation Desktop Names Desktop Switch On-Screen Display Desktop Switching Desktop navigation wraps around Desktops Duration: EMAIL OF TRANSLATORSYour emails Enable this option if you want keyboard or active desktop border navigation beyond the edge of a desktop to take you to the opposite edge of the new desktop. Enabling this option will show a small preview of the desktop layout indicating the selected desktop. Here you can enter the name for desktop %1 Here you can set how many virtual desktops you want on your KDE desktop. KWin development team Layout N&umber of rows: NAME OF TRANSLATORSYour names No Animation No suitable Shortcut for Desktop %1 found Shortcut conflict: Could not set Shortcut %1 for Desktop %2 Shortcuts Show desktop layout indicators Show shortcuts for all possible desktops Switch One Desktop Down Switch One Desktop Up Switch One Desktop to the Left Switch One Desktop to the Right Switch to Desktop %1 Switch to Next Desktop Switch to Previous Desktop Switching Walk Through Desktop List Walk Through Desktop List (Reverse) Walk Through Desktops Walk Through Desktops (Reverse) Project-Id-Version: kcm_kwindesktop
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-06 05:35+0100
PO-Revision-Date: 2018-01-24 20:20+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
  ms Kor mange s&krivebord: <h1>Fleire skrivebord</h1>I denne modulen kan du velja kor mange virtuelle skrivebord du vil ha og kva dei skal heita. Animasjon: Tildelte den globale snarvegen «%1» til skrivebord %2 Skrivebord %1 Skrivebord %1: Animasjon for skrivebordseffekt Skrivebordsnamn Skjermvising av skrivebordsbyte Skrivebordsbyte Skrivebordsnavigering går rundt Skrivebord Lengd: eirbir@gmail.com,karl@huftis.org Slå på denne funksjonen dersom du vil at navigasjon med tastatur eller ut over skrivebordskanten frå eit endeskrivebord skal føra deg til skrivebordet i motsett ende. Kryss av her for å sjå ei lita førehandsvising av skrivebordsoppsettet på det valde skrivebordet. Her kan du oppgje eit namn på skrivebord %1 Her kan du velja kor mange virtuelle skrivebord du vil ha på KDE-skrivebordet. KWin-utviklingslaget Oppsett Kor mange &rader: Eirik U. Birkeland,Karl Ove Hufthammer Ingen animasjon Fann ikkje nokon passande snarveg for skrivebord %1 Snarvegkonflikt: Klarte ikkje laga snarvegen %1 til skrivebord %2 Snøggtastar Vis skrivebordoppsett-indikatorar Vis snøggtastar for alle tilgjengelege skrivebord Byt til neste skrivebord nedanfor Byt til neste skrivebord ovanfor Byt til neste skrivebord til venstre Byt til neste skrivebord til høgre Byt til skrivebord %1 Byt til neste skrivebord Byt til førre skrivebord Byte Bla gjennom skrivebordsliste Bla gjennom skrivebordsliste (baklengs) Bla gjennom skrivebord Bla gjennom skrivebord (baklengs) 