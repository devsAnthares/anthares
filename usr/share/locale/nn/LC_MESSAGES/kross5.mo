��    $      <  5   \      0     1  +   E     q  4   �  &   �     �     �                     5     :     P     X  ,   u  3   �     �     �               !  '   .     V     u     {     �     �     �     �     �     �  &   �       B        b  �  y     p     ~     �     �     �     �     �     �     �  $    	     %	     -	     B	     O	  *   i	  .   �	  $   �	  $   �	     
     
  
   
  !   #
  )   E
     o
     u
     �
     �
     �
     �
     �
     �
  &   �
       2        Q                                       
                              !                                  $      #                               	                           "             @info:creditAuthor @info:creditCopyright 2006 Sebastian Sauer @info:creditSebastian Sauer @info:shell command-line argumentThe script to run. @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: application descriptionCommand-line utility to run Kross scripts. application nameKross Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2014-10-05 09:59+0200
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Opphavsperson © 2006 Sebastian Sauer Sebastian Sauer Skriptet som skal køyrast. Generelt Legg til nytt skript. Legg til … Avbryt? Merknad: gaute@verdsveven.com,karl@huftis.org Rediger Rediger vald skript. Rediger … Køyr det valde skriptet. Klarte ikkje laga skript til tolken «%1» Klarte ikkje finna tolk til skriptfila «%1». Klarte ikkje lasta inn tolken «%1» Klarte ikkje opna skriptfila «%1». Fil: Ikon: Fortolkar: Tryggleiksnivå på Ruby-tolkaren Gaute Hvoslef Kvalnes,Karl Ove Hufthammer Namn: Funksjonen «%1» finst ikkje Tolken «%1» finst ikkje Fjern Fjern valt skript. Køyr Skriptfila «%1» finst ikkje Stopp Stopp køyringa av det valde skriptet. Tekst: Kommandolinjeprogram for køyring av Kross-skript. Kross 