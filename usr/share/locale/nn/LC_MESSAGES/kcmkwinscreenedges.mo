��    %      D  5   l      @     A     E     G  	   Y     c     |     �     �     �     �     �     �            S   ,  w   �     �  M         [     |  ?   �     �  	   �     �     
     #  %   2     X     e  F   �  #   �     �  ]     R   f     �     �     �     �	     �	     �	     �	     
     
     :
     J
     W
  (   q
     �
  $   �
     �
  
   �
  G   �
  G   :     �  C   �     �     �  3   �     '     ;     I     i     x  '   �     �     �  x   �  #   A     e  ^   ~  \   �     :     M               !      "          #   %                                                                            $                          	                
                            ms % %1 - All Desktops %1 - Cube %1 - Current Application %1 - Current Desktop %1 - Cylinder %1 - Sphere &Reactivation delay: &Switch desktop on edge: Activation &delay: Active Screen Corners and Edges Activity Manager Always Enabled Amount of time required after triggering an action until the next trigger can occur Amount of time required for the mouse cursor to be pushed against the edge of the screen before the action is triggered Application Launcher Change desktop when the mouse cursor is pushed against the edge of the screen EMAIL OF TRANSLATORSYour emails Lock Screen Maximize windows by dragging them to the top edge of the screen NAME OF TRANSLATORSYour names No Action Only When Moving Windows Open krunnerRun Command Other Settings Quarter tiling triggered in the outer Show Desktop Switch desktop on edgeDisabled Tile windows by dragging them to the left or right edges of the screen Toggle alternative window switching Toggle window switching Trigger an action by pushing the mouse cursor against the corresponding screen edge or corner Trigger an action by swiping from the screen edge towards the center of the screen Window Management of the screen Project-Id-Version: kcmkwinscreenedges
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-30 03:13+0100
PO-Revision-Date: 2018-01-24 20:01+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
  ms  % %1 – alle skrivebord %1 – kube %1 – gjeldande program %1 – gjeldande skrivebord %1 – sylinder %1 – kule Tid før &neste handling: &Byt skrivebord når musa er på kanten: Tid før &handling: Aktive skjermhjørne og skjermkantar Aktivitetshandsamar Alltid på Tid etter at ei handling er sett i gang før neste handling kan gjerast Tid musa må dragast mot kanten av skjermen før handlinga vert utført Programstartar Byt skrivebord når musepeikaren vert dregen mot kanten av skjermen karl@huftis.org Lås skjermen Maksimer vindauge ved å dra dei øvst på skjermen Karl Ove Hufthammer Inga handling Berre når vindauge vert flytta Køyr kommando Andre innstillingar Kvadrantflislegging utløyst i det ytre Vis skrivebordet Av Still vindauge ved sida av eller over/under kvarandre ved å dra dei til venstre eller høgre på kant kant på skjermen Slå på/av alternativ vindaugsbyte Slå på/av vindaugsbyte Utløys ei handling ved å flytta musepeikaren til tilhøyrande skjermkant eller skjermhjørne For å setja i gang ei handling drar du fingeren frå skjermkanten og mot midten av skjermen Vindaugshandsaming -området av skjermen 