��    &      L  5   |      P     Q     p  	   ~  0   �  .   �     �  7         ?     `     i  \   �  t   �  9   Z  -   �  $   �     �  [   �     P  E   o  G   �  >   �     <     C     Z     g     �     �     
     )  <   .  :   k  s   �  o   	  .   �	  ,   �	  .   �	  ,   
  �  B
  '   1     Y     j     y     �  %   �  <   �     �       #     J   2  g   }     �     �  )        @  _   Q     �     �     �  	   �     �     �       �        �     �      �     �     �     �               2     @     O     \                 &   %                     !   	      
                       #       "                     $                                                                             Activate Task Manager Entry %1 Activities... Add Panel Bluetooth was disabled, keep shortBluetooth Off Bluetooth was enabled, keep shortBluetooth On Configure Mouse Actions Plugin Do not restart plasma-shell automatically after a crash EMAIL OF TRANSLATORSYour emails Empty %1 Enable QML Javascript debugger Enables test mode and specifies the layout javascript file to set up the testing environment Fatal error message bodyAll shell packages missing.
This is an installation issue, please contact your distribution Fatal error message bodyShell package %1 cannot be found Fatal error message titlePlasma Cannot Start Force loading the given shell plugin Hide Desktop Load plasmashell as a standalone application, needs the shell-plugin option to be specified NAME OF TRANSLATORSYour names OSD informing that some media app is muted, eg. Amarok Muted%1 Muted OSD informing that the microphone is muted, keep shortMicrophone Muted OSD informing that the system is muted, keep shortAudio Muted Plasma Plasma Failed To Start Plasma Shell Plasma is unable to start as it could not correctly use OpenGL 2.
 Please check that your graphic drivers are set up correctly. Show Desktop Stop Current Activity Unable to load script file: %1 file mobile internet was disabled, keep shortMobile Internet Off mobile internet was enabled, keep shortMobile Internet On on screen keyboard was disabled because physical keyboard was plugged in, keep shortOn-Screen Keyboard Deactivated on screen keyboard was enabled because physical keyboard got unplugged, keep shortOn-Screen Keyboard Activated touchpad was disabled, keep shortTouchpad Off touchpad was enabled, keep shortTouchpad On wireless lan was disabled, keep shortWifi Off wireless lan was enabled, keep shortWifi On Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-12 03:10+0100
PO-Revision-Date: 2017-02-05 10:20+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Gå til oppføring %1 på oppgåvelinja Aktivitetar … Legg til panel Bluetooth av Bluetooth på Tillegg for oppsett av musehandlingar Ikkje start plasma-shell automatisk på nytt etter ein krasj karl@huftis.org Tom %1 Slå på QML JavaScript-feilsøkjar Slår på testmodus og vel JavaScript-fil for utforming av testomgjevnaden Alle skalpakkane manglar.
Programma er ikkje rett installerte. Kontakt dei ansvarlege bak distroen din. Fann ikkje skalpakken %1 Kan ikkje starta Plasma Tving innlasting av oppgjeve skal-tillegg Gøym skrivebord Last inn plasmahell som eit frittstående program (treng at valet «shell-plugin» er oppgjeve) Karl Ove Hufthammer %1 dempa Mikrofon slått av Dempa lyd Plasma Klarte ikkje starta Plasma Plasma-skal Kan ikkje starta Plasma, då det ikkje er mogleg å bruka OpenGL 2 rett.
Sjå til at skjermkortdrivarane er sette skikkeleg opp. Vis skrivebord Stopp gjeldande aktivitet Klarte ikkje lasta skriptfil: %1 fil Mobilt samband av Mobilt samband på Skjermtastatur av Skjermtastatur på Styreplate av Styreplate på Trådlaus av Trådlaus på 