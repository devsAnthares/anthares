��          t      �         C        U     i  3   �     �     �  F   �  5   *     `       �  �  ?   }     �     �     �     �     
  E     8   e     �     �                              	                          
    Attempting to perform the action '%1' failed with the message '%2'. Media playback next Media playback previous Name for global shortcuts categoryMedia Controller Play/Pause media playback Stop media playback The argument '%1' for the action '%2' is missing or of the wrong type. The media player '%1' cannot perform the action '%2'. The operation '%1' is unknown. Unknown error. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-06 03:09+0200
PO-Revision-Date: 2015-10-08 21:46+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Klarte ikkje utføra handlinga «%1». Feilmeldinga var «%2». Media spel neste Media spel førre Mediestyring Spel av / pause mediespeling Stopp medieavspeling Argumentet «%1» til handlinga «%2» manglar eller er av feil type. Mediespelaren «%1» kan ikkje utføra handlinga «%2». Handlinga «%1» er ukjend. Ukjend feil. 