��          |      �          %   !     G     U     c     u     �     �  
   �     �     �  $   �    �  )   �     "     .     7     P     f     o  	   �     �     �     �     
   	                                                   Automatically copy color to clipboard Clear History Color Options Copy to Clipboard Default color format: General Open Color Dialog Pick Color Pick a color Show history When pressing the keyboard shortcut: Project-Id-Version: plasma_applet_kolourpicker
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-12 03:03+0200
PO-Revision-Date: 2017-01-14 16:38+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Kopier automatisk farge til utklippstavla Tøm loggen Fargeval Kopier til utklippstavla Standard fargeformat: Generelt Opna fargevindauge Vel farge Vel ein farge Vis logg Ved bruk av snøggtasten: 