��          �   %   �      @     A  �   `  m   �  �   P  )   �  `        o     |     �     �     �      �     �     �  '        ,     >     ]     b     s  ?   �  Y   �  +     H   F  �  �     �  �   �  _   2	     �	     �	  [   �	     %
     1
  
   K
  	   V
     `
  $   �
     �
     �
  $   �
     �
  )   �
     '     ,  
   =  A   H  J   �  !   �  @   �                                                 	                                                               
              (c) 2003-2007 Fredrik Höglund <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) @item:inlistbox sizeResolution dependent A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Cursor Theme Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Fredrik Höglund Get new Theme Get new color schemes from the Internet Install from File NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. You have to restart the Plasma session for these changes to take effect. Project-Id-Version: kcminput
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-03-12 13:39+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: NorwegianNynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 © 2003–2007 Fredrik Höglund <qt>Er du sikker på at du vil fjerna peikartemaet <strong>%1</strong>? <br />Alle filene som høyrer til dette temaet vert då fjerna.</qt> <qt>Du kan ikkje sletta temaet som er i bruk.<br />Du må skifta til eit anna tema først.</qt> (Tilgjengelege storleikar: %1) Avhengig av oppløysing Det finst alt eit tema som heiter %1 i mappa for ikontema. Vil du byta ut det gamle temaet? Stadfesting Peikarinnstillingar endra Peikartema Skildring Dra eller skriv inn temaadresse gaute@verdsveven.com,karl@huftis.org Fredrik Höglund Hent nytt tema Hent nye fargeoppsett frå Internett Installer frå fil Gaute Hvoslef Kvalnes,Karl Ove Hufthammer Namn Skriv over tema? Fjern tema Fila %1 ser ikkje ut til å vera eit gyldig arkiv for peikartema. Klarte ikkje lasta ned peikartema-arkivet. Sjå til at adressa %1 er rett. Fann ikkje peikartema-arkivet %1. Du må starta økta på nytt for at endringane skal tre i kraft. 