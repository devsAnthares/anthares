��    �      t  �   �      �  
   �  
   �     �     �     �     �     �                         ,     ?     K     O     ]     k     r     w     }     �     �     �     �     �     �     �     �     �     �          &     :     X     `     o     ~     �     �     �     �     �     �     �     �     �     �     �  	   �     �  
   �     �  	          	   $     .     :     K  
   R     ]     m     {     �  
   �     �     �     �  	   �     �     �     �     �     �     �     �     
     !     '  	   -     7     D     J     Z     h     u     |     �     �     �     �  
   �     �     �     �  
   �     �       	        &     .  
   3     >  	   K  
   U     `     e     j     p     u     �     �     �     �     �     �     �     �     �     �     	  	        $     )     :     F     K     S     c     u     |     �     �     �     �  	   �     �     �     �  �  �     �  
   �     �     �     �     �     �     �                
                    !     &     ,     3     8     >     E     S  	   f     p     ~  
   �     �     �     �     �     �     �  	   �     �     �     �     �     �     �     �          
                 	   $     .     5  	   B     L     U     b     i     r  
   �  	   �     �     �     �     �     �     �  	   �  
   �                                3     8  	   <     F     O     V     f  	   |     �     �  
   �     �     �     �     �     �     �     �     �                    !     2  	   A  	   K     U     h  	   w     �     �     �  	   �     �     �     �     �     �     �     �     �     �     �               !     2     A     U  
   ]     h  	   y     �     �     �     �     �     �     �     �     �     �  	   �     �     �  	   �  
   	          !        ^           	   +                y   A       2       n   i       /       �   :       b   �   g       w             B   j   �   1   L   6   <   �   �   8       K   �   ]   E   0   Q   s              D       V      Y      %   G      !   R       X       l   3   '   >          $               ;   O       m   �   q       P   u       |   �       {          @   W   .       f   a                  T   c       (   ?       4       ~      *       [   r   �          I   )      \   d   J   7   p                  -   N            F   `           �   h   H      "   M   C       5   ,       
          k   }   e           =   x   9   z             &              �   _   �   Z       S   o   t   U                           v       #        Accessible Appendable Audio Available Content BD BD-R BD-RE Battery Blank Block Blu Ray Recordable Blu Ray Rewritable Blu Ray Rom Bus CD Recordable CD Rewritable CD Rom CD-R CD-RW Camera Camera Battery Can Change Frequency Capacity Cdrom Drive Charge Percent Charge State Charging Compact Flash DVD DVD Plus Recordable DVD Plus Recordable Duallayer DVD Plus Rewritable DVD Plus Rewritable Duallayer DVD Ram DVD Recordable DVD Rewritable DVD Rom DVD+DL DVD+DLRW DVD+R DVD+RW DVD-R DVD-RAM DVD-RW Data Description Device Device Types Disc Type Discharging Drive Type Emblems Encrypted Encrypted Container File Path File System File System Type Floppy Free Space Free Space Text Fully Charged HD DVD Recordable HD DVD Rewritable HD DVD Rom HDDVD HDDVD-R HDDVD-RW Hard Disk Hotpluggable Icon Ide Ieee1394 Ignored In Use Keyboard Battery Keyboard Mouse Battery Label Major Max Speed Memory Stick Minor Monitor Battery Mouse Battery Not Charging Number Operation result Optical Drive OpticalDisc Other PDA Battery Parent UDI Partition Table Phone Battery Platform Plugged In Portable Media Player Primary Battery Processor Product Raid Read Speed Rechargeable Removable Rewritable Sata Scsi SdMmc Size Smart Media State Storage Access Storage Drive Storage Volume Super Video CD Supported Drivers Supported Media Supported Protocols Tape Temperature Temperature Unit Timestamp Type Type Description UPS Battery UUID Unknown Unknown Battery Unknown Disc Type Unused Usage Usb Vendor Video Blu Ray Video CD Video DVD Write Speed Write Speeds Xd Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2015-10-08 21:48+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Tilgjengeleg Tilleggbar Lyd Tilgjengeleg innhald BD BD-R BD-RE Batteri Tom Blokk BD-R BD-RE BD-ROM Buss CD-R CD-RW CD-ROM CD-R CD-RW Kamera Kamerabatteri Kan endra frekvens Kapasitet CD-ROM-spelar Ladingsprosent Ladestatus Ladar Compact Flash DVD DVD+R DVD+R DL DVD+RW DVD+RW DL DVD-RAM DVD-R DVD-RW DVD-ROM DVD+DL DVD+DLRW DVD+R DVD+RW DVD-R DVD-RAM DVD-RW Data Skildring Eining Einingstypar Platetype Ladar ut Stasjonstype Emblem Kryptert Kryptert behaldar Filadresse Filsystem Filsystemtype Diskett­stasjon Ledig plass Ledig plass-tekst Ferdig ladd HD DVD-R HD DVD-RW HD DVD-ROM HDDVD HDDVD-R HDDVD-RW Harddisk Hotplug-kompatibel Ikon IDE IEEE 1394 Ignorert I bruk Tastaturbatteri Tastatur-/musebatteri Merkelapp Hovud Høgste fart Minnepinne Del Skjermbatteri Musebatteri Ladar ikkje Nummer Handlingsresultat Optisk stasjon Optisk plate Anna PDA-batteri Forelder-UDI Partisjonstabell Telefonbatteri Plattform Kopla til Berbar mediespelar Primærbatteri Prosessor Produkt RAID Lesefart Oppladbar Flyttbar Overskrivbar SATA SCSI SD MMC Storleik Smart Media Status Lagringstilgang Lagringsstasjon Lagringsplass Super Video-CD Støtta drivarar Støtta medium Støtta protokollar Kassett Temperatur Temperatureining Tidspunkt Type Typeskildring UPS-batteri UUID Ukjend Ukjent batteri Ukjend platetype Ubrukt Bruk USB Produsent Video Blu-ray Video-CD Video-DVD Skrivefart Skrivefartar XD 