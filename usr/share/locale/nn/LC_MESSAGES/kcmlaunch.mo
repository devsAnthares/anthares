��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  �  K     C      K    l  M  �  B   :
     }
     �
     �
     �
     �
     �
      �
                                               
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2018-02-12 19:31+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
  sekund Tidsgrense &for oppstartsvarsel: <h1>Varsel på oppgåvelinja</h1>
Ein annan måte å varsla om programstart på er å leggja inn ein knapp med eit roterande timeglas på oppgåvelinja. Dette signaliserer at programmet er i ferd med å starta. Enkelte program er ikkje laga for denne typen oppstartsvarsel. I så fall vil peikaren slutta å blinka etter tidsgrensa som er gjeven i «Tidsgrense for oppstartsvarsel». <h1>Ventepeikar</h1>
I Plasma er ventepeikaren ein måte å signalisera programstart på. For å slå ventepeikaren på, vel type ventepeikar i nedtrekksfeltet. Enkelte program er ikkje laga for denne typen oppstartsvarsel. I så fall vil peikaren slutta å blinka etter tidsgrensa som er gjeven i «Tidsgrense for oppstartsvarsel». <h1>Programstart</h1>
Her kan du setja opp signal om programstart. Blinkande peikar Sprettande peikar &Ventepeikar Vis varse&l på oppgåvelinja Ingen ventepeikar Passiv ventepeikar Tidsgrense &for oppstartsvarsel: &Varsel på oppgåvelinja 