��          4      L       `   $   a   /   �   
  �   *   �  0   �                    Finds Konsole sessions matching :q:. Lists all the Konsole sessions in your account. Project-Id-Version: plasma_runner_konsolesessions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2009-06-23 13:19+0200
Last-Translator: Eirik U. Birkeland <eirbir@gmail.com>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: MagicPO 0.4
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Finn Konsoll-økter med treff på «:q:». Listar opp alle Konsoll-øktene på kontoen din. 