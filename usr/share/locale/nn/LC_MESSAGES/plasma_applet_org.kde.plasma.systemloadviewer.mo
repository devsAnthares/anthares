��    &      L  5   |      P     Q     l     y     �     �     �     �     �     �     �     �     �  &   �               #     ,     3     ?     M     U     ]     d     s     �     �     �     �     �     �     �     �     �     �  
   �                   '     )     2     N     U  	   b     l     y     �     �     �     �  ,   �               *     1     8     G     T     ]     j     p     �     �     �     �     �     �     �     �     �  
   �     
	     !	     4	     <	                           	      #      $                                                    &                                          !          %                            "   
    Abbreviation for secondss Application: Average clock: %1 MHz Bar Buffers: CPU CPU %1 CPU monitor CPU%1: %2% @ %3 Mhz CPU: %1% CPUs separately Cache Cache Dirty, Writeback: %1 MiB, %2 MiB Cache monitor Cached: Circular Colors Compact Bar Dirty memory: General IOWait: Memory Memory monitor Memory: %1/%2 MiB Monitor type: Nice: Set colors manually Show: Swap Swap monitor Swap: %1/%2 MiB Sys: System load Update interval: Used swap: User: Writeback memory: Project-Id-Version: plasma_applet_systemloadviewer
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-29 03:31+0200
PO-Revision-Date: 2018-01-27 12:13+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 s Program: Gjennomsnittsklokke: %1 MHz Søyle Mellomlager: Prosessor Prosessor %1 Prosessor-overvakar Prosessor %1: %2% @ %3 Mhz Prosessor: %1 % Prosessorar for seg Mellomlager Skite mellomlager, writeback: %1 MiB, %2 MiB Mellomlager-overvakar Mellomlagra: Sirkel Fargar Kompakt søyle Skite minne: Generelt I/U-venting: Minne Minne-overvakar Minne: %1/%2 MiB Skjermtype: Snillverdi: Vel fargar manuelt Vis: Vekselminne Veksleminne-overvakar Veksleminne: %1/%2 MiB Sys: Systemlast Oppdateringsintervall: Brukt veksleminne: Brukar: Writeback-mine: 