��            )   �      �  !   �  "   �  '   �  ,     #   ;  &   _  !   �  &   �  '   �     �                 V   $  1   {     �  *   �     �        
     
   "     -     6     ;     D     T     [     a     g  �  p     p     v     {     �     �     �     �     �     �     �     �     �     �  X   �  2   S  	   �  %   �     �     �  	   �  
   �     	     	     	     	     -	  
   2	     =	     B	                                                                          
                                                    	           @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Borders @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large Application menu Border si&ze: Buttons Close Close by double clicking:
 To open the menu, keep the button pressed until it appears. Close windows by double clicking &the menu button Context help Drag buttons between here and the titlebar Drop here to remove button Get New Decorations... Keep above Keep below Maximize Menu Minimize On all desktops Search Shade Theme Titlebar Project-Id-Version: kcmkwindecoration
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-04-14 02:49+0200
PO-Revision-Date: 2016-10-30 17:38+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Enorm Stor Ingen kantlinjer Ingen sidekantlinjer Vanleg Altfor stor Ørliten Kolossal Svært stor Programmeny Kant&storleik: Knappar Lukk Lukk ved å dobbeltklikka.
 For å visa menyen, hald inne knappen til menyen dukkar opp. Lukk vindauga ved å dobbeltklikka på menyknappen Emnehjelp Dra knappar mellom her og tittellinja Slepp her for å fjerna knapp Hent ny vindaugspynt … Hald over Hald under Maksimer Meny Minimer På alle skrivebord Søk Fald saman Tema Tittellinje 