��          �            h     i     �  #   �      �      �     �  J        [  &   y     �  T   �       *   $     O  �  ]          $     4     S     n     ~  D   �     �      �     
  Z        y  !   �     �                            	                                             
       Also index file content Configure File Search Copyright 2007-2010 Sebastian Trüg Do not search in these locations EMAIL OF TRANSLATORSYour emails Enable File Search File Search helps you quickly locate all your files based on their content Folder %1 is already excluded Folder's parent %1 is already excluded NAME OF TRANSLATORSYour names Not allowed to exclude root folder, please disable File Search if you do not want it Sebastian Trüg Select the folder which should be excluded Vishesh Handa Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2018-01-22 21:19+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Indekser òg innhaldet Set opp filsøk © 2007–2010 Sebastian Trüg Ikkje søk i desse mappene karl@huftis.org Bruk filsøk Filsøk hjelper deg å raskt finna filer basert på kva dei inneheld Mappa %1 er alt utelaten Foreldermappa %1 er alt utelaten Karl Ove Hufthammer Du kan ikkje ekskludera rotmappa. Slå av filsøket dersom du ikkje ønskjer å bruka det. Sebastian Trüg Vel kva mapper som skal utelatast Vishesh Handa 