��    ,      |  ;   �      �  (   �     �  �   �     �     �     �     �     �     �     �     �     �               %     1      J     k     s     �     �     �     �     �     �     �               /     4     I     Y     l     y     �     �      �  7   �                     1     6  �  <     +	     8	     >	  
   ]	     h	     q	     	  	   �	     �	  
   �	     �	     �	     �	     �	  	   �	     
     
     +
     4
     C
     R
     e
     y
     �
  !   �
     �
     �
     �
     �
  "   �
       "   -  
   P     [     l     �     �  3   �     �     �     �               !                 %   +   ,      "   #                               &                  	                           )                             *      '      $      (                                
    %1 is the name of a session%1 (Wayland) ... @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) @title:windowSelect Image Advanced Author Auto &Login Background: Clear Image Commands Could not decompress archive Cursor Theme: Customize theme Default Description Download New SDDM Themes EMAIL OF TRANSLATORSYour emails General Get New Theme Halt Command: Install From File Install a theme. Invalid theme package Load from file... Login screen using the SDDM Maximum UID: Minimum UID: NAME OF TRANSLATORSYour names Name No preview available Reboot Command: Relogin after quit Remove Theme SDDM KDE Config SDDM theme installer Session: The default cursor theme in SDDM The theme to install, must be an existing archive file. Theme Unable to install theme Uninstall a theme. User User: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2018-01-22 21:10+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 %1 (Wayland)  … (Tilgjengelege storleikar: %1) Vel bilete Avansert Opphavsperson Automatisk &innlogging Bakgrunn: Fjern biletet Kommandoar Klarte ikkje pakka ut arkivfil Peikartema: Tilpass tema Standard Skildring Last ned nye SDDM-tema karl@huftis.org Generelt Hent nytt tema Stoppkommando: Installer frå fil Installer eit tema. Installer temapakke Last frå fil … Innloggingsbilete som brukar SDDM Maksimums-UID: Minimums-UID: Karl Ove Hufthammer Namn Inga førehandsvising tilgjengeleg Omstartskommando: Logg inn på nytt etter avslutting Fjern tema SDDM KDE-oppsett SDDM-temainstallering Økt: Standard peikartema i SDDM Temaet som skal installerast. Må vera ei arkivfil. Tema Klarte ikkje installera temaet Avinstaller eit tema. Brukar Brukar: 