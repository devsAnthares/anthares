��          L      |       �       �      �   #   �        �      �  �     �     �     �       �   4                                         EMAIL OF TRANSLATORSYour emails NAME OF TRANSLATORSYour names Standard Actions successfully saved Standard Shortcuts The changes have been saved. Please note that:<ul><li>Applications need to be restarted to see the changes.</li>    <li>This change could introduce shortcut conflicts in some applications.</li></ul> Project-Id-Version: KDE 4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2015-09-19 14:20+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 karl@huftis.org Karl Ove Hufthammer Standardhandlingane vart lagra Standard snøggtastar Endringane er lagra. Pass på dette:<ul><li>Programma må startast på nytt for at endringane skal verta tekne i bruk.</li>    <li>Denne endringa kan laga snøggtastkonfliktar i nokre program.</li></ul> 