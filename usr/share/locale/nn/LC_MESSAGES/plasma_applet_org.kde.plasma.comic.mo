��    =        S   �      8  ;   9     u     �     �     �  6   �  A   
     L     U  $   Y  
   ~     �  #   �     �     �     �     �     �  7        >     [     w  '   �     �     �  $   �  ,   �          3     C     K     ]     y     �     �     �     �     �  �   �  9   �	  "   �	     �	     �	     
     *
     <
     R
     c
  %   u
     �
     �
     �
     �
     �
  
   �
  7        :     T     l     t    �  ?   �     �     �     �          %     8     T     ]  +   b  	   �     �  4   �     �     �               .  1   ;     m     �     �  6   �     �     �  +   �  3   %     Y     p     �     �     �     �     �     �     �     
       �   .  ?        L     h  "   q     �     �     �     �     �  '   �       !   "     D     c     j  
   p     {     �     �     �     �     !          3      0      (                 1      "          =   .   6                                         	      :          5       ,   #   &   7       ;   '             -   
             8                 *           %           <   4   9                )   /   $          2             +       

Choose the previous strip to go to the last cached strip. &Create Comic Book Archive... &Save Comic As... &Strip Number: *.cbz|Comic Book Archive (Zip) @option:check Context menu of comic image&Actual Size @option:check Context menu of comic imageStore current &Position Advanced All An error happened for identifier %1. Appearance Archiving comic failed Automatically update comic plugins: Cache Check for new comic strips: Comic Comic cache: Configure... Could not create the archive at the specified location. Create %1 Comic Book Archive Creating Comic Book Archive Destination: Display error when getting comic failed Download Comics Error Handling Failed adding a file to the archive. Failed creating the file with identifier %1. From beginning to ... From end to ... General Get New Comics... Getting comic strip failed: Go to Strip Information Jump to &current Strip Jump to &first Strip Jump to Strip ... Manual range Maybe there is no Internet connection.
Maybe the comic plugin is broken.
Another reason might be that there is no comic for this day/number/string, so choosing a different one might work. Middle-click on the comic to show it at its original size No zip file is existing, aborting. Range: Show arrows only on mouse over Show comic URL Show comic author Show comic identifier Show comic title Strip identifier: The range of comic strips to archive. Update Visit the comic website Visit the shop &website an abbreviation for Number# %1 days dd.MM.yyyy here strip means comic strip&Next Tab with a new Strip in a range: from toFrom: in a range: from toTo: minutes strips per comic Project-Id-Version: plasma_applet_comic
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-07-18 03:20+0200
PO-Revision-Date: 2015-06-06 09:23+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 

Vel førre stripe for å gå til den sist mellomlagra stripa. &Lag teikneseriearkiv … Lagra &stripa som … &Stripenummer: *.cbz|Teikneseriearkiv (Zip) &Verkeleg storleik Lagra gjeldande &plassering Avansert Alle Det oppstod ein feil for identifikatoren %1 Utsjånad Feil ved arkivering Oppdater automatisk programtillegg for teikneseriar: Mellomlager Sjå etter nya striper: Teikneserie Mellomlager for teikneserie: Set opp … Klarte ikkje å lagra arkivet på oppgjeven stad. Lag %1 teikneseriearkiv Lagar teikneseriearkiv Mål: Vis feilmelding når dry ikkje lukkast å henta stripe Last ned striper Feilhandtering Klarte ikkje å leggja til fil til arkivet. Klarte ikkje å oppretta fila med identifikator %1. Frå byrjinga til … Frå slutten til … Generelt Hent nye striper … Klarte ikkje henta stripe: Gå til stripe Informasjon Gå til &nyaste stripe Gå til &første stripe Gå til stripe … Manuelt område Kanskje det ikkje er noko Internett-samband,
eller kanskje programtillegget for teikneseriar er øydelagt.
Ein annan grunn kan vera at det ikkje finst noko stripe for denne dagen/talet/teksten. Prøv i så fall ein annan. Midtklikk på teikneserien for å visa han i opphavleg storleik Finst inga zip-fil. Avbryt. Område: Vis piler berre under musepeikaren Vis nettadresse Vis teikneserieskapar Vis stripenummer Vis stripetittel Stripeidentifikator: Teikneseriestriper som skal arkiverast. Oppdater Besøk heimesida til teikneserien &Besøk heimesida til butikken nr. %1 dagar dd.MM.yyyy &Neste fane med ei ny stripe Frå: Til: minutt striper per teikneserie 