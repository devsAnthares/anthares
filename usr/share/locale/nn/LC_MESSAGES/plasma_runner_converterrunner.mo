��          4      L       `   �   a   L   �   �  E  �   9  	   �                    Converts the value of :q: when :q: is made up of "value unit [>, to, as, in] unit". You can use the Unit converter applet to find all available units. list of words that can used as amount of 'unit1' [in|to|as] 'unit2'in;to;as Project-Id-Version: KDE 4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2010-01-30 21:13+0100
Last-Translator: Eirik U. Birkeland <eirbir@gmail.com>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Gjer om verdien av :q: når «verdi eining [>, til, som, i] eining» utgjer :q:. Du kan bruka einingsomreknaren for å finna alle tilgjengelege einingar. i;til;som 