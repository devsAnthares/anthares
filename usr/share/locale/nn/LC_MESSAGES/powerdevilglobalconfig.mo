��          �      l      �     �     �     �  *         +  >   ?  9   ~     �     �  
   �      �       	        (  !   :     \  $   {  	   �     �  �   �  �  8     +     /     F     T     i  P   �  H   �           8     M      ]     ~     �     �  #   �  &   �  2   �     $     4  �   E                              
                                                  	                  % &Critical level: &Low level: <b>Battery Levels                     </b> A&t critical level: Battery will be considered critical when it reaches this level Battery will be considered low when it reaches this level Configure Notifications... Critical battery level Do nothing EMAIL OF TRANSLATORSYour emails Enabled Hibernate Low battery level Low level for peripheral devices: NAME OF TRANSLATORSYour names Pause media players when suspending: Shut down Suspend The Power Management Service appears not to be running.
This can be solved by starting or scheduling it inside "Startup and Shutdown" Project-Id-Version: KDE 4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-11 03:05+0200
PO-Revision-Date: 2018-01-25 20:55+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: NorwegianNynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
  % &Kritisk batterinivå: &Lågt nivå: <b>Batterinivå</b>> &Kritisk lågt batterinivå: Batteriet vert rekna å vera på kritisk lågt nivå når det når dette nivået Batteriet vert rekna å vera på lågt nivå når det når dette nivået Set opp varslingar … Kritisk batterinivå Ikkje gjer noko eirbir@gmail.com,karl@huftis.org På Gå i dvalemodus Lågt batterinivå Lågt nivå for tilleggsmaskinvare: Eirik U. Birkeland,Karl Ove Hufthammer Set mediespelarar på pause ved kvile-/dvalemodus: Slå av maskina Gå i kvilemodus Det ser ikkje ut til at straumstyringstenesta køyrer.
Du kan løysa dette ved å starta ho eller setja ho opp for automatisk start under «Oppstart og avslutning». 