��    	      d      �       �      �   -   �   *   %      P  '   q  
   �     �     �  �  �     �  &   �  *   �     &  &   6     ]     k     x                   	                           (c) 2009 Marco Martin Display informational tooltips on mouse hover Display visual feedback for status changes EMAIL OF TRANSLATORSYour emails Global options for the Plasma Workspace Maintainer Marco Martin NAME OF TRANSLATORSYour names Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-08 06:27+0100
PO-Revision-Date: 2018-01-24 20:06+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 © 2009 Marco Martin Vis hjelpebobler når peikaren er over Visuell tilbakemelding ved statusendringar karl@huftis.org Globale val for Plasma-arbeidsområdet Vedlikehaldar Marco Martin Karl Ove Hufthammer 