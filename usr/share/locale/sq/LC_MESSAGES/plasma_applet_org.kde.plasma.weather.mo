��          �      |      �     �  '     "   0     S     m     �  ?   �     �  &   �        %   ?     e  >        �     �  '   �  !        >     ^     }  3   �  �  �     �     �     �     �     �     �     �                    +     8     D     ]     m     r     �     �     �     �     �                                                                       	   
                       Degree, unit symbol° Forecast period timeframe1 Day %1 Days High & Low temperatureH: %1 L: %2 High temperatureHigh: %1 Low temperatureLow: %1 Short for no data available- Shown when you have not set a weather providerPlease Configure Wind conditionCalm content of water in airHumidity: %1%2 distance, unitVisibility: %1 %2 ground temperature, unitDewpoint: %1 humidex, unitHumidex: %1 pressure tendency, rising/falling/steadyPressure Tendency: %1 pressure, unitPressure: %1 %2 temperature, unit%1%2 visibility from distanceVisibility: %1 weather warningsWarnings Issued: weather watchesWatches Issued: wind direction, speed%1 %2 %3 windchill, unitWindchill: %1 winds exceeding wind speed brieflyWind Gust: %1 %2 Project-Id-Version: kdeplasma-addons
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-28 05:50+0100
PO-Revision-Date: 2010-03-08 14:39+0000
Last-Translator: Vilson Gjeci <vilsongjeci@gmail.com>
Language-Team: Albanian <sq@li.org>
Language: sq
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2011-04-22 00:46+0000
X-Generator: Launchpad (build 12883)
 ° 1 Ditë %1 Ditë H: %1 L: %2 I Lartë: %1 I Ulët: %1 - Ju Lutemi Konfigurojeni Qetë Lagështia: %1%2 Dukshmëria: %1 %2 Dewpoint: %1 Humidex: %1 Tendenca e Presionit: %1 Presioni: %1 %2 %1%2 Dukshmëria: %1 Paralajmërimet e lëshuara: Njoftimet e lëshuara: %1 %2 %3 Windchill: %1 Wind Gust: %1 %2 