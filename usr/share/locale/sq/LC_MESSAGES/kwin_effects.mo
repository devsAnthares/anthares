��    ^           �      �     �     �                         !     *     3     ?     G     K  5   N  %   �     �     �  
   �     �     �     	     	  	   	  
   	  	   !	  
   +	     6	     H	     O	     [	     h	     t	  
   �	     �	     �	     �	     �	     �	     �	      �	     �	  "   �	     
     1
  
   J
     U
     Y
  
   `
     k
     n
     {
     �
     �
     �
     �
     �
     �
     �
  	   �
     �
     �
     �
     �
     �
     �
  
   �
     �
     �
     �
                    %  	   7     A     N     S     Z     a     g     m     r     ~  
   �     �     �  	   �     �  	   �     �     �  
   �     �     �  �  �     �     �     �     �     �     �  
   �            
        %     )     ,     5     C     I  
   [     f     v     �     �  
   �     �  	   �     �     �     �     �     �     �     �     
     '     0  !   8     Z     _     m     z     �     �     �     �     �     �  
   �  
   �     �     �                    $  	   )     3     ;     A     R     `     g     t     y     �     �  	   �  
   �     �     �     �     �  	   �     �               ,     5     ;     D     J     P     W     c     u     �     �     �     �     �     �     �     �     �     �                ,   R   *      1   D   >   2                 8       <   )       F   J       U       @   V   .   Y                   N   ]   0       7   	             Z       9   6               C   S       "   Q          W   X       :   5   &      
   (                          I                 $           M             E   !   ;   P   3   \   [   K                           O       +   #   G   4   B   ^                     ?   H   A   %          L   -       '   =   /   T     %  msec  pixel  pixels  px  ° &Color: &Height: &Radius: &Stiffness: &Width: -90 90 @title:group actions when clicking on desktopDesktop @title:tab Advanced SettingsAdvanced @title:tab Basic SettingsBasic Activate window Activation Additional Options Advanced Alt Angle: Animation Appearance Automatic Background Background color: Bottom Bottom Left Bottom Right Bottom-Left Bottom-Right Cap color: Caps Center Clear All Mouse Marks Ctrl Custom Desktop Cube Desktop name alignment:Disabled Disabled Duration of flip animationDefault Duration of rotationDefault Duration of zoomDefault End effect Far Faster Filter:
%1 In Layout mode: Left Less Light Meta More Natural Near Nicer No action Nowhere Opacity Opaque Out Pager Plane Reflection Reflections Regular Grid Right Rotation duration: Shift Shortcut Show Desktop Grid Show caps Show desktop Size Sphere Strong Tab 1 Tab 2 Text Text alpha: Text color: Text font: Top Top Left Top Right Top-Left Top-Right Translucency Transparent Wallpaper: Windows Zoom Project-Id-Version: kdebase-workspace
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-02 03:18+0100
PO-Revision-Date: 2010-08-19 12:19+0000
Last-Translator: Vilson Gjeci <vilsongjeci@gmail.com>
Language-Team: Albanian <sq@li.org>
Language: sq
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2011-05-06 01:37+0000
X-Generator: Launchpad (build 12959)
  %  msec  pixel  pixelë  px  ° &Ngjyra: &Lartësi: &Rrezja: &Stiffness: &Gjerësi: -90 90 Desktopi Të Avancuara Bazë Aktivizo dritaren Aktivizimi Opsione Shtesë Të Avancuara Alt Këndi: Animacioni Pamja Automatik Sfondi Ngjyra e sfondit: Poshtë Poshtë Majtas Poshtë Djathtas Poshtë-Majtas Fundi-Djathtas Ngjyra e gërmave të mëdha Kapitale Qendër Pastro të Gjitha Gjurmët e Miut Ctrl Personalizuar Kub Desktopi Çaktivizuar Çaktivizuar I Parazgjedhur I Parazgjedhur I Parazgjedhur Përfundo efektin Larg Më Shpejt Filtri:
%1 Brenda Mënyra e daljes: Majtas Më i vogël I Lehtë Meta Me tepër Natyror Afër Më i Këndshëm Asnjë veprim Askund Transparenca Opak Jashtë Pager Plani Pasqyrimi Pasqyrimet Rrjetë e Rregullt Djathtas Kohëzgjatja e rrotullimit: Shift Shkurtore Shfaq Rrjetën e Desktopit Shfaq gërmat e mëdha Shfaq desktopin Përmasa Sfera I fortë Tab 1 Tab 2 Teksti Text alpha: Ngjyra e tekstit: Gërma e tekstit: Kreu Lart Mjatas Sipër në të djathtë Kreu-Majtas Kreu-Djathtas Transparenca E tejdukshme Figura e Sfondit: Dritaret Zmadho 