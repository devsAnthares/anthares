��          <      \       p   !   q   3   �      �   �  �   ,   �  C   �     )                   Finds Kate sessions matching :q:. Lists all the Kate editor sessions in your account. Open Kate Session Project-Id-Version: kdeplasma-addons
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-18 03:08+0100
PO-Revision-Date: 2009-07-30 22:15+0000
Last-Translator: Vilson Gjeci <vilsongjeci@gmail.com>
Language-Team: Albanian <sq@li.org>
Language: sq
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2011-04-22 00:53+0000
X-Generator: Launchpad (build 12883)
Plural-Forms: nplurals=2; plural=n != 1;
 Gjej seksionet e Kate që përputhen me :q:. Listo të gjitha seksionet e modifikuesit Kate në llogarinë tuaj. Hapni Seksionin Kate 