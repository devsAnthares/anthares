��    3      �  G   L      h     i     o     �     �     �     �  !   �  '   �          -  -   ?     m     r     �     �     �     �     �     �     �      �       	             .     ;  
   I     T     s     �  A   �     �     �     �                     ,     :  3   A     u  �   �     *     0  ;   6     r     �     �  %   �  b   �  �  %	     �
  M   �
  *        <     Z     v  I   �  /   �  %     +   2  �   ^  	   �  S   �  	   >     H     Q  9   Z  %   �  (   �     �     �          $  5   A     w  "   �     �     �  E   �  ?     �   ]     �       /        E  !   _     �  6   �     �  �   �  %   |  �  �     7     C  �   O  B     9   X     �  \   �  �   �             *      &      1           .   #                 (      -      3       +                                       '   0   !           2             "          %                         ,   )                    /                   
   	         $    %1 Hz &Disable All Outputs &Reconfigure (c), 2012-2013 Daniel Vrátil 90° Clockwise 90° Counterclockwise @title:windowDisable All Outputs @title:windowUnsupported Configuration Active profile Advanced Settings Are you sure you want to disable all outputs? Auto Break Unified Outputs Button Checkbox Combobox Configuration for displays Daniel Vrátil Display Configuration Display: EMAIL OF TRANSLATORSYour emails Enabled Group Box Identify outputs KCM Test App Laptop Screen Maintainer NAME OF TRANSLATORSYour names No Primary Output No available resolutions No kscreen backend found. Please check your kscreen installation. Normal Orientation: Primary display: Radio button Refresh rate: Resolution: Scale Display Scale: Scaling changes will come into effect after restart Screen Scaling Sorry, your configuration could not be applied.

Common reasons are that the overall screen size is too big, or you enabled more displays than supported by your GPU. Tab 1 Tab 2 Tip: Hold Ctrl while dragging a display to disable snapping Unified Outputs Unify Outputs Upside Down Warning: There are no active outputs! Your system only supports up to %1 active screen Your system only supports up to %1 active screens Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-24 03:18+0200
PO-Revision-Date: 2017-04-09 10:32-0600
Last-Translator: A S Alam <aalam@users.sf.net>
Language-Team: Punjabi <kde-i18n-doc@kde.org>
Language: pa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 %1 Hz ਸਭ ਆਉਟਪੁੱਟਾਂ ਨੂੰ ਅਸਮਰੱਥ ਕਰੋ(&D) ਮੁੜ-ਸੰਰਚਨਾ ਕਰੋ(&R) (c), 2012-2013 Daniel Vrátil 90° ਸੱਜੇ ਦਾਅ 90° ਖੱਬੇ ਦਾਅ ਸਭ ਆਉਟਪੁੱਟਾਂ ਨੂੰ ਅਸਮਰੱਥ ਕਰੋ ਗ਼ੈਰ-ਸਹਾਇਕ ਸੰਰਚਨਾ ਸਰਗਰਮ ਪਰੋਫਾਇਲ ਤਕਨੀਕੀ ਸੈਟਿੰਗਾਂ ਕੀ ਤੁਸੀਂ ਸਭ ਆਉਟਪੁੱਟਾਂ ਨੂੰ ਅਸਮਰੱਥ ਕਰਨਾ ਚਾਹੁੰਦੇ ਹੋ? ਆਟੋ ਇਕਰੂਪ ਆਉਟਪੁੱਟਾਂ ਨੂੰ ਵੱਖ-ਵੱਖ ਕਰੋ ਬਟਨ Checkbox Combobox ਡਿਸਪਲੇਅ ਲਈ ਸੰਰਚਨਾ ਕਰੋ ਡੈਨੀਅਲ ਵਰਾਟਿਲ ਡਿਸਪਲੇਅ ਸੰਰਚਨਾ ਡਿਸਪਲੇਅ: aalam@users.sf.net ਸਮਰੱਥ ਹੈ ਗਰੁੱਪ ਬਾਕਸ ਆਉਟਪੁੱਟਾਂ ਨੂੰ ਪਛਾਣੋ KCM ਟੈਸਟ ਐਪ ਲੈਪਟਾਪ ਸਕਰੀਨ ਪਰਬੰਧਕ ਅ ਸ ਆਲਮ ਕੋਈ ਪ੍ਰਾਇਮਰੀ ਆਉਟਪੁੱਟ ਨਹੀਂ ਕੋਈ ਰੈਜ਼ੋਲੂਸ਼ਨ ਮੌਜੂਦ ਨਹੀਂ ਕੋਈ kscreen ਬੈਕਐਂਡ ਨਹੀਂ ਮਿਲਿਆ। ਆਪਣੀ kscreen ਇੰਸਟਾਲੇਸ਼ਨ ਦੀ ਜਾਂਚ ਕਰੋ। ਸਧਾਰਨ ਸਥਿਤੀ: ਪ੍ਰਾਇਮਰੀ ਡਿਸਪਲੇਅ: ਰੇਡੀਓ ਬਟਨ ਤਾਜ਼ਾ ਕਰਨ ਰੇਟ: ਰੈਜ਼ੋਲੂਸ਼ਨ: ਡਿਸਪਲੇਅ ਨੂੰ ਸਕੇਲ ਕਰੋ ਸਕੇਲ: ਸਕੇਲ ਕਰਨ ਦੀਆਂ ਤਬਦੀਲੀਆਂ ਮੁੜ-ਚਾਲੂ ਕਰਨ ਦੇ ਬਾਅਦ ਹੀ ਲਾਗੂ ਹੋਣਗੀਆਂ ਸਕਰੀਨ ਸਕੇਲਿੰਗ ਅਫ਼ਸੋਸ, ਤੁਹਾਡੀ ਸੰਰਚਨਾ ਲਾਗੂ ਨਹੀਂ ਕੀਤੀ ਜਾ ਸਕੀ।

ਸਕਰੀਨ ਦਾ ਪੂਰਾ ਆਕਾਰ ਬਹੁਤ ਵੱਡਣਾ ਹੋਣ ਜਾਂ ਤੁਹਾਡੇ ਵਲੋਂ ਆਪਣੇ GPU ਵਲੋਂ ਸਹਾਇਕ ਡਿਸਪਲੇਅ ਤੋਂ ਵੱਧ ਨੂੰ ਸਮਰੱਥ ਕਰਨਾ ਆਮ ਕਾਰਨ ਹਨ। ਟੈਬ 1 ਟੈਬ 2 ਗੁਰ: ਡਿਪਲੇਅ ਨੂੰ ਖਿੱਚਣ ਦੇ ਦੌਰਾਨ ਸਨੈਪ ਕਰਨ ਨੂੰ ਅਸਮਰੱਥ ਕਰਨ ਵਾਸਤੇ Ctrl ਨੂੰ ਦੱਬੀ ਰੱਖੋ ਆਉਟਪੁੱਟਾਂ ਨੂੰ ਇਕਸਾਰ ਕੀਤਾ ਆਉਟਪੁੱਟ ਨੂੰ ਇਕਰੂਪ ਕਰੋ ਉਲਟਾ ਸਾਵਧਾਨ: ਕੋਈ ਵੀ ਸਰਗਰਮ ਆਉਟਪੁਟ ਨਹੀਂ ਹੈ! ਤੁਹਾਡਾ ਸਿਸਟਮ ਕੇਵਲ %1 ਸਰਗਰਮ ਸਕਰੀਨ ਲਈ ਹੀ ਸਹਾਇਕ ਹੈ ਤੁਹਾਡਾ ਸਿਸਟਮ ਕੇਵਲ %1 ਸਰਗਰਮ ਸਕਰੀਨਾਂ ਲਈ ਹੀ ਸਹਾਇਕ ਹੈ 