��    )      d  ;   �      �     �     �     �     �     �     �     �  	   �     �          %     :     G     _     v     ~  
   �  
   �     �     �     �     �     �     �     �     �               .  U   C  Z   �  O   �  D   D     �     �     �  	   �  	   �     �     �  �  �     a  0   q  3   �     �     �     �     	     -	  2   F	  2   y	  6   �	  0   �	  ?   
  ?   T
     �
  '   �
  :   �
  &     D   ;  <   �  "   �     �     �          )  9   6  3   p  9   �  G   �  Q   &  =   x  -   �  V   �     ;     R      l     �  #   �     �  	   �                                             %                   	         (          &   !          '                          )   #          "                          $          
                            Adapter Add New Device Add New Device... Address Audio Audio device Available devices Bluetooth Bluetooth is Disabled Bluetooth is disabled Bluetooth is offline Browse Files Configure &Bluetooth... Configure Bluetooth... Connect Connected devices Connecting Disconnect Disconnecting Enable Bluetooth File transfer Input Input device Network No No Adapters Available No Devices Found No adapters available No connected devices Notification when the connection failed due to FailedConnection to the device failed Notification when the connection failed due to Failed:HostIsDownThe device is unreachable Notification when the connection failed due to NotReadyThe device is not ready Number of connected devices%1 connected device %1 connected devices Other device Paired Remote Name Send File Send file Trusted Yes Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:01+0100
PO-Revision-Date: 2016-06-09 17:22-0600
Last-Translator: A S Alam <aalam@users.sf.net>
Language-Team: Punjabi <kde-i18n-doc@kde.org>
Language: pa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 ਐਡਪਟਰ ਨਵੇਂ ਜੰਤਰ ਨੂੰ ਜੋੜੋ ...ਨਵੇਂ ਜੰਤਰ ਨੂੰ ਜੋੜੋ ਐਡਰੈੱਸ ਆਡੀਓ ਆਡੀਓ ਜੰਤਰ ਮੌਜੂਦ ਜੰਤਰ ਬਲਿਊਟੁੱਥ ਬਲਿਊਟੁੱਥ ਅਸਮਰੱਥ ਹੈ ਬਲਿਊਟੁੱਥ ਅਸਮਰੱਥ ਹੈ ਬਲਿਊਟੁੱਥ ਬੰਦ ਕੀਤਾ ਹੈ ਫਾਇਲਾਂ ਦੀ ਝਲਕ ਵੇਖੋ ...ਬਲਿਊਟੁੱਲ ਦੀ ਸੰਰਚਨਾ ਕਰੋ ...ਬਲਿਊਟੁੱਲ ਦੀ ਸੰਰਚਨਾ ਕਰੋ ਕੁਨੈਕਟ ਕਰੋ  ਕਨੈਕਟ ਹੋਏ ਜੰਤਰ ਕੁਨੈਕਟ ਕੀਤਾ ਜਾ ਰਿਹਾ ਹੈ ਡਿਸ-ਕੁਨੈਕਟ ਕਰੋ ਡਿਸ-ਕੁਨੈਕਟ ਕੀਤਾ ਜਾ ਰਿਹਾ ਹੈ ਬਲਿਊਟੁੱਥ ਨੂੰ ਸਮਰੱਥ ਕਰੋ ਫਾਇਲ ਟਰਾਂਸਫਰ ਇੰਪੁੱਟ ਇੰਪੁੱਟ ਜੰਤਰ ਨੈੱਟਵਰਕ ਨਹੀਂ ਕੋਈ ਐਡਪਟਰ ਉਪਲੱਬਧ ਨਹੀਂ ਕੋਈ ਜੰਤਰ ਨਹੀਂ ਲੱਭਿਆ ਕੋਈ ਐਡਪਟਰ ਉਪਲੱਬਧ ਨਹੀਂ ਕੋਈ ਕਨੈਕਟ ਹੋਇਆ ਜੰਤਰ ਨਹੀਂ ਹੈ ਜੰਤਰ ਨਾਲ ਕਨੈਕਸ਼ਨ ਕਰਨ ਲਈ ਫੇਲ੍ਹ ਹੈ ਜੰਤਰ ਪਹੁੰਚ ਵਿੱਚ ਨਹੀਂ ਹੈ ਜੰਤਰ ਤਿਆਰ ਨਹੀਂ ਹੈ %1 ਕਨੈਕਟ ਹੋਇਆ ਜੰਤਰ %1 ਕਨੈਕਟ ਹੋਏ ਜੰਤਰ ਹੋਰ ਜੰਤਰ ਪੇਅਰ ਕੀਤੇ ਰਿਮੋਟ ਦਾ ਨਾਂ ਫਾਇਲ ਭੇਜੋ ਫਾਇਲ ਨੂੰ ਭੇਜੋ ਟਰੱਸਟ ਕੀਤੇ ਹਾਂ 