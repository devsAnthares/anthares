��    K      t  e   �      `     a  ^   f  H   �          !     -     D     L  '   [     �  "   �     �     �     �  
   �     �          %  	   .     8     P     f     l          �  "   �     �     �  	   �     �     �  ?   	     D	     Q	     W	     e	     q	     �	     �	  ;   �	  &   �	      
  %   %
     K
     e
     
     �
  >   �
     �
       '   &  !   N     p     �     �     �     �     �     �     �          "     3     E     X     k     }     �     �     �     �     �     �  3     �  G     �                    0     J     M     ]     z  "   �     �     �  "   �     �       :   !     \     |     �     �  0   �  	   �  *   �          ,  R   I     �     �     �     �     �  #   �     �       *        D     R     o     |     �     �     �     �      �               '      :     [     o     t  2   �      �     �     �     �     
               4     I     W     ^     l     �     �     �     �     �     �     �     �     �          '   I   8                            >   5   B           .   6   !      +       -   4       2         "       C         %       9       0   7   @      )         
   H       :         G              F   *      /   =               &                            D   #      A         K   ,       1      (   $          ?   J       <      3      	      ;       E                   min %1 is the weather condition, %2 is the temperature,  both come from the weather provider%1 %2 A weather station location and the weather service it comes from%1 (%2) Beaufort scale bft Celsius °C Degree, unit symbol° Details Fahrenheit °F Forecast period timeframe1 Day %1 Days Hectopascals hPa High & Low temperatureH: %1 L: %2 High temperatureHigh: %1 Inches of Mercury inHg Kelvin K Kilometers Kilometers per Hour km/h Kilopascals kPa Knots kt Location: Low temperatureLow: %1 Meters per Second m/s Miles Miles per Hour mph Millibars mbar N/A No weather stations found for '%1' Notices Percent, measure unit% Pressure: Search Short for no data available- Shown when you have not set a weather providerPlease Configure Temperature: Units Update every: Visibility: Weather Station Wind conditionCalm Wind speed: certain weather condition (probability percentage)%1 (%2%) content of water in airHumidity: %1%2 distance, unitVisibility: %1 %2 ground temperature, unitDewpoint: %1 humidex, unitHumidex: %1 pressure tendencyfalling pressure tendencyrising pressure tendencysteady pressure tendency, rising/falling/steadyPressure Tendency: %1 pressure, unitPressure: %1 %2 temperature, unit%1%2 visibility from distanceVisibility: %1 weather warningsWarnings Issued: weather watchesWatches Issued: wind directionE wind directionENE wind directionESE wind directionN wind directionNE wind directionNNE wind directionNNW wind directionNW wind directionS wind directionSE wind directionSSE wind directionSSW wind directionSW wind directionVR wind directionW wind directionWNW wind directionWSW wind direction, speed%1 %2 %3 wind speedCalm windchill, unitWindchill: %1 winds exceeding wind speed brieflyWind Gust: %1 %2 Project-Id-Version: plasma_applet_weather
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-28 05:50+0100
PO-Revision-Date: 2016-08-08 15:10-0600
Last-Translator: A S Alam <aalam@users.sf.net>
Language-Team: Punjabi <punjabi-users@lists.sf.net>
Language: pa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
  ਮਿੰਟ %1 %2 %1 (%2) Beaufort ਸਕੇਲ bft ਸੈਲਸੀਅਸ °C ° ਵੇਰਵੇ ਫਾਰਨਹਾਈਟ °F 1 ਦਿਨ %1 ਦਿਨ ਹੈਕਟੋਪਾਸਕਲ hPa H: %1 L: %2 ਉੱਚ: %1 ਪਾਰਾ ਦੇ ਇੰਚ inHg ਕੈਲਵਿਨ K ਕਿਲੋਮੀਟਰ ਕਿਲੋਮੀਟਰ ਪ੍ਰਤੀ ਘੰਟਾ km/h ਕਿਲੋਪਾਸਕਲ kPa ਨਾਟ kt ਟਿਕਾਣਾ: ਘੱਟ: %1 ਮੀਟਰ ਪ੍ਰਤੀ ਸਕਿੰਟ m/s ਮੀਲ ਮੀਲ ਪ੍ਰਤੀ ਘੰਟਾ mph ਮਿਲੀਬਾਰ mbar ਮੌਜੂਦ ਨਹੀਂ '%1' ਲਈ ਕੋਈ ਮੌਸਮੀ ਸਟੇਸ਼ਨ ਨਹੀਂ ਲੱਭਿਆ ਨੋਟਿਸ % ਦਬਾਓ: ਖੋਜੋ - ਸੰਰਚਨਾ ਕਰੋ ਜੀ ਤਾਮਮਾਨ: ਯੂਨਿਟਾਂ ਅੱਪਡੇਟ ਕਰੋ ਹਰੇਕ: ਦਿੱਖ: ਮੌਸਮ ਸਟੇਸ਼ਨ ਸਥਿਰ ਹਵਾ ਦੀ ਗਤੀ: %1 (%2%) ਨਮੀਂ: %1%2 ਦਿੱਖ: %1 %2 ਤਰੇਲ-ਦਰਜਾ: %1 ਨਮੀ-ਇੰਡੈਕਸ: %1 ਘੱਟਦਾ ਵੱਧਦਾ ਟਿਕਵਾਂ ਦਬਾਉ ਰੁਝਾਨ: %1 ਦਬਾਉ: %1 %2 %1%2 ਦਿੱਖ: %1 ਦਿੱਤੀਆਂ ਚੇਤਾਵਨੀਆਂ: ਦਿੱਤੀਆਂ ਵਾਚ: ਪੂ ਪੂ-ਉੱ-ਪੂ ਪੂ-ਦੱ-ਪੂ ਉੱ ਉੱ-ਪੂ ਉੱ-ਉੱ-ਪੂ ਉੱ-ਉੱ-ਪੱ ਉੱ-ਪੱ ਦੱ ਦੱ-ਪੂ ਦੱ-ਦੱ-ਪੂ ਦੱ-ਦੱ-ਪੱ ਦੱ-ਪੱ VR ਪੱ ਪੱ-ਉੱ-ਪੱ ਪੱ-ਦੱ-ਪੱ %1 %2 %3 ਸਥਿਰ ਸਰਦ-ਹਵਾ: %1 ਝੱਖੜ: %1 %2 