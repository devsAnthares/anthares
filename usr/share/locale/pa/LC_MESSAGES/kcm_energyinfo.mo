��    7      �  I   �      �     �     �     �     �     �     �               $     -     5     H     T      `     �     �     �     �     �     �     �     �                     )     7  	   C  	   M     W     d     j     �     �     �     �     �     �     �     �     �     �  @   �     7     @     \     c     j     r     �     �     �  4   �     �  �  �     �	     �	     �	  2   �	     �	     �	     �	     
  $   %
     J
     W
     [
  -   o
     �
     �
     �
  )   �
            ,   1  #   ^  )   �     �     �     �          +     G     a     ~     �     �     �  *   �     �     �           "     9     P     W     g  �   z       F   '     n     ~     �  A   �     �     �  	   �     �     �     &   0      7                  #                
          	   '   (   +                       6              3      1                   ,   2                  "       *          $      4   )               %      5                     !          /   .   -       % %1 is value, %2 is unit%1 %2 %1: Application Energy Consumption Battery Capacity Charge Percentage Charge state Charging Current Degree Celsius°C Details: %1 Discharging EMAIL OF TRANSLATORSYour emails Energy Energy Consumption Energy Consumption Statistics Environment Full design Fully charged Has power supply Kai Uwe Broulik Last 12 hours Last 2 hours Last 24 hours Last 48 hours Last 7 days Last full Last hour Manufacturer Model NAME OF TRANSLATORSYour names No Not charging PID: %1 Path: %1 Rechargeable Refresh Serial Number Shorthand for WattsW System Temperature This type of history is currently not available for this device. Timespan Timespan of data to display Vendor VoltV Voltage Wakeups per second: %1 (%2%) WattW Watt-hoursWh Yes current power draw from the battery in WConsumption literal percent sign% Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2017-04-09 10:12-0600
Last-Translator: A S Alam <aalam@users.sf.net>
Language-Team: Punjabi <kde-i18n-doc@kde.org>
Language: pa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 % %1 %2 %1: ਐਪਲੀਕੇਸ਼ਨ ਊਰਜਾ ਖ਼ਪਤ ਬੈਟਰੀ ਸਮਰੱਥਾ ਚਾਰਜ ਫੀਸਦੀ ਚਾਰਜ ਹਾਲਤ ਚਾਰਜ ਹੋ ਰਹੀ ਹੈ ਕਰੰਟ °C ਵੇਰਵੇ: %1 ਡਿਸਚਾਰਜ ਹੋ ਰਹੀ ਹੈ aalam@users.sf.net ਊਰਜਾ ਊਰਜਾ ਖ਼ਪਤ ਊਰਜਾ ਖ਼ਪਤ ਅੰਕੜੇ ਮਾਹੌਲ ਪੂਰਾ ਡਿਜ਼ਾਈਨ ਪੂਰੀ ਤਰ੍ਹਾਂ ਚਾਰਜ ਪਾਵਰ ਸਪਲਾਈ ਹੈ ਕੋਈ ਉਵੇ ਬਰੋਉਲਿਕ ਪਿਛਲੇ 12 ਘੰਟੇ ਪਿਛਲੇ 2 ਘੰਟੇ ਪਿਛਲੇ 24 ਘੰਟੇ ਪਿਛਲੇ 48 ਘੰਟੇ ਪਿਛਲੇ 7 ਦਿਨ ਆਖਰੀ ਪੂਰਾ ਪਿਛਲੇ ਘੰਟੇ ਨਿਰਮਾਤਾ ਮਾਡਲ ਅ ਸ ਆਲਮ ਨਹੀਂ ਚਾਰਜ ਨਹੀਂ ਹੋ ਰਹੀ PID: %1 ਪਾਥ: %1 ਮੁੜ-ਚਾਰਜ ਯੋਗ ਤਾਜ਼ਾ ਕਰੋ ਲੜੀ ਨੰਬਰ ਵਾ ਸਿਸਟਮ ਤਾਪਮਾਨ ਇਸ ਡਿਵਾਈਸ ਵਾਸਤੇ ਇਸ ਕਿਸਮ ਦਾ ਅਤੀਤ ਇਸ ਵੇਲੇ ਮੌਜੂਦ ਨਹੀਂ ਹੈ। ਸਮਾਂ-ਵਿਸਤਾਰ ਡਾਟਾ ਵਿਖਾਉਣ ਲਈ ਸਮਾਂ-ਵਿਸਤਾਰ ਵੇਂਡਰ ਵੋ ਵੋਲਟੇਜ਼ ਹਰੇਕ ਸਕਿੰਟ ਲਈ ਵੇਕ-ਅੱਪ: %1 (%2%) ਵਾ Wh ਹਾਂ ਖ਼ਪਤ % 