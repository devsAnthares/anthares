��    �     �  �  �,      <     	<     &<     9<     G<  !   f<     �<  #   �<     �<     �<     �<  
   =  7   =     J=     Q=  	   Y=     c=  &   =      �=     �=  (   �=  .   >  /   <>     l>  -   �>     �>     �>      �>     	?  0   )?     Z?     r?     �?     �?     �?     �?     �?  '   @      ?@  !   `@  "   �@  2   �@  ,   �@  "   A  *   (A     SA  7   eA  !   �A  '   �A  #   �A  0   B  .   <B  4   kB     �B     �B  %   �B  *   �B  ,   C     GC      fC  %   �C  -   �C     �C  #   �C  &   D  	   9D     CD     JD  	   ZD  2  dD     �F     �F     �F     �F  &   �F     �F     G  /   G     LG  -   TG  	   �G  %   �G  *   �G  5   �G  4   H     HH     MH     QH     ]H     jH     sH     �H     �H     �H     �H  &   �H     �H     �H     �H  	   I  
   $I  $   /I  %   TI  5   zI  4   �I      �I     J  +   J     AJ     VJ     lJ  	   �J     �J     �J     �J     �J     �J     
K     K     K     $K     0K  	   6K     @K     EK     XK     _K  	   tK     ~K     �K     �K     �K     �K     �K     �K  	   �K  
   L     L  )   L  ,   HL  )   uL     �L  /   �L  +   �L  :   M  ?   SM     �M  !   �M  9   �M  4   �M  #   1N  #   UN     yN     �N     �N     �N     �N  %   O      ,O      MO     nO     �O     �O  '   �O  '   �O  &   P      EP      fP     �P     �P     �P     �P     �P     �P     Q     Q     Q     )Q     ;Q  
   DQ     OQ     VQ     bQ      iQ     �Q  %   �Q     �Q     �Q     �Q  '   �Q  -   R  )   LR     vR  (   �R     �R     �R     �R     �R     �R     S  &   S     ;S  #   MS     qS  0   �S  *   �S  #   �S     T  J   #T  �  nT  k   AV  6  �V  �   �W  I  �X     �Y  b   Z  !   tZ  '   �Z  
   �Z     �Z  0   �Z  
   [     $[      3[  >   T[  2   �[  B   �[     	\     #\     <\     W\     p\      �\  .   �\  #   �\  0   ]  .   2]  5   a]  A   �]  '   �]  %   ^  '   '^  %   O^  )   u^     �^  #   �^  ;   �^  %   _     C_  -   a_     �_     �_     �_  '   �_  1   `  >   F`  6   �`  >   �`  5   �`  0   1a  !   ba     �a     �a     �a  6   �a  /   b  3   Jb  #   ~b  *   �b  (   �b  "   �b  *   c  '   Dc  0   lc  )   �c  #   �c  #   �c  #   d  /   3d  !   cd     �d  ;   �d  5   �d      e  (   5e  )   ^e  )   �e  +   �e  9   �e  (   f  )   Af     kf     f  8   �f     �f     �f     g  +   )g  J   Ug     �g     �g  $   �g     �g  "   h  &   &h  <   Mh  =   �h     �h  5   �h  ,   i  )   Bi  )   li  '   �i  +   �i  0   �i  ,   j  0   Hj  ,   yj  -   �j  "   �j      �j      k  +   9k  +   ek     �k     �k     �k     �k  !   �k     l     l     l     &l  
   3l     >l     Cl  
   Ll  L   Wl     �l     �l  #   �l  -   �l     m  �   %m     �m  <   �m  A   �m  
   @n  A   Kn     �n  1   �n     �n     �n  (   �n  )   o  0   =o  B   no     �o     �o     �o     �o     �o     �o     �o  
   p      p  5   7p     mp     �p     �p     �p  4   �p  0   �p  9   q  0   Rq  /   �q  4   �q     �q     �q     �q     �q     r  !   .r     Pr     Ur     qr  (   �r  !   �r  #   �r  =   �r     :s  )   Os     ys  0   }s     �s     �s     �s     �s     t  �   2t     u     *u     @u  
   ]u  (   hu     �u     �u     �u     �u     �u     �u     v     v     7v  	   :v  !   Dv     fv     rv     �v  $   �v  &   �v  !   �v     �v     �v  2   w     Cw  
   Uw     `w     yw     �w     �w  
   �w  N   �w  	   �w     	x     !x     (x  %   0x     Vx  $   _x     �x     �x     �x  Z   �x     y     y     (y     7y  +   >y  +   jy     �y     �y     �y  (   �y     �y     �y  '   �y     z  1   (z  4   Zz  	   �z     �z     �z     �z     �z  /   �z     {     {  
   {      ({  )   I{     s{     x{     �{  "   �{     �{     �{     �{  5   �{  9   |  K   N|  E   �|  >   �|  E   }  @   e}  E   �}  F   �}  G   3~  C   {~     �~  	   �~     �~     �~  ,   �~  '     $   ;  q   `  `   �  1   3�     e�  *   |�  ,   ��     Ԁ     �  "   �  5   0�     f�     w�     ��  S   ��     �  E   �  *   ]�     ��      ��     ��  %   ͂     �     �  +   ,�     X�     `�     ��     ��     ��     ��     ԃ     �  �   �  V  ��  �   ��  Q   ��  �   O�  �   @�  �  �    z�  n  ��  b   ��  �   a�  �   �  �   Ύ  >   ��  <   Ώ  }   �  ~   ��  }   �  �   ��     �     �     �     0�     8�     <�     J�  )   Y�  $   ��     ��     ��     Ӓ     �     ��     �     -�     F�     J�  !   a�  	   ��     ��     ��     ��     Ǔ     ד     ݓ     �     �     �     �  J   #�  J   n�  F   ��      �     �     �     #�     7�     J�     Z�     n�     t�     z�     ��     ��     ĕ  *   ȕ  (   �     �     3�     M�  �   j�  	   i�  
   s�     ~�     ��     ��     ��     ��  !   ��  &   ˗     �  O   ��  	   H�     R�     i�  
   �  %   ��     ��  %   ɘ      �     �  +   %�  
   Q�     \�     n�     z�     ��  �   ��  �   �  /   ��     �     ��     �  (   �     =�     M�  	   U�     _�     z�     ��     ��     ��  �   ��  &   ��  0   ܜ  4   �  !   B�  (   d�     ��     ��     Ý     ݝ     ��  %   �     1�     K�     f�     n�      ��     ��     ��     Ԟ  &   �  +   
�     6�     <�  V  I�     ��     ��  #   ��     ߠ  #   ��     "�     4�     J�     O�     j�     y�     ��     ��  $   ��     ��  	   ��  �   ¡  �   G�  $   �     1�     B�     a�     s�     ��  0   ��     ̣  .   ݣ  �   �  
  ��     ��  3   ��     �     �     �  �    �  U   ��  5   ��     0�     8�     A�     N�     c�  (   ��     ��     ��     ��  	   ӧ  ?   ݧ  :   �  "   X�  	   {�     ��  �  ��  D   >�  "   ��     ��  B   ��  ?   ��     ?�  H   T�  1   ��  C   ϫ  ,   �     @�  x   Y�     Ҭ     ٬  	   �  B   �  K   .�  ?   z�  C   ��  [   ��  ^   Z�  c   ��  %   �  j   C�  ,   ��  F   ۯ  O   "�  A   r�  Z   ��  9   �  C   I�  +   ��  :   ��  S   ��  B   H�  B   ��  W   β  X   &�  F   �  U   Ƴ  k   �  s   ��  G   ��  S   D�  +   ��  o   ĵ  D   4�  J   y�  F   Ķ  u   �  j   ��  w   �  *   d�  "   ��  D   ��  h   ��  v   `�  I   ׹  J   !�  G   l�  Q   ��  8   �  Y   ?�  L   ��     �     �  $   �     0�  .  B�     q�     ��     ��     ��  I   ��  C   	�  6   M�  �   ��     �  N   �  	   h�  O   r�  c   ��  k   &�  v   ��     	�     �     �  "   )�  "   L�  <   o�  "   ��     ��  &   ��  A   �  Z   I�     ��     ��  I   ��  	   �     �  a   ,�  :   ��  ]   ��  y   '�  E   ��     ��  k   �  )   r�  6   ��  J   ��     �  -   8�  &   f�  !   ��  0   ��  ?   ��      �     .�     ;�     K�     h�     n�     ��  (   ��     ��  6   ��     �     �  /   +�     [�  4   h�  9   ��  2   ��  2   
�     =�  :   Z�     ��  V   ��  \   �  \   c�  J   ��  �   �  v   ��  �   �  �   ��     /�  P   N�  g   ��  k   �  A   s�  T   ��  f   
�  Z   q�  =   ��  V   
�  A   a�  [   ��  F   ��  F   F�  E   ��  ]   ��  ]   1�  \   ��  \   ��  [   I�  F   ��  F   ��     3�  -   @�  ?   n�  $   ��  1   ��  H   �     N�     _�  "   c�     ��     ��  %   ��     ��     ��     ��  C   �     P�  {   m�  =   ��  G   '�     o�  i   ��  x   ��  f   l�     ��  T   ��  1   F�  %   x�     ��      ��  &   ��     ��  D   �  0   Q�  X   ��  K   ��  z   '�  Y   ��  O   ��     L�  �   j�  �  ��  �   ��    j�  �  ��  �  j�  (   �  �   6�  A   �  N   I�     ��  P   ��  b   ��     ^�     o�  B   ��  �   ��  9   h�  �   ��  "   3�     V�  !   v�     ��  #   ��  '   ��  \   �  M   a�  R   ��  h   �  �   k�  �   ��  i   ��  ^   ��  X   Q�  N   ��  W   ��  ?   Q�  T   ��  �   ��  c   s�  X   ��  r   0�  ]   ��  T   �  Z   V�  d   ��  e   �  E   |�  =   ��  E    �  R   F�  X   ��  I   ��  W   <�  D   ��  ;   ��  d   �  a   z�  u   ��  I   R�  I   ��  c   ��  Q   J�  U   ��  \   ��  p   O�  @   ��  E   �  G   G�  Q   ��  h   ��  9   J�  2   ��  �   ��  u   9  E   �  c   �  D   Y x   � o    �   � e     X   �    � 8   � x   4 2   � <   � =    _   [ �   �    < (   R j   { V   � b   = f   � �    �   � =   O	 �   �	 l   
 a   �
 H   �
 ]   3 W   � s   � a   ] p   � i   0 j   � H    Z   N b   � �    y   � #       + H   8 ?   � L   � 1       @    G "   Z    }    �    � 
   � �   �    �    � R   � |   $    � o  �    ! �   A �   � 
   � t   � 4   5 }   j    �    � q    r   � U   � t   N    �    � 	   �    � &    (   ) #   R    v [   � �   � -   �    �    � F   � [    b   u e   � q   > W   � r       { '   �    � +   � >   � X   . 	   � @   � E   � P     =   i  H   �  �   �  3   �! }   �!    G" i   a" Q   �" L   # ,   j# V   �# :   �#   )$ :   <& 4   w& U   �&    ' _   !'    �'    �' W   �' V   ( -   d( =   �( ;   �( Q   )    ^)    o) L   �)    �) =   �)    ,* f   0* i   �* a   +    c+ $   g+ j   �+ #   �+    , 7   6, A   n,    �,    �,    �, �   �,    �- ;   �-    .    . \   ).    �. W   �. 3   �.    "/    B/ �   W/    W0    k0    }0    �0 J   �0 J   �0 c   :1    �1    �1 ]   �1    2    +2 P   K2    �2 �   �2 �   =3    �3    �3 3   �3 8   4 C   N4 \   �4    �4    5    5 C   :5 ]   ~5    �5 &   �5    6 4   &6 ,   [6 &   �6    �6 �   �6 �   ]7 �   �7 �   �8 �   F9 �   �9 �   z: �   ; �   �; �   8< �   	=    �=    �= 	   �= &   �= r   #> I   �> Z   �> $  ;? �   `@ h   ^A -   �A S   �A p   IB G   �B K   C 9   NC t   �C -   �C =   +D <   iD �   �D    CE g   `E T   �E    F J   !F M   lF f   �F C   !G T   eG {   �G    6H E   FH ,   �H "   �H .   �H +   I -   7I    eI <  uI U  �J   M �   O �  �O F  �Q *  �R �  !V   �X �   �[ 5  �\ �   �] z  �^ g   ` i   {` �   �` �   ua �   tb   dc    }d    �d     �d    �d    �d    �d >   �d u   &e t   �e .   f    @f 9   `f     �f =   �f 5   �f =   /g    mg "   qg 1   �g    �g    �g    �g    �g    �g    �g    �g    h    -h    2h    7h �   Vh �   4i �   �i    �j    �j    �j    �j -   k -   @k    nk    �k    �k <   �k 2   �k -   l    @l \   Dl a   �l '   m    +m -   Jm   xm    �o 4   �o    �o !   �o    p    p 	   *p U   4p h   �p    �p �   q    �q <   r 5   Tr :   �r �   �r \   Is q   �s i   t L   �t �   �t    mu )   }u /   �u C   �u 	   v =  %v 4  cw `   �x    �x )   
y D   4y k   yy    �y    z    z 4   2z )   gz )   �z L   �z    { �  { K   �| E   �| E   4} B   z} P   �} C   ~ /   R~ )   �~ )   �~ $   �~ M   �~ 7   I >   �    �    � 2   �    	� ]   �    j� Y   �� �   ۀ    \� )   s� L  �� !   �    � b   � Z   |� U   ׄ :   -� =   h�    �� P   �� $   � .   3� 	   b�    l� y   y�    �     � �   $� �  � E   ��    �� O    �    P� &   n�    �� Z   ��    � `   %� �  �� I  � C   [� x   �� O   �    h�    �� �   �� �   �� �   ]�    � #   �    +� 2   J� :   }� F   ��    ��    � *   )�    T� \   n� W   ˔ A   #�    e� 	   }�      �     �  N   �   �    �       �  3       �         �   R           �  :  �  w  �   2  '   E  q  �      �   �    N  I  �      }  �  Y  g  �  M              �          W      �        �   	  �  /     I  D  �  ]       �  a  F  �  H  r   �            �   �  �         $  �          �  �   �  :        �  �   A  �  �  B      J   6   �  X              f  �        D   �  m   �  $    &  �      �          �   x  �      �  /      
      C   �  I   5       �     �   �  E      �  �   �  �  Y  =  �  g       �   n  �   T     �      �  �  u  �       t                    �   %      o  z      �   �   �  ]  �  )       >   c          ,      �  e          0           �           �  b   �          (  �      `  �      �   �  7   �   �  
         �     p   ^   �  !      �      �      h  �   e   �  �       �  �   _       �  �       5           h   �   �  4  m             �       �    c          z  ;  �     �      �  �   >     v   �       �  3          w     �  �        �       `             &   �  �   o    <   y       �  x      b  K  �  �   �          E      �   �      �       Z        �   i  �  A  �   �      �  5      �      �  k      C  �   v  L   #     9  �   �     �  	  �  �  �        �   �  M   �  �      �  �   �  r  m  �                     �       ?   �  a  6  �      �   �      k  �  '      (          h  �            �   _  �   �  +  �        �   .          6  V  �   �   K     �   �      0    �  �       !  �  �          �  �       Q  �     �   �  �   [  @   �  @  �   =   j  S    �   #  3  �  �          �  !   �       d  8        �      �   �          ^  �             �    �           �          W                  �  �  @  �   �  c   �       �  d  k         )          �      \    �         0              �  �  |                1  t   q  O  Z  �             �   �   s      T  �  �  �  �           �  p      %  �         "      �   l   �  F     �   <  /       	   �      �   �  �       \   �     U       �           F  �      �   |  �  �  �  �            d   s   N      ?      U      R      �   �  A   }   �  O    �  �   t  �   L      �   y     i      �  .        �  �       B  u   �   -       P   x     ;   n   7          [   W   �      �  f  �           D      �   ,          �        �  J  �          �   �  ;      �       �  �      �       e      �   1   =  :   Q          �  f      �  �         �  �   ?  >      �  �  �   �  "       �       �      G  �  b  \  �  �   X  �   n  �  -  �   �  S      �   '  l  �  2       ^      �  �  �      V  j   �   ~  �          �   s  �      {   &  �        �           �    �  Z   *  9   �  �    *  �      �   �   �       ~  V           �     �   �  �              �      �   �      �        �    "  �   �   R  z       $       _  o   G       �   H   �      #  �       �      �  L          �      1      8   �        y  w       �  )  [  �  �   B       �          �   P                ~   v  �              �  r             �  �   �   �  |   ]  �          j  �          {        G  �          �  �          �       ,       �  �  �   *       Q       J      <            2          �  `  
   �   q   M  �         �  �  i   9  K  �  �  %   �  +  p  �   }  �   -  �      �   7  �         �      S       l          .  u      �      C      �  �  �  �  4   a   �   {  O   U  �                 �  g  Y   X   H  T    �          8  �   �   (   �  +   P      �  4  �  �  �    # Created by NetworkManager
 # Merged from %s

 %d (disabled) %d (enabled, prefer public IP) %d (enabled, prefer temporary IP) %d (unknown) %d. IPv4 address has invalid prefix %d. IPv4 address is invalid %d. route has invalid prefix %d. route is invalid %s Network %s.  Please use --help to see a list of valid options.
 %u MHz %u Mb/s %u Mbit/s '%d' is not a valid channel '%d' is out of valid range <128-16384> '%d' value is out of range <0-3> '%ld' is not a valid channel '%s' can only be used with '%s=%s' (WEP) '%s' connections require '%s' in this property '%s' contains invalid char(s) (use [A-Za-z._-]) '%s' is ambiguous (%s x %s) '%s' is neither an UUID nor an interface name '%s' is not a number '%s' is not a valid DCB flag '%s' is not a valid Ethernet MAC '%s' is not a valid IBoIP P_Key '%s' is not a valid IPv4 address for '%s' option '%s' is not a valid MAC '%s' is not a valid MAC address '%s' is not a valid PSK '%s' is not a valid UUID '%s' is not a valid Wi-Fi mode '%s' is not a valid band '%s' is not a valid channel '%s' is not a valid channel; use <1-13> '%s' is not a valid duplex value '%s' is not a valid hex character '%s' is not a valid interface name '%s' is not a valid interface name for '%s' option '%s' is not a valid number (or out of range) '%s' is not a valid value for '%s' '%s' is not a valid value for the property '%s' is not valid '%s' is not valid master; use ifname or connection UUID '%s' is not valid; use 0, 1, or 2 '%s' is not valid; use <option>=<value> '%s' is not valid; use [%s] or [%s] '%s' length is invalid (should be 5 or 6 digits) '%s' not a number between 0 and %u (inclusive) '%s' not a number between 0 and %u (inclusive) or %u '%s' not among [%s] '%s' option is empty '%s' option is only valid for '%s=%s' '%s' option requires '%s' option to be set '%s' security requires '%s' setting presence '%s' security requires '%s=%s' '%s' value doesn't match '%s=%s' '%s=%s' is incompatible with '%s > 0' '%s=%s' is not a valid configuration for '%s' (No custom routes) (No support for dynamic-wep yet...) (No support for wpa-enterprise yet...) (default) (none) (unknown error) (unknown) ---[ Property menu ]---
set      [<value>]               :: set new value
add      [<value>]               :: add new option to the property
change                           :: change current value
remove   [<index> | <option>]    :: delete the value
describe                         :: describe property
print    [setting | connection]  :: print property (setting/connection) value(s)
back                             :: go to upper level
help/?   [<command>]             :: print this help or command description
quit                             :: exit nmcli
 0 (NONE) 0 (disabled) 0 (none) 802.1X 802.1X supplicant configuration failed 802.1X supplicant disconnected 802.1X supplicant failed 802.1X supplicant took too long to authenticate 802.3ad ===| nmcli interactive connection editor |=== A (5 GHz) A dependency of the connection failed A password is required to connect to '%s'. A problem with the RFC 2684 Ethernet over ADSL bridge A secondary connection of the base connection failed ADSL ARP ARP targets Access Point Activate Activate a connection Active Backup Ad-Hoc Ad-Hoc Network Adaptive Load Balancing (alb) Adaptive Transmit Load Balancing (tlb) Add Add... Adding a new '%s' connection Addresses Aging time Allow control of network connections Allowed values for '%s' property: %s
 An http(s) address for checking internet connectivity Are you sure you want to delete the connection '%s'? Ask for this password every time Authentication Authentication required by wireless network AutoIP service error AutoIP service failed AutoIP service failed to start Automatic Automatic (DHCP-only) Automatically connect Available properties: %s
 Available settings: %s
 Available to all users B/G (2.4 GHz) BOND BRIDGE BRIDGE PORT BSSID Bluetooth Bond Bond connection %d Bridge Bridge connection %d Broadcast Cancel Carrier/link changed Channel Cloned MAC address Closing %s failed: %s
 Config directory location Config file location Connected Connecting Connecting... Connection '%s' (%s) successfully added.
 Connection '%s' (%s) successfully modified.
 Connection '%s' (%s) successfully saved.
 Connection is already active Connection sharing via a protected WiFi network Connection sharing via an open WiFi network Connection successfully activated (D-Bus active path: %s)
 Connection with UUID '%s' created and activated on device '%s'
 Connectivity Could not activate connection: %s Could not create editor for connection '%s' of type '%s'. Could not create editor for invalid connection '%s'. Could not create temporary file: %s Could not daemonize: %s [error %u]
 Could not decode private key. Could not generate random data. Could not load file '%s'
 Could not parse arguments Could not re-read file: %s Couldn't convert password to UCS2: %d Couldn't decode PKCS#12 file: %d Couldn't decode PKCS#12 file: %s Couldn't decode PKCS#8 file: %s Couldn't decode certificate: %d Couldn't decode certificate: %s Couldn't initialize PKCS#12 decoder: %d Couldn't initialize PKCS#12 decoder: %s Couldn't initialize PKCS#8 decoder: %s Couldn't verify PKCS#12 file: %d Couldn't verify PKCS#12 file: %s Create Current nmcli configuration:
 DCB or FCoE setup failed DHCP client error DHCP client failed DHCP client failed to start DNS servers DSL DSL authentication DSL connection %d Datagram Deactivate Delete Destination Device Device '%s' has been connected.
 Device details Device disconnected by user or client Device is now managed Device is now unmanaged Disabled Do you also want to clear '%s'? [yes]:  Do you also want to set '%s' to '%s'? [yes]:  Doesn't look like a PEM private key file. Don't become a daemon Don't become a daemon, and log to stderr Don't print anything Dynamic WEP (802.1x) ETHERNET Edit '%s' value:  Edit a connection Edit... Editing existing '%s' connection: '%s' Editor failed: %s Enable STP (Spanning Tree Protocol) Enable or disable WiFi devices Enable or disable WiMAX mobile broadband devices Enable or disable mobile broadband devices Enable or disable system networking Enter '%s' value:  Enter a list of IPv4 addresses of DNS servers.

Example: 8.8.8.8, 8.8.4.4
 Enter a list of IPv6 addresses of DNS servers.  If the IPv6 configuration method is 'auto' these DNS servers are appended to those (if any) returned by automatic configuration.  DNS servers cannot be used with the 'shared' or 'link-local' IPv6 configuration methods, as there is no upstream network. In all other IPv6 configuration methods, these DNS servers are used as the only DNS servers for this connection.

Example: 2607:f0d0:1002:51::4, 2607:f0d0:1002:51::1
 Enter a list of S/390 options formatted as:
  option = <value>, option = <value>,...
Valid options are: %s
 Enter a list of bonding options formatted as:
  option = <value>, option = <value>,... 
Valid options are: %s
'mode' can be provided as a name or a number:
balance-rr    = 0
active-backup = 1
balance-xor   = 2
broadcast     = 3
802.3ad       = 4
balance-tlb   = 5
balance-alb   = 6

Example: mode=2,miimon=120
 Enter a list of user permissions. This is a list of user names formatted as:
  [user:]<user name 1>, [user:]<user name 2>,...
The items can be separated by commas or spaces.

Example: alice bob charlie
 Enter bytes as a list of hexadecimal values.
Two formats are accepted:
(a) a string of hexadecimal digits, where each two digits represent one byte
(b) space-separated list of bytes written as hexadecimal digits (with optional 0x/0X prefix, and optional leading 0).

Examples: ab0455a6ea3a74C2
          ab 4 55 0xa6 ea 3a 74 C2
 Enter connection type:  Enter the type of WEP keys. The accepted values are: 0 or unknown, 1 or key, and 2 or passphrase.
 Error in configuration file: %s.
 Error initializing certificate data: %s Error: %s
 Error: %s argument is missing. Error: %s properties, nor it is a setting name.
 Error: %s. Error: %s: %s. Error: '%s' argument is missing. Error: '%s' is not a valid monitoring mode; use '%s' or '%s'.
 Error: '%s' is not valid argument for '%s' option. Error: '--fields' value '%s' is not valid here (allowed field: %s) Error: 'autoconnect': %s. Error: 'device show': %s Error: 'device status': %s Error: 'device wifi': %s Error: 'general logging': %s Error: 'general permissions': %s Error: 'networking' command '%s' is not valid. Error: 'type' argument is required. Error: <setting>.<property> argument is missing. Error: Access point with bssid '%s' not found. Error: Argument '%s' was expected, but '%s' provided. Error: BSSID to connect to (%s) differs from bssid argument (%s). Error: Cannot activate connection: %s.
 Error: Connection activation failed.
 Error: Connection activation failed: %s Error: Connection deletion failed: %s Error: Device '%s' is not a Wi-Fi device. Error: Device '%s' not found. Error: Device activation failed: %s Error: Failed to add/activate new connection: Unknown error Error: NetworkManager is not running. Error: No Wi-Fi device found. Error: No access point with BSSID '%s' found. Error: No arguments provided. Error: No connection specified. Error: No interface specified. Error: No network with SSID '%s' found. Error: Option '%s' is unknown, try 'nmcli -help'. Error: Option '--pretty' is mutually exclusive with '--terse'. Error: Option '--pretty' is specified the second time. Error: Option '--terse' is mutually exclusive with '--pretty'. Error: Option '--terse' is specified the second time. Error: Parameter '%s' is neither SSID nor BSSID. Error: SSID or BSSID are missing. Error: Timeout %d sec expired. Error: Unexpected argument '%s' Error: Unknown connection '%s'. Error: bssid argument value '%s' is not a valid BSSID. Error: cannot delete unknown connection(s): %s. Error: connection is not saved. Type 'save' first.
 Error: connection is not valid: %s
 Error: connection verification failed: %s
 Error: extra argument not allowed: '%s'. Error: failed to modify %s.%s: %s. Error: failed to remove value of '%s': %s
 Error: failed to set '%s' property: %s
 Error: invalid '%s' argument: '%s' (use on/off). Error: invalid <setting>.<property> '%s'. Error: invalid connection type; %s
 Error: invalid connection type; %s. Error: invalid extra argument '%s'. Error: invalid or not allowed setting '%s': %s. Error: invalid property '%s': %s. Error: invalid property: %s
 Error: invalid property: %s, neither a valid setting name.
 Error: invalid setting argument '%s'; valid are [%s]
 Error: invalid setting name; %s
 Error: missing argument for '%s' option. Error: missing setting for '%s' property
 Error: no argument given; valid are [%s]
 Error: no setting selected; valid are [%s]
 Error: only one of 'id', uuid, or 'path' can be provided. Error: only these fields are allowed: %s Error: openconnect failed with status %d
 Error: property %s
 Error: save-confirmation: %s
 Error: setting '%s' is mandatory and cannot be removed.
 Error: status-line: %s
 Error: unknown setting '%s'
 Error: unknown setting: '%s'
 Error: value for '%s' argument is required. Error: wep-key-type argument value '%s' is invalid, use 'key' or 'phrase'. Ethernet Ethernet connection %d Failed to decode PKCS#8 private key. Failed to decode certificate. Failed to decrypt the private key. Failed to decrypt the private key: %d. Failed to decrypt the private key: decrypted data too large. Failed to decrypt the private key: unexpected padding length. Failed to encrypt: %d. Failed to finalize decryption of the private key: %d. Failed to find expected PKCS#8 end tag '%s'. Failed to find expected PKCS#8 start tag. Failed to initialize the MD5 context: %d. Failed to initialize the crypto engine. Failed to initialize the crypto engine: %d. Failed to initialize the decryption cipher slot. Failed to initialize the decryption context. Failed to initialize the encryption cipher slot. Failed to initialize the encryption context. Failed to register with the requested network Failed to select the specified APN Failed to set IV for decryption. Failed to set IV for encryption. Failed to set symmetric key for decryption. Failed to set symmetric key for encryption. Forward delay GROUP GSM Modem's SIM PIN required GSM Modem's SIM PUK required GSM Modem's SIM card not inserted GSM Modem's SIM wrong GVRP,  Gateway Hairpin mode Hello time Hide Hostname INFINIBAND IP configuration could not be reserved (no available address, timeout, etc.) IPv4 CONFIGURATION IPv6 CONFIGURATION IV contains non-hexadecimal digits. IV must be an even number of bytes in length. Identity If you are creating a VPN, and the VPN connection you wish to create does not appear in the list, you may not have the correct VPN plugin installed. Ignore Ignoring unrecognized log domain(s) '%s' from config files.
 Ignoring unrecognized log domain(s) '%s' passed on command line.
 InfiniBand InfiniBand P_Key connection did not specify parent interface name InfiniBand connection %d InfiniBand device does not support connected mode Infra Interface:  Invalid IV length (must be at least %d). Invalid IV length (must be at least %zd). Invalid configuration option '%s'; allowed [%s]
 Invalid option.  Please use --help to see a list of valid options. JSON configuration Key LEAP LOOSE_BINDING,  Link down delay Link monitoring Link up delay Link-Local List of plugins separated by ',' Log domains separated by ',': any combination of [%s] Log level: one of [%s] MII (recommended) MTU Make all warnings fatal Malformed PEM file: DEK-Info was not the second tag. Malformed PEM file: Proc-Type was not first tag. Malformed PEM file: invalid format of IV in DEK-Info tag. Malformed PEM file: no IV found in DEK-Info tag. Malformed PEM file: unknown Proc-Type tag '%s'. Malformed PEM file: unknown private key cipher '%s'. Manual Max age Metric Mobile Broadband Mobile broadband connection %d Mobile broadband network password Mode Modem initialization failed ModemManager is unavailable Modify network connections for all users Modify persistent system hostname Modify personal network connections Monitoring connection activation (press any key to continue)
 Monitoring frequency Must specify a P_Key if specifying parent N/A Necessary firmware for the device may be missing Network registration denied Network registration timed out NetworkManager TUI NetworkManager is not running. NetworkManager logging NetworkManager monitors all network connections and automatically
chooses the best connection to use.  It also allows the user to
specify wireless access points which wireless cards in the computer
should associate with. NetworkManager permissions NetworkManager status NetworkManager went to sleep Networking Never use this network for default route New Connection Next Hop No carrier could be established No custom routes are defined. No dial tone No reason given No such connection '%s' Not searching for networks OK OLPC Mesh One custom route %d custom routes Open System Opening %s failed: %s
 PCI PEM certificate had no end tag '%s'. PEM certificate had no start tag '%s'. PEM key file had no end tag '%s'. PIN PIN check failed PIN code is needed for the mobile broadband device PIN code required PPP failed PPP service disconnected PPP service failed to start Parent Password Password:  Passwords or encryption keys are required to access the wireless network '%s'. Path cost Please select an option Prefix Primary Print NetworkManager version and exit Priority Private key cipher '%s' was unknown. Private key password Profile name Property name?  Put NetworkManager to sleep or wake it up (should only be used by system power management) Quit REORDER_HEADERS,  Radio switches Remove Require IPv4 addressing for this connection Require IPv6 addressing for this connection Round-robin Routing SSID SSID length is out of range <1-32> bytes SSID or BSSID:  Search domains Secrets were required, but not provided Security Select the type of connection you wish to create. Select the type of slave connection you wish to add. Select... Service Set Hostname Set hostname to '%s' Set system hostname Setting '%s' is not present in the connection.
 Setting name?  Shared Shared Key Shared connection service failed Shared connection service failed to start Show Show password Slaves Specify the location of a PID file State file location Status of devices Success System policy prevents control of network connections System policy prevents enabling or disabling WiFi devices System policy prevents enabling or disabling WiMAX mobile broadband devices System policy prevents enabling or disabling mobile broadband devices System policy prevents enabling or disabling system networking System policy prevents modification of network settings for all users System policy prevents modification of personal network settings System policy prevents modification of the persistent system hostname System policy prevents putting NetworkManager to sleep or waking it up System policy prevents sharing connections via a protected WiFi network System policy prevents sharing connections via an open WiFi network TEAM TEAM PORT Team Team connection %d The Bluetooth connection failed or timed out The IP configuration is no longer valid The Wi-Fi network could not be found The connection profile has been removed from another client. You may type 'save' in the main menu to restore it.
 The connection profile has been removed from another client. You may type 'save' to restore it.
 The device could not be readied for configuration The device was removed The device's active connection disappeared The device's existing connection was assumed The dialing attempt failed The dialing request timed out The expected start of the response The interval between connectivity checks (in seconds) The line is busy The modem could not be found The supplicant is now available Time to wait for a connection, in seconds (without the option, default value is 30) Transport mode Type 'describe [<setting>.<prop>]' for detailed property description. Type 'help' or '?' for available commands. USB Unable to add new connection: %s Unable to delete connection: %s Unable to determine private key type. Unable to save connection: %s Unable to set hostname: %s Unexpected amount of data after encrypting. Unknown Unknown command argument: '%s'
 Unknown command: '%s'
 Unknown error Unknown log domain '%s' Unknown log level '%s' Unknown parameter: %s
 Usage Usage: nmcli connection delete { ARGUMENTS | help }

ARGUMENTS := [id | uuid | path] <ID>

Delete a connection profile.
The profile is identified by its name, UUID or D-Bus path.

 Usage: nmcli connection edit { ARGUMENTS | help }

ARGUMENTS := [id | uuid | path] <ID>

Edit an existing connection profile in an interactive editor.
The profile is identified by its name, UUID or D-Bus path

ARGUMENTS := [type <new connection type>] [con-name <new connection name>]

Add a new connection profile in an interactive editor.

 Usage: nmcli connection load { ARGUMENTS | help }

ARGUMENTS := <filename> [<filename>...]

Load/reload one or more connection files from disk. Use this after manually
editing a connection file to ensure that NetworkManager is aware of its latest
state.

 Usage: nmcli connection reload { help }

Reload all connection files from disk.

 Usage: nmcli device connect { ARGUMENTS | help }

ARGUMENTS := <ifname>

Connect the device.
NetworkManager will try to find a suitable connection that will be activated.
It will also consider connections that are not set to auto-connect.

 Usage: nmcli device show { ARGUMENTS | help }

ARGUMENTS := [<ifname>]

Show details of device(s).
The command lists details for all devices, or for a given device.

 Usage: nmcli device status { help }

Show status for all devices.
By default, the following columns are shown:
 DEVICE     - interface name
 TYPE       - device type
 STATE      - device state
 CONNECTION - connection activated on device (if any)
Displayed columns can be changed using '--fields' global option. 'status' is
the default command, which means 'nmcli device' calls 'nmcli device status'.

 Usage: nmcli general hostname { ARGUMENTS | help }

ARGUMENTS := [<hostname>]

Get or change persistent system hostname.
With no arguments, this prints currently configured hostname. When you pass
a hostname, NetworkManager will set it as the new persistent system hostname.

 Usage: nmcli general logging { ARGUMENTS | help }

ARGUMENTS := [level <log level>] [domains <log domains>]

Get or change NetworkManager logging level and domains.
Without any argument current logging level and domains are shown. In order to
change logging state, provide level and/or domain. Please refer to the man page
for the list of possible logging domains.

 Usage: nmcli general permissions { help }

Show caller permissions for authenticated operations.

 Usage: nmcli general status { help }

Show overall status of NetworkManager.
'status' is the default action, which means 'nmcli gen' calls 'nmcli gen status'

 Usage: nmcli general { COMMAND | help }

COMMAND := { status | hostname | permissions | logging }

  status

  hostname [<hostname>]

  permissions

  logging [level <log level>] [domains <log domains>]

 Usage: nmcli networking connectivity { ARGUMENTS | help }

ARGUMENTS := [check]

Get network connectivity state.
The optional 'check' argument makes NetworkManager re-check the connectivity.

 Usage: nmcli networking off { help }

Switch networking off.

 Usage: nmcli networking on { help }

Switch networking on.

 Usage: nmcli networking { COMMAND | help }

COMMAND := { [ on | off | connectivity ] }

  on

  off

  connectivity [check]

 Usage: nmcli radio all { ARGUMENTS | help }

ARGUMENTS := [on | off]

Get status of all radio switches, or turn them on/off.

 Usage: nmcli radio wifi { ARGUMENTS | help }

ARGUMENTS := [on | off]

Get status of Wi-Fi radio switch, or turn it on/off.

 Usage: nmcli radio wwan { ARGUMENTS | help }

ARGUMENTS := [on | off]

Get status of mobile broadband radio switch, or turn it on/off.

 Username VLAN VLAN connection %d VLAN id VPN VPN connected VPN connecting VPN connecting (getting IP configuration) VPN connecting (need authentication) VPN connecting (prepare) VPN connection %d VPN connection failed VPN disconnected Valid connection types: %s
 Verify connection: %s
 Verify setting '%s': %s
 WEP WEP 128-bit Passphrase WEP 40/128-bit Key (Hex or ASCII) WEP index WEP key index1 (Default) WEP key index2 WEP key index3 WEP key index4 WI-FI WPA & WPA2 Enterprise WPA & WPA2 Personal WPA1 WPA2 WWAN radio switch Waits for NetworkManager to finish activating startup network connections. Warning: editing existing connection '%s'; 'con-name' argument is ignored
 Warning: editing existing connection '%s'; 'type' argument is ignored
 Wi-Fi Wi-FiAutomatic Wi-FiClient Wi-Fi connection %d Wi-Fi radio switch Wi-Fi scan list Wi-Fi securityNone WiMAX Wired Wired 802.1X authentication Wired connection %d Writing to %s failed: %s
 XOR You may edit the following properties: %s
 You may edit the following settings: %s
 ['%s' setting values]
 [NM property description] [nmcli specific description] activate [<ifname>] [/<ap>|<nsp>]  :: activate the connection

Activates the connection.

Available options:
<ifname>    - device the connection will be activated on
/<ap>|<nsp> - AP (Wi-Fi) or NSP (WiMAX) (prepend with / when <ifname> is not specified)
 activated activating advertise,  agent-owned,  asleep auth auto back  :: go to upper menu level

 bandwidth percentages must total 100%% bytes change  :: change current value

Displays current value and allows editing it.
 connected connected (local only) connected (site only) connecting connecting (checking IP connectivity) connecting (configuring) connecting (getting IP configuration) connecting (need authentication) connecting (prepare) connecting (starting secondary connections) connection connection failed deactivated deactivating default describe  :: describe property

Shows property description. You can consult nm-settings(5) manual page to see all NM settings and properties.
 describe [<setting>.<prop>]  :: describe property

Shows property description. You can consult nm-settings(5) manual page to see all NM settings and properties.
 device '%s' not compatible with connection '%s' disabled disconnected disconnecting don't know how to get the property value element invalid enabled enabled,  field '%s' has to be alone flags are invalid flags invalid flags invalid - disabled full goto <setting>[.<prop>] | <prop>  :: enter setting/property for editing

This command enters into a setting or property for editing it.

Examples: nmcli> goto connection
          nmcli connection> goto secondaries
          nmcli> goto ipv4.addresses
 has to match '%s' property for PKCS#12 help/? [<command>]  :: help for nmcli commands

 help/? [<command>]  :: help for the nmcli commands

 index '%d' is not in range <0-%d> index '%d' is not in the range of <0-%d> invalid '%s' or its value '%s' invalid IP address: %s invalid IPv4 address '%s' invalid IPv6 address '%s' invalid option '%s' invalid option '%s' or its value '%s' invalid priority map '%s' is not a valid MAC address limited long device name%s %s mandatory option '%s' is missing millisecondsms missing name, try one of [%s] missing option must contain 8 comma-separated numbers neither a valid connection nor device given never new hostname nmcli can accepts both direct JSON configuration data and a file name containing the configuration. In the latter case the file is read and the contents is put into this property.

Examples: set team.config { "device": "team0", "runner": {"name": "roundrobin"}, "ports": {"eth1": {}, "eth2": {}} }
          set team.config /etc/my-team.conf
 nmcli tool, version %s
 no no active connection on device '%s' no active connection or device no device found for connection '%s' no item to remove no priority to remove none not a valid interface name not required,  not saved,  off on only one of '%s' and '%s' can be set portal preparing print [all]  :: print setting or connection values

Shows current property or the whole connection.

Example: nmcli ipv4> print all
 print [property|setting|connection]  :: print property (setting, connection) value(s)

Shows property value. Providing an argument you can also display values for the whole setting or connection.
 priority '%s' is not valid (<0-%ld>) property invalid property invalid (not enabled) property is empty property is invalid property is missing property is not specified and neither is '%s:%s' property missing property value '%s' is empty or too long (>64) quit  :: exit nmcli

This command exits nmcli. When the connection being edited is not saved, the user is asked to confirm the action.
 remove <setting>[.<prop>]  :: remove setting or reset property value

This command removes an entire setting from the connection, or if a property
is given, resets that property to the default value.

Examples: nmcli> remove wifi-sec
          nmcli> remove eth.mtu
 requires '%s' or '%s' setting requires presence of '%s' setting in the connection requires setting '%s' property running seconds set [<setting>.<prop> <value>]  :: set property value

This command sets property value.

Example: nmcli> set con.id My connection
 set [<value>]  :: set new value

This command sets provided <value> to this property
 setting this property requires non-zero '%s' property started starting sum not 100% teamd control failed the property can't be changed this property is not allowed for '%s=%s' unavailable unknown unknown device '%s'. unmanaged use 'goto <setting>' first, or 'describe <setting>.<property>'
 use 'goto <setting>' first, or 'set <setting>.<property>'
 value '%d' is out of range <%d-%d> willing,  yes Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-11-10 15:29+0100
PO-Revision-Date: 2017-04-21 06:13-0400
Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>
Language-Team: Punjabi/Panjabi <punjabi-users@lists.sf.net>
Language: pa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Zanata 3.9.6
Plural-Forms: nplurals=2; plural=(n != 1);
 #ਨੈੱਟਵਰਕ ਮੈਨੇਜਰ ਵਲੋਂ ਬਣਾਈ
 #%s ਤੋਂ ਮਿਲਾਇਆ

 %d (ਬੰਦ ਹੈ) %d (ਚਾਲੂ ਹੈ, ਪਸੰਦੀਦਾ ਪਬਲਿਕ IP) %d (ਚਾਲੂ ਹੈ, ਪਸੰਦੀਦਾ ਆਰਜ਼ੀ IP) %d (ਅਣਜਾਣ) %d। IPv4 ਪਤੇ ਦਾ ਅਗੇਤਰ ਅਢੁਕਵਾਂ ਹੈ %d। IPv4 ਪਤਾ ਅਢੁਕਵਾਂ ਹੈ %d। ਰਾਹ ਦਾ ਅਗੇਤਰ ਅਢੁਕਵਾਂ ਹੈ %d। ਰਾਹ ਅਢੁਕਵਾਂ ਹੈ %s ਨੈੱਟਵਰਕ %s। ਢੁੱਕਵੀਆਂ ਚੋਣਾਂ ਦੀ ਲਿਸਟ ਵੇਖਣ ਲਈ --help ਵਰਤੋਂ ਜੀ।
 %u MHz %u Mb/s %u Mbit/s '%d' ਇੱਕ ਢੁਕਵਾਂ ਚੈਨਲ ਨਹੀਂ ਹੈ '%d' ਢੁਕਵੀਂ ਹੱਦ <128-16384> ਤੋਂ ਬਾਹਰ ਹੈ '%d' ਮੁੱਲ ਹੱਦ <0-3> ਤੋਂ ਬਾਹਰ ਹੈ '%ld' ਇੱਕ ਢੁਕਵਾਂ ਚੈਨਲ ਨਹੀਂ ਹੈ '%s' ਸਿਰਫ '%s=%s' (WEP) ਨਾਲ ਹੀ ਵਰਤਿਆ ਜਾ ਸਕਦਾ ਹੈ '%s' ਸੰਪਰਕ ਨੂੰ ਇਸ ਗੁਣ ਵਿੱਚ '%s' ਲੋੜੀਂਦਾ ਹੈ '%s' ਵਿੱਚ ਅਢੁਕਵੇਂ ਅੱਖਰ ਸਮਾਏ ਹਨ ([A-Za-z._-] ਵਰਤੋ) '%s' ਅਸਪਸ਼ਟ ਹੈ (%s x %s) '%s' ਨਾ ਤਾਂ ਇੱਕ UUID ਹੈ ਨਾ ਹੀ ਇੱਕ ਇੰਟਰਫੇਸ ਨਾਂ ਹੈ '%s' ਇੱਕ ਅੰਕ ਨਹੀਂ ਹੈ '%s' ਇੱਕ ਢੁਕਵਾਂ DCB ਫਲੈਗ ਨਹੀਂ ਹੈ '%s' ਇੱਕ ਢੁਕਵਾਂ ਈਥਰਨੈੱਟ MAC ਨਹੀਂ ਹੈ '%s' ਇੱਕ ਢੁਕਵੀਂ IBoIP P_Key ਨਹੀਂ ਹੈ '%s' ਇੱਕ ਢੁਕਵਾਂ IPv4 ਪਤਾ ਨਹੀਂ ਹੈ '%s' ਚੋਣ ਲਈ '%s' ਇੱਕ ਢੁਕਵਾਂ MAC ਨਹੀਂ ਹੈ '%s' ਇੱਕ ਢੁਕਵਾਂ MAC ਪਤਾ ਨਹੀਂ ਹੈ '%s' ਢੁੱਕਵਾਂ PSK ਨਹੀਂ '%s' ਇੱਕ ਢੁਕਵਾਂ UUID ਨਹੀਂ ਹੈ '%s' ਇੱਕ ਢੁਕਵਾਂ ਵਾਈ-ਫਾਈ ਮੋਡ ਨਹੀਂ ਹੈ '%s' ਇੱਕ ਢੁਕਵਾਂ ਬੈਂਡ ਨਹੀਂ ਹੈ '%s' ਇੱਕ ਢੁਕਵਾਂ ਚੈਨਲ ਨਹੀਂ ਹੈ '%s' ਇੱਕ ਢੁਕਵਾਂ ਚੈਨਲ ਨਹੀਂ ਹੈ; <1-13> ਵਰਤੋ '%s' ਇੱਕ ਢੁਕਵਾਂ ਡੁਪਲੈਕਸ ਮੁੱਲ ਨਹੀਂ ਹੈ '%s' ਇੱਕ ਢੁਕਵਾਂ hex ਅੱਖਰ ਨਹੀਂ ਹੈ '%s' ਇੱਕ ਢੁਕਵਾਂ ਇੰਟਰਫੇਸ ਨਾਂ ਨਹੀਂ ਹੈ '%s' ਇੱਕ ਢੁਕਵਾਂ ਇੰਟਰਫੇਸ ਨਾਂ ਨਹੀਂ ਹੈ '%s' ਚੋਣ ਲਈ '%s' ਇੱਕ ਢੁਕਵਾਂ ਅੰਕ ਨਹੀਂ ਹੈ (ਜਾਂ ਹੱਦ ਤੋਂ ਬਾਹਰ ਹੈ) '%s' ਇੱਕ ਢੁਕਵਾਂ ਮੁੱਲ ਨਹੀਂ '%s' ਲਈ ਗੁਣ ਲਈ '%s' ਇੱਕ ਢੁਕਵਾਂ ਮੁੱਲ ਨਹੀਂ ਹੈ '%s' ਢੁਕਵਾਂ ਨਹੀਂ ਹੈ '%s' ਢੁਕਵਾਂ ਮਾਸਟਰ ਨਹੀਂ ਹੈ; ifname ਜਾਂ ਸੰਪਰਕ UUID ਵਰਤੋ '%s' ਢੁਕਵਾਂ ਨਹੀਂ; 0, 1, ਜਾਂ 2 ਵਰਤੋ '%s' ਢੁਕਵਾਂ ਨਹੀਂ ਹੈ; <option>=<value> ਵਰਤੋ '%s' ਢੁਕਵਾਂ ਨਹੀਂ; [%s] ਜਾਂ [%s] ਵਰਤੋ '%s' ਲੰਬਾਈ ਅਢੁਕਵੀਂ ਹੈ (5 ਜਾਂ 6 ਅੱਖਰ ਹੋਣੇ ਚਾਹੀਦੇ ਹਨ) '%s' ਕੋਈ 0 ਅਤੇ %u (ਸਮੇਤ) ਦੇ ਵਿਚਕਾਰਲਾ ਅੰਕ ਨਹੀਂ ਹੈ '%s' ਕੋਈ 0 ਅਤੇ %u (ਸਮੇਤ) ਜਾਂ %u ਦੇ ਵਿਚਕਾਰਲਾ ਅੰਕ ਨਹੀਂ ਹੈ '%s' [%s] ਵਿੱਚ ਨਹੀਂ ਹੈ '%s' ਚੋਣ ਖਾਲੀ ਹੈ '%s' ਚੋਣ ਸਿਰਫ '%s=%s' ਲਈ ਢੁਕਵੀਂ ਹੈ '%s' ਚੋਣ ਲਈ '%s' ਚੋਣ ਸੈੱਟ ਕੀਤੀ ਜਾਣੀ ਲੋੜੀਂਦੀ ਹੈ '%s' ਸੁਰੱਖਿਆ ਨੂੰ '%s' ਸੈਟਿੰਗ ਦੀ ਮੌਜੂਦਗੀ ਲੋੜੀਂਦੀ ਹੈ '%s' ਸੁਰੱਖਿਆ ਨੂੰ '%s=%s' ਲੋੜੀਂਦਾ ਹੈ '%s' ਮੁੱਲ '%s=%s' ਨਾਲ ਮੇਲ ਨਹੀਂ ਖਾਂਦਾ '%s=%s' ਅਨੁਕੂਲਿਤ ਨਹੀਂ ਹੈ '%s > 0' ਨਾਲ '%s=%s' ਇੱਕ ਜਾਇਜ ਸੰਰਚਨਾ ਨਹੀਂ ਹੈ '%s' ਲਈ (ਕੋਈ ਚੁਣਿੰਦਾ ਰਾਹ ਨਹੀਂ) (ਡਾਇਨਾਮਿਕ-wep ਲਈ ਅਜੇ ਕੋਈ ਸਮਰਥਨ ਨਹੀਂ...) (wpa-ਉਦਯੋਗਿਕ ਲਈ ਅਜੇ ਸਮਰਥਨ ਨਹੀਂ...) (ਮੂਲ) (ਕੋਈ ਨਹੀਂ) (ਅਣਪਛਾਤੀ ਗਲਤੀ) (ਅਣਜਾਣ) ---[ ਗੁਣ ਮੇਨੂ ]---
set      [<value>]               :: ਨਵਾਂ ਮੁੱਲ ਸੈੱਟ ਕਰੋ
add      [<value>]               :: ਗੁਣ ਵਿੱਚ ਇੱਕ ਨਵੀਂ ਚੋਣ ਜੋੜੋ
change                           :: ਮੌਜੂਦਾ ਮੁੱਲ ਬਦਲੋ
remove   [<index> | <option>]    :: ਮੁੱਲ ਮਿਟਾਉ
describe                         :: ਮੁੱਲ ਦੱਸੋ
print    [setting | connection]  :: ਗੁਣ ਛਾਪੋ (ਸੈਟਿੰਗ/ਸੰਪਰਕ) ਮੁੱਲ(s)
back                             :: ਉੱਪਰਲੇ ਪੱਧਰ ਤੇ ਜਾਉ
help/?   [<command>]             :: ਇਹ ਮਦਦ ਜਾਂ ਕਮਾਂਡ ਵੇਰਵਾ ਛਾਪੋ
quit                             :: nmcli ਤੋਂ ਬਾਹਰ ਹੋਵੋ
 0 (ਕੋਈ ਨਹੀਂ) 0 (ਅਯੋਗ ਕੀਤਾ) 0 (ਕੋਈ) 802.1X 802.1X ਸਪਲੀਕੈਂਟ ਸੰਰਚਨਾ ਫੇਲ੍ਹ ਹੈ 802.1X ਸਪਲੀਕੈਂਟ ਡਿਸ-ਕੁਨੈਕਟ ਹੈ 802.1X ਸਪਲੀਕੈਂਟ ਫੇਲ੍ਹ ਹੈ 802.1X ਸਪਲੀਕੈਂਟ ਨੇ ਪਰਮਾਣਿਤ ਹੋਣ ਲਈ ਬਹੁਤ ਲੰਮਾ ਸਮਾਂ ਲੈ ਲਿਆ ਹੈ 802.3ad ===| nmcli ਇੰਟਰਐਕਟਿਵ ਸੰਪਰਕ ਸੰਪਾਦਕ |=== A (5 GHz) ਕੁਨੈਕਸ਼ਨ ਦੀ ਨਿਰਭਰਤਾ ਫੇਲ੍ਹ ਹੋਇਆ '%s' ਨਾਲ ਜੁੜਨ ਲਈ ਇੱਕ ਗੁਪਤ-ਸ਼ਬਦ ਲੋੜੀਂਦਾ ਹੈ। ADSL ਬਰਿੱਜ਼ ਉੱਤੇ RFC 2684 ਈਥਰਨੈੱਟ ਨਾਲ ਸਮੱਸਿਆ ਆਈ ਹੈ ਬੇਸ ਕੁਨੈਕਸ਼ਨ ਦਾ ਸੈਕੰਡਰੀ ਕੁਨੈਕਸ਼ਨ ਫੇਲ੍ਹ ਹੋਇਆ ਹੈ ADSL ARP ARP ਟਿਕਾਣੇ ਐਕਸੈੱਸ ਬਿੰਦੂ ਕਿਰਿਆਸ਼ੀਲ ਕਰੋ ਕੋਈ ਸੰਪਰਕ ਕਿਰਿਆਸ਼ੀਲ ਕਰੋ ਸਰਗਰਮ ਬੈਕਅੱਪ ਐਡ-ਹਾਕ ਐਡ-ਹੌਕ ਨੈੱਟਵਰਕ ਅਡਾਪਟਿਵ ਲੋਡ ਬੈਲੈਂਸਿੰਗ (alb) ਅਡਾਪਟਿਵ ਟਰਾਂਸਮਿਟ ਲੋਡ ਬੈਲੈਂਸਿੰਗ (tlb) ਜੋੜੋ ਜੋੜੋ... ਇੱਕ ਨਵਾਂ ਸੰਪਰਕ '%s' ਜੋੜ ਰਿਹਾ ਹੈ ਪਤੇ ਉਮਰ ਸਮਾਂ ਨੈੱਟਵਰਕ ਕੁਨੈਕਸ਼ਨਾਂ ਲਈ ਕੰਟਰੋਲ ਮਨਜ਼ੂਰ '%s' ਗੁਣ ਦੇ ਮਨਜ਼ੂਰ ਮੁੱਲ: %s
 ਇੰਟਰਨੈੱਟ ਕੁਨੈਕਸ਼ਨ ਜਾਂਚਣ ਲਈ http(s) ਐਡਰੈਸ ਕੀ ਤੁਸੀਂ ਯਕੀਨਨ ਸੰਪਰਕ '%s' ਨੂੰ ਮਿਟਾਉਣਾ ਚਾਹੁੰਦੇ ਹੋ? ਇਸ ਗੁਪਤ-ਸ਼ਬਦ ਲਈ ਹਰ ਵਾਰ ਪੁੱਛੋ ਪ੍ਰਮਾਣਿਕਤਾ ਬੇ-ਤਾਰ ਨੈੱਟਵਰਕ ਦੁਆਰਾ ਪ੍ਰਮਾਣਿਕਤਾ ਲੋੜੀਂਦੀ ਆਟੋ-IP ਸਰਵਿਸ ਗਲਤੀ ਆਟੋ-IP ਸਰਵਿਸ ਫੇਲ੍ਹ ਹੋਈ ਆਟੋ-IP ਸਰਵਿਸ ਸ਼ੁਰੂ ਕਰਨ ਲਈ ਫੇਲ੍ਹ ਸ੍ਵੈ-ਚਲਿਤ ਸ੍ਵੈ-ਚਲਿਤ (ਸਿਰਫ-DHCP) ਸ੍ਵੈ-ਚਲਿਤ ਜੁੜੋ ਉਪਲਬੱਧ ਗੁਣ: %s
 ਉਪਲੱਬਧ ਸੈਟਿੰਗਾਂ: %s
 ਸਾਰੇ ਯੂਜ਼ਰਾਂ ਨੂੰ ਉਪਲੱਬਧ B/G (2.4 GHz) ਬੌਂਡ ਬਰਿੱਜ ਬਰਿੱਜ ਪੋਰਟ BSSID ਬਲੂਟੁੱਥ ਬੌਂਡ ਬੌਂਡ ਕੁਨੈਕਸ਼ਨ %d ਬਰਿੱਜ ਸੰਪਰਕ %d ਨੂੰ ਬਰਿੱਜ ਕਰੋ ਪ੍ਰਸਾਰਣ ਰੱਦ ਕਰੋ ਕੈਰੀਅਰ/ਲਿੰਕ ਬਦਲਿਆ ਚੈਨਲ ਕਲੋਨ ਕੀਤਾ ਹੋਇਆ MAC ਪਤਾ %s ਬੰਦ ਕਰਨ ਲਈ ਫੇਲ੍ਹ ਹੈ: %s
 Config ਡਾਇਰੈਕਟਰੀ ਸਥਿਤੀ ਸੰਰਚਨਾ ਫਾਇਲ ਟਿਕਾਣਾ ਜੁੜਿਆ ਹੋਇਆ ਕੁਨੈਕਟ ਕੀਤਾ ਜਾ ਰਿਹਾ ਹੈ ਜੁੜ ਰਿਹਾ... ਸੰਪਰਕ '%s' (%s) ਸਫਲਤਾਪੂਰਵਕ ਜੋੜਿਆ ਗਿਆ।
 ਸੰਪਰਕ '%s' (%s) ਸਫਲਤਾਪੂਰਵਕ ਸੁਧਾਰਿਆ ਗਿਆ।
 ਸੰਪਰਕ '%s' (%s) ਸਫਲਤਾਪੂਰਵਕ ਸੰਭਾਲਿਆ ਗਿਆ।
 ਸੰਪਰਕ ਪਹਿਲਾਂ ਤੋਂ ਹੀ ਸਰਗਰਮ ਹੈ ਸੁਰੱਖਿਅਤ ਵਾਈ-ਫਾਈ ਨੈੱਟਵਰਕ ਰਾਹੀਂ ਕੁਨੈਕਸ਼ਨ ਸਾਂਝਾ ਕਰੋ ਓਪਨ ਵਾਈ-ਫਾਈ ਨੈੱਟਵਰਕ ਰਾਹੀਂ ਕੁਨੈਕਸ਼ਨ ਸਾਂਝਾ ਕਰੋ ਕੁਨੈਕਸ਼ਨ ਠੀਕ ਤਰ੍ਹਾਂ ਸਰਗਰਮ ਕੀਤਾ ਗਿਆ (ਡੀ-ਬੱਸ ਐਕਟਿਵ ਪਾਥ: %s)
 UUID '%s' ਨਾਲ ਕੁਨੈਕਸ਼ਨ ਬਣਾਇਆ ਗਿਆ ਅਤੇ ਜੰਤਰ '%s' ਉੱਤੇ ਐਕਟੀਵੇਟ ਕੀਤਾ ਗਿਆ
 ਕੁਨੈਕਟਵਿਟੀ ਸੰਪਰਕ ਨੂੰ ਕਿਰਿਆਸ਼ੀਲ ਨਹੀਂ ਸਕਿਆ: %s ਸੰਪਰਕ '%s' ਕਿਸਮ '%s' ਲਈ ਸੰਪਾਦਕ ਨਹੀਂ ਬਣਾ ਸਕਿਆ। ਅਢੁਕਵੇਂ ਸੰਪਰਕ '%s' ਲਈ ਸੰਪਾਦਕ ਨਹੀਂ ਬਣਾ ਸਕਿਆ। ਆਰਜੀ ਫਾਈਲ ਨਹੀਂ ਬਣਾ ਸਕਿਆ: %s ਡੈਮਨ ਨਹੀਂ ਬਣਾਇਆ ਜਾ ਸਕਿਆ: %s [ਗਲਤੀ %u]
 ਪ੍ਰਾਈਵੇਟ ਕੁੰਜੀ ਡੀਕੋਡ ਨਹੀਂ ਕੀਤੀ ਜਾ ਸਕੀ। ਰਲਵਾਂ ਡਾਟਾ ਤਿਆਰ ਨਹੀਂ ਕੀਤਾ ਜਾ ਸਕਿਆ। ਫਾਈਲ '%s' ਲੋਡ ਨਹੀਂ ਕਰ ਸਕਿਆ
 ਆਰਗੂਮੈਂਟਾਂ ਨੂੰ ਪਾਰਸ ਨਹੀਂ ਕਰ ਸਕਿਆ ਫਾਈਲ ਮੁੜ ਪੜ੍ਹ ਨਹੀਂ ਸਕਿਆ: %s ਪਾਸਵਰਡ ਨੂੰ UCS2 'ਚ ਬਦਲਿਆ ਨਹੀਂ ਜਾ ਸਕਿਆ: %d PKCS#12 ਫਾਇਲ ਜਾਂਚੀ ਨਹੀਂ ਜਾ ਸਕੀ: %d PKCS#12 ਫਾਇਲ ਡੀਕੋਡ ਨਹੀਂ ਜਾ ਸਕੀ: %s PKCS#8 ਫਾਇਲ ਡੀਕੋਡ ਨਹੀਂ ਜਾ ਸਕੀ: %s ਸਰਟੀਫਿਕੇਟ ਡੀਕੋਡ ਨਹੀਂ ਕੀਤਾ ਜਾ ਸਕਿਆ: %d ਸਰਟੀਫਿਕੇਟ ਡੀਕੋਡ ਨਹੀਂ ਕੀਤਾ ਜਾ ਸਕਿਆ: %s PKCS#12 ਡੀਕੋਡਰ ਸ਼ੁਰੂ ਨਹੀਂ ਕੀਤਾ ਜਾ ਸਕਿਆ: %d PKCS#12 ਡੀਕੋਡਰ ਸ਼ੁਰੂ ਨਹੀਂ ਕੀਤਾ ਜਾ ਸਕਿਆ: %s PKCS#8 ਡੀਕੋਡਰ ਸ਼ੁਰੂ ਨਹੀਂ ਕੀਤਾ ਜਾ ਸਕਿਆ: %s PKCS#12 ਫਾਇਲ ਜਾਂਚੀ ਨਹੀਂ ਜਾ ਸਕੀ: %d PKCS#12 ਫਾਇਲ ਜਾਂਚੀ ਨਹੀਂ ਜਾ ਸਕੀ: %s ਬਣਾਉ ਮੌਜੂਦਾ nmcli ਸੰਰਚਨਾ:
 DCB ਜਾਂ FCoE ਸੈੱਟਅਪ ਅਸਫਲ ਹੋਇਆ DHCP ਕਲਾਈਂਟ ਗਲਤੀ DHCP ਕਲਾਈਂਟ ਅਸਫਲ ਹੋਇਆ DHCP ਕਲਾਈਂਟ ਸ਼ੁਰੂ ਹੋਣ ਵਿੱਚ ਅਸਫਲ DNS ਸਰਵਰ DSL DSL ਪ੍ਰਮਾਣਿਕਤਾ DSL ਸੰਪਰਕ %d ਡਾਟਾਗਰਾਮ ਅਕਿਰਿਆਸ਼ੀਲ ਕਰੋ ਮਿਟਾਉ ਟਿਕਾਣਾ ਯੰਤਰ ਯੰਤਰ '%s' ਜੋੜਿਆ ਜਾ ਚੁੱਕਾ ਹੈ।
 ਜੰਤਰ ਵੇਰਵਾ ਜੰਤਰ ਯੂਜ਼ਰ ਜਾਂ ਕਲਾਇਟ ਵਲੋਂ ਡਿਸ-ਕੁਨੈਕਟ ਕੀਤਾ ਗਿਆ ਹੈ ਜੰਤਰ ਹੁਣ ਪ੍ਰਬੰਧ ਅਧੀਨ ਹੈ ਜੰਤਰ ਹੁਣ ਪਰਬੰਧ ਅਧੀਨ ਨਹੀਂ ਹੈ ਅਯੋਗ ਕੀਤਾ ਕੀ ਤੁਸੀਂ '%s' ਨੂੰ ਵੀ ਸਾਫ ਕਰਨਾ ਚਾਹੁੰਦੇ ਹੋ? [yes]:  ਕੀ ਤੁਸੀਂ '%s' ਨੂੰ ਵੀ '%s' ਤੇ ਸੈੱਟ ਕਰਨਾ ਚਾਹੁੰਦੇ ਹੋ? [yes]:  PEM ਪ੍ਰਾਈਵੇਟ ਕੁੰਜੀ ਫਾਈਲ ਵਾਂਗ ਨਹੀਂ ਜਾਪਦੀ। ਡੈਮਨ ਨਾ ਬਣੋ ਇੱਕ ਡੈਮਨ ਨਾ ਬਣੋ, ਅਤੇ stderr ਤੇ ਦਰਜ ਕਰੋ ਕੁਝ ਵੀ ਪਰਿੰਟ ਨਾ ਕਰੋ ਡਾਇਨਾਮਿਕ WEP (802.1x) ਈਥਰਨੈੱਟ '%s' ਮੁੱਲ ਸੋਧੋ:  ਇੱਕ ਸੰਪਰਕ ਸੋਧੋ ਸੋਧੋ... ਮੌਜੂਦਾ ਸੰਪਰਕ '%s' ਸੋਧ ਰਿਹਾ: '%s' ਸੰਪਾਦਕ ਅਸਫਲ ਹੋਇਆ: %s STP (ਸਪੈਨਿੰਗ ਟ੍ਰੀ ਪ੍ਰੋਟੋਕੌਲ) ਯੋਗ ਕਰੋ ਵਾਈ-ਫਾਈ ਜੰਤਰ ਚਾਲੂ ਜਾਂ ਬੰਦ ਕਰੋ ਵਾਈ-ਮੈਕਸ ਮੋਬਾਇਲ ਬਰਾਡਬੈਂਡ ਜੰਤਰ ਚਾਲੂ ਜਾਂ ਬੰਦ ਕਰੋ ਮੋਬਾਇਲ ਬਰਾਡਬੈਂਡ ਜੰਤਰ ਚਾਲੂ ਜਾਂ ਬੰਦ ਸਿਸਟਮ ਨੈੱਟਵਰਕਿੰਗ ਚਾਲੂ ਜਾਂ ਬੰਦ '%s' ਮੁੱਲ ਭਰੋ:  DNS ਸਰਵਰਾਂ ਦੇ IPv4 ਪਤਿਆਂ ਦੀ ਇੱਕ ਸੂਚੀ ਭਰੋ।

ਉਦਾਹਰਣ: 8.8.8.8, 8.8.4.4
 DNS ਸਰਵਰਾਂ ਦੇ IPv4 ਪਤਿਆਂ ਦੀ ਇੱਕ ਸੂਚੀ ਭਰੋ।  ਜੇ IPv6 ਸੰਰਚਨਾਂ ਢੰਗ 'auto' ਹੈ ਇਹ DNS ਸਰਵਰ ਸ੍ਵੈ-ਚਲਿਤ ਸੰਰਚਨਾ ਦੁਆਰਾ ਵਾਪਸ ਕੀਤੇ ਗਏ (ਜੇ ਕੋਈ ਹੋਵੇ) ਵਿੱਚ ਜੋੜ ਦਿੱਤੇ ਜਾਂਦੇ ਹਨ।  DNS ਸਰਵਰ 'shared' ਜਾਂ 'link-local' IPv6 ਸੰਰਚਨਾ ਢੰਗਾਂ ਨਾਲ ਵਰਤੇ ਨਹੀਂ ਜਾ ਸਕਦੇ, ਕਿਉਂਕਿ ਕੋਈ ਅੱਪਸਟਰੀਮ ਨੈੱਟਵਰਕ ਨਹੀਂ ਹੁੰਦਾ। ਬਾਕੀ ਸਾਰੀਆਂ IPv6 ਸੰਰਚਨਾ ਢੰਗਾਂ ਵਿੱਚ, ਇਹ DNS ਸਰਵਰ ਸਿਰਫ ਇਸ ਸੰਪਰਕ ਲਈ DNS ਸਰਵਰਾਂ ਵਜੋਂ ਵਰਤੇ ਜਾਂਦੇ ਹਨ।

ਉਦਾਹਰਣ: 2607:f0d0:1002:51::4, 2607:f0d0:1002:51::1
 ਇਸ ਤਰ੍ਹਾਂ ਫਾਰਮੈੱਟ ਕੀਤੀ ਹੋਈ S/390 ਚੋਣਾਂ ਦੀ ਇੱਕ ਸੂਚੀ ਭਰੋ:
  ਚੋਣ = <value>, ਚੋਣ = <value>,...
ਢੁਕਵੀਆਂ ਚੋਣਾਂ ਹਨ: %s
 ਇਸ ਤਰ੍ਹਾਂ ਫਾਰਮੈਟ ਕੀਤੀਆਂ ਬੌਂਡਿੰਗ ਚੋਣਾਂ ਦੀ ਸੂਚੀ ਭਰੋ:
  option = <value>, option = <value>,... 
ਢੁਕਵੀਆਂ ਚੋਣਾਂ ਹਨ: %s
'mode' ਨੂੰ ਇੱਕ ਨਾਂ ਜਾਂ ਇੱਕ ਅੰਕ ਵਜੋਂ ਮੁਹੱਈਆ ਕਰਵਾਇਆ ਜਾ ਸਕਦਾ ਹੈ:
balance-rr    = 0
active-backup = 1
balance-xor   = 2
broadcast     = 3
802.3ad       = 4
balance-tlb   = 5
balance-alb   = 6

ਉਦਾਹਰਣ: mode=2,miimon=120
 ਯੂਜ਼ਰ ਮਨਜੂਰੀਆਂ ਦੀ ਇੱਕ ਸੂਚੀ ਭਰੋ। ਇਹ ਯੂਜਰ ਨਾਵਾਂ ਦੀ ਇਸ ਤਰ੍ਹਾਂ ਫਾਰਮੈਟ ਕੀਤੀ ਹੋਈ ਸੂਚੀ ਹੈ:
  [user:]<user name 1>, [user:]<user name 2>,...
ਆਈਟਮਾਂ ਨੂੰ ਕੌਮਿਆਂ ਜਾਂ ਖਾਲੀ ਥਾਵਾਂ ਨਾਲ ਅੱਡ ਕੀਤਾ ਜਾ ਸਕਦਾ ਹੈ।

ਉਦਾਹਰਣ: ਸੁਰਿੰਦਰ ਮਨਦੀਪ ਅਨੁਜ
 ਬਾਈਟਾਂ ਨੂੰ ਹੈਕਸਾਡੈਸੀਮਲ ਮੁੱਲਾਂ ਦੀ ਸੂਚੀ ਵਜੋਂ ਭਰੋ
ਦੋ ਫਾਰਮੈਟ ਪ੍ਰਵਾਨ ਹਨ:
(a) ਹੈਕਸਾਡੈਸੀਮਲ ਅੱਖਰਾਂ ਦੀ ਇੱਕ ਸਤਰ, ਜਿੱਥੇ ਹਰ ਦੋ ਅੱਖਰ ਇੱਕ ਬਾਈਟ ਦਰਸਾਉਂਦੇ ਹਨ
(b) ਖਾਲੀ ਥਾਵਾਂ ਨਾਲ ਵੱਖ ਕੀਤੀ ਹੈਕਸਾਡੈਸੀਮਲ ਅੱਖਰਾਂ (ਚੋਣਵੇਂ 0x/0X ਅਗੇਤਰ, ਅਤੇ ਚੋਣਵੀਂ ਅਗਲੀ 0 ਨਾਲ) ਦੀ ਸੂਚੀ।

ਉਦਾਹਰਣ: ab0455a6ea3a74C2
          ab 4 55 0xa6 ea 3a 74 C2
 ਸੰਪਰਕ ਕਿਸਮ ਭਰੋ:  WEP ਚਾਬੀਆਂ ਦੀ ਕਿਸਮ ਭਰੋ। ਪਰਵਾਨਿਤ ਮੁੱਲ ਹਨ: 0 ਜਾਂ ਅਣਪਛਾਤਾ, 1 ਜਾਂ ਚਾਬੀ, ਅਤੇ 2 ਜਾਂ ਪਛਾਣ-ਸ਼ਬਦ।
 ਸੰਰਚਨਾ ਫਾਈਲ ਵਿੱਚ ਗਲਤੀ: %s।
 ਸਰਟੀਫਿਕੇਟ ਡਾਟਾ ਸ਼ੁਰੂ 'ਚ ਗਲਤੀ: %s ਗਲਤੀ: %s
 ਗਲਤੀ: %s ਆਰਗੂਮੈਂਟ ਮੌਜੂਦ ਨਹੀਂ ਹੈ। ਗਲਤੀ: %s ਗੁਣ, ਨਾ ਹੀ ਇਹ ਇੱਕ ਸੈਟਿੰਗ ਨਾਂ ਹੈ।
 ਗਲਤੀ: %s ਗਲਤੀ: %s: %s। ਗਲਤੀ: '%s' ਆਰਗੂਮੈਂਟ ਗੁੰਮ ਹੈ। ਗਲਤੀ: '%s' ਇੱਕ ਢੁਕਵਾਂ ਮੌਨੀਟਰ ਕਰਨ ਵਾਲਾ ਮੋਡ ਨਹੀਂ ਹੈ; '%s' ਜਾਂ '%s' ਵਰਤੋ।
 ਗਲਤੀ: '%s' is not valid argument for '%s' option. ਗਲਤੀ: '--fields' ਮੁੱਲ '%s' ਇੱਥੇ ਢੁਕਵਾਂ ਨਹੀਂ ਹੈ (ਮਨਜ਼ੂਰਸ਼ੁਦਾ ਖੇਤਰ: %s) ਗਲਤੀ: 'autoconnect': %s। ਗਲਤੀ: 'device show': %s ਗਲਤੀ: 'device status': %s ਗਲਤੀ: 'device wifi': %s ਗਲਤੀ: 'general logging': %s ਗਲਤੀ: 'general permissions': %s ਗਲਤੀ: 'networking' ਕਮਾਂਡ '%s' ਢੁੱਕਵੀਂ ਨਹੀਂ ਹੈ। ਗਲਤੀ: 'type' ਆਰਗੂਮੈਂਟ ਲੋੜੀਂਦਾ ਹੈ। ਗਲਤੀ: <setting>.<property> ਆਰਗੂਮੈਂਟ ਗੁੰਮ ਹੈ। ਗਲਤੀ: bssid '%s' ਵਾਲਾ ਅਸੈੱਸ ਪੁਆਇੰਟ ਨਹੀਂ ਲੱਭਿਆ। ਗਲਤੀ: ਆਰਗੂਮੈਂਟ '%s' ਦੀ ਆਸ ਕੀਤੀ ਸੀ, ਪਰ '%s' ਮੁਹੱਈਆ ਕਰਵਾਇਆ ਗਿਆ। ਗਲਤੀ: (%s) ਨਾਲ ਕੁਨੈਕਟ ਕਰਨ ਲਈ BSSID bssid ਆਰਗੂਮੈਂਟ (%s) ਤੋਂ ਵੱਖਰਾ ਹੈ। ਗਲਤੀ: ਸੰਪਰਕ ਨੂੰ ਕਿਰਿਆਸ਼ੀਲ ਨਹੀਂ ਕਰ ਸਕਦਾ: %s।
 ਗਲਤੀ: ਸੰਪਰਕ ਕਿਰਿਆਸ਼ੀਲ ਕਰਨ ਵਿੱਚ ਅਸਫਲ।
 ਗਲਤੀ: ਕੁਨੈਕਸ਼ਨ ਚਾਲੂ ਕਰਨ ਲਈ ਫੇਲ੍ਹ: %s ਗਲਤੀ: ਕੁਨੈਕਸ਼ਨ ਹਟਾਉਣ ਲਈ ਫੇਲ੍ਹ: %s ਗਲਤੀ: ਜੰਤਰ '%s' ਵਾਈ-ਫਾਈ ਜੰਤਰ ਨਹੀਂ ਹੈ। ਗਲਤੀ: ਜੰਤਰ '%s' ਨਹੀਂ ਲੱਭਿਆ। ਗਲਤੀ: ਯੰਤਰ ਕਿਰਿਆਸ਼ੀਲਤਾ ਅਸਫਲ ਹੋਈ: %s ਗਲਤੀ: ਨਵਾਂ ਕੁਨੈਕਸ਼ਨ ਜੋੜਨ/ਸਰਗਰਮ ਕਰਨ ਲਈ ਫੇਲ੍ਹ: ਅਣਜਾਣ ਗਲਤੀ ਗਲਤੀ: ਨੈੱਟਵਰਕਮੈਨੇਜਰ ਚੱਲ ਨਹੀਂ ਰਿਹਾ ਹੈ। ਗਲਤੀ: ਕੋਈ ਵਾਈ-ਫਾਈ ਜੰਤਰ ਨਹੀਂ ਮਿਲਿਆ। ਗਲਤੀ: BSSID '%s' ਵਾਲਾ ਕੋਈ ਅਸੈੱਸ ਪੁਆਇੰਟ ਨਹੀਂ ਲੱਭਿਆ। ਗਲਤੀ: ਕੋਈ ਆਰਗੂਮੈਂਟ ਮੁਹੱਈਆ ਕਰਵਾਏ ਗਏ। ਗਲਤੀ: ਕੋਈ ਸੰਪਰਕ ਦਰਸਾਇਆ ਨਹੀਂ ਗਿਆ। ਗਲਤੀ: ਕੋਈ ਇੰਟਰਫੇਸ ਦਰਸਾਇਆ ਨਹੀਂ ਗਿਆ। ਗਲਤੀ: SSID '%s' ਵਾਲਾ ਕੋਈ ਨੈੱਟਵਰਕ ਨਹੀਂ ਲੱਭਿਆ। ਗਲਤੀ: ਚੋਣ '%s' ਅਣਜਾਣੀ ਹੈ 'nmcli -help' ਵਰਤ ਕੇ ਵੇਖੋ। ਗਲਤੀ: Option '--pretty' is mutually exclusive with '--terse'. ਗਲਤੀ: Option '--pretty' is specified the second time. ਗਲਤੀ: Option '--terse' is mutually exclusive with '--pretty'. ਗਲਤੀ: ਚੋਣ '--terse' ਦੂਜੀ ਵਾਰ ਦਿੱਤੀ ਗਈ। ਗਲਤੀ: ਪੈਰਾਮੀਟਰ '%s' ਨਾ ਤਾਂ SSID ਹੈ ਨਾ BSSID। ਗਲਤੀ: SSID ਜਾਂ BSSID ਮੌਜੂਦ ਨਹੀਂ ਹੈ। ਗਲਤੀ: ਸਮਾਂ-ਸਮਾਪਤੀ %d ਸਕਿੰਟ ਖਤਮ ਹੋਏ। ਗਲਤੀ: ਅਣਕਿਆਸਿਆ ਆਰਗੂਮੈਂਟ '%s' ਗਲਤੀ: ਅਣਪਛਾਤਾ ਸੰਪਰਕ '%s'। ਗਲਤੀ: bssid ਆਰਗੂਮੈਂਟ '%s' ਢੁੱਕਵਾਂ BSSID ਨਹੀਂ ਹੈ। ਗਲਤੀ: ਅਣਪਛਾਤੇ ਸੰਪਰਕ ਮਿਟਾ ਨਹੀਂ ਸਕਦਾ: %s। ਗਲਤੀ: ਸੰਪਰਕ ਸੰਭਾਲਿਆ ਨਹੀਂ ਹੈ। ਪਹਿਲਾਂ 'save' ਲਿਖੋ।
 ਗਲਤੀ: ਸੰਪਰਕ ਢੁਕਵਾਂ ਨਹੀਂ ਹੈ: %s
 ਗਲਤੀ: ਸੰਪਰਕ ਪੜਤਾਲ ਅਸਫਲ ਹੋਈ: %s
 ਗਲਤੀ: ਵਾਧੂ ਆਰਗੂਮੈਂਟ ਮਨਜ਼ੂਰ ਨਹੀਂ ਹੈ: '%s'। ਗਲਤੀ: %s.%s ਨੂੰ ਸੁਧਾਰਨ ਵਿੱਚ ਅਸਫਲ: %s। ਗਲਤੀ: '%s' ਦਾ ਮੁੱਲ ਹਟਾਉਣ ਵਿੱਚ ਅਸਫਲ: %s
 ਗਲਤੀ: '%s' ਗੁਣ ਨੂੰ ਸੈੱਟ ਕਰਨ ਵਿੱਚ ਅਸਫਲ: %s
 ਗਲਤੀ: ਅਢੁਕਵਾਂ ਆਰਗੂਮੈਂਟ '%s': '%s' (ਚਾਲੂ/ਬੰਦ ਵਰਤੋ)। ਗਲਤੀ: ਅਢੁਕਵੀਂ <setting>.<property> '%s'। ਗਲਤੀ: ਅਢੁਕਵੀਂ ਸੰਪਰਕ ਕਿਸਮ; %s
 ਗਲਤੀ: ਅਢੁਕਵੀਂ ਸੰਪਰਕ ਕਿਸਮ; %s। ਗਲਤੀ: ਅਢੁਕਵਾਂ ਵਾਧੂ ਆਰਗੂਮੈਂਟ '%s'। ਗਲਤੀ: ਅਢੁਕਵੀਂ ਜਾਂ ਨਾਂਮਨਜ਼ੂਰ ਸੈਟਿੰਗ '%s': %s। ਗਲਤੀ: ਅਢੁਕਵਾਂ ਗੁਣ '%s': %s। ਗਲਤੀ: ਅਢੁਕਵਾਂ ਗੁਣ: %s
 ਗਲਤੀ: ਅਢੁਕਵਾਂ ਗੁਣ: %s, ਨਾ ਤਾਂ ਇੱਕ ਢੁਕਵਾਂ ਸੈਟਿੰਗ ਨਾਂ।
 ਗਲਤੀ: ਅਢੁਕਵਾਂ ਸੈਟਿੰਗ ਆਰਗੂਮੈਂਟ '%s'; ਢੁਕਵੇਂ ਹਨ [%s]
 ਗਲਤੀ: ਅਢੁਕਵਾਂ ਸੈਟਿੰਗ ਨਾਂ; %s
 ਗਲਤੀ: '%s' ਚੋਣ ਲਈ ਆਰਗੂਮੈਂਟ ਮੌਜੂਦ ਨਹੀਂ ਹੈ। ਗਲਤੀ: '%s' ਗੁਣ ਲਈ ਸੈਟਿੰਗ ਗੁੰਮ
 ਗਲਤੀ: ਕੋਈ ਆਰਗੂਮੈਂਟ ਦਿੱਤਾ ਨਹੀਂ ਗਿਆ; ਢੁਕਵੇਂ ਹਨ [%s]
 ਗਲਤੀ: ਕੋਈ ਸੈਟਿੰਗ ਚੁਣੀ ਨਹੀਂ ਹੋਈ; ਢੁਕਵੇਂ ਹਨ [%s]
 ਗਲਤੀ: 'id', uuid, ਜਾਂ 'path' ਵਿੱਚੋਂ ਸਿਰਫ ਇੱਕ ਮੁਹੱਈਆ ਕਰਵਾਇਆ ਜਾ ਸਕਦਾ ਹੈ। ਗਲਤੀ: ਸਿਰਫ ਇਹਨਾਂ ਫਾਈਲਾਂ ਨੂੰ ਮਨਜੂਰੀ ਹੈ: %s ਗਲਤੀ: ਓਪਨਕਨੈਕਟ %d ਹਾਲਤ ਨਾਲ ਫੇਲ੍ਹ ਹੈ
 ਗਲਤੀ: ਗੁਣ %s
 ਗਲਤੀ: ਸੰਰਚਨਾ-ਸੰਭਾਲੋ: %s
 ਗਲਤੀ: ਸੈਟਿੰਗ '%s' ਲਾਜਮੀ ਹੈ ਅਤੇ ਹਟਾਈ ਨਹੀਂ ਜਾ ਸਕਦੀ।
 ਗਲਤੀ: ਹਾਲਾਤ-ਸਤਰ੍ਹ: %s
 ਗਲਤੀ: ਅਣਪਛਾਤੀ ਸੈਟਿੰਗ '%s'
 ਗਲਤੀ: ਅਣਪਛਾਤੀ ਸੈਟਿੰਗ: '%s'
 ਗਲਤੀ: ਆਰਗੂਮੈਂਟ '%s' ਦਾ ਮੁੱਲ ਲੋੜੀਂਦਾ ਹੈ। ਗਲਤੀ: wep-key-type ਆਰਗੂਮੈਂਟ ਮੁੱਲ '%s' ਗਲਤ ਹੈ, 'key' ਜਾਂ 'phrase' ਵਰਤੋ। ਈਥਰਨੈੱਟ ਈਥਰਨੈੱਟ ਸੰਪਰਕ %d PKCS#8 ਪ੍ਰਾਈਵੇਟ ਕੁੰਜੀ ਡੀਕੋਡ ਕਰਨ ਲਈ ਫੇਲ੍ਹ ਹੈ। ਸਰਟੀਫਿਕੇਟ ਡੀਕੋਡ ਕਰਨ ਲਈ ਫੇਲ੍ਹ ਹੈ। ਪ੍ਰਾਈਵੇਟ ਕੁੰਜੀ ਡਿਕ੍ਰਿਪਟ ਕਰਨ ਲਈ ਫੇਲ੍ਹ ਪ੍ਰਾਈਵੇਟ ਕੁੰਜੀ ਡਿਕ੍ਰਿਪਟ ਕਰਨ ਲਈ ਫੇਲ੍ਹ: %d ਪ੍ਰਾਈਵੇਟ ਕੁੰਜੀ ਡਿਕ੍ਰਿਪਟ ਕਰਨ ਲਈ ਫੇਲ੍ਹ: ਡਿਕ੍ਰਿਪਟ ਡਾਟਾ ਬਹੁਤ ਵੱਡਾ ਹੈ। ਪ੍ਰਾਈਵੇਟ ਕੁੰਜੀ ਡਿਕ੍ਰਿਪਟ ਕਰਨ ਲਈ ਫੇਲ੍ਹ: ਅਣਜਾਣ ਪੈਡਿੰਗ ਲੰਬਾਈ। ਇੰਕ੍ਰਿਪਟ ਕਰਨ ਲਈ ਫੇਲ੍ਹ: %d ਪ੍ਰਾਈਵੇਟ ਕੁੰਜ ਲਈ ਡੀਕ੍ਰਿਪਸ਼ਨ ਮੁਕੰਮਲ ਕਰਨ ਲਈ ਫੇਲ੍ਹ ਹੈ: %d। ਲੋੜੀਂਦਾ PKCS#8 ਅੰਤ ਟੈਗ '%s' ਲੱਭਣ ਵਿੱਚ ਅਸਫਲ ਹੋਇਆ। ਉਮੀਦ ਕੀਤਾ PKCS#8 ਸ਼ੁਰੂ ਟੈਗ ਲੱਭਣ ਵਿੱਚ ਅਸਫਲ। MD5 ਪਰਸੰਗ ਸ਼ੁਰੂ ਕਰਨ ਲਈ ਫੇਲ੍ਹ: %d ਕ੍ਰਿਪਟੂ ਇੰਜਣ ਸ਼ੁਰੂ ਕਰਨ ਲਈ ਫੇਲ੍ਹ ਹੈ। ਕ੍ਰਿਪਟੂ ਇੰਜਣ ਸ਼ੁਰੂ ਕਰਨ ਲਈ ਫੇਲ੍ਹ: %d ਡਿਕ੍ਰਿਪਸ਼ਨ ਸੀਫਰ ਸਲਾਟ ਸ਼ੁਰੂ ਕਰਨ ਲਈ ਫੇਲ੍ਹ ਹੈ। ਡਿਕ੍ਰਿਪਸ਼ਨ ਪਰਸੰਗ ਸ਼ੁਰੂ ਕਰਨ ਲਈ ਫੇਲ੍ਹ:  ਕ੍ਰਿਪਸ਼ਨ ਸੀਫ਼ਰ ਸਲਾਟ ਸ਼ੁਰੂ ਕਰਨ ਲਈ ਫੇਲ੍ਹ ਹੈ। ਇੰਕ੍ਰਿਪਸ਼ਨ ਪਰਸੰਗ ਸ਼ੁਰੂ ਕਰਨ ਲਈ ਫੇਲ੍ਹ ਹੈ। ਮੰਗੇ ਗਏ ਨੈੱਟਵਰਕ ਉੱਤੇ ਰਜਿਸਟਰ ਕਰਨ ਲਈ ਫੇਲ੍ਹ ਦਿੱਤੇ APN ਨੂੰ ਚੁਣਨ ਲਈ ਫੇਲ੍ਹ ਹੈ ਡਿਕ੍ਰਿਪਟ ਲਈ IV ਸੈੱਟ ਕਰਨ ਲਈ ਫੇਲ੍ਹ ਹੈ। ਇੰਕ੍ਰਿਪਸ਼ਨ ਲਈ IV ਸੈੱਟ ਕਰਨ ਵਾਸਤੇ ਫੇਲ੍ਹ। ਡਿਕ੍ਰਿਪਸ਼ਨ ਲਈ ਸਮਮਿਤੀ ਕੁੰਜੀ ਸੈੱਟ ਕਰਨ ਲਈ ਫੇਲ੍ਹ ਹੈ। ਇੰਕ੍ਰਿਪਸ਼ਨ ਲਈ ਸਮਮਿਤੀ ਕੁੰਜੀ ਸੈੱਟ ਕਰਨ ਲਈ ਫੇਲ੍ਹ। ਅੱਗੇ ਨੂੰ ਦੇਰੀ ਸਮੂਹ GSM ਮਾਡਮ ਦਾ ਸਿਮ ਪਿੰਨ ਚਾਹੀਦਾ ਹੈ GSM ਮਾਡਮ ਦਾ ਸਿਮ PUK ਚਾਹੀਦਾ ਹੈ GSM ਦਾ ਸਿਮ ਕਾਰਡ ਨਹੀਂ ਪਾਇਆ ਗਿਆ ਹੈ GSM ਮਾਡਮ ਦਾ ਗਲਤ ਸਿਮ (SIM) GVRP,  ਗੇਟਵੇਅ ਹੇਅਰਪਿੰਨ ਮੋਡ ਹੈਲੋ ਸਮਾਂ ਲੁਕਾਉ ਮੇਜਬਾਨ ਨਾਂ INFINIBAND IP ਸੰਰਚਨਾ ਨੂੰ ਰਾਖਵਾਂ ਨਹੀਂ ਕੀਤਾ ਜਾ ਸਕਿਆ (ਕੋਈ ਐਡਰੈਸ ਉਪਲੱਬਧ ਨਹੀਂ, ਸਮਾਂ ਸਮਾਪਤ ਹੋਣਾ ਆਦਿ) IPv4 ਸੰਰਚਨਾ IPv6 ਸੰਰਚਨਾ IV ਵਿੱਚ ਗ਼ੈਰ-ਹੈਕਸਾਡੈਸੀਮਲ ਅੰਕ ਹਨ। IV ਗਿਣਤੀ ਵਿੱਚ ਲਾਜ਼ਮੀ ਜਿਸਤ ਲੰਬਾਈ ਦੀਆਂ ਬਾਈਟਾਂ ਹੋਣ। ਸ਼ਨਾਖਤ ਜੇ ਤੁਸੀਂ ਇੱਕ VPN ਬਣਾ ਰਹੇ ਹੋ, ਅਤੇ ਜਿਹੜਾ VPN ਤੁਸੀਂ ਬਣਾਉਣਾ ਚਾਹੁੰਦੇ ਹੋ ਉਹ ਸੂਚੀ ਵਿੱਚ ਵਿਖਾਈ ਨਹੀਂ ਦਿੰਦਾ, ਤੁਹਾਡੇ ਕੋਲ ਸਹੀ VPN ਪਲੱਗਇਨ ਇੰਸਟਾਲ ਨਹੀਂ ਹੋਇਆ ਹੋ ਸਕਦਾ। ਅਣਗੌਲਿਆ ਕਰੋ ਸੰਰਚਨਾ ਫਾਈਲਾਂ ਤੋਂ ਬੇਪਛਾਣ ਡੋਮੇਨ(ਡੋਮੇਨਾਂ) '%s' ਨੂੰ ਅਣਗੌਲਿਆਂ ਕਰ ਰਿਹਾ।
 ਕਮਾਂਡ ਲਾਈਨ ਤੇ ਪਾਸ ਕੀਤੀ (ਕੀਤੀਆਂ) ਬੇਪਛਾਣ ਡੋਮੇਨ(ਡੋਮੇਨਾਂ) '%s' ਨੂੰ ਅਣਗੌਲਿਆਂ ਕਰ ਰਿਹਾ।
 InfiniBand InfiniBand P_Key ਸੰਪਰਕ ਨੇ ਮੁੱਖ ਇੰਟਰਫੇਸ ਨਾਂ ਨਹੀਂ ਦਰਸਾਇਆ ਇਫੀਂਬੈਂਡ ਕੁਨੈਕਸ਼ਨ %d ਇੰਫੀਬੀਮ ਜੰਤਰ ਕੁਨੈਕਟ ਕੀਤੇ ਮੋਡ ਵਿੱਚ ਸਹਾਇਕ ਨਹੀਂ ਹੈ ਇੰਨਫਰਾ ਇੰਟਰਫੇਸ:  ਅਢੁੱਕਵੀਂ IV ਲੰਬਾਈ (ਘੱਟੋ-ਘੱਟ %d ਹੋਣੀ ਲਾਜ਼ਮੀ ਹੈ)। ਅਢੁੱਕਵੀਂ IV ਲੰਬਾਈ (ਘੱਟੋ-ਘੱਟ %zd ਹੋਣੀ ਲਾਜ਼ਮੀ ਹੈ)। ਅਢੁਕਵੀਂ ਸੰਰਚਨਾ ਚੋਣ '%s'; ਮਨਜੂਰ [%s] ਹੈ
 ਗਲਤ ਚੋਣ। ਢੁੱਕਵੀਆਂ ਚੋਣਾਂ ਵੇਖਣ ਲਈ --help ਵਰਤੋਂ ਜੀ। JSON ਸੰਰਚਨਾ ਚਾਬੀ ਲੀਪ LOOSE_BINDING,  ਲਿੰਕ ਡਾਊਨ ਦੇਰੀ ਲਿੰਕ ਮੌਨੀਟਰਿੰਗ ਲਿੰਕ ਅੱਪ ਦੇਰੀ ਲਿੰਕ-ਸਥਾਨਕ ',' ਰਾਹੀਂ ਵੱਖ ਕਰਕੇ ਪਲੱਗਇਨ ਦੀ ਲਿਸਟ ਦਿਓ ਲੌਗ ਡੋਮੇਨਾਂ ',' ਨਾਲ ਵੱਖਰੀਆਂ ਕੀਤੀਆਂ ਹੋਈਆਂ: [%s] ਦਾ ਕੋਈ ਵੀ ਮਿਸ਼ਰਣ ਲੌਗ ਪੱਧਰ: [%s] ਦਾ ਇੱਕ MII (ਸਿਫਾਰਸ਼ੀ) MTU ਸਭ ਚੇਤਾਵਨੀਆਂ ਨੂੰ ਘਾਤਕ ਬਣਾਓ ਨਿਕਾਰਾ PEM ਫਾਈਲ: DEK-Info ਦੂਜਾ ਟੈਗ ਨਹੀਂ ਹੈ। ਨਿਕਾਰਾ PEM ਫਾਈਲ: Proc-Type ਪਹਿਲਾਂ ਟੈਗ ਨਹੀਂ ਹੈ।  ਨਿਕਾਰਾ PEM ਫਾਈਲ: DEK-Info ਟੈਗ ਵਿੱਚ ਗਲਤ ਫਾਰਮੈਟ IV ਨਿਕਾਰਾ PEM ਫਾਈਲ: DEK-Info ਟੈਗ ਵਿੱਚ ਕੋਈ IV ਨਹੀਂ ਲੱਭਿਆ। ਨਿਕਾਰਾ PEM ਫਾਈਲ: ਅਣਜਾਣ Proc-Type ਟੈਗ '%s' ਹੈ। ਨਿਕਾਰਾ PEM ਫਾਈਲ: ਅਣਜਾਣ ਪ੍ਰਾਈਵੇਟ ਕੁੰਜੀ ਸੀਫਰ '%s'। ਦਸਤੀ ਵੱਧ ਤੋਂ ਵੱਧ ਉਮਰ ਮੀਟਰਿਕ ਮੋਬਾਇਲ ਬਰੌਡਬੈਂਡ ਮੋਬਾਇਲ ਬਰੌਡਬੈਂਡ ਸੰਪਰਕ %d ਮੋਬਾਇਲ ਬਰੌਡਬੈਂਡ ਨੈੱਟਵਰਕ ਗੁਪਤ-ਸ਼ਬਦ ਮੋਡ ਮਾਡਮ ਸ਼ੁਰੂਆਤ ਕਰਨ ਲਈ ਫੇਲ੍ਹ ਮਾਡਮਮੈਨੇਜਰ ਉਪਲੱਬਧ ਨਹੀਂ ਹੈ ਸਭ ਯੂਜ਼ਰ ਲਈ ਨੈੱਟਵਰਕ ਕੁਨੈਸ਼ਨ ਸੋਧ ਸਥਿਰ ਸਿਸਟਮ ਹੋਸਟ-ਨਾਂ ਸੋਧ ਨਿੱਜੀ ਨੈੱਟਵਰਕ ਕੁਨੈਕਸ਼ਨ ਸੋਧੋ ਸੰਪਰਕ ਕਿਰਿਆਸ਼ੀਲਤਾ ਨੂੰ ਮੌਨੀਟਰ ਕਰ ਰਿਹਾ (ਜਾਰੀ ਰਹਿਣ ਲਈ ਕੋਈ ਬਟਨ ਦੱਬੋ)
 ਮੌਨੀਟਰ ਕਰਨ ਦੀ ਆਵਰਤੀ ਜੇ ਮੁੱਖ ਦਰਸਾਉਣਾ ਹੋਵੇ ਤਾਂ P_Key ਨੂੰ ਦਰਸਾਉਣਾ ਲਾਜਮੀ ਹੈ ਨਾ ਉਪਲੱਬਧ ਜੰਤਰ ਲਈ ਲੋੜੀਦਾ ਫਿਰਮਵੇਅਰ ਸ਼ਾਇਦ ਮੌਜੂਦ ਨਹੀਂ ਨੈੱਟਵਰਕ ਰਜਿਸਟਰੇਸ਼ਨ ਉੱਤੇ ਪਾਬੰਦੀ ਨੈੱਟਵਰਕ ਰਜਿਸਟਰ ਲਈ ਸਮਾਂ-ਸਮਾਪਤ ਨੈੱਟਵਰਕ-ਮੈਨੇਜਰ TUI ਨੈੱਟਵਰਕ ਮੈਨੇਜਰ ਚੱਲ ਨਹੀਂ ਰਿਹਾ ਹੈ। ਨੈੱਟਵਰਕਮੈਨੇਜਰ ਲੌਗਿੰਗ ਨੈੱਟਵਰਕਮੈਨੇਜਰ ਸਭ ਨੈੱਟਵਰਕ ਕੁਨਕੈਸ਼ਨ ਦੀ ਨਿਗਰਾਨੀ ਕਰਨ ਅਤੇ ਵਰਤਣ ਲਈ
ਸਭ ਤੋਂ ਵਧੀਆ ਦੀ ਚੋਣ ਆਪਣੇ-ਆਪ ਕਰ ਸਕਦਾ ਹੈ। ਇਹ ਯੂਜ਼ਰ ਨੂੰ ਬੇਤਾਰ ਅਸੈਸ
ਪੁਆਇੰਟ ਦੇਣ ਲਈ ਵੀ ਸਹਾਇਕ ਹੈ, ਜਿਸ ਨਾਲ ਕੰਪਿਊਟਰ ਵਿਚਲੇ ਬੇਤਾਰ ਕਾਰਡ
ਸਬੰਧਿਤ ਹੋਣੇ ਚਾਹੀਦੇ ਹਨ। ਨੈੱਟਵਰਕਮੈਨੇਜਰ ਅਧਿਕਾਰ ਨੈੱਟਵਰਕਮੈਨੇਜਰ ਹਾਲਤ ਨੈੱਟਵਰਕਮੈਨੇਜਰ ਸਲੀਪ ਮੋਡ ਵਿੱਚ ਗਿਆ ਨੈੱਟਵਰਕਿੰਗ ਮੂਲ ਰਾਹ ਲਈ ਇਸ ਨੈੱਟਵਰਕ ਨੂੰ ਕਦੇ ਨਾ ਵਰਤੋ ਨਵਾਂ ਸੰਪਰਕ ਅਗਲੀ ਛਾਲ ਕੋਈ ਕੈਰੀਅਰ ਤਿਆਰ ਨਹੀਂ ਕੀਤਾ ਜਾ ਸਕਿਆ ਕੋਈ ਚੁਣਿੰਦਾ ਰੂਟ ਪਰਿਭਾਸ਼ਤ ਨਹੀਂ ਹਨ। ਕੋਈ ਡਾਇਲ ਟੋਨ ਨਹੀਂ ਕੋਈ ਕਾਰਨ ਨਹੀਂ ਦਿੱਤਾ ਹੈ। ਅਜਿਹਾ ਕੋਈ ਸੰਪਰਕ ਨਹੀਂ '%s' ਨੈੱਟਵਰਕ ਲਈ ਖੋਜ ਨਹੀਂ ਕੀਤੀ ਜਾ ਰਹੀ ਠੀਕ ਹੈ OLPC ਜਾਲੀ ਇੱਕ ਚੁਣਿੰਦਾ ਰਾਹ %d ਚੁਣਿੰਦਾ ਰਾਹ ਖੁੱਲਾ ਸਿਸਟਮ '%s' ਨੂੰ ਖੋਲ੍ਹਣ ਲਈ ਫੇਲ੍ਹ: %s
 PCI PEM ਸਰਟੀਫਿਕੇਟ ਵਿੱਚ ਕੋਈ ਅੰਤ ਟੈਗ '%s' ਨਹੀਂ ਹੈ। PEM ਸਰਟੀਫਿਕੇਟ ਵਿੱਚ ਕੋਈ ਸ਼ੁਰੂ ਟੈਗ '%s' ਨਹੀਂ ਹੈ। PEM ਕੁੰਜੀ ਫਾਈਲ ਦਾ ਕੋਈ ਅੰਤ ਟੈਗ '%s' ਨਹੀਂ ਹੈ। PIN PIN ਚੈਕ ਫੇਲ੍ਹ ਹੈ ਮੋਬਾਇਲ ਬਰੌਡਬੈਂਡ ਯੰਤਰ ਲਈ PIN ਕੋਡ ਲੋੜੀਂਦਾ ਹੈ PIN ਕੋਡ ਲੋੜੀਂਦਾ PPP ਫੇਲ੍ਹ ਹੈ PPP ਸਰਵਿਸ ਡਿਸ-ਕੁਨੈਕਟ ਹੈ PPP ਸਰਵਿਸ ਸ਼ੁਰੂ ਕਰਨ ਲਈ ਫੇਲ੍ਹ ਪ੍ਰਮੁੱਖ ਗੁਪਤ-ਸ਼ਬਦ ਗੁਪਤ-ਸ਼ਬਦ:  ਬੇ-ਤਾਰ ਨੈੱਟਵਰਕ '%s' ਤੇ ਦਖਲ ਲਈ ਗੁਪਤ-ਸ਼ਬਦ ਜਾਂ ਇੰਕ੍ਰਿਪਸ਼ਨ ਚਾਬੀਆਂ ਲੋੜੀਂਦੀਆਂ ਹਨ। ਰਾਹ ਕੀਮਤ ਕਿਰਪਾ ਕਰ ਕੇ ਕੋਈ ਚੋਣ ਕਰੋ ਅਗੇਤਰ ਪ੍ਰਮੁੱਖ ਨੈੱਟਵਰਕਮੈਨੇਜਰ ਵਰਜਨ ਦਿਉ ਅਤੇ ਬੰਦ ਕਰੋ ਤਰਜੀਹ ਪ੍ਰਾਈਵੇਟ ਕੁੰਜੀ ਸੀਫ਼ਰ '%s' ਅਣਜਾਣ ਹੈ। ਨਿੱਜੀ ਚਾਬੀ ਪਛਾਣ-ਵਾਕ ਪਰੋਫਾਈਲ ਨਾਂ ਗੁਣ ਨਾਂ? ਨੈੱਟਵਰਕਮੈਨਜੇਰ ਨੂੰ ਸਲੀਪ ਕਰੋ ਜਾਂ ਵੇਕ ਅੱਪ ਕਰੋ (ਕੇਵਲ ਸਿਸਟਮ ਪਾਵਰ ਮੈਨਜੇਮੈਂਟ ਰਾਹੀਂ ਵਰਤਿਆ ਜਾਣਾ ਚਾਹੀਦਾ ਹੈ) ਛੱਡ ਦਿਉ REORDER_HEADERS,  ਰੇਡੀਉ ਬਟਨ ਹਟਾਉ ਇਸ ਸੰਪਰਕ ਲਈ IPv4 ਪਤਿਆਂ ਦੀ ਲੋੜ ਹੈ ਇਸ ਸੰਪਰਕ ਲਈ IPv6 ਪਤਿਆਂ ਦੀ ਲੋੜ ਹੈ ਰਾਊਂਡ-ਰੌਬਿਨ (ਹਰ ਇੱਕ ਨਾਲ ਵਾਰੀ ਵਾਰੀ ਹਰੇਕ) ਰੂਟਿੰਗ SSID SSID ਲੰਬਾਈ <1-32> ਬਾਈਟਾਂ ਦੀ ਹੱਦ ਤੋਂ ਬਾਹਰ ਹੈ SSID ਜਾਂ BSSID:  ਖੋਜ ਡੋਮੇਨਾਂ ਭੇਦ ਦੀ ਲੋੜ ਸੀ, ਪਰ ਦਿੱਤਾ ਨਹੀਂ ਗਿਆ ਸੁਰੱਖਿਆ ਜਿਸ ਕਿਸਮ ਦਾ ਨੈੱਟਵਰਕ ਤੁਸੀਂ ਬਣਾਉਣਾ ਚਾਹੁੰਦੇ ਹੋ ਉਹ ਚੁਣੋ। ਜਿਹੜਾ ਮਾਤਹਿਤ ਸੰਪਰਕ ਤੁਸੀਂ ਜੋੜਨਾ ਚਾਹੁੰਦੇ ਹੋ ਉਹ ਚੁਣੋ। ਚੁਣੋ... ਸੇਵਾ ਮੇਜਬਾਨ ਨਾਂ ਸੈੱਟ ਕਰੋ ਮੇਜਬਾਨ ਨਾਂ '%s' ਸੈੱਟ ਕਰੋ ਸਿਸਟਮ ਮੇਜਬਾਨ ਨਾਂ ਸੈੱਟ ਕਰੋ ਸੰਪਰਕ ਵਿੱਚ ਸੈਟਿੰਗ '%s' ਮੌਜੂਦ ਨਹੀਂ ਹੈ।
 ਸੈਟਿੰਗ ਨਾਂ? ਸਾਂਝਾ ਸਾਂਝੀ ਚਾਬੀ ਸਾਂਝੀ ਸੰਪਰਕ ਸੇਵਾ ਅਸਫਲ ਹੋਈ ਸਾਂਝੀ ਸੰਪਰਕ ਸੇਵਾ ਸ਼ੁਰੂ ਹੋਣ ਵਿੱਚ ਅਸਫਲ ਵਿਖਾਉ ਗੁਪਤ-ਸ਼ਬਦ ਵਿਖਾਉ ਮਾਤਹਿਤ PID ਫਾਇਲ ਦਾ ਟਿਕਾਣਾ ਦਿਓ ਹਾਲਤ ਫਾਇਲ ਟਿਕਾਣਾ ਜੰਤਰਾਂ ਦੀ ਹਾਲਤ ਸਫ਼ਲ ਸਿਸਟਮ ਪਾਲਸੀ ਨੈੱਟਵਰਕ ਕੁਨੈਕਸ਼ਨ ਕੰਟਰੋਲ ਕਰਨ ਤੋਂ ਸੋਧ ਤੋਂ ਰੋਕਦੀ ਹੈ ਸਿਸਟਮ ਪਾਲਸੀ ਵਾਈ-ਫਾਈ ਜੰਤਰ ਚਾਲੂ ਜਾਂ ਬੰਦ ਕਰਨੋਂ ਰੋਕਦੀ ਹੈ ਸਿਸਟਮ ਪਾਲਸੀ ਵਾਈਮੈਕਸ ਮੋਬਾਇਲ ਬਰਾਡਬੈਂਡ ਜੰਤਰ ਚਾਲੂ ਜਾਂ ਬੰਦ ਕਰਨ ਤੋਂ ਰੋਕਦੀ ਹੈ ਸਿਸਟਮ ਪਾਲਸੀ ਮੋਬਾਇਲ ਬਰਾਡਬੈਂਡ ਜੰਤਰ ਚਾਲੂ ਜਾਂ ਬੰਦ ਕਰਨ ਤੋਂ ਰੋਕਦੀ ਹੈ ਸਿਸਟਮ ਪਾਲਸੀ ਸਿਸਟਮ ਨੈੱਟਵਰਕਿੰਗ ਚਾਲੂ ਜਾਂ ਬੰਦ ਕਰਨ ਤੋਂ ਰੋਕਦੀ ਹੈ ਸਿਸਟਮ ਪਾਲਸੀ ਸਭ ਯੂਜ਼ਰ ਨੂੰ ਨੈੱਟਵਰਕ ਸੈਟਿੰਗ ਲਈ ਸੋਧ ਤੋਂ ਰੋਕਦੀ ਹੈ ਸਿਸਟਮ ਪਾਲਸੀ ਨਿੱਜੀ ਨੈੱਟਵਰਕ ਸੈਟਿੰਗ ਲਈ ਸੋਧ ਤੋਂ ਰੋਕਦੀ ਹੈ ਸਿਸਟਮ ਪਾਲਸੀ ਸਥਿਰ ਸਿਸਟਮ ਹੋਸਟ-ਨਾਂ ਲਈ ਸੋਧਾਂ ਤੋਂ ਰੋਕਦੀ ਹੈ ਸਿਸਟਮ ਪਾਲਸੀ ਨੈੱਟਵਰਕਮੈਨੇਜਰ ਨੂੰ ਸਲੀਪ ਜਾਂ ਵੇਕਅੱਪ ਕਰਨ ਤੋਂ ਰੋਕਦੀ ਹੈ ਸਿਸਟਮ ਪਾਲਸੀ ਕੁਨੈਕਸ਼ਨ ਨੂੰ ਸੁਰੱਖਿਅਤ ਵਾਈ-ਫਾਈ ਨੈੱਟਵਰਕ ਰਾਹੀਂ ਸਾਂਝਾ ਕਰਨ ਤੋਂ ਰੋਕਦੀ ਹੈ ਸਿਸਟਮ ਪਾਲਸੀ ਕੁਨੈਕਸ਼ਨ ਨੂੰ ਓਪਨ ਵਾਈ-ਫਾਈ ਨੈੱਟਵਰਕ ਰਾਹੀਂ ਸਾਂਝਾ ਕਰਨ ਤੋਂ ਰੋਕਦੀ ਹੈ ਟੋਲੀ ਟੋਲੀ ਪੋਰਟ ਟੀਮ %d ਸੰਪਰਕ ਟੀਮ ਕਰੋ ਬਲਿਊਟੁੱਥ ਕੁਨੈਕਸ਼ਨ ਫੇਲ੍ਹ ਹੋਇਆ ਜਾਂ ਸਮਾਂ ਸਮਾਪਤ IP ਸੰਰਚਨਾ ਹੁਣ ਢੁੱਕਵੀਂ ਨਹੀਂ ਹੈ ਵਾਈ-ਫਾਈ ਨੈੱਟਵਰਕ ਲੱਭਿਆ ਨਹੀਂ ਜਾ ਸਕਿਆ ਸੰਪਰਕ ਪਰੋਫਾਈਲ ਕਿਸੇ ਹੋਰ ਕਲਾਈਂਟ ਤੋਂ ਹਟਾਇਆ ਗਿਆ ਹੈ। ਇਸ ਨੂੰ ਮੁੜ ਬਹਾਲ ਕਰਨ ਲਈ ਤੁਸੀਂ ਮੁੱਖ ਮੇਨੂ ਵਿੱਚ 'ਸੰਭਾਲੋ' ਲਿਖ ਸਕਦੇ ਹੋ।
 ਸੰਪਰਕ ਪਰੋਫਾਈਲ ਕਿਸੇ ਹੋਰ ਕਲਾਈਂਟ ਤੋਂ ਹਟਾਇਆ ਗਿਆ ਹੈ। ਇਸ ਨੂੰ ਮੁੜ ਬਹਾਲ ਕਰਨ ਲਈ ਤੁਸੀਂ 'ਸੰਭਾਲੋ' ਲਿਖ ਸਕਦੇ ਹੋ।
 ਜੰਤਰ ਨੂੰ ਸੰਰਚਨਾ ਲਈ ਤਿਆਰ ਨਹੀਂ ਕੀਤ ਜਾ ਸਕਿਆ ਜੰਤਰ ਹਟਾਇਆ ਗਿਆ ਸੀ ਜੰਤਰ ਦਾ ਸਰਗਰਮ ਕੁਨੈਕਸ਼ਨ ਅਲੋਪ ਹੋਇਆ ਜੰਤਰ ਦਾ ਮੌਜੂਦਾ ਕੁਨੈਕਸ਼ਨ ਮੁੜ-ਪ੍ਰਾਪਤ ਕੀਤਾ ਗਿਆ ਡਾਇਲ ਕਰਨ ਦੀ ਕੋਸ਼ਿਸ਼ ਫੇਲ੍ਹ ਹੋਈ ਡਾਇਲ ਕਰਨ ਦੀ ਮੰਗ ਲਈ ਸਮਾਂ ਸਮਾਪਤ ਜਵਾਬ ਲਈ ਲੋੜੀਦੀ ਸ਼ੁਰੂਆਤ ਕੁਨੈਕਟਵਿਟੀ ਚੈੱਕਕ ਰਨ ਲਈ ਅੰਤਰਾਲ (ਸਕਿੰਟਾਂ ਵਿੱਚ) ਲਾਈਨ ਰੁੱਝੀ ਹੋਈ ਹੈ ਮਾਡਮ ਨਹੀਂ ਲੱਭਿਆ ਜਾ ਸਕਿਆ ਸਪਲੀਕੈਂਟ ਹੁਣ ਉਪਲੱਬਧ ਹੈ ਸੰਪਰਕ ਲਈ ਉਡੀਕਣ ਦਾ ਸਮਾਂ, ਸੈਕਿੰਡਾਂ ਵਿੱਚ (ਚੋਣ ਤੋਂ ਬਿਨਾਂ, ਮੂਲ 30 ਹੈ) ਆਵਾਜਾਈ ਮੋਡ ਵੇਰਵੇਵਾਰ ਗੁਣ ਬਿਉਰੇ ਲਈ 'describe [<setting>.<prop>]' ਲਿਖੋ। ਉਪਲੱਬਧ ਕਮਾਂਡਾਂ ਲਈ 'help' ਜਾਂ '?' ਲਿਖੋ। USB ਨਵਾਂ ਸੰਪਰਕ ਜੋੜਨ ਤੋਂ ਅਸਮਰੱਥ: %s ਸੰਪਰਕ ਨੂੰ ਮਿਟਾਉਣ ਤੋਂ ਅਸਮਰੱਥ: %s ਪ੍ਰਾਈਵੇਟ ਕੁੰਜੀ ਕਿਸਮ ਜਾਣਨ ਲਈ ਅਸਮਰੱਥ ਹੈ। ਸੰਪਰਕ ਸੰਭਾਲਣ ਤੋਂ ਅਸਮਰੱਥ: %s ਮੇਜਬਾਨ ਨਾਂ ਸੈੱਟ ਕਰਨ ਤੋਂ ਅਸਮਰੱਥ: %s ਇੰਕ੍ਰਿਪਟ ਕਰਨ ਦੇ ਬਾਅਦ ਅਚਾਨਕ ਅਣਜਾਣ ਮਾਤਰਾ 'ਚ ਡਾਟਾ। ਅਣਜਾਣ ਅਣਪਛਾਤਾ ਕਮਾਂਡ ਆਰਗੂਮੈਂਟ: '%s'
 ਅਣਪਛਾਤੀ ਕਮਾਂਡ: '%s'
 ਅਣਪਛਾਤੀ ਗਲਤੀ ਅਣਜਾਣ ਲਾਗ ਡੋਮੇਨ '%s' ਅਣਜਾਣ ਲਾਗ ਲੈਵਲ '%s' ਅਣਜਾਣ ਪੈਰਾਮੀਟਰ: %s
 ਵਰਤੋਂ ਵਰਤੋਂ: nmcli connection delete { ARGUMENTS | help }

ਆਰਗੂਮੈਂਟ := [id | uuid | path] <ID>

ਇੱਕ ਸੰਪਰਕ ਪਰੋਫਾਈਲ ਮਿਟਾਉ।
ਪਰੋਫਾਈਲ ਆਪਣੇ ਨਾਂ, UUID ਜਾਂ D-Bus ਰਾਹ ਦੁਆਰਾ ਪਛਾਣਿਆ ਗਿਆ ਹੈ।

 ਵਰਤੋਂ: nmcli connection edit { ARGUMENTS | help }

ਆਰਗੂਮੈਂਟ := [id | uuid | path] <ID>

ਇੱਕ ਅੰਤਰ-ਸਰਗਰਮ ਸੰਪਾਦਕ ਵਿੱਚ ਇੱਕ ਮੌਜੂਦਾ ਸੰਪਰਕ ਸੋਧੋ।
ਪਰੋਫਾਈਲ ਆਪਣੇ ਨਾਂ, UUID ਜਾਂ D-Bus ਰਾਹ ਦੁਆਰਾ ਪਛਾਣਿਆ ਗਿਆ ਹੈ।

ਆਰਗੂਮੈਂਟ := [type <new connection type>] [con-name <new connection name>]

ਇੱਕ ਅੰਤਰ-ਸਰਗਰਮ ਸੰਪਾਦਕ ਵਿੱਚ ਇੱਕ ਨਵਾਂ ਸੰਪਰਕ ਜੋੜੋ।

 ਵਰਤੋਂ: nmcli connection load { ARGUMENTS | help }

ਆਰਗੂਮੈਂਟ := <filename> [<filename>...]

ਡਿਸਕ ਤੋਂ ਇੱਕ ਜਾਂ ਜਿਆਦਾ ਸੰਪਰਕ ਫਾਈਲਾਂ ਲੋਡ/ਮੁੜ-ਲੋਡ ਕਰੋ। ਇਸ ਨੂੰ ਸੰਪਰਕ ਫਾਈਲ ਨੂੰ ਦਸਤੀ ਸੋਧ ਕੇ
ਇਹ ਪੱਕਾ ਕਰਨ ਲਈ ਵਰਤੋ ਕਿ ਨੈੱਟਵਰਕ-ਪ੍ਰਬੰਧਕ ਇਸਦੇ ਤਾਜਾ ਹਾਲਾਤ ਤੋਂ
ਜਾਣੂ ਹੈ।

 ਵਰਤੋਂ: nmcli connection reload { help }

ਡਿਸਕ ਤੋਂ ਸਾਰੀਆਂ ਸੰਪਰਕ ਫਾਈਲਾਂ ਮੁੜ ਲੋਡ ਕਰੋ।

 ਵਰਤੋਂ: nmcli device connect { ARGUMENTS | help }

ਆਰਗੂਮੈਂਟ := <ifname>

ਯੰਤਰ ਜੋੜੋ।
ਨੈੱਟਵਰਕ-ਮੈਨੇਜਰ ਇੱਕ ਅਨੁਕੂਲ ਸੰਪਰਕ ਲੱਭਣ ਦੀ ਕੋਸ਼ਿਸ਼ ਕਰੇਗਾ ਜਿਹੜਾ ਕਿ ਕਿਰਿਆਸ਼ੀਲ ਕੀਤਾ ਜਾਵੇਗਾ।
ਇਹ ਉਹ ਸੰਪਰਕ ਵੀ ਵਿਚਾਰੇਗਾ ਜਿਹੜੇ ਸ੍ਵੈ-ਸੰਪਰਕ ਲਈ ਸੈੱਟ ਕੀਤੇ ਹੋਏ ਹਨ।

 ਵਰਤੋਂ: nmcli device show { ARGUMENTS | help }

ਆਰਗੂਮੈਂਟ := [<ifname>]

ਯੰਤਰ(ਯੰਤਰਾਂ) ਦੇ ਵੇਰਵੇ ਵਿਖਾਉ।
ਕਮਾਂਡ ਸਾਰੇ ਯੰਤਰਾਂ, ਜਾਂ ਦਿੱਤੇ ਗਏ ਯੰਤਰ ਲਈ ਵੇਰਵੇ ਵਿਖਾਉਂਦੀ ਹੈ।

 ਵਰਤੋਂ: nmcli device status { help }

ਸਾਰੇ ਯੰਤਰਾਂ ਲਈ ਹਾਲਾਤ ਵਿਖਾਉ।
ਮੂਲ ਤੌਰ ਤੇ, ਹੇਠ ਲਿਖੇ ਕਾਲਮ ਵਿਖਾਏ ਜਾਂਦੇ ਹਨ:
 ਯੰਤਰ     - ਇੰਟਰਫੇਸ ਨਾਂ
 ਕਿਸਮ       - ਯੰਤਰ ਕਿਸਮ
 ਹਾਲਾਤ      - ਯੰਤਰ ਹਾਲਾਤ
 ਸੰਪਰਕ - ਯੰਤਰ ਤੇ ਕਿਰਿਆਸ਼ੀਲ ਕੀਤਾ ਗਿਆ ਸੰਪਰਕ (ਜੇ ਕੋਈ ਹੈ)
ਪਰਦਰਸ਼ਿਤ ਕੀਤੇ ਗਏ ਕਾਲਮ '--fields' ਵਿਆਪਕ ਚੋਣਾਂ ਵਰਤੇ ਜਾ ਸਕਦੇ ਹਨ। 'status' ਮੂਲ
ਕਮਾਂਡ ਹੈ, ਜਿਸਦਾ ਮਤਲਬ ਹੈ 'nmcli device' 'nmcli device status' ਨੂੰ ਬੁਲਾਉਂਦਾ ਹੈ।

 ਵਰਤੋਂ: nmcli general hostname { ARGUMENTS | help }

ਆਰਗੂਮੈਂਟ := [<hostname>]

ਚਿਰਸਥਾਈ ਸਿਸਟਮ ਮੇਜਬਾਨ ਨਾਂ ਪ੍ਰਾਪਤ ਕਰੋ ਜਾਂ ਬਦਲੋ।
ਬਿਨਾਂ ਆਰਗੂਮੈਂਟ ਦੇ, ਇਹ ਮੌਜੂਦਾ ਸੰਰਚਿਤ ਮੇਜਬਾਨ ਨਾਂ ਛਾਪਦਾ ਹੈ। ਜਦੋਂ ਤੁਸੀਂ ਇੱਕ ਮੇਜਬਾਨ ਨਾਂ ਦਿੰਦੇ ਹੋ,
ਨੈੱਟਵਰਕ-ਮੈਨੇਜਰ ਇਸ ਨੂੰ ਇੱਕ ਨਵੇਂ ਚਿਰ-ਸਥਾਈ ਮੇਜਬਾਨ ਨਾਂ ਵਜੋਂ ਸੈੱਟ ਕਰ ਦੇਵੇਗਾ।

 ਵਰਤੋਂ: nmcli general logging { ARGUMENTS | help }

ਆਰਗੂਮੈਂਟ := [level <log level>] [domains <log domains>]

ਨੈੱਟਵਰਕ-ਮੈਨੇਜਰ ਲਾਗਿੰਗ ਪੱਧਰ ਅਤੇ ਡੋਮੇਨਾਂ ਪ੍ਰਾਪਤ ਕਰੋ ਜਾਂ ਬਦਲੋ।
ਬਿਨਾਂ ਕਿਸੇ ਆਰਗੂਮੈਂਟ ਦੇ ਮੌਜੂਦਾ ਲਾਗਿੰਗ ਪੱਧਰ ਅਤੇ ਡੋਮੇਨਾਂ ਵਿਖਾਈਆਂ ਜਾਂਦੀਆਂ ਹਨ। ਲਾਗਿੰਗ ਹਾਲਾਤ
 ਬਦਲਣ ਲਈ, ਪੱਧਰ ਅਤੇ/ਜਾਂ ਡੋਮੇਨ ਮੁਹੱਈਆ ਕਰਵਾਉ। ਸੰਭਵ ਲਾਗਿੰਗ ਡੋਮੇਨਾਂ ਦੀ ਸੂਚੀ ਲਈ ਕਿਰਪਾ ਕਰ ਕੇ
 ਮੁੱਖ ਸਫ੍ਹਾ ਵੇਖੋ।

 ਵਰਤੋਂ: nmcli general permissions { help }

ਪਰਮਾਣਿਕ ਕਾਰਵਾਈਆ ਲਈ ਯੂਜ਼ਰ ਦੀਆਂ ਮਨਜ਼ੂਰੀਆਂ ਵਿਖਾਉ।

 ਵਰਤੋਂ: nmcli general status { help }

ਨੈੱਟਵਰਕ-ਮੈਨੇਜਰ ਦੀ ਲੱਗਭਗ ਸਾਰੀ ਹਾਲਾਤ ਵਿਖਾਉ।
'status' ਮੂਲ ਕਾਰਵਾਈ ਹੈ, ਜਿਸਦਾ ਮਤਲਬ 'nmcli gen status' ਨੂੰ 'nmcli gen' ਬੁਲਾਉਂਦੀ ਹੈ

 ਵਰਤੋਂ: nmcli general { COMMAND | help }

ਕਮਾਂਡ := { status | hostname | permissions | logging }

  status

  hostname [<hostname>]

  permissions

  logging [level <log level>] [domains <log domains>]

 ਵਰਤੋਂ: nmcli networking connectivity { ARGUMENTS | help }

ਆਰਗੂਮੈਂਟ := [check]

ਨੈੱਟਵਰਕ ਸੰਪਰਕ ਹਾਲਾਤ ਪ੍ਰਾਪਤ ਕਰੋ।
ਵਿਕਲਪਿਕ 'check' ਆਰਗੂਮੈਂਟ ਨੈੱਟਵਰਕ-ਮੈਨੇਜਰ ਤੋਂ ਸੰਪਰਕਤਾ ਜਾਂਚਮੁੜ ਕਰਵਾਉਂਦਾ ਹੈ।

 ਵਰਤੋਂ: nmcli networking off { help }

ਨੈੱਟਵਰਕਿੰਗ ਬੰਦ ਕਰੋ।

 ਵਰਤੋਂ: nmcli networking on { help }

ਨੈੱਟਵਰਕਿੰਗ ਚਾਲੂ ਕਰੋ।

 ਵਰਤੋਂ: nmcli networking { COMMAND | help }

ਕਮਾਂਡ := { [ on | off | connectivity ] }

  on

  off

  connectivity [check]

 ਵਰਤੋਂ: nmcli radio all { ARGUMENTS | help }

ਆਰਗੂਮੈਂਟ := [on | off]

ਸਾਰੇ ਰੇਡੀਉ ਬਟਨਾਂ ਦੀ ਹਲਾਤ ਪਤਾ ਕਰੋ, ਜਾਂ ਉਹਨਾਂ ਨੂੰ ਚਾਲੂ/ਬੰਦ ਕਰੋ।

 ਵਰਤੋਂ: nmcli radio wifi { ARGUMENTS | help }

ਆਰਗੂਮੈਂਟ := [on | off]

Wi-Fi ਰੇਡੀਉ ਬਟਨਾਂ ਦੀ ਹਲਾਤ ਪਤਾ ਕਰੋ, ਜਾਂ ਇਸ ਨੂੰ ਚਾਲੂ/ਬੰਦ ਕਰੋ।

 ਵਰਤੋਂ: nmcli radio wwan { ARGUMENTS | help }

ਆਰਗੂਮੈਂਟ := [on | off]

ਮੋਬਾਇਲ ਬਰਾਡਬੈਂਡ ਰੇਡੀਉ ਬਟਨ ਦੀ ਹਲਾਤ ਪ੍ਰਾਪਤ ਕਰੋ, ਜਾਂ ਇਸ ਨੂੰ ਚਾਲੂ/ਬੰਦ ਕਰੋ।

 ਯੂਜ਼ਰ-ਨਾਂ VLAN VLAN ਕੁਨੈਕਸ਼ਨ %d VLAN id VPN VPN ਕੁਨੈਕਟ ਹੈ VPN ਕੁਨੈਕਟ ਕੀਤਾ ਜਾ ਰਿਹਾ ਹੈ VPN ਕੁਨੈਕਟ ਕੀਤਾ ਜਾ ਰਿਹਾ ਹੈ (IP ਸੰਰਚਨਾ ਲਈ ਜਾ ਰਹੀ ਹੈ) VPN ਕੁਨੈਕਟ ਕੀਤਾ ਜਾ ਰਿਹਾ ਹੈ (ਪਰਮਾਣਕਿਤਾ ਦੀ ਲੋੜ ਹੈ) VPN ਕੁਨੈਕਸ਼ਨ (ਤਿਆਰੀ) VPN ਕੁਨੈਕਸ਼ਨ %d VPN ਕੁਨੈਕਸ਼ਨ ਫੇਲ੍ਹ ਹੋਇਆ VPN ਡਿਸ-ਕੁਨੈਕਟ ਢੁਕਵੀਆਂ ਸੰਪਰਕ ਕਿਸਮਾਂ: %s
 ਸੰਪਰਕ ਦੀ ਪੜਤਾਲ ਕਰੋ: %s
 ਸੈਟਿੰਗ '%s' ਦੀ ਪੁਸ਼ਟੀ ਕਰੋ: %s
 WEP WEP 128-bit ਪਛਾਣ-ਵਾਕ WEP 40/128-bit ਚਾਬੀ (Hex ਜਾਂ ASCII) WEP ਇੰਡੈਕਸ 1 (ਮੂਲ) 2 3 4 WI-FI WPA & WPA2 ਉਦਯੋਗ WPA & WPA2 ਨਿੱਜੀ WPA1 WPA2 WWAN ਰੇਡੀਉ ਬਟਨ ਨੈੱਟਵਰਕ ਸੰਪਰਕਾਂ ਦੀ ਸ਼ੁਰੂਆਤ ਨੂੰ ਕਿਰਿਆਸ਼ੀਲਤਾ ਪੂਰੀ ਕਰਨ ਲਈ ਨੈੱਟਵਰਕ-ਮੈਨੇਜਰ ਦੀ ਉਡੀਕ ਕਰਦਾ ਹੈ ਚੇਤਾਵਨੀ: ਮੌਜੂਦਾ ਸੰਪਰਕ '%s' ਨੂੰ ਸੋਧ ਰਿਹਾ; 'con-name' ਆਰਗੂਮੈਂਟ ਨੂੰ ਅਣਗੌਲਿਆ ਕੀਤਾ ਗਿਆ ਹੈ
 ਚੇਤਾਵਨੀ: ਮੌਜੂਦਾ ਸੰਪਰਕ '%s' ਨੂੰ ਸੋਧ ਰਿਹਾ; 'type' ਆਰਗੂਮੈਂਟ ਨੂੰ ਅਣਗੌਲਿਆ ਕੀਤਾ ਗਿਆ ਹੈ
 ਵਾਈ-ਫਾਈ ਸ੍ਵੈ-ਚਲਿਤ ਕਲਾਈਂਟ Wi-Fi ਸੰਪਰਕ %d ਵਾਈ-ਫਾਈ ਰੇਡੀਉ ਬਟਨ ਵਾਈ-ਫਾਈ ਸਕੈਨ ਸੂਚੀ ਕੋਈ ਨਹੀਂ WiMAX ਤਾਰ-ਯੁਕਤ ਤਾਰ-ਯੁਕਤ 802.1X ਪ੍ਰਮਾਣਿਕਤਾ ਤਾਰ ਵਾਲਾ ਕੁਨੈਕਸ਼ਨ %d '%s' ਲਿਖਣ ਲਈ ਫੇਲ੍ਹ: %s
 XOR ਤੁਸੀਂ ਹੇਠਾਂ ਦਿੱਤੇ ਗੁਣ ਸੋਧ ਸਕਦੇ ਹੋ: %s
 ਤੁਸੀਂ ਹੇਠਲੀਆਂ ਸੈਟਿੰਗਾਂ ਸੋਧ ਸਕਦੇ ਹੋ: %s
 ['%s' ਸੈਟਿੰਗ ਮੁੱਲ]
 [NM ਗੁਣ ਵੇਰਵਾ] [nmcli ਸੰਬੰਧਿਤ ਵੇਰਵਾ] ਕਿਰਿਆਸ਼ੀਲ ਕਰੋ [<ifname>] [/<ap>|<nsp>]  :: ਸੰਪਰਕ ਨੂੰ ਕਿਰਿਆਸ਼ੀਲ ਕਰਦਾ ਹੈ

ਸੰਪਰਕ ਨੂੰ ਕਿਰਿਆਸ਼ੀਲ ਕਰਦਾ ਹੈ।

ਉਪਲੱਬਧ ਚੋਣਾਂ:
<ifname>    - ਉਹ ਯੰਤਰ ਜਿਸ ਤੇ ਸੰਪਰਕ ਕਿਰਿਆਸ਼ੀਲ ਕੀਤਾ ਜਾਵੇਗਾ
/<ap>|<nsp> - AP (Wi-Fi) ਜਾਂ NSP (WiMAX) (ਪਹਿਲਾਂ / ਜੋੜੋ ਜਦੋਂ <ifname> ਦਰਸਾਇਆ ਨਹੀਂ ਗਿਆ ਹੈ)
 ਚਾਲੂ ਕੀਤਾ ਚਾਲੂ ਕੀਤਾ ਜਾ ਰਿਹਾ ਹੈ ਮਸ਼ਹੂਰ ਕਰੋ,  ਏਜੰਟ-ਮਲਕੀਅਤ,  asleep ਪਰਮਾਣ ਆਟੋ back  :: ਉੱਪਰਲੇ ਮੇਨੂ ਪੱਧਰ ਤੇ ਜਾਂਦਾ ਹੈ

 ਬੈਂਡਵਿਡਥ ਪ੍ਰਤੀਸ਼ਤਾਂ ਦਾ ਜੋੜ ਲਾਜਮੀ 100%% ਹੋਵੇ ਬਾਈਟਾਂ change  :: ਮੌਜੂਦਾ ਮੁੱਲ ਨੂੰ ਬਦਲੋ

ਮੌਜੂਦਾ ਮੁੱਲ ਪ੍ਰਦਰਸ਼ਿਤ ਕਰਦਾ ਹੈ ਅਤੇ ਇਸ ਦੀ ਸੋਧ ਦੀ ਵੀ ਮਨਜ਼ੂਰੀ ਦਿੰਦਾ ਹੈ।
 ਕੁਨੈਕਟ ਹੋਇਆ ਕੁਨੈਕਟ ਹੈ (ਕੇਵਲ ਲੋਕਲ ਹੀ) ਕੁਨੈਕਟ ਹੈ (ਕੇਵਲ ਸਾਈਟ) ਕੁਨੈਕਟ ਕੀਤਾ ਜਾ ਰਿਹਾ ਹੈ ਕੁਨੈਕਟ ਕੀਤਾ ਜਾ ਰਿਹਾ ਹੈ (IP ਕੁਨੈਕਸ਼ਨ ਜਾਂਚਿਆ ਜਾ ਰਿਹਾ ਹੈ) ਕੁਨੈਕਟ ਕੀਤਾ ਜਾ ਰਿਹਾ ਹੈ (ਸੰਰਚਨਾ ਜਾਰੀ) ਕੁਨੈਕਟ ਕੀਤਾ ਜਾ ਰਿਹਾ ਹੈ (IP ਸੰਰਚਨਾ ਲਈ ਜਾ ਰਹੀ ਹੈ) ਕੁਨੈਕਟ ਕੀਤਾ ਜਾ ਰਿਹਾ ਹੈ (ਪਰਮਾਣਿਕਤਾ ਦੀ ਲੋੜ) ਕੁਨੈਕਟ ਕੀਤਾ ਜਾ ਰਿਹਾ ਹੈ (ਤਿਆਰੀ) ਕੁਨੈਕਟ ਕੀਤਾ ਜਾ ਰਿਹਾ ਹੈ (ਸੈਕੰਡਰੀ ਕੁਨੈਕਸ਼ਨ ਸ਼ੁਰੂ ਕੀਤਾ ਜਾ ਰਿਹਾ ਹੈ) ਸੰਪਰਕ ਸੰਪਰਕ ਅਸਫਲ ਹੋਇਆ ਗੈਰ-ਕਿਰਿਆਸ਼ੀਲ ਕੀਤਾ ਅਕਿਰਿਆਸ਼ੀਲ ਕੀਤਾ ਜਾ ਰਿਹਾ ਹੈ ਮੂਲ describe  :: ਗੁਣ ਵੇਰਵਾ ਦੱਸਦਾ ਹੈ

ਗੁਣ ਦਾ ਵੇਰਵਾ ਦੱਸਦਾ ਹੈ। ਤੁਸੀਂ ਸਾਰੀਆ NM ਸੈਟਿੰਗਾਂ ਅਤੇ ਗੁਣਾਂ ਲਈ nm-settings(5) ਹਦਾਇਤ ਕਿਤਾਬਚਾ ਸਫ੍ਹਾ ਦੇਖ ਸਕਦੇ ਹੋ।
 describe [<setting>.<prop>]  :: ਗੁਣ ਸਮਝਾਉ

ਗੁਣ ਵੇਰਵਾ ਵਿਖਾਉ। ਤੁਸੀਂ ਸਾਰੀਆਂ NM ਸਾਰੀਆਂ ਸੈੱਟਿੰਗਾਂ ਅਤੇ ਗੁਣਾਂ ਲਈ nm-settings(5) ਹਦਾਇਤ ਕਿਤਾਬਚਾ ਵੇਖ ਸਕਦੇ ਹੋ।
 ਜੰਤਰ '%s' ਕੁਨੈਕਸ਼ਨ '%s' ਨਾਲ ਅਨੁਕੂਲ ਨਹੀਂ ਹੈ ਬੰਦ ਹੈ ਡਿਸ-ਕੁਨੈਕਟ ਕੀਤਾ ਡਿਸ-ਕੁਨੈਕਟ ਕੀਤਾ ਜਾ ਰਿਹਾ ਹੈ ਗੁਣ ਦਾ ਮੁੱਲ ਕਿਵੇਂ ਪ੍ਰਾਪਤ ਕਰਨਾ ਹੈ ਪਤਾ ਨਹੀਂ ਤੱਤ ਅਢੁਕਵਾਂ ਚਾਲੂ ਹੈ ਯੋਗ ਕੀਤਾ,  ਖੇਤਰ '%s' ਇਕੱਲਾ ਹੋਵੇਗਾ ਫਲੈਗ ਅਢੁਕਵੇਂ ਹਨ ਫਲੈਗ ਅਢੁਕਵੇਂ ਹਨ ਫਲੈਗ ਅਢੁਕਵੇਂ ਹਨ - ਅਯੋਗ ਕੀਤੇ ਗਏ ਪੂਰਾ goto <setting>[.<prop>] | <prop>  :: ਸੋਧਣ ਲਈ ਸੈਟਿੰਗ/ਗੁਣ ਭਰੋ

ਇਹ ਕਮਾਂਡ ਸੈਟਿੰਗ ਜਾਂ ਗੁਣ ਵਿੱਚ ਉਹਨੂੰ ਸੋਧਣ ਲਈ ਦਾਖਲ ਹੁੰਦੀ ਹੈ।

ਉਦਾਹਰਣਾਂ: nmcli> ਸੰਪਰਕ ਤੇ ਜਾਉ
          nmcli connection> goto secondaries
          nmcli> goto ipv4.addresses
 ਨੂੰ '%s' ਗੁਣ PKCS#12 ਲਈ ਮੇਲ ਕਰਾਉਣਾ ਹੈ help/? [<command>]  :: nmcli ਕਮਾਂਡਾਂ ਲਈ ਮਦਦ

 help/? [<command>]  :: nmcli ਕਮਾਂਡਾਂ ਲਈ ਮਦਦ

 ਇੰਡੈਕਸ '%d' ਹੱਦ <0-%d> ਵਿੱਚ ਨਹੀਂ ਇੰਡੈਕਸ '%d' <0-%d> ਹੱਦ ਦੇ ਅੰਦਰ ਨਹੀਂ ਹੈ ਅਢੁਕਵਾਂ '%s' ਜਾਂ ਇਸਦਾ ਮੁੱਲ '%s' ਅਢੁਕਵਾਂ IPv4 ਐਡਰੈਸ '%s' ਅਢੁਕਵਾਂ IPv4 ਪਤਾ '%s' ਅਢੁਕਵਾਂ IPv6 ਪਤਾ '%s' ਅਢੁਕਵੀਂ ਚੋਣ '%s' ਚੋਣ '%s' ਜਾਂ ਇਸਦਾ ਮੁੱਲ '%s' ਅਢੁਕਵੇਂ ਅਢੁਕਵਾਂ ਤਰਜੀਹ ਨਕਸ਼ਾ '%s' ਇੱਕ ਢੁਕਵਾਂ MAC ਪਤਾ ਨਹੀਂ ਹੈ ਸੀਮਿਤ %s %s ਲਾਜਮੀ ਚੋਣ '%s' ਗੁੰਮ ਹੈ ms ਗੁੰਮ ਨਾਂ, [%s] ਵਿੱਚੋਂ ਇੱਕ ਨਾਲ ਕੋਸ਼ਿਸ਼ ਕਰੋ ਗੁੰਮ ਚੋਣ ਕੌਮਿਆਂ ਨਾਲ ਅੱਡ ਕੀਤੇ 8 ਅੰਕ ਲਾਜਮੀ ਹੋਣ ਨਾ ਤਾਂ ਕੋਈ ਢੁਕਵਾਂ ਸੰਪਰਕ ਤੇ ਨਾ ਹੀ ਯੰਤਰ ਦਿੱਤਾ ਗਿਆ ਹੈ ਕਦੇ ਨਹੀਂ ਨਵਾਂ ਮੇਜਬਾਨ ਨਾਂ nmcli ਦੋਵੇਂ ਹੀ ਸਿੱਧਾ JSON ਸੰਰਚਨਾ ਡਾਟਾ ਅਤੇ ਸੰਰਚਨਾ ਸਮਾਈ ਹੋਈ ਫਾਈਲ ਦਾ ਨਾਂ ਪ੍ਰਵਾਨ ਕਰਦੀ ਹੈ। ਬਾਅਦ ਵਾਲੀ ਹਾਲਾਤ ਵਿੱਚ ਫਾਈਲ ਪੜ੍ਹੀ ਜਾਂਦੀ ਹੈ ਅਤੇ ਇਸ ਦੇ ਅੰਸ਼ ਇਸ ਗੁਣ ਵਿੱਚ ਰੱਖੇ ਜਾਂਦੇ ਹਨ।

ਉਦਾਹਰਣ: set team.config { "device": "team0", "runner": {"name": "roundrobin"}, "ports": {"eth1": {}, "eth2": {}} }
          set team.config /etc/my-team.conf
 nmcli ਟੂਲ, ਵਰਜਨ %s
 ਨਹੀਂ ਜੰਤਰ '%s' ਉੱਤੇ ਕੋਈ ਚਾਲੂ ਕੁਨੈਕਸ਼ਨ ਨਹੀਂ ਹੈ ਕੋਈ ਚਾਲੂ ਕੁਨੈਕਸ਼ਨ ਜਾਂ ਜੰਤਰ ਨਹੀਂ ਹੈ ਕੁਨੈਕਸ਼ਨ '%s' ਲਈ ਕੋਈ ਜੰਤਰ ਨਹੀਂ ਲੱਭਿਆ ਹਟਾਉਣ ਲਈ ਕੋਈ ਆਈਟਮ ਨਹੀਂ ਹਟਾਉਣ ਲਈ ਕੋਈ ਤਰਜੀਹ ਨਹੀਂ ਕੋਈ ਨਹੀਂ ਇੱਕ ਢੁਕਵਾਂ ਇੰਟਰਫੇਸ ਨਾਂ ਨਹੀਂ ਹੈ ਲੋੜੀਂਦਾ ਨਹੀਂ,  ਸੰਭਾਲਿਆ ਨਹੀਂ ਗਿਆ,  ਬੰਦ ਚਾਲੂ '%s' ਅਤੇ '%s' ਵਿੱਚੋਂ ਸਿਰਫ ਇੱਕ ਹੀ ਸੈੱਟ ਕੀਤਾ ਜਾ ਸਕਦਾ ਹੈ ਪੋਰਟਲ ਤਿਆਰ ਹੋ ਰਿਹਾ print [all]  :: ਸੈਟਿੰਗ ਜਾਂ ਸੰਪਰਕ ਮੁੱਲ ਛਾਪੋ

ਮੌਜੂਦਾ ਗੁਣ ਜਾਂ ਸਾਰਾ ਸੰਪਰਕ ਵਿਖਾਉਂਦਾ ਹੈ।

ਉਦਾਹਰਣ: nmcli ipv4> print all
 print [property|setting|connection]  :: print property (ਸੈਟਿੰਗ, ਸੰਪਰਕ) ਮੁੱਲ

ਗੁਣ ਮੁੱਲ ਵਿਖਾਉਂਦਾ ਹੈ। ਇੱਕ ਆਰਗੂਮੈਂਟ ਮੁਹੱਈਆ ਕਰਵਾਉਣ ਨਾਲ ਤੁਸੀ ਸਾਰੀ ਸੈਟਿੰਗ ਜਾਂ ਸੰਪਰਕ ਲਈ ਮੁੱਲ ਪ੍ਰਦਰਸ਼ਿਤ ਕਰ ਸਕਦੇ ਹੋ।
 ਤਰਜੀਹ '%s' ਢੁਕਵੀਂ ਨਹੀਂ ਹੈ (<0-%ld>) ਗੁਣ ਅਢੁਕਵਾਂ ਗੁਣ ਅਢੁਕਵਾਂ (ਯੋਗ ਨਹੀਂ ਕੀਤਾ ਗਿਆ) ਗੁਣ ਖਾਲੀ ਹੈ ਗੁਣ ਅਢੁਕਵਾਂ ਹੈ ਗੁਣ ਗੁੰਮ ਹੈ ਗੁਣ ਦਰਸਾਇਆ ਨਹੀਂ ਗਿਆ ਹੈ ਅਤੇ ਨਾ ਹੀ '%s:%s' ਗੁਣ ਗੁੰਮ ਗੁਣ ਮੁੱਲ '%s' ਖਾਲੀ ਹੈ ਜਾਂ ਬਹੁਤ ਲੰਬਾ ਹੈ (>64) quit  :: nmcli ਤੋਂ ਬਾਹਰ ਹੋਵੋ

ਇਹ ਕਮਾਂਡ nmcli ਤੋਂ ਬਾਹਰ ਕੱਢ ਦਿੰਦੀ ਹੈ। ਜਦੋਂ ਸੋਧਿਆ ਜਾਣ ਵਾਲਾ ਸੰਪਰਕ ਸੰਭਾਲਿਆ ਨਹੀਂ ਗਿਆ ਹੁੰਦਾ, ਯੂਜ਼ਰ ਨੂੰ ਕਾਰਵਾਈ ਦੀ ਪੁਸ਼ਟੀ ਕਰਨ ਲਈ ਕਿਹਾ ਜਾਂਦਾ ਹੈ।
 remove <setting>[.<prop>]  :: ਸੈਟਿੰਗ ਹਟਾਉ ਜਾਂ ਗੁਣ ਮੁੱਲ ਨੂੰ ਮੁੜ-ਸੈੱਟ ਕਰੋ

ਇਹ ਕਮਾਂਡ ਇੱਕ ਪੂਰੀ ਸੈਟਿੰਗ ਨੂੰ ਇੱਕ ਵਿੱਚੋਂ ਸੰਪਰਕ ਹਟਾ ਦਿੰਦੀ ਹੈ, ਜਾਂ ਫਿਰ ਜੇ ਇੱਕ ਗੁਣ
ਦਿੱਤਾ ਹੋਇਆ ਹੈ, ਉਸ ਗੁਣ ਨੂੰ ਉਸਦੇ ਮੂਲ ਮੁੱਲ ਤੇ ਮੁੜ-ਸੈੱਟ ਕਰ ਦਿੰਦੀ ਹੈ।

ਉਦਾਹਰਣਾਂ: nmcli> remove wifi-sec
          nmcli> remove eth.mtu
 ਸੈਟਿੰਗ '%s' ਜਾਂ '%s' ਲੋੜੀਂਦੀ ਹੈ ਨੂੰ ਸੰਪਰਕ ਵਿੱਚ '%s' ਸੈਟਿੰਗ ਦੀ ਮੌਜੂਦਗੀ ਲੋੜੀਂਦੀ ਹੈ '%s' ਗੁਣ ਨੂੰ ਸੈੱਟ ਕਰਨਾ ਲੋੜੀਂਦਾ ਹੈ ਚੱਲ ਰਿਹਾ ਹੈ ਸੈਕਿੰਡ set [<setting>.<prop> <value>]  :: ਗੁਣ ਦਾ ਮੁੱਲ ਸੈੱਟ ਕਰੋ

ਇਹ ਕਮਾਂਡ ਗੁਣ ਮੁੱਲ ਸੈੱਟ ਕਰਦੀ ਹੈ।

ਉਦਾਹਰਣਾਂ: nmcli> set con.id ਮੇਰਾ ਸੰਪਰਕ
 set [<value>]  :: ਨਵਾਂ ਮੁੱਲ ਸੈੱਟ ਕਰੋ

ਇਹ ਕਮਾਂਡ ਮੁਹੱਈਆ ਕੀਤੇ ਗਏ <value> ਨੂੰ ਇਸ ਗੁਣ ਤੇ ਸੈੱਟ ਕਰਦੀ ਹੈ
 ਇਸ ਗੁਣ ਨੂੰ ਸੈੱਟ ਕਰਨ ਲਈ '%s' ਗੁਣ ਦਾ ਗੈਰ-ਸਿਫਰ ਹੋਣਾ ਲੋੜੀਂਦਾ ਹੈ ਸ਼ੁਰੂ ਹੋਇਆ ਸ਼ੁਰੂ ਕਰ ਰਿਹਾ ਕੁੱਲ 100% ਨਹੀਂ teamd ਕੰਟਰੋਲ ਅਸਫਲ ਹੋਇਆ ਗੁਣ ਬਦਲਿਆ ਨਹੀਂ ਜਾ ਸਕਦਾ ਇਹ ਗੁਣ '%s=%s' ਲਈ ਮਨਜ਼ੂਰ ਨਹੀਂ ਹੈ ਨਾ-ਉਪਲੱਬਧ ਅਣਜਾਣ ਅਣਪਛਾਤਾ ਯੰਤਰ '%s'। ਬਿਨ-ਪਰਬੰਧ ਪਹਿਲਾਂ 'goto <setting>' ਵਰਤੋ, ਜਾਂ 'describe <setting>.<property>'
 ਪਹਿਲਾਂ 'goto <setting>' ਵਰਤੋ, ਜਾਂ 'set <setting>.<property>'
 ਮੁੱਲ '%d' ਹੱਦ <%d-%d> ਤੋਂ ਬਾਹਰ ਹੈ ਰਜ਼ਾਮੰਦ,  ਹਾਂ 