��          �      �           	  +     .   K  6   z  *   �  #   �  &      /   '  2   W  :   �  -   �  $   �  '        @     U     p     �  #   �     �     �     �       �       �  �   �  �   S  z   �  G   R  3   �  K   �  _   	  p   z	  m   �	  ]   Y
  <   �
  Z   �
     O     c     w     �  -   �  #   �     �                         
                                          	                                                @action:buttonCommit @info:statusAdded files to SVN repository. @info:statusAdding files to SVN repository... @info:statusAdding of files to SVN repository failed. @info:statusCommit of SVN changes failed. @info:statusCommitted SVN changes. @info:statusCommitting SVN changes... @info:statusRemoved files from SVN repository. @info:statusRemoving files from SVN repository... @info:statusRemoving of files from SVN repository failed. @info:statusUpdate of SVN repository failed. @info:statusUpdated SVN repository. @info:statusUpdating SVN repository... @item:inmenuSVN Add @item:inmenuSVN Commit... @item:inmenuSVN Delete @item:inmenuSVN Update @item:inmenuShow Local SVN Changes @item:inmenuShow SVN Updates @labelDescription: @title:windowSVN Commit Show updates Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2010-12-26 09:16+0530
Last-Translator: A S Alam <aalam@users.sf.net>
Language-Team: Punjabi/Panjabi <punjabi-users@lists.sf.net>
Language: pa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.1
Plural-Forms: nplurals=2; plural=n != 1;
 ਕਮਿਟ ਫਾਇਲਾਂ SVN ਰਿਪੋਜ਼ਟਰੀ ਵਿੱਚ ਸ਼ਾਮਲ ਕੀਤੀਆਂ ਜਾ ਰਹੀਆਂ ਹਨ। ਫਾਇਲਾਂ SVN ਰਿਪੋਜ਼ਟਰੀ ਵਿੱਚ ਸ਼ਾਮਲ ਕੀਤੀਆਂ ਜਾ ਰਹੀਆਂ ਹਨ... ਫਾਇਲਾਂ SVN ਰਿਪੋਜ਼ਟਰੀ ਵਿੱਚ ਸ਼ਾਮਲ ਕਰਨ ਲਈ ਫੇਲ੍ਹ ਹੈ। SVN ਬਦਲਾਅ ਕਮਿੱਟ ਕਰਨ ਲਈ ਫੇਲ੍ਹ। SVN ਬਦਲਾਅ ਕਮਿੱਟ ਕੀਤੇ। SVN ਬਦਲਾਅ ਕਮਿੱਟ ਕੀਤੇ ਜਾ ਰਹੇ ਹਨ... SVN ਰਿਪੋਜ਼ਟਰੀ ਤੋਂ ਫਾਇਲਾਂ ਹਟਾਈਆਂ ਗਈਆਂ। SVN ਰਿਪੋਜ਼ਟਰੀ ਤੋਂ ਫਾਇਲਾਂ ਹਟਾਈਆਂ ਜਾ ਰਹੀਆਂ ਹਨ... SVN ਰਿਪੋਜ਼ਟਰੀ ਤੋਂ ਫਾਇਲਾਂ ਹਟਾਉਣ ਲਈ ਫੇਲ੍ਹ ਹੈ। SVN ਰਿਪੋਜ਼ਟਰੀ ਅੱਪਡੇਟ ਕਰਨ ਲਈ ਫੇਲ੍ਹ ਹੈ। SVN ਰਿਪੋਜ਼ਟਰੀ ਅੱਪਡੇਟ ਹੈ। SVN ਰਿਪੋਜ਼ਟਰੀ ਅੱਪਡੇਟ ਕੀਤੀ ਜਾ ਰਹੀ ਹੈ... SVN ਸ਼ਾਮਲ SVN ਕਮਿਟ... SVN ਹਟਾਓ SVN ਅੱਪਡੇਟ ਲੋਕਲ SVN ਬਦਲਾਅ ਵੇਖੋ SVN ਅੱਪਡੇਟ ਵੇਖੋ ਵੇਰਵਾ: SVN ਕਮਿੱਟ  ਅੱਪਡੇਟ ਵੇਖੋ 