��          �      <      �     �     �     �     �     �      �  	        &     E  &   _     �     �     �  �   �  7   Y  +   �     �  �  �     i     w     �  #   �  3   �             ,   ,  ;   Y  ^   �  G   �  T   <  4   �  A  �     	  c   &	     �	                                         
   	                                                          min Act like Always Define a special behavior Don't use special settings EMAIL OF TRANSLATORSYour emails Hibernate NAME OF TRANSLATORSYour names Never shutdown the screen Never suspend or shutdown the computer PC running on AC power PC running on battery power PC running on low battery The Power Management Service appears not to be running.
This can be solved by starting or scheduling it inside "Startup and Shutdown" This is meant to be: Act like activity %1Activity "%1" Use separate settings (advanced users only) after Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-09 03:03+0200
PO-Revision-Date: 2013-01-05 08:18+0530
Last-Translator: A S Alam <aalam@users.sf.net>
Language-Team: Punjabi/Panjabi <punjabi-users@lists.sf.net>
Language: pa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
  ਮਿੰਟ ਇੰਝ ਕੰਮ ਕਰੋ ਹਮੇਸ਼ਾ ਖਾਸ ਰਵੱਈਆ ਦਿਓ ਖਾਸ ਸੈਟਿੰਗ ਨਾ ਵਰਤੋਂ aalam@users.sf.net ਹਾਈਬਰਨੇਟ ਅਮਨਪਰੀਤ ਸਿੰਘ ਆਲਮ ਸਕਰੀਨ ਕਦੇ ਵੀ ਬੰਦ ਨਾ ਕਰੋ ਕੰਪਿਊਟਰ ਕਦੇ ਵੀ ਸਸਪੈਂਡ ਜਾਂ ਬੰਦ ਨਾ ਕਰੋ ਪੀਸੀ AC ਪਾਵਰ ਉੱਤੇ ਚੱਲ ਰਿਹਾ ਹੈ ਪੀਸੀ ਬੈਟਰੀ ਪਾਵਰ ਉੱਤੇ ਚੱਲ ਰਿਹਾ ਹੈ ਪੀਸੀ ਦੀ ਬੈਟਰੀ ਘੱਟ ਹੈ ਪਾਵਰ ਮੈਨੇਜਮੈਂਟ ਸਰਵਿਸ ਚੱਲਦੀ ਨਹੀਂ ਜਾਪਦੀ ਹੈ।
ਇਸ ਨੂੰ ਸ਼ੁਰੂ ਕਰਕੇ ਜਾਂ "ਸਟਾਰਟਅੱਪ ਅਤੇ ਸੱਟਡਾਊਨ" ਵਿੱਚ ਸੈਡਿਊਲ ਕਰਕੇ ਹੱਲ ਕੀਤਾ ਜਾ ਸਕਦਾ ਹੈ ਐਕਟੀਵਿਟੀ "%1" ਵੱਖ-ਵੱਖ ਸੈਟਿੰਗ ਵਰਤੋਂ (ਮਾਹਰ ਯੂਜ਼ਰ ਲਈ ਹੀ) ਬਾਅਦ 