��    ,      |  ;   �      �     �     �  .   �  5        7     >     N     T  	   a     k     �     �     �  1   �     �     �                  '   0     X     [     d  '   k     �     �     �     �  5   �     �          
             $   ,  A   Q  �   �     y     �  C   �  '   �     �       �  "     �	     �	  &   �	  "   �	     
  *   ,
     W
  '   k
     �
  <   �
  &   �
  (   
  &   3  �   Z     �  &   �  O        d  D   �     �     �     �      �  b      "   �     �      �     �  n   �  6   W     �     �  #   �  /   �            7   ;     s  3   �  .   �     �  "   �                  #                    '                +   $       &      )                	                                    !   (      "                %                                    
   *       ,        %1% Back Button to change keyboard layoutSwitch layout Button to show/hide virtual keyboardVirtual Keyboard Cancel Caps Lock is on Close Close Search Configure Configure Search Plugins Desktop Session: %1 Different User Keyboard Layout: %1 Logging out in 1 second Logging out in %1 seconds Login Login Failed Login as different user Logout No media playing Nobody logged in on that sessionUnused OK Password Reboot Reboot in 1 second Reboot in %1 seconds Recent Queries Remove Restart Shutdown Shutting down in 1 second Shutting down in %1 seconds Start New Session Suspend Switch Switch Session Switch User Textfield placeholder textSearch... Textfield placeholder text, query specific KRunnerSearch '%1'... This is the first text the user sees while starting in the splash screen, should be translated as something short, is a form that can be seen on a product. Plasma is the project name so shouldn't be translated.Plasma made by KDE Unlock Unlocking failed User logged in on console (X display number)on TTY %1 (Display %2) User logged in on console numberTTY %1 Username Username (location)%1 (%2) Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-09 03:21+0100
PO-Revision-Date: 2017-04-09 09:48-0600
Last-Translator: A S Alam <aalam@users.sf.net>
Language-Team: Punjabi <kde-i18n-doc@kde.org>
Language: pa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 %1% ਪਿੱਛੇ ਲੇਆਉਟ ਨੂੰ ਬਦਲੋ ਫ਼ਰਜ਼ੀ ਕੀਬੋਰਡ ਰੱਦ ਕਰੋ ਕੈਪਸ ਲਾਕ ਚਾਲੂ ਹੈ ਬੰਦ ਕਰੋ ਖੋਜ ਨੂੰ ਬੰਦ ਕਰੋ ਸੰਰਚਨਾ ਖੋਜ ਪਲੱਗਇਨਾਂ ਦੀ ਸੰਰਚਨਾ ਡੈਸਕਟਾਪ ਸ਼ੈਸ਼ਨ: %1 ਵੱਖਰਾ ਵਰਤੋਂਕਾਰ ਕੀਬੋਰਡ ਲੇਆਉਟ: %1 1 ਸਕਿੰਟ 'ਚ ਲਾਗ ਆਉਟ ਹੋਵੇਗਾ %1 ਸਕਿੰਟਾਂ 'ਚ ਲਾਗ ਆਉਟ ਹੋਵੇਗਾ ਲਾਗਇਨ ਲਾਗਇਨ ਫੇਲ੍ਹ ਹੈ ਵੱਖਰੇ ਵਰਤੋਂਕਾਰ ਵਜੋਂ ਲਾਗਇਨ ਕਰੋ ਲਾਗ ਆਉਟ ਕਰੋ ਕੋਈ ਮੀਡੀਆ ਨਹੀਂ ਚੱਲ ਰਿਹਾ ਹੈ ਨਾ-ਵਰਤੇ ਠੀਕ ਹੈ ਪਾਸਵਰਡ ਮੁੜ-ਚਾਲੂ ਕਰੋ 1 ਸਕਿੰਟ 'ਚ ਮੁੜ ਚਾਲੂ %1 ਸਕਿੰਟਾਂ 'ਚ ਮੁੜ-ਚਾਲੂ ਤਾਜ਼ਾ ਕਿਊਰੀਆਂ ਹਟਾਓ ਮੁੜ-ਚਾਲੂ ਕਰੋ ਬੰਦ ਕਰੋ 1 ਸਕਿੰਟ 'ਚ ਬੰਦ ਹੋਵੇਗਾ %1 ਸਕਿੰਟਾਂ 'ਚ ਬੰਦ ਹੋਵੇਗਾ ਨਵਾਂ ਸ਼ੈਸ਼ਨ ਸ਼ੁਰੂ ਕਰੋ ਸਸਪੈਂਡ ਕਰੋ ਬਦਲੋ ਸ਼ੈਸ਼ਨ ਨੂੰ ਬਦਲੋ ਵਰਤੋਂਕਾਰ ਨੂੰ ਬਦਲੋ ...ਖੋਜੋ '%1' ਨੂੰ ਖੋਜੋ... ਪਲਾਜ਼ਮਾ ਨੂੰ KDE ਨੇ ਬਣਾਇਆ ਅਣ-ਲਾਕ ਕਰੋ ਅਣਲਾਕ ਕਰਨਾ ਫੇਲ੍ਹ ਹੈ TTY %1 ਉੱਤੇ (ਡਿਸਪਲੇਅ %2) TTY %1 ਵਰਤੋਂਕਾਰ ਨਾਂ %1 (%2) 