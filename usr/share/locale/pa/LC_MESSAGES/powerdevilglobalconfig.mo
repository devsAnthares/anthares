��          �      �       0     1  *   3  >   ^  9   �     �     �  
   	        	   5     ?     Q  �   p  �  �     �  8   �  �   �  �   Z  7   �  /        F     a     t  &   �  ,   �  A  �                                      
                	    % <b>Battery Levels                     </b> Battery will be considered critical when it reaches this level Battery will be considered low when it reaches this level Configure Notifications... Critical battery level Do nothing EMAIL OF TRANSLATORSYour emails Hibernate Low battery level NAME OF TRANSLATORSYour names The Power Management Service appears not to be running.
This can be solved by starting or scheduling it inside "Startup and Shutdown" Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-11 03:05+0200
PO-Revision-Date: 2014-07-08 16:03-0500
Last-Translator: A S Alam <aalam@users.sf.net>
Language-Team: Punjabi/Panjabi <punjabi-users@lists.sf.net>
Language: pa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 % <b>ਬੈਟਰੀ ਪੱਧਰ                     </b> ਬੈਟਰੀ ਨੂੰ ਨਾਜ਼ੁਕ ਮੰਨਿਆ ਜਾਵੇਗਾ, ਜੇ ਇਹ ਪੱਧਰ ਅੱਪੜ ਗਿਆ ਬੈਟਰੀ ਨੂੰ ਘੱਟ ਮੰਨਿਆ ਜਾਵੇਗਾ, ਜੇ ਇਹ ਪੱਧਰ ਤੱਕ ਅੱਪੜ ਗਿਆ ਨੋਟੀਫਿਕੇਸ਼ਨ ਸੰਰਚਨਾ... ਨਾਜ਼ੁਕ ਬੈਟਰੀ ਪੱਧਰ ਕੁਝ ਨਾ ਕਰੋ aalam@users.sf.net ਹਾਈਬਰਨੇਟ ਘੱਟ ਬੈਟਰੀ ਪੱਧਰ ਅਮਨਪਰੀਤ ਸਿੰਘ ਆਲਮ ਪਾਵਰ ਮੈਨੇਜਮੈਂਟ ਸਰਵਿਸ ਚੱਲਦੀ ਨਹੀਂ ਜਾਪਦੀ ਹੈ।
ਇਸ ਨੂੰ ਸ਼ੁਰੂ ਕਰਕੇ ਜਾਂ "ਸਟਾਰਟਅੱਪ ਅਤੇ ਸੱਟਡਾਊਨ" ਵਿੱਚ ਸੈਡਿਊਲ ਕਰਕੇ ਹੱਲ ਕੀਤਾ ਜਾ ਸਕਦਾ ਹੈ 