��            )   �      �  9   �     �     �     �          -     H  7   _  	   �     �     �     �     �     �     �     	          %     ,     C  #   P  %   t     �  5   �     �     �     
     $  )   ,  �  V     	  :   &  +   a  '   �  Z   �  4     #   E     i     �  $   �  /   �  -   �     	  <   7	     t	     �	     �	     �	  6   �	  *   �	     "
     &
  9   *
  �   d
  +   �
  +   %     Q     V     g                                                                                                             
                        	    %1 is remaining time, %2 is percentage%1 Remaining (%2%) %1% Battery Remaining %1%. Charging %1%. Plugged in %1%. Plugged in, not Charging &Configure Power Saving... Battery and Brightness Battery is currently not present in the bayNot present Capacity: Charging Configure Power Saving... Discharging Display Brightness Enable Power Management Fully Charged General Keyboard Brightness Model: No Batteries Available Not Charging Placeholder is battery capacity%1% Placeholder is battery percentage%1% Power management is disabled The battery applet has enabled system-wide inhibition Time To Empty: Time To Full: Used for measurement100% Vendor: battery percentage below battery icon%1% Project-Id-Version: plasma_applet_battery
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-03 03:15+0100
PO-Revision-Date: 2016-08-08 15:52-0600
Last-Translator: A S Alam <aalam@users.sf.net>
Language-Team: Punjabi <punjabi-users@lists.sf.net>
Language: pa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 %1 ਬਾਕੀ ਹੈ (%2%) %1% ਬੈਟਰੀ ਬਾਕੀ ਰਹਿੰਦੀ ਹੈ %1%। ਚਾਰਜ ਹੋ ਰਹੀ ਹੈ %1%। ਪਲੱਗ ਲੱਗਾ ਹੈ %1%। ਪਲੱਗ ਲੱਗਾ ਹੈ, ਚਾਰਜ ਨਹੀਂ ਹੋ ਰਹੀ ਹੈ ਪਾਵਰ ਬੱਚਤ ਸੰਰਚਨਾ(&C)...  ਬੈਟਰੀ ਅਤੇ ਚਮਕ ਮੌਜੂਦ ਨਹੀਂ ਸਮੱਰਥਾ: ਚਾਰਜ ਹੋ ਰਹੀ ਹੈ ਪਾਵਰ ਬੱਚਤ ਸੰਰਚਨਾ... ਡਿਸਚਾਰਜ ਹੋ ਰਹੀ ਹੈ ਸਕਰੀਨ ਚਮਕ ਪਾਵਰ ਮੈਨਜੇਮੈਂਟ ਚਾਲੂ ਹੈ ਪੂਰੀ ਚਾਰਜ ਆਮ ਕੀਬੋਰਡ ਚਮਕ ਮਾਡਲ: ਕੋਈ ਬੈਟਰੀ ਮੌਜੂਦ ਨਹੀਂ ਚਾਰਜ ਨਹੀਂ ਹੋ ਰਹੀ %1% %1% ਪਾਵਰ ਮੈਨਜੇਮੈਂਟ ਬੰਦ ਹੈ ਬੈਟਰੀ ਐਪਲਿਟ ਨੇ ਪੂਰੇ ਸਿਸਟਮ ਲਈ ਇਨਹੈਬਟੇਸ਼ਨ ਚਾਲੂ ਕਰ ਦਿੱਤੀ ਹੈ ਖਾਲੀ ਹੋਣ ਲਈ ਸਮਾਂ: ਪੂਰਾ ਹੋਣ ਲਈ ਸਮਾਂ: 100% ਵੇਂਡਰ: %1% 