��    f      L  �   |      �  %   �  $   �     �     �     		     	     4	  
   <	     G	     X	     g	     ~	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
     
     
     +
  	   0
     :
     B
     S
  
   c
     n
     �
     �
     �
     �
  
   �
     �
     �
     �
  
   �
     �
     �
                    *  
   1     <     Q     g     |     �  	   �     �     �     �     �     �     �  	   �     �            	   )  
   3     >  	   C     M     \     d     r     x     �     �     �     �     �     �     �  
   �     �     �     �          	       
   !  	   ,     6     >     N     [     j     q     �     �     �     �     �          %  �  :  #   �            "   ;  +   ^      �     �     �  "   �     �  ,        8  "   X  )   {     �     �  	   �  )   �     �  (   �  9   $     ^  	   ~  "   �     �     �     �  "   �  "   �     "  '   6     ^  &   q     �     �     �      �     �          +  (   B  ,   k     �  )   �     �     �     �  $   
  %   /  $   U     z     �     �  #   �  )   �  3        <     S     c     t  %   �  %   �  (   �     �          &     -  ,   M     z     �     �     �     �     �     �               .     H     U     l  6   �     �     �     �     �          6     M     Z     t     �     �     �     �     �     �     �     �     �     �        Y   ]             O       8   >       ;   J   D   a   f   [   F                E          5             L      @   P              3   X      '      :   _   
   c   =   B   (   Z   d       R   S   $                    G   !       0      U                  ^   V   .      ,      9          e       6      +           1   Q       `   K   7       "   2      W       &   b   N   *                                    )   <       ?      -   \   	   %   A       /   I   T      C       #       M           4   H            %1 is a host nameMessage from %1:
%2 @item sensor descriptionSystem Load Active Devices Active Memory Application Memory Average CPU Temperature Battery Battery %1 Battery Capacity Battery Charge Battery Discharge Rate Battery Usage Battery Voltage Buffered Memory CPU %1 CPU Load CPU LoadLoad Cached Memory Change Clock Frequency Connection to %1 refused Cooling Device Cores Current State Data Data Rate Disk %1 Disk Information Disk Throughput Error Rate Error for host %1: %2 Errors Failed Devices Fan Fan %1 File Pages Frame Error Rate Free Inodes Free Memory Free Space Hardware Sensors Host %1 not found Idling Inactive Memory Inode Level Int %1 Interfaces Load Average (1 min) Load Average (15 min) Load Average (5 min) Memory Network Nice Load Number of Blocks Number of Devices Number of Raid Devices Packet Rate Packets Pages In Pages Out Partition Usage Physical Memory Process Controller Processes Processors Rate Read Data Remaining Time Sockets Spare Devices State Swap Memory System System Calls Table Temperature Temperature %1 Thermal Zone Total Total Load Total Number Total Number of Devices Traps Uptime Used Inodes Used Memory Used Space User Load Waiting Working Devices Written Data a percentage% kBytes the frequency unitMHz the unit 1 per second1/s the unit milliampere hoursmAh the unit milliamperesmA the unit millivoltsmV the unit milliwatt hoursmWh the unit milliwattsmW the unit minutesmin Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2013-04-22 07:34+0530
Last-Translator: A S Alam <aalam@users.sf.net>
Language-Team: Punjabi/Panjabi <punjabi-users@lists.sf.net>
Language: pa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 %1 ਤੋਂ ਸੁਨੇਹਾ:
%2 ਸਿਸਟਮ ਲੋਡ ਸਰਗਰਮ ਜੰਤਰ ਐਕਟਿਵ ਮੈਮੋਰੀ ਐਪਲੀਕੇਸ਼ਨ ਮੈਮੋਰੀ ਔਸਤ CPU ਤਾਪਮਾਨ ਬੈਟਰੀ ਬੈਟਰੀ %1 ਬੈਟਰੀ ਸਮਰੱਥਾ ਬੈਟਰ ਚਾਰਜ ਬੈਟਰੀ ਡਿਸਚਾਰਜ ਦਰ ਬੈਟਰੀ ਵਰਤੋਂ ਬੈਟਰੀ ਵੋਲਟੇਜ਼ ਬਫ਼ਰ ਕੀਤੀ ਮੈਮੋਰੀ CPU %1 CPU ਲੋਡ ਲੋਡ ਕੈਸ਼ ਕੀਤੀ ਮੈਮੋਰੀ ਬਦਲੋ ਕਲਾਕ ਫਰੀਕਿਊਂਸੀ %1 ਨਾਲ ਕੁਨੈਕਸ਼ਨ ਤੋਂ ਨਾਂਹ ਕੂਲਿੰਗ ਜੰਤਰ ਕੋਰ ਮੌਜੂਦਾ ਸਥਿਤੀ ਡਾਟਾ ਡਾਟਾ ਦਰ ਡਿਸਕ %1 ਡਿਸਕ ਜਾਣਕਾਰੀ ਡਿਸਕ ਥਰੂਪੁੱਟ ਗਲਤੀ ਦਰ ਹੋਸਟ %1 ਲਈ ਗਲਤੀ: %2 ਗਲਤੀਆਂ ਫੇਲ੍ਹ ਹੋਏ ਜੰਤਰ ਪੱਖਾ ਪੱਖਾ %1 ਫਾਈਲ ਪੇਜ਼ ਫਰੇਮ ਗਲਤੀ ਦਰ ਖਾਲੀ ਆਈ-ਨੋਡ ਖਾਲੀ ਮੈਮੋਰੀ ਖਾਲੀ ਥਾਂ ਹਾਰਡਵੇਅਰ ਸੈਂਸਰ ਹੋਸਟ %1 ਨਹੀਂ ਲੱਭਿਆ ਵੇਹਲ ਨਾ-ਸਰਗਰਮ ਮੈਮੋਰੀ ਆਈ-ਨੋਲ ਪੱਧਰ Int %1 ਇੰਟਰਫੇਸ ਲੋਡ ਔਸਤ (1 ਮਿੰਟ) ਲੋਡ ਔਸਤ (15 ਮਿੰਟ) ਲੋਡ ਔਸਤ (5 ਮਿੰਟ) ਮੈਮੋਰੀ ਨੈੱਟਵਰਕ ਨਾਈਸ ਲੋਡ ਬਲਾਕ ਦੀ ਗਿਣਤੀ ਜੰਤਰਾਂ ਦੀ ਗਿਣਤੀ ਰੇਡ ਜੰਤਰਾਂ ਦੀ ਗਿਣਤੀ ਪੈਕੇਟ ਦਰ ਪੈਕੇਟ ਪੇਜ਼ ਇਨ ਪੇਜ਼ ਆਉਟ ਪਾਰਟੀਸ਼ਨ ਵਰਤੋਂ ਫਿਜ਼ੀਕਲ ਮੈਮੋਰੀ ਪਰੋਸੈਸ ਕੰਟਰੋਲਰ ਪਰੋਸੈਸ ਪਰੋਸੈਸਰ ਦਰ ਪੜ੍ਹਿਆ ਡਾਟਾ ਬਾਕੀ ਰਹਿੰਦਾ ਸਮਾਂ ਸਾਕਟ ਵਾਧੂ ਜੰਤਰ ਹਾਲਤ ਸਵੈਪ ਮੈਮੋਰੀ ਸਿਸਟਮ ਸਿਸਟਮ ਕਾਲ ਟੇਬਲ ਤਾਪਮਾਨ ਤਾਪਮਾਨ %1 ਥਰਮਲ ਖੇਤਰ ਕੁੱਲ ਕੁੱਲ ਲੋਡ ਕੁੱਲ ਨੰਬਰ ਜੰਤਰਾਂ ਦੀ ਕੁੱਲ ਗਿਣਤੀ ਟਰੈਪ ਅੱਪ-ਟਾਈਪ ਵਰਤੇ ਆਈ-ਨੋਡ ਵਰਤੀ ਮੈਮੋਰੀ ਵਰਤੀ ਥਾਂ ਯੂਜ਼ਰ ਲੋਡ ਉਡੀਕ ਚਾਲੂ ਜੰਤਰ ਲਿਖਿਆ ਡਾਟਾ % kBytes MHz 1/s mAh mA mV mWh mW ਮਿੰਟ 