��            )         �     �  	   �  0   �  .   �     )  7   H      �     �  9   �  -   �            E   >  G   �  >   �               )     6     C     Y     x  <   }  :   �  s   �  o   i  .   �  ,     .   5  ,   d  �  �     /	     K	  &   e	  )   �	  \   �	  o   
     �
     �
  M   �
  W   �
  /   L  7   |     �  /   �  !   �       G   3     {  /   �  2   �  W   �     V  <   c  ?   �  a   �  Z   B  #   �  &   �  $   �  '                                                                        
      	                                                             Activities... Add Panel Bluetooth was disabled, keep shortBluetooth Off Bluetooth was enabled, keep shortBluetooth On Configure Mouse Actions Plugin Do not restart plasma-shell automatically after a crash EMAIL OF TRANSLATORSYour emails Empty %1 Fatal error message bodyShell package %1 cannot be found Fatal error message titlePlasma Cannot Start Hide Desktop NAME OF TRANSLATORSYour names OSD informing that some media app is muted, eg. Amarok Muted%1 Muted OSD informing that the microphone is muted, keep shortMicrophone Muted OSD informing that the system is muted, keep shortAudio Muted Plasma Plasma Failed To Start Plasma Shell Show Desktop Stop Current Activity Unable to load script file: %1 file mobile internet was disabled, keep shortMobile Internet Off mobile internet was enabled, keep shortMobile Internet On on screen keyboard was disabled because physical keyboard was plugged in, keep shortOn-Screen Keyboard Deactivated on screen keyboard was enabled because physical keyboard got unplugged, keep shortOn-Screen Keyboard Activated touchpad was disabled, keep shortTouchpad Off touchpad was enabled, keep shortTouchpad On wireless lan was disabled, keep shortWifi Off wireless lan was enabled, keep shortWifi On Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-12 03:10+0100
PO-Revision-Date: 2017-07-23 15:22-0600
Last-Translator: A S Alam <aalam@users.sf.net>
Language-Team: Punjabi <punjabi-users@lists.sf.net>
Language: pa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 ...ਸਰਗਰਮੀਆਂ ਪੈਨਲ ਸ਼ਾਮਲ ਬਲੂਟੁੱਥ ਬੰਦ ਹੈ ਬਲੂਟੁੱਥ ਚਾਲੂ ਹੈ ਮਾਊਸ ਕਰਾਰਵਾਈਆਂ ਦੀ ਸੰਰਚਨਾ ਲਈ ਪਲੱਗਇਨ ਕਰੈਸ਼ ਹੋਣ ਦੇ ਬਾਅਦ ਪਲਾਜ਼ਮਾ-ਸ਼ੈਲ ਮੁੜ-ਚਾਲੂ ਨਾ ਕਰੋ aalam@users.sf.net %1 ਖਾਲੀ ਸ਼ੈਲ ਪੈਕੇਜ %1 ਨਹੀਂ ਲੱਭਿਆ ਜਾ ਸਕਦਾ ਪਲਾਜ਼ਮਾ ਨੂੰ ਸ਼ੁਰੂ ਨਹੀਂ ਕੀਤਾ ਜਾ ਸਕਦਾ ਡੈਸਕਟਾਪ ਨੂੰ ਲੁਕਾਓ ਅਮਨਪਰੀਤ ਸਿੰਘ ਆਲਮ (A S Alam) %1 ਚੁੱਪ ਹੈ ਮਾਈਕਰੋਫ਼ੋਨ ਮੌਨ ਹੈ ਆਡੀਓ ਚੁੱਪ  ਹੈ ਪਲਾਜ਼ਮਾ ਪਲਾਜ਼ਮਾ ਸ਼ੁਰੂ ਹੋਣ ਲਈ ਫੇਲ੍ਹ ਹੈ ਪਲਾਜ਼ਮਾ ਸ਼ੈਲ ਡੈਸਕਟਾਪ ਨੂੰ ਵੇਖਾਓ ਮੌਜੂਦਾ ਸਰਗਰਮੀ ਰੋਕੋ ਸਕ੍ਰਿਪਟ ਫ਼ਾਈਲ ਲੋਡ ਕਰਨ ਲਈ ਅਸਮਰੱਥ: %1 ਫਾਇਲ ਮੋਬਾਈਲ ਇੰਟਰਨੈੱਟ ਬੰਦ ਹੈ ਮੋਬਾਈਲ ਇੰਟਰਨੈੱਟ ਚਾਲੂ ਹੈ ਆਨ-ਸਕਰੀਨ ਕੀਬੋਰਡ ਨੂੰ ਨਾ-ਸਰਗਰਮ ਕੀਤਾ ਗਿਆ ਆਨ-ਸਕਰੀਨ ਕੀਬੋਰਡ ਨੂੰ ਸਰਗਰਮ ਕੀਤਾ ਗਿਆ ਟੱਚਪੈਡ ਬੰਦ ਹੈ ਟੱਚਪੈਡ ਚਾਲੂ ਹੈ ਵਾਈ-ਫਾਈ ਬੰਦ ਹੈ ਵਾਈ-ਫਾਈ ਚਾਲੂ ਹੈ 