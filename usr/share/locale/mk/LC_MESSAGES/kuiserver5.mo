��          �   %   �      @     A     R     g     z     �     �  
   �     �     �     �     �     �     �          0     6     B     O     V     n     �     �     �     �  �  �  ;   �  )         .  *   O     z  "   �     �     �     �     �     �       0   .  1   _  
   �     �     �     �  9   �  :     ;   O  <   �  .   �  /   �                             
                                                                                             	    %1 file %1 files %1 folder %1 folders %1 of %2 processed %1 of %2 processed at %3/s %1 processed %1 processed at %2/s Appearance Behavior Cancel Clear Configure... Finished Jobs Move them to a different list Move them to a different list. Pause Remove them Remove them. Resume Show all jobs in a list Show all jobs in a list. Show all jobs in a tree Show all jobs in a tree. Show separate windows Show separate windows. Project-Id-Version: kuiserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2009-06-21 21:50+0200
Last-Translator: Bozidar Proevski <bobibobi@freemail.com.mk>
Language-Team: Macedonian <mkde-l10n@lists.sourceforge.net>
Language: mk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: Plural-Forms: nplurals=3; plural=n%10==1 ? 0 : n%10==2 ? 1 : 2;
 %1 датотека %1 датотеки %1 датотеки %1 папка %1 папки %1 папки завршени се %1 од %2 завршени се %1 од %2 на %3/s завршени се %1 завршени се %1 на %2/s Изглед Однесување Откажи Исчисти Конфигурирај... Завршени задачи Премести ги на друга листа Премести ги на друга листа. Пауза Отстрани ги Отстрани ги. Продолжи Прикажи ги сите задачи во листа Прикажи ги сите задачи во листа. Прикажи ги сите задачи во стебло Прикажи ги сите задачи во стебло. Прикажи одделни прозорци Прикажи одделни прозорци. 