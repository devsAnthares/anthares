��            )   �      �     �     �  
   '  +   2  
   ^     i     u     �     �  	   �      �  *   �  H   �     =     D     c  )   p  ;   �  	   �     �  (   �     (     @     V     u     �     �     �  	   �  �  �     �  �   �     �  _   �  "   D	     g	      |	  )   �	     �	     �	     �	  R   
  �   a
     �
     �
       ]   0  �   �       S   %  I   y  :   �  :   �  :   9  <   t  ,   �  <   �  @        \                                                                                       	                                           
        msec <h1>Multiple Desktops</h1>In this module, you can configure how many virtual desktops you want and how these should be labeled. Animation: Assigned global Shortcut "%1" to Desktop %2 Desktop %1 Desktop %1: Desktop Names Desktop Switching Desktops Duration: EMAIL OF TRANSLATORSYour emails Here you can enter the name for desktop %1 Here you can set how many virtual desktops you want on your KDE desktop. Layout NAME OF TRANSLATORSYour names No Animation No suitable Shortcut for Desktop %1 found Shortcut conflict: Could not set Shortcut %1 for Desktop %2 Shortcuts Show desktop layout indicators Show shortcuts for all possible desktops Switch One Desktop Down Switch One Desktop Up Switch One Desktop to the Left Switch One Desktop to the Right Switch to Desktop %1 Switch to Next Desktop Switch to Previous Desktop Switching Project-Id-Version: kcm_kwindesktop
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-06 05:35+0100
PO-Revision-Date: 2010-01-29 22:40+0100
Last-Translator: Bozidar Proevski <bobibobi@freemail.com.mk>
Language-Team: Macedonian <mkde-l10n@lists.sourceforge.net>
Language: mk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: Plural-Forms: nplurals=3; plural=n%10==1 ? 0 : n%10==2 ? 1 : 2;

  msec <h1>Повеќекратни површини</h1>Во овој модул може да конфигурирате колку виртуелни работни површини сакате да имате и како тие ќе се викаат. Анимација: Глобалната кратенка „%1“ е доделена на површината %2 Работна површина %1 Површина %1: Имиња на површини Менување раб. површини Работни површини Траење: bobibobi@freemail.com.mk Тука може да го внесете името за површината %1 Тука може да поставите колку виртуелни работни површини сакате да имате. Распоред Божидар Проевски Без анимации Не е пронајдена соодветна кратенка за површината %1 Конфликт на кратенки: Не можев да ја доделам кратенката %1 за површината %2 Кратенки Прикажи индикатори за распоред на површините Прикажи кратенки за сите можни површини Префрли се една површина надолу Префрли се една површина нагоре Префрли се една површина налево Префрли се една површина надесно Префрли се на површина %1 Префрли се на наредната површина Префрли се на претходната површина Префрлање 