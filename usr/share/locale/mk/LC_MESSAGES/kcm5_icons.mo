��    9      �  O   �      �     �     �     �     
          #  �   >  -   �  )   �  -   "  +   P  r   |  	   �  	   �               #     ,  
   9     D     P     X     `      w     �     �     �      �     �     �  r   �  5   r     �     �     �  	   �     �     �     �  (   �     '	     5	     N	     h	     �	     �	  +   �	  3   �	     �	     �	     
     
  S    
  )   t
     �
  �   �
  �  �     j     �     �     �  	   �     �  �   �  C   �          (     A  �   V          -     A  
   Y     d     m     |     �     �     �  H   �     	  &   "     I     R  :   _  
   �  A   �    �  i   �     U     q     �     �  
   �     �     �  ]   �     >  E   [  K   �  O   �     =     O  W   c  b   �          ,     L     [  �   h  Y   �      X  �  y               1                &              0   '                7                            
          -               4      5   #      (       6   3          )                 /   	             9         +                    *      $      !   ,   .   "       8           %      2    &Amount: &Effect: &Second color: &Semi-transparent &Theme (c) 2000-2003 Geert Jansen <qt>Are you sure you want to remove the <strong>%1</strong> icon theme?<br /><br />This will delete the files installed by this theme.</qt> <qt>Installing <strong>%1</strong> theme</qt> @label The icon rendered as activeActive @label The icon rendered as disabledDisabled @label The icon rendered by defaultDefault A problem occurred during the installation process; however, most of the themes in the archive have been installed Ad&vanced All Icons Antonio Larrosa Jimenez Co&lor: Colorize Confirmation Desaturate Description Desktop Dialogs Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Effect Parameters Gamma Geert Jansen Get new themes from the Internet Icons Icons Control Panel Module If you already have a theme archive locally, this button will unpack it and make it available for KDE applications Install a theme archive file you already have locally Main Toolbar NAME OF TRANSLATORSYour names Name No Effect Panel Preview Remove Theme Remove the selected theme from your disk Set Effect... Setup Active Icon Effect Setup Default Icon Effect Setup Disabled Icon Effect Size: Small Icons The file is not a valid icon theme archive. This will remove the selected theme from your disk. To Gray To Monochrome Toolbar Torsten Rahn Unable to download the icon theme archive;
please check that address %1 is correct. Unable to find the icon theme archive %1. Use of Icon You need to be connected to the Internet to use this action. A dialog will display a list of themes from the http://www.kde.org website. Clicking the Install button associated with a theme will install this theme locally. Project-Id-Version: kcmicons
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-08 06:27+0100
PO-Revision-Date: 2010-01-29 22:23+0100
Last-Translator: Bozidar Proevski <bobibobi@freemail.com.mk>
Language-Team: Macedonian <mkde-l10n@lists.sourceforge.net>
Language: mk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: Plural-Forms: nplurals=3; plural=n%10==1 ? 0 : n%10==2 ? 1 : 2;
 К&оличество: &Ефект: &Втора боја: Полупр&оѕирно &Тема (c) 2000-2003 Geert Jansen <qt>Дали навистина сакате да ја отстраните темата со икони <strong>%1</strong>?<br /><br />Ова ќе ги избрише датотеките инсталирани со оваа тема.</qt> <qt>Ја инсталирам темата <strong>%1</strong></qt> Активна Оневозможена Стандардна Настана проблем за време на процесот на инсталација. Сепак е инсталиран најголем дел од темите во архивата. &Напредно Сите икони Antonio Larrosa Jimenez Бо&ја: Обои Потврда Одзасити Опис Раб. површина Дијалози Довлечете го или впишете го URL на темата bobibobi@freemail.com.mk Параметри на ефектот Гама Geert Jansen Преземете нови теми од Интернет Икони Модул на Контролниот панел за икони Ако веќе имате датотека со темо на Вашиот локален тврд диск, ова копче ќе ја отпакува и ќе ја инсталира за да биде достапна за апликациите на KDE Инсталирање на датотека со тема што веќе ја имате локално Главен алатник Божидар Проевски Име Без ефекти Панел Преглед Отстрани тема Отстранување на избраната тема од Вашиот тврд диск Постави ефект... Постави го ефектот на активната икона Постави го ефектот на стандардната икона Постави го ефектот на оневозможената икона Големина: Мали икони Датотеката не е валидна архива со тема со икони. Ова ќе ја отстрани избраната тема од Вашиот тврд диск. Во сиво Во монохроматско Алатник Torsten Rahn Не можам да ја симнам архивата со тема со икони.
Проверете дали е точна адресата %1. Не можам да ја најдам архивата %1 со тема со икони. Употреба на икона За да ја користите оваа можност, треба да сте поврзани на Интернет. Ќе се појави дијалог што ќе прикаже листа со теми од веб-страницата http://www.kde.org. Кликнувањето на копчето „Инсталирај“, поврзано со одредена тема, ќе ја инсталира темата локално. 