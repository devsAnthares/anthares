��    d      <  �   \      �     �     �     �     �     �     �     �  	   �     �  	   �     �  5   �  %   	     3	     S	  
   c	     n	     �	     �	  	   �	     �	  
   �	     �	     �	     �	  	   
  
   
     
     .
     5
     A
     N
     Z
     g
     �
     �
     �
     �
     �
     �
      �
               -     D  &   Z      �     �  "   �     �     �          !     %  
   ,     7     E     W     d     i     v     {     �     �     �     �     �     �  	   �     �     �     �     �     �     �               #     ?     L     ^     k     p     w     }     �     �  
   �     �     �     �  	   �     �  	   �     �  0   �  
              (  �  -            -        J     N  
   R     ]     l     �     �     �     �     �     �  #   �        #   5     Y  	   j     t  %   �     �  *   �  ,   �  8        K     `     o     �     �     �     �     �  F   �     )  =   8  @   v  /   �  8   �           3     L  3   ^  4   �  "   �  N   �  B   9     |     �     �     �  -   �                  %   1  $   W  !   |     �     �     �  #   �     �     �       *   #     N     _     l     �     �     �     �  !   �  
          '   #  >   K     �  ;   �  '   �       
        '     4  
   A     L     f  !   �     �     �     �     �     �     �  l        u     �     �        0   [   1          6                
       S       &   Z   F       	   5      *   V       A   b          ]   4   >   O   $               R             M   3          9   I   T   -   E           +                 `   N              @   =   W       X   D      "   ^   H           ,   B                    :           K             /      (      ?          %   )       <          8          2   \       a   7                !       d   G   P       J   c           #           _   '   Y      Q   .   U   ;   C   L     %  msec  pixel  pixels  px  ° &Color: &Height: &Opacity: &Radius: &Spacing: &Width: @title:group actions when clicking on desktopDesktop @title:tab Advanced SettingsAdvanced @title:tab Basic SettingsBasic Activate window Activation Additional Options Advanced Angle: Animation Animation duration: Appearance Apply effect to &groups Apply effect to &panels Apply effect to the desk&top Automatic Background Background color: Bottom Bottom Left Bottom Right Bottom-Left Bottom-Right Bring window to current desktop Center Clear All Mouse Marks Clear Last Mouse Mark Clear Mouse Marks Close after mouse dragging Custom Desktop name alignment:Disabled Dialogs: Display desktop name Display window &titles Do not animate panels Do not animate windows on all desktops Do not change opacity of windows Dropdown menus: Duration of flip animationDefault Duration of rotationDefault Duration of zoomDefault Enable &advanced mode Far Faster Filter:
%1 Flexible Grid Inactive windows: Invert mouse Left Left button: Less Maximum &width: Menus: Middle button: More Moving windows: Natural Near No action Opacity Opaque Plane Popup menus: Regular Grid Right Right button: Rotation duration: Send window to all desktops Show &panels Show Desktop Grid Show desktop Size Sphere Tab 1 Tab 2 Text Text color: Text font: Text position: Top Top Left Top Right Top-Left Top-Right Transparent Use this effect for walking through the desktops Wallpaper: Windows Zoom Project-Id-Version: kwin_effects
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-02 03:18+0100
PO-Revision-Date: 2010-01-30 20:20+0100
Last-Translator: Bozidar Proevski <bobibobi@freemail.com.mk>
Language-Team: Macedonian <mkde-l10n@lists.sourceforge.net>
Language: mk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: Plural-Forms: nplurals=3; plural=n%10==1 ? 0 : n%10==2 ? 1 : 2;
  %  msec  пиксел  пиксели  пиксели  px  ° &Боја: &Висина: Непр&овидност: &Радиус: Ра&стојание: &Ширина: Работна површина Напредни Основни Активирај прозорец Активирање Дополнителни опции Напредно Агол: Анимација Траење на анимација: Изглед Примени ефект на &групи Примени ефект на &панели Примени ефект на раб. п&овршина Автоматски Подлога Боја на подлога: Долу Долу лево Долу десно Долу лево Долу десно Донеси прозорец на тековната површина Средина Исчисти ги сите траги од глушецот Исчисти последна трага од глушецот Исчисти траги од глушецот Затвори по влечење на глушецот Сопствено Оневозможено Дијалози: Прикажи име на раб. површина Прикажи &наслови на прозорци Не анимирај панели Не ги анимирај прозорците од сите површини Не менувај непровидност на прозорци Паѓачки менија: Стандардно Стандардно Стандардно Овозможи н&апреден режим Далеку Побрзо Филтер:
%1 Флексибилна решетка Неактивни прозорци: Инвертирај глушец Лево Лево копче: Помалку Максимална &ширина: Менија: Средно копче: Повеќе Преместување прозорци: Природно Блиску Нема дејство Непровидност Непровидно Рамнина Скокачки менија: Регуларна решетка Десно Десно копче Траење на ротацијата: Испрати прозорец на сите површини Прикажи &панели Прикажи решетка од раб. површина Прикажи раб. површина Големина Сфера Ливче 1 Ливче 2 Текст Боја на текст: Фонт за текст: Позиција на текст: Горе Горе лево Горе десно Горе лево Горе десно Провидно Користете го овој ефект за движење меѓу работните површини Тапет: Прозорци Зумирање 