��          �            x  �   y  m   �  `   i     �     �     �     �           3     R     W     h  ?   u  Y   �  +     �  ;  �   �  �   �  �   �     Q  A   l     �  =   �     �     	     %	  "   ,	  !   O	  v   q	  �   �	  G   j
        	                                                  
                     <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. Project-Id-Version: kcminput
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-01-29 22:17+0500
Last-Translator: Victor Ibragimov <victor.ibragimov@gmail.com>
Language-Team: Tajik
Language: tg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.0.1
Plural-Forms: nplurals=2; plural=n != 1;
 <qt>Вы действительно хотите удалить тему курсоров<i>%1</i>? <br />Все файлы, установленные этой темой, будут удалены.</qt> <qt>Вы не можете удалить эту тему, потому что она сейчас используется.<br />Сначала необходимо переключиться на другую тему.</qt> Файл бо номи %1 аллакай дар феҳристи мавзӯъҳои тасвирӣ мавҷуд аст. Оё мехоҳед, ки онро бо яке аз инҳо ҷойиваз кунед? Мувофиқаткунӣ Гузоришҳои Нишоннамо Тағир ёфтаанд Тасвир Кашолакунӣ ё Хориҷкунии Мавзӯи URL victor.ibragimov@gmail.com Victor Ibragimov Ном Перезаписать тему? Хориҷкунии Мавзӯъ Файли %1 ҳамчун нишоннамои дурусти бойгонии мавзӯъҳо наменамояд. Не удаётся загрузить архив темы курсоров, Убедитесь, что адрес %1 верен. Не удаётся найти архив темы курсоров %1. 