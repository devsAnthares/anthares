��            )         �     �  $   �     �     �     �               %     ,     @     X     k  &   t      �  	   �  �   �  �   d  �      �   �     &     +     :     M     l     �     �     �     �  S   �  G   )  �  q     		  H   	     c	  
   l	  )   w	  5   �	  1   �	      	
      *
      K
  .   l
     �
  L   �
  6         7    M  R  i  :  �    �            %   +  �   Q  B     4   R  9   �  1   �     �  �     �   �                                                               
                	                                                         &Folder: &Microsoft® Windows® network drive &Name: &Port: &Recent connection: &Secure shell (ssh) &Use encryption &User: &WebFolder (webdav) (c) 2004 George Staikos Add Network Folder C&onnect Cr&eate an icon for this remote folder EMAIL OF TRANSLATORSYour emails Encoding: Enter a name for this <i>File Transfer Protocol connection</i> as well as a server address and folder path to use and press the <b>Save & Connect</b> button. Enter a name for this <i>Microsoft Windows network drive</i> as well as a server address and folder path to use and press the <b>Save & Connect</b> button. Enter a name for this <i>Secure shell connection</i> as well as a server address, port and folder path to use and press the <b>Save & Connect</b> button. Enter a name for this <i>WebFolder</i> as well as a server address, port and folder path to use and press the <b>Save & Connect</b> button. FT&P George Staikos KDE Network Wizard NAME OF TRANSLATORSYour names Network Folder Information Network Folder Wizard Primary author and maintainer Save && C&onnect Se&rver: Select the type of network folder you wish to connect to and press the Next button. Unable to connect to server.  Please check your settings and try again. Project-Id-Version: knetattach
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-10 05:48+0100
PO-Revision-Date: 2009-01-11 13:04+0500
Last-Translator: Victor Ibragimov <victor.ibragimov@gmail.com>
Language-Team: Tajik
Language: tg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.10
Plural-Forms: nplurals=2; plural=n != 1;
 &Феҳрист: &Майкрософт® Виндоувс® идораи шабакавӣ &Ном: &Порт: &Пайвасткунии наздиқӣ: &Терминали Алоқаи Бехатар (ssh) &Рамзгузорӣ истифода баред &Истифодакунанда: &ФеҳристиВэб (webdav) (c) 2004 Ҷорҷ Стайкос Иловаи Феҳристи Шабакавӣ П&айвасткунӣ Со&хтани нишона барои ин феҳристи дурдаст ,rkovacs@khujand.org,youth_opportunities@tajikngo.org, Рамзгузорӣ: Барои истифодабарии <i>Алоқаи FTP</i> ва ҳамчун суроғаи сервер, рақами порт ва роҳча ба феҳрист ворид кунед ва тугмаи <b>Нигоҳ доштани пайвасткунӣ</b> пахш занед. Барои истифодабарии  <i>Идораи Шабакавии Майкрософт Виндоувс</i> ва ҳамчун суроғаи сервер, рақами порт ва роҳча ба феҳрист ворид кунед ва тугмаи  <b>Нигоҳ доштани пайвасткунӣ</b> пахш занед. Барои истифодабарии <i>Терминали Алоқаи Бехатар</i> ва ҳамчун суроғаи сервер, рақами порт ва роҳча ба феҳрист ворид кунед ва тугмаи  <b>Нигоҳ доштани пайвасткунӣ</b> пахш занед. Барои истифодабарии ин <i>Феҳрист</i> ва ҳамчун суроғаи сервер, рақами порт ва роҳча ба феҳрист ворид кунед ва тугмаи  <b>Нигоҳ доштани пайвасткунӣ</b> пахш занед. FT&P George Staikos Танзимоти Шабакаи KDE НПО Имкониятҳои ҷавонон ва Хуҷанд Компютер Технолоҷис:,Роҷер Ковакс, Виктор Ибрагимов, Марина Колючева Ахборот дар бораи феҳристи шабакавӣ Танзимоти Феҳристи Шабакавӣ Муаллифи аслӣ ва тавлидкунанда Нигоҳ доштани &пайвасткунӣ Сер&вер: Намуди феҳристи шабакавиро барои пайвасткунӣ интихобкунед ва тугмаи Оянда-ро пашх занед. Пайвасткунӣ имконпазир аст.  Лутфан танзимотҳои шумо санҷет ва аз нав кӯшиш кунед. 