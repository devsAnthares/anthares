��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  �  K     �  3   �  9  #    ]
  �   |  '     -   >  "   l  <   �  "   �  8   �  5   (  2   ^                                          
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2009-01-11 14:38+0500
Last-Translator: Victor Ibragimov <victor.ibragimov@gmail.com>
Language-Team: Tajik
Language: tg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.0.1
Plural-Forms: nplurals=2; plural=n != 1;
  сония &Намоиши поёнаи вақти шурӯъ: <H1>Огоҳонӣ дар панели супориш</H1>
Шумо усули дигари огоҳкуниро оиди оғозёбӣ даргиронида
метавонед, ки аз тарафи панели супоришҳо истифода мегардад ва тугмаи бо диски гардандаро тасвир мекунад ва он маънои
боркунии замимаи сардиҳандаро ифода мекунад.
Он фоиданок буди метавонад зеро баъзеи замимаҳо огоҳониро оиди боркунӣ амалӣ намесозанд. Дар ин ҳолат тугма баъди
гузаштани мӯҳлати вақти дар қисмати 'Вақти нишондиҳии оғозёбӣ'
додашуда нест мешавад <h1>Нишоннамои Машғул</h1>
KDE нишоннамои машғулро барои огоҳонидан оиди оғозёбии замима пешкаш мекунад.
Барои даргиронидани нишоннамои машғул як навъи посухи
дидориро аз рӯйхати афтанда интихоб кунед.
Чунин ҳодиса ба амал омаданаш мумкин аст, ки баъзе замимаҳо огоҳкуниро оиди оғозёбӣ пуштибонӣ намекунанд.
Дар ин ҳолат нишоннамо баъди мӯҳлати вақте, ки дар қисмати
'Вақти нишондиҳии оғозёбӣ' дода шудааст, милт-милт карданро боз медорад. <h1>Бозтоби роҳандозӣ</h1> Бозтоби роҳандози як барномаро метавонед инҷо танзим кунед. Нишоннамои чашмакзан Нишоннамои Ҷаҳишкунанда Нишоннамои &машғул Фаъолсози &итлоъияи майли вазифа Нишоннамо банд аст Нишоннамои машғули ғайрифаъол Поёни муддати &намоиши шурӯъ: &Огоҳонӣ дар панели супориш 