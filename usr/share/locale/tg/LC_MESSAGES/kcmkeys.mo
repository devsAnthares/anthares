��          �      l      �     �     �     �  
   	          &  2   7     j     o     �     �     �     �     �     �     �  /        8  ?   R  S   �  �  �     �  !   �     �     �     �     �  s   
     ~     �     �  5   �     �        &   &  5   M  3   �  k   �  @   #  �   d  �   �                                         
                                            	             &Path: &Standard scheme: All Components Components Current Component Export Scheme... Failed to contact the KDE global shortcuts daemon
 File Import Scheme... Load Load Shortcut Scheme Message: %1
Error: %2 Remove component Reset to defaults Select Shortcut Scheme Select a shortcut scheme file Select one of the standard KDE shortcut schemes Set All Shortcuts to None This file (%1) does not exist. You can only select local files. Your current changes will be lost if you load another scheme before saving this one Project-Id-Version: kcmkeys
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-03 04:12+0100
PO-Revision-Date: 2009-08-04 00:37+0500
Last-Translator: Victor Ibragimov <victor.ibragimov@gmail.com>
Language-Team: Тоҷикӣ
Language: tg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.0.1
Plural-Forms: nplurals=2; plural=n != 1;
 &Роҳча: &Нақшаи стандартӣ: Ҳамаи қисмҳо Қисмҳо Қисми ҷорӣ Содироти нақша... Пайвастшавӣ бо модули глобалии тугмаҳои тези KDE қатъ карда шуд
 Файл Воридоти нақша... Боркунӣ Боркунии нақшаи тугмаҳои тез Паём: %1
Хато: %2 Нест кардани қисм Бозсозӣ ба стандартӣ Интихоби нақшаи тугмаҳои тез Интихоби файли тугмаҳои тез Як аз нақшаҳои стандартии тугмаҳои тези KDEро интихоб кунед Ҳамаи тугмаҳо ба ҳеҷ чиз насб кунед Файли (%1) мавҷуд нест. Шумо метавонед танҳо фалҳои локалиро интихоб кунед. Агар шумо нақшаи навро бе захиракунии нақшаи ҷорӣ бор кунед, танзимотҳои ҳозира нест карда мешаванд 