��          �   %   �      0     1     >     F     O     W     ^     f     j      s  �   �  l   G  �   �     ?     O     U     \     c     t     �     �     �  	   �     �  �  �     {     �     �     �     �     �     �     �     �  D    �   W    !	  4   >
  
   s
     ~
     �
     �
     �
  J   �
  *   $     O     h     �                                                           
                           	                                         &Application &Delay: &Desktop &Global &Rate: &Window ... Advanced EMAIL OF TRANSLATORSYour emails If supported, this option allows you to set the delay after which a pressed key will start generating keycodes. The 'Repeat rate' option controls the frequency of these keycodes. If supported, this option allows you to set the rate at which keycodes are generated while a key is pressed. If you select "Application" or "Window" switching policy, changing the keyboard layout will only affect the current application or window. Keyboard Repeat Label Label: Layout Leave unchan&ged NAME OF TRANSLATORSYour names Switch to Next Keyboard Layout Switching Policy T&urn on Turn o&ff Variant Project-Id-Version: kcmkeyboard
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-09 07:18+0200
PO-Revision-Date: 2009-01-16 13:42+0500
Last-Translator: Victor Ibragimov <victor.ibragimov@gmail.com>
Language-Team: Tajik <>
Language: tg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 0.2
Plural-Forms: nplurals=2; plural=n != 1;
 &Барномаҳо &Нигоҳ доштан: &Мизи корӣ &Глобалӣ &Суръат: &Тиреза ... Иловагӣ H_Abrorova@rambler.ru Если поддерживается, этот параметр позволяет установить задержку, после которой нажатая клавиша начинает генерировать коды. Параметр 'Частота' определяет частоту автоповтора. Если поддерживается, этот параметр позволяет установить частоту автоповтора при длительном нажатии клавиши. Агар шумо"Барнома" ёки "тиреза"-и таъғири образи корбариро интихоб кунед, дигаргунии забонаки тарҳбандӣ танҳо ба барномаи ҳозира ёки тиреза таъсир мекунад. Рафтори тугмаҳои клавиатура Тамға Тамға: Тарҳбандӣ Тағйир надиҳед Абророва Хиромон Гузариш ба Ҷобаҷогузории Забонаки Оянда Таъғири образи корбарӣ Ф&аъол кардан Хомӯш кар&дан Вариант 