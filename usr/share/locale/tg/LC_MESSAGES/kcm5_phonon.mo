��    @        Y         �  m   �     �  b        j       6   �     �  7   �            	   #     -  (   K  )   t  )   �     �     �  Y   �     E     Z  �   l      �     	  
   	     (	     =	     I	     _	     h	     |	     �	     �	     �	     �	     �	     �	     �	     
     
     
  	   "
  
   ,
     7
     F
  	   \
  
   f
  
   q
     |
     �
  	   �
     �
     �
  
   �
  �   �
     o  8     �   �     @  8   P  ,   �  ,   �  %   �     	  �  $  �   �  ?   {  �   �  >   m  )   �  p   �     G  ^   _     �     �     �     �  g   
  U   r  U   �  8        W  �   `  '     %   E  �   k     g  -   �  !   �  .   �  #     0   %     V  +   o     �     �  B   �                  *   @  #   k     �     �  %   �     �     �       2        L     f     �  %   �  <   �            :        Z  D  v  !   �  �   �  �   `     #  ^   ;  j   �  j     L   p  8   �     "      #          9   *          @                     )                 5   7                      '   =   +   :         4                                   (   0       <         6       /       >       ,   3   1   8   ?         $      -         !       2       
       .                    ;   &   %            	           A list of Phonon Backends found on your system.  The order here determines the order Phonon will use them in. Apply Device List To... Apply the currently shown device preference list to the following other audio playback categories: Audio Hardware Setup Audio Playback Audio Playback Device Preference for the '%1' Category Audio Recording Audio Recording Device Preference for the '%1' Category Backend Colin Guthrie Connector Copyright 2006 Matthias Kretz Default Audio Playback Device Preference Default Audio Recording Device Preference Default Video Recording Device Preference Default/Unspecified Category Defer Defines the default ordering of devices which can be overridden by individual categories. Device Configuration Device Preference Devices found on your system, suitable for the selected category.  Choose the device that you wish to be used by the applications. EMAIL OF TRANSLATORSYour emails Front Center Front Left Front Left of Center Front Right Front Right of Center Hardware Independent Devices Input Levels Invalid KDE Audio Hardware Setup Matthias Kretz Mono NAME OF TRANSLATORSYour names Phonon Configuration Module Playback (%1) Prefer Profile Rear Center Rear Left Rear Right Recording (%1) Show advanced devices Side Left Side Right Sound Card Sound Device Speaker Placement and Testing Subwoofer Test Test the selected device Testing %1 The order determines the preference of the devices. If for some reason the first device cannot be used Phonon will try to use the second, and so on. Unknown Channel Use the currently shown device list for more categories. Various categories of media use cases.  For each category, you may choose what device you prefer to be used by the Phonon applications. Video Recording Video Recording Device Preference for the '%1' Category  Your backend may not support audio recording Your backend may not support video recording no preference for the selected device prefer the selected device Project-Id-Version: kcm_phonon
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2012-01-04 21:47+0500
Last-Translator: Victor Ibragimov <victor.ibragimov@gmail.com>
Language-Team: Tajik <>
Language: tg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 0.2
Plural-Forms: nplurals=2; plural=n != 1;
 Рӯйхати пуштибонии Phonon дар системаи шумо ёфт шуд.  Тартиботи рӯйхат истифодабарии Phonon муайян мекунад. Қабул кардани рӯйхати дастгоҳ ба... Использовать приоритет использования устройств для следующих категорий воспроизведения звука: Настройка звукового оборудования Воспроизведение звука Приоритет устройств воспроизведения звука для категории «%1» Запись звука Приоритет устройств записи звука для категории «%1» Пуштибонӣ Colin Guthrie Разъём Copyright 2006 Matthias Kretz Приоритет устройств воспроизведения звука по умолчанию Приоритет устройств записи звука по умолчанию Приоритет устройств записи видео по умолчанию Категорияи стандартӣ/номуайян Қатъ Муайянкунии тартиботи дастгоҳҳои стандартие, ки метавонанд бо интихоби шахси ба кор дароварда шаванд. Настройка устройства Хусусиятҳои дастгоҳ Обнаруженные в системе устройства, подходящие для выбранной категории. Выберите устройство, которое будет использоваться приложениями. victor.ibragimov@gmail.com Центральный фронтальный Левый фронтальный Фронтальный левее центра Правый фронтальный Фронтальный правее центра Оборудование Независимые устройства Уровни на входе Ошибка Настройка звукового оборудования KDE Matthias Kretz Моно Виктор Ибрагимов Барномаи танзимоти Phonon Воспроизведение (%1) Мақбул Профиль Центральный тыловой Левый тыловой Правый тыловой Запись (%1) Намоиши дастгоҳҳои иловагӣ Левый боковой Правый боковой Звуковая плата Звуковое устройство Проверка конфигурации динамиков Сабвуфер Санҷиш Проверить выбранное устройство Тестирование %1 Приоритет определяет порядок использования устройств: если невозможно вывести звук на первое устройство, будет предпринята попытка вывода на следующем устройстве и так далее. Неизвестный канал Использовать текущий список устройств для некоторых других категорий. Различные категории работы с мультимедиа. Для каждой категории вы можете назначить отдельное устройство. Запись видео Приоритет устройств записи видео для категории «%1» Выбранный механизм вероятно не поддерживает запись звука Выбранный механизм вероятно не поддерживает запись видео имконоти дастгоҳи интихобшуда маҷуд нест имконоти дастгоҳи интихобшуда 