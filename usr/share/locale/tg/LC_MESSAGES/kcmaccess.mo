��    8      �  O   �      �     �     �     �     �     �       +     *   I  +   t  #   �     �  J   �     #     ,     9     @  	   L  H   V     �      �     �  �   �  �   �  M   I     �     �  �   �    >	     K
     b
     o
     �
     �
     �
  1   �
  !   �
  .     0   A      r  :   �  #   �  
   �     �       N     H   b  =   �     �     �             5   1  D   g  D   �  .   �  �        �     �     �  &   �  D     :   F  b   �  V   �  \   ;  #   �     �  �   �          �     �  %   �     �  �   �  -   �     �     �  �    f  �  �   �     �     �  ^  �  �    ,   �          1     K     \     o  C   �  5   �  @   �  B   <  4     L   �  7        9     X     t  �   z  �   0  }   �  0   @  B   q  ?   �  B   �  u   7   �   �   �   2!  f   �!                  *                 4          )       !                                    -      .      
       0   1   '                         2       /   ,      6   8         &          %   3              $             	   (   "   5                         +      7              #     min  msec &Bell &Keyboard Filters &Lock sticky keys &Use slow keys &Use system bell whenever a key is accepted &Use system bell whenever a key is pressed &Use system bell whenever a key is rejected (c) 2000, Matthias Hoelzer-Kluepfel Activation Gestures All screen colors will be inverted for the amount of time specified below. AltGraph Audible Bell Author Bounce Keys Browse... Click here to choose the color used for the "flash screen" visible bell. Configure &Notifications... EMAIL OF TRANSLATORSYour emails F&lash screen Here you can activate keyboard gestures that turn on the following features: 
Mouse Keys: %1
Sticky keys: Press Shift key 5 consecutive times
Slow keys: Hold down Shift for 8 seconds Here you can activate keyboard gestures that turn on the following features: 
Sticky keys: Press Shift key 5 consecutive times
Slow keys: Hold down Shift for 8 seconds Here you can customize the duration of the "visible bell" effect being shown. Hyper I&nvert screen If the option "Use customized bell" is enabled, you can choose a sound file here. Click "Browse..." to choose a sound file using the file dialog. If this option is checked, KDE will show a confirmation dialog whenever a keyboard accessibility feature is turned on or off.
Ensure you know what you are doing if you uncheck it, as the keyboard accessibility settings will then always be applied without confirmation. KDE Accessibility Tool Locking Keys Matthias Hoelzer-Kluepfel NAME OF TRANSLATORSYour names Notification Press %1 Press %1 while CapsLock and ScrollLock are active Press %1 while CapsLock is active Press %1 while NumLock and CapsLock are active Press %1 while NumLock and ScrollLock are active Press %1 while NumLock is active Press %1 while NumLock, CapsLock and ScrollLock are active Press %1 while ScrollLock is active Slo&w Keys Sound &to play: Super The screen will turn to a custom color for the amount of time specified below. Turn sticky keys and slow keys off after a certain period of inactivity. Turn sticky keys off when two keys are pressed simultaneously Us&e customized bell Use &sticky keys Use &system bell Use bou&nce keys Use gestures for activating sticky keys and slow keys Use system bell whenever a locking key gets activated or deactivated Use system bell whenever a modifier gets latched, locked or unlocked Use the system bell whenever a key is rejected Project-Id-Version: kcmaccess
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-10 03:17+0100
PO-Revision-Date: 2009-01-16 13:30+0500
Last-Translator: Victor Ibragimov <victor.ibragimov@gmail.com>
Language-Team: Tajik
Language: tg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.10
Plural-Forms: nplurals=2; plural=n != 1;
  дақ  msec &Садои занг &Филтрҳои клавиатура Тугмачаҳо&и Часпанда муҳосира кардан &Тугмачаҳои суст истифода кунед &Подавать системный сигнал при использовании клавиши &Подавать системный сигнал при нажатии клавиши &Подавать системный сигнал при отклонении клавиши (c) 2000, Matthias Hoelzer-Kluepfel Жесты активации Ҳамаи ранги экран дар мӯҳлати муайяни вақт ба ранги дар поён таъиншуда мубаддал мегардад. AltGraph Занги овоз Муаллиф Тугмачаҳои Паридани Кушодан... Барои интихоби ранге, ки баҳри занги дидории "намоёншавии экран" истифода мешавад, дар ин ҷо ангушт занед. Настроить &уведомления... victor.ibragimov@gmail.com &Мижазании экран Здесь вы можете активировать жесты клавиатуры, которые включают следующие функции: 
Клавиши мыши: %1
Залипающие клавиши: Нажатие клавиши Shift 5 раз подряд
Медленные клавиши: Удерживание клавиши Shift нажатой 8 секунд Здесь вы можете активировать жесты клавиатуры, которые включают следующие функции: 
Залипающие клавиши: Нажатие клавиши Shift 5 раз подряд
Медленные клавиши: Удерживание клавиши Shift нажатой 8 секунд Дар ин ҷо шумо метавонед мӯҳлати намоишдиҳии натиҷаи "занги дидорӣ"-ро танзим кунед. Hyper Инверсияи эк&ран Агар хосияти "Истифодаи занги интихобӣ" фаъол гашта бошад, шумо файли овозиро аз ин ҷо интихоб карда метавонед. Барои интихоби файли овозӣ ба воситаи муколамаи файл "Баррасӣ..."-ро ангушт занед. Если флажок установлен, при включении или отключении специальных возможностей клавиатуры KDE покажет диалог подтверждения.
Будьте осторожны - если вы отключите этот параметр, специальные возможности клавиатуры всегда будут применяться без подтверждения. Мизроби лавозимот дар KDE Тугмаҳои қулф Matthias Hoelzer-Kluepfel Victor Ibragimov Хабарнома Нажатие %1 Нажатие %1, когда активны CapsLock и ScrollLock Нажатие %1, когда активен CapsLock Нажатие %1, когда активны NumLock и CapsLock Нажатие %1, когда активны NumLock и ScrollLock Нажатие %1, когда активен NumLock Нажатие %1, когда активны NumLock, CapsLock и ScrollLock Нажатие %1, когда активен ScrollLock &Тугмачаҳои Суст Овоз &навохтан: Super Ҳамаи ранги экран дар мӯҳлати муайяни вақт ба ранги дар поён таъиншуда интихобӣ мубаддал мегардад. Отключать залипающие и замедленные клавиши через некоторое время бездействия. Выключать залипающие клавиши при одновременном нажатии двух клавиш Занги оддӣ истиф&ода кунед Тугмачаҳо&и Часпанда истифода кунед &Тартиби садои занг истифода кунед Тугма&чаҳои паридани истифода кунед Использовать жесты для активации залипающих и медленных клавиш Подавать звуковой сигнал при включении и выключении клавиш индикаторов Подавать звуковой сигнал при активации, нажатии или отжатии клавиш-модификаторов Подавать системный сигнал при отклонении ввода клавиши 