��          |      �          +   !      M     n     �     �     �  �   �  g   [  0   �     �  @     �  E  L   �     2     M     ^     g  D   {  K  �  �     F     )   I  
   s                              	               
              Conflict with Standard Application Shortcut EMAIL OF TRANSLATORSYour emails NAME OF TRANSLATORSYour names No shortcut definedNone Reassign Reserved Shortcut The '%1' key combination is also used for the standard action "%2" that some applications use.
Do you really want to use it as a global shortcut as well? The F12 key is reserved on Windows, so cannot be used for a global shortcut.
Please choose another one. The key you just pressed is not supported by Qt. Unsupported Key What the user inputs now will be taken as the new shortcutInput Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-30 03:07+0100
PO-Revision-Date: 2013-06-04 17:36+0500
Last-Translator: Victor Ibragimov <victor.ibragimov@gmail.com>
Language-Team: Tajik Language
Language: tg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.4
Plural-Forms: nplurals=2; plural=n != 1;
 Мухолифат бо тугмаи якҷояшудаи стандартӣ victor.ibragimov@gmail.com Victor Ibragimov Холӣ Якҷояи нав Зарезервированная комбинация клавиш Тугмаи '%1' аллакай бо амали стандартии "%2" якҷоя карда шуд. Ин якҷояшавии тугмаҳо бо бисёр барномаҳо истифода мешавад.
Ин тавр шумо наметавонед ин тугмаҳоро бо амали ҷорӣ якҷоя кунед. В Windows клавиша F12 зарезервирована, поэтому её нельзя использовать в глобальных комбинациях клавиш.
Выберите другую комбинацию клавиш. Нажатая клавиша не поддерживается в Qt. Неподдерживаемый ключ Вуруд 