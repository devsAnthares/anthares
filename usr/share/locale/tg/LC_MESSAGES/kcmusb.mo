��    O      �  k         �  "   �     �     �  8     9   =  ,   w      �  2   �  4   �  4   -  8   b  ?   �  0   �  #   	  /   0	  0   `	  #   �	  2   �	  /   �	     
     $
     3
     D
     J
     X
  
   p
     {
     �
     �
     �
     �
     �
     �
  }   �
     Y     ^     e      q     �     �     �     �     �     �     �  	   �     �               *     7     Q     W     e     �     �     �     �     �     �     �     �     �  	   �  	   �  G   �     0     <     H     W     _     t     |     �     �     �     �     �  �  �  5     #   �     �  I   �  W   ;  2   �  #   �  E   �  I   0  P   z  K   �  V     6   n  +   �  B   �  =     )   R  9   |  7   �     �       (     
   B     M  4   b  &   �     �     �     �     �     �       &   ,    S     h     y     �     �     �     �     �  #   �       6        J     Y     f     {     �     �     �     �     �     �     
     "     @     G     T     Z     a     i     x     }     �  �   �     O     \     q     �     �     �  %   �  0   �  8     6   >  %   u     �         /   6          A   -   >      B                  5             M              )      %   E   L         4             3   +             H   ;      $   1   J      &          '   D   0           	   #   !       C      *   <          ,           2   I          @      
       .   O                         G          F             =                 K   8      9   7   :   ?   N           "   (    (c) 2001 Matthias Hoelzer-Kluepfel <b>Manufacturer:</b>  <b>Serial #:</b>  <tr><td><i>Attached Devicenodes</i></td><td>%1</td></tr> <tr><td><i>Bandwidth</i></td><td>%1 of %2 (%3%)</td></tr> <tr><td><i>Channels</i></td><td>%1</td></tr> <tr><td><i>Class</i></td>%1</tr> <tr><td><i>Intr. requests</i></td><td>%1</td></tr> <tr><td><i>Isochr. requests</i></td><td>%1</td></tr> <tr><td><i>Max. Packet Size</i></td><td>%1</td></tr> <tr><td><i>Power Consumption</i></td><td>%1 mA</td></tr> <tr><td><i>Power Consumption</i></td><td>self powered</td></tr> <tr><td><i>Product ID</i></td><td>0x%1</td></tr> <tr><td><i>Protocol</i></td>%1</tr> <tr><td><i>Revision</i></td><td>%1.%2</td></tr> <tr><td><i>Speed</i></td><td>%1 Mbit/s</td></tr> <tr><td><i>Subclass</i></td>%1</tr> <tr><td><i>USB Version</i></td><td>%1.%2</td></tr> <tr><td><i>Vendor ID</i></td><td>0x%1</td></tr> AT-commands ATM Networking Abstract (modem) Audio Bidirectional Boot Interface Subclass Bulk (Zip) CAPI 2.0 CAPI Control CDC PUF Communications Control Device Control/Bulk Control/Bulk/Interrupt Could not open one or more USB controller. Make sure, you have read access to all USB controllers that should be listed here. Data Device Direct Line EMAIL OF TRANSLATORSYour emails Ethernet Networking Floppy HDLC Host Based Driver Hub Human Interface Devices I.430 ISDN BRI Interface Keyboard Leo Savernik Live Monitoring of USB Bus Mass Storage Matthias Hoelzer-Kluepfel Mouse Multi-Channel NAME OF TRANSLATORSYour names No Subclass Non Streaming None Printer Q.921 Q.921M Q.921TM Q.932 EuroISDN SCSI Streaming Telephone This module allows you to see the devices attached to your USB bus(es). Transparent USB Devices Unidirectional Unknown V.120 V.24 rate ISDN V.42bis Vendor Specific Vendor Specific Class Vendor Specific Protocol Vendor Specific Subclass Vendor specific kcmusb Project-Id-Version: kcmusb
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2009-01-16 13:52+0500
Last-Translator: Victor Ibragimov <victor.ibragimov@gmail.com>
Language-Team: Tajik
Language: tg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.10
Plural-Forms: nplurals=2; plural=n != 1;
 (c) 2001 Маттиас Хоэлзер-Клупфел <b>Тавлидкунанда:</b>  <b>Сериол #:</b>  <tr><td><i>Дастгоҳҳои васлшуда</i></td><td>%1</td></tr> <tr><td><i>Имконияти гузарониш</i></td><td>%1 аз %2 (%3%)</td></tr> <tr><td><i>Каналҳо</i></td><td>%1</td></tr> <tr><td><i>Синф</i></td>%1</tr> <tr><td><i>Дархостҳои дохила</i></td><td>%1</td></tr> <tr><td><i>Дархостҳои изохронӣ</i></td><td>%1</td></tr> <tr><td><i>Андозаи қуттиҳои ниҳоят</i></td><td>%1</td></tr> <tr><td><i>Энергопотребление</i></td><td>%1 мА</td></tr> <tr><td><i>Энергопотребление</i></td><td>автономно</td></tr> <tr><td><i>Маҳсулот</i></td><td>0x%1</td></tr> <tr><td><i>Протокол</i></td>%1</tr> <tr><td><i>Тафтиши дубора</i></td><td>%1.%2</td></tr> <tr><td><i>Суръат</i></td><td>%1 Mбит/ҳо</td></tr> <tr><td><i>Зерсинф</i></td>%1</tr> <tr><td><i>Ривояти USB</i></td><td>%1.%2</td></tr> <tr><td><i>Вендори ID</i></td><td>0x%1</td></tr> Фармонҳои -AT Шабақаи ATM  Таври муҷаррад (модем) Аудио Духусусият Боркунии Интерфейси Зерсинф Ҳаҷм (Омехтани фаврӣ) CAPI 2.0 Роҳбари CAPI CDC PUF Пайвастшавӣ Сохтмони Роҳбар Идора/Ҳаҷм Идора/Ҳаҷм/Кандашавӣ Не удаётся открыть как минимум один контроллер USB. Проверьте, есть ли у вас права на чтение для всех контроллеров USB, которые должны быть показаны здесь. Маълумот Дастгоҳ Хати рост victor.ibragimov@gmail.com Шабақаи Ethernet  Дискета HDLC Драйвери Интернетӣ Хаб (Hub) Дастгоҳҳҳои иртиботии инсонӣ I.430 ISDN BRI Робита Клавиатура Leo Savernik Назорати USB Bus Захирагоҳ Matthias Hoelzer-Kluepfel Муш Бисёрканал Виктор Ибрагимов Зерсинф нест Пайдарҳамӣ нест Ҳеҷ Чопгар Q.921 Q.921M Q.921TM Q.932 EuroISDN SCSI Пайдарҳамӣ Телефон Ин модул ба шумо имконияти дидани дастгоҳҳое, ки ба гузаргоҳ(ҳо)и USB-и шумо пайваст аст, медиҳад. Шаффоф Сохтмони USB Якравия Номуайян V.120 V.24 rate ISDN V.42bis Тафсилотҳои дастгоҳ Синфи тафсилотҳои дастгоҳ Протоколи тафсилотҳои дастгоҳ Зерсинфи тафсилотҳои дастгоҳ Тафсилотҳои дастгоҳ kcmusb 