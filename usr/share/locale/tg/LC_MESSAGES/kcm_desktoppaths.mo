��            )   �      �  �   �  	   G     Q     a     w       	   �     �  	   �     �  %   �     �     �                    %     .  X   =  S   �  �   �  I   �  F   �  E   >  H   �  B   �  :     7   K  �  �  ]  	     w
  "   �
  &   �
     �
  '   �
     
           >     O     l     �     �     �     �     �     �  "   �  ]     j   x  ?  �  �   #  �   �  �   j  �   �  }   �          #                                                        	                                                                 
                 <h1>Paths</h1>
This module allows you to choose where in the filesystem the files on your desktop should be stored.
Use the "Whats This?" (Shift+F1) to get help on specific options. Autostart Autostart path: Confirmation Required Desktop Desktop path: Documents Documents path: Downloads Downloads path: Move files from old to new placeMove Move the directoryMove Movies Movies path: Music Music path: Pictures Pictures path: The path for '%1' has been changed.
Do you want the files to be moved from '%2' to '%3'? The path for '%1' has been changed.
Do you want to move the directory '%2' to '%3'? This folder contains all the files which you see on your desktop. You can change the location of this folder if you want to, and the contents will move automatically to the new location as well. This folder will be used by default to load or save documents from or to. This folder will be used by default to load or save movies from or to. This folder will be used by default to load or save music from or to. This folder will be used by default to load or save pictures from or to. This folder will be used by default to save your downloaded items. Use the new directory but do not move anythingDo not Move Use the new directory but do not move filesDo not Move Project-Id-Version: kcmkonq
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-09 07:18+0200
PO-Revision-Date: 2010-01-29 22:19+0500
Last-Translator: Victor Ibragimov <victor.ibragimov@gmail.com>
Language-Team: Tajik
Language: tg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.0.1
Plural-Forms: nplurals=2; plural=n != 1;
 <h1>Пути</h1>
Этот модуль позволяет указать, где должны располагаться файлы рабочего стола в файловой системе.
Используйте подсказку «Что это?» (Shift+F1) для вызова справки по отдельным параметрам. Худоғоз Папка автозапуска: Тасдиқкунӣ лозим аст Мизи корӣ Папка рабочего стола: Документы Папка документов: Загрузки Папка загрузок: Переместить Переместить Фильмы Папка фильмов: Музыка Папка музыки: Рисунки Папка изображений: Путь для '%1' изменился.
Переместить файлы из '%2' в '%3'? Путь к папке «%1» изменился.
Переместить файлы из «%2» в «%3»? Ин папка дорои ҳамаи файлҳое, ки дар мизи корӣ равшан мебошанд. Шумо метавонед адреси ин каталогро бо табъи худ таъғир диҳед, дар ин ҳолат окана худкорона ба ҷои нав мегузарад. Боркардан ё ки нигоҳдоштани ҳуҷҷатҳо дар/аз феҳристи додашуда бо пешфарзӣ истифода карда мешавад. Эта папка будет использоваться по умолчанию для открытия и сохранения фильмов. Эта папка будет использоваться по умолчанию для открытия и сохранения музыки. Эта папка будет использоваться по умолчанию для открытия и сохранения изображений. Эта папка будет использоваться по умолчанию для загружаемых файлов. Оставить Оставить 