��    ,      |  ;   �      �  	   �  	   �     �     �     �     �     �            
   /     :  ?   F     �  	   �     �      �     �     �     �  
   �     	          )  
   6     A     P     _     ~     �     �     �     �     �     �     �     �               )     @     N     a  S   o  �  �     p     �     �     �     �  	   �     �     �  ;   	     >	     V	  ?   j	     �	     �	  (   �	     �	  N   
  
   V
  %   a
     �
  !   �
     �
  (   �
               .     =     [     s  $   �     �  +   �     �          0  2   B  :   u     �  ,   �     �  !        0  �   >     (                    ,                   "                           )      *   	                          !   +   #       '                        &   %       $               
                             [Hidden] &Comment: &Delete &Description: &Edit &File &Name: &New Submenu... &Run as a different user &Username: &Work path: (C) 2000-2003, Waldo Bastian, Raffaele Sandrini, Matthias Elter Advanced Co&mmand: Current shortcut &key: EMAIL OF TRANSLATORSYour emails Enable &launch feedback General Hidden entry Item name: KDE Menu Editor KDE menu editor Main Toolbar Maintainer Matthias Elter Montel Laurent NAME OF TRANSLATORSYour names New &Item... New Item New S&eparator New Submenu Only show in KDE Original Author Previous Maintainer Raffaele Sandrini Run in term&inal Save Menu Changes? Spell Checking Spell checking Options Submenu name: Terminal &options: Waldo Bastian You have made changes to the menu.
Do you want to save the changes or discard them? Project-Id-Version: kmenuedit
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2009-06-21 15:07+0500
Last-Translator: Victor Ibragimov <victor.ibragimov@gmail.com>
Language-Team: Tajik <kct_tj@yahoo.co.uk>
Language: tg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.0.1
Plural-Forms: nplurals=2; plural=n != 1;
  [Пинҳоншуда] &Тафсилот: &Нест кардан &Тасвирот: &Таҳрир &Файл &Ном: Зерменюи &Нав... Ҳамчун корбари &дигар иҷро кунед &Номи корбар: &Роҳи корӣ: (C) 2000-2003, Waldo Bastian, Raffaele Sandrini, Matthias Elter Иловагӣ &Фармон: Миёнбури &калиди ҷорӣ: H_Abrorova@rambler.ru Даргиронидани &боркунии алоқаи мутақобила Умумӣ Воридоти пинҳоншуда Номи пункт: Муҳаррири Менюи KDE Тағйири менюи KDE Панели асбобҳои умумӣ Ҳамроҳии ҷорӣ Matthias Elter Montel Laurent Абророва Хирмон &Пункти Нав... Пункти Нав &Тақсимкунандаи нав Зерменюи Нав Танҳо дар KDE нишон диҳед Муаллифи Ибтидоӣ Ҳамроҳии Гузашта Raffaele Sandrini Коргузорӣ кардан дар &поёна Тағиротҳои Менюро Захира кунам? Санҷиши имло Интихобҳои санҷиши имло Номи зерменю: &Интихобҳои поёна: Waldo Bastian Шумо ба меню тағиротҳо ворид кардед.
Мехоҳед, ки тағиротҳоро захира кунед ё онҳоро партоед? 