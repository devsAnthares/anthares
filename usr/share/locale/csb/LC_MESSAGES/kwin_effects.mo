��    �        �   
      �     �     �     �     �     �     �     �     �     �  	   �     �  	   �     �  
   �     
            %   3     Y     y  
   �     �     �     �     �  	   �     �     �     �  
        $     <     T  	   q  
   {     �     �     �     �     �     �     �  
   �     �                    4     F     a     r  ,   y  -   �     �     �     �               #     8     N     d     {  &   �      �     �  D   �     %  "   5     X     u     �  
   �     �     �  
   �  
   �     �     �     �          !  q   ?     �     �     �     �     �     
               )     .     >     E     T     Y     i     q     �     �  	   �     �     �     �     �     �     �  
   �  
   �     �     �     �               $  #   @     d     q  	   �     �     �     �     �     �     �     �     �     �  
   �     �  !      $   "     G     \  %   {  (   �  %   �  #   �            	   !     +  	   4     >     N     [  0   g     �  0   �     �  
                  0     D     Z     _  �  o     ]     `     g     �     �     �     �     �     �     �  	   �  
   �     �     �  
   �        #   	     -  
   :     E     V     c     x     �     �     �     �  /   �     �  
        *     F     b  
   }     �     �     �  
   �     �  
   �     �  &   �     �  
     
     )   #  '   M     u  "   �     �     �  2   �  2        ;     X     g     w  
   �     �  $   �     �     �       -     ,   K     x  X   �     �  
   �  
   �  
               .      @      F      O   
   b      m      �      �      �   '   �   o   �   #   a!     �!     �!     �!     �!     �!     �!     �!     �!     �!     "     "     /"     4"  	   L"     V"     u"     }"     �"     �"     �"     �"     �"     �"     �"     �"     �"     #     #     )#     /#     =#  #   W#  "   {#     �#     �#     �#     �#     �#     �#     $     $     '$     0$  
   6$     A$     O$     ]$  6   s$  4   �$     �$  #   �$  $   %  "   C%  1   f%  -   �%     �%  
   �%     �%  
   �%     �%     �%     &     &  B   %&  7   h&  4   �&  $   �&     �&     '     '     2'     L'  
   j'     u'                    H   M   E               �          F   |   #   r   �   l   2       e      V   �           3   �   �   �       o              N       U       "      �   c       y   &   �      1   ,       ;       )   �       v   {   Y   ^   4      D   9   >       '      g   L                   k      O   7      p   P   a              I       5   =       B           Q   �       6      z   w   Z          �       d   �       �   q       �           �   }      S   8   -              J       �      �   m   +      t   �   A   .   $           i      �               <   ]   @   `   0               n                  �               [   �      �   %   (   ?       �   *          T       !   C   �   j   �   ~       b   K   X   :   s   x       _   W      G       u   �                �   
       �   f      h   \   �   /             �   �   �       	   R            %  msec  pixel  pixels  px  ° &Color: &Height: &Layout mode: &Move factor: &Opacity: &Radius: &Spacing: &Stiffness: &Strength: &Width: &Wobbliness (Un-)Minimize window @title:tab Advanced SettingsAdvanced @title:tab Basic SettingsBasic Activate window Activation Additional Options Advanced Angle: Animate switch Animation Animation duration: Animation on tab box close Animation on tab box open Appearance Apply effect to &groups Apply effect to &panels Apply effect to the desk&top Automatic Background Background color: Bottom Bottom Left Bottom Right Bottom-Left Bottom-Right Bring window to current desktop Cap color: Caps Center Clear All Mouse Marks Clear Last Mouse Mark Clear Mouse Marks Close after mouse dragging Combobox popups: Custom Define how far away the object should appear Define how far away the windows should appear Desktop &name alignment: Desktop Cube Desktop Cylinder Desktop Sphere Dialogs: Display desktop name Display image on caps Display window &icons Display window &titles Do not animate panels Do not animate windows on all desktops Do not change opacity of windows Dra&g: Draw with the mouse by holding Shift+Meta keys and moving the mouse. Dropdown menus: Duration of flip animationDefault Duration of rotationDefault Duration of zoomDefault Enable &advanced mode End effect Far Faster Fill &gaps Filter:
%1 Flexible Grid Flip animation duration: Front color General Translucency Settings Horizontal position of front: If enabled the effect will be deactivated after rotating the cube with the mouse,
otherwise it will remain active Ignore &minimized windows Inactive windows: Inside Graph Invert cursor keys Invert mouse Layout mode: Left Left button: Less Maximum &width: Menus: Middle button: More Moving windows: Natural Natural Layout Settings Near Nicer No action Nowhere Opacity Opaque Pager Plane Popup menus: Rear color Reflection Reflections Regular Grid Right Right button: Rotation duration: Send window to all desktops Set menu translucency independently Show &panels Show Desktop Grid Show caps Show desktop Size Sphere Sphere Cap Deformation Tab 1 Tab 2 Text Text alpha: Text color: Text font: Text position: Toggle Flip Switch (All desktops) Toggle Flip Switch (Current desktop) Toggle Invert Effect Toggle Invert Effect on Window Toggle Present Windows (All desktops) Toggle Present Windows (Current desktop) Toggle Present Windows (Window class) Toggle Thumbnail for Current Window Top Top Left Top Right Top-Left Top-Right Torn-off menus: Translucency Transparent Use Present Windows effect to layout the windows Use pager layout for animation Use this effect for walking through the desktops Vertical position of front: Wallpaper: Windows Windows hover above cube Wo&bble when moving Wobble when &resizing Zoom Zoom &duration: Project-Id-Version: kwin_effects
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-02 03:18+0100
PO-Revision-Date: 2009-12-15 21:16+0100
Last-Translator: Mark Kwidzińśczi <mark@linuxcsb.org>
Language-Team: Kaszëbsczi <i18n-csb@linuxcsb.org>
Language: csb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)
  %  msek.  piksla  pikslë  pikslów  px  ° &Farwa: &Wiżô: Ôrt ù&stôwù: Faktor &rëszniô &Przezérnota: &Parmiń: &Rozstãp: &Cwiardota: &Mòc: &Szérzô: &Trząsc Zmniészanié/zwiékszanié òknów Awansowóné Spòdlowé Aktiwùjë òkno Aktiwacëjô Dodôwné òptacëje Awansowóné Kąt: Animùjë przestôwnika òknów Animacëjô Dérowanié animacëji: Animacëjô tacëniô sã przestôwnika òknów Animacëjô òtemkniãcô òkna Wëzdrzatk Ùżëjë efekt na &grëpie Ùżëjë efekt na &listwie Ùżëjë efekt na &pùlce Aùtomatno Spódk Farwa spódkù: Dół Dół-lewi Dół-prawi Dół-lewi Dół-prawi Przeniesë òkna do biéżnegò pùltu Farwa przëkriwka: Przëkriwk Westrzódk Wëczëszczë wszëtczé szlachë mëszë Wëczëszczë slédné szlachë mëszë Wëczëszczë szlachë mëszë Zakùńczë pò rëszeniô mëszą Òkna kòmbinowóné: Swòjò Definiujë jak dalek mô sã wëskrzënic òbiekt. Definiujë jak dalek mają sã wëskrzëniac òkna Pòłożenié &miona pùltu: Kòstka pùltu Cylinder pùltu Kùgla pùltu Dialodżi: Wëskrzëni miono pùltu Wëskrzëni òbrôzk na przëkriwkù Wëskrzëni &ikònë òknów Wëskrzëni &title òknów Nie animùjë panelów Nie animùjë òknów na wszëtczich pùltach Nie zmieniôj mòcë przëkrëwaniô òknów Prze&cygôj: Céchùjë mëszą przez przëtrzëmanié klawiszów Shift+Meta a przesëwając mëszã. Rozwijóné menu: Domëslné Domëslné Domëslné Aktiwùjë &awansowóny trib Zakùńczë efekt Dalek Chùtkò Dofùlëjë &luczi Filter:
%1 Elasticznô mrzéżka Dérowanié animacëji: Farwa przódkù Òglowi nastôw przezérnotë Hòrizontalné pòłożenié przódkù: Przë aktiwacëji efekt bãdze zdeaktiwòwóny pò rëszeniô kòstczi mëszą.
Jinaczi efekt òstanie aktiwny. Ignorëjë z&minimalizowóné òkna Nieaktiwné òkna: Bënowô grafika Òdwrócë farwã klawiszów Òdwrócr mësz Ôrt ùstôwù: Lewi Lewô knąpa: Mni Maksymalnô &szérzô: Menu: Westrzódnô knąpa: Wicy Przenoszenié òknów:: Naturalno Nastôw natularnegò ùstôwù Blëżi Snôżo Bez dzejaniô Nigdze Przezérnota Niéprzezérné Przestôwnik Rówizna Wëskakùjącé menu: Farwa spódkù Szpédżlowanié Szpédżlowania Regùlarnô mrzéżka Prawi Prawô knapa: Dérowanié òbrôcaniô: Sélôj òkna na wszëtczé pùltë Nastôwi ekstra przezérnotã menu Pòkażë &panele Pòkażë mrzéżkã pùltu Wëskrzëni przëkriwk Wëskrzëni pùlt Miara Kùgla Défòrmacëjô przëkriwka Kôrta 1 Kôrta 2 Tekst Text alfa: Farwa tekstu: Fònt tekstu: Pòłożenié tekstu: Przełączë trzëmiarowé sztaple (wszëtczé pùltu) Przełączë trzëmiarowé sztaple (aktualny pùltu) Włączë òbrôcony efekt Włączë òbrôcony efekt w òknie Włączë òkna wszëtczich pùltów Włączë òkna biéżnegò pùltu Przełączë prezentacëjã òknów (klasa òkna) Włączë miniaturkã dlô biéżnégò òkna Góra Góra-lewi Góra-prawi Góra-lewi Góra-prawi Òberwóné menu: Przezérnota Przezérné Brëkùjë fektu "Prezentëjë òkna" do pòrządkòwaniô òknów Brëkùjë przełącznika wëzdrzatków dlô animacëji Brëkùjë tegò efektu do rëchaniô sã pò pùlce Wertikalné pòłożenié przódkù: Òbrôzk spódkù: Òkna Wëcôgnąc òkno nad kòstkã &Trząsc przë rëchaniu  Trząsc przë zmianie mia&rë Wëzdrzatk &Dérowanié skalowaniô: 