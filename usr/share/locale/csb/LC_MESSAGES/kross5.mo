��            )   �      �  &   �     �     �     �      �     �                  ,   ;  3   h     �     �     �     �     �  '   �          ;     A     W     p     w     �     �     �  &   �     �  �  �     �     �  	   �     �  &     	   )     3     O     \  5   w  3   �  $   �  '        .     4     <  $   J  '   o     �     �     �     �     �     �     �     	  .   !	     P	                                       	                                              
                                                     @title:group Script propertiesGeneral Add a new script. Add... Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2009-12-04 22:09+0100
Last-Translator: Mark Kwidzińśczi <mark@linuxcsb.org>
Language-Team: Kaszëbsczi <i18n-csb@linuxcsb.org>
Language: csb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)
 Òglowé Dodôj nowi skript. Dodôj... Dopòwiesc: michol@linuxcsb.org, mark@linuxcsb.org Edicëjô Editëjë wëbróny skript. Edicëjô... Zrëszë wëbróny skript. Nie mòże ùsôdzëc skriptu dlô interpretera "%1". Ni mòże òznaczëc interpretera dlô skripta "%1" Ni mòże wladowac interpretera "%1" Ni mòże òtemknąc lopka skripta "%1" Lopk: Ikòna: Interpretera: Równiô bezpiekù interpretera Ruby Michôł Òstrowsczi, Mark Kwidzińsczi Miono: Felëje fùnkcëji "%1" Felëje interpreter "%1" Rëmôj Rëmôj wëbróny skript. Zrëszë Felëje lopk skripta "%1". Zatrzëmôj Zatrzëmôj zrëszenié wëbrónégò skriptu. Tekst: 