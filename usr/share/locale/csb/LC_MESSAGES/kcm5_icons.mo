��    9      �  O   �      �     �     �     �     
          #  �   >  -   �  )   �  -   "  +   P  r   |  	   �  	   �               #     ,  
   9     D     P     X     `      w     �     �     �      �     �     �  r   �  5   r     �     �     �  	   �     �     �     �  (   �     '	     5	     N	     h	     �	     �	  +   �	  3   �	     �	     �	     
     
  S    
  )   t
     �
  �   �
    �  	   �     �     �     �     �     �  �   �  1   �     �     �     �  u   �     K     Y     m     �  	   �     �     �  	   �     �  	   �  #   �  &   �          0     6  "   C     f     n  |   �  :        B  '   _     �     �     �  
   �     �  ,   �     �     �       "   8     [     b  6   r  .   �     �     �     �     	  e     /   |     �  �   �               1                &              0   '                7                            
          -               4      5   #      (       6   3          )                 /   	             9         +                    *      $      !   ,   .   "       8           %      2    &Amount: &Effect: &Second color: &Semi-transparent &Theme (c) 2000-2003 Geert Jansen <qt>Are you sure you want to remove the <strong>%1</strong> icon theme?<br /><br />This will delete the files installed by this theme.</qt> <qt>Installing <strong>%1</strong> theme</qt> @label The icon rendered as activeActive @label The icon rendered as disabledDisabled @label The icon rendered by defaultDefault A problem occurred during the installation process; however, most of the themes in the archive have been installed Ad&vanced All Icons Antonio Larrosa Jimenez Co&lor: Colorize Confirmation Desaturate Description Desktop Dialogs Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Effect Parameters Gamma Geert Jansen Get new themes from the Internet Icons Icons Control Panel Module If you already have a theme archive locally, this button will unpack it and make it available for KDE applications Install a theme archive file you already have locally Main Toolbar NAME OF TRANSLATORSYour names Name No Effect Panel Preview Remove Theme Remove the selected theme from your disk Set Effect... Setup Active Icon Effect Setup Default Icon Effect Setup Disabled Icon Effect Size: Small Icons The file is not a valid icon theme archive. This will remove the selected theme from your disk. To Gray To Monochrome Toolbar Torsten Rahn Unable to download the icon theme archive;
please check that address %1 is correct. Unable to find the icon theme archive %1. Use of Icon You need to be connected to the Internet to use this action. A dialog will display a list of themes from the http://www.kde.org website. Clicking the Install button associated with a theme will install this theme locally. Project-Id-Version: kcmicons
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-08 06:27+0100
PO-Revision-Date: 2009-10-20 18:05+0200
Last-Translator: Mark Kwidzińśczi <mark@linuxcsb.org>
Language-Team: Kaszëbsczi <i18n-csb@linuxcsb.org>
Language: csb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)
n%100>=20) ? 1 : 2);
n%100>=20) ? 1 : 2);
 &Lëczba: &Efekt: &Drëgô farwa: &Półprzezérny &Téma (c) 2000-2003 Geert Jansen <qt>Jes të gwës, że chcesz rëmnąc témã ikònów <strong>%1</strong>? <br /><br />To rëmnie lopczi zainstalowóné bez tã témã.</qt> <qt>Instalowanié <strong>%1</strong> témë</qt> Aktiwné Wëłączoné Domëszlné Wëstãpiła problema òbczas procesu instalacëji; równak, wikszi dzél témów w archiwùm òstôł zainstalowóny &Awansowóné Wszëtczé ikònë  Antonio Larrosa Jimenez &Farwa: Farwùjë Pòcwierdzenié Desaturacëjô Òpisënk Pùlt Dialodżi Przësënie abò wpiszë URL témë michol@linuxcsb.org, mark@linuxcsb.org Paramétrë efektów Gamma Geert Jansen Zladëjë nową témã z internetu Ikònë Mòdul sprôwiania ikònama Jeżlë môsz môlowò zarchiwizowóny lopk temë, to na knapa rozpakùje gò ë zrobi przëstãpnëm dlô aplikacëjów KDE Instalëjë zarchizowóny lopk témë jaczi môsz môlowò Przidnô lestew nôrzãdzów Michôł Òstrowsczi, Mark Kwidzińsczi Miono Felënk efektu Panel &Pòdzérk Rëmôj témã Rëmôj wëbróną témã z twòjegò diskù Ùstawi efekt... Ùstawi aktiwny efekt ikònë Ùstawi domëslny efekt ikònë Ùstawi wëłączony efekt ikònë Miara: Môłé ikònë Nen lopk nie je prôwdzewim archiwòm témë ikònów. Rëmnié wëbróną témã z twòjegò diskù. Do szarégò Do farwów mono Lestew nôrzãdzów Torsten Rahn Nie mòże zladowac archiwùm témë ikònów;
proszã sprôwdzë, czë na adresa %1 je prôwdzëwô. Nie mòże nalezc archiwùm témë ikònów %1. Brëkùnk ikònów Mùszisz bëc sparłãczony z internetã bë brëkòwac tegò dzéjaniô. Wëskrzëni ce sã lësta temów ze starnë http://www.kde.org. Klëkni na knąpã Instalëjë bë téma winstalowa sã môlowò. 