��          �   %   �      @  �  A            6   )  -   `     �     �     �     �  (   �  d        h          �     �     �     �     �     �        "   1     T     `     {  �  �  �  �     Z
     v
  :   �
  3   �
     �
             !     B  /   U  z   �                /     7     F     U     d     p     w          �     �     �                               	                                                                      
                         <p>You have chosen to open another desktop session.<br />The current session will be hidden and a new login screen will be displayed.<br />An F-key is assigned to each session; F%1 is usually assigned to the first session, F%2 to the second session and so on. You can switch between sessions by pressing Ctrl, Alt and the appropriate F-key at the same time. Additionally, the KDE Panel and Desktop menus have actions for switching between sessions.</p> Lists all sessions Lock the screen Locks the current sessions and starts the screen saver Logs out, exiting the current desktop session New Session Reboots the computer Restart the computer Shutdown the computer Starts a new session as a different user Switches to the active session for the user :q:, or lists all active sessions if :q: is not provided Turns off the computer Warning - New Session lock screen commandlock log out log out commandLogout log out commandlogout new session restart computer commandreboot restart computer commandrestart shutdown computer commandshutdown switch user switch user commandswitch switch user commandswitch :q: Project-Id-Version: krunner_sessions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2009-12-13 00:28+0100
Last-Translator: Mark Kwidzińśczi <mark@linuxcsb.org>
Language-Team: Kaszëbsczi <i18n-csb@linuxcsb.org>
Language: csb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)
 <p>Môsz wëbróné òtemkniãcé nowi sesëji pùltu.<br />Biéżnô sesëja bãdze zataconô a nowi ekran logòwaniô òstanie wëskrzëniony.<br /> Dlô kòżdi sesëji przëpisóny je jiny fùnkcyjny klawisz. F%1 je zwëkòwò dóny pierszi sesëji, F%2 drëdżi ëtd. Mòżesz przełącziwac so midze sesëjama wcëskając Ctrl+Alt+gwësny klawisz fùnkcyjny razã. Dodôwno, menu panela ë pùltu KDE mają akcëje dlô przełączania midze sesëjama.</p> Lëstë wszëtczich sesëji Blokùje ekran Blokùje biéżną sesëj@ ë startëje wëgaszôcz ekranu Wëlogòwùje, wëchôdô z biéżny sesëji pùltu Nowô sesja Pònowné zrëszenié kòmpùtra Pònowné zrëszenié kòmpùtra Zamëkô kòmpùtr Startëje nową sesëjã jakno jiny brëkòwnik Przełącziwô do aktiwny sesëji brëkòwnika :q:, abò lëstëje wszëtczé aktiwne sesëje, jeżlë :q: nie je pòdóny Wëłączô kòmpùtr Òstrzéga - Nowô sesja blokada wëlogòwanié Wëlogòwanié wëlogòwanié nowô sesja reboot restart zamkniãcé przełączô brëkòwnika przełączë przełączë :q: 