��          �            h  m   i     �  "   �  F     F   N  F   �  J   �     '  %   8  $   ^  $   �  &   �  �   �     W  �  j     L     d  .   u  k   �  l     k   }  q   �     [     v  	   �     �     �     �     �           	                                                             
        Close the encrypted container; partitions inside will disappear as they had been unpluggedLock the container Eject medium Finds devices whose name match :q: Lists all devices and allows them to be mounted, unmounted or ejected. Lists all devices which can be ejected, and allows them to be ejected. Lists all devices which can be mounted, and allows them to be mounted. Lists all devices which can be unmounted, and allows them to be unmounted. Mount the device Note this is a KRunner keyworddevice Note this is a KRunner keywordeject Note this is a KRunner keywordmount Note this is a KRunner keywordunmount Unlock the encrypted container; will ask for a password; partitions inside will appear as they had been plugged inUnlock the container Unmount the device Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2009-12-21 00:17+0100
Last-Translator: Mark Kwidzińśczi <mark@linuxcsb.org>
Language-Team: Kaszëbsczi <i18n-csb@linuxcsb.org>
Language: csb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)
 Òdblokùjë kònténer Wësënie medium Nalézë ùrządzenia z mionã pasownym do :q: Wëskrzëniô wszëtczé ùrządzenia ë zezwôlô na jich mòntowanié, òdmòntowanié abò wësëwanié. Wëskrzëniô wszëtczé ùrządzenia, chtërné mògą bëc wësëniãté ëzezwôlô na jich wësëwanié. Wëskrzëniô wszëtczé ùrządzenia, chtërné mògą bëc mòntowóné ëzezwôlô na jich mòntowanié. Wëskrzëniô wszëtczé ùrządzenia, chtërné mògą bëc òdmòntowóné ëzezwôlô na jich òdmòntowanié. Zamòntëjë ùrządzenié ùrządzenié wësënie zamòntëjë òdmòntëjë Zablokùjë kònténer Òdmòntëjë ùrządzenié 