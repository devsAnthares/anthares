��          �   %   �      P     Q     W  
   �  
   �     �     �            	   "      ,  *   M  H   x     �     �     �  	   �  (   �     '     ?     U     t     �     �     �  	   �  �  �     �  o   �     M     Z  	   c     m     |     �     �     �  %   �  E   �  
   (     3     E     T  @   `     �     �     �     �     	     *	  !   J	     l	                                                                                                         
          	           msec <h1>Multiple Desktops</h1>In this module, you can configure how many virtual desktops you want and how these should be labeled. Animation: Desktop %1 Desktop %1: Desktop Names Desktop Switching Desktops Duration: EMAIL OF TRANSLATORSYour emails Here you can enter the name for desktop %1 Here you can set how many virtual desktops you want on your KDE desktop. Layout NAME OF TRANSLATORSYour names No Animation Shortcuts Show shortcuts for all possible desktops Switch One Desktop Down Switch One Desktop Up Switch One Desktop to the Left Switch One Desktop to the Right Switch to Desktop %1 Switch to Next Desktop Switch to Previous Desktop Switching Project-Id-Version: kcm_kwindesktop
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-06 05:35+0100
PO-Revision-Date: 2009-12-20 19:12+0100
Last-Translator: Mark Kwidzińśczi <mark@linuxcsb.org>
Language-Team: Kaszëbsczi <i18n-csb@linuxcsb.org>
Language: csb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)
  msek. <h1>Wiele pùltów</h1>W tim mòdule mòżesz skònfigùrowac kùli pùltów chcesz ë jaczé mają miec miona. Animacëjô: Pùlt %1 Pùlt %1: Miona pùltów Przełączanié pùltów Pùltë Dérowanié: mark@linuxcsb.org Tuwò mòżesz wpisac miono pùltu %1 Tuwò mòżesz nastôwic jak wiele wirtualnych pùltów chcesz w KDE. Wëzdrzatk Mark Kwidzińsczi Bez animacëji Skrodzënë Wëskrzëni skrodzënë dlô wszëtczich przëstãpnych pùltów Skòkni jeden pùlt w dół Skòkni jeden pùlt w górã Skòkni jeden pùlt na lewò Skòkni jeden pùlt na prawò Skòkni do pùltu %1 Skòkni do nôslédnegò pùltu Skòkni do wczasniészegò pùltu Przełącznié 