��          �      L      �  m   �     /  "   <  F   _  F   �  F   �  J   4  N     R   �     !  %   2  $   X  #   }  $   �  %   �  &   �  �        �  ?  �     �       $     P   4  R   �  X   �  \   1	  U   �	  U   �	     :
     K
     S
  
   \
  	   g
  
   q
     |
     �
     �
                                
                          	                                          Close the encrypted container; partitions inside will disappear as they had been unpluggedLock the container Eject medium Finds devices whose name match :q: Lists all devices and allows them to be mounted, unmounted or ejected. Lists all devices which can be ejected, and allows them to be ejected. Lists all devices which can be mounted, and allows them to be mounted. Lists all devices which can be unmounted, and allows them to be unmounted. Lists all encrypted devices which can be locked, and allows them to be locked. Lists all encrypted devices which can be unlocked, and allows them to be unlocked. Mount the device Note this is a KRunner keyworddevice Note this is a KRunner keywordeject Note this is a KRunner keywordlock Note this is a KRunner keywordmount Note this is a KRunner keywordunlock Note this is a KRunner keywordunmount Unlock the encrypted container; will ask for a password; partitions inside will appear as they had been plugged inUnlock the container Unmount the device Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2011-02-15 16:43+0100
Last-Translator: Marko Dimjasevic <marko@dimjasevic.net>
Language-Team: Croatian <kde-croatia-list@lists.sourceforge.net>
Language: hr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Zaključaj spremnik Izbaci medij Nađi uređaj čije ime odgovara :q: Izlistaj sve uređaje i dozvoli im da budu montirani, demontirani ili izbačeni. Izlistaj sve uređaje koji mogu biti izbačeni i kojima je dozvoljeno izbacivanje. Izslistaj sve uređaje koji mogu biti montirani i kojima je dozvoljeno da budu motirani. Izlistaj sve uređaji koji mogu biti demontirani i kojima je dozvoljeno da budu demontirani. Izlistava sve uređaji koji mogu biti zaključani i omogućuje im da ih se zaključa. Izlistava sve uređaji koji mogu biti otključani i omogućuje im da ih se otključa. Montiraj uređaj uređaj izbaciti zaključaj montirati otključaj demontirati Otključaj spremnik Demontiraj uređaj 