��    #      4  /   L           	     !     .  +   5      a     �  ;   �     �     �                 >     _  9   m     �  3   �  	   �     �  )   �  %   )  |   O  [   �  5   (  *   ^     �     �     �     �     �  +   �  (        A  n   a  3   �  I       N
     c
     s
  )   |
  !   �
     �
  ;   �
          -  #   K  )   o     �     �  3   �     �  ;   �  	   9     C     X  $   x  t   �  L     >   _  &   �     �     �     �     �     �  )     '   =     e  y   �  /   �                          #      
                                         !             	          "                                                             %1 theme already exists Add Emoticon Add... Choose the type of emoticon theme to create Could Not Install Emoticon Theme Create a new emoticon Create a new emoticon by assigning it an icon and some text Delete emoticon Design a new emoticon theme Do you want to remove %1 too? Drag or Type Emoticon Theme URL EMAIL OF TRANSLATORSYour emails Edit Emoticon Edit the selected emoticon to change its icon or its text Edit... Emoticon themes must be installed from local files. Emoticons Emoticons Manager Enter the name of the new emoticon theme: Get new icon themes from the Internet If you already have an emoticon theme archive locally, this button will unpack it and make it available for KDE applications Insert the string for the emoticon.  If you want multiple strings, separate them by spaces. Install a theme archive file you already have locally Modify the selected emoticon icon or text  NAME OF TRANSLATORSYour names New Emoticon Theme Remove Remove Theme Remove the selected emoticon Remove the selected emoticon from your disk Remove the selected theme from your disk Require spaces around emoticons Start a new theme by assigning it a name. Then use the Add button on the right to add emoticons to this theme. This will remove the selected theme from your disk. Project-Id-Version: kcm_emoticons
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-04 06:03+0100
PO-Revision-Date: 2010-01-26 11:57+0100
Last-Translator: Andrej Dundovic <adundovi@gmail.com>
Language-Team: Croatian <kde-croatia-list@lists.sourceforge.net>
Language: hr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 %1 tema već postoji Dodaj emotikone Dodaj… Izaberite tip za stvaranje emotikon teme  Ne mogu instalirati emotikon temu Stvori novu emotikonu Stvori novu emotikonu pridružujući joj ikonu i neki tekst Izbriši emotikone Dizajniraj novu emotikon temu Da li želite ukloniti %1 također? Povucite ili unesite URL od emotikon teme zarko.pintar@gmail.com Uredi emotikone Uredi odabranu emotikonu promjenom ikone ili teksta Uredi… Emotikon teme moraju biti instalirane iz lokalnih datoteka. Emotikone Upravitelj emotikona Unesi naziv nove emotikon teme: Preuzmi nove teme ikona sa Interneta Ako već imate emotikon temu arhiviranu lokalno, ovaj će je gumb odpakirati i napravit idostupnom za KDE aplikacije Umetnite niz sa emotikonu. Ako želite više nizova, odvojite ih razmacima.  Instalirajte datoteku teme koju već imate na Vašem računalu Promijeni odabranu emotikonu ili tekst Žarko Pintar Nova emotikon tema Ukloni Ukloni temu Ukloni odabranu emotikonu Ukloni odabranu emotikonu sa vašeg diska Uklonite odabranu temu sa vašeg diska  Potreban prostor oko emotikone Započni novu temu pridružujući joj naziv. Tada koristie Dodaj gumb na desnoj strani za dodavanje emotikona u ovu temu. Ovo će izbrisati odabranu temu s Vašeg diska. 