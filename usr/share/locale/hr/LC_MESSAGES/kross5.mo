��            )   �      �  &   �     �     �     �     �      �               .     6  ,   S  3   �     �     �     �     �     �  '        4     S     Y     o     �     �     �     �     �  &   �     �  �  �     �     �     �  	   �  	   �  \   �     E     K     g     p  5   �  @   �  *   	  *   ,	  	   W	     a	     h	  %   p	  E   �	     �	     �	     �	     
     
     5
     =
     ]
  &   f
     �
                                       
                      	                                                                            @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2011-07-22 16:08+0200
Last-Translator: Marko Dimjašević <marko@dimjasevic.net>
Language-Team: Croatian <kde-croatia-list@lists.sourceforge.net>
Language: hr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Lokalize 1.2
X-Poedit-Language: Croatian
X-Poedit-Country: CROATIA
X-Poedit-Bookmarks: 1601,-1,-1,-1,-1,-1,-1,-1,-1,-1
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Opće Dodaj novu skriptu. Dodaj… Odustati? Komentar: renato@translator-shop.org, zarko.pintar@gmail.com, marko@dimjasevic.net, adundovi@gmail.com Uredi Promijeni odabranu skriptu. Uredi… Izvrši odabranu skriptu. Nije uspjelo stvaranje skripte za interpretator "%1". Neuspjelo određivanje interpretatora za skriptnu datoteku "%1". Neuspjelo učitavanje interpretatora "%1". Neuspjelo otvaranje skriptne datoteke "%1" Datoteka: Ikona: Tumač: Razina sigurnosti interpretera Rubyja Renato Pavičić, Žarko Pintar, Marko Dimjašević, Andrej Dundović Naziv: Nema takve funkcije "%1" Nema takvog tumača "%1" Ukloni Ukloni odabranu skriptu. Izvrši Datoteka skripte %1 ne postoji. Zaustavi Prekini izvršavanje odabrane skripte. Tekst: 