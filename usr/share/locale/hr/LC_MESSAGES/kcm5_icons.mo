��    9      �  O   �      �     �     �     �     
          #  �   >  -   �  )   �  -   "  +   P  r   |  	   �  	   �               #     ,  
   9     D     P     X     `      w     �     �     �      �     �     �  r   �  5   r     �     �     �  	   �     �     �     �  (   �     '	     5	     N	     h	     �	     �	  +   �	  3   �	     �	     �	     
     
  S    
  )   t
     �
  �   �
  D  �     �     �     �     �     �       �     /   �     �     �     �  j   
  	   u  	        �     �     �     �     �     �     �     �  &   �  7        @     S     X     e     �      �  �   �  9   0     j     ~     �  
   �     �     �     �     �     �     �            
   1  
   <  /   G  3   w  
   �     �     �     �  Q   �  /   .     ^  �   m               1                &              0   '                7                            
          -               4      5   #      (       6   3          )                 /   	             9         +                    *      $      !   ,   .   "       8           %      2    &Amount: &Effect: &Second color: &Semi-transparent &Theme (c) 2000-2003 Geert Jansen <qt>Are you sure you want to remove the <strong>%1</strong> icon theme?<br /><br />This will delete the files installed by this theme.</qt> <qt>Installing <strong>%1</strong> theme</qt> @label The icon rendered as activeActive @label The icon rendered as disabledDisabled @label The icon rendered by defaultDefault A problem occurred during the installation process; however, most of the themes in the archive have been installed Ad&vanced All Icons Antonio Larrosa Jimenez Co&lor: Colorize Confirmation Desaturate Description Desktop Dialogs Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Effect Parameters Gamma Geert Jansen Get new themes from the Internet Icons Icons Control Panel Module If you already have a theme archive locally, this button will unpack it and make it available for KDE applications Install a theme archive file you already have locally Main Toolbar NAME OF TRANSLATORSYour names Name No Effect Panel Preview Remove Theme Remove the selected theme from your disk Set Effect... Setup Active Icon Effect Setup Default Icon Effect Setup Disabled Icon Effect Size: Small Icons The file is not a valid icon theme archive. This will remove the selected theme from your disk. To Gray To Monochrome Toolbar Torsten Rahn Unable to download the icon theme archive;
please check that address %1 is correct. Unable to find the icon theme archive %1. Use of Icon You need to be connected to the Internet to use this action. A dialog will display a list of themes from the http://www.kde.org website. Clicking the Install button associated with a theme will install this theme locally. Project-Id-Version: kcmicons
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-08 06:27+0100
PO-Revision-Date: 2010-01-26 12:00+0100
Last-Translator: Andrej Dundovic <adundovi@gmail.com>
Language-Team: Croatian <kde-croatia-list@lists.sourceforge.net>
Language: hr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 &Količina: &Efekt: &Druga boja: &Poluprozirno &Tema © 2000–2003 Geert Jansen <qt>Jeste li sigurni da želite ukloniti temu ikona <strong>%1</strong>?<br /><br />Na ovaj ćete način izbrisati datoteke instalirane s ovom temom.</qt> <qt>Instaliranje teme: <strong>%1</strong></qt> Aktivno Onemogućeno Uobičajeno Tijekom postupka instalacije došlo je do problema. Veći dio tema iz arhive ipak je uspješno instaliran. &Napredno Sve ikone Antonio Larrosa Jimenez &Boja: Obojano Potvrda Nezasićeno Opis Radna površina Dialozi Prevucite ili upišite URL adresu teme renato@translator-shop.org, DoDoEntertainment@gmail.com Vrijednosti efekta Gama Geert Jansen Preuzmi nove teme s Interneta Ikone Modul kontrolnog panela za ikone Ako već imate datoteku teme na Vašem računalu, ovaj gumb će raspakirati temu iz datoteke i učiniti je dostupnom KDE aplikacijama Instalirajte datoteku teme koju imate na Vašem računalu Glavna alatna traka Renato Pavičić, Nenad Mikša Naziv Bez efekta Panel Pregled Ukloni temu Izbriši odabranu temu Postavi efekt… Efekt aktivne ikone Zadani efekt ikone Efekt  onemogućene ikone Veličina: Male ikone Ova datoteka nije valjana arhiva s temama ikona Ovo će izbrisati odabranu temu s Vašeg računala. Zasivljeno Sivi tonovi Alatna traka Torsten Rahn Nije moguće preuzeti arhivu s temama ikona.
Provjerite je li adresa %1 ispravna. Nije moguće pronaći arhivu s temama ikona %1. Upotreba ikona Morate biti spojeni na Internet da biste koristili ovu mogućnost. Dialog će biti prikazan s popisom tema sa web stranice http://www.kde.org. Ako kliknete na gumb Instaliraj pokraj teme, to će ju instalirati na Vaše računalo. 