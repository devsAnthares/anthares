��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  R  K     �  '   �  �  �  �  �	  c   I     �     �     �  %   �          -  -   H     v                                          
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch 0
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2006-12-13 22:31+0100
Last-Translator: Renato Pavicic <renato@translator-shop.org>
Language-Team: Croatian <kde-croatia-list@lists.sourceforge.net>
Language: hr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: TransDict server
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
  sek Vrijeme obavještavanja pri pokretanju: <H1>Obavijest u traci zadataka</H1>
Možete omogućiti drugi način obavještavanja pri pokretanju,
a koji upotrebljava traku zadataka. Prikazuje se gumb s
rotirajućim pješčanim satom koji obavještava o učitavanju
pokrenute aplikacije.
Može se dogoditi da neke aplikacije nisu svjesne ovakvog obavještavanja
tijekom pokretanja. U tom će slučaju pokazivač prestati s treptanjem
nakon razdoblja datog u odjeljku "Vrijeme obavještavanja pri
pokretanju". <h1>Zauzet pokazivač</h1>
KDE nudi zauzet pokazivač za obavještavanje tijekom pokretanja
aplikacija. Da biste omogućili zauzet pokazivač, s kombniniranog
popisa odaberite jednu vrstu vizualnog pokazivača.
Može se dogoditi da neke aplikacije nisu svjesne ovakvog obavještavanja
tijekom pokretanja. U tom će slučaju pokazivač prestati s treptanjem
nakon razdoblja datog u odjeljku "Vrijeme obavještavanja pri
pokretanju". <h1>Pokreni povratne podatke</h1> Na ovom mjestu možete konfigurirati povratne podatke aplikacija. Trepćući pokazivač Poskakujući pokazivač &Zauzet pokazivač Omogući obavijesti u &traci zadataka Bez zauzetog pokazivača Pasivni zauzeti pokazivač &Vrijeme obavještavanja prilikom pokretanja: Obavijest u traci &zadataka 