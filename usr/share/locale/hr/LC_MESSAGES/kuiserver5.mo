��          �   %   �      P     Q     b     w     �     �     �  
   �     �     �     �     �     �  /        3     Q     p     v     �     �     �     �     �     �     �       H  %  #   n     �     �      �     �     �       
             #     ,     @  7   R     �     �     �  	   �  
   �     �     �     �          ;     Z     t                            
                                                                                             	    %1 file %1 files %1 folder %1 folders %1 of %2 processed %1 of %2 processed at %3/s %1 processed %1 processed at %2/s Appearance Behavior Cancel Clear Configure... Finished Jobs List of running file transfers/jobs (kuiserver) Move them to a different list Move them to a different list. Pause Remove them Remove them. Resume Show all jobs in a list Show all jobs in a list. Show all jobs in a tree Show all jobs in a tree. Show separate windows Show separate windows. Project-Id-Version: kuiserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2011-02-15 16:27+0100
Last-Translator: Marko Dimjasevic <marko@dimjasevic.net>
Language-Team: Croatian <kde-croatia-list@lists.sourceforge.net>
Language: hr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Lokalize 1.2
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 %1 datoteka %1 datoteke %1 datoteka %1 mapa %1 mape %1 mapa %1 od %2 procesiran %1 od %2 procesiran brzinom %3/s %1 procesiran %1 procesiran brzinom %2/s Izgled Ponašanje Odustani Izbriši Konfiguriranje … Završeni poslovi Lista pokrenutih prijenosa datoteka/poslova (kuiserver) Pomakni ih u drugi popis Pomakni ih u drugi popis. Pauza Ukloni ih Ukloni ih. Nastavi Prikaži sve poslove u popisu Prikaži sve poslove u popisu. Prikaži sve poslove u stablu Prikaži sve poslove u stablu. Prikaži odvojene prozore Prikaži odvojene prozore. 