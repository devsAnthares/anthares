��    x      �  �   �      (
  	   )
     3
     :
     H
     T
     a
     i
     q
  
   x
     �
     �
     �
  
   �
     �
     �
  7   �
     �
     �
     	       
   (  
   3  	   >     H     V  
   p     {  ,   �  /   �     �     �     �     �  
   �     
  
        "     0     6     D  
   J     U     c     i     x     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �               .     =     B     P     c     o     {     �     �     �     �  B   �          2  /   R  =   �  #   �  *   �          (     0     6     D  
   J     U     a     f  	   {     �     �  
   �     �     �  
   �  
   �  	   �     �     �  
        "     8     =     C     J     R     ^     b     p  	   y     �     �     �     �     �  
   �     �  N  �          0     4     F     S     _     e     n     v     �     �     �     �     �     �  F   �  
             -  	   9     C     O     ]     f     u     �     �     �     �     �     �     �     �     �     �  
   �                    *     2     C     U     [     k      r     �     �     �     �     �  
   �     �     �     �     �     �                    #     3     E     `     s     z  
   �     �     �     �     �     �     �     �  <        O     d  )   }  .   �  #   �  '   �      "     C     L     Q     ^     b     s     �     �  
   �     �     �     �     �     �     �     �  
             (     >     L     c     j     q     y     �     �     �     �  
   �     �     �     �  
   �     
          !             %   G      _       t         l       g   j   '              K              M   0   /           W   w   v   x                  7   H      B   =   o          1   <         $   f         Y   E   c   >               Q       e       ]   #      8   [   X   P   5   2   U   b   )      u   
   L   i   Z   m       n   A   I           J   V   `   ;           T   \   h       S              O                        6   +                  F   4      "   k       !      9          R   *          N   (       p   ^   -                d          D   &   s   ,   3   r       a       	          :   C           q   @   ?   .    Benchmark Bottom Bottom to Top Bottom-left Bottom-right Buttons Cascade Center Checkboxes Description Dialog Document mode Down Arrow East Editors Emulates user interaction with widgets for benchmarking Enabled Example text First Column First Description First Item First Page First Row First Subitem First Subitem Description First item Flat Flat frame. No frame is actually drawn.Flat Flat group box. No frame is actually drawnFlat Frame Frames GroupBox Hide tabbar Horizontal Huge (48x48) Icons Only Input Widgets Large Large (32x32) Left  Left Arrow Left to Right Lists Medium (22x22) Modules Multi-line text editor: New New Row Normal North Off On Open Options Oxygen Demo Partial Password editor: Preview Raised Right Right Arrow Right to Left Right to left layout Run Simulation Save Second Column Second Description Second Item Second Page Second Subitem Second Subitem Description Second item Select Next Window Select Previous Window Select below the modules for which you want to run the simulation: Show Corner Buttons Shows the appearance of buttons Shows the appearance of lists, trees and tables Shows the appearance of sliders, progress bars and scrollbars Shows the appearance of tab widgets Shows the appearance of text input widgets Single line text editor: Sliders Small Small (16x16) South Tab Widget Tab Widgets Tabs Text Alongside Icons Text Only Text Under Icons Text and icon: Text only: Third Column Third Description Third Item Third Page Third Row Third Subitem Third Subitem Description Third item This is a sample text Tile Title Title: Toolbox Toolbuttons Top Top to Bottom Top-left Top-right Up Arrow Use flat buttons Use flat widgets Vertical West Wrap words password Project-Id-Version: kstyle_config
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-16 03:34+0200
PO-Revision-Date: 2011-03-18 18:29+0100
Last-Translator: Marko Dimjašević <marko@dimjasevic.net>
Language-Team: Croatian <kde-croatia-list@lists.sourceforge.net>
Language: hr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Lokalize 1.2
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Test performansi Dno Od dna prema vrhu Dolje lijevo Dolje desno Gumbi Kaskadno Sredina Odabirni okviri Opis Dijalog Način dokumenta Strelica prema dolje Istok Uređivači Oponaša interakciju korisnika s widgetima radi testiranja performansi Omogućeno Primjerni tekst Prvi stupac Prvi opis Prva stavka Prva stranica Prvi red Prva podstavka Opis prve podstavke Prva stavka Ravan Ravan Ravno Okvir Okviri Skupna kućica Sakrij traku s karticama Horizontalni Ogromna (48x48) Samo ikone Widgeti za unos Veliki Velika (32x32) Lijevo  Strelica ulijevo S lijeva na desno Liste Srednja (22x22) Moduli Višelinijski uređivač teksta: Novi Novi red Normalan Sjever Isključeno Uključeno Otvori Opcije Demonstracija Oxygena Djelomično Uređivač zaporki: Pregled Podignut Desno Strelica udesno S desna na lijevo Raspored s desna na lijevo Pokreni simulaciju Spremi Drugi stupac Drugi opis Druga stavka Druga stranica Druga podstavka Opis druge podstavke Druga stavka Odaberi sljedeći prozor Odaberi prethodni prozor Niže odaberite module za koje želite pokrenuti simulaciju: Prikaži kutne gumbe Prikazuje izgled gumbiju Prikazuje izgled lista, stabala i tablica Prikazuje izgled klizača i trake napredovanja Prikazuje izgled kartičnih widgeta Prikazuje izgled widgeta za unos teksta Jednolinijski uređivač teksta: Klizači Mali Mala (16x16) Jug Kartični widget Kartični widgeti Kartice Tekst uz ikone Samo tekst Tekst ispod ikona Tekst i ikona: Samo tekst: Treći stupac Treći opis Treća stavka Treća stranica Treći red Treća podstavka Opis treće podstavke Treća stavka Ovo je primjerni tekst Naslov Naslov Naslov: Okvir s alatima Alatni gumbi Vrh Od vrha prema dnu Gore lijevo Gore desno Strelica prema gore Koristi ravne gumbe Koristi ravne gumbe Vertikalni Zapad Prelamaj riječi zaporka 