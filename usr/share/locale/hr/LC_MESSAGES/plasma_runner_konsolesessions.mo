��          4      L       `   $   a   /   �   <  �   ,   �  0                        Finds Konsole sessions matching :q:. Lists all the Konsole sessions in your account. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2009-11-10 14:03+0100
Last-Translator: Andrej Dundovic <adundovi@gmail.com>
Language-Team: Croatian <kde-croatia-list@lists.sourceforge.net>
Language: hr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Nalazi Konzoline sesije koje odgovaraju :q:. Popisuje sve Konzoline sesije na vašem računu. 