��          �            x     y     }     �     �     �  S   �  w   !  M   �     �  	   �     �          %     2     R  N  d     �     �  !   �     �       T     \   r  H   �       
   *     5     S     c     |     �                                              	          
                        ms &Reactivation delay: &Switch desktop on edge: Activation &delay: Always Enabled Amount of time required after triggering an action until the next trigger can occur Amount of time required for the mouse cursor to be pushed against the edge of the screen before the action is triggered Change desktop when the mouse cursor is pushed against the edge of the screen Lock Screen No Action Only When Moving Windows Other Settings Show Desktop Switch desktop on edgeDisabled Window Management Project-Id-Version: kcmkwinscreenedges
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-30 03:13+0100
PO-Revision-Date: 2010-03-18 11:06+0100
Last-Translator: Andrej Dundovic <adundovi@gmail.com>
Language-Team: Croatian <kde-croatia-list@lists.sourceforge.net>
Language: hr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 ms Odgoda do &ponovne aktivacije: &Prebaci radnu površinu na rubu: O&dgoda aktivacije: Uvijek omogućeno Vrijeme koje je potrebno da se aktivira sljedeća radnja nakon aktivacije posljednje Vrijeme potrebno da se aktivira radnja nakon što se pokazivač miša primakne rubu zaslona  Promijeni radnu površinu kad se pokazivač miša primakne rubu zaslona  Zaključaj zaslon Bez radnje Samo prilikom micanja prozora Ostale postavke Prikaži radnu površinu Onemogućeno Upravljanje prozorima 