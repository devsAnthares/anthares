��    #      4  /   L           	       
   �  +   �  
   �     �     �     �           %     7     W  	   `      j  �   �  e   )  *   �  H   �          
     )  )   6  ;   `  	   �     �  (   �     �               ;     [     p     �  	   �  O  �     �	  �   
  
   �
  3   �
     �
     �
      �
       *   -     X  #   v     �  	   �  *   �  �   �  {   �  0     m   7     �     �     �  :   �  B        X  -   a  /   �  (   �  '   �  '     '   8     `  %   ~  $   �     �        !                	                                                                                   
                #                    "               msec <h1>Multiple Desktops</h1>In this module, you can configure how many virtual desktops you want and how these should be labeled. Animation: Assigned global Shortcut "%1" to Desktop %2 Desktop %1 Desktop %1: Desktop Effect Animation Desktop Names Desktop Switch On-Screen Display Desktop Switching Desktop navigation wraps around Desktops Duration: EMAIL OF TRANSLATORSYour emails Enable this option if you want keyboard or active desktop border navigation beyond the edge of a desktop to take you to the opposite edge of the new desktop. Enabling this option will show a small preview of the desktop layout indicating the selected desktop. Here you can enter the name for desktop %1 Here you can set how many virtual desktops you want on your KDE desktop. Layout NAME OF TRANSLATORSYour names No Animation No suitable Shortcut for Desktop %1 found Shortcut conflict: Could not set Shortcut %1 for Desktop %2 Shortcuts Show desktop layout indicators Show shortcuts for all possible desktops Switch One Desktop Down Switch One Desktop Up Switch One Desktop to the Left Switch One Desktop to the Right Switch to Desktop %1 Switch to Next Desktop Switch to Previous Desktop Switching Project-Id-Version: kcm_kwindesktop
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-06 05:35+0100
PO-Revision-Date: 2010-07-11 15:05+0200
Last-Translator: Andrej Dundovic <andrej@dundovic.com.hr>
Language-Team: Croatian <kde-croatia-list@lists.sourceforge.net>
Language: hr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 msek <h1>Višestruke radne površine</h1>U ovom modulu možete podesiti koliko želite virtualnih radnih površina i kako će one biti označene. Animacija: Pridružen opći prečac "%1" na radnu površinu %2 Radna površina %1 Radna površina %1: Efekt animacije radne površine  Nazivi radnih površina Prekidač radne površine na zaslonu (OSD) Prebacivanje radnih površina Navigacija radne površine se omata Radne površine Trajanje: zarko.pintar@gmail.com, adundovi@gmail.com Omogućite ovu opciju ako želite da vas tipkovnica ili aktivna granica radne površine navigira preko ruba radne površine i odvede na suprotni rub nove radne površine. Omogućavanjem ove opcije dobit će se prikaz sličice s izgledom radne površine koja ukazuje na odabranu radnu površinu. Ovdje možete unjeti naziv za radnu površinu %1 Ovdje možete postaviti koliko mnogo virtualnih radnih površina želite na vašoj KDE-ovoj radnoj površini. Prikaz Žarko Pintar, Andrej Dundović Bez animacije Nije pronađen odgovarajući prečac za radnu površinu %1 Sukob prečaca: Ne mogu postaviti prečac %1 na radnu površinu %2 Prečaci Prikaži indikatore rasporeda radne površine Pokaži prečace za sve moguće radne površine Prebaci za jednu radnu površinu nadolje Prebaci za jednu radnu površinu nagore Prebaci za jednu radnu površinu uljevo Prebaci za jednu radnu površinu udesno Prebaci na radnu površinu %1 Prebaci na slijedeću radnu površinu Prebaci na prethodnu radnu površinu Prebacivanje 