��          <      \       p   !   q   3   �      �   =  �   *     7   B     z                   Finds Kate sessions matching :q:. Lists all the Kate editor sessions in your account. Open Kate Session Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-18 03:08+0100
PO-Revision-Date: 2010-01-21 11:39+0100
Last-Translator: Andrej Dundović <adundovi@gmail.com>
Language-Team: Croatian <kde-croatia-list@lists.sourceforge.net>
Language: hr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Nalazi Kateove sesije koje odgovaraju :q:. Popisuje sve sesija uređivača Kate na vašem računu. Otvori sesiju Katea 