��          �   %   �      P  �  Q          )  6   9  -   p     �     �     �     �  (   �  d        x     �     �     �     �     �     �                7  "   X     {     �     �  �  �  �  Y     T
     l
  A   }
  C   �
               (     =  0   ]  h   �     �  	             -     3     E     Y     m     z  	   �     �     �     �     �                               	                                                                      
                        <p>You have chosen to open another desktop session.<br />The current session will be hidden and a new login screen will be displayed.<br />An F-key is assigned to each session; F%1 is usually assigned to the first session, F%2 to the second session and so on. You can switch between sessions by pressing Ctrl, Alt and the appropriate F-key at the same time. Additionally, the KDE Panel and Desktop menus have actions for switching between sessions.</p> Lists all sessions Lock the screen Locks the current sessions and starts the screen saver Logs out, exiting the current desktop session New Session Reboots the computer Restart the computer Shutdown the computer Starts a new session as a different user Switches to the active session for the user :q:, or lists all active sessions if :q: is not provided Turns off the computer User sessionssessions Warning - New Session lock screen commandlock log out log out commandLogout log out commandlogout new session restart computer commandreboot restart computer commandrestart shutdown computer commandshutdown switch user switch user commandswitch switch user commandswitch :q: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2011-01-11 10:21+0100
Last-Translator: g.sora <g.sora@tiscali.it>
Language-Team: Interlingua <kde-i18n-it@kde.org>
Language: ia
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=2; plural=n != 1;
 <p>Tu ha seligite de aperir un altere session de scriptorio.<br />Le session currente essera celate e un nove schermo de accesso de identification essera monstrate.<br />Un clave-F es assignate a cata session; F%1 es usualmente assignate al prime the session, F%2 al secunde session e assi facente. Tu pote commutar inter sessiones per pressar Ctrl, Alt e le appropriate clave-F in le mesme tempore. Additionalmente, le menus de Pannello de KDE e de Scriptorio ha actiones pro commutar inter sessiones.</p> Il lista omne sessiones Bloca le schermo Il bloca le sessiones currente e il initia le salvator de schermo Il claude le session, abandonante le currente session de scriptorio Nove session Re-starta le computator Re-starta computator Stoppa (shutdown) le computator Il initia un nove session como differente usator Il commuta a le session active pro le usator :q:, o il lista omne active sessiones si :q: non es fornite Extingue le computator sessiones Aviso - Nove session bloca claude de session Clausura de session Clausura de session nove session reboot (re-initia) re-starta stoppa (shutdown) commuta usator commuta commuta a :q: 