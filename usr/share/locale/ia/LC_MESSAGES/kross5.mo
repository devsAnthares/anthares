��    $      <  5   \      0     1  +   E     q  4   �  &   �     �     �                     5     :     P     X  ,   u  3   �     �     �               !  '   .     V     u     {     �     �     �     �     �     �  &   �       B        b  �  y               0     @     V     ^     s  	   {  	   �     �     �     �  	   �     �  3   �  @   #	  &   d	  *   �	     �	     �	     �	  )   �	     �	     
     
     +
     A
     H
     g
  "   n
     �
  +   �
     �
  ;   �
                                            
                              !                                  $      #                               	                           "             @info:creditAuthor @info:creditCopyright 2006 Sebastian Sauer @info:creditSebastian Sauer @info:shell command-line argumentThe script to run. @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: application descriptionCommand-line utility to run Kross scripts. application nameKross Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2014-04-04 15:53+0200
Last-Translator: G.Sora <g.sora@tiscali.it>
Language-Team: Interlingua <kde-l10n-ia@kde.org>
Language: ia
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 Autor (C) 2006 Sebastian Sauer Sebastian Sauer Le script de exequer. General Adde un nove script. Adde... Cancella? Commento: g.sora@tiscali.it Edita Edita le selectionate script. Edita ... Executa le script selectionate. Il falleva a crear le script pro le interprete "%1" Il falleva a determinar le interprete pro le file de script "%1" Il falleva a cargar le interprete "%1" Il falleva a aperir le file de script "%1" File: Icone: Interprete: Nivello de securitate del interprete Ruby Giovanni Sora Nomine: Non existe le function "%1" Nulle interprete "%1" Remove Remove le selectionate script. Exeque Le file de script  "%1" non existe Stoppa Stoppa le execution del script selectionate Texto: Utilitate de linea de commando per exequer scripts de Kross Kross 