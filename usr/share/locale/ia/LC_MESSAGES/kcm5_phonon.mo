��    B      ,  Y   <      �  i   �  m        y  b   �     �     	  6        O  7   _     �     �  	   �     �  (   �  )   �  )   (     R     o  Y   u     �     �  �   �      y	  .   �	     �	  
   �	     �	     �	     
     
     !
     5
     B
     J
     c
     r
     w
     �
     �
     �
     �
     �
  	   �
  
   �
     �
     �
  	     
     
   *     5     B  	   `     j     o  
   �  �   �     (  8   8  �   q     �  8   	  ,   B  ,   o  %   �     �  �  �  `   u  y   �  #   P  m   t     �       G        ]  G   p     �     �  	   �     �  <     <   @  <   }  %   �     �  e   �     O     l  �   �       1   /     a     o          �     �     �     �     �     �  &        )     8     =  !   K     m     |     �     �     �     �     �     �     �               !  !   5  	   W     a     h     �  �   �     @  N   P  �   �     6  F   I  J   �  C   �  -        M     $   !   %          ;   ,          B                 8   #                 7   9       +              )   ?   -   <         6                                    *   2       >                1       @       .   5   3   :   A         &      /                4              0      "             =   (   '         	   
           @info User changed Phonon backendTo apply the backend change you will have to log out and back in again. A list of Phonon Backends found on your system.  The order here determines the order Phonon will use them in. Apply Device List To... Apply the currently shown device preference list to the following other audio playback categories: Audio Hardware Setup Audio Playback Audio Playback Device Preference for the '%1' Category Audio Recording Audio Recording Device Preference for the '%1' Category Backend Colin Guthrie Connector Copyright 2006 Matthias Kretz Default Audio Playback Device Preference Default Audio Recording Device Preference Default Video Recording Device Preference Default/Unspecified Category Defer Defines the default ordering of devices which can be overridden by individual categories. Device Configuration Device Preference Devices found on your system, suitable for the selected category.  Choose the device that you wish to be used by the applications. EMAIL OF TRANSLATORSYour emails Failed to set the selected audio output device Front Center Front Left Front Left of Center Front Right Front Right of Center Hardware Independent Devices Input Levels Invalid KDE Audio Hardware Setup Matthias Kretz Mono NAME OF TRANSLATORSYour names Phonon Configuration Module Playback (%1) Prefer Profile Rear Center Rear Left Rear Right Recording (%1) Show advanced devices Side Left Side Right Sound Card Sound Device Speaker Placement and Testing Subwoofer Test Test the selected device Testing %1 The order determines the preference of the devices. If for some reason the first device cannot be used Phonon will try to use the second, and so on. Unknown Channel Use the currently shown device list for more categories. Various categories of media use cases.  For each category, you may choose what device you prefer to be used by the Phonon applications. Video Recording Video Recording Device Preference for the '%1' Category  Your backend may not support audio recording Your backend may not support video recording no preference for the selected device prefer the selected device Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2014-04-08 14:28+0200
Last-Translator: G.Sora <g.sora@tiscali.it>
Language-Team: Interlingua <kde-l10n-ia@kde.org>
Language: ia
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 Pro applical le modification de retro administration tu debe abandonar e identificar te de nove. Un lista de retro-administration de Phonon trovava se sur tu systema. Le ordine hic determina le ordine que Phonon usara. Applica lista de dispositivos a ... Applica le lista de preferentias currentemente monstrate al sequente altere categorias de reproduction audio: Configuration Hardware de audio Reproduction audio Preferentia de dispositivo de reproduction audio  per le categoria '%1' Registration Audio Preferentia de dispositivo de registration audio  per le categoria '%1' Retro-Administration Colin Guthrie Connector Copyright 2006 Matthias Kretz Preferentia de dispositivo de reproduction audio predefinite Preferentia de dispositivo de registration audio predefinite Preferentia de dispositivo de registration video predefinite Categoria Predefinite/Non specificate Postpone Il defini le ordine predefinite de dispositivos que pote esser supplantate per categorias individual. Configuration de dispositivo Preferentia de Dispositivo On trovava dispositivos sur tu systema, convenibile per le categoria seligite. Tu selige le dispositivo que tu vole esser usate per le applicationes. g.sora@tiscali.it Il falleva fixar le seligite dispositivo de exito Fronte centro Fronte sinistre Fronte sinistre de centro Fronte dextere Fronte dextere de centro Hardware Dispositivos independente Nivellos de ingresso Invalide Configuration Hardware de Audio de KDE Matthias Kretz Mono Giovanni Sora Modulo de configuration de Phonon Reproduce (%1) Prefere Profilo Posterior centro Posterior sinistre Posterior dextere Registration (%1) Monstra dispositivos avantiate Latere sinistre Latere dextere Carta de sono  Dispositivo de sono Position e essayo de altoparlator Subwoofer Essaya Essaya le dispositivo seligite Essayante %1 Le ordine determina le preferentias de dispositivos. Si pro alcun ration le prime dispositivo non pote esser usate Phonon tentara de usar le secunde, e assi il continuara. Canal Incognite Usa le lista de dispositivo monstrate currentemente poro ulterior categorias.  Varie categorias de casos de uso de media.  Pro cata categoria tu pote seliger a qual dispositivo tu prefere que es usate per applicationes de Phonon. Registration video Preferentia de dispositivo de registration video per le categoria '%1' Tu retro-administration poterea non supportar registration de sono (audio) Tu retro-administration poterea non supportar registration de video Nulle preferentia pro le dispositivo seligite prefere le dispositivo seligite 