��    *      l  ;   �      �     �     �     �     �  	   �  A   �  >   -  9   l  9   �  9   �  W     E   r     �     �      �     �  7         E     f  X   {     �     �     �     
     )     /     F  
   V  
   a     l     �     �     �     �     �     �                 	   6     @  �  H     �	     �	     �	     
  	   (
     2
  
   B
     M
     S
     X
  +   e
     �
     �
     �
     �
  )   �
  >   �
  &   8      _  i   �     �  !   �           6     D      L     m  
   �  
   �     �     �     �     �  .   �     
          "     (  #   ;     _     k            (                                            
      $                           )   "      %             #                          *           '      !      &                                  	    Accurate Always Animation speed: Author: %1
License: %2 Automatic Category of Desktop Effects, used as section headerAccessibility Category of Desktop Effects, used as section headerAppearance Category of Desktop Effects, used as section headerCandy Category of Desktop Effects, used as section headerFocus Category of Desktop Effects, used as section headerTools Category of Desktop Effects, used as section headerVirtual Desktop Switching Animation Category of Desktop Effects, used as section headerWindow Management Configure filter Crisp EMAIL OF TRANSLATORSYour emails Enable compositor on startup Exclude Desktop Effects not supported by the Compositor Exclude internal Desktop Effects Full screen repaints Hint: To find out or configure how to activate an effect, look at the effect's settings. Instant KWin development team Keep window thumbnails: NAME OF TRANSLATORSYour names Never Only for Shown Windows Only when cheap OpenGL 2.0 OpenGL 3.1 OpenGL Platform InterfaceEGL OpenGL Platform InterfaceGLX Re-enable OpenGL detection Re-use screen content Rendering backend: Scale method: Search Smooth Smooth (slower) Tearing prevention ("vsync"): Very slow XRender Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2014-07-25 14:43+0200
Last-Translator: Giovanni Sora <g.sora@tiscali.it>
Language-Team: Interlingua <kde-i18n-doc@kde.org>
Language: ia
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 Accurate Sempre Velocitate de animation: Autor: %1
Licentia: %2 Automatic Accessibilitate Apparentia Candy Foco Instrumentos Animation de commutar de scriptorio virtual Gestion de Fenestra Configura Filtro Crispe g.sora@tiscali.it Habilita effectos de compositor al initio Exclude effectos de scriptorio non supportate pe le compositor Exclude effectos interne de scriptorio Il repingi schermo completemente Suggestion: Pro discoperir o configurar como activar un effecto, tu guarda a le preferentias de effectos. Instante Equipa de disveloppamento de KWin Mantene miniaturas de fenestras: Giovanni Sora Jammais Solmente pro fenestras monstrate Solo quando incostose OpenGL 2.0 OpenGL 3.1 EGL GLX Re-habilita detection de openGL Reusa contento de schermo Retro administration de rendition (rendering): Methodo de scala: Cerca Lisie Lisie (plus lente) Prevention de laceration ("vsync"): Multe lente XRender 