��          �      L      �  m   �     /  "   <  F   _  F   �  F   �  J   4  N     R   �     !  %   2  $   X  #   }  $   �  %   �  &   �  �        �  �  �     F     [  8   l  V   �  Z   �  V   W  \   �  _   	  e   k	     �	     �	     �	     �	      
     
     
     
     0
                                
                          	                                          Close the encrypted container; partitions inside will disappear as they had been unpluggedLock the container Eject medium Finds devices whose name match :q: Lists all devices and allows them to be mounted, unmounted or ejected. Lists all devices which can be ejected, and allows them to be ejected. Lists all devices which can be mounted, and allows them to be mounted. Lists all devices which can be unmounted, and allows them to be unmounted. Lists all encrypted devices which can be locked, and allows them to be locked. Lists all encrypted devices which can be unlocked, and allows them to be unlocked. Mount the device Note this is a KRunner keyworddevice Note this is a KRunner keywordeject Note this is a KRunner keywordlock Note this is a KRunner keywordmount Note this is a KRunner keywordunlock Note this is a KRunner keywordunmount Unlock the encrypted container; will ask for a password; partitions inside will appear as they had been plugged inUnlock the container Unmount the device Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-12-17 13:07+0100
Last-Translator: g.sora <g.sora@tiscali.it>
Language-Team: Interlingua <kde-i18n-it@kde.org>
Language: ia
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=2; plural=n != 1;
 Bloca le receptaculo Expelle le medio Il trova dispositivos cuje nomine es coincidente con :q: Il lista omne dispositivos e il permitte lor de esser montate, dismontate o expellite. Il lista omne dispositivos que pote esser expellite, e il permitte lor de esser expellite. Il lista omne dispositivos que pote esser montate, e il permitte lor de esser montate. Il lista omne dispositivos que pote esser dismontate, e il permitte lor de esser dismontate. Il lista omne dispositivos cryptate que pote esser montate, e il permitte lor de esser montate. Il lista omne dispositivos cryptate que pote esser dismontate, e il permitte lor de esser dismontate. Monta le dispositivo dispositivo expelle bloca monta disbloca dismonta Disbloca le receptaculo Dismonta le dispositivo 