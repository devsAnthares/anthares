��    *      l  ;   �      �  A   �     �  /        2     ;     O     a     w     �     �  	   �     �  3   �  	   �     �      �  $      *   E     p  	   u  5        �     �  
   �     �          $  5   3  6   i     �  ,   �  /   �     	        O   0  Z   �  b   �  	   >  
   H  P   S     �  �  �  A   L
     �
  ;   �
     �
     �
       '   .     V     k     q  	   ~     �  4   �     �     �     �  (   �     %     -     4  =   B  !   �     �  
   �     �     �     �  ?   �  D   9     ~  ;   �     �     �     �  o     q   t  `   �     G     V  a   e     �            )             &      $   	   %         '                                                !      
       *                     "                                                 (                 #    %1 is an external application and has been automatically launched (c) 2009, Ben Cooksley <i>Contains 1 item</i> <i>Contains %1 items</i> About %1 About Active Module About Active View About System Settings Apply Settings Author Ben Cooksley Configure Configure your system Determines whether detailed tooltips should be used Developer Dialog EMAIL OF TRANSLATORSYour emails Expand the first level automatically General config for System SettingsGeneral Help Icon View Internal module representation, internal module model Internal name for the view used Keyboard Shortcut: %1 Maintainer Mathias Soeken NAME OF TRANSLATORSYour names No views found Provides a categorized icons view of control modules. Provides a classic tree-based view of control modules. Relaunch %1 Reset all current changes to previous values Search through a list of control modulesSearch Show detailed tooltips System Settings System Settings was unable to find any views, and hence has nothing to display. System Settings was unable to find any views, and hence nothing is available to configure. The settings of the current module have changed.
Do you want to apply the changes or discard them? Tree View View Style Welcome to "System Settings", a central place to configure your computer system. Will Stephenson Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:08+0100
PO-Revision-Date: 2010-12-17 17:37+0100
Last-Translator: g.sora <g.sora@tiscali.it>
Language-Team: Interlingua <kde-i18n-it@kde.org>
Language: ia
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.0
 %1 es un application externe e ha essite lanceate automaticamente (c) 2009, Ben Cooksley <i>Il contine 1 elemento</i> <i>Il contine %1 elementos</i> A proposito de %1 a proposito del Modulo Active A proposito del vista active A proposito del Preferentias de Systema Applica Preferentias Autor Ben Cooksley Configura Il configura tu systema Il determina si debe esser usate consilios detaliate Developpator Dialogo g.sora@tiscali.it Expande le prime nivello automaticamente General Adjuta Vista a Icone representation del modulo interne, modello del modulo interne Nomine interne pro le vista usate Via Breve de Claviero: %1 Mantenitor Mathias Soeken Giovanni Sora Nulle vistas trovate Il forni un vista de icones a categoria del modulos de controlo Il forni un classic vista basate sur arbore del modulos de controlo. Il re-lancea %1 Il reporta tote le currente variationes a le valores previe Cerca Monstra consilios detaliate Preferentias de Systema Preferentias de Systema non esseva habile de trovar alicun vistas, e ergo nihil esseva disponibile de monstrar. Preferentias de Systema non esseva habile de trovar alicun vistas, e ergo nihil esseva disponibile de configurar. Le preferentias del modulo currente ha cambiate.
Tu vole applicar le variationes o rejectar los? Vista a arbore Stilo de Vista Benvenite a "Preferentias de Systema", un placia central per configurar tu systema de computator. Will Stephenson 