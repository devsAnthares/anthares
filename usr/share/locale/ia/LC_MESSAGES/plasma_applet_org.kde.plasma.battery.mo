��          �      \      �  7   �  	   	               (     ;     S     a     i     }     �  #   �  %   �     �  5   �     .     =     K  )   S  �  }          "     .     7     C  !   \     ~     �  '   �     �     �     �     �  #   �  E        L     ^  	   o     y                                                  	                       
                          Battery is currently not present in the bayNot present Capacity: Charging Discharging Display Brightness Enable Power Management Fully Charged General Keyboard Brightness Model: Not Charging Placeholder is battery capacity%1% Placeholder is battery percentage%1% Power management is disabled The battery applet has enabled system-wide inhibition Time To Empty: Time To Full: Vendor: battery percentage below battery icon%1% Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-03 03:15+0100
PO-Revision-Date: 2014-01-10 13:24+0100
Last-Translator: G.Sora <g.sora@tiscali.it>
Language-Team: Interlingua <kde-l10n-ia@kde.org>
Language: ia
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 Non presente Capacitate: Cargante Discargante Brillantia de Monstrator habilita le Gestion de Energia... Completemente cargate General Intensitate de illumination de claviero Modello: Non cargante %1% %1% Gestion de energia es dishabilitate Le applet de batteria ha habilitate le inhibition pro omne le systema Tempore al vacue: Tempore al plen: Venditor: %1% 