��    >        S   �      H     I     R     [     j     |     �  �  �  �   y  -   	  )   3	  -   ]	  +   �	  r   �	  	   *
  	   4
     >
     V
     ^
     g
  
   t
     
     �
     �
     �
      �
     �
     �
     �
      �
            r   :  5   �     �     �                -     L  	   Q     [     a     i  (   v     �     �     �     �     �       +     3   9     m     u     �     �  "   �  S   �  )        9  �   E  �  #     �  	   �     �     �     �     �      �   +  ,   �     �     �     �  z   	  
   �     �     �     �     �     �  	   �     �  
   �     �     �          '     =     C  !   P     r  (   y  ~   �  <   !     ^     p     �     �     �     �     �     �     �     �  $   	     .  !   >  &   `  (   �  
   �     �  3   �  ,   �     )     0     =     R  !   _  h   �  3   �       �   +     )            5                        
   8             2   -          $   %      ;   4              <      1   7                 #   6       &       "       	            3      ,            :   (       9                 =   0   '   /              .       +       *                 >         !        &Amount: &Effect: &Second color: &Semi-transparent &Theme (c) 2000-2003 Geert Jansen <h1>Icons</h1>This module allows you to choose the icons for your desktop.<p>To choose an icon theme, click on its name and apply your choice by pressing the "Apply" button below. If you do not want to apply your choice you can press the "Reset" button to discard your changes.</p><p>By pressing the "Install Theme File..." button you can install your new icon theme by writing its location in the box or browsing to the location. Press the "OK" button to finish the installation.</p><p>The "Remove Theme" button will only be activated if you select a theme that you installed using this module. You are not able to remove globally installed themes here.</p><p>You can also specify effects that should be applied to the icons.</p> <qt>Are you sure you want to remove the <strong>%1</strong> icon theme?<br /><br />This will delete the files installed by this theme.</qt> <qt>Installing <strong>%1</strong> theme</qt> @label The icon rendered as activeActive @label The icon rendered as disabledDisabled @label The icon rendered by defaultDefault A problem occurred during the installation process; however, most of the themes in the archive have been installed Ad&vanced All Icons Antonio Larrosa Jimenez Co&lor: Colorize Confirmation Desaturate Description Desktop Dialogs Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Effect Parameters Gamma Geert Jansen Get new themes from the Internet Icons Icons Control Panel Module If you already have a theme archive locally, this button will unpack it and make it available for KDE applications Install a theme archive file you already have locally Install from File Installing icon themes... Jonathan Riddell Main Toolbar NAME OF TRANSLATORSYour names Name No Effect Panel Preview Remove Theme Remove the selected theme from your disk Set Effect... Setup Active Icon Effect Setup Default Icon Effect Setup Disabled Icon Effect Size: Small Icons The file is not a valid icon theme archive. This will remove the selected theme from your disk. To Gray To Monochrome Toolbar Torsten Rahn Unable to create a temporary file. Unable to download the icon theme archive;
please check that address %1 is correct. Unable to find the icon theme archive %1. Use of Icon You need to be connected to the Internet to use this action. A dialog will display a list of themes from the http://www.kde.org website. Clicking the Install button associated with a theme will install this theme locally. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-08 06:27+0100
PO-Revision-Date: 2017-04-15 23:04+0100
Last-Translator: giovanni <g.sora@tiscali.it>
Language-Team: Interlingua <kde-i18n-it@kde.org>
Language: ia
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 &Amonta: &Effecto: &Secunde color: &Semi-transparente &Thema (c) 2000-2003 Geert Jansen <h1>Icones</h1>Isto modulo permitte te de seliger le icones pro tu scriptorio.<p>Pro eliger un thema de icone, tu pulsa sur su nomine e tu applica tu selection per pressar le button de "Applica" a basso. Si tu non vole applicar tu selection tu pote pressar le button de "Re-initia" pro rejectar tu modificationes.</p><p>Per pressar le button de  "Installa Nove Thema" tu pote installar tu nove thema de icone per scriber su location in le quadro o navigar a le location. Tu pressa le button de "OK" pro finir le installation.</p><p>Le button de  "Remove thema" solmente essera activate si tu selectiona un thema que tu installava usante iste modulo. Tu non pote remover hic themas installate globalmente.</p><p>Tu anque pote specificar effectos que on debe esser applicate a le icones.</p> <qt>Tu es secur que tu vole remover le thema de icone <strong>%1</strong> ?<br /><br />Isto cancellara le files installate per iste thema.</qt> <qt>Installar <strong>%1</strong> thema</qt> Activa Dishabilitate Predefinite Un problema occurreva durante le processo de installation, totevia, le plure del themas in le archivo ha essite installate A&vantiate Omne icones Antonio Larrosa Jimenez Co&lor: Colorize Confirmation De-satura Description Scriptorio Dialogos Trahe o typa URL de thema g.sora@tiscali.it Parametros de effecto Gamma Geert Jansen Obtene nove themas ex le internet Icones Modulo de Pannello de Controlo de Icones Si tu ja ha un archivo de thema localmente, iste button de-impacchettara lo e facera lo disponibile pro applicationes de KDE.  Installa un file de archivo de thema que tu ja ha localmente Installa ex file  Installante themas de icone... Jonathan Riddell Barra de instrumento principal Giovanni Sora Nomine Nulle effecto Pannello Vista preliminar Remove thema Remove le thema seligite ex tu disco Fixa effecto... Configura effecto de icone active Configura effecto de icone predefinite Configura effecto de icone dishabilitate Dimension: Icones parve Le file non es un valide archivo de thema de icone. Isto removera le thema seligite ex tu disco. A gris A monochrome Barra de Instrumento Torsten Rahn Incapace crear un file temporari. Il non pote discargar le archivo de thema de icone;
pro favor tu verifica que le adressa %1 es correcte. Il non pote trovar le archivo de thema de icone %1. Uso de icone Tu necessita de esser connectite a le Internet pro usar iste action. Un dialogo monstrara un lista de themas ex le sito web http://www.kde.org. Pulsar le button de install associate con un thema installara iste thema localmente. 