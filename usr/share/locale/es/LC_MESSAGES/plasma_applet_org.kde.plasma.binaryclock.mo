��    	      d      �       �   
   �      �      �   	              "     C  "   ]  �  �     3     B     K     `     w  0   �  /   �  2   �                             	                 Appearance Colors: Display seconds Draw grid Show inactive LEDs: Use custom color for active LEDs Use custom color for grid Use custom color for inactive LEDs Project-Id-Version: plasma_applet_binaryclock
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-31 05:47+0100
PO-Revision-Date: 2017-05-04 19:38+0200
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Aspecto visual Colores: Mostrar los segundos Dibujar la cuadrícula Mostrar los LED inactivos: Usar un color personalizado para los LED activos Usar un color personalizado para la cuadrícula Usar un color personalizado para los LED inactivos 