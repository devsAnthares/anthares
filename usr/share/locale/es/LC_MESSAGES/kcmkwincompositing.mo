��    3      �  G   L      h  6   i  M   �  K   �     :  '   C     k     r  �   �     ;  	   R  A   \  >   �  9   �  9     9   Q  W   �  E   �     )     :      @     a  7   ~      �     �     �  X   �     X	     `	     v	  �   �	     '
     F
     L
     c
  
   s
  
   ~
     �
     �
  E  �
          &     <  w   O     �     �     �     �     �  	          �  #  D   �  y     l   �     �  6        :     B  �   [     M     d     p  
   ~     �     �     �  *   �     �     �            '     A   G  *   �     �     �  a   �     J     S  $   p  �   �  !   [     }      �     �  
   �  
   �     �     �  �  �     a  &   �     �  �   �     Y     n     u     {  *   �  	   �     �         /   +   )               ,      .                 	          &                       %   #                       '       0       3                         -       1   2                        (   
      *   !         $                           "    "Full screen repaints" can cause performance problems. "Only when cheap" only prevents tearing for full screen changes like a video. "Re-use screen content" causes severe performance problems on MESA drivers. Accurate Allow applications to block compositing Always Animation speed: Applications can set a hint to block compositing when the window is open.
 This brings performance improvements for e.g. games.
 The setting can be overruled by window-specific rules. Author: %1
License: %2 Automatic Category of Desktop Effects, used as section headerAccessibility Category of Desktop Effects, used as section headerAppearance Category of Desktop Effects, used as section headerCandy Category of Desktop Effects, used as section headerFocus Category of Desktop Effects, used as section headerTools Category of Desktop Effects, used as section headerVirtual Desktop Switching Animation Category of Desktop Effects, used as section headerWindow Management Configure filter Crisp EMAIL OF TRANSLATORSYour emails Enable compositor on startup Exclude Desktop Effects not supported by the Compositor Exclude internal Desktop Effects Full screen repaints Get New Effects... Hint: To find out or configure how to activate an effect, look at the effect's settings. Instant KWin development team Keep window thumbnails: Keeping the window thumbnail always interferes with the minimized state of windows. This can result in windows not suspending their work when minimized. NAME OF TRANSLATORSYour names Never Only for Shown Windows Only when cheap OpenGL 2.0 OpenGL 3.1 OpenGL Platform InterfaceEGL OpenGL Platform InterfaceGLX OpenGL compositing (the default) has crashed KWin in the past.
This was most likely due to a driver bug.
If you think that you have meanwhile upgraded to a stable driver,
you can reset this protection but be aware that this might result in an immediate crash!
Alternatively, you might want to use the XRender backend instead. Re-enable OpenGL detection Re-use screen content Rendering backend: Scale method "Accurate" is not supported by all hardware and can cause performance regressions and rendering artifacts. Scale method: Search Smooth Smooth (slower) Tearing prevention ("vsync"): Very slow XRender Project-Id-Version: kcmkwincompositing
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2017-10-22 12:11+0100
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 «Repintar toda la pantalla» puede causar problemas de rendimiento. «Solo cuando sea económico» previene únicamente el efecto bandera en los cambios a pantalla completa, como un vídeo. «Reutilizar el contenido de la pantalla» causa serios problemas de rendimiento con los controladores MESA. Preciso Permitir que las aplicaciones bloqueen la composición Siempre Velocidad de animación: Las aplicaciones pueden definir un consejo para bloquear la composición cuando se abra la ventana.
Esto puede mejorar el rendimiento en juegos, por ejemplo.
Las reglas específicas de las ventanas pueden tener preferencia sobre este ajuste. Autor: %1
Licencia: %2 Automático Accesibilidad Apariencia Embellecimiento Enfocar Herramientas Animación de cambio de escritorio virtual Gestión de ventanas Configurar el filtro Nítido cronopios@gmail.com Activar el compositor durante el inicio Excluir los efectos de escritorio no permitidos por el compositor Excluir los efectos de escritorio internos Repintar toda la pantalla Obtener nuevos efectos... Pista: para encontrar o configurar cómo activar un efecto, mire en la configuración del efecto. Instante Equipo de desarrollo de KWin Mantener miniaturas de las ventanas: Mantener las miniaturas de las ventanas en todo momento interfiere con el estado minimizado de las ventanas. Esto puede ocasionar que las ventanas no suspendan su trabajo cuando estén minimizadas. Enrique Matías Sánchez (Quique) Nunca Solo para las ventanas mostradas Solo cuando sea económico OpenGL 2.0 OpenGL 3.1 EGL GLX La composición OpenGL (por omisión) ha ocasionado que KWin se
bloquee en el pasado.
Es probable que esto se deba a un error en el controlador.
Si cree que ya se ha actualizado a un controlador estable, puede
restablecer esta protección, pero debe tener presente que es posible
que se produzca un bloqueo inmediato.
De forma alternativa, puede desear usar el motor XRender en su lugar. Reactivar detección de OpenGL Reutilizar el contenido de la pantalla Motor de renderizado: El método de escalado «Preciso» no está permitido por todo tipo de hardware y puede ocasionar regresiones de rendimiento y problemas de visualización. Método de escalado: Buscar Suave Suave (más lento) Prevención de efecto bandera («vsync»): Muy lenta XRender 