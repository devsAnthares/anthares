��    7      �  I   �      �     �     �     �     �     �     �               $     -     5     H     T      `     �     �     �     �     �     �     �     �                     )     7  	   C  	   M     W     d     j     �     �     �     �     �     �     �     �     �     �  @   �     7     @     \     c     j     r     �     �     �  4   �     �  �  �     �	     �	     �	  '   �	     �	  	   �	     �	     �	     

     
     
     
     +
     7
     J
     S
  $   g
     �
     �
     �
     �
     �
     �
     �
               '     8     F  
   S     ^     e     q     t     �     �  
   �  	   �     �     �     �     �  Q   �     %  *   9     d     m     o  ,   w     �     �     �     �     �     &   0      7                  #                
          	   '   (   +                       6              3      1                   ,   2                  "       *          $      4   )               %      5                     !          /   .   -       % %1 is value, %2 is unit%1 %2 %1: Application Energy Consumption Battery Capacity Charge Percentage Charge state Charging Current Degree Celsius°C Details: %1 Discharging EMAIL OF TRANSLATORSYour emails Energy Energy Consumption Energy Consumption Statistics Environment Full design Fully charged Has power supply Kai Uwe Broulik Last 12 hours Last 2 hours Last 24 hours Last 48 hours Last 7 days Last full Last hour Manufacturer Model NAME OF TRANSLATORSYour names No Not charging PID: %1 Path: %1 Rechargeable Refresh Serial Number Shorthand for WattsW System Temperature This type of history is currently not available for this device. Timespan Timespan of data to display Vendor VoltV Voltage Wakeups per second: %1 (%2%) WattW Watt-hoursWh Yes current power draw from the battery in WConsumption literal percent sign% Project-Id-Version: kcm_energyinfo
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2016-06-17 18:37+0200
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 % %1 %2 %1: Consumo de energía de las aplicaciones Batería Capacidad Porcentaje de carga Estado de la carga Cargando Actual °C Detalles: %1 En descarga ecuadra@eloihr.net Energía Consumo de energía Estadísticas de consumo de energía Entorno Diseño completo Totalmente cargada Tiene toma de corriente Kai Uwe Broulik Últimas 12 horas Últimas 2 horas Últimas 24 horas Últimas 48 horas Últimos 7 días Última carga Última hora Fabricante Modelo Eloy Cuadra No No está cargando PID: %1 Ruta: %1 Recargable Refrescar Número de serie W Sistema Temperatura Este tipo de historial no está disponible en la actualidad para este dispositivo Intervalo de tiempo Intervalo de tiempo de los datos a mostrar Vendedor V Voltaje Vueltas a la actividad por segundo: %1 (%2%) W Wh Sí Consumo % 