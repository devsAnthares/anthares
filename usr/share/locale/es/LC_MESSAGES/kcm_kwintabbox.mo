��    "      ,  /   <      �  
   �               ,     >     J  !   V     x     �     �     �     �     �  L   �     #     +     J     Y     u     z     �     �     �     �  	   �     �     �     �  �   �  F   �     �     �     �  �  �     �     �     �          *     =     I  	   V     `     q     �     �     �  a   �     	  3   +	     _	  '   p	  	   �	     �	  "   �	     �	     �	  	   �	     �	     
     7
     D
  �   U
  ]   �
     Q     g     y                    !                                            	                                                           "               
                    Activities All other activities All other desktops All other screens All windows Alternative An example Desktop NameDesktop 1 Content Current activity Current application Current desktop Current screen Filter windows by Focus policy settings limit the functionality of navigating through windows. Forward Get New Window Switcher Layout Hidden windows Include "Show Desktop" icon Main Minimization Only one window per application Recently used Reverse Screens Shortcuts Show selected window Sort order: Stacking order The currently selected window will be highlighted by fading out all other windows. This option requires desktop effects to be active. The effect to replace the list window when desktop effects are active. Virtual desktops Visible windows Visualization Project-Id-Version: kcm_kwintabbox
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2012-07-24 12:33+0200
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
com>
X-Poedit-Language: Spanish
X-Poedit-Country: SPAIN
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.4
 Actividades Las demás actividades Los demás escritorios Las demás pantallas Todas las ventanas Alternativo Escritorio 1 Contenido Actividad actual Aplicación actual Escritorio actual Pantalla actual Filtrar ventanas por Las preferencias de políticas de foco limitan la funcionalidad de la navegación entre ventanas. Redireccionar Obtener nueva disposición del selector de ventanas Ventanas ocultas Incluir el icono «Mostrar escritorio» Principal Minimización Una única ventana por aplicación Usados recientemente Invertir Pantallas Accesos rápidos de teclado Mostrar la ventana seleccionada Ordenación: Orden de apilado La ventana seleccionada actualmente se resaltará mediante un fundido sobre las demás. Esta opción necesita que los efectos de escritorio estén activados. El efecto para reemplazar el listado de ventanas cuando se activan los efectos de escritorio. Escritorios virtuales Ventanas visibles Visualización 