��          �      <      �     �     �     �  +   �  @        L     a     i     w     }     �  
   �     �     �     �     �     �  �  
     �     �     �  2   �  I        U     n     v     �     �     �     �  	   �  !   �     �       !   5     
         	                                                                                       <a href='%1'>%1</a> Close Copy Automatically: Don't show this dialog, copy automatically. Drop text or an image onto me to upload it to an online service. Error during upload. General History Size: Paste Please wait Please, try again. Sending... Share Shares for '%1' Successfully uploaded The URL was just shared Upload %1 to an online service Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-01 04:04+0100
PO-Revision-Date: 2015-04-27 12:48+0200
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 <a href='%1'>%1</a> Cerrar Copiar automáticamente: No mostrar este diálogo. Copiar automáticamente. Soltar texto o una imagen sobre mi para enviarlo a un servicio en línea. Error durante el envío. General Tamaño de la historia: Pegar Espere, por favor Inténtelo de nuevo, por favor. Enviando... Compartir Veces que se ha compartido «%1» Actualizado satisfactoriamente Se ha compartido el URL Enviar %1 a un servicio en línea 