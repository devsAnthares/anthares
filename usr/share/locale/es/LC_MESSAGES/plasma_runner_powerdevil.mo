��          �      L      �     �     �  �   �  T   }  )   �  (   �  0   %  $   V  &   {  &   �  %   �  ?   �  F   /     v     �     �     �     �  �  �  +   �  +   �  �   �  a   �      �     	          (  	   /     9  
   B  '   M     u     �     �     �     �     �                                              
                    	                                  Dim screen by half Dim screen totally Lists screen brightness options or sets it to the brightness defined by :q:; e.g. screen brightness 50 would dim the screen to 50% maximum brightness Lists system suspend (e.g. sleep, hibernate) options and allows them to be activated Note this is a KRunner keyworddim screen Note this is a KRunner keywordhibernate Note this is a KRunner keywordscreen brightness Note this is a KRunner keywordsleep Note this is a KRunner keywordsuspend Note this is a KRunner keywordto disk Note this is a KRunner keywordto ram Note this is a KRunner keyword; %1 is a parameterdim screen %1 Note this is a KRunner keyword; %1 is a parameterscreen brightness %1 Set Brightness to %1 Suspend to Disk Suspend to RAM Suspends the system to RAM Suspends the system to disk Project-Id-Version: krunner_powerdevil
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-26 03:33+0200
PO-Revision-Date: 2017-04-20 10:45+0200
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 Reducir el brillo de la pantalla a la mitad Reducir el brillo de la pantalla totalmente Lista las opciones de brillo de la pantalla o establece el brillo definido por :q:; por ejemplo, un brillo de 50 fijará el brillo máximo de la pantalla al 50% Lista todas las opciones de suspensión del sistema (como dormir o hibernar) y permite activarlas reducir el brillo de la pantalla hibernar brillo de la pantalla dormir suspender en disco en memoria reducción del brillo de la pantalla %1 brillo de pantalla %1 Establecer brillo a %1 Suspender en disco Suspender en RAM Suspende el sistema en RAM Suspende el sistema en disco 