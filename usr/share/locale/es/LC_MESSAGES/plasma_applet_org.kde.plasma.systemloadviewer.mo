��    &      L  5   |      P     Q     l     y     �     �     �     �     �     �     �     �     �  &   �               #     ,     3     ?     M     U     ]     d     s     �     �     �     �     �     �     �     �     �     �  
   �            �       �     �     �     �  	   �     �     �     �               !     2  5   9     o  	   �     �     �     �     �     �     �     �     �     �       
        #     B     K     W     n     �     �     �     �     �     �                           	      #      $                                                    &                                          !          %                            "   
    Abbreviation for secondss Application: Average clock: %1 MHz Bar Buffers: CPU CPU %1 CPU monitor CPU%1: %2% @ %3 Mhz CPU: %1% CPUs separately Cache Cache Dirty, Writeback: %1 MiB, %2 MiB Cache monitor Cached: Circular Colors Compact Bar Dirty memory: General IOWait: Memory Memory monitor Memory: %1/%2 MiB Monitor type: Nice: Set colors manually Show: Swap Swap monitor Swap: %1/%2 MiB Sys: System load Update interval: Used swap: User: Writeback memory: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-29 03:31+0200
PO-Revision-Date: 2017-04-11 13:04+0200
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 s Aplicación: Media de reloj: %1 MHz Barra Búferes: CPU CPU %1 Monitor de CPU CPU%1: %2% @ %3 Mhz CPU: %1% CPU por separado Caché Caché modificada, escritura diferida: %1 MiB, %2 MiB Monitor de la caché Cacheada: Circular Colores Barra compacta Memoria modificada: General Espera de E/S: Memoria Monitor de memoria Memoria: %1/%2 MiB Tipo de monitor: Agradable: Establecer colores manualmente Mostrar: Intercambio Monitor de intercambio Intercambio: %1/%2 MiB Sis: Carga del sistema Intervalo de actualización: Intercambio usado: Usuario: Memoria de escritura diferida: 