��          |      �          %   !     G     U     c     u     �     �  
   �     �     �  $   �  �  �  0   �     �     �     �     	     (     0     K     Y     j  '        
   	                                                   Automatically copy color to clipboard Clear History Color Options Copy to Clipboard Default color format: General Open Color Dialog Pick Color Pick a color Show history When pressing the keyboard shortcut: Project-Id-Version: plasma_applet_org
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-12 03:03+0200
PO-Revision-Date: 2016-12-17 20:54+0100
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Copiar el color automáticamente al portapapeles Borrar el historial Opciones del color Copiar al portapapeles Formato de color por omisión: General Abrir el diálogo de color Escoger color Escoger un color Mostrar el historial Al pulsar el acceso rápido de teclado: 