��    ?        Y         p     q  !   �  "   �  &   �  ,   �  #   &  &   J  !   q  &   �  '   �  "   �  #     "   )  '   L     t     �  +   �  2   �  
   �     �               ,     3     G  U   O  7   �     �     �     		     	      	     6	     T	     c	     k	  !   �	     �	  	   �	     �	     �	     �	     �	  &   
     /
     N
     U
     p
     v
     ~
     �
  4   �
  $   �
     �
               /     G     ]     w  &   �     �  �  �  $   o     �     �  	   �     �     �     �     �     �  
   �     �     �     �  
             2  @   :  H   {     �     �     �  0   �     -     5     N  j   V  `   �     "  "   :     ]     f  ,   m  "   �     �     �  )   �  *     	   -     7     =  +   C  #   o     �  3   �  !   �     �  &        ,     4     <  	   W  R   a  2   �     �  &        ,     J     i  %   �     �  5   �  (   �     $                               1          "   *   )          :             	         ;   !                          9   =   5   /   3                    (         #   4       8       -   6   
   7          %                                         ?       0   &      +          '         .       ,      >       <   2        &Matching window property:  @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Border @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large @item:inlistbox Button size:Large @item:inlistbox Button size:Normal @item:inlistbox Button size:Small @item:inlistbox Button size:Very Large Active Window Glow Add Add handle to resize windows with no border Allow resizing maximized windows from window edges Animations B&utton size: Border size: Button mouseover transition Center Center (Full Width) Class:  Configure fading between window shadow and glow when window's active state is changed Configure window buttons' mouseover highlight animation Decoration Options Detect Window Properties Dialog Edit Edit Exception - Oxygen Settings Enable/disable this exception Exception Type General Hide window title bar Information about Selected Window Left Move Down Move Up New Exception - Oxygen Settings Question - Oxygen Settings Regular Expression Regular Expression syntax is incorrect Regular expression &to match:  Remove Remove selected exception? Right Shadows Tit&le alignment: Title:  Use the same colors for title bar and window content Use window class (whole application) Use window title Warning - Oxygen Settings Window Class Name Window Drop-Down Shadow Window Identification Window Property Selection Window Title Window active state change transitions Window-Specific Overrides Project-Id-Version: breeze_kwin_deco
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-13 05:20+0100
PO-Revision-Date: 2017-12-14 22:19+0100
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 C&oincidir propiedad de la ventana:  Enorme Grande Sin borde Sin bordes laterales Normal Desmesurado Pequeño Gigante Muy grande Grande Normal Pequeño Muy grande Resplandor de la ventana activa Añadir Añadir un asa para cambiar el tamaño de las ventanas sin borde Permitir cambiar el tamaño de las ventanas maximizadas desde sus bordes Animaciones Ta&maño de los botones: Tamaño del borde: Transición al pasar el ratón sobre los botones Centrar Centrar (ancho completo) Clase:  Configurar el desvanecimiento entre la sombra y el resplandor de la ventana cuando cambia su estado activo Configurar la animación de resaltado cuando se pasa el ratón sobre los botones de las ventanas Opciones de decoración Detectar propiedades de la ventana Diálogo Editar Editar excepción - Preferencias de Oxígeno Activar/desactivar esta excepción Tipo de excepción General Ocultar la barra de título de la ventana Información sobre la ventana seleccionada Izquierda Bajar Subir Nueva excepción - Preferencias de Oxígeno Pregunta - Preferencias de Oxígeno Expresión regular La sintaxis de la expresión regular no es correcta E&xpresión regular a coincidir:  Eliminar ¿Eliminar la excepción seleccionada? Derecha Sombras A&lineamiento del título: Título:  Usar los mismos colores para la barra de título y para el contenido de la ventana Usar la clase de la ventana (aplicación completa) Usar el título de la ventana Advertencia - Preferencias de Oxígeno Nombre de la clase de ventana Sombra arrojada por la ventana Identificación de la ventana Selección de propiedad de la ventana Título de la ventana Transiciones de cambio de estado de la ventana activa Sustituciones específicas de la ventana 