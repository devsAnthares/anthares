��          �      ,      �     �     �     �     �     �     �     �     �  .   �     .     L     \     m     �     �  H   �  �  �     �     �     �     �     �                   5   0  $   f     �     �  "   �  
   �     �  O           	                                                                  
    &Configure Printers... Active jobs only All jobs Completed jobs only Configure printer General No active jobs No jobs No printers have been configured or discovered One active job %1 active jobs One job %1 jobs Open print queue Print queue is empty Printers Search for a printer... There is one print job in the queue There are %1 print jobs in the queue Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2015-02-21 09:37+0000
PO-Revision-Date: 2015-02-22 19:30+0100
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 &Configurar impresoras... Solo los trabajos activos Todos los trabajos Solo los trabajos completados Configurar impresora General No hay trabajos activos No hay trabajos No se ha configurado ni descubierto ninguna impresora 1 trabajo activo %1 trabajos activos 1 trabajo %1 trabajos Abrir la cola de impresión La cola de impresión está vacía Impresoras Buscar una impresora... Hay 1 trabajo de impresión en la cola Hay %1 trabajos de impresión en la cola 