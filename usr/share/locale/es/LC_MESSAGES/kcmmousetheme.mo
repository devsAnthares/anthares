��          �   %   �      @     A  �   `  m   �  �   P  )   �  `        o     |     �     �     �      �     �     �  '        ,     >     ]     b     s  ?   �  Y   �  +     H   F  �  �     8  �   V  e   �     B	     ]	  \   {	     �	  "   �	     	
     
  %   '
     M
     `
     q
  ,   �
     �
     �
     �
     �
     �
  F   �
  q   E  =   �  I   �                                                 	                                                               
              (c) 2003-2007 Fredrik Höglund <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) @item:inlistbox sizeResolution dependent A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Cursor Theme Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Fredrik Höglund Get new Theme Get new color schemes from the Internet Install from File NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. You have to restart the Plasma session for these changes to take effect. Project-Id-Version: kcm_cursortheme
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-07-14 18:24+0200
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 © 2003-2007 Fredrik Höglund <qt>¿Seguro que quiere eliminar el tema de cursores <i>%1</i>?<br />Esto borrará todos los archivos instalados por dicho tema.</qt> <qt>No puede borrar el tema que está usando actualmente.<br />Primero debe cambiar a otro tema.</qt> (Tamaños disponibles: %1) Dependiente de la resolución Ya existe un tema llamado %1 en su carpeta de temas de iconos. ¿Desea sustituirlo por este? Confirmación Preferencias de cursores cambiadas Tema de cursores Descripción Arrastre o introduzca el URL del tema ecuadra@eloihr.net Fredrik Höglund Obtener nuevo tema Obtener nuevos esquemas de color de Internet Instalar desde archivo Eloy Cuadra Nombre ¿Sobrescribir tema? Eliminar tema Parece que el archivo %1 no es un archivo válido de tema de cursores. No se ha podido descargar el archivo del tema de cursores. Por favor, compruebe que la dirección %1 es correcta. No se ha podido encontrar el archivo del tema de cursores %1. Debe reiniciar la sesión de Plasma para que estos cambios tengan efecto. 