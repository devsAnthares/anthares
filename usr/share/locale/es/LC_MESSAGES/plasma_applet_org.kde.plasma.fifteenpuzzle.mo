��          �            x  
   y  	   �     �     �  3   �     �     �                    %     *     F  B   Y     �  �  �     b     j     v     �  :   �     �     �            
   +     6     >     Y  
   v     �     	                                                  
                        Appearance Browse... Choose an image Fifteen Puzzle Image Files (*.png *.jpg *.jpeg *.bmp *.svg *.svgz) Number color Path to custom image Piece color Show numerals Shuffle Size Solve by arranging in order Solved! Try again. The time since the puzzle started, in minutes and secondsTime: %1 Use custom image Project-Id-Version: plasma_applet_fifteenPuzzle
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-01-12 03:59+0100
PO-Revision-Date: 2017-01-13 17:03+0100
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 Aspecto Explorar... Escoja una imagen Puzzle Quince Archivos de imagen (*.png *.jpg *.jpeg *.bmp *.svg *.svgz) Color de los números Ruta a la imagen personalizada Color de las piezas Mostrar números Desordenar Tamaño Resolver poniendo en orden ¡Resuelto! Probar otra vez. Tiempo: %1 Usar imagen personalizada 