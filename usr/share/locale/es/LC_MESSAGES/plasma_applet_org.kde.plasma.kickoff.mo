��            )   �      �     �  
   �  /   �  -   �          !  
   2     =     J     `  W   i     �  ,   �  	                  !     '     -  
   :     E     W     o     �     �     �     �  1   �       �       �  
   �  
   �     �     �                     -     H  �   O     �  B   �  	   -     7  	   I     S     Z     `     q     �     �     �     �  "   �     	  0   	     O	     f	                                                                                  
                                	                       %1@%2 %2@%3 (%1) @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Favorites All Applications Appearance Applications Applications updated. Computer Drag tabs between the boxes to show/hide them, or reorder the visible tabs by dragging. Edit Applications... Expand search to bookmarks, files and emails Favorites Hidden Tabs History Icon: Leave Menu Buttons Often Used On All Activities On The Current Activity Remove from Favorites Show In Favorites Show applications by name Sort alphabetically Switch tabs on hover Type is a verb here, not a nounType to search... Visible Tabs Project-Id-Version: plasma_applet_org
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2017-09-10 13:02+0100
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 %1@%2 %2@%3 (%1) Escoger... Borrar icono Añadir a favoritos Todas las aplicaciones Aspecto Aplicaciones Aplicaciones actualizadas. Equipo Arrastre las pestañas entre los cuadros para mostrarlas u ocultarlas, o cambie el orden de las pestañas visibles arrastrándolas. Editar aplicaciones... Expandir la búsqueda a marcadores, archivos y correo electrónico Favoritos Pestañas ocultas Historial Icono: Salir Botones de menú Usadas con frecuencia En todas las actividades En la actividad actual Eliminar de favoritos Mostrar en favoritos Mostrar aplicaciones por su nombre Ordenar alfabéticamente Cambiar pestañas al pasar el ratón sobre ellas Escriba para buscar... Pestañas visibles 