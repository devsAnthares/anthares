��    _                   C   	  /   M  -   }     �     �     �     �      	     	     1	     >	     J	  
   S	     ^	     g	     p	     �	  	   �	     �	     �	     �	  ,   �	  	    
     

  
   )
     4
     L
     `
     u
     �
     �
     �
     �
     �
  	   �
     �
     �
     
               !     (  	   ;     E     ]  
   r     }     �  
   �     �     �     �  
   �     �     �               $     2     @     R     h     y     �     �     �     �  	   �     �     �     �               4     Q     j     �     �     �     �  	   �     �  ,   �          !     0     @     L     S     b     t     �  #   �     �  �  �     w  
   �     �     �     �  $   �  -   �           7     ?     L     f     u     �  	   �     �     �  
   �     �     �     �  B   	  	   L  &   V     }     �     �     �     �     �               6     >     F  
   O     Z     n     u     �     �     �     �  "   �      �          /     H  
   _     j     �     �     �  	   �     �     �     �     �     
          9     O     c  +   u     �     �     �  	   �     �     �  $   �          1  *   L  '   w  (   �     �     �                )     0  .   I  	   x     �     �     �     �     �     �     �  &      -   '     U         Q         Y   5           %       H       J   !              \   *       @   V   4   [       A   (   
       U   B   ]   ^       $   _         '   >       D   K      -                  ?      ,   O   0       R   8   6   W   2   I   =   X   E   #       N   C       T   G   M          3   "         9             +          S          F           ;   :                    &   <   L          P         7          .       /       1                     )            	   Z       @action opens a software center with the applicationManage '%1'... @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Desktop Add to Favorites Add to Panel (Widget) Align search results to bottom All Applications App name (Generic name)%1 (%2) Applications Apps & Docs Behavior Categories Computer Contacts Description (Name) Description only Documents Edit Application... Edit Applications... End session Expand search to bookmarks, files and emails Favorites Flatten menu to a single level Forget All Forget All Applications Forget All Contacts Forget All Documents Forget Application Forget Contact Forget Document Forget Recent Documents General Generic name (App name)%1 (%2) Hibernate Hide %1 Hide Application Icon: Lock Lock screen Logout Name (Description) Name only Often Used Applications Often Used Documents Often used On All Activities On The Current Activity Open with: Pin to Task Manager Places Power / Session Properties Reboot Recent Applications Recent Contacts Recent Documents Recently Used Recently used Removable Storage Remove from Favorites Restart computer Run Command... Run a command or a search query Save Session Search Search results Search... Searching for '%1' Session Show Contact Information... Show In Favorites Show applications as: Show often used applications Show often used contacts Show often used documents Show recent applications Show recent contacts Show recent documents Show: Shut Down Sort alphabetically Start a parallel session as a different user Suspend Suspend to RAM Suspend to disk Switch User System System actions Turn off computer Type to search. Unhide Applications in '%1' Unhide Applications in this Submenu Widgets Project-Id-Version: plasma_applet_org
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2017-10-16 17:55+0100
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Gestionar «%1»... Escoger... Borrar icono Añadir al escritorio Añadir a favoritos Añadir al panel (elemento gráfico) Alinear el resultado de la búsqueda al fondo Todas las aplicaciones %1 (%2) Aplicaciones Aplicaciones y documentos Comportamiento Categorías Equipo Contactos Descripción (Nombre) Solo la descripción Documentos Editar aplicación... Editar aplicaciones... Terminar la sesión Expandir la búsqueda a marcadores, archivos y correo electrónico Favoritos Aplanar el menú hasta un único nivel Olvidar todo Olvidar todas las aplicaciones Olvidar todos los contactos Olvidar todos los documentos Olvidar aplicación Olvidar contacto Olvidar documento Olvidar documentos recientes General %1 (%2) Hibernar Ocultar %1 Ocultar aplicación Icono: Bloquear la sesión Bloquear la pantalla Cerrar la sesión Nombre (Descripción) Solo el nombre Aplicaciones usadas con frecuencia Documentos usados con frecuencia Usadas con frecuencia En todas las actividades En la actividad actual Abrir con: Fijar en el gestor de tareas Lugares Inicio y apagado Propiedades Reiniciar Aplicaciones recientes Contactos recientes Documentos recientes Usado recientemente Usadas recientemente Almacenamiento extraíble Eliminar de favoritos Reiniciar el equipo Ejecutar orden... Ejecutar una orden o realizar una búsqueda Guardar la sesión Buscar Resultado de la búsqueda Buscar... Buscando «%1» Sesión Mostrar información del contacto... Mostrar en favoritos Mostrar aplicaciones como: Mostrar aplicaciones usadas con frecuencia Mostrar contactos usados con frecuencia Mostrar documentos usados con frecuencia Mostrar aplicaciones recientes Mostrar contactos recientes Mostrar documentos recientes Mostrar: Apagar Ordenar alfabéticamente Iniciar una sesión paralela como otro usuario Suspender Suspender en memoria Suspender en disco Cambiar de usuario Sistema Acciones del sistema Apagar el equipo Escriba para buscar. Mostrar aplicaciones ocultas en «%1» Mostrar aplicaciones ocultas en este submenú Elementos gráficos 