��            )   �      �  ;   �  J   �          /  ~   7  A   �     �       )        G     X     i      q     �     �     �  
   �     �  
   �     �             "   <     _  	   y     �     �     �  �  �     g     o     s     �  �   �  Q        l     y  8   �     �     �     �     	     	     -	     =	  	   K	     U	     a	     o	      �	  <   �	  >   �	  )   
     I
     V
     t
  	   �
                                      
                                                                                            	        %1 is the full user name, %2 is the user login name%1 (%2) %1 is the name of a detail about the current action provided by polkit%1: (c) 2009 Red Hat, Inc. Action: An application is attempting to perform an action that requires privileges. Authentication is required to perform this action. Another client is already authenticating, please try again later. Application: Authentication Required Authentication failure, please try again. Click to edit %1 Click to open %1 Details EMAIL OF TRANSLATORSYour emails Former maintainer Jaroslav Reznik Lukáš Tinkl Maintainer NAME OF TRANSLATORSYour names P&assword: Password for %1: Password for root: Password or swipe finger for %1: Password or swipe finger for root: Password or swipe finger: Password: PolicyKit1 KDE Agent Select User Vendor: Project-Id-Version: polkit-kde-authentication-agent-1
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:22+0100
PO-Revision-Date: 2015-02-03 11:02+0100
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 %1 (%2) %1: © 2009 Red Hat, Inc. Acción: Una aplicación está intentando realizar una acción que necesita privilegios. Se necesita autenticación para realizar dicha acción. Ya se está autenticando otro cliente; por favor, vuelva a intentarlo más tarde. Aplicación: Se necesita autenticación Fallo de autenticación; por favor, vuelva a intentarlo. Haga clic para editar %1 Haga clic para abrir %1 Detalles ecuadra@eloihr.net Encargado anterior Jaroslav Reznik Lukáš Tinkl Encargado Eloy Cuadra C&ontraseña: Contraseña de %1: Contraseña de usuario «root»: Introduzca la contraseña o pase el dedo para el usuario %1: Introduzca la contraseña o pase el dedo para el usuario root: Introduzca la contraseña o pase el dedo: Contraseña: Agente de KDE para PolicyKit1 Seleccionar usuario Vendedor: 