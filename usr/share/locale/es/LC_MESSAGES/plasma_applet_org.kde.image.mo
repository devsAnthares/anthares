��            )   �      �     �     �     �     �     �     �     �     �  0        3     G     ]     c     o     w     �  
   �     �     �     �     �     �               (     A     I     a     m  �  s  	     )   (     R     e     w  
   ~     �     �  T   �     �  &        <     B     U  (   ]     �     �  $   �     �  *   �          /     N     W  &   l     �     �     �  
   �                                            
                                                                                      	       %1 by %2 Add Custom Wallpaper Add Folder... Add Image... Background: Blur Centered Change every: Directory with the wallpaper to show slides from Download Wallpapers Get New Wallpapers... Hours Image Files Minutes Next Wallpaper Image Open Containing Folder Open Image Open Wallpaper Image Positioning: Recommended wallpaper file Remove wallpaper Restore wallpaper Scaled Scaled and Cropped Scaled, Keep Proportions Seconds Select Background Color Solid color Tiled Project-Id-Version: plasma_applet_org
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:39+0100
PO-Revision-Date: 2017-11-20 21:44+0100
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 %1 por %2 Añadir fondo de escritorio personalizado Añadir carpeta... Añadir imagen... Fondo: Desenfocar Centrado Cambiar cada: Directorio que contiene el fondo de escritorio del que se van a mostrar diapositivas Descargar fondos del escritorio Obtener nuevos fondos de escritorio... Horas Archivos de imagen Minutos Siguiente imagen de fondo del escritorio Abrir la carpeta contenedora Abrir imagen Abrir imagen de fondo del escritorio Posicionamiento: Archivo de fondo de escritorio recomendado Eliminar fondo del escritorio Restaurar fondo del escritorio Escalado Escalado y recortado Escalado, manteniendo las proporciones Segundos Selección del color de fondo Color sólido En mosaico 