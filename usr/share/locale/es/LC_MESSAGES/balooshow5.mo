��          �      \      �     �     �  
   �                :      N     o  
   }     �     �     �  	   �  (   �  \   	     f  2   t     �     �  �  �     f     w     �  $   �     �  !   �  &   �        	   5     ?  ,   K     x  
   �  8   �  v   �     P  9   d     �     �     	                                                                                 
             %1 Terms: %2 (c) 2012, Vishesh Handa Baloo Show Device id for the files EMAIL OF TRANSLATORSYour emails File Name Terms: %1 Inode number of the file to show Internal Info Maintainer NAME OF TRANSLATORSYour names No index information found Print internal info Terms: %1 The Baloo data Viewer - A debugging tool The Baloo index could not be opened. Please run "%1" to see if Baloo is enabled and working. The file urls The fileID is not equal to the actual Baloo fileID This is a bug Vishesh Handa Project-Id-Version: balooshow
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-28 03:17+0100
PO-Revision-Date: 2018-01-31 21:54+0100
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Reglas de %1: %2 © 2012, Vishesh Handa Muestra de Baloo Id del dispositivo para los archivos ecuadra@eloihr.net Reglas de nombres de archivos: %1 Número de inodo del archivo a mostrar Información interna Encargado Eloy Cuadra No se ha encontrado información del índice Mostrar información interna Reglas: %1 Visor de datos de Baloo - Una herramienta de depuración El índice de Baloo no se puede abrir. Por favor, ejecute «%1» para ver si Baloo está activado y en funcionamiento. URL de los archivos El «fileID» no es idéntico al «fileID» real de Baloo Esto es un fallo Vishesh Handa 