��    =        S   �      8  ;   9     u     �     �     �  6   �  A   
     L     U  $   Y  
   ~     �  #   �     �     �     �     �     �  7        >     [     w  '   �     �     �  $   �  ,   �          3     C     K     ]     y     �     �     �     �     �  �   �  9   �	  "   �	     �	     �	     
     *
     <
     R
     c
  %   u
     �
     �
     �
     �
     �
  
   �
  7        :     T     l     t  �  �  <   2     o     �     �     �     �     �            -        C      K  8   l     �  '   �     �     �     �  :   �  !   7     Y     w  6   �     �     �  2   �  /        C     _     w       ,   �     �     �     �     �          '  �   8  `     .   o  
   �  ,   �     �     �  #        0     N  *   h  
   �  !   �  $   �     �     �  
   �  &   �     $     +     2     :     !          3      0      (                 1      "          =   .   6                                         	      :          5       ,   #   &   7       ;   '             -   
             8                 *           %           <   4   9                )   /   $          2             +       

Choose the previous strip to go to the last cached strip. &Create Comic Book Archive... &Save Comic As... &Strip Number: *.cbz|Comic Book Archive (Zip) @option:check Context menu of comic image&Actual Size @option:check Context menu of comic imageStore current &Position Advanced All An error happened for identifier %1. Appearance Archiving comic failed Automatically update comic plugins: Cache Check for new comic strips: Comic Comic cache: Configure... Could not create the archive at the specified location. Create %1 Comic Book Archive Creating Comic Book Archive Destination: Display error when getting comic failed Download Comics Error Handling Failed adding a file to the archive. Failed creating the file with identifier %1. From beginning to ... From end to ... General Get New Comics... Getting comic strip failed: Go to Strip Information Jump to &current Strip Jump to &first Strip Jump to Strip ... Manual range Maybe there is no Internet connection.
Maybe the comic plugin is broken.
Another reason might be that there is no comic for this day/number/string, so choosing a different one might work. Middle-click on the comic to show it at its original size No zip file is existing, aborting. Range: Show arrows only on mouse over Show comic URL Show comic author Show comic identifier Show comic title Strip identifier: The range of comic strips to archive. Update Visit the comic website Visit the shop &website an abbreviation for Number# %1 days dd.MM.yyyy here strip means comic strip&Next Tab with a new Strip in a range: from toFrom: in a range: from toTo: minutes strips per comic Project-Id-Version: plasma_applet_comic
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-07-18 03:20+0200
PO-Revision-Date: 2015-03-06 22:24+0100
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 

Elija la tira anterior para ir a la última tira cacheada. &Crear un archivo de cómics... &Guardar el cómic como... Número de &tira: *.cbz|Archivo de cómics (Zip) Tamaño re&al Guardar la &posición actual Avanzado Todas Se produjo un error para el identificador %1. Aspecto El archivo del cómic ha fallado Actualizar automáticamente los complementos de cómics: Caché Comprobar si hay nuevas tiras cómicas: Cómic Caché de cómics: Configurar... No fue posible crear el archivo en la ubicación indicada. Crear un archivo de cómics de %1 Creando el archivo de cómics Destino: Mostrar un error cuando falle la obtención del cómic Descargar cómics Tratamiento de errores Fallo al añadir un archivo al archivo comprimido. Fallo al crear el archivo con identificador %1. Desde el principio hasta... Desde el final hasta... General Obtener cómics nuevos... La obtención de la tira cómica ha fallado: Ir a la tira Información Saltar a la tira a&ctual Saltar a la &primera tira Saltar a la tira... Intervalo manual Quizás no haya conexión a Internet.
Quizás el complemento de cómics esté roto.
Otra razón podría ser que no haya cómic para este día/número/cadena, de modo que eligiendo otro diferente podría funcionar. Pulse con el botón central del ratón sobre el cómic para que se muestre a su tamaño original No existe ningún archivo zip, interrumpiendo. Intervalo: Mostrar las flechas solo al pasar por encima Mostrar el URL del cómic Mostrar el autor del cómic Mostrar el identificador del cómic Mostrar el título del cómic Identificador de la tira: El intervalo de tiras cómicas a archivar. Actualizar Visitar la página web del cómic Visitar la página &web de la tienda nº %1 días dd.MM.aaaa Siguie&nte pestaña con una tira nueva Desde: Hasta: minutos tiras por cómic 