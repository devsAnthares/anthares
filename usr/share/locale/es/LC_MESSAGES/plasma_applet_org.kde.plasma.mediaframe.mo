��            )   �      �     �     �     �     �     �     �     �  
           )   (     R     V     \     c     v     �     �     �  3   �     �       <   #  5   `  8   �  8   �                    /  �  1     �     �               )     =     N     f     w  T        �     �     �  !   �          8     W     i  /   q     �  &   �  P   �  A   4	  F   v	  F   �	     
     
     
     0
                                                  
                                                                               	        Add files... Add folder... Background frame Change picture every Choose a folder Choose files Configure plasmoid... Fill mode: General Left click image opens in external viewer Pad Paths Paths: Pause on mouseover Preserve aspect crop Preserve aspect fit Randomize items Stretch The image is duplicated horizontally and vertically The image is not transformed The image is scaled to fit The image is scaled uniformly to fill, cropping if necessary The image is scaled uniformly to fit without cropping The image is stretched horizontally and tiled vertically The image is stretched vertically and tiled horizontally Tile Tile horizontally Tile vertically s Project-Id-Version: plasma_applet_org
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-16 03:31+0100
PO-Revision-Date: 2017-11-20 21:47+0100
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Añadir archivos... Añadir carpeta... Marco de fondo Cambiar la imagen cada Escoger una carpeta Escoger archivos Configurar plasmoide... Modo de relleno: General Abrir la imagen en un visor externo al hacer clic con el botón izquierdo sobre ella Rellenar con espacio Rutas Rutas: Pausar al situar el ratón encima Preservar el aspecto recortando Preservar el aspecto ajustando Elementos al azar Estirar La imagen se duplica horizontal y verticalmente La imagen no se transforma La imagen se escala para que se ajuste La imagen se escala uniformemente para que se ajuste, recortando si es necesario La imagen se escala uniformemente para que se ajuste sin recortar La imagen se estira horizontalmente y se pone en mosaico verticalmente La imagen se estira verticalmente y se pone en mosaico horizontalmente Mosaico Mosaico horizontal Mosaico vertical s 