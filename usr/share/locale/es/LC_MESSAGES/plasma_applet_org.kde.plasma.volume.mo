��    &      L  5   |      P     Q  8   S     �     �     �     �     �     �  3   �  >        N     i     y     �  m   �     �          "     2     7     ?  *   O      z     �     �  "   �     �     �          3     :     H     X     e     �  ;   �     �  �  �     �     �     �     �     �     �     �     	  	   (	     2	     ?	     _	     p	     }	     �	     �	     �	     �	  	   �	     �	     �	  4   �	  9   3
     m
     �
     �
     �
     �
  $   �
                    5     F     N     U     Z        #   %                                                              "          $                      
                                           &      !                	        % Accessibility data on volume sliderAdjust volume for %1 Applications Audio Muted Audio Volume Behavior Capture Devices Capture Streams Checkable switch for (un-)muting sound output.Mute Checkable switch to change the current default output.Default Decrease Microphone Volume Decrease Volume Devices General Heading for a list of ports of a device (for example built-in laptop speakers or a plug for headphones)Ports Increase Microphone Volume Increase Volume Maximum volume: Mute Mute %1 Mute Microphone No applications playing or recording audio No output or input devices found Playback Devices Playback Streams Port is unavailable (unavailable) Port is unplugged (unplugged) Raise maximum volume Show additional options for %1 Volume Volume at %1% Volume feedback Volume step: label of device items%1 (%2) label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: plasma_applet_org
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2017-09-24 23:04+0100
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 % Ajustar el volumen para %1 Aplicaciones Sonido silenciado Volumen del sonido Comportamiento Dispositivos de captura Emisiones de captura Silenciar Por omisión Bajar el volumen del micrófono Bajar el volumen Dispositivos General Puertos Subir el volumen del micrófono Subir el volumen Volumen máximo: Silenciar Silenciar %1 Silenciar el micrófono No hay aplicaciones reproduciendo ni grabando sonido No se han encontrado dispositivos de entrada ni de salida Dispositivos de reproducción Emisiones de reproducción  (no disponible)  (sin conectar) Subir el volumen máximo Mostrar opciones adicionales para %1 Volumen Volumen al %1% Acoplamiento del volumen Paso de volumen: %1 (%2) %1: %2 100% %1% 