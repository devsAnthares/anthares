��          �      |      �     �  2        B     b       #   �      �     �  %   �       
   &     1     >     ]  �   }       ]   (     �  p   �  :        P  �  \  $     ?   *  *   j     �  "   �  +   �       #     1   8     j     �     �     �  0   �  �   �     �	  x   �	  #   k
  �   �
  ?        \                                                                      
                	           Apply a look and feel package Command line tool to apply look and feel packages. Configure Look and Feel details Copyright 2017, Marco Martin Cursor Settings Changed Download New Look And Feel Packages EMAIL OF TRANSLATORSYour emails Get New Looks... List available Look and feel packages Look and feel tool Maintainer Marco Martin NAME OF TRANSLATORSYour names Reset the Plasma Desktop layout Select an overall theme for your workspace (including plasma theme, color scheme, mouse cursor, window and desktop switcher, splash screen, lock screen etc.) Show Preview This module lets you configure the look of the whole workspace with some ready to go presets. Use Desktop Layout from theme Warning: your Plasma Desktop layout will be lost and reset to the default layout provided by the selected theme. You have to restart KDE for cursor changes to take effect. packagename Project-Id-Version: kcm_lookandfeel
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-27 05:46+0100
PO-Revision-Date: 2017-12-26 17:35+0100
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Aplicar un paquete de aspecto visual Herramienta de consola para aplicar paquetes de aspecto visual. Configurar los detalles del aspecto visual Copyright 2017, Marco Martin Preferencias de cursores cambiadas Descargar nuevos paquetes de aspecto visual ecuadra@eloihr.net Obtener nuevos aspectos visuales... Listar los paquetes de aspecto visual disponibles Herramienta de aspecto visual Responsable Marco Martin Eloy Cuadra Reiniciar la distribución del escritorio Plasma Seleccione un tema de aspecto visual general para el espacio de trabajo (que incluye un tema para Plasma, un esquema de color, un cursor para el ratón, un cambiador de ventanas y de escritorios, una pantalla de bienvenida, una pantalla de bloqueo, etc.) Mostrar vista previa Este módulo le permite configurar el aspecto visual de todo el espacio de trabajo con algunos ajustes listos para usar. Usar diseño de escritorio del tema Advertencia: el diseño de su escritorio Plasma se perderá y se reiniciará al diseño por omisión proporcionado por el tema seleccionado. Debe iniciar KDE para que tengan efecto los cambios de cursores nombredepaquete 