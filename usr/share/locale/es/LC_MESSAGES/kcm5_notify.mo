��          �      �       0     1  �  H     �       &         ?     `     n     v     �     �  (   �  �  �     �  �  �     �     �  +   �     �     �     �               "  @   ;         
                 	                                (c) 2002-2006 KDE Team <h1>System Notifications</h1>Plasma allows for a great deal of control over how you will be notified when certain events occur. There are several choices as to how you are notified:<ul><li>As the application was originally designed.</li><li>With a beep or other noise.</li><li>Via a popup dialog box with additional information.</li><li>By recording the event in a logfile without any additional visual or audible alert.</li></ul> Carsten Pfeiffer Charles Samuels Disable sounds for all of these events EMAIL OF TRANSLATORSYour emails Event source: KNotify NAME OF TRANSLATORSYour names Olivier Goffart Original implementation System Notification Control Panel Module Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-09 07:18+0200
PO-Revision-Date: 2017-08-10 20:47+0100
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 © 2002-2006 El equipo de KDE <h1>Notificaciones del sistema</h1>Plasma permite gran cantidad de opciones para controlar cómo se le mostrarán las notificaciones cuando ocurra un cierto evento. También dispone de varias opciones sobre cómo se le avisará:<ul><li>Como se diseñó originalmente en la aplicación.</li><li>Con un pitido u otro sonido.</li><li>Usando un diálogo emergente con información adicional.</li><li>Grabando el evento en un archivo de registro sin una alerta visual o sonora adicional.</li></ul> Carsten Pfeiffer Charles Samuels Desactivar sonidos para todos estos eventos ecuadra@eloihr.net Origen del evento: KNotify Eloy Cuadra Olivier Goffart Implementación original Módulo del panel de control para las notificaciones del sistema 