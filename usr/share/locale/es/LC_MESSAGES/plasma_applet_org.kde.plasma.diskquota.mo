��    
      l      �       �   
   �      �        .   0     _     t     �  (   �  8   �  �       �  ,   �     �  K     %   _     �     �     �     �        
                             	        Disk Quota No quota restrictions found. Please install 'quota' Quota tool not found.

Please install 'quota'. Running quota failed e.g.: 12 GiB of 20 GiB%1 of %2 e.g.: 8 GiB free%1 free example: Quota: 83% usedQuota: %1% used usage of quota, e.g.: '/home/bla: 38% used'%1: %2% used Project-Id-Version: plasma_applet_org
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2016-11-20 00:00+0100
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Cuota de disco No se han encontrado restricciones de cuota. Por favor, instale «quota» No se ha encontrado la herramienta de cuota.

Por favor, instale «quota». La ejecución de «quota» ha fallado %1 de %2 %1 libre Cuota: %1% usado %1: %2% usado 