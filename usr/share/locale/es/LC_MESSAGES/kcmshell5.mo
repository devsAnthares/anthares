��          �   %   �      0  !   1  J   S  .   �     �     �  N        R     c      ~     �     �  
   �     �     �     �          +     D     b  $   y  %   �  "   �     �  �  �  (   �  W   �  J        [  !   r  k   �                1     D  "   R  	   u          �     �     �  &   �     �  #   	  +   +	  +   W	  )   �	     �	         	                                                                                 
                                     (c) 1999-2016, The KDE Developers --lang is deprecated. Please set the LANGUAGE environment variable instead A tool to start single system settings modules Arguments for the module Configuration module to open Could not find module '%1'. See kcmshell5 --list for the full list of modules. Daniel Molkentin Do not display main window EMAIL OF TRANSLATORSYour emails Frans Englich List all possible modules Maintainer Matthias Elter Matthias Ettrich Matthias Hoelzer-Kluepfel NAME OF TRANSLATORSYour names No description available Specify a particular language System Settings Module The following modules are available: Use a specific caption for the window Use a specific icon for the window Waldo Bastian Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-07 06:55+0200
PO-Revision-Date: 2016-03-16 22:53+0100
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 © 1999-2016, los desarrolladores de KDE --lang está desaconsejado. Por favor, use en su lugar la variable de entorno LANGUAGE. Herramienta para iniciar módulos de preferencias del sistema individuales Argumentos del módulo Módulo de configuración a abrir No se ha encontrado el módulo «%1». Ejecute kcmshell5 --list para obtener la lista completa de módulos. Daniel Molkentin No mostrar la ventana principal ecuadra@eloihr.net Frans Englich Listar todos los módulos posibles Encargado Matthias Elter Matthias Ettrich Matthias Hoelzer-Kluepfel Eloy Cuadra No hay disponible ninguna descripción Indique un idioma concreto Módulo de preferencias del sistema Están disponibles los siguientes módulos: Usar un título específico para la ventana Usar un icono específico para la ventana Waldo Bastian 