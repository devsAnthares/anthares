��    !      $  /   ,      �  .   �  1     9   J     �  -   �  &   �  )   �  .   #  &   R  )   y  .   �  &   �  )   �  2   #  5   V  =   �  #   �     �  !     '   /  "   W  0   z  '   �  *   �     �          7     R     j     �     �  &   �  �  �  ,   x	  .   �	  <   �	  $   
  )   6
     `
     |
  4   �
      �
  #   �
  6     "   K  '   n  .   �  0   �  A   �  #   8     \     o  0   �  #   �  7   �  "     %   4     Z     p     �     �     �     �     �  !   �                                                              	                                
                                    !                             @info:statusAdded files to Bazaar repository. @info:statusAdding files to Bazaar repository... @info:statusAdding of files to Bazaar repository failed. @info:statusBazaar Log closed. @info:statusCommit of Bazaar changes failed. @info:statusCommitted Bazaar changes. @info:statusCommitting Bazaar changes... @info:statusPull of Bazaar repository failed. @info:statusPulled Bazaar repository. @info:statusPulling Bazaar repository... @info:statusPush of Bazaar repository failed. @info:statusPushed Bazaar repository. @info:statusPushing Bazaar repository... @info:statusRemoved files from Bazaar repository. @info:statusRemoving files from Bazaar repository... @info:statusRemoving of files from Bazaar repository failed. @info:statusReview Changes failed. @info:statusReviewed Changes. @info:statusReviewing Changes... @info:statusRunning Bazaar Log failed. @info:statusRunning Bazaar Log... @info:statusUpdate of Bazaar repository failed. @info:statusUpdated Bazaar repository. @info:statusUpdating Bazaar repository... @item:inmenuBazaar Add... @item:inmenuBazaar Commit... @item:inmenuBazaar Delete @item:inmenuBazaar Log @item:inmenuBazaar Pull @item:inmenuBazaar Push @item:inmenuBazaar Update @item:inmenuShow Local Bazaar Changes Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2011-12-15 17:46+0100
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=2; plural=n != 1;
 Archivos añadidos al repositorio de Bazaar. Añadiendo archivos a repositorio de Bazaar... La adición de archivos al repositorio de Bazaar ha fallado. Se ha cerrado el registro de Bazaar. El envío de cambios a Bazaar ha fallado. Cambios de Bazaar enviados. Enviando cambios de Bazaar... La extracción del repositorio de Bazaar ha fallado. Repositorio de Bazaar extraído. Extrayendo repositorio de Bazaar... La incorporación al repositorio de Bazaar ha fallado. Repositorio de Bazaar incorporado. Incorporando a repositorio de Bazaar... Archivos eliminados del repositorio de Bazaar. Eliminando archivos del repositorio de Bazaar... La eliminación de archivos del repositorio de Bazaar ha fallado. La revisión de cambios ha fallado. Cambios revisados. Revisando cambios... La ejecución del registro de Bazaar ha fallado. Ejecutando el registro de Bazaar... La actualización del repositorio de Bazaar ha fallado. Repositorio de Bazaar actualizado. Actualizando repositorio de Bazaar... Adición de Bazaar... Envío de Bazaar... Borrado de Bazaar Registro de Bazaar Extracción de Bazaar Incorporación de Bazaar Actualización de Bazaar Mostrar cambios locales de Bazaar 