��          �      �       H     I     a      m     �     �     �  
   �     �  &   �          .     L  1   b  �  �  !   J     l  C   x     �     �  ;   �  
   *     5  ,   A  $   n     �  $   �  :   �         	                                    
                    Configure Desktop Theme David Rosca EMAIL OF TRANSLATORSYour emails Get New Themes... Install from File... NAME OF TRANSLATORSYour names Open Theme Remove Theme Theme Files (*.zip *.tar.gz *.tar.bz2) Theme installation failed. Theme installed successfully. Theme removal failed. This module lets you configure the desktop theme. Project-Id-Version: kcm_desktopthemedetails
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-22 05:21+0100
PO-Revision-Date: 2017-10-22 12:10+0100
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
com>
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 Configurar el tema del escritorio David Rosca ecuadra@eloihr.net,ignaciop.3@gmail.com,the.blue.valkyrie@gmail.com Obtener nuevos temas... Instalar desde archivo... Eloy Cuadra,Ignacio Poggi,Cristina Yenyxe González García Abrir tema Borrar tema Archivos de temas (*.zip *.tar.gz *.tar.bz2) La instalación del tema ha fallado. Tema instalado con éxito. La eliminación del tema ha fallado. Este módulo le permite configurar el tema del escritorio. 