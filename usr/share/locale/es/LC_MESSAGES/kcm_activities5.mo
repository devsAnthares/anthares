��    *      l  ;   �      �     �     �  )   �               /     H     ^  #   ~     �  
   �     �     �  %   �  +        E     Z     m  @   z     �     �     �     �               '     ,     9     ?     _     e  .   m     �  F   �  (   �  	   '  	   1  	   ;  '   E  7   m  "   �  �  �     p	     }	  -   �	     �	     �	  
   �	     �	     �	     
     
     *
     6
     V
  !   j
  J   �
     �
     �
       X        n     �     �     �     �     �     �     �       %        2  
   8  .   C  !   r  N   �  -   �          "     )     =     B     N                   $                                 %                      *                                               	   #   )   "             &           
                        '             (   !    &Do not remember @actionWalk through activities @actionWalk through activities (Reverse) @action:buttonApply @action:buttonCancel @action:buttonChange... @action:buttonCreate @title:windowActivity Settings @title:windowCreate a New Activity @title:windowDelete Activity Activities Activity information Activity switching Are you sure you want to delete '%1'? Blacklist all applications not on this list Clear recent history Create activity... Description: Error loading the QML files. Check your installation.
Missing %1 For a&ll applications Forget a day Forget everything Forget the last hour Forget the last two hours General Icon Keep history Name: O&nly for specific applications Other Privacy Private - do not track usage for this activity Remember opened documents: Remember the current virtual desktop for each activity (needs restart) Shortcut for switching to this activity: Shortcuts Switching Wallpaper for in 'keep history for 5 months'for  unit of time. months to keep the history month  months unlimited number of monthsforever Project-Id-Version: kcm_activities
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-04-01 09:39+0200
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 No recor&dar Recorrer las actividades Recorrer las actividades (en sentido inverso) Aplicar Cancelar Cambiar... Crear Preferencias de la actividad Crear nueva actividad Borrar actividad Actividades Información sobre la actividad Cambio de actividad ¿Seguro que desea borrar «%1»? Poner en la lista negra todas las aplicaciones que no estén en esta lista Borrar el historial reciente Crear actividad... Descripción: Se ha producido un error al cargar los archivos QML. Compruebe su instalación.
Falta %1 Para todas &las aplicaciones Olvidar un día Olvidar todo Olvidar la última hora Olvidar las últimas dos horas General Icono Conservar el historial Nombre: Solo para las aplicacio&nes indicadas Otros Privacidad Privada (no rastrear el uso de esta actividad) Recordar los documentos abiertos: Recordar el escritorio virtual actual para cada actividad (necesita reiniciar) Acceso rápido para cambiar a esta actividad: Accesos rápidos Cambio Fondo de escritorio para  mes  meses siempre 