��    F      L  a   |         
     
     
        "     1     5     I     X     ^  	   m     w  	        �     �     �  
   �     �     �  3   �  	   �     �               $     9     @     G     V     f     m  
        �  2   �  ?   �                    )     2     ?     N     S     a     r     �     �     �     �     �     �  	   �     �     �     �     �  b   �  �   [	     �	     �	  	   
     
     (
  
   8
  	   C
     M
     ]
     e
     k
     }
  �  �
     8     D     T     f     �      �     �     �     �     �     �     �                    (     4     ;     B  
   D     O     d     w     |     �     �     �  "   �     �     �             Y   *  X   �     �  	   �     �  	             .     >     C     R     g     {     �     �     �     �     �  	   �     �     �     �       ]     �   y     T     [     t     �     �     �     �     �     �     	          .     +       E   C   4       &       D           
   <      *   9           !   F      /   #                A       @   =                     $   ;                  B             :       '           0         1   2                                -   (       6   ?         >              "   .   %   ,              )       8       3   5   	                  7    Activities Add Action Add Spacer Add Widgets... Alt Alternative Widgets Always Visible Apply Apply Settings Apply now Author: Auto Hide Back-Button Bottom Cancel Categories Center Close Concatenation sign for shortcuts, e.g. Ctrl+Shift+ Configure Configure activity Create activity... Ctrl Currently being used Delete Email: Forward-Button Get new widgets Height Horizontal-Scroll Input Here Keyboard shortcuts Layout cannot be changed whilst widgets are locked Layout changes must be applied before other changes can be made Layout: Left Left-Button License: Lock Widgets Maximize Panel Meta Middle-Button More Settings... Mouse Actions OK Panel Alignment Remove Panel Right Right-Button Screen Edge Search... Shift Stop activity Stopped activities: Switch The settings of the current module have changed. Do you want to apply the changes or discard them? This shortcut will activate the applet: it will give the keyboard focus to it, and if the applet has a popup (such as the start menu), the popup will be open. Top Undo uninstall Uninstall Uninstall widget Vertical-Scroll Visibility Wallpaper Wallpaper Type: Widgets Width Windows Can Cover Windows Go Below Project-Id-Version: plasma_shell_org
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-09 03:10+0200
PO-Revision-Date: 2017-09-10 13:02+0100
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Actividades Añadir acción Añadir separador Añadir elementos gráficos... Alt Elementos gráficos alternativos Siempre visible Aplicar Aplicar preferencias Aplicar ahora Autor: Ocultar automáticamente Botón de retroceso Abajo Cancelar Categorías Centro Cerrar + Configurar Configurar actividad Crear actividad... Ctrl Actualmente en uso Borrar Correo electrónico: Botón de avance Obtener nuevos elementos gráficos Altura Desplazamiento horizontal Escriba aquí Accesos rápidos de teclado No se puede modificar la distribución mientras estén bloqueados los elementos gráficos Los cambios en la disposición de elementos se deben aplicar antes de hacer más cambios Distribución: Izquierda Botón izquierdo Licencia: Bloquear elementos gráficos Maximizar panel Meta Botón central Más preferencias... Acciones del ratón Aceptar Alineación del panel Eliminar panel Derecha Botón derecho Borde de la pantalla Buscar... Mayúsculas Detener actividad Actividades detenidas: Cambiar Las preferencias del módulo actual han cambiado. ¿Desea aplicar los cambios o descartarlos? Este acceso rápido de teclado activará la miniaplicación: centrará en ella el foco del teclado y, si la miniaplicación posee una ventana emergente (como un menú inteligente), se mostrará dicha ventana emergente. Arriba Deshacer la instalación Desinstalar Desinstalar elemento gráfico Desplazamiento vertical Visibilidad Fondo del escritorio Tipo de fondo del escritorio: Elementos gráficos Anchura Se puede cubrir con ventanas Las ventanas van por detrás 