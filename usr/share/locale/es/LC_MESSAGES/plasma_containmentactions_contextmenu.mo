��          \      �       �   F   �   .     1   ?  4   q  7   �  5   �  1     �  F  -   8     f     o     �      �     �     �                                       plasma_containmentactions_contextmenuConfigure Contextual Menu Plugin plasma_containmentactions_contextmenuLeave... plasma_containmentactions_contextmenuLock Screen plasma_containmentactions_contextmenuRun Command... plasma_containmentactions_contextmenuWallpaper Actions plasma_containmentactions_contextmenu[Other Actions] plasma_containmentactions_contextmenu[Separator] Project-Id-Version: plasma_containmentactions_contextmenu
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2014-06-14 21:36+0200
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-Language: Spanish
X-Poedit-Country: SPAIN
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 Configurar el complemento de menú contextual Salir... Bloquear pantalla Ejecutar orden... Acciones del fondo de escritorio [Otras acciones] [Separador] 