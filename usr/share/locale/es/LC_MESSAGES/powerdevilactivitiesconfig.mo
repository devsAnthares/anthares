��          �      |      �     �     �     �                 ;  	   \     f     �  &   �     �     �     �  	          �   %  �   �  t   ?  7   �  +   �       �       �     �     �  "   �          .     A     J     V  &   r  "   �     �  $   �       	     �     �   �  �   t	     

  ?   
     [
                                      
   	                                                         min Act like Always Define a special behavior Don't use special settings EMAIL OF TRANSLATORSYour emails Hibernate NAME OF TRANSLATORSYour names Never shutdown the screen Never suspend or shutdown the computer PC running on AC power PC running on battery power PC running on low battery Shut down Suspend The Power Management Service appears not to be running.
This can be solved by starting or scheduling it inside "Startup and Shutdown" The activity service is not running.
It is necessary to have the activity manager running to configure activity-specific power management behavior. The activity service is running with bare functionalities.
Names and icons of the activities might not be available. This is meant to be: Act like activity %1Activity "%1" Use separate settings (advanced users only) after Project-Id-Version: powerdevilactivitiesconfig
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-09 03:03+0200
PO-Revision-Date: 2016-05-05 20:58+0200
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
  min Actuar como Siempre Definir un comportamiento especial No usar preferencias especiales ecuadra@eloihr.net Hibernar Eloy Cuadra No apagar la pantalla nunca No suspender ni apagar el equipo nunca Equipo funcionando con energía AC Equipo funcionando con batería Equipo funcionando con batería baja Apagar Suspender Parece que el servicio de gestión de energía no está en ejecución.
Esto se puede solucionar iniciándolo o programándolo en «Inicio y apagado» El servicio de actividades no se está ejecutando.
Es necesario que el gestor de actividades esté funcionando para configurar el comportamiento de la gestión de energía para una actividad específica. El servicio de actividades se está ejecutando con funcionalidad básica.
Es posible que no se muestren los nombres ni los iconos de las actividades. Actividad «%1» Usar preferencias independientes (solo para usuarios avanzados) tras 