��          �   %   �      0     1     E     X     l          �     �     �     �  
   �     �     �                                    "     0  	   <     F     L  �  S            !      A     b     �     �  !   �  #   �  "        *     2     8     =     E     K     S     [     `     e     m  	   z     �     �     	                                                                          
                                                A black sticky note A blue sticky note A green sticky note A pink sticky note A red sticky note A translucent sticky note A white sticky note A yellow sticky note An orange sticky note Appearance Black Blue Bold Green Italic Orange Pink Red Strikethrough Translucent Underline White Yellow Project-Id-Version: plasma_applet_notes
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-07-09 03:12+0200
PO-Revision-Date: 2015-07-28 22:30+0200
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 Una nota adhesiva de color negro Una nota adhesiva de color azul Una nota adhesiva de color verde Una nota adhesiva de color rosa Una nota adhesiva de color rojo Una nota adhesiva transparente Una nota adhesiva de color blanco Una nota adhesiva de color amarillo Una nota adhesiva de color naranja Aspecto Negro Azul Negrita Verde Cursiva Naranja Rosa Rojo Tachado Translúcido Subrayado Blanco Amarillo 