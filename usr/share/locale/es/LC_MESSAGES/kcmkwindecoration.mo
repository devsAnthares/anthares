��            )   �      �  !   �  "   �  '   �  ,     #   ;  &   _  !   �  &   �  '   �     �                 V   $  1   {     �  *   �     �        
     
   "     -     6     ;     D     T     [     a     g  �  p     W     ^  
   e     p     �     �     �  
   �  
   �     �     �     �     �  `   �  >   [     �  ?   �  $   �     	     /	     C	  	   W	     a	  	   g	     q	     �	     �	     �	     �	                                                                          
                                                    	           @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Borders @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large Application menu Border si&ze: Buttons Close Close by double clicking:
 To open the menu, keep the button pressed until it appears. Close windows by double clicking &the menu button Context help Drag buttons between here and the titlebar Drop here to remove button Get New Decorations... Keep above Keep below Maximize Menu Minimize On all desktops Search Shade Theme Titlebar Project-Id-Version: kcmkwindecoration
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-04-14 02:49+0200
PO-Revision-Date: 2015-10-06 20:53+0200
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
com>
First-Translator: Pablo de Vicente <pvicentea@nexo.es>
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 Enorme Grande Sin bordes Sin bordes laterales Normal De tamaño excesivo Pequeño Gigantesco Muy grande Menú de la aplicación &Tamaño del borde: Botones Cerrar Cerrar haciendo doble clic:
 Para abrir el menú, mantenga pulsado el botón hasta que aparezca. Cerrar las ven&tanas al hacer doble clic en el botón de menú Ayuda contextual Arrastre los botones de aquí a la barra de título y viceversa Suelte aquí para eliminar un botón Obtener nuevas decoraciones... Mantener por encima Mantener por debajo Maximizar Menú Minimizar En todos los escritorios Buscar Recoger Tema Barra de título 