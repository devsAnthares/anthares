��          �      l      �     �     �     �  *         +  >   ?  9   ~     �     �  
   �      �       	        (  !   :     \  $   {  	   �     �  �   �  �  8     �     �     �  /        8  G   O  C   �     �     �          !     4     =     F  .   ]     �  -   �     �  	   �  �   �                              
                                                  	                  % &Critical level: &Low level: <b>Battery Levels                     </b> A&t critical level: Battery will be considered critical when it reaches this level Battery will be considered low when it reaches this level Configure Notifications... Critical battery level Do nothing EMAIL OF TRANSLATORSYour emails Enabled Hibernate Low battery level Low level for peripheral devices: NAME OF TRANSLATORSYour names Pause media players when suspending: Shut down Suspend The Power Management Service appears not to be running.
This can be solved by starting or scheduling it inside "Startup and Shutdown" Project-Id-Version: powerdevilglobalconfig
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-11 03:05+0200
PO-Revision-Date: 2017-05-12 20:45+0200
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 % Nivel &crítico: Nive&l bajo: <b>Niveles de batería                     </b> En el nivel crí&tico: La batería se considerará en nivel crítico cuando alcance este nivel La batería se considerará en nivel bajo cuando alcance este nivel Configurar notificaciones... Nivel de batería crítico No hacer nada ecuadra@eloihr.net Activado Hibernar Nivel de batería bajo Nivel bajo para los dispositivos periféricos: Eloy Cuadra Pausar reproductores de música al suspender: Apagar Suspender Parece que el servicio de gestión de energía no está en ejecución.
Esto se puede solucionar iniciándolo o programándolo en «Inicio y apagado» 