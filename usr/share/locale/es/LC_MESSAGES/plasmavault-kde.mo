��    ?        Y         p  �  q  }  J  M  �        
   7     B     K     j     �  <   �     �     �  	   �     �  .     %   A     g     }     �     �     �     �     �     �     �     �  4        B  9   T     �     �     �  �   �  !   �  t   �     8     D     a     n     s  	   �     �  -   �     �  B   �  &     ?   B  '   �  )   �  7   �  .     5   ;  +   q     �     �     �  ,   �          -  9   :  V   t  C   �  �    �  �  �  �  �  y     #     4#     @#  )   G#  .   q#     �#  @   �#     �#     $  
   $     #$  =   @$  "   ~$  '   �$     �$     �$     �$      �$      %     6%     ?%     ^%      d%  ?   �%     �%  X   �%     8&     R&     c&  �   k&  (   W'  �   �'     (  %   (     C(  	   U(     _(     (     �(  A   �(     �(  Y   �(  %   I)  Q   o)  2   �)  6   �)  A   +*  +   m*  G   �*  :   �*  %   +  '   B+     j+  .   �+     �+     �+  D   �+  _   .,     �,     !      	   2   ?   
   /                 +   (   *       1            )   >           8          9   $              7             .       '   6          %   <             3   ,          ;       =   &                              -           #      "          :      5      0                                   4               <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body style=" font-family:'hlv'; font-size:9pt; font-weight:400; font-style:normal;">
<p style="-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><br /></p></body></html> <b>Security notice:</b>
                             According to a security audit by Taylor Hornby (Defuse Security),
                             the current implementation of Encfs is vulnerable or potentially vulnerable
                             to multiple types of attacks.
                             For example, an attacker with read/write access
                             to encrypted data might lower the decryption complexity
                             for subsequently encrypted data without this being noticed by a legitimate user,
                             or might use timing analysis to deduce information.
                             <br /><br />
                             This means that you should not synchronize
                             the encrypted data to a cloud storage service,
                             or use it in other circumstances where the attacker
                             can frequently access the encrypted data.
                             <br /><br />
                             See <a href='http://defuse.ca/audits/encfs.htm'>defuse.ca/audits/encfs.htm</a> for more information. <b>Security notice:</b>
                             CryFS encrypts your files, so you can safely store them anywhere.
                             It works well together with cloud services like Dropbox, iCloud, OneDrive and others.
                             <br /><br />
                             Unlike some other file-system overlay solutions,
                             it does not expose the directory structure,
                             the number of files nor the file sizes
                             through the encrypted data format.
                             <br /><br />
                             One important thing to note is that,
                             while CryFS is considered safe,
                             there is no independent security audit
                             which confirms this. @title:windowCreate a New Vault Activities Backend: Can not create the mount point Can not open an unknown vault. Change Choose the encryption system you want to use for this vault: Choose the used cypher: Close the vault Configure Configure Vault... Configured backend can not be instantiated: %1 Configured backend does not exist: %1 Correct version found Create Create a New Vault... CryFS Device is already open Device is not open Dialog Do not show this notice again EncFS Encrypted data location Failed to create directories, check your permissions Failed to execute Failed to fetch the list of applications using this vault Failed to open: %1 Forcefully close  General If you limit this vault only to certain activities, it will be shown in the applet only when you are in those activities. Furthermore, when you switch to an activity it should not be available in, it will automatically be closed. Limit to the selected activities: Mind that there is no way to recover a forgotten password. If you forget the password, your data is as good as gone. Mount point Mount point is not specified Mount point: Next Open with File Manager Password: Plasma Vault Please enter the password to open this vault: Previous The mount point directory is not empty, refusing to open the vault The specified backend is not available The vault configuration can only be changed while it is closed. The vault is unknown, can not close it. The vault is unknown, can not destroy it. This device is already registered. Can not recreate it. This directory already contains encrypted data Unable to close the vault, an application is using it Unable to close the vault, it is used by %1 Unable to detect the version Unable to perform the operation Unknown device Unknown error, unable to create the backend. Use the default cipher Vaul&t name: Wrong version installed. The required version is %1.%2.%3 You need to select empty directories for the encrypted storage and for the mount point formatting the message for a command, as in encfs: not found%1: %2 Project-Id-Version: plasmavault-kde
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-11 05:53+0100
PO-Revision-Date: 2017-12-14 22:21+0100
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body style=" font-family:'hlv'; font-size:9pt; font-weight:400; font-style:normal;">
<p style="-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><br /></p></body></html> <b>Aviso de seguridad:</b>
                             Según una auditoría de seguridad realizada por Taylor Hornby (Defuse Security),
                             la implementación actual de Encfs es vulnerable o potencialmente vulnerable
                             a diversos tipos de ataques.
                             Por ejemplo, un atacante con acceso de lectura/escritura
                             a los datos cifrados, puede rebajar la complejidad del descifrado
                             para los datos cifrados posteriores sin que esto sea advertido por un usuario legítimo,
                             o puede usar un análisis de tiempos para deducir la información.
                             <br /><br />
                             Esto significa que usted no debería sincronizar los datos
                             cifrados con un servicio de almacenamiento en la red,
                             ni usarlos en otras circunstancias en las que el atacante
                             pueda acceder con frecuencia a los datos cifrados.
                             <br /><br />
                             Consulte <a href='http://defuse.ca/audits/encfs.htm'>defuse.ca/audits/encfs.htm</a> para más información. <b>Aviso de seguridad:</b>
                             CryFS cifra sus archivos, por lo que puede guardarlos con seguridad en cualquier lugar.
                             Funciona bien en conjunción con servicios en la nube, como Dropbox, iCloud y OneDrive, entre otros.
                             <br /><br />
                             Al contrario que otras soluciones de capas de sistemas de archivos,
                             no expone la estructura de directorios,
                             el número de archivos ni sus tamaños
                             a través del formato de datos cifrados.
                             <br /><br />
                             Algo importante a tener en cuenta es que,
                             aunque CryFS se considera seguro, no existe
                             ninguna auditoría de seguridad independiente
                             que lo confirme. Crear una nueva caja fuerte Actividades Motor: No se ha podido crear el punto de montaje No se puede abrir una caja fuerte desconocida. Cambiar Escoja el sistema de cifrado que desea usar en esta caja fuerte: Escoja el cifrado usado: Cerrar la caja fuerte Configurar Configurar la caja fuerte... No se ha podido crear una instancia del motor configurado: %1 El motor configurado no existe: %1 Se ha encontrado una versión correcta. Crear Crear una nueva caja fuerte... CryFS El dispositivo ya está abierto. El dispositivo no está abierto. Diálogo No volver a mostrar este aviso EncFS Ubicación de los datos cifrados La creación de directorios ha fallado; compruebe los permisos. La ejecución ha fallado. Ha fallado la obtención de la lista de aplicaciones que están usando esta caja fuerte. No se ha podido abrir: %1 Forzar el cierre General Si limita esta caja fuerte solo a ciertas actividades, se mostrará en la miniaplicación solo cuando esté en dichas actividades. Además, cuando cambie a una actividad en la que no deba estar disponible, se cerrará automáticamente. Limitar a las actividades seleccionadas: Recuerde que no hay ningún modo de recuperar una contraseña olvidada. Si olvida la contraseña, sus datos están prácticamente perdidos. Punto de montaje No se ha indicado el punto de montaje Punto de montaje: Siguiente Abrir con el gestor de archivos Contraseña: Cajas fuertes de Plasma Por favor, introduzca la contraseña para abrir esta caja fuerte: Anterior El directorio del punto de montaje no está vacío: se ha rechazado abrir la caja fuerte. El motor indicado no está disponible La configuración de la caja fuerte solo se puede modificar cuando está cerrada. No se reconoce la caja fuerte: no se puede cerrar. No se reconoce esta caja fuerte: no se puede destruir. Este dispositivo ya está registrado. No se puede volver a crear. Este directorio ya contiene datos cifrados. No se ha podido cerrar la caja fuerte: una aplicación la está usando. No se ha podido cerrar la caja fuerte: %1 la está usando. No se ha podido detectar la versión. No se ha podido realizar la operación. Dispositivo desconocido Error desconocido: no se puede crear el motor. Usar el cifrado por omisión Nombre de la ca&ja fuerte: Versión incorrecta instalada. La versión necesaria es la %1.%2.%3. Debe seleccionar directorios vacíos para el almacenamiento cifrado y para el punto de montaje. %1: %2 