��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  �  K     %  ,   +  �  X  �  	  Z   �
               ,  ,   <     i       ,   �  $   �                                          
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-08-07 12:01+0200
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
First-Translator: Pablo de Vicente <pvicentea@nexo.es>
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
  segs Duración má&xima de indicación de inicio: <H1>Notificación en la barra de tareas</H1>
Puede activar un segundo método de notificación de inicio para
usar en la barra de tareas, donde aparece un botón con un reloj de
arena para indicar que la aplicación se está cargando.
Es posible que algunas aplicaciones no reciban esta
notificación de inicio. En tal caso, el botón desaparecerá
después del tiempo fijado en «Duración máxima de indicación de inicio». <h1>Cursor ocupado</h1>
KDE ofrece un cursor ocupado para notificar el inicio de una aplicación.
Para activar el cursor ocupado, seleccione en la lista desplegable el tipo de información visual.
Puede ocurrir que algunas aplicaciones no sean compatibles con esta
notificación de inicio. En este caso, el cursor dejará de parpadear una vez
transcurrido el tiempo indicado en «Duración máxima de indicación de inicio». <h1>Lanzamiento</h1> Aquí puede configurar la notificación del lanzador de aplicaciones. Cursor parpadeante Cursor elástico Cursor ocu&pado Ac&tivar notificación en la barra de tareas Sin cursor de ocupado Cursor ocupado pasivo Duración má&xima de indicación de inicio: &Notificación en la barra de tareas 