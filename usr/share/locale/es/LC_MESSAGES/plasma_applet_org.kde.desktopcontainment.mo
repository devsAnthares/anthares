��    `        �         (     )     1     B     Q     W     ^     j     {     �     �  /   �  -   �     �     �  
   		  
   	     	     ,	     1	     8	     @	     R	     _	     d	  
   l	     w	     �	     �	     �	  	   �	     �	  	   �	     �	     �	     �	     
     
  	   #
     -
     4
     H
  	   M
     W
     ]
     c
     h
     m
  	   v
     �
     �
     �
     �
     �
     �
     �
     �
  <   �
               /     6     =     X     ^     e     j  
   ~     �     �     �     �     �  )   �               5     :     @     M     U     ]     f  
   x     �     �  	   �     �     �     �     �     �     �     �  E   �  *   A  �  l               3     H     O     V     c     |     �     �  
   �     �     �     �     �     �     �                          /     E     K     X     d     q     �  )   �     �     �     �            /         P     m     �  	   �     �     �     �     �     �  	   �     �  
   �            	   #     -      3     T     [     c     k  J   |     �     �     �       	        "     *     0     6     P     a     u     �     �     �  0   �  !   
  .   ,     [     c     l     u     �     �     �     �     �     �     �     �     �     �                <  !   Z  Q   |     �     J       -      M       %   Q           ^              U       L   S   $      .       H       X          Y   8   *         <   &          '       ]   N       3                     `          #   V             /   T   G   9   @   >   =   6   2   O   C   F   ?      0      
      A   E       	      P              _                D          Z               1   R   ,   +                \           I   )   B   K      :   !   W   4          ;               (      5   "       7       [           &Delete &Empty Trash Bin &Move to Trash &Open &Paste &Properties &Refresh Desktop &Refresh View &Reload &Rename @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Align Appearance: Arrange In Arrange in Arrangement: Back Cancel Columns Configure Desktop Custom title Date Default Descending Description Deselect All Desktop Layout Enter custom title here Features: File name pattern: File type File types: Filter Folder preview popups Folders First Folders first Full path Got it Hide Files Matching Huge Icon Size Icons Large Left List Location Location: Lock in place Locked Medium More Preview Options... Name None OK Panel button: Press and hold widgets to move them and reveal their handles Preview Plugins Preview thumbnails Remove Resize Restore from trashRestore Right Rotate Rows Search file type... Select All Select Folder Selection markers Show All Files Show Files Matching Show a place: Show files linked to the current activity Show the Desktop folder Show the desktop toolbox Size Small Small Medium Sort By Sort by Sorting: Specify a folder: Text lines Tiny Title: Tool tips Tweaks Type Type a path or a URL here Unsorted Use a custom icon Widget Handling Widgets unlocked You can press and hold widgets to move them and reveal their handles. whether to use icon or list viewView mode Project-Id-Version: plasma_applet_org
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:11+0100
PO-Revision-Date: 2017-11-20 21:42+0100
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 &Borrar Vaciar la pap&elera &Mover a la Papelera &Abrir &Pegar &Propiedades &Refrescar el escritorio &Refrescar vista &Volver a cargar &Cambiar nombre Escoger... Borrar icono Alinear Aspecto: Organizar en Organizar en Organización: Volver Cancelar Columnas Configurar escritorio Título personalizado Fecha Por omisión Descendente Descripción Desmarcar todo Distribución del escritorio Introduzca aquí un título personalizado Funcionalidades: Patrón de nombre de archivo: Tipo de archivo Tipos de archivos: Filtro Ventanas emergentes de vista previa de carpetas Las carpetas en primer lugar Las carpetas en primer lugar Ruta completa Entendido Ocultar archivos coincidentes Enorme Tamaño del icono Iconos Grande Izquierda Lista Ubicación Ubicación: Bloquear en el lugar Bloqueado Medio Más opciones de vista previa... Nombre Ninguno Aceptar Botón de panel: Mantener pulsados los elementos gráficos para moverlos y revelar sus asas Complementos de vista previa Miniaturas de vista previa Eliminar Cambiar tamaño Restaurar Derecha Rotar Filas Buscar tipo de archivo... Seleccionar todo Seleccionar carpeta Marcadores de selección Mostrar todos los archivos Mostrar archivos coincidentes Mostrar un lugar: Mostrar archivos enlazados a la actividad actual Mostrar la carpeta del escritorio Mostrar la caja de herramientas del escritorio Tamaño Pequeño Pequeño Ordenar por Ordenar por Orden: Especificar una carpeta: Líneas de texto Diminuto Título: Ayudas emergentes Microajustes Tipo Escriba aquí una ruta o un URL Sin ordenar Usar un icono personalizado Manejo de elementos gráficos Elementos gráficos desbloqueados Puede mantener pulsados los elementos gráficos para moverlos y revelar sus asas. Modo de la vista 