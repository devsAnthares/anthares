��    B      ,  Y   <      �  i   �  m        y  b   �     �     	  6        O  7   _     �     �  	   �     �  (   �  )   �  )   (     R     o  Y   u     �     �  �   �      y	  .   �	     �	  
   �	     �	     �	     
     
     !
     5
     B
     J
     c
     r
     w
     �
     �
     �
     �
     �
  	   �
  
   �
     �
     �
  	     
     
   *     5     B  	   `     j     o  
   �  �   �     (  8   8  �   q     �  8   	  ,   B  ,   o  %   �     �  �  �  O   �  q   �  "   C  {   f  #   �       O        m  L   �     �     �     �     �  D   	  A   N  B   �  )   �     �  l        r     �  �   �  B   <  M        �     �     �               5     >     Z  
   m  *   x     �     �  O   �  #        +     >     G     N     ^     p     �     �     �     �     �     �  !   �  	        %  "   ,     O  �   [        ;     �   N     �  M   �  *   9  +   d  9   �  $   �     $   !   %          ;   ,          B                 8   #                 7   9       +              )   ?   -   <         6                                    *   2       >                1       @       .   5   3   :   A         &      /                4              0      "             =   (   '         	   
           @info User changed Phonon backendTo apply the backend change you will have to log out and back in again. A list of Phonon Backends found on your system.  The order here determines the order Phonon will use them in. Apply Device List To... Apply the currently shown device preference list to the following other audio playback categories: Audio Hardware Setup Audio Playback Audio Playback Device Preference for the '%1' Category Audio Recording Audio Recording Device Preference for the '%1' Category Backend Colin Guthrie Connector Copyright 2006 Matthias Kretz Default Audio Playback Device Preference Default Audio Recording Device Preference Default Video Recording Device Preference Default/Unspecified Category Defer Defines the default ordering of devices which can be overridden by individual categories. Device Configuration Device Preference Devices found on your system, suitable for the selected category.  Choose the device that you wish to be used by the applications. EMAIL OF TRANSLATORSYour emails Failed to set the selected audio output device Front Center Front Left Front Left of Center Front Right Front Right of Center Hardware Independent Devices Input Levels Invalid KDE Audio Hardware Setup Matthias Kretz Mono NAME OF TRANSLATORSYour names Phonon Configuration Module Playback (%1) Prefer Profile Rear Center Rear Left Rear Right Recording (%1) Show advanced devices Side Left Side Right Sound Card Sound Device Speaker Placement and Testing Subwoofer Test Test the selected device Testing %1 The order determines the preference of the devices. If for some reason the first device cannot be used Phonon will try to use the second, and so on. Unknown Channel Use the currently shown device list for more categories. Various categories of media use cases.  For each category, you may choose what device you prefer to be used by the Phonon applications. Video Recording Video Recording Device Preference for the '%1' Category  Your backend may not support audio recording Your backend may not support video recording no preference for the selected device prefer the selected device Project-Id-Version: kcm_phonon
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2014-03-26 14:36+0100
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 Para aplicar el cambio del motor tendrá que cerrar sesión y volver a abrirla. Una lista de motores de Phonon encontrados en su sistema. Este orden determina el orden en que los usará Phonon. Aplicar lista de dispositivos a... Aplicar la lista de preferencia de dispositivo mostrada actualmente a las siguientes categorías de reproducción de audio: Configuración de hardware de audio Reproducción de audio Preferencia del dispositivo de reproducción de audio para la categoría «%1» Grabación de audio Preferencia del dispositivo de grabación de audio para la categoría «%1» Motor Colin Guthrie Conector Copyright 2006 Matthias Kretz Preferencia del dispositivo de reproducción de audio predeterminado Preferencia del dispositivo de grabación de audio predeterminado Preferencia del dispositivo de grabación de vídeo predeterminado Categoría predeterminada/no especificada Diferir Define el orden predeterminado de dispositivos, que se puede sobrescribir mediante categorías individuales. Configuración de dispositivos Preferencia del dispositivo Dispositivos encontrados en su sistema, y aptos para la categoría seleccionada. Elija el dispositivo que desee que utilicen las aplicaciones. ecuadra@eloihr.net,cronopios@gmail.com,the.blue.valkyrie@gmail.com Ha fallado el establecimiento del dispositivo de salida de audio seleccionado Frontal central Frontal izquierdo Frontal izquierdo o central Frontal derecho Frontal derecho o central Hardware Dispositivos independientes Niveles de entrada No válido Configuración de hardware de audio de KDE Matthias Kretz Mono Eloy Cuadra,Enrique Matías Sánchez (Quique),Cristina Yenyxe González García Módulo de configuración de Phonon Reproducción (%1) Preferir Perfil Trasero central Trasero izquierdo Trasero derecho Grabación (%1) Mostrar dispositivos avanzados Lateral izquierdo Lateral derecho Tarjeta de sonido Dispositivo de sonido Ubicación de altavoces y pruebas Subwoofer Prueba Probar el dispositivo seleccionado Probando %1 El orden determina la preferencia de los dispositivos. Si por  alguna razón no se pudiera usar el primero, Phonon intentaría usar el segundo y así sucesivamente. Canal desconocido Usar la lista actual de dispositivos para más categorías. Varias categorías de casos de uso de medios. En cada una, puede elegir qué dispositivo prefiere que utilicen las aplicaciones Phonon. Grabación de vídeo Preferencia del dispositivo de grabación de vídeo para la categoría «%1» Puede que su motor no permita grabar audio Puede que su motor no permita grabar vídeo sin orden de preferencia para el dispositivo seleccionado preferir el dispositivo seleccionado 