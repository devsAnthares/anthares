��          �      �           	               4  	   I     S      c  "   �      �     �     �  	   �     �               )  
   9     D     S     a     g     {  �  �     ,     /  !   7     Y     s          �  ,   �  *   �     �  	                  $     3     S  
   g     r     �     �     �     �                              	                                     
                                      K (HH:MM) (In minutes - min. 1, max. 600) Activate Night Color Automatic Detect location EMAIL OF TRANSLATORSYour emails Error: Morning not before evening. Error: Transition time overlaps. Latitude Location Longitude NAME OF TRANSLATORSYour names Night Color Night Color Temperature:  Operation mode: Roman Gilg Sunrise begins Sunset begins Times Transition duration and ends Project-Id-Version: kcm_nightcolor
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-12 03:09+0100
PO-Revision-Date: 2017-12-14 22:26+0100
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
  K (HH:MM) (En minutos [mín. 1, máx. 600]) Activar el color nocturno Automático Detectar posición ecuadra@eloihr.net Error: la mañana no es anterior a la tarde. Error: el tiempo de transición se solapa. Latitud Posición Longitud Eloy Cuadra Color nocturno Temperatura del color nocturno: Modo de operación: Roman Gilg El amanecer empieza a las La puesta de sol empieza a las Horas Duración de la transición y termina a las 