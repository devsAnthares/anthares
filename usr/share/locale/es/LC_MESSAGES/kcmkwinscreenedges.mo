��    %      D  5   l      @     A     E     G  	   Y     c     |     �     �     �     �     �     �            S   ,  w   �     �  M         [     |  ?   �     �  	   �     �     
     #  %   2     X     e  F   �  #   �     �  ]     R   f     �     �  �  �     �	     �	     �	  	   �	     �	     �	     �	     �	     �	  %   
     :
  %   S
     y
     �
  ]   �
  W   �
     V  O   o     �     �  C   �     +     7     G     ^     m  )   �     �     �  c   �  1   0  #   b  h   �  b   �     R     g               !      "          #   %                                                                            $                          	                
                            ms % %1 - All Desktops %1 - Cube %1 - Current Application %1 - Current Desktop %1 - Cylinder %1 - Sphere &Reactivation delay: &Switch desktop on edge: Activation &delay: Active Screen Corners and Edges Activity Manager Always Enabled Amount of time required after triggering an action until the next trigger can occur Amount of time required for the mouse cursor to be pushed against the edge of the screen before the action is triggered Application Launcher Change desktop when the mouse cursor is pushed against the edge of the screen EMAIL OF TRANSLATORSYour emails Lock Screen Maximize windows by dragging them to the top edge of the screen NAME OF TRANSLATORSYour names No Action Only When Moving Windows Open krunnerRun Command Other Settings Quarter tiling triggered in the outer Show Desktop Switch desktop on edgeDisabled Tile windows by dragging them to the left or right edges of the screen Toggle alternative window switching Toggle window switching Trigger an action by pushing the mouse cursor against the corresponding screen edge or corner Trigger an action by swiping from the screen edge towards the center of the screen Window Management of the screen Project-Id-Version: kcmkwinscreenedges
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-30 03:13+0100
PO-Revision-Date: 2018-01-16 04:58+0100
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
  ms % %1 - Todos los escritorios %1 - Cubo %1 - Aplicación actual %1 - Escritorio actual %1 - Cilindro %1 - Esfera &Retardo de reactivación: Cambiar de e&scritorio en los bordes: Retar&do de activación: Esquinas y bordes de pantalla activos Gestor de actividades Siempre activado El tiempo necesario tras el lanzamiento de la acción hasta que puede tener lugar otro evento El tiempo que el ratón debe ser empujado contra el borde para que se lance la acción. Lanzador de aplicaciones Cambiar el escritorio cuando el ratón se empuja contra el borde de la pantalla ecuadra@eloihr.net Bloquear la pantalla Maximizar ventanas al arrastrarlas al borde superior de la pantalla Eloy Cuadra Ninguna acción Solo al mover ventanas Ejecutar orden Otras preferencias Mosaico en cuartos lanzado en el exterior Mostrar el escritorio Desactivado Disponer las ventanas en mosaico al arrastrarlas a uno de bordes derecho o izquierdo de la pantalla Conmutar a un intercambio alternativo de ventanas Conmutar el intercambio de ventanas Provocar una acción al situar el cursor del ratón en la correspondiente esquina o borde de la pantalla Provocar una acción al deslizar el dedo desde el borde de la pantalla hacia el centro de la misma Gestión de ventanas de la pantalla 