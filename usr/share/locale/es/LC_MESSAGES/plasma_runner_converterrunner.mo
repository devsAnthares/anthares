��          4      L       `   �   a   L   �   �  E  �     	   �                    Converts the value of :q: when :q: is made up of "value unit [>, to, as, in] unit". You can use the Unit converter applet to find all available units. list of words that can used as amount of 'unit1' [in|to|as] 'unit2'in;to;as Project-Id-Version: plasma_runner_converterrunner
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2009-12-27 18:40+0100
Last-Translator: Enrique Matias Sanchez (Quique) <cronopios@gmail.com>
Language-Team: Spanish <es@li.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=n != 1;
 Convierte el valor de :q: cuando :q: se compone de «valor unidad [>, a, como, en] unidad». Puede usar la miniaplicación de conversión de unidades para encontrar todas las unidades disponibles. en;a;como 