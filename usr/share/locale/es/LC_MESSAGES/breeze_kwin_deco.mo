��    D      <  a   \      �     �     �     �  !     "   %  &   H  ,   o  #   �  &   �  !   �  &   	  '   0  "   X  #   {  !   �  "   �  '   �       +     2   <     o  
   �     �     �     �     �     �     �     �     �     	  !   	  +   *	     V	     v	      {	     �	     �	     �	     �	     �	  !   �	     
  	    
     *
     2
     R
     m
  &   �
     �
     �
     �
     �
     �
     �
     �
            $        A     R     l     ~     �     �     �  >   �  �       �     �  $   �     �     �  	   �                    )     2  
   :     E     L     R     Z  
   c     n  @   v  H   �                +     D     W     _     x     �     �  "   �     �  1   �  ;   �  )   9     c  )   j     �  "   �     �     �  )   �  *     	   ;     E     K  (   Q      z     �  3   �  !   �       &        4     <  	   D     N     W  	   r  2   |     �  #   �     �       %   -     S  (   i  	   �     D   8      1                 )                  .               3   @   4       &      /   >       ?      <   ;             7       (          
                 $                  0   5       A   :   #   2         C   +                        =   '             *                 	          ,   !                             B   9   6       "       %   -     ms % &Matching window property:  @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Border @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large @item:inlistbox Button size:Large @item:inlistbox Button size:Medium @item:inlistbox Button size:None @item:inlistbox Button size:Small @item:inlistbox Button size:Very Large Add Add handle to resize windows with no border Allow resizing maximized windows from window edges Anima&tions duration: Animations B&utton size: Border size: Center Center (Full Width) Class:  Color: Decoration Options Detect Window Properties Dialog Draw a circle around close button Draw separator between Title Bar and Window Draw window background gradient Edit Edit Exception - Breeze Settings Enable animations Enable/disable this exception Exception Type General Hide window title bar Information about Selected Window Left Move Down Move Up New Exception - Breeze Settings Question - Breeze Settings Regular Expression Regular Expression syntax is incorrect Regular expression &to match:  Remove Remove selected exception? Right Shadows Si&ze: Tiny Tit&le alignment: Title:  Use window class (whole application) Use window title Warning - Breeze Settings Window Class Name Window Identification Window Property Selection Window Title Window-Specific Overrides strength of the shadow (from transparent to opaque)S&trength: Project-Id-Version: breeze_kwin_deco
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-09 03:20+0100
PO-Revision-Date: 2018-01-13 18:26+0100
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
  ms % C&oincidir propiedad de la ventana:  Enorme Grande Sin borde Sin bordes laterales Normal Desmesurado Pequeño Gigante Muy grande Grande Medio Ninguno Pequeño Muy grande Añadir Añadir un asa para cambiar el tamaño de las ventanas sin borde Permitir cambiar el tamaño de las ventanas maximizadas desde sus bordes &Duración de las animaciones: Animaciones Ta&maño de los botones: Tamaño del borde: Centrar Centrar (ancho completo) Clase:  Color: Opciones de decoración Detectar propiedades de la ventana Diálogo Dibujar un círculo alrededor el botón de cierre Dibujar un separador entre la barra de título y la ventana Dibujar gradiente del fondo de la ventana Editar Editar excepción - Preferencias de Brisa Activar animaciones Activar/desactivar esta excepción Tipo de excepción General Ocultar la barra de título de la ventana Información sobre la ventana seleccionada Izquierda Bajar Subir Nueva excepción - Preferencias de Brisa Pregunta - Preferencias de Brisa Expresión regular La sintaxis de la expresión regular no es correcta E&xpresión regular a coincidir:  Eliminar ¿Eliminar la excepción seleccionada? Derecha Sombras &Tamaño: Pequeño A&lineamiento del título: Título:  Usar la clase de la ventana (aplicación completa) Usar el título de la ventana Advertencia - Preferencias de Brisa Nombre de la clase de ventana Identificación de la ventana Selección de propiedad de la ventana Título de la ventana Sustituciones específicas de la ventana Solide&z: 