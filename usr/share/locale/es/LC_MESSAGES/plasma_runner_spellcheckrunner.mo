��          t      �                 '     6     R     Z     w  Q   �     �      �       �    !   �     �           '  -   0  -   ^     �     �     �     �     
      	                                                 &Require trigger word &Trigger word: Checks the spelling of :q:. Correct Could not find a dictionary. Spell Check Settings Spelling checking runner syntax, first word is trigger word, e.g.  "spell".%1:q: Suggested words: %1 separator for a list of words,  spell Project-Id-Version: krunner_spellcheckrunner
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2016-04-09 13:21+0200
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 &Exigir la palabra de activación Palabra de ac&tivación: Comprueba la ortografía de :q:. Correcto No fue posible encontrar ningún diccionario. Preferencias de la corrección de ortografía %1:q: Palabras sugeridas: %1 ,  ortografía 