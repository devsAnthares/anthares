��          �      L      �  &   �  +   �       @     	   ]     g     �     �     �     �  (   �     �     �  ,   �               +     7  �  ;  &   �  )        7     ?     H      Q     r     x     �     �  ,   �     �     �  5   �  	   #     -     =     M        
                          	                                                                  Do you want to suspend to RAM (sleep)? Do you want to suspend to disk (hibernate)? General Heading for list of actions (leave, lock, shutdown, ...)Actions Hibernate Hibernate (suspend to disk) Leave Leave... Lock Lock the screen Logout, turn off or restart the computer No Sleep (suspend to RAM) Start a parallel session as a different user Suspend Switch User Switch user Yes Project-Id-Version: plasma_applet_org
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2015-07-05 20:04+0200
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 ¿Desea suspender en memoria (dormir)? ¿Desea suspender en el disco (hibernar)? General Acciones Hibernar Hibernar (suspender en el disco) Salir Salir... Bloquear Bloquear la pantalla Cerrar sesión, apagar o reiniciar el equipo No Dormir (suspender en memoria) Iniciar una sesión paralela como un usuario distinto Suspender Cambiar usuario Cambiar usuario Sí 