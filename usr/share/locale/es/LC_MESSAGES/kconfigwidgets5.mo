��    w      �  �   �      
     
  	   &
     0
     =
     K
     Q
     X
     i
     o
     w
     
     �
     �
     �
     �
  	   �
     �
     �
  
   �
     �
     �
       
             !     (  	   7     A  
   G  
   R     ]     i     x     ~     �     �     �     �  ;   �  -   �  #        ;     T     l  
   �     �     �  
   �     �     �     �               7     K     P  	   k      u     �     �     �  
   �     �     �               .     E     V     f     v     �     �  
   �     �  )   �     �          #     2     A     R     X     `     s  '   �     �     �     �     �     �     
           .  C   <     �  s   �           %     :     Q     f     �     �      �     �  -   �  /        4  	   =  !   G     i      �     �     �     �     �     �  �  �     �     �     �     �                    .     6     >     E  
   [     f     w     �     �     �     �     �  
   �     �     �     �  	             "     4     A  
   H     S     f     t     �     �     �  	   �     �     �  
   �  	   �     �       
        (     <     K  #   S     w     �  !   �  #   �  )   �  $        *     @  $   H     m     |     �     �     �     �     �  "   �  #        0     K     c     {     �     �  	   �     �     �     �  .   �     (  #   E     i     w     �     �  	   �     �  #   �  E   �     6     G  (   Y     �  #   �     �     �  !   �  a        v  �   �  7   @  #   x  $   �  *   �  '   �          )  $   E     j  *   {  0   �     �     �  :   �  -   $  9   R     �  	   �     �     �  
   �     o   5   l   W   	             1               R   6          J         >   u   d           V          M   T       X   r   v          G   .   Q   \       a              E   -       O               ^          b   7           +   Y   C      [       4                 H       k   B   c       ]   I   *   P   (   h   w   ;   N   3   
                @      "   S      _                   L   $      9   F      D          m   U   %       K       &                     t   Z   `   !       ,   0   g   f   p          i      2   e       n   /      q       ?      <   A   #   8          =   '   s   )               :   j           %1 &Handbook &About %1 &Actual Size &Add Bookmark &Back &Close &Configure %1... &Copy &Delete &Donate &Edit Bookmarks... &Find... &First Page &Fit to Page &Forward &Go To... &Go to Line... &Go to Page... &Last Page &Mail... &Move to Trash &New &Next Page &Open... &Paste &Previous Page &Print... &Quit &Redisplay &Rename... &Replace... &Report Bug... &Save &Save Settings &Spelling... &Undo &Up &Zoom... @action:button Goes to next tip, opposite to previous&Next @action:button Goes to previous tip&Previous @option:check&Show tips on startup @titleDid you know...?
 @title:windowConfigure @title:windowTip of the Day About &KDE C&lear Check spelling in document Clear List Close document Configure &Notifications... Configure S&hortcuts... Configure Tool&bars... Copy selection to clipboard Create new document Cu&t Cut selection to clipboard Dese&lect EMAIL OF TRANSLATORSYour emails Encodings menuAutodetect Encodings menuDefault F&ull Screen Mode Find &Next Find Pre&vious Fit to Page &Height Fit to Page &Width Go back in document Go forward in document Go to first page Go to last page Go to next page Go to previous page Go up NAME OF TRANSLATORSYour names No Entries Open &Recent Open a document which was recently opened Open an existing document Paste clipboard content Print Previe&w Print document Quit application Re&do Re&vert Redisplay document Redo last undone action Revert unsaved changes made to document Save &As... Save document Save document under a new name Select &All Select zoom level Send document by mail Show &Menubar Show &Toolbar Show Menubar<p>Shows the menubar again after it has been hidden</p> Show St&atusbar Show Statusbar<p>Shows the statusbar, which is the bar at the bottom of the window used for status information.</p> Show a print preview of document Show or hide menubar Show or hide statusbar Show or hide toolbar Switch Application &Language... Tip of the &Day Undo last action View document at its actual size What's &This? You are not allowed to save the configuration You will be asked to authenticate before saving Zoom &In Zoom &Out Zoom to fit page height in window Zoom to fit page in window Zoom to fit page width in window go back&Back go forward&Forward home page&Home show help&Help without name Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-16 03:10+0100
PO-Revision-Date: 2017-08-10 20:40+0100
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
First-Translator: Boris Wesslowski <Boris@Wesslowski.com>
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 &Manual de %1 &Acerca de %1 Tamaño re&al &Añadir marcador &Atrás &Cerrar &Configurar %1... &Copiar &Borrar &Donar &Editar marcadores... &Buscar... P&rimera página A&justar a la página A&vanzar &Ir a... Ir a la &línea... &Ir a la página... Ú&ltima página &Correo... &Mover a la papelera &Nuevo Página siguie&nte &Abrir... &Pegar &Página anterior Im&primir... &Salir &Redibujar Cambiar nomb&re... &Remplazar... Informa&r de fallo... &Guardar &Guardar preferencias &Ortografía... &Deshacer Arr&iba &Ampliación... Siguie&nte &Anterior &Mostrar sugerencias al inicio ¿Sabía...?
 Configurar Sugerencia del día Acerca de &KDE &Borrar Comprobar ortografía del documento Borrar lista Cerrar documento Configurar las &notificaciones... Configurar los accesos &rápidos... Configurar las barras de herramien&tas... Copiar la selección al portapapeles Crear nuevo documento Cor&tar Cortar la selección al portapapeles Dese&leccionar ecuadra@eloihr.net Autodetectar Predeterminada Modo de pantalla c&ompleta Buscar siguie&nte Buscar &anterior Ajustar a la a&ltura de la página Ajustar a la &anchura de la página Retroceder en el documento Avanzar en el documento Ir a la primera página Ir a la última página Ir a la página siguiente Ir a la página anterior Ir arriba Eloy Cuadra Sin entradas Abrir &reciente Abrir un documento que se abrió recientemente Abrir un documento existente Pegar el contenido del portapapeles Vista pre&via Imprimir documento Salir de la aplicación Re&hacer Re&vertir Redibujar el documento Rehacer la última acción deshecha Revertir los cambios sin guardar que se han realizado en el documento Gu&ardar como... Guardar documento Guardar el documento con un nuevo nombre Seleccionar &todo Seleccionar el nivel de ampliación Enviar el documento por correo Mostrar la barra de &menú Mostrar la barra de herramien&tas Mostrar la barra de menú<p>Muestra la barra de menú de nuevo después de que se ha ocultado</p> Mostrar la barra de &estado Mostrar la barra de estado<p>Muestra la barra de estado, que es la barra que hay en la parte inferior de la ventana y que se utiliza para mostrar información de estado.</p> Mostrar una vista previa de la impresión del documento Mostrar u ocultar la barra de menú Mostrar u ocultar la barra de estado Mostrar u ocultar la barra de herramientas Cambiar e&l idioma de la aplicación... Sugerencia del &día Deshacer la última acción Ver el documento con su tamaño real ¿Qué es es&to? No se le permite guardar la configuración Se le pedirá que se autentique antes de guardar Ampl&iar Red&ucir Ampliar para ajustar la altura de la página en la ventana Ampliar para ajustar la página en la ventana Ampliar para ajustar el ancho de la página en la ventana &Atrás A&delante &Inicio A&yuda sin nombre 