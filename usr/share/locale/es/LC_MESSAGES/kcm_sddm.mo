��    ,      |  ;   �      �  (   �     �  �   �     �     �     �     �     �     �     �     �     �               %     1      J     k     s     �     �     �     �     �     �     �               /     4     I     Y     l     y     �     �      �  7   �                     1     6  �  <     �     �     �     
	     	     &	     ,	     K	     R	     `	  #   i	     �	     �	     �	     �	     �	     �	     �	     
     
     *
     A
     S
     n
  *   �
     �
     �
     �
     �
     �
     �
  #        2     @     [     s  (   |  =   �     �     �               %     !                 %   +   ,      "   #                               &                  	                           )                             *      '      $      (                                
    %1 is the name of a session%1 (Wayland) ... @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) @title:windowSelect Image Advanced Author Auto &Login Background: Clear Image Commands Could not decompress archive Cursor Theme: Customize theme Default Description Download New SDDM Themes EMAIL OF TRANSLATORSYour emails General Get New Theme Halt Command: Install From File Install a theme. Invalid theme package Load from file... Login screen using the SDDM Maximum UID: Minimum UID: NAME OF TRANSLATORSYour names Name No preview available Reboot Command: Relogin after quit Remove Theme SDDM KDE Config SDDM theme installer Session: The default cursor theme in SDDM The theme to install, must be an existing archive file. Theme Unable to install theme Uninstall a theme. User User: Project-Id-Version: kcm_sddm
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2017-10-22 12:10+0100
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 %1 (Wayland) ... (Tamaños disponibles: %1) Seleccionar imagen Avanzado Autor &Inicio de sesión automático Fondo: Borrar imagen Órdenes No se puede descomprimir el archivo Tema de cursores: Personalizar tema Por omisión Descripción Descargar nuevos temas SDDM ecuadra@eloihr.net General Obtener nuevo tema Orden de detención: Instalar desde archivo Instalar un tema. Paquete de tema no válido Cargar del archivo... Pantalla de inicio de sesión que usa SDDM UID máximo: UID mínimo: Eloy Cuadra Nombre No hay vista previa disponible Orden de reinicio: Volver a iniciar sesión tras salir Eliminar tema Configuración de SDDM KDE Instalador de tema SDDM Sesión: El tema de cursores por omisión en SDDM El tema a instalar. Debe ser un archivo comprimido existente. Tema No se puede desinstalar el tema Desinstalar un tema. Usuario Usuario: 