��          �   %   �      p     q     ~     �     �     �     �     �     �  )   �               !     4     I     ]     m  3   u     �     �  <   �  5     8   T  8   �     �     �     �     �     �     �                :     Y     q      �     �  W   �     	       %   &  5   L  3   �     �     �  ;   �  )   	  ,   E	  f   r	  C   �	  H   
  H   f
     �
     �
     �
     �
                        
                                                	                                                   Add files... Add folder... Background frame Change picture every Choose a folder Choose files Configure plasmoid... General Left click image opens in external viewer Pad Paths Pause on mouseover Preserve aspect crop Preserve aspect fit Randomize items Stretch The image is duplicated horizontally and vertically The image is not transformed The image is scaled to fit The image is scaled uniformly to fill, cropping if necessary The image is scaled uniformly to fit without cropping The image is stretched horizontally and tiled vertically The image is stretched vertically and tiled horizontally Tile Tile horizontally Tile vertically s Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-16 03:31+0100
PO-Revision-Date: ٢٠١٦-٠٨-٣١ ١١:١٣+0300
Last-Translator: Safa Alfulaij <safa1996alfulaij@gmail.com>
Language-Team: Arabic <doc@arabeyes.org>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
X-Generator: Lokalize 2.0
 أضف ملفّات... أضف مجلّدًا... إطار الخلفيّة غيّر الصّورة كلّ اختر مجلّدًا اختر ملفّات اضبط البلازمويد... عامّ النّقر الأيسر على الصّورة يفتحها في عارض خارجيّ كما هي المسارات ألبث عند وضع المؤشّر أبقِ اقتصاص النّسبة الباعيّة أبقِ تناسب النّسبة الباعيّة العناصر عشوائيّة مدّد الصّورة تُكرّر أفقيًّا ورأسيًّا لا تعديلات على الصّورة تُحجَّم الصّورة لتتناسب تُحجّم الصّورة بشكل موحّد لتتناسب، وتُقتصّ إن لزم الأمر تُحجّم الصّورة بشكل موحّد دون اقتصاص الصّورة تُمدّد أفقيًّا وتُكرّر رأسيًّا الصّورة تُمدّد رأسيًّا وتُكرّر أفقيًّا كرّر كرّر أفقيًّا كرّر رأسيًّا ثا 