��          4      L       `   $   a   /   �     �   A   �  :                       Finds Konsole sessions matching :q:. Lists all the Konsole sessions in your account. Project-Id-Version: plasma_runner_konsolesessions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2009-07-11 17:55+0400
Last-Translator: Zayed Al-Saidi <zayed.alsaidi@gmail.com>
Language-Team: Arabic <kde-i18n-doc@kde.org>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
X-Generator: Lokalize 0.3
 تعثر على جلسات كونسول المطابقة لـ:q:. تسرد جميع جلسات كونسول في حسابك. 