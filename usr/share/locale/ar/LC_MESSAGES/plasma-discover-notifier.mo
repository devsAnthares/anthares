��    
      l      �       �   P   �   )   B  %   l  @   �     �  V   �     @     [     m  �       t  �   |  �   3  �   �     �  +  �  (   �          +            	               
                  %1 is '%1 packages to update' and %2 is 'of which %1 is security updates'%1, %2 1 package to update %1 packages to update 1 security update %1 security updates First part of '%1, %2'1 package to update %1 packages to update No packages to update Second part of '%1, %2'of which 1 is security update of which %1 are security updates Security updates available System up to date Updates available Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-15 03:15+0100
PO-Revision-Date: 2017-02-14 20:33+0300
Last-Translator: Safa Alfulaij <safa1996alfulaij@gmail.com>
Language-Team: Arabic <doc@arabeyes.org>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
X-Generator: Lokalize 2.0
 %1، %2 لا حزم لتحديثها حزمة واحدة لتحديثها حزمتان لتحديثهما %1 حزم لتحديثها %1 حزمة لتحديثها %1 حزمة لتحديثها لا تحديثت أمنيّة تحديث أمنيّ واحد تحديثان أمنيّان %1 تحديثات أمنيّة %1 تحديثًا أمنيًّا %1 تحديث أمنيّ لا حزم لتحديثها حزمة واحدة لتحديثها حزمتان لتحديثهما %1 حزم لتحديثها %1 حزمة لتحديثها %1 حزمة لتحديثها لا حزم لتحديثها حيث ليس بينها تحديثات أمنيّة حيث واحدة منها تحديث أمنيّ حيث اثنتين منها تحديثات أمنيّة حيث %1 منها تحديثات أمنيّة حيث %1 منها تحديثات أمنيّة حيث %1 منها تحديثات أمنيّة تتوفّر تحديثات أمنيّة النّظام محدّث تتوفّر تحديثات 