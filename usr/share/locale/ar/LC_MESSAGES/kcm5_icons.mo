��    9      �  O   �      �     �     �     �     
          #  �   >  -   �  )   �  -   "  +   P  r   |  	   �  	   �               #     ,  
   9     D     P     X     `      w     �     �     �      �     �     �  r   �  5   r     �     �     �  	   �     �     �     �  (   �     '	     5	     N	     h	     �	     �	  +   �	  3   �	     �	     �	     
     
  S    
  )   t
     �
  �   �
    �     �     �     �     �     �        �     9   �     
       
     �   %     �     �     �            
        "  
   <     G     [  .   u  '   �     �     �     �  8   �     7  3   J  �   ~  =     &   J  4   q  
   �     �     �     �     �  2   �     &  3   A  5   u  5   �     �     �  >     E   F     �      �     �     �  y   �  E   b     �    �               1                &              0   '                7                            
          -               4      5   #      (       6   3          )                 /   	             9         +                    *      $      !   ,   .   "       8           %      2    &Amount: &Effect: &Second color: &Semi-transparent &Theme (c) 2000-2003 Geert Jansen <qt>Are you sure you want to remove the <strong>%1</strong> icon theme?<br /><br />This will delete the files installed by this theme.</qt> <qt>Installing <strong>%1</strong> theme</qt> @label The icon rendered as activeActive @label The icon rendered as disabledDisabled @label The icon rendered by defaultDefault A problem occurred during the installation process; however, most of the themes in the archive have been installed Ad&vanced All Icons Antonio Larrosa Jimenez Co&lor: Colorize Confirmation Desaturate Description Desktop Dialogs Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Effect Parameters Gamma Geert Jansen Get new themes from the Internet Icons Icons Control Panel Module If you already have a theme archive locally, this button will unpack it and make it available for KDE applications Install a theme archive file you already have locally Main Toolbar NAME OF TRANSLATORSYour names Name No Effect Panel Preview Remove Theme Remove the selected theme from your disk Set Effect... Setup Active Icon Effect Setup Default Icon Effect Setup Disabled Icon Effect Size: Small Icons The file is not a valid icon theme archive. This will remove the selected theme from your disk. To Gray To Monochrome Toolbar Torsten Rahn Unable to download the icon theme archive;
please check that address %1 is correct. Unable to find the icon theme archive %1. Use of Icon You need to be connected to the Internet to use this action. A dialog will display a list of themes from the http://www.kde.org website. Clicking the Install button associated with a theme will install this theme locally. Project-Id-Version: kcmicons
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-08 06:27+0100
PO-Revision-Date: 2010-07-18 17:29+0400
Last-Translator: Zayed Al-Saidi <zayed.alsaidi@gmail.com>
Language-Team: Arabic <linuxac-kde-arabic-team@googlegroups.com>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
X-Generator: Translate Toolkit 1.10.0
 ال&كمية: ال&تأثير: اللون ال&ثاني: &شبه شفاف ال&سِمة (c) 2000-2003 Geert Jansen <qt>هل أنت متأكد من حذف <strong>%1</strong>سمة الأيقونة؟<br /><br />هذا سيقوم بحذف الملفات المثبتة مع هذه السمة.</qt> <qt>جاري تثبيت <strong>سِمة</strong> %1</qt> نشط معطل مبدئي هناك مشكلة حدثت أثناء عملية التثبيت; على أية حال, معظم السِمات في الأرشيف قد تم تثبيتها مت&قدم كلّ الأيقونات Antonio Larrosa Jimenez اللو&ن: لوّن تأكيد إلغاء الإشباع الوصف سطح المكتب مربعات الحوار اسحب أو اكتب عنوان السِمة metehyi@free.fr,zayed.alsaidi@gmail.com موسطات التأثير جاما Geert Jansen استيراد مخطط اللون من الإنترنت الأيقونات وحدة لوحة التحكم بالأيقونات إذا كنت تملك سمة مضغوطة، هذا الزر سيفك الضغط عنها ويجعلها متوفرة لتطبيقات كدي ثبت سمة مضغوطة تملكها مسبقا محلبا شريط الأدوات الرئيسي Mohamed SAAD,زايد السعيدي محمد سعد الاسم لا تأثير اللوحة المعاينة أزل السِمة احذف السمة المحددة من القرص عيّن التأثير... إعداد تأثير الأيقونة النشطة إعداد تأثير الأيقونة المبدئي إعداد تأثير الأيقونة الخاملة الحجم: أيقونات صغيرة الملف ليس أرشيف سِمة أيقونات صالح. هذا سيقوم بحذف السمة المحددة من القرص. إلى الرمادي إلى أحاديِّ اللون شريط الأدوات Torsten Rahn لم استطع تنزيل أرشيف سِمة الأيقونات;
من فضلك تأكد من صحة العنوان %1 . لم استطع إيجاد أرشيف سِمة الأيقونات %1. استخدام الأيقونة يجب أن تتصل بالإنترنت للقيام بهذه الخطوة. ستظهر قائمة للسمات من الموقع http://www.kde.org. يالضغط على .زر التثبيت المقابل لإحدى السمات سيقوم بثبيت السمية محليا 