��    <      �  S   �      (     )  !   E  "   g  &   �  ,   �  #   �  &     !   )  &   K  '   r  "   �  #   �  "   �  '        ,     ?  +   C  
   o     z     �     �     �     �     �  U   �     *     =     V     ]      b     �     �     �     �  !   �     �  	   �     �     	     '	     B	  &   U	     |	     �	     �	     �	     �	     �	     �	  $   �	     

     
     5
     G
     _
     u
     �
  &   �
     �
     �
  1   �                     .  
   K     V     p     y     �     �  
   �     �     �  &   �     �  X   �     W     l     �  K   �  
   �      �       x        �  *   �     �     �  9   �  +   0     \     v  -     5   �     �     �       5     &   M  !   t  5   �  3   �        .        6     C     P     n  F     (   �  (   �       "   7     Z  (   v     �  `   �  ,            "          0   %                  	   :          4       ;   /   1          *                                 &   9              3   ,             .          )               2   (       8             
          #             +   7   6               $      !      '       <          5      -       &Matching window property:  @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Border @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large @item:inlistbox Button size:Large @item:inlistbox Button size:Normal @item:inlistbox Button size:Small @item:inlistbox Button size:Very Large Active Window Glow Add Add handle to resize windows with no border Animations B&utton size: Border size: Button mouseover transition Center Center (Full Width) Class:  Configure fading between window shadow and glow when window's active state is changed Decoration Options Detect Window Properties Dialog Edit Edit Exception - Oxygen Settings Enable/disable this exception Exception Type General Hide window title bar Information about Selected Window Left Move Down Move Up New Exception - Oxygen Settings Question - Oxygen Settings Regular Expression Regular Expression syntax is incorrect Regular expression &to match:  Remove Remove selected exception? Right Shadows Tit&le alignment: Title:  Use window class (whole application) Use window title Warning - Oxygen Settings Window Class Name Window Drop-Down Shadow Window Identification Window Property Selection Window Title Window active state change transitions Window-Specific Overrides Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-13 05:20+0100
PO-Revision-Date: ٢٠١٥-٠٤-٠٣ ١٩:٣٦+0300
Last-Translator: Safa Alfulaij <safa1996alfulaij@gmail.com>
Language-Team: Arabic <doc@arabeyes.org>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
 خاصيّة النّافذة ال&مطابقة:  ضخم كبير لا حدود لا حدود جانبيّة عاديّ فوق الطّبيعيّ ضئيل ضخم جدًّا كبير جدًّا كبير عاديّ صغير كبير جدًّا توهح النافذة المفعلة أضف أضف مقبضًا لتغيير حجم النّافذة الّتي لا حدود لها التّحريكات حجم الزّ&رّ: حجم الإطار: التأثير الانتقالي لتمرير الفأرة فوق الزر الوسط الوسط (كامل العرض) الصّنف:  ضبط التلاشي بين ظل النافذة والتوهج عندما تتغير حالة تنشيط النافذة خيارات الزّخرفة اكتشف خاصيّات النّافذة حواريّ حرّر حرّر الإستثناء - إعدادات أكسجين مكّن/عطّل هذا الاستثناء نوع الاستثناء عامّ أخفِ شريط عنوان النّافذة معلومات عن النّافذة المحدّدة اليسار انقل لأسفل انقل ﻷعلى استنثاء جديد - إعدادات أكسجين سؤال - إعدادات أكسجين التّعبير النّمطيّ صياغة التّعبير النمطيّ خاطئة التّعبير النّمطي لم&طابقته:  أزل أأزيل الاستثناء المحدّد؟ اليمين الظلال محاذاة الع&نوان: العنوان:  استخدم صنف النّافذة (التّطبيق بالكامل) استخدم عنوان النّافذة تحذير - إعدادات أكسجين اسم صنف النّافذة ظل النافذة المنسدل تعريف النّافذة تحديد خاصيّة النّافذة عنوان النّافذة التأثيرات الانتقالية عندما تتغير حالة تنشيط النافذة تجاوزات خاصّة بالنّافذة 