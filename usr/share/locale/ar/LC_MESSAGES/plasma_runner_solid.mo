��          �      �       H  m   I     �  "   �  F   �  F   .  F   u  J   �  N     R   V     �  %   �  �   �     h  �  {     p     �  :   �  m   �  b   G  Z   �  p     i   v  �   �     b	     v	     �	     �	                                         	         
               Close the encrypted container; partitions inside will disappear as they had been unpluggedLock the container Eject medium Finds devices whose name match :q: Lists all devices and allows them to be mounted, unmounted or ejected. Lists all devices which can be ejected, and allows them to be ejected. Lists all devices which can be mounted, and allows them to be mounted. Lists all devices which can be unmounted, and allows them to be unmounted. Lists all encrypted devices which can be locked, and allows them to be locked. Lists all encrypted devices which can be unlocked, and allows them to be unlocked. Mount the device Note this is a KRunner keyworddevice Unlock the encrypted container; will ask for a password; partitions inside will appear as they had been plugged inUnlock the container Unmount the device Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2014-07-04 11:07+0300
Last-Translator: Safa Alfulaij <safa1996alfulaij@gmail.com>
Language-Team: Arabic <doc@arabeyes.org>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
X-Generator: Lokalize 1.5
 اقفل الحاوي أخرِج الوسيط يعثر الأجهزة التي يطابق اسمها :q: يسرد كلّ الأجهزة المسموح لها بالضمّ، وإزالة الضمّ والإخراج. يسرد كلّ الأجهزة التي يمكن إخراجها، والسماح بإخراجها. يسرد كلّ الأجهزة التي يمكن ضمّها، والسماح بضمّها. يسرد كلّ الأجهزة التي يمكن إزالة ضمّها، والسماح بإزالة ضمّها. يسرد كلّ الأجهزة المشفّرة التي يمكن قفلها، والسماح قفلها. يسرد كلّ الأجهزة المشفّرة التي يمكن إزالة قفلها، والسماح بإزالة قفلها. ضمّ الجهاز الجهاز أزل قفل الحاوي أزل ضمّ الجهاز 