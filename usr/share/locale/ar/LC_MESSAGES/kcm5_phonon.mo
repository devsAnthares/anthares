��    A      $  Y   ,      �  i   �  m   �     i  b   �     �     �  6        ?  7   O     �     �  	   �     �  (   �  )   �  )        B     _  Y   e     �     �  �   �      i	     �	  
   �	     �	     �	     �	     �	     �	     �	     
     
     $
     3
     8
     W
     s
     �
     �
     �
  	   �
  
   �
     �
     �
  	   �
  
   �
  
   �
     �
       	   !     +     0  
   I  �   T     �  8   �  �   2     �  8   �  ,     ,   0  %   ]     �    �  >   �  �   �  *   �  �   �     Y     v  .   �     �  C   �          1     ?  6   H  3     3   �  7   �  %        E  q   N     �     �  �   �  V   �     �     �          2     J     i     r     �     �  -   �     �     �  8   �     /     N     ^     g          �     �     �  (   �               .     D  &   b     �  
   �  $   �     �  �   �     }  O   �  �   �     �  D   �  =   �  =   3  :   q  $   �     #       $          :   +          A                     "                 6   8       *              (   >   ,   ;         5                                   )   1       =         7       0       ?       -   4   2   9   @         %      .                3              /      !             <   '   &         	   
           @info User changed Phonon backendTo apply the backend change you will have to log out and back in again. A list of Phonon Backends found on your system.  The order here determines the order Phonon will use them in. Apply Device List To... Apply the currently shown device preference list to the following other audio playback categories: Audio Hardware Setup Audio Playback Audio Playback Device Preference for the '%1' Category Audio Recording Audio Recording Device Preference for the '%1' Category Backend Colin Guthrie Connector Copyright 2006 Matthias Kretz Default Audio Playback Device Preference Default Audio Recording Device Preference Default Video Recording Device Preference Default/Unspecified Category Defer Defines the default ordering of devices which can be overridden by individual categories. Device Configuration Device Preference Devices found on your system, suitable for the selected category.  Choose the device that you wish to be used by the applications. EMAIL OF TRANSLATORSYour emails Front Center Front Left Front Left of Center Front Right Front Right of Center Hardware Independent Devices Input Levels Invalid KDE Audio Hardware Setup Matthias Kretz Mono NAME OF TRANSLATORSYour names Phonon Configuration Module Playback (%1) Prefer Profile Rear Center Rear Left Rear Right Recording (%1) Show advanced devices Side Left Side Right Sound Card Sound Device Speaker Placement and Testing Subwoofer Test Test the selected device Testing %1 The order determines the preference of the devices. If for some reason the first device cannot be used Phonon will try to use the second, and so on. Unknown Channel Use the currently shown device list for more categories. Various categories of media use cases.  For each category, you may choose what device you prefer to be used by the Phonon applications. Video Recording Video Recording Device Preference for the '%1' Category  Your backend may not support audio recording Your backend may not support video recording no preference for the selected device prefer the selected device Project-Id-Version: kcm_phonon
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2012-02-09 14:38+0200
Last-Translator: Abdalrahim Fakhouri <abdilra7eem@yahoo.com>
Language-Team: Arabic <doc@arabeyes.org>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
X-Generator: Translate Toolkit 1.10.0
 لتطبيق التغيرات اخرج ثم لُج مجددا. قائمة الواجهات الخلفية لفونون الموجودة في نظامك. يحدد الترتيب المحدّد الترتيب الذي سيستعمله فونون. طبق قائمة الأجهزة على... طبق قائمة تفضيلات الأجهزة المعروضة حالياً على فئات أجهزة إخراج الصوت التالية: تنصيب جهاز صوتي قراءة الصوت تفضيل قارئ الصوت للفئة '%1' تسجيل صوتي تفضيلات جهاز التسجيل الصوتي للفئة '%1' الواجهة الخلفية Colin Guthrie موصل حقوق النسخ محفوظة 2006 - Matthias Kretz التفضيل المبدئي لقارئ الصوت التفضيل المبدئي لمسجل الصوت التفضيل المبدئي لمسجل الفيديو فئة مبدئية/غير معينة أجِل يحدد الترتيب المبدئي للأجهزة، ويمكن تغييرها في كل فئة على حدى. تهيئة الجهاز تفضيلات الجهاز عُثر على مخارج صوتية في نظامك ملائمة للفئة المختارة. اختر الجهاز الذي تريد استخدامه. chahibi@gmail.com,zayed.alsaidi@gmail.com,frostbutterfly@gmail.com,a.kitouni@gmail.com مقدمة الوسط يسار المقدمة يسار مقدمة الوسط يمين المقدمة يمين مقدمة الوسط جهاز جهاز مستقل مستوى الإدخال غير صالح منصب كدي للأجهزة الصوتية Matthias Kretz مونو Youssef Chahibi,Zayed,Hajur Alaseefer,Abderrahim Kitouni وحدة إعداد فونون العزف (%1) فضّل الملف الشخصي وسط المؤخرة يسار المؤخرة يمين المؤخرة التسجيل (%1) اعرض الأجهزة المتقدمة يسار الجانب يمين الجانب بطاقة الصوت الأجهزة الصوتية مموضع ومختبر السبيكر مضخم الصوت اختبر اختبر الجهاز المحدد يختبر %1 يحدد ترتيب الأجهزة أفضليتها. إن لم يعمل الجهاز الأول فسيستخدم فونون الجهاز التالي وهكذا. قناة غير معلومة طبق قائمة الأجهزة المعروضة على فئات إضافية. الفئات المتباينة لحالات استخدام الوسائط. يمكنك اختيارجهاز مفضل من كل فئة لتستعمله تطبيقات فونون. تسجيل مرئي تفضيلات جهاز التسجيل المرئي للفئة '%1'  قد لا تدعم الخلفية التسجيل الصوتي قد لا تدعم الخلفية التسجيل المرئي لا يوجد تفضيلات للجهاز المُنتقى فضّل الجهاز المحدّد 