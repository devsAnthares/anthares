��    c      4  �   L      p     q     �     �     �     �  5   �  #   �  E   		  E   O	  >   �	     �	     �	  7   �	     5
     O
     `
     
     �
     �
     �
     �
     �
     �
     �
          $     2     G     Y     ^  	   e     o     �     �  !   �  F   �          0     A  <   S     �     �  *   �     �      �     �       	             &  	   .     8     H     O      X     y  
   �     �     �     �     �  
   �  F   �  :   ;     v     ~     �     �     �     �  8   �     �     �  	     
             -     6     G     M     e     m     �     �     �     �     �  &   �     �  
        $     4     T     \     e     v     �  $   �  �  �      �     �     �     �     �  ?     5   T  T   �  T   �  M   4     �     �  F   �  %   �       "   -  (   P     y     �     �     �     �     �     �  !   �       #   :  #   ^     �     �     �  "   �  &   �  $   �  0     O   F  2   �     �     �  J   �  
   =     H  <   U     �  )   �     �     �  $   �          2     ;     J     Z     c  0   s  2   �     �  7   �          9     T     `  t   t  S   �  
   =     H     Z     a     u     ~  ]   �     �     �          $     5     K     Z     q  $   }     �  ,   �     �     �  .        6     E     Y  /   _     �     �     �     �     �     �  "        )  7   E         J   _   :       	   V                 Z   .   E         )      (   Q   M   '   O   $       7   X   4                -                    9       8   #   R       0   c       B      
   +                         L   >              N   @      *   D      F   /   ;       5       2   ]      T               ?   6       <   U   ,       b       [   Y      G   1                          C   P   &       A          ^   a       =   S      !   3             `   K           \              H      %      I   W          "    
Also available in %1 %1 %1 (%2) %1 (Default) <b>%1</b> by %2 <em>%1 out of %2 people found this review useful</em> <em>Tell us about this review!</em> <em>Useful? <a href='true'><b>Yes</b></a>/<a href='false'>No</a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'><b>No</b></a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'>No</a></em> @infoFetching updates @infoFetching... @infoIt is unknown when the last check for updates was @infoLooking for updates @infoNo updates @infoNo updates are available @infoShould check for updates @infoThe system is up to date @infoUpdates @infoUpdating... Accept Add Source... Addons Aleix Pol Gonzalez An application explorer Apply Changes Available backends:
 Available modes:
 Back Cancel Category: Checking for updates... Comment too long Comment too short Compact Mode (auto/compact/full). Could not close the application, there are tasks that need to be done. Could not find category '%1' Couldn't open %1 Delete the origin Directly open the specified application by its package name. Discard Discover Display a list of entries with a category. Extensions... Failed to remove the source '%1' Featured Help... Homepage: Improve summary Install Installed Jonathan Thomas Launch License: List all the available backends. List all the available modes. Loading... Local package file to install Make default More Information... More... No Updates Open Discover in a said mode. Modes correspond to the toolbar buttons. Open with a program that can deal with the given mimetype. Proceed Rating: Remove Resources for '%1' Review Reviewing '%1' Running as <em>root</em> is discouraged and unnecessary. Search Search in '%1'... Search... Search: %1 Search: %1 + %2 Settings Short summary... Size: Sorry, nothing found... Source: Specify the new source for %1 Still looking... Summary: Supports appstream: url scheme Tasks Tasks (%1%) TransactioName TransactionStatus%1 %2 Unable to find resource: %1 Update All Update Selected Update section nameUpdate (%1) Updates Version: unknown reviewer updates not selected updates selected © 2010-2016 Plasma Development Team Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:02+0100
PO-Revision-Date: 2018-01-21 09:19+0300
Last-Translator: Safa Alfulaij <safa1996alfulaij@gmail.com>
Language-Team: Arabic <doc@arabeyes.org>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
X-Generator: Lokalize 2.0
 
متوفّر أيضًا في %1 %1 %1 (%2) ‏%1 (المبدئيّ) ‏<b>%1</b> كتبه %2 <em>وجد %1 من %2 هذا التّقييم مفيدًا</em> <em>أخبرنا حول هذا التّقييم!</em> <em>أهو مفيد؟ <a href='true'><b>نعم</b></a>/<a href='false'>لا</a></em> <em>أهو مفيد؟ <a href='true'>نعم</a>/<a href='false'><b>لا</b></a></em> <em>أهو مفيد؟ <a href='true'>نعم</a>/<a href='false'>لا</a></em> يجلب التّحديثات يجلب... لا يُعرف متى كان آخر التماس للتّحديثات يبحث عن التّحديثات... لا تحديثات لا تحديثات متوفّرة يجب التماس التّحديثات النّظام محدّث التّحديثات يحدّث... اقبل أضف مصدرًا... الإضافات Aleix Pol Gonzalez متصفّح للتّطبيقات طبّق التّغييرات الأسناد المتوفّرة:
 الأوضاع المتوفّرة:
 السّابق ألغِ الفئة: يلتمس التّحديثات... التّعليق طويل جديًّا التّعليق قصير جدًّا الوضع المتضامّ (auto/compact/full). تعذّر إغلاق التّطبيق، هناك مهام يجب أداءها. تعذّر العثور على الفئة ’%1‘ تعذّر فتح %1 احذف الأصل افتح التّطبيق المحدّد مباشرة باسم حزمته. تجاهل استكشف اعرض قائمة من المدخلات حسب الفئة. الامتدادات... فشلت إزالة المصدر ’%1‘ مُختارة مساعدة... الصّفحة الرّئيسيّة: حسّن التّعليق ثبّت المثبّت Jonathan Thomas أطلق الرّخصة: اسرد كل الأسناد المتوفّرة. اسرد كلّ الأوضاع المتوفّرة. يحمّل... ملفّ الحزمة المحليّة لتثبيتها اجعله المبدئيّ معلومات أخرى... أكثر... لا تحديثات افتح «استكشف» بالوضع المُعطى. تقابل الأوضاع أزرار شريط الأدوات. افتح ببرنامج يمكن أن يتعامل مع نوع MIME المُعطى. تابِع التّقييم: أزل موارد ’%1‘ قيّم تقييم ’%1‘ التّشغيل بمستخدم <em>الجذر</em> غير مستحسن ولا حاجة له. ابحث ابحث في ’%1‘... ابحث عن... ابحث عن %1 ابحث عن %1 + %2 إعدادات ملخّص قصير... الحجم: نأسف، لم نجد شيئًا... المصدر: حدّد مصدرًا جديدًا لِـ %1 ما زلنا نبحث... الملخّص: يدعم مخطّط مسارات appstream:‎ المهامّ المهام (%1٪) %1 %2 تعذّر العثور على المورد: %1 حدّث الكلّ حدّث المحدّد حدّث (%1) التّحديثات الإصدارة: مقيّم مجهول تحديثات غير محدّدة تحديثات محدّدة © ٢٠١٠-٢٠١٦ لفريق تطوير بلازما 