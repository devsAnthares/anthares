��          �   %   �      @     A     J     _     h  0   v     �     �     �     �     �     �        
        "     7     D     _     p     �     �     �     �     �     �  �  �     �      �     �       R        p  %   �     �     �  
   �  *   �  &        .     @     _  (   m     �     �     �     �  )   �  
         (     I                                                       	                                                            
           %1 by %2 Add Custom Wallpaper Centered Change every: Directory with the wallpaper to show slides from Download Wallpapers Get New Wallpapers... Hours Image Files Minutes Next Wallpaper Image Open Containing Folder Open Image Open Wallpaper Image Positioning: Recommended wallpaper file Remove wallpaper Restore wallpaper Scaled Scaled and Cropped Scaled, Keep Proportions Seconds Select Background Color Tiled Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:39+0100
PO-Revision-Date: 2017-12-01 08:06+0300
Last-Translator: Safa Alfulaij <safa1996alfulaij@gmail.com>
Language-Team: Arabic <doc@arabeyes.org>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
X-Generator: Lokalize 2.0
 %1 لِ‍ %2 أضف خلفيّة مخصّصة موسّطة غيّر كلّ: الدّليل الذي فيه الخلفيّات لعرض الشّرائح منه نزّل خلفيّات اجلب خلفيّات جديدة... ساعة ملفّات الصّور دقيقة صورة الخلفيّة التّالية افتح المجلّد المحتوي افتح صورة افتح صورة خلفيّة الموضع: ملف الخلفيّة المستحسن أزل الخلفيّة استعد الخلفيّة مكبّرة مكبّرة ومقصوصة مكبّرة مع إبقاء النّسب ثانية اختر لون الخلفيّة مكرّرة 