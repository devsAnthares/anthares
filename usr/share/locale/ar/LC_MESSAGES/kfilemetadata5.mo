��    8      �  O   �      �     �     �     �     
          +     9     H     X     g     x     �     �     �     �     �     �     �     �     
          0     A     U     h     {     �     �     �     �     �     �     �           0     P     p  #   �  $   �     �     �          1     O  3   m     �     �  &   �  <   �  !   7	  8   Y	  0   �	  !   �	      �	  +   
  �  2
     '  
   A     L     [     {     �     �     �  
   �     �     �  
   �     �          #     4  #   =  
   a     l     �     �     �     �     �     �                 
   ,  
   7  "   B     e     }      �  "   �      �  $   �  +   #  5   O     �     �  &   �  &   �  &        ,     D     S     `  #   x     �  .   �     �     �               8           4   5                   2   6   !         +               #                                	                               1   3   -            *             $   %      )   "          .       &                      (                
   /   0           7   ,       '        @labelAlbum Artist @labelArchive @labelArtist @labelAspect Ratio @labelAudio @labelAuthor @labelBitrate @labelChannels @labelComment @labelCopyright @labelCreation Date @labelDocument @labelDuration @labelFrame Rate @labelHeight @labelImage @labelKeywords @labelLanguage @labelLyricist @labelPage Count @labelPresentation @labelPublisher @labelRelease Year @labelSample Rate @labelSpreadsheet @labelSubject @labelText @labelTitle @labelVideo @labelWidth @label EXIFImage Date Time @label EXIFImage Model @label EXIFImage Orientation @label EXIFPhoto Aperture Value @label EXIFPhoto Exposure Bias @label EXIFPhoto Exposure Time @label EXIFPhoto Focal Length @label EXIFPhoto Focal Length 35mm @label EXIFPhoto Original Date Time @label EXIFPhoto Saturation @label EXIFPhoto Sharpness @label EXIFPhoto White Balance @label EXIFPhoto X Dimension @label EXIFPhoto Y Dimension @label date of template creation8Template Creation @label music albumAlbum @label music genreGenre @label music track numberTrack Number @label number of fuzzy translated stringsDraft Translations @label number of linesLine Count @label number of translatable stringsTranslatable Units @label number of translated stringsTranslations @label number of wordsWord Count @label translation authorAuthor @label translations last updateLast Update Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:36+0200
PO-Revision-Date: 2015-01-10 03:30+0300
Last-Translator: Safa Alfulaij <safa1996alfulaij@gmail.com>
Language-Team: Arabic <doc@arabeyes.org>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
X-Generator: Lokalize 1.5
 فنّان الألبوم أرشيف الفنّان النّسبة الباعيّة صوت المؤلّف معدّل بِتّ القنوات تعليق حقوق النّشر تاريخ الإنشاء مستند المدّة معدّل الإطارات الارتفاع صورة الكلمات المفتاحيّة اللغة كاتب الأغنية عدد الصّفحات عرض تقديميّ النّاشر سنة الإطلاق معدّل الإعتيان جدول ممتدّ الموضوع نصّ العنوان فيديو العرض تاريخ ووقت الصّورة طراز الصّورة اتّجاه الصّورة قيمة فتحة الصّورة تحيّز تعرض الصّورة زمن تعرّض الصّورة طول الصّورة البؤريّ طول الصّورة البؤريّ 35مم تاريخ ووقت الصّورة الأصليّين تشبّع الصّورة حدّة الصّورة توازن الصّورة الأبيض بعد الصّورة السّينيّ بعد الصّورة الصّاديّ إنشاء القالب الألبوم النّوع رقم المقطوعة التّرجمات المسودّة عدد الأسطر الوحدات القابلة للتّرجمة التّرجمات عدد الكلمات المؤلّف آخر تحديث 