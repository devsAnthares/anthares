��    &      L  5   |      P     Q     W     l  
   �  +   �  
   #     .     :     S     a     s  	   |      �  e   �  *     H   8     �     �     �     �     �  )   �  ;     	   A  (   K     t     �     �     �     �     �       	   (     2  #   L     p     �    �     �	     �	  �   �	     �
  O   �
     �
       '   +      S     t     �     �     �  �   �  <   k  }   �  $   &     K     \     t     �  G   �  h   �     G  G   \  ,   �  ,   �  5   �  5   4  &   j  2   �  2   �     �  0     ?   9  '   y  6   �           
                 !                     $                                             "            %      	                                       &         #                   msec &Number of desktops: <h1>Multiple Desktops</h1>In this module, you can configure how many virtual desktops you want and how these should be labeled. Animation: Assigned global Shortcut "%1" to Desktop %2 Desktop %1 Desktop %1: Desktop Effect Animation Desktop Names Desktop Switching Desktops Duration: EMAIL OF TRANSLATORSYour emails Enabling this option will show a small preview of the desktop layout indicating the selected desktop. Here you can enter the name for desktop %1 Here you can set how many virtual desktops you want on your KDE desktop. KWin development team Layout N&umber of rows: NAME OF TRANSLATORSYour names No Animation No suitable Shortcut for Desktop %1 found Shortcut conflict: Could not set Shortcut %1 for Desktop %2 Shortcuts Show shortcuts for all possible desktops Switch One Desktop Down Switch One Desktop Up Switch One Desktop to the Left Switch One Desktop to the Right Switch to Desktop %1 Switch to Next Desktop Switch to Previous Desktop Switching Walk Through Desktop List Walk Through Desktop List (Reverse) Walk Through Desktops Walk Through Desktops (Reverse) Project-Id-Version: kcm_kwindesktop
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-06 05:35+0100
PO-Revision-Date: ٢٠١٥-٠٨-١٨ ١٢:٤٧+0300
Last-Translator: Safa Alfulaij <safa1996alfulaij@gmail.com>
Language-Team: Arabic <doc@arabeyes.org>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
X-Generator: Lokalize 2.0
  م‌ث &عدد أسطح المكتب: <h1>أسطح المكتب المتعددة</h1>يمكنك في هذه الوحدة ضبط عدد أسطح المكتب الوهميّة التي تريدها، إلى جانب تسميتها. الحركة: أُسند الاختصار العموميّ "%1" إلى سطح المكتب %2 سطح المكتب %1 سطح المكتب %1: حركة تأثير سطح المكتب أسماء أسطح المكتب تبديل سطح المكتب أسطح المكتب المدّة: safa1996alfulaij@gmail.com تفعيل هذا الخيار سيظهر معاينة صغيرة لتخطيط سطح المكتب مشيرة إلى سطح المكتب المحدّد. هنا يمكنك كتابة اسم لسطح المكتب %1 يمكنك هنا تعيين عدد أسطح المكتب الوهميّة التي تريدها في سطح مكتب كدي. فريق تطوير «نافذتك» التّخطيط عدد ال&صّفوف: صفا الفليج لا حركة لم يُعثر على اختصار مناسب لسطح المكتب %1 تضارب في الاختصارات: تعذّر تعيين الاختصار %1 لسطح المكتب %2 الاختصارات أظهر اختصارات لكلّ أسطح المكتب الممكنة بدّل سطح مكتب واحد لأسفل بدّل سطح مكتب واحد لأعلى بدّل سطح مكتب واحد إلى اليسار بدّل سطح مكتب واحد إلى اليمين بدّل إلى سطح المكتب %1 بدّل إلى سطح المكتب التّالي بدّل إلى سطح المكتب السّابق التّبديل تنقّل في قائمة أسطح المكتب تنقّل في قائمة أسطح المكتب (بالعكس) تنقّل بين أسطح المكتب تنقّل بين أسطح المكتب (بالعكس) 