��          �   %   �      @     A     G     T     c     y     �     �     �      �     �  
   �     �          0  A   I     �     �     �     �     �  �   �  ;   p  %   �  b   �     5     6     B  #   W  %   {     �     �     �     �     �  &        7     D     X     q  c   �  
   �     �     	     (	     E	  �   Q	  _   P
     �
  0  �
                                            	                                               
                                   %1 Hz &Reconfigure 90° Clockwise 90° Counterclockwise Advanced Settings Configuration for displays Display Configuration Display: EMAIL OF TRANSLATORSYour emails Laptop Screen Maintainer NAME OF TRANSLATORSYour names No Primary Output No available resolutions No kscreen backend found. Please check your kscreen installation. Normal Orientation: Primary display: Refresh rate: Resolution: Sorry, your configuration could not be applied.

Common reasons are that the overall screen size is too big, or you enabled more displays than supported by your GPU. Tip: Hold Ctrl while dragging a display to disable snapping Warning: There are no active outputs! Your system only supports up to %1 active screen Your system only supports up to %1 active screens Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-24 03:18+0200
PO-Revision-Date: ٢٠١٥-٠٨-١٧ ٠٩:٣٨+0300
Last-Translator: Safa Alfulaij <safa1996alfulaij@gmail.com>
Language-Team: Arabic <doc@arabeyes.org>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
X-Generator: Lokalize 2.0
 %1 هرتز أ&عد الضّبط 90° مع عقارب السّاعة 90° عكس عقارب السّاعة إعدادات متقدّمة اضبط العروض اضبط العرض العرض: safa1996alfulaij@gmail.com شاشة الحاسوب المحمول المصين صفا الفليج لا خرج أساسيّ لا ميز متوفّر لم يُعثر على سند «شاشتك». فضلًا تحقّق من تثبيت «شاشتك». عاديّ الاتّجاه: العرض الأساسيّ: معدّل التّحديث: الميز: آسفون، تعذّر تطبيق الضّبط.

من الأسباب الشّائعة كون حجم الشّاشة الكلّيّ كبيرًا جدًّا، أو أنّك فعّلت شاشات أكثر ممّا تدعمه وحدة الرّسوميّات. تلميحة: اضغط Ctrl مطوّلًا أثناء سحب العرض لتعطيل الجذب تحذير: لا خرج نشط! لا يدعم نظامك أيّ شاشة نشطة يدعم نظامك شاشة واحدة نشطة فقط يدعم نظامك شاشتين نشطتين فقط يدعم نظامك %1 شاشات نشطة فقط يدعم نظامك %1 شاشة نشطة فقط يدعم نظامك %1 شاشة نشطة فقط 