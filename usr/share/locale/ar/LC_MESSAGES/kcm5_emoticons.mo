��    #      4  /   L           	     !     .  +   5      a     �  ;   �     �     �                 >     _  9   m     �  3   �  	   �     �  )   �  %   )  |   O  [   �  5   (  *   ^     �     �     �     �     �  +   �  (        A  n   a  3   �  &    !   +
     M
  	   _
  6   i
  4   �
     �
  ]   �
     P  #   h  &   �  =   �     �     	  L     	   h  S   r     �     �  3   �  H   %  �   n  c     6   g  ?   �     �     �            $   ,  4   Q  4   �  9   �  �   �  ?   �                          #      
                                         !             	          "                                                             %1 theme already exists Add Emoticon Add... Choose the type of emoticon theme to create Could Not Install Emoticon Theme Create a new emoticon Create a new emoticon by assigning it an icon and some text Delete emoticon Design a new emoticon theme Do you want to remove %1 too? Drag or Type Emoticon Theme URL EMAIL OF TRANSLATORSYour emails Edit Emoticon Edit the selected emoticon to change its icon or its text Edit... Emoticon themes must be installed from local files. Emoticons Emoticons Manager Enter the name of the new emoticon theme: Get new icon themes from the Internet If you already have an emoticon theme archive locally, this button will unpack it and make it available for KDE applications Insert the string for the emoticon.  If you want multiple strings, separate them by spaces. Install a theme archive file you already have locally Modify the selected emoticon icon or text  NAME OF TRANSLATORSYour names New Emoticon Theme Remove Remove Theme Remove the selected emoticon Remove the selected emoticon from your disk Remove the selected theme from your disk Require spaces around emoticons Start a new theme by assigning it a name. Then use the Add button on the right to add emoticons to this theme. This will remove the selected theme from your disk. Project-Id-Version: kcm_emoticons
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-04 06:03+0100
PO-Revision-Date: 2012-02-09 18:53+0200
Last-Translator: Abdalrahim Fakhouri <abdilra7eem@yahoo.com>
Language-Team: Arabic <linuxac-kde-arabic-team@googlegroups.com>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
X-Generator: Translate Toolkit 1.10.0
 سمة %1 موجود بالفعل أضف تعبير أضف... اختر نوع سمة التعبير لإنشائها لم يستطع تثبيت سمة التعبيرات أنشئ تعبير جديد أنشئ تعبير  جديد بواسطة تخصيص أيقونه له وبعض النصوص احذف التعبير صمم سمة تعبير جديدة هل تريد إزالة %1 أيضا؟ ضع تعبير أو أكتب موقع سمة التعبير  zayed.alsaidi@gmail.com حرر تعبير حرر التعبير المختار لتغيير أيقونته أو نصه حرر... يجب أن تثبت سمات التعبيرات من الملفات المحلية تعبيرات مدير التعبيرات أدخل اسم سمة التعبير الجديد: احصل على سمات أيقونات جديدة من الإنترنت إذا كان لديك سمة تعبيرات على جهازك, هذا الزر سيقوم بفكها وسيجعلها متاح لبرامج كدي. أدرج نص للتعبير ، إذا أردت أكثر من نص افتصل بينها بفراغ ثبت سمة من ملف السمات في جهازك تعديل أيقونة أو نص التعبير المختار زايد السعيدي سمة تعبير جديد أزِل أزل السمه أزل التعبير المختار أزل التعبير المختار من القرص أزل التعبير المختار من القرص المساحة المطلوبة حول التعبيرات ابدأ سمة مع بتخصيص إسم لها. وبعد ذلك استخدم زر أضف الموجود على اليمين لإضافة تعبيرات إلى هذه السمة. هذا سيزيل التعبير المختار من القرص 