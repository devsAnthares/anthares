��    '      T  5   �      `     a     �  <   �  .   �  %        1     G     N     T     k     ~     �     �     �  4   �     �  9        B  !   T     v     �     �     �     �  B   �  &     '   .  )   V  7   �  .   �  5   �  +        I     f     �  ,   �     �  V   �  �  &  +   
  +   G
  L   s
  5   �
  1   �
  1   (     Z     c  $   i  "   �     �  4   �     �  *   �  K   $     p  W   �     �  0   �     )  '   ?     g  $   v     �  Q   �  /   �  9   ,  9   f  L   �  @   �  J   .  8   y  (   �  &   �       7        N  c   d                             %                                         &   !                        $                      "       
                    	                  '   #          Can not create the mount point Can not open an unknown vault. Choose the encryption system you want to use for this vault: Configured backend can not be instantiated: %1 Configured backend does not exist: %1 Correct version found Create CryFS Device is already open Device is not open Dialog Do not show this notice again EncFS Encrypted data location Failed to create directories, check your permissions Failed to execute Failed to fetch the list of applications using this vault Forcefully close  Limit to the selected activities: Mount point Mount point is not specified Next Open with File Manager Previous The mount point directory is not empty, refusing to open the vault The specified backend is not available The vault is unknown, can not close it. The vault is unknown, can not destroy it. This device is already registered. Can not recreate it. This directory already contains encrypted data Unable to close the vault, an application is using it Unable to close the vault, it is used by %1 Unable to detect the version Unable to perform the operation Unknown device Unknown error, unable to create the backend. Vaul&t name: You need to select empty directories for the encrypted storage and for the mount point Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-11 05:53+0100
PO-Revision-Date: 2017-07-28 15:14+0300
Last-Translator: Safa Alfulaij <safa1996alfulaij@gmail.com>
Language-Team: Arabic <doc@arabeyes.org>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
X-Generator: Lokalize 2.0
 تعذّر إنشاء نقطة الضّمّ لا يمكن فتح خزنة مجهولة. اختر نظام التّعمية لاستخدامه لهذه الخزنة: تعذّر تمهيد السّند المضبوط: %1 السّند المضبوط غير موجود: %1 عُثر على الإصدارة الصّحيحة أنشئ CryFS الجهاز مفتوح بالفعل الجهاز ليس مفتوحًا حواريّ لا تعرض هذه الملاحظة مجدّدًا EncFS مكان البيانات المعمّاة فشل إنشاء المجلّدات، افحص التّصاريح لديك فشل التّنفيذ فشل جلب قائمة التّطبيقات التي تستخدم هذه الخزنة أغلق بالقوّة اقصر على الأنشطة المحدّدة: نقطة الضّمّ لم تُحدّد نقطة الضّمّ التّالي افتح بمدير الملفّات السّابق دليل نقطة الضّمّ ليس فارغًا، رُفض فتح الخزنة السّند المحدّد غير متوفّر الخزنة مجهولة، لا يمكن إغلاقها. الخزنة مجهولة، لا يمكن تدميرها. الجهاز مسجّل بالفعل. لا يمكن إعادة إنشائه. يحتوي الدّليل بيانات معمّاة بالفعل تعذّر إغلاق الخزنة، ثمّة تطبيق يستخدمها. تعذّر إغلاق الخزنة، %1 يستخدمها تعذّر اكتشاف الإصدارة تعذّر إجراء العمليّة جهاز مجهول خطأ مجهول، تعذّر إنشاء السّند. اسم ال&خزنة: عليك اختيار أدلّة فارغة للتّخزين المعمّى ولنقطة الضّم 