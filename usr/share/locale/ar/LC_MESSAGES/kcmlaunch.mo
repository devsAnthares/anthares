��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5    K  
   W  1   b  3  �  2  �	  w   �     s     �     �  /   �     �  (     1   6  %   h                                          
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2008-12-01 17:30+0400
Last-Translator: zayed <zayed.alsaidi@gmail.com>
Language-Team: Arabic <linuxac-kde-arabic-team@googlegroups.com>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
X-Generator: KBabel 1.11.4
 ثانية المهلة لإظهار &بدء التشغيل: <H1>اشعارات شريط المهام</H1>
يمكنك تمكين طريقة ثانية للإشعار إلى بدء تشغيل التطبيق و هي
المستخدمة في شريط المهام حيث يظهر زر مع صورة ساعة رملية ، ترمز إلى
أن التطبيق جاري تشغيله.
قد يحصل أن بعض التطبيقات قد لا تعي إلى اشعار بدء التشغيل هذا.
في هذه الحالة يختفي الزر بعد الوقت المعطى في قسم
'المهلة لإظهار بدء التشغيل' <h1>المؤشر المشغول</h1>
كيدي يعرض ميزة المؤشر المشغول للاشعار إلى بدء تشغيل التطبيق.
لتمكين المؤشر المشغول, انتقي نوع واحد من الارتداد المرئي من المربع المركب.
من الممكن أن يحدث أن بعض التطبيقات قد لا تعي اشعار بدء
التشغيل هذا. في هذه الحالة يتوقف المؤشر عن النبض بعد الوقت
المعطى في قسم 'المهلة لإظهار بدء التشغيل' <h1>اشعار الإطلاق</h1> هناك يمكنك ضبط كيفية إشعارك عند إطلاق التطبيق. المؤشر الوامض المؤشر المرتد ال&مؤشر المشغول مكّن إشع&ارات شريط المهام  لا مؤشر مشغول المؤشر المشغول السلبي الم&هلة لاظهار بدء التشغيل: اشعارات &شريط المهام 