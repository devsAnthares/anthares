��            )         �     �  "   �     �  !   �  !        4  
   J     U     p     �  !   �     �  0   �  8        ?  !   ]          �     �     �     �                '  
   /  
   :  
   E  &   P     w     �  �  �     �  3   �  +   �  >   �  >   5     t     �  -   �     �  -   �  C   	  =   a	  n   �	  w   
  8   �
  G   �
  :     >   B  <   �  8   �     �  :        P     a     j     |     �  E   �  
   �      �                                                                                                 	                       
                 ms &Keyboard accelerators visibility: &Top arrow button type: Always Hide Keyboard Accelerators Always Show Keyboard Accelerators Anima&tions duration: Animations Bottom arrow button t&ype: Breeze Settings Center tabbar tabs Drag windows from all empty areas Drag windows from titlebar only Drag windows from titlebar, menubar and toolbars Draw a thin line to indicate focus in menus and menubars Draw focus indicator in lists Draw frame around dockable panels Draw frame around page titles Draw frame around side panels Draw slider tick marks Draw toolbar item separators Enable animations Enable extended resize handles Frames General No Buttons One Button Scrollbars Show Keyboard Accelerators When Needed Two Buttons W&indows' drag mode: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:42+0200
PO-Revision-Date: 2018-01-26 07:57+0300
Last-Translator: Safa Alfulaij <safa1996alfulaij@gmail.com>
Language-Team: Arabic <doc@arabeyes.org>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
X-Generator: Lokalize 2.0
  م‌ث ظهور مسرّعات لو&حة المفاتيح: نوع زرّ السّهم ال&علويّ: أخفِ دائمًا مسرّعات لوحة المفاتيح أظهر دائمًا مسرّعات لوحة المفاتيح مدّة التّ&حريكات: التّحريكات &نوع زرّ السّهم السّفليّ: إعدادات نسيم وسّط ألسنة أشرطة الألسنة اسحب النّوافذ من كلّ المناطق الفارغة اسحب النّوافذ من شريط العنوان فقط اسحب النّوافذ من شريط العنوان، وشريط القوائم وأشرطة الأدوات ارسم خطًّا رفيعًا للإشارة إلى التّركيز في القوائم وأشرطة القوائم ارسم مؤشّر التّركيز في القوائم ارسم إطارًا حول اللوحات القابلة للرّصف ارسم إطارًا حول عناوين الصّفحات ارسم إطارًا حول اللوحات الجانبيّة ارسم علامات التّأشير في المنزلقة ارسم فواصل عناصر أشرطة الأدوات مكّن التّحريكات مكّن قوابض تغيير الحجم الممتدّة الإطارات عامّ بلا أزرار زرّ واحد أشرطة التّمرير أظهر مسرّعات لوحة المفاتيح عند الحاجة زرّان وضع سحب ال&نّوافذ: 