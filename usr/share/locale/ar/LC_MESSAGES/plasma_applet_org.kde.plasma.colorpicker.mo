��          |      �          %   !     G     U     c     u     �     �  
   �     �     �  $   �    �  6   �     5     O     g  +   �     �  $   �     �     �       5   &     
   	                                                   Automatically copy color to clipboard Clear History Color Options Copy to Clipboard Default color format: General Open Color Dialog Pick Color Pick a color Show history When pressing the keyboard shortcut: Project-Id-Version: plasma_applet_kolourpicker
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-12 03:03+0200
PO-Revision-Date: 2017-02-14 19:55+0300
Last-Translator: Safa Alfulaij <safa1996alfulaij@gmail.com>
Language-Team: Arabic <doc@arabeyes.org>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
X-Generator: Lokalize 2.0
 انسخ اللون إلى الحافظة آليًّا امسح التّأريخ خيارات اللون انسخ إلى الحافظة تنسيق اللون الافتراضيّ: عامّ افتح حواريّ الألوان انتقِ لونًا انتقِ لونًا أظهر التّأريخ عند ضغط اختصار لوحة المفاتيح: 