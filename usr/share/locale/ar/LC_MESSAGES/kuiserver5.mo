��          �   %   �      P     Q     b     w     �     �     �  
   �     �     �     �     �     �  /        3     Q     p     v     �     �     �     �     �     �     �         %  J   3  Z   ~     �  ,   �       $   &     K     X     e     l     s       U   �  4   �  5   (     ^     g     {     �  ,   �  -   �  1   �  2   *	  (   ]	  )   �	                            
                                                                                             	    %1 file %1 files %1 folder %1 folders %1 of %2 processed %1 of %2 processed at %3/s %1 processed %1 processed at %2/s Appearance Behavior Cancel Clear Configure... Finished Jobs List of running file transfers/jobs (kuiserver) Move them to a different list Move them to a different list. Pause Remove them Remove them. Resume Show all jobs in a list Show all jobs in a list. Show all jobs in a tree Show all jobs in a tree. Show separate windows Show separate windows. Project-Id-Version: kuiserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2012-01-12 21:50+0300
Last-Translator: Abdalrahim G. Fakhouri <abdilra7eem@yahoo.com>
Language-Team: Arabic <doc@arabeyes.org>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
X-Generator: Translate Toolkit 1.10.0
 لا شيء ملف واحد ملفين %1 ملفات %1 ملفا %1 ملف لا مجلدات مجلد واحد مجلدان %1 مجلدات %1 مجلدا %1 مجلد %1 من %2 نفذت %1 من %2 تنفذ بسرعة %3/ثانية %1 نفذت %1 تنفذ بسرعة %2/ثانية المظهر السلوك ألغ امح اضبط... المهام المنتهية قائمة بعمليات نقل الملفات الجارية/أعمال (kuiserver) انقل العنصر إلى قائمة مختلفة انقل العنصر إلى قائمة مختلفة. توقف أزل العنصر أزل العنصر. استأنف أظهر كلّ المهام في لائحة أظهر كلّ المهام في لائحة. أظهر كلّ المهام في عرض شجري أظهر كلّ المهام في عرض شجري. أظهر النوافذ المفصولة أظهر النوافذ المفصولة. 