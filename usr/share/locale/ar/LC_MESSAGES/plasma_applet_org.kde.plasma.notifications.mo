��    "      ,  /   <      �      �          &  -   E  #   s  #   �  ?   �  1   �     -     A  H   F  L   �  V   �  N   3     �  
   �     �     �     �     �  2   �  
     )     8   E  #   ~  .   �  -   �  D   �  6   D  0   {  A   �  =   �  9   ,  �  f  h   [
     �
  �   �
  ?   k     �     �     �     �     �       x      |   �  
        !     %     4     L     f  #   �     �     �     �  =   �       F   '  J   n  f   �  e         �     �     �     �     �                                                           !       	                      
                                                           "           %1 notification %1 notifications %1 of %2 %3 %1 running job %1 running jobs &Configure Event Notifications and Actions... 10 seconds ago, keep short10 s ago 30 seconds ago, keep short30 s ago A button tooltip; expands the item to show detailsShow Details A button tooltip; hides item detailsHide Details Clear Notifications Copy Either just 1 dir or m of n dirs are being processed1 dir %2 of %1 dirs Either just 1 file or m of n files are being processed1 file %2 of %1 files How much many bytes (or whether unit in the locale has been copied over total%1 of %2 Indicator that there are more urls in the notification than previews shown+%1 Information Job Failed Job Finished No new notifications. No notifications or jobs Open... Placeholder is job name, eg. 'Copying'%1 (Paused) Select All Show application and system notifications Speed and estimated time to completion%1 (%2 remaining) Track file transfers and other jobs Use custom position for the notification popup minutes ago, keep short%1 min ago %1 min ago notification was added n days ago, keep short%1 day ago %1 days ago notification was added yesterday, keep shortYesterday notification was just added, keep shortJust now placeholder is row description, such as Source or Destination%1: the job, which can be anything, failed to complete%1: Failed the job, which can be anything, has finished%1: Finished Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:07+0100
PO-Revision-Date: 2017-02-14 20:00+0300
Last-Translator: Safa Alfulaij <safa1996alfulaij@gmail.com>
Language-Team: Arabic <doc@arabeyes.org>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
X-Generator: Lokalize 2.0
 لا إخطارات إخطار واحد إخطاران %1 إخطارات %1 إخطارًا %1 إخطار %1 من %2 %3 لا مهامّ تعمل مهمّة واحدة تعمل مهمّتان تعملان %1 مهامّ تعمل %1 مهمّة تعمل %1 مهمّة تعمل ا&ضبط إخطارات الأحداث والإجراءات... منذ 10ث منذ 30ث أظهر التّفاصيل أخفِ التّفاصيل امسح الإخطارات انسخ لا أدلّة دليل واحد %2 من دليلين %2 من %1 أدلّة %2 من %1 دليلًا %2 من %1 دليل لا ملفّات ملفّ واحد %2 من ملفّين %2 من %1 ملفّات %2 من %1 ملفًّا %2 من %1 ملفّ %1 من %2 +%1 معلومات فشلت المهمّة انتهت المهمّة لا إخطارات جديدة. لا إخطارات أو مهامّ افتح... %1 (مُلبثة) حدّد الكل أظهر إخطارات التّطبيقات والنّظام %1 (بقي %2) تتبّع نقولات الملفّات والمهامّ الأخرى استخدم مكانًا مخصّصّا لمنبثقة الإخطارات منذ ثوانٍ منذ دقيقة منذ دقيقتين منذ %1 دق منذ %1 دق منذ %1 دق اليوم منذ يوم منذ يومان منذ %1 أيّام منذ %1 يومًا منذ %1 يوم بالأمس منذ لحظات %1: %1: فشلت %1: انتهت 