��          �      <      �     �     �     �  +   �  @        L     a     i     w     }     �  
   �     �     �     �     �     �     
               (  >   ?  a   ~     �           	     "     +  #   A     e     q     z     �  &   �  .   �     
         	                                                                                       <a href='%1'>%1</a> Close Copy Automatically: Don't show this dialog, copy automatically. Drop text or an image onto me to upload it to an online service. Error during upload. General History Size: Paste Please wait Please, try again. Sending... Share Shares for '%1' Successfully uploaded The URL was just shared Upload %1 to an online service Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-01 04:04+0100
PO-Revision-Date: ٢٠١٥-٠٨-١٨ ١٣:٣١+0300
Last-Translator: Safa Alfulaij <safa1996alfulaij@gmail.com>
Language-Team: Arabic <doc@arabeyes.org>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
X-Generator: Lokalize 2.0
 <a href='%1'>%1</a> أغلق انسخ آليًّا: لا تظهر هذا الحواريّ وانسخ آليًّا. أسقط النّصّ أو الصّور عليّ لرفعها إلى خدمة إنترنتيّة. خطأ أثناء الرّفع. عامّ حجم التّأريخ: ألصق فضلًا انتظر فضلًا جرّب مجدّدًا. يرسل... شارك مشاركات '%1' رُفعت بنجاح شُورك العنوان للتّوّ ارفع %1 إلى خدمة إنترنتيّة 