��    D      <  a   \      �     �     �     �  !     "   %  &   H  ,   o  #   �  &   �  !   �  &   	  '   0  "   X  #   {  !   �  "   �  '   �       +     2   <     o  
   �     �     �     �     �     �     �     �     �     	  !   	  +   *	     V	     v	      {	     �	     �	     �	     �	     �	  !   �	     
  	    
     *
     2
     R
     m
  &   �
     �
     �
     �
     �
     �
     �
     �
            $        A     R     l     ~     �     �     �  >   �  �       	       1        F     M     V     d  
   �     �     �     �     �     �     �     �     �     �       X     _   o     �     �            
   /      :     [     j     v  *   �     �  0   �  G   �  1   E     w  5   �     �  +   �             -   #  5   Q     �     �     �  1   �  "   �  !     5   2  3   h     �  .   �     �     �     �     �             F   1  (   x  $   �     �     �  (        *  ,   F     s     D   8      1                 )                  .               3   @   4       &      /   >       ?      <   ;             7       (          
                 $                  0   5       A   :   #   2         C   +                        =   '             *                 	          ,   !                             B   9   6       "       %   -     ms % &Matching window property:  @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Border @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large @item:inlistbox Button size:Large @item:inlistbox Button size:Medium @item:inlistbox Button size:None @item:inlistbox Button size:Small @item:inlistbox Button size:Very Large Add Add handle to resize windows with no border Allow resizing maximized windows from window edges Anima&tions duration: Animations B&utton size: Border size: Center Center (Full Width) Class:  Color: Decoration Options Detect Window Properties Dialog Draw a circle around close button Draw separator between Title Bar and Window Draw window background gradient Edit Edit Exception - Breeze Settings Enable animations Enable/disable this exception Exception Type General Hide window title bar Information about Selected Window Left Move Down Move Up New Exception - Breeze Settings Question - Breeze Settings Regular Expression Regular Expression syntax is incorrect Regular expression &to match:  Remove Remove selected exception? Right Shadows Si&ze: Tiny Tit&le alignment: Title:  Use window class (whole application) Use window title Warning - Breeze Settings Window Class Name Window Identification Window Property Selection Window Title Window-Specific Overrides strength of the shadow (from transparent to opaque)S&trength: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-09 03:20+0100
PO-Revision-Date: 2018-01-26 07:57+0300
Last-Translator: Safa Alfulaij <safa1996alfulaij@gmail.com>
Language-Team: Arabic <doc@arabeyes.org>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
  م‌ث % خاصيّة النّافذة ال&مطابقة:  ضخم كبير لا حدود لا حدود جانبيّة عاديّ فوق الطّبيعيّ ضئيل ضخم جدًّا كبير جدًّا كبير متوسّط بلا صغير كبير جدًّا أضف أضف مقبضًا لتغيير حجم النّافذة الّتي لا حدود لها اسمح بتغيير حجم النّوافذ المكبّرة من حوافّ النّوافذ مدّة التّ&حريكات: التّحريكات حجم الزّ&رّ: حجم الإطار: الوسط الوسط (كامل العرض) الصّنف:  اللون: خيارات الزّخرفة اكتشف خاصيّات النّافذة حواريّ ارسم دائرة حول زرّ الإغلاق ارسم فاصلًا بين شريط العنوان والنّافذة ارسم تدرّج خلفيّة للنّافذة حرّر حرّر الإستثناء - إعدادات نسيم مكّن التّحريكات مكّن/عطّل هذا الاستثناء نوع الاستثناء عامّ أخفِ شريط عنوان النّافذة معلومات عن النّافذة المحدّدة اليسار انقل لأسفل انقل ﻷعلى استنثاء جديد - إعدادات نسيم سؤال - إعدادات نسيم التّعبير النّمطيّ صياغة التّعبير النمطيّ خاطئة التّعبير النّمطي لم&طابقته:  أزل أأزيل الاستثناء المحدّد؟ اليمين الظلال الح&جم: ضئيل محاذاة الع&نوان: العنوان:  استخدم صنف النّافذة (التّطبيق بالكامل) استخدم عنوان النّافذة تحذير - إعدادات نسيم اسم صنف النّافذة تعريف النّافذة تحديد خاصيّة النّافذة عنوان النّافذة تجاوزات خاصّة بالنّافذة ال&قوّة: 