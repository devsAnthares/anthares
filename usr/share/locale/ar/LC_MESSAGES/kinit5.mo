��          |      �             !  $   A     f     �     �     �     �  '   �  �     �   �     }  �  �  J   �  1   �  4     .   B  -   q     �  1   �  I   �  �   2  �   �  +   �                   
                           	             Could not find '%1' executable. Could not find 'kdemain' in '%1'.
%2 Could not find service '%1'. Could not open library '%1'.
%2 KDEInit could not launch '%1' Launching %1 Service '%1' is malformatted. Service '%1' must be executable to run. Unable to create new process.
The system may have reached the maximum number of processes possible or the maximum number of processes that you are allowed to use has been reached. Unable to start new process.
The system may have reached the maximum number of open files possible or the maximum number of open files that you are allowed to use has been reached. Unknown protocol '%1'.
 Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-04 03:08+0100
PO-Revision-Date: 2013-08-08 13:54+0300
Last-Translator: Safa Alfulaij <safaalfulaij@hotmail.com>
Language-Team: Arabic <doc@arabeyes.org>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
X-Generator: Lokalize 1.5
 لم أتمكن من العثور على الملف التنفيذي '%1'. فشل العثور على 'kdemain' في '%1'
%2 لا يمكن العثور على الخدمة '%1'. لم أستطع فتح المكتبة '%1'.
%2 لم يتمكن KDEInit من إطلاق '%1'. جاري تشغيل %1 الخدمة %1 غير صحيحة التهيئة. لا بد أن تكون الخدمة '%1' تنفيذية حتى تعمل. لا يمكن إنشاء عملية جديدة.
بما وصل النظام إلى الحد الأقصى للعمليات (أو الحد الذي حددته أنت). لا يمكن بدء العملية الجديدة.
ربما وصل النظام إلى الحد الأقصى للملفات المفتوحة (أو الحد الذي تسمح به أنت). البرتوكول '%1' غير معروف.
 