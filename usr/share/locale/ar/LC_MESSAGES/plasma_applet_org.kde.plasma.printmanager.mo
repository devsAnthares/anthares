��          �      ,      �     �     �     �     �     �     �     �     �  .   �     .     L     \     m     �     �  H   �  �  �     �  $        &  &   <     c     w     �     �  3   �  �   �  b   x  $   �  $         %     8  \  T        	                                                                  
    &Configure Printers... Active jobs only All jobs Completed jobs only Configure printer General No active jobs No jobs No printers have been configured or discovered One active job %1 active jobs One job %1 jobs Open print queue Print queue is empty Printers Search for a printer... There is one print job in the queue There are %1 print jobs in the queue Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2015-02-21 09:37+0000
PO-Revision-Date: 2015-04-19 18:30+0300
Last-Translator: Safa Alfulaij <safa1996alfulaij@gmail.com>
Language-Team: Arabic <doc@arabeyes.org>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
X-Generator: Lokalize 1.5
 ا&ضبط الطّابعات... المهامّ النّشطة فقط كلّ المهامّ المهامّ المكتملة فقط اضبط طابعة عامّ لا مهامّ نشطة لا مهامّ لم تُضبط أو تُكتشف أيّ طابعة لا مهامّ نشطة مهمّة نشطة واحدة مهمّتان نشطتان %1 مهامّ نشطة %1 مهمّةً نشطة %1 مهمّة نشطة لا مهامّ مهمّة واحدة مهمّتان %1 مهمّات %1 مهمّةً %1 مهمّة افتح طابور الطّباعة طابور الطّباعة فارغ الطّابعات ابحث عن طابعة... ليس هناك مهامّ طباعة في الطّابور هناك مهمّة طباعة واحدة في الطّابور هناك مهمّتا طباعة في الطّابور هناك %1 مهمّات طباعة في الطّابور هناك %1 مهمّة طباعة في الطّابور هناك %1 مهمّة طباعة في الطّابور 