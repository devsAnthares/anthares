��    '      T  5   �      `     a  H   f     �     �     �     �  "   �       
   /     :     S     c  	   l     v     �     �     �     �     �  "   �     �  	               ?   :     z     �     �     �     �     �  ;   �  &   �      &  %   G     m     �  '   �  	  �     �     �     �     �     �     	     '	     5	     F	  .   U	     �	     �	     �	     �	  &   �	     �	  (   �	     
     )
  9   /
     i
     k
     y
     �
  $   �
     �
     �
     �
     �
     �
     
     #     ,     C     Y     s     �     �             &                                                                 '   #              !                     	   %   $          "                        
                      min A weather station location and the weather service it comes from%1 (%2) Celsius °C Degree, unit symbol° Fahrenheit °F Hectopascals hPa High & Low temperatureH: %1 L: %2 High temperatureHigh: %1 Kilometers Kilometers per Hour km/h Kilopascals kPa Knots kt Location: Low temperatureLow: %1 Meters per Second m/s Miles Miles per Hour mph Millibars mbar N/A No weather stations found for '%1' Percent, measure unit% Pressure: Search Short for no data available- Shown when you have not set a weather providerPlease Configure Temperature: Units Update every: Visibility: Weather Station Wind speed: certain weather condition (probability percentage)%1 (%2%) content of water in airHumidity: %1%2 distance, unitVisibility: %1 %2 ground temperature, unitDewpoint: %1 pressure, unitPressure: %1 %2 temperature, unit%1%2 visibility from distanceVisibility: %1 Project-Id-Version: plasma_applet_weather
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-28 05:50+0100
PO-Revision-Date: 2017-02-14 17:47+0300
Last-Translator: Safa Alfulaij <safa1996alfulaij@gmail.com>
Language-Team: Arabic <doc@arabeyes.org>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
X-Generator: Lokalize 2.0
  دق %1 (%2) سيليزيّ °س ° فهرنهايت °ف هيكتوباسكال ك: %1 ص: %2 الكبرى: %1 كيلومتر كيلومتر في السّاعة (كم/سا) كيلوباسكال عقدة المكان: الصّغرى: %1 متر في الثّانية (م/ثا) ميل ميل في السّاعة (ميل/سا) ملّيبار غ/م لم يُعثر على محطّات طقس ل‍ ’%1‘ % الضّغط: ابحث - رجاء اضبط الإعدادات درجة الحرارة: الوحدات حدّث كلّ: الرّؤية: محطّة الطّقس سرعة الرّياح: %1 (%2%) الرّطوبة: %1%2 الرّؤية: %1 %2 نقطة النّدى: %1 الضّغط: %1 %2 %1%2 الرّؤية: %1 