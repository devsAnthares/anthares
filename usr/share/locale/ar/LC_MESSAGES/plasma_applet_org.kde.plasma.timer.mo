��          �      �           	          &     -     4  
   =     H     Q     Y     i  >   w     �     �     �     �  
   �     �     �     �            U   %     {     |     �  	   �  	   �     �     �     �     �     �     �           	   +     5     M     g          �     �     �     �  �   �                       	                                                
                                 %1 is running %1 not running &Reset &Start Advanced Appearance Command: Display Execute command Notifications Remaining time left: %1 second Remaining time left: %1 seconds Run command S&top Show notification Show seconds Show title Text: Timer Timer finished Timer is running Title: Use mouse wheel to change digits or choose from predefined timers in the context menu Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: ٢٠١٥-٠٨-١٨ ١٤:٣٧+0300
Last-Translator: Safa Alfulaij <safa1996alfulaij@gmail.com>
Language-Team: Arabic <doc@arabeyes.org>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
X-Generator: Lokalize 2.0
 %1 يعمل %1 لا يعمل &صفّر ا&بدأ متقدّم المظهر الأمر: اعرض نفّذ أمرًا الإخطارات الوقت المتبقّي: أقلّ من ثانية الوقت المتبقّي: ثانية واحدة الوقت المتبقّي: ثانيتين الوقت المتبقّي: %1 ثوان الوقت المتبقّي: %1 ثانية الوقت المتبقّي: %1 ثانية شغّل أمرًا أو&قف أظهر إخطارًا أظهر الثّواني أظهر العنوان النّصّ: المؤقّت انتهى المؤقّت المؤقّت يعمل العنوان: استخدم عجلة الفأرة لتغيير الأرقام أو اختر أحد المؤقّتات المعرّفة مسبقًا في قائمة السّياق 