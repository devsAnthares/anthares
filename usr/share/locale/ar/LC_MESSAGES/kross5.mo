��            )   �      �  &   �     �     �     �     �      �               .     6  ,   S  3   �     �     �     �     �     �  '        4     S     Y     o     �     �     �     �     �  &   �     �  �  �     �     �  	        %     .  I   >  
   �  %   �     �  %   �  2   �  *     #   I     m     �     �     �  "   �  F   �     	  &   )	  *   P	     {	  #   �	     �	  "   �	     �	  0   �	  	   

                                       
                      	                                                                            @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2013-08-08 13:54+0300
Last-Translator: Safa Alfulaij <safaalfulaij@hotmail.com>
Language-Team: Arabic <doc@arabeyes.org>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
X-Generator: Lokalize 1.5
 عام أضف سكربت جديد. أضف... ألغ؟ التعليق: zayed.alsaidi@gmail.com,hannysabbagh@hotmail.com,safaalfulaij@hotmail.com حرِّر حرّر السكربت المحدد. حرّر... نفّذ السكربت المحدد. فشل إنشاء السكربت للمؤول "%1" فشل تحديد مفسر سكربت "%1" فشل تحميل المفسر "%1" فشل فتح سكربت "%1" ملف: الأيقونة: المؤول: مستوى أمان مفسر Ruby. زايد السعيدي,محمد هاني صباغ,صفا الفليج الاسم: الدالة  "%1" غير موجودة هذا المؤول غير موجود "%1" أزِل أزل السكربت المحدد. شغّل الملف "%1"  غير موجود قف أوقف تنفيذ السكربت المحدد. النص: 