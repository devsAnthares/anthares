��          �      L      �  m   �     /  "   <  F   _  F   �  F   �  J   4  N     R   �     !  %   2  $   X  #   }  $   �  %   �  &   �  �        �  �  �     O     d  /   t  J   �  T   �  R   D  X   �  `   �  f   Q	     �	     �	     �	     �	     �	     �	  	   �	     
     
                                
                          	                                          Close the encrypted container; partitions inside will disappear as they had been unpluggedLock the container Eject medium Finds devices whose name match :q: Lists all devices and allows them to be mounted, unmounted or ejected. Lists all devices which can be ejected, and allows them to be ejected. Lists all devices which can be mounted, and allows them to be mounted. Lists all devices which can be unmounted, and allows them to be unmounted. Lists all encrypted devices which can be locked, and allows them to be locked. Lists all encrypted devices which can be unlocked, and allows them to be unlocked. Mount the device Note this is a KRunner keyworddevice Note this is a KRunner keywordeject Note this is a KRunner keywordlock Note this is a KRunner keywordmount Note this is a KRunner keywordunlock Note this is a KRunner keywordunmount Unlock the encrypted container; will ask for a password; partitions inside will appear as they had been plugged inUnlock the container Unmount the device Project-Id-Version: plasma_runner_solid
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-11-06 12:53+0000
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
 Bloquear o contentor Ejectar o disco Descobre os dispositivos que correspondem a :q: Apresenta todos os dispositivos e permite-os montar, desmontar ou ejectar. Apresenta todos os dispositivos que podem ser ejectados, permitindo essa operação. Apresenta todos os dispositivos que podem ser montados, permitindo a sua montagem. Apresenta todos os dispositivos que podem ser desmontados, permitindo a sua desmontagem. Apresenta todos os dispositivos encriptados que podem ser bloqueados, permitindo o seu bloqueio. Apresenta todos os dispositivos encriptados que podem ser desbloqueados, permitindo o seu desbloqueio. Montar o dispositivo dispositivo ejectar bloquear montar desbloquear desmontar Desbloquear o contentor Desmontar o dispositivo 