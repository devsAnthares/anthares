��          t      �         P     )   b  %   �  @   �     �  V   	     `     {     �     �  �  �     A  -   H  ?   v  3   �  $   �  _     '   o     �  
   �     �                         	      
                             %1 is '%1 packages to update' and %2 is 'of which %1 is security updates'%1, %2 1 package to update %1 packages to update 1 security update %1 security updates First part of '%1, %2'1 package to update %1 packages to update No packages to update Second part of '%1, %2'of which 1 is security update of which %1 are security updates Security updates available System up to date Update Updates available Project-Id-Version: muon-notifier
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-15 03:15+0100
PO-Revision-Date: 2018-01-15 09:47+0000
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
 %1, %2 1 pacote a actualizar %1 pacotes a actualizar 1 actualização de segurança %1 actualizações de segurança 1 pacote para actualizar %1 pacotes para actualizar Não existem pacotes para actualizar dos quais 1 é uma actualização de segurança dos quais %1 são actualizações de segurança Actualizações do sistema disponíveis Sistema actualizado Actualizar Actualizações disponíveis 