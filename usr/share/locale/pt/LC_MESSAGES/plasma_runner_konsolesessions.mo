��          4      L       `   $   a   /   �   �  �   9   ~  4   �                    Finds Konsole sessions matching :q:. Lists all the Konsole sessions in your account. Project-Id-Version: plasma_runner_konsolesessions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2009-04-01 16:47+0100
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POFile-SpellExtra: Konsole
Plural-Forms: nplurals=2; plural=n != 1;
 Procura pelas sessões do Konsole que correspondam a :q:. Apresenta todas as sessões do Konsole na sua conta. 