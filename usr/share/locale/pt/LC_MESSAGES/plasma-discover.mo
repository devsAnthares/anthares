��    d      <  �   \      �     �     �     �     �     �  5   �  #   �  E   	  E   _	  >   �	     �	     �	  7   
     E
     _
     p
     �
     �
     �
     �
     �
     �
          	          4     B     W     i     n  	   u          �     �  !   �  F   �     #     @     Q  <   c     �     �  *   �     �      �            	        &     6  	   >     H     X     _      h     �  
   �     �     �     �     �  
   �  F     :   K     �     �     �     �     �     �  8   �     �       	     
   "     -     =     F     W     l     r     �     �     �     �     �     �     �  &   �     "  
   >     I     Y     y     �     �     �     �  $   �  S  �     :     U     X     `     t  5   �  +   �  F   �  F   -  ?   t     �     �  ?   �          =  (   Q  $   z     �     �     �     �     �  
   �               6     N     n     �     �  
   �      �     �     �  '   �  R   !  -   t     �     �  L   �           '  6   0     g  '   u     �     �     �     �     �  	   �     �     �  	   �  .     #   5     Y  %   g     �     �     �     �  [   �  A   .  
   p     {     �     �     �     �  ?   �     �               &     3     F     U     e     �  $   �     �     �     �     �  '   �          #     1  *   7     b     r     �     �     �     �  "   �     �  3            J   `   :       	   W                 [   .   E         )      (   R   M   '   O   $       7   Y   4                -                    9       8   #   S       0   P       B      
   +                         L   >              N   @      *   D      F   /   ;       5       2   ^      U               ?   6       <   V   ,       c       \   Z      G   1                  d       C   Q   &       A          _   b       =   T      !   3             a   K           ]              H      %      I   X          "    
Also available in %1 %1 %1 (%2) %1 (Default) <b>%1</b> by %2 <em>%1 out of %2 people found this review useful</em> <em>Tell us about this review!</em> <em>Useful? <a href='true'><b>Yes</b></a>/<a href='false'>No</a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'><b>No</b></a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'>No</a></em> @infoFetching updates @infoFetching... @infoIt is unknown when the last check for updates was @infoLooking for updates @infoNo updates @infoNo updates are available @infoShould check for updates @infoThe system is up to date @infoUpdates @infoUpdating... Accept Add Source... Addons Aleix Pol Gonzalez An application explorer Apply Changes Available backends:
 Available modes:
 Back Cancel Category: Checking for updates... Comment too long Comment too short Compact Mode (auto/compact/full). Could not close the application, there are tasks that need to be done. Could not find category '%1' Couldn't open %1 Delete the origin Directly open the specified application by its package name. Discard Discover Display a list of entries with a category. Extensions... Failed to remove the source '%1' Featured Help... Homepage: Improve summary Install Installed Jonathan Thomas Launch License: List all the available backends. List all the available modes. Loading... Local package file to install Make default More Information... More... No Updates Open Discover in a said mode. Modes correspond to the toolbar buttons. Open with a program that can deal with the given mimetype. Proceed Rating: Remove Resources for '%1' Review Reviewing '%1' Running as <em>root</em> is discouraged and unnecessary. Search Search in '%1'... Search... Search: %1 Search: %1 + %2 Settings Short summary... Show reviews (%1)... Size: Sorry, nothing found... Source: Specify the new source for %1 Still looking... Summary: Supports appstream: url scheme Tasks Tasks (%1%) TransactioName TransactionStatus%1 %2 Unable to find resource: %1 Update All Update Selected Update section nameUpdate (%1) Updates Version: unknown reviewer updates not selected updates selected © 2010-2016 Plasma Development Team Project-Id-Version: muon-discover
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:02+0100
PO-Revision-Date: 2018-01-14 11:43+0000
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POFile-IgnoreConsistency: Launch
X-POFile-SpellExtra: Gonzalez Muon Discover Aleix Pol APT Buzz Jonathan
Plural-Forms: nplurals=2; plural=n != 1;
X-POFile-SpellExtra: stdout appstream Hola
X-POFile-IgnoreConsistency: Update
 
Também disponível em %1 %1 %1 (%2) %1 (Predefinição) <b>%1</b> de %2 <em>%1 em %2 pessoas acharam útil esta revisão</em> <em>Diga-nos algo sobre esta revisão!</em> <em>Útil? <a href='true'>Sim</a>/<a href='false'><b>Não</b></a></em> <em>Útil? <a href='true'>Sim</a>/<a href='false'><b>Não</b></a></em> <em>Útil? <a href='true'>Sim</a>/<a href='false'>Não</a></em> A obter as actualizações A transferir... Desconhece-se quando foi a última pesquisa por actualizações À procura de actualizações Sem actualizações Não estão disponíveis actualizações Se deve procurar por actualizações O sistema está actualizado Actualizações A actualizar... Aceitar Adicionar uma Fonte... Extensões Aleix Pol Gonzalez Um explorador de aplicações Aplicar as Alterações Infra-estruturas disponíveis:
 Modos disponíveis:
 Recuar Cancelar Categoria: À procura de actualizações... Comentário demasiado extenso Comentário demasiado curto Modo Compacto (auto/compacto/completo). Não foi possível fechar a aplicação; existem tarefas que precisam de terminar. Não foi possível encontrar a categoria '%1' Não foi possível aceder ao %1 Remover a origem Abre directamente a aplicação indicada de acordo com o nome do seu pacote. Apagar Discover Mostrar uma lista de elementos com uma dada categoria. Extensões... Não foi possível remover a fonte '%1' Em Destaque Ajuda... Página Web: Melhorar o resumo Instalar Instalado Jonathan Thomas Lançar Licença: Mostra todas as infra-estruturas disponíveis. Mostra todos os modos disponíveis. A carregar... O ficheiro local do pacote a instalar Tornar predefinido Mais Informações... Mais... Sem Actualizações Abre o Discover num modo seguro. Os modos correspondem aos botões da barra de ferramentas. Abrir com um programa que consiga lidar com o tipo MIME indicado. Prosseguir Classificação: Remover Recursos de '%1' Revisão A rever o '%1' A execução <em>root</em> não é recomendada nem necessária. Procurar Procurar no '%1'... Procurar... Procurar: %1 Pesquisar: %1 + %2 Configuração Breve resumo... Mostrar as revisões (%1)... Tamanho: Infelizmente, nada foi encontrado... Origem: Indique a nova fonte para '%1' Ainda à procura... Resumo: Suporta o esquema de URL's 'appstream:' Tarefas Tarefas (%1%) %1 %2 Não foi possível descobrir o recurso: %1 Actualizar Tudo Actualizar os Seleccionados Actualização (%1) Actualizações Versão: revisor desconhecido actualizações não seleccionadas actualizações seleccionadas © 2010-2016 da Equipa de Desenvolvimento do Plasma 