��          �      ,      �     �     �     �     �     �     �     �     �  .   �     .     L     \     m     �     �  H   �  �  �     �     �     �               5     ;     O  2   [  $   �     �     �      �            d   0        	                                                                  
    &Configure Printers... Active jobs only All jobs Completed jobs only Configure printer General No active jobs No jobs No printers have been configured or discovered One active job %1 active jobs One job %1 jobs Open print queue Print queue is empty Printers Search for a printer... There is one print job in the queue There are %1 print jobs in the queue Project-Id-Version: plasma_applet_printmanager
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2015-02-21 09:37+0000
PO-Revision-Date: 2015-02-21 16:21+0000
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-POFile-IgnoreConsistency: Release
 &Configurar as Impressoras... Apenas as tarefas activas Todas as tarefas Apenas as tarefas completas Configurar a impressora Geral Sem tarefas activas Sem tarefas Não foram configuradas ou descobertas impressoras Uma tarefa activa %1 tarefas activas Uma tarefa %1 tarefas Abrir a fila de impressão A fila de impressão está vazia Impressoras Procurar por uma impressora... Existe uma tarefa de impressão na fila de espera Existem %1 tarefas de impressão na fila de espera 