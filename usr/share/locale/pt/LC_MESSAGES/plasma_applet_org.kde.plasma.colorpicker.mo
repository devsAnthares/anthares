��          |      �          %   !     G     U     c     u     �     �  
   �     �     �  $   �  �  �  ;   �     �     �  %   �          3     9     Q     b     s  %   �     
   	                                                   Automatically copy color to clipboard Clear History Color Options Copy to Clipboard Default color format: General Open Color Dialog Pick Color Pick a color Show history When pressing the keyboard shortcut: Project-Id-Version: plasma_applet_org
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-12 03:03+0200
PO-Revision-Date: 2016-12-14 11:37+0000
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
 Copiar automaticamente a cor para a área de transferência Limpar o Histórico Opções de Cores Copiar para a Área de Transferência Formato por omissão da cor: Geral Abrir a Janela de Cores Escolher uma Cor Escolher uma cor Mostrar o histórico Ao carregar a combinação de teclas: 