��    2      �  C   <      H     I     M     R  .   a  5   �     �     �     �     �  	   �     �          '     6  1   J     |     �     �     �  
   �     �  '   �     �     �     �          !  '   (     P     _     f     n     �  5   �     �     �     �     �     �  $   �  A   #  �   e     K     R  C   c  '   �     �     �     �  �  	     �
     �
     �
     �
                    7     >  
   P  #   [          �     �  O   �            *   6     a     j     y     �     �     �     �     �  	   �  =   �             	   (  !   2     T  ;   ]     �  	   �     �     �     �     �     �               "     A     V     ]     p  %   x               $   0       .                      %                ,         )   2          /   "                        &   1      '       +   !   	         -                               
         (                    *            #       %1% Back Battery at %1% Button to change keyboard layoutSwitch layout Button to show/hide virtual keyboardVirtual Keyboard Cancel Caps Lock is on Close Close Search Configure Configure Search Plugins Desktop Session: %1 Different User Keyboard Layout: %1 Logging out in 1 second Logging out in %1 seconds Login Login Failed Login as different user Logout Next track No media playing Nobody logged in on that sessionUnused OK Password Play or Pause media Previous track Reboot Reboot in 1 second Reboot in %1 seconds Recent Queries Remove Restart Show media controls: Shutdown Shutting down in 1 second Shutting down in %1 seconds Start New Session Suspend Switch Switch Session Switch User Textfield placeholder textSearch... Textfield placeholder text, query specific KRunnerSearch '%1'... This is the first text the user sees while starting in the splash screen, should be translated as something short, is a form that can be seen on a product. Plasma is the project name so shouldn't be translated.Plasma made by KDE Unlock Unlocking failed User logged in on console (X display number)on TTY %1 (Display %2) User logged in on console numberTTY %1 Username Username (location)%1 (%2) in category recent queries Project-Id-Version: plasma_lookandfeel_org
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-09 03:21+0100
PO-Revision-Date: 2018-01-09 10:23+0000
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-POFile-SpellExtra: Lock Caps TTY
 %1% Recuar Bateria a %1% Mudar de disposição Teclado Virtual Cancelar O Caps Lock está ligado Fechar Fechar a Pesquisa Configurar Configurar os 'Plugins' de Pesquisa Sessão do Ecrã: %1 Utilizador Diferente Disposição do Teclado: %1 A encerrar a sessão daqui a 1 segundo A encerrar a sessão daqui a %1 segundos Ligar Autenticação sem Sucesso Autenticar-se como um utilizador diferente Encerrar Faixa seguinte Nada em reprodução Não utilizado OK Senha Tocar ou pausar Faixa anterior Reiniciar A reiniciar daqui a 1 segundo A reiniciar daqui a %1 segundos Pesquisas Recentes Remover Reiniciar Mostrar os controlos multimédia: Desligar A desligar daqui a 1 segundo A desligar daqui a %1 segundos Iniciar uma Nova Sessão Suspender Mudar Mudar de Sessão Mudar de Utilizador Procurar... Procurar por '%1'... Plasma do KDE Desbloquear O desbloqueio foi mal-sucedido no TTY %1 (Ecrã %2) TTY %1 Nome do Utilizador %1 (%2) nas pesquisas recentes por categorias 