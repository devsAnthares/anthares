��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  �  K     �  )   �  �  (  �  �  p   �
     %     9     V  ,   i     �     �  )   �  "   �                                          
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2004-11-24 17:30+0000
Last-Translator: José Nuno Pires <zepires@gmail.com>
Language-Team: pt <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POFile-SpellExtra:  Feedback feedback
Plural-Forms: nplurals=2; plural=n != 1;
  s &Tempo-limite de indicação de arranque: <H1>Notificação da Barra de Tarefas</H1>
O utilizador pode activar um segundo método de notificação do arranque que é
usado pela barra de tarefas em que aparece um disco a rodar, o que
indica que a aplicação iniciada pelo utilizador está a ser lida.
Pode acontecer que certas aplicações não se dêem conta desta notificação.
Neste caso o botão desaparece ao fim do período indicado na secção
'Tempo-limite da indicação do arranque' <h1>Cursor de Ocupado</h1>
O KDE mostra um cursor de ocupado para notificar o arranque duma aplicação.
Para activar o cursor de ocupado, seleccione 'Activar o cursor de Ocupado'.
Para ter o cursor a piscar, seleccione 'Activar a intermitência' em baixo.
Pode acontecer que certas aplicações não se dêem conta desta notificação.
Neste caso, o cursor deixa de piscar ao fim do período indicado na secção
'Tempo-limite da indicação do arranque' <h1>'Feedback' de Lançamento</h1>O utilizador pode configurar aqui o 'feedback' do lançamento de aplicações. Cursor Intermitente Cursor de Ocupado Saltitante Cur&sor de Ocupado Ac&tivar a notificação da barra de tarefas Sem Cursor de Ocupado Cursor de Ocupado Passivo Tempo-limite de indicação de arranq&ue: &Notificação da Barra de Tarefas 