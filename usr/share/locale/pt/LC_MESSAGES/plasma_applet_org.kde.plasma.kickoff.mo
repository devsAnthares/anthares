��            )   �      �     �  
   �  /   �  -   �          !  
   2     =     J     `  W   i     �  ,   �  	                  !     '     -  
   :     E     W     o     �     �     �     �  1   �       �       �  
   �     �     �             
   .     9  #   F  
   j  z   u     �  :   
  	   E     O  
   c     n     v     {     �     �     �     �     �  !   �     	     4	     R	     l	                                                                                  
                                	                       %1@%2 %2@%3 (%1) @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Favorites All Applications Appearance Applications Applications updated. Computer Drag tabs between the boxes to show/hide them, or reorder the visible tabs by dragging. Edit Applications... Expand search to bookmarks, files and emails Favorites Hidden Tabs History Icon: Leave Menu Buttons Often Used On All Activities On The Current Activity Remove from Favorites Show In Favorites Show applications by name Sort alphabetically Switch tabs on hover Type is a verb here, not a nounType to search... Visible Tabs Project-Id-Version: plasma_applet_org
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2017-09-10 00:54+0100
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-POFile-SpellExtra: mails
 %1@%2 %2@%3 (%1) Escolher... Limpar o Ícone Adicionar aos Favoritos Todas as Aplicações Aparência Aplicações As aplicações foram actualizadas. Computador Arraste as páginas entre as diversas áreas para as mostrar/esconder, ou reordene as páginas visíveis por arrastamento. Editar as Aplicações... Expandir a pesquisa para os favoritos, ficheiros e e-mails Favoritos Páginas Escondidas Histórico Ícone: Sair Botões do Menu Usados Frequentemente Em Todas as Actividades À Actividade Actual Remover dos Favoritos Mostrar nos Favoritos Mostrar as aplicações pelo nome Ordenar alfabeticamente Mudar de páginas à passagem Escreva para pesquisar... Páginas Visíveis 