��            )   �      �  ;   �  J   �          /  ~   7  A   �     �       )        G     X     i      q     �     �     �  
   �     �  
   �     �             "   <     _  	   y     �     �     �  �  �     �     �     �     �  �   �  B   h     �     �  <   �     	     *	     C	     L	     ^	     t	     �	     �	     �	     �	     �	     �	  '   �	  )    
     *
     J
     Q
     o
     �
                                      
                                                                                            	        %1 is the full user name, %2 is the user login name%1 (%2) %1 is the name of a detail about the current action provided by polkit%1: (c) 2009 Red Hat, Inc. Action: An application is attempting to perform an action that requires privileges. Authentication is required to perform this action. Another client is already authenticating, please try again later. Application: Authentication Required Authentication failure, please try again. Click to edit %1 Click to open %1 Details EMAIL OF TRANSLATORSYour emails Former maintainer Jaroslav Reznik Lukáš Tinkl Maintainer NAME OF TRANSLATORSYour names P&assword: Password for %1: Password for root: Password or swipe finger for %1: Password or swipe finger for root: Password or swipe finger: Password: PolicyKit1 KDE Agent Select User Vendor: Project-Id-Version: polkit-kde-authentication-agent-1
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:22+0100
PO-Revision-Date: 2015-01-10 12:51+0000
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POFile-SpellExtra: PolicyKit Jaroslav Reznik Hat Red Inc Lukáš Tinkl
Plural-Forms: nplurals=2; plural=n != 1;
 %1 (%2) %1: (c) 2009 Red Hat, Inc. Acção: Uma aplicação está a tentar efectuar uma acção que precisa de privilégios adicionais. É necessária a autenticação para executar esta acção. Já está outro cliente a autenticar-se; por favor, tente de novo. Aplicação: Autenticação Necessária Ocorreu um erro de autenticação; por favor, tente de novo. Carregue para editar o %1 Carregue para abrir o %1 Detalhes zepires@gmail.com Manutenção anterior Jaroslav Reznik Lukáš Tinkl Manutenção José Nuno Pires Senh&a: Senha do '%1': Senha do 'root': Digite a senha do '%1' ou passe o dedo: Digite a senha do 'root' ou passe o dedo: Digite a senha ou passe o dedo: Senha: Agente para KDE do PolicyKit1 Seleccionar o Utilizador Fornecedor: 