��    F      L  a   |         
     
     
        "     1     5     I     X     ^  	   m     w  	        �     �     �  
   �     �     �  3   �  	   �     �               $     9     @     G     V     f     m  
        �  2   �  ?   �                    )     2     ?     N     S     a     r     �     �     �     �     �     �  	   �     �     �     �     �  b   �  �   [	     �	     �	  	   
     
     (
  
   8
  	   C
     M
     ]
     e
     k
     }
  �  �
     O     [     q     �     �     �     �     �     �     �     �     �               "  
   +     6     =     D  
   F     Q     i     �     �     �     �     �     �     �     �     �     �  M     ^   \     �     �     �  	   �     �                    )     A     R     U     k     |     �     �     �     �     �     �     �  `   �  �   H     �     �               4     J     W     g  	   �     �     �     �     +       E   C   4       &       D           
   <      *   9           !   F      /   #                A       @   =                     $   ;                  B             :       '           0         1   2                                -   (       6   ?         >              "   .   %   ,              )       8       3   5   	                  7    Activities Add Action Add Spacer Add Widgets... Alt Alternative Widgets Always Visible Apply Apply Settings Apply now Author: Auto Hide Back-Button Bottom Cancel Categories Center Close Concatenation sign for shortcuts, e.g. Ctrl+Shift+ Configure Configure activity Create activity... Ctrl Currently being used Delete Email: Forward-Button Get new widgets Height Horizontal-Scroll Input Here Keyboard shortcuts Layout cannot be changed whilst widgets are locked Layout changes must be applied before other changes can be made Layout: Left Left-Button License: Lock Widgets Maximize Panel Meta Middle-Button More Settings... Mouse Actions OK Panel Alignment Remove Panel Right Right-Button Screen Edge Search... Shift Stop activity Stopped activities: Switch The settings of the current module have changed. Do you want to apply the changes or discard them? This shortcut will activate the applet: it will give the keyboard focus to it, and if the applet has a popup (such as the start menu), the popup will be open. Top Undo uninstall Uninstall Uninstall widget Vertical-Scroll Visibility Wallpaper Wallpaper Type: Widgets Width Windows Can Cover Windows Go Below Project-Id-Version: plasma_shell_org
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-09 03:10+0200
PO-Revision-Date: 2017-09-10 00:55+0100
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-POFile-IgnoreConsistency: Switch
 Actividades Adicionar uma Acção Adicionar um Espaço Adicionar Elementos... Alt Elementos Alternativos Sempre Visível Aplicar Aplicar a Configuração Aplicar agora Autor: Auto-Esconder Botão de Recuo Fundo Cancelar Categorias Centro Fechar + Configurar Configurar a actividade Criar uma actividade... Ctrl Em uso de momento Apagar E-mail: Botão de Avanço Obter elementos novos Altura Deslocamento Horizontal Escreva Aqui Atalhos do Teclado A disposição não pode ser alterada enquanto os elementos estão bloqueados As mudanças de disposição devem ser aplicadas antes de se poderem fazer outras alterações Disposição: Esquerda Botão Esquerdo Licença: Bloquear os Elementos Maximizar o Painel Meta Botão do Meio Mais Configurações... Acções do Rato OK Alinhamento do Painel Remover o Painel Direita Botão Direito Extremo do Ecrã Procurar... Shift Parar a actividade Actividades paradas: Mudança A configuração do módulo actual foi alterada. Deseja aplicar as alterações ou esquecê-las? Este atalho irá activar a 'applet': irá colocar o teclado em primeiro plano para ele e, caso a 'applet' tenha uma janela (como o menu inicial), a janela será aberta. Topo Desfazer a desinstalação Desinstalar Desinstalar o elemento Deslocamento Vertical Visibilidade Papel de Parede Tipo de Papel de Parede: Elementos Largura As Janelas Podem Cobrir As Janelas Vão Para Baixo 