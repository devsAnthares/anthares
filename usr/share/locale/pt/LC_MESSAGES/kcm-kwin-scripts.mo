��          �      �       0  (   1     Z      q     �     �     �     �     �     �  `     ^   u     �  �  �  -   �     �               ,     H     g  "   y     �  6   �  *   �             
              	                                    *.kwinscript|KWin scripts (*.kwinscript) Configure KWin scripts EMAIL OF TRANSLATORSYour emails Get New Scripts... Import KWin Script Import KWin script... KWin Scripts KWin script configuration NAME OF TRANSLATORSYour names Placeholder is error message returned from the install serviceCannot import selected script.
%1 Placeholder is name of the script that was importedThe script "%1" was successfully imported. Tamás Krutki Project-Id-Version: kcm-kwin-scripts
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-19 03:12+0100
PO-Revision-Date: 2017-10-13 09:29+0100
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POFile-SpellExtra: KWin Krutki Tamás kwinscript
Plural-Forms: nplurals=2; plural=n != 1;
 *.kwinscript|Programas do KWin (*.kwinscript) Configurar os programas do KWin zepires@gmail.com Obter Novos Programas... Importar o Programa do KWin Importar o programa do KWin... Programas do KWin Configuração do programa do KWin José Nuno Pires Não é possível importar o programa seleccionado.
%1 O programa "%1" foi importado com sucesso. Tamás Krutki 