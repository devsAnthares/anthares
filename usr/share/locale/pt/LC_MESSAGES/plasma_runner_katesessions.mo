��          <      \       p   !   q   3   �      �   �  �   2   �  5   �     �                   Finds Kate sessions matching :q:. Lists all the Kate editor sessions in your account. Open Kate Session Project-Id-Version: krunner_katesessions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-18 03:08+0100
PO-Revision-Date: 2009-04-01 08:31+0100
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POFile-SpellExtra: Kate
Plural-Forms: nplurals=2; plural=n != 1;
 Procura as sessões do Kate correspondentes a :q:. Mostra todas as sessões do editor Kate na sua conta. Abrir uma Sessão do Kate 