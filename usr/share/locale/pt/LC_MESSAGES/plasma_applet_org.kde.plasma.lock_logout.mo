��          �      L      �  &   �  +   �       @     	   ]     g     �     �     �     �  (   �     �     �  ,   �               +     7  �  ;  (   �  )        -     3     <     E     ]     b     j     s  ,   �     �     �  9   �  	             %     9        
                          	                                                                  Do you want to suspend to RAM (sleep)? Do you want to suspend to disk (hibernate)? General Heading for list of actions (leave, lock, shutdown, ...)Actions Hibernate Hibernate (suspend to disk) Leave Leave... Lock Lock the screen Logout, turn off or restart the computer No Sleep (suspend to RAM) Start a parallel session as a different user Suspend Switch User Switch user Yes Project-Id-Version: plasma_applet_org
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2015-07-05 11:46+0100
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
 Deseja suspender para a RAM (adormecer)? Deseja suspender para o disco (hibernar)? Geral Acções Hibernar Hibernar (para o disco) Sair Sair... Bloquear Bloquear o ecrã Encerrar, desligar ou reiniciar o computador Não Suspender (para a RAM) Iniciar uma sessão paralela como um utilizador diferente Suspender Mudar de Utilizador Mudar de utilizador Sim 