��    ,      |  ;   �      �  (   �     �  �   �     �     �     �     �     �     �     �     �     �               %     1      J     k     s     �     �     �     �     �     �     �               /     4     I     Y     l     y     �     �      �  7   �                     1     6  �  <     �     	     		     %	  	   :	     D	     L	     h	     o	     	  (   �	     �	     �	     �	     �	     �	     
     
     $
     7
     P
     h
     y
     �
  &   �
     �
     �
     �
     �
          #  "   9     \     k     �     �  &   �  @   �               7  
   K     V     !                 %   +   ,      "   #                               &                  	                           )                             *      '      $      (                                
    %1 is the name of a session%1 (Wayland) ... @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) @title:windowSelect Image Advanced Author Auto &Login Background: Clear Image Commands Could not decompress archive Cursor Theme: Customize theme Default Description Download New SDDM Themes EMAIL OF TRANSLATORSYour emails General Get New Theme Halt Command: Install From File Install a theme. Invalid theme package Load from file... Login screen using the SDDM Maximum UID: Minimum UID: NAME OF TRANSLATORSYour names Name No preview available Reboot Command: Relogin after quit Remove Theme SDDM KDE Config SDDM theme installer Session: The default cursor theme in SDDM The theme to install, must be an existing archive file. Theme Unable to install theme Uninstall a theme. User User: Project-Id-Version: kcm_sddm
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2017-10-17 10:21+0100
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POFile-SpellExtra: SDDM UID Wayland
Plural-Forms: nplurals=2; plural=n != 1;
 %1 (Wayland) ... (Tamanhos disponíveis: %1) Seleccionar a Imagem Avançado Autoria &Autenticação Automática Fundo: Limpar a Imagem Comandos Não foi possível descomprimir o pacote Tema de Cursores: Personalizar o tema Predefinição Descrição Obter Novos Temas do SDDM zepires@gmail.com Geral Obter um Tema Novo Comando de Encerramento: Instalar de um Ficheiro Instala um tema. Pacote de tema inválido Carregar de um ficheiro... Ecrã de autenticação que usa o SDDM UID Máximo: UID Mínimo: José Nuno Pires Nome Nenhuma antevisão disponível Comando de Reinício: Reiniciar a sessão após a saída Remover o Tema Configuração do KDE no SDDM Instalador de temas do SDDM Sessão: O tema de cursores predefinido no SDDM O tema a instalar - deverá ser um ficheiro de pacote existente. Tema Impossível instalar o tema Desinstala um tema. Utilizador Utilizador: 