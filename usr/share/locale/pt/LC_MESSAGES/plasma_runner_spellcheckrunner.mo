��          t      �                 '     6     R     Z     w  Q   �     �      �       �    "   �     �  +   �     '  0   0  ,   a     �     �     �  
   �     
      	                                                 &Require trigger word &Trigger word: Checks the spelling of :q:. Correct Could not find a dictionary. Spell Check Settings Spelling checking runner syntax, first word is trigger word, e.g.  "spell".%1:q: Suggested words: %1 separator for a list of words,  spell Project-Id-Version: krunner_spellcheckrunner
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2016-04-09 11:37+0100
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
 Obriga&r à palavra de activação Palavra de ac&tivação: Verifica a correcção ortográfica de :q:. Correcto Não foi possível encontrar nenhum dicionário. Configuração da Verificação Ortográfica %1:q: Palavras sugeridas: %1 ,  ortografia 