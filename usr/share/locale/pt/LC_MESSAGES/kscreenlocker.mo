��          t      �         :        L     Y     g     {     �     �  2   �  @   �    "  �  $  8   �     3     F     X     q  "   �     �  :   �  9   �  "  ,                      
                      	                Ensuring that the screen gets locked before going to sleep Lock Session Screen Locker Screen lock enabled Screen locked Screen saver timeout Screen unlocked Sets the minutes after which the screen is locked. Sets whether the screen will be locked after the specified time. The screen locker is broken and unlocking is not possible anymore.
In order to unlock switch to a virtual terminal (e.g. Ctrl+Alt+F2),
log in and execute the command:

loginctl unlock-session %1

Afterwards switch back to the running session (Ctrl+Alt+F%2). Project-Id-Version: kscreenlocker
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-03 03:06+0200
PO-Revision-Date: 2017-06-03 14:13+0100
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-POFile-SpellExtra: DPMS unlock loginctl sessions session
 Garante que o ecrã fica bloqueado antes de se suspender Bloquear a Sessão Bloqueio de Ecrã Bloqueio do ecrã activo Ecrã bloqueado Tempo-limite do protector do ecrã Ecrã desbloqueado Define os minutos, após os quais o ecrã será bloqueado. Define se o ecrã será bloqueado após o tempo indicado. O bloqueio de ecrã está com problemas, sendo que não é possível mais desbloquear.
Para poder desbloquear, mude para um terminal virtual (p.ex. Ctrl+Alt+F2),
autentique-se e execute o comando:

loginctl unlock-session %1

Depois disso, volte para a sessão em execução (Ctrl+Alt+F%2). 