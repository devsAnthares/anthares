��          �      |      �     �               9     R     g          �     �     �     �  '     !   +     M     f     ~     �  )   �     �  1   �     "  �  9     �     �     �     �            
        &     -     @     N     [     w  
   �     �     �     �  +   �       +      
   L                                     	      
                                                      @action:buttonAbort @action:buttonIgnore @action:buttonNew Folder... @action:buttonNo to All @action:buttonRetry @action:buttonSave All @action:buttonYes to All @action:inmenuDelete @action:inmenuMove to Trash @action:inmenuNew Folder... @action:inmenuProperties @label:textboxCreate new folder in:
%1 @option:checkShow Hidden Folders @title:windowNew Folder @title:windowOpen File @title:windowSave File @title:windowSelect Folder A file or folder named %1 already exists. Show Hidden Folders You do not have permission to create that folder. folder nameNew Folder Project-Id-Version: plasmaintegration5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-26 05:50+0100
PO-Revision-Date: 2016-01-12 11:24+0000
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
 Interromper Ignorar Nova Pasta... Não a Tudo Repetir Gravar Tudo Sim a Tudo Apagar Enviar para o Lixo Nova Pasta... Propriedades Criar uma pasta nova em:
%1 Mostrar as Pastas Escondidas Nova Pasta Abrir um Ficheiro Gravar o Ficheiro Seleccionar a Pasta Já existe um ficheiro ou pasta chamado %1. Mostrar as Pastas Escondidas Não tem permissões para criar essa pasta. Nova Pasta 