��    O      �  k         �     �  ^   �  H     
   f     q     �     �     �     �  '   �     �  "   �          4     K  
   T     _     x     �  	   �     �     �     �     �     �     �  "   �     	      	  	   8	     B	  !   I	     k	  !   �	  ?   �	     �	     �	     �	     
     
     (
     <
  ;   H
  &   �
      �
  %   �
     �
          &     ?  >   X     �     �  '   �  +   �  !   !     C     c     t     �     �     �     �     �     �     �               +     >     P     b     s     �     �     �     �  3   �  n       �     �     �  
   �     �     �     �     �     �     �     �     	               :     C     P     k     |     �     �     �     �     �     �     �  (   �     
       	          7   &     ^  '   `     �     �     �     �     �     �     �     �                     4     I  
   f  
   q     |     �     �     �     �     �     �     �     �     �     �     �     �               	                                        !     %     )     2     8     P     /       ?       $         K   M   !   )      @                                 2   E       '               1          6              O   <      H   J      8       7      >       =   0   *          .      ,   :      N   D   G   C   3   	   #   +   4                F          %   A                   5       &   ;              9   
           -                      (   "   L      B   I             min %1 is the weather condition, %2 is the temperature,  both come from the weather provider%1 %2 A weather station location and the weather service it comes from%1 (%2) Appearance Beaufort scale bft Celsius °C Degree, unit symbol° Details Fahrenheit °F Forecast period timeframe1 Day %1 Days Hectopascals hPa High & Low temperatureH: %1 L: %2 High temperatureHigh: %1 Inches of Mercury inHg Kelvin K Kilometers Kilometers per Hour km/h Kilopascals kPa Knots kt Location: Low temperatureLow: %1 Meters per Second m/s Miles Miles per Hour mph Millibars mbar N/A No weather stations found for '%1' Notices Percent, measure unit% Pressure: Search Select weather services providers Short for no data available- Show temperature in compact mode: Shown when you have not set a weather providerPlease Configure Temperature: Units Update every: Visibility: Weather Station Wind conditionCalm Wind speed: certain weather condition (probability percentage)%1 (%2%) content of water in airHumidity: %1%2 distance, unitVisibility: %1 %2 ground temperature, unitDewpoint: %1 humidex, unitHumidex: %1 pressure tendencyfalling pressure tendencyrising pressure tendencysteady pressure tendency, rising/falling/steadyPressure Tendency: %1 pressure, unitPressure: %1 %2 temperature, unit%1%2 visibility from distanceVisibility: %1 weather services provider name (id)%1 (%2) weather warningsWarnings Issued: weather watchesWatches Issued: wind directionE wind directionENE wind directionESE wind directionN wind directionNE wind directionNNE wind directionNNW wind directionNW wind directionS wind directionSE wind directionSSE wind directionSSW wind directionSW wind directionVR wind directionW wind directionWNW wind directionWSW wind direction, speed%1 %2 %3 wind speedCalm windchill, unitWindchill: %1 winds exceeding wind speed brieflyWind Gust: %1 %2 Project-Id-Version: plasma_applet_weather
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-28 05:50+0100
PO-Revision-Date: 2018-01-15 09:48+0000
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-POFile-SpellExtra: Mín inHg mb Hectopascais bft mph Fahrenheit kPa
X-POFile-SpellExtra: Quilopascais kt hPa Máx Beaufort Humidex mbar min
X-POFile-SpellExtra: NNO ENE NNE NE VR ESE SO OSO SSE SSO ONO
  min %1 %2 %1 (%2) Aparência Escala de Beaufort bft Centígrados °C ° Detalhes Fahrenheit °F 1 Dia %1 Dias Hectopascais hPa A: %1 B: %2 Máx: %1 Polegadas de Mercúrio inHg Kelvin K Quilómetros Quilómetros por Hora km/h Quilopascais kPa Nós kt Localização: Mín: %1 Metros por Segundo m/s Milhas Milhas por Hora mph Milibares mbar N/D Sem estações meteorológicas para '%1' Avisos % Pressão: Procurar Selecciona os fornecedores de serviços meteorológicos - Mostrar a temperatura no modo compacto: Configure por Favor Temperatura: Unidades Actualizar a cada: Visibilidade: Estação Meteorológica Calmo Velocidade do vento: %1 (%2%) Humidade: %1 %2 Visibilidade: %1 %2 Ponto de orvalho: %1 Índice de calor Humidex: %1 a diminuir a aumentar estável Tendência da Pressão: %1 Pressão: %1 %2 %1 %2 Visibilidade: %1 %1 (%2) Alertas Emitidos: Vigias Emitidas: E ENE ESE N NE NNE NNO NO S SE SSE SSO SO VR O ONO OSO %1 %2 %3 Calmo Sensação térmica: %1 Rajada de Vento: %1 %2 