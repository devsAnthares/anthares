��    	      d      �       �   	   �      �   <   �   5   0     f     �  4   �     �  �  �     p     ~     �     �  .   �     �     �     �                                          	    Ends at 5 General Show the number of the day (eg. 31) in the iconDay in month Show the week number (eg. 50) in the iconWeek number Show week numbers in Calendar Starts at 9 What information is shown in the calendar iconIcon: Working Day Project-Id-Version: plasma_applet_org
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2016-06-16 11:45+0100
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
 Termina às 5 Geral Dia do mês Número da semana Mostrar os números das semanas no Calendário Começa às 9 Ícone: Dia de Trabalho 