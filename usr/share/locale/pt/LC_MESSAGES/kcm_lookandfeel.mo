��          �      |      �     �  2        B     b       #   �      �     �  %   �       
   &     1     >     ]  �   }       ]   (     �  p   �  :        P  �  \  /   �  V   )  4   �     �  !   �     �          &  ?   =  (   }     �     �     �  1   �  �   	     �	  y   �	  0   l
  �   �
  E   A     �                                                                      
                	           Apply a look and feel package Command line tool to apply look and feel packages. Configure Look and Feel details Copyright 2017, Marco Martin Cursor Settings Changed Download New Look And Feel Packages EMAIL OF TRANSLATORSYour emails Get New Looks... List available Look and feel packages Look and feel tool Maintainer Marco Martin NAME OF TRANSLATORSYour names Reset the Plasma Desktop layout Select an overall theme for your workspace (including plasma theme, color scheme, mouse cursor, window and desktop switcher, splash screen, lock screen etc.) Show Preview This module lets you configure the look of the whole workspace with some ready to go presets. Use Desktop Layout from theme Warning: your Plasma Desktop layout will be lost and reset to the default layout provided by the selected theme. You have to restart KDE for cursor changes to take effect. packagename Project-Id-Version: kcm_lookandfeel
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-27 05:46+0100
PO-Revision-Date: 2017-12-20 15:09+0000
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
 Aplicar um pacote de Aparência e Comportamento Ferramenta da linha de comandos para aplicar os pacotes de aparência e comportamento. Configurar os detalhes da Aparência e Comportamento Copyright 2017, Marco Martin Configuração do Cursor Alterada Obter Pacotes com Novos Visuais zepires@gmail.com Obter Novos Visuais... Apresenta os pacotes de Aparência e Comportamento disponíveis Ferramenta de Aparência e Comportamento Manutenção Marco Martin José Nuno Pires Repõe a disposição da Área de Trabalho Plasma Seleccione um tema de aparência e comportamento global para a sua área de trabalho (incluindo o tema do Plasma, esquema de cores, cursor do rato, selector de janelas e ecrãs, ecrã inicial, ecrã de bloqueio, etc.) Mostrar a Antevisão Este módulo permite-lhe configurar a aparência de toda a área de trabalho, com algumas predefinições prontas a usar. Usar a Disposição da Área de Trabalho do tema Atenção: a disposição da sua Área de Trabalho Plasma perder-se-á e será substituída pela disposição que é fornecida por omissão pelo tema seleccionado. Tem de reiniciar o KDE para as alterações do cursor fazerem efeito. nome-pacote 