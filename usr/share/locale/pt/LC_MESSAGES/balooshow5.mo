��          �      \      �     �     �  
   �                :      N     o  
   }     �     �     �  	   �  (   �  \   	     f  2   t     �     �  �  �     �     �  
   �     �     �       )   "     L     a     n  3        �  
   �  @   �  u        �  @   �     �     �     	                                                                                 
             %1 Terms: %2 (c) 2012, Vishesh Handa Baloo Show Device id for the files EMAIL OF TRANSLATORSYour emails File Name Terms: %1 Inode number of the file to show Internal Info Maintainer NAME OF TRANSLATORSYour names No index information found Print internal info Terms: %1 The Baloo data Viewer - A debugging tool The Baloo index could not be opened. Please run "%1" to see if Baloo is enabled and working. The file urls The fileID is not equal to the actual Baloo fileID This is a bug Vishesh Handa Project-Id-Version: balooshow
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-28 03:17+0100
PO-Revision-Date: 2018-01-28 12:17+0000
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POFile-SpellExtra: Handa Vishesh Baloo Show status balooctl node
Plural-Forms: nplurals=2; plural=n != 1;
 Termos do %1: %2 (c) 2012, Vishesh Handa Baloo Show ID do dispositivo dos ficheiros zepires@gmail.com Termos do Nome do Ficheiro: %1 Número de 'i-node' do ficheiro a mostrar Informação Interna Manutenção José Nuno Pires Não foi encontrada nenhuma informação do índice Imprimir a informação interna Termos: %1 O visualizador de dados do Baloo - Uma ferramenta de depuração Não foi possível abrir o índice do Baloo. Execute por favor "%1" para ver se o Baloo está activo e em execução. Os URL's do ficheiro O ID do ficheiro não é igual ao ID do ficheiro actual no Baloo Isto é um erro Vishesh Handa 