��    
      l      �       �      �            	               .  I   3     }  	   �  �  �     V     o     �     �  *   �     �  b   �     5     C               
   	                           &Trigger word: Add Item Alias Alias: Character Runner Config Code Creates Characters from :q: if it is a hexadecimal code or defined alias. Delete Item Hex Code: Project-Id-Version: plasma_runner_charrunner
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2010-05-10 11:33+0100
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POFile-SpellExtra: hex Hex
Plural-Forms: nplurals=2; plural=n != 1;
 Palavra de ac&tivação: Adicionar um Item Alternativa Nome alternativo: Configuração da Execução de Caracteres Código Cria caracteres a partir do :q:, caso seja um código hexadecimal ou um nome alternativo definido. Apagar o Item Código Hex.: 