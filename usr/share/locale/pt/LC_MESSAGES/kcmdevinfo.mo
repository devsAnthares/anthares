��    ]           �      �  !   �       	   !     +  S   4  	   �     �     �     �     �     �     �     �     �     �     	  J   $	     o	     }	     �	      �	  	   �	  
   �	     �	     �	     �	     �	     
     
     
  L   
  	   l
  	   v
  
   �
  
   �
  
   �
     �
     �
     �
     �
     �
     �
     �
     �
     	     (  	   +     5     G     S     [     i     m     }     �     �  
   �  	   �     �  
   �     �     �     �     �     �     	          +     ?     \     r     x     |     �  H   �     �     �     �     �     �       
     &        A  "   T     w     �     �     �     �       	     %  (  ;   N     �  	   �     �     �     �     �     �     �            
   -     8     F     ^     m  >   �     �     �     �        
             +     @     _     w     �     �     �  C   �  	   �  	   �  
   �  
     
               	   .     8     J     a     n     s     �     �  	   �     �     �     �     �     �     �  	                  6  	   D     N     S     _     d     i     z  "   �     �     �     �  "        )     A     G     K     O  c   V     �     �     �     �     �       
        &     3     :  
   A     L     Y     f     s     �     �           B   "       (   W   #   $   %            [   @      L   M   '   6       >   T           <   9   G   &           A      .   8   R       7       J             ?   4                   H   -                      F          5   )   N   D       ,       K             3       U   =   
       ;       O   :           +           !   Z   /      V   Y              Q         E       1       X             ]       0      P         \   I          	          C       S   2       *    
Solid Based Device Viewer Module (c) 2010 David Hubner AMD 3DNow ATI IVEC Available space out of total partition size (percent used)%1 free of %2 (%3% used) Batteries Battery Type:  Bus:  Camera Cameras Charge Status:  Charging Collapse All Compact Flash Reader Default device tooltipA Device Device Information Device Listing Whats ThisShows all the devices that are currently listed. Device Viewer Devices Discharging EMAIL OF TRANSLATORSYour emails Encrypted Expand All File System File System Type:  Fully Charged Hard Disk Drive Hotpluggable? IDE IEEE1394 Info Panel Whats ThisShows information about the currently selected device. Intel MMX Intel SSE Intel SSE2 Intel SSE3 Intel SSE4 Keyboard Keyboard + Mouse Label:  Max Speed:  Memory Stick Reader Mounted At:  Mouse Multimedia Players NAME OF TRANSLATORSYour names No No Charge No data available Not Mounted Not Set Optical Drive PDA Partition Table Primary Processor %1 Processor Number:  Processors Product:  Raid Removable? SATA SCSI SD/MMC Reader Show All Devices Show Relevant Devices Smart Media Reader Storage Drives Supported Drivers:  Supported Instruction Sets:  Supported Protocols:  UDI:  UPS USB UUID:  Udi Whats ThisShows the current device's UDI (Unique Device Identifier) Unknown Drive Unused Vendor:  Volume Space: Volume Usage:  Yes kcmdevinfo name of something is not knownUnknown no device UDINone no instruction set extensionsNone platform storage busPlatform unknown battery typeUnknown unknown deviceUnknown unknown device typeUnknown unknown storage busUnknown unknown volume usageUnknown xD Reader Project-Id-Version: kcm_devinfo
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-23 03:11+0200
PO-Revision-Date: 2016-03-09 14:21+0000
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POFile-IgnoreConsistency: Control
X-POFile-SpellExtra: Compact DNow Flash System kcmdevinfo Memory Máx
X-POFile-SpellExtra: Smart AMD SmartCard Stick Sound AC RAID Hubner Solid
X-POFile-SpellExtra: XD UPS PDA UUID IVEC MMX FireWire SATA SSE UDI Open
X-POFile-SpellExtra: Gateway MMC SD Device Unique Identifier xD AltiVec
Plural-Forms: nplurals=2; plural=n != 1;
X-POFile-IgnoreConsistency: Device Information
X-POFile-SpellExtra: ATI
 
Módulo de Visualização de Dispositivos Baseado no Solid (c) 2010 David Hubner AMD 3DNow ATI IVEC %1 livres em %2 (%3% utilizado) Baterias Tipo de Bateria:  Barramento:  Câmara Máquinas Fotográficas Estado da Carga:  A Carregar Recolher Tudo Leitor de Compact Flash Um Dispositivo Informação do Dispositivo Mostra todos os dispositivos que são apresentados de momento. Visualizador de Dispositivos Dispositivos A Descarregar zepires@gmail.com Encriptado Expandir Tudo Sistema de Ficheiros Tipo de Sistema de Ficheiros:  Completamente Carregada Disco Rígido Acoplável? IDE IEEE1394 Mostra informações acerca do dispositivo seleccionado de momento. Intel MMX Intel SSE Intel SSE2 Intel SSE3 Intel SSE4 Teclado Teclado + Rato Legenda:  Velocidade Máx:  Leitor de Memory Stick Montado Em:  Rato Leitores Multimédia José Nuno Pires Não Sem Carga Sem dados disponíveis Não Montado Não Definido Unidade Óptica PDA Tabela de Partições Primária Processador %1 Número do Processador:  Processadores Produto:  RAID Removível? SATA SCSI Leitor de SD/MMC Mostrar Todos os Dispositivos Mostrar os Dispositivos Relevantes Leitor de Smart Media Unidades de Armazenamento Controladores Suportados:  Tipos de Instruções Suportados:  Protocolos Suportados:  UDI:  UPS USB UUID:  Mostra o UDI (Unique Device Identifier - Identificador de Dispositivo Único) do dispositivo actual Unidade Desconhecida Não utilizado Fabricante:  Espaço do Volume: Utilização do Volume:  Sim kcmdevinfo Desconhecido Nenhum Nenhum Plataforma Desconhecida Desconhecido Desconhecido Desconhecido Desconhecida Leitor de xD 