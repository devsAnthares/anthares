��            )   �      �  !   �  "   �  '   �  ,     #   ;  &   _  !   �  &   �  '   �     �                 V   $  1   {     �  *   �     �        
     
   "     -     6     ;     D     T     [     a     g    p     �     �     �     �     �     �  
   �     �     �     �     
           (  [   /  7   �     �  2   �  !   	     *	     G	     T	  	   b	     l	  	   q	     {	     �	     �	     �	     �	                                                                          
                                                    	           @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Borders @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large Application menu Border si&ze: Buttons Close Close by double clicking:
 To open the menu, keep the button pressed until it appears. Close windows by double clicking &the menu button Context help Drag buttons between here and the titlebar Drop here to remove button Get New Decorations... Keep above Keep below Maximize Menu Minimize On all desktops Search Shade Theme Titlebar Project-Id-Version: kcmkwindecoration
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-04-14 02:49+0200
PO-Revision-Date: 2015-10-06 14:55+0100
Last-Translator: José Nuno Pires <zepires@gmail.com>
Language-Team: pt <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POFile-SpellExtra: kcmkwindecoration Szwed Karol Oxygen main kwin qml
Plural-Forms: nplurals=2; plural=n != 1;
X-POFile-IgnoreConsistency: Oxygen
X-POFile-SpellExtra: Form
 Enorme Grande Sem Contornos Sem Contornos Laterais Normal Ainda Maior Minúsculo Mais do que Enorme Muito Grande Menu da aplicação Tamanho do &contorno: Botões Fechar Feche com um duplo-click:
Para abrir o menu, mantenha o botão carregado até que apareça. Fechar as janelas com um duplo-click no bo&tão do menu Ajuda de contexto Arraste os botões entre a barra de título e aqui Largar aqui para remover o botão Obter Decorações Novas ... Manter acima Manter abaixo Maximizar Menu Minimizar Em todos os ecrãs Procurar Enrolar Tema Barra de título 