��          �      L      �  
   �     �  >   �                  
   -     8     @     H     O  	   [     e     s     z  2   �     �     �  9  �     	
  
   
  H   !
     j
     s
     x
  
   �
     �
     �
  	   �
     �
  
   �
     �
     �
     �
  C   �
     ?     P                                   	                                                         
        &Browse... &Search: *.png *.xpm *.svg *.svgz|Icon Files (*.png *.xpm *.svg *.svgz) Actions All Applications Categories Devices Emblems Emotes Icon Source Mimetypes O&ther icons: Places S&ystem icons: Search interactively for icon names (e.g. folder). Select Icon Status Project-Id-Version: kio4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:11+0100
PO-Revision-Date: 2012-12-12 11:38+0000
Last-Translator: José Nuno Pires <zepires@gmail.com>
Language-Team: pt <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POFile-IgnoreConsistency: Open &File
X-POFile-IgnoreConsistency: Write
X-POFile-IgnoreConsistency: Moving
X-POFile-IgnoreConsistency: Open &File
X-POFile-SpellExtra: kurifiltertest ioslave tempfiles mnt cookies
X-POFile-SpellExtra: fileshareset klauncher KPPP UID KWallet kioslave svgz
X-POFile-SpellExtra: FIXME kfile KSendBugMail FSDevice adr niversal
X-POFile-SpellExtra: esource htm GID Bind POST proxies ocation Listen
X-POFile-IgnoreConsistency: Read
X-POFile-IgnoreConsistency: &Remove Entry
X-POFile-IgnoreConsistency: Open file dialog
X-POFile-SpellExtra: trabal Dispositi es Errno mount umount vold
X-POFile-IgnoreConsistency: Upload
X-POFile-IgnoreConsistency: Sorting
X-POFile-IgnoreConsistency: Comment
Plural-Forms: nplurals=2; plural=n != 1;
X-POFile-SpellExtra: kiometainfo accept ACL Stephan GetActionMenu home
X-POFile-SpellExtra: Kulow Type filesharelist Exec desktop Link usr listen
X-POFile-SpellExtra: Extension floppy Dev fd KMailService DSA MD sbin
X-POFile-SpellExtra: Multipurpose bind DBUS suid RSA dev image AES
X-POFile-IgnoreConsistency: Separate Folders
X-POFile-SpellExtra: Schneier Acme DH DB Auth SHA MAC Kx Lakeridge
X-POFile-SpellExtra: lastUpdate AC notBefore nextUpdate notAfter
X-POFile-SpellExtra: pathlength CRL Maceira Thiago Leupold Staikos mm
X-POFile-SpellExtra: Flash WinCE TextLabel Hartmetz Andreas Peter
X-POFile-SpellExtra: KFileMetaDataReader Penz FindProxyForURL
X-POFile-SpellExtra: FindProxyForURLEx canResume Co
X-POFile-IgnoreConsistency: Status
 Es&colher... &Procurar: *.png *.xpm *.svg *.svgz|Ficheiros de Ícones (*.png *.xpm *.svg *.svgz) Acções Tudo Aplicações Categorias Dispositivos Emblemas Emoções Origem dos Ícones Tipos MIME Ou&tros ícones: Locais Ícones de &sistema: Procurar interactivamente por nomes de ícones (por exemplo pasta). Escolha o Ícone Estado 