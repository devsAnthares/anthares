��          �   %   �      @     A  �   `  m   �  �   P  )   �  `        o     |     �     �     �      �     �     �  '        ,     >     ]     b     s  ?   �  Y   �  +     H   F  �  �     P  �   o  o   �     j	     �	  _   �	      
  !   
     0
     A
      M
     n
     �
     �
  #   �
     �
     �
     �
     �
     	  E     m   ^  >   �  Q                                                    	                                                               
              (c) 2003-2007 Fredrik Höglund <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) @item:inlistbox sizeResolution dependent A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Cursor Theme Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Fredrik Höglund Get new Theme Get new color schemes from the Internet Install from File NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. You have to restart the Plasma session for these changes to take effect. Project-Id-Version: kcmmousetheme
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-02-25 13:57+0000
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POFile-SpellExtra: Höglund Fredrik
Plural-Forms: nplurals=2; plural=n != 1;
 (c) 2003-2007 Fredrik Höglund <qt>Tem a certeza que deseja remover o tema de cursores <i>%1</i>?<br />Isto irá apagar todos os ficheiros instalados por este tema.</qt> <qt>Não poderá apagar o tema que está usar neste momento.<br />Terá de mudar primeiro para outro tema.</qt> (Tamanhos disponíveis: %1) Dependente da resolução Já existe um tema chamado %1 na sua pasta de temas de cursores. Deseja substituí-lo por este? Confirmação Configuração do Cursor Alterada Tema de Cursores Descrição Arraste ou Escreva o URL do Tema zepires@gmail.com Fredrik Höglund Obter um Novo Tema Obter esquemas de cores da Internet Instalar de um Ficheiro José Nuno Pires Nome Substituir o Tema? Remover o Tema O ficheiro %1 não parece ser um pacote de temas de cursores válido. Não foi possível obter o pacote de temas de cursores; verifique por favor se o endereço %1 está correcto. Não foi possível encontrar o pacote de temas de cursores %1. Terá de reiniciar a sessão do Plasma para que estas alterações façam efeito. 