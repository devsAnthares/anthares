��    �      t  �   �
      `     a  	   j     t     �  
   �  	   �  	   �     �     �     �     �  	   �     �     �                     1     @     O     ^     l     |     �     �     �     �     �     �     �     �     �               %     1     =     K     Z     f     u  	   {     �     �     �     �     �     �     �     �     �               .     <     H     U     [     j     r     x     �     �     �     �     �     �     �     �               ,     =     P     d     u     �     �     �     �     �     �     �     �               )     8     I     Y     g     m     {     �     �     �     �     �     �     �     �     �                    .     ;     H     V     b     n  
   z     �  
   �  
   �     �     �  
   �     �     �     �               %     6     G     W     i     y     �     �     �     �     �     �     �     �               *     8     F     V     g     u     �     �     �     �     �     �          '     <     S     k     �     �     �     �     �     �     �          "     5     J     `     s     �  �  �     &/     ./     4/     ;/  
   J/     U/     f/     v/  	   }/  
   �/  
   �/     �/     �/     �/     �/     �/     �/     �/     �/     0     0     %0     20     ?0     K0     Y0     f0     s0     �0     �0     �0     �0     �0     �0     �0     1     1     21     E1     X1     e1     l1     u1     �1     �1     �1     �1  
   �1     �1     �1  
   �1     �1     �1     �1     2  
   2     $2     ,2     ;2     B2  
   H2     S2     \2     e2     v2     �2     �2     �2     �2     �2     �2     �2     �2     3     "3     33     J3     a3     y3     �3     �3     �3     �3     �3     4     4     14     H4  
   X4  
   c4     n4     u4     �4  	   �4  
   �4  
   �4     �4     �4  
   �4  	   �4  
   �4  
   �4  	   �4     �4  
   5  
   5     5     -5     >5     P5     c5     t5     �5     �5     �5     �5     �5     �5     �5  
   �5      6     6     6     (6     76     D6     P6     ]6     j6     v6     �6     �6     �6     �6     �6     �6     �6      7     7     $7     77     I7     ]7     p7     �7     �7     �7     �7     �7     �7     8     8     -8     A8     W8     l8     �8     �8     �8     �8     �8     9     $9     ?9     Z9     t9     �9     �9  
   �9     �9        D   N   }   "   /   X   5   @   p          �   f   ^   ~   ]   l       n   �      -   �   =       d   U       �   �   7   u   i   �                  H   _           �           '   T       �   L   &           9      I   #   �              A   �       O   Q   C         ?   �   $           
   [           v   6   h   �   �   `           S      +   J   �   1   )   \   �   �   Y                 �   �                  B   �   R             V      x       �   �   �   �                 3                      a   �   P   �   <          G   �   �   �          g   �           �   |   k   b   2   (   q          %   �       �   �   �   �           E      �       y            !   c   �   o       �          .   �   w   Z   W   �   M   4   	               F   ,   0   r   t   �      �       j   >   ;                 K   e   �       s       m   �   8               {       z   *   :    Accurate Afternoon Afternoon tea Almost noon Appearance Bold text Breakfast Dinner Early morning Eight o’clock Eleven o’clock Elevenses End of week Evening Five o’clock Five past eight Five past eleven Five past five Five past four Five past nine Five past one Five past seven Five past six Five past ten Five past three Five past twelve Five past two Five to eight Five to eleven Five to five Five to four Five to nine Five to one Five to seven Five to six Five to ten Five to three Five to twelve Five to two Four o’clock Fuzzy Fuzzyness Half past eight Half past eleven Half past five Half past four Half past nine Half past one Half past seven Half past six Half past ten Half past three Half past twelve Half past two Italic text Late evening Lunch Middle of week Morning Night Nine o’clock Noon One o’clock Quarter past eight Quarter past eleven Quarter past five Quarter past four Quarter past nine Quarter past one Quarter past seven Quarter past six Quarter past ten Quarter past three Quarter past twelve Quarter past two Quarter to eight Quarter to eleven Quarter to five Quarter to four Quarter to nine Quarter to one Quarter to seven Quarter to six Quarter to ten Quarter to three Quarter to twelve Quarter to two Second Breakfast Seven o’clock Six o’clock Sleep Start of week Supper Ten o’clock Ten past eight Ten past eleven Ten past five Ten past four Ten past nine Ten past one Ten past seven Ten past six Ten past ten Ten past three Ten past twelve Ten past two Ten to eight Ten to eleven Ten to five Ten to four Ten to nine Ten to one Ten to seven Ten to six Ten to ten Ten to three Ten to twelve Ten to two Three o’clock Twelve o’clock Twenty past eight Twenty past eleven Twenty past five Twenty past four Twenty past nine Twenty past one Twenty past seven Twenty past six Twenty past ten Twenty past three Twenty past twelve Twenty past two Twenty to eight Twenty to eleven Twenty to five Twenty to four Twenty to nine Twenty to one Twenty to seven Twenty to six Twenty to ten Twenty to three Twenty to twelve Twenty to two Twenty-five past eight Twenty-five past eleven Twenty-five past five Twenty-five past four Twenty-five past nine Twenty-five past one Twenty-five past seven Twenty-five past six Twenty-five past ten Twenty-five past three Twenty-five past twelve Twenty-five past two Twenty-five to eight Twenty-five to eleven Twenty-five to five Twenty-five to four Twenty-five to nine Twenty-five to one Twenty-five to seven Twenty-five to six Twenty-five to ten Twenty-five to three Twenty-five to twelve Twenty-five to two Two o’clock Weekend! Project-Id-Version: plasma_applet_fuzzy_clock
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-05 03:13+0100
PO-Revision-Date: 2017-11-05 14:08+0000
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: pt <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POFile-IgnoreConsistency: Appearance
X-POFile-SpellExtra: Brunei Qatar Palau Guam Mayotte Barém Yakutsk
X-POFile-SpellExtra: Pitcairn Bermuda Aruba Niue Marino Nauru Anguilla
X-POFile-SpellExtra: Fiji Irkutsk Kamchatka Daviess Tengarra Sumatra Wayne
X-POFile-SpellExtra: Bailey FI Columbia Aktobe Casey Niamey Skopje Salaam
X-POFile-SpellExtra: GW Kashgar GT GS GR GQ Kigali Yellowknife GY Rioja GF
X-POFile-SpellExtra: GE Melilla Lubumbashi Accra GA GO GN GM Harbin GH
X-POFile-SpellExtra: Uzhgorod Brisbane Edmonton Station Ushuaia Vilnius
X-POFile-SpellExtra: Choibalsan Rep Denver Chisinau Thimphu Nayarit Boise
X-POFile-SpellExtra: Truk Honolulu Amapa Line Kuching Fort ZM SM Rondonia
X-POFile-SpellExtra: Syowa ZA Libreville ZW SA MD MG Labrador MC MM ML MN
X-POFile-SpellExtra: MH MK MU MT MW MV MQ MP MR MY MX GB MZ FR Istanbul
X-POFile-SpellExtra: Cayenne Yekaterinburg Maceió Rép Alberta Hovd FJ FK
X-POFile-SpellExtra: FM VU Tamaulipas Altai Saigão NR St Gur Atikokan
X-POFile-SpellExtra: Aden Yerevan Ittoqqortoormiit SZ SY Cancún Faroe SP
X-POFile-SpellExtra: SV ST SK SJ SH SO SN Hermosillo SL SC SB Halifax SG
X-POFile-SpellExtra: SF SD Vestfold Starke Borneo YE GU Rarotonga Mandan
X-POFile-SpellExtra: YT LB LC GP Dushanbe Alagoas Frances LV LT LU LR LS
X-POFile-SpellExtra: LP LY Dhaka Holme Novosibirsk Urumqi Magadan Atirau
X-POFile-SpellExtra: Riade Tortola GD Winamac Manitoba Morton Yukon
X-POFile-SpellExtra: Monterrey Hills Kwajalein Orda LK Jan Perth Bamako
X-POFile-SpellExtra: Brunswick Simferopol RU RW RS Bangui Monticello
X-POFile-SpellExtra: Malabo Shiprock GI Kerguelen RN RO Spain Bogotá
X-POFile-SpellExtra: Zaporozh Norfolk Lome ye Bay Adak Zaporizhia
X-POFile-SpellExtra: Banguecoque Mogadishu Taipei Mazatlan Pontianak
X-POFile-SpellExtra: Abidjan Goose Funafuti Fakaofo Eucla Harare Chuuk
X-POFile-SpellExtra: Chubut Petersburg Bissau es Ross Moresby Omsk Chatham
X-POFile-SpellExtra: Nunavut Xinjiang EE EG EC IT Chagos Gambier Cabul ET
X-POFile-SpellExtra: Mahe ES ER Aaiun Bali Baku Kyzylorda IM Pangnirtung
X-POFile-SpellExtra: Blanc Chihuahua IO Tocantins Mendoza Amur Lindeman
X-POFile-SpellExtra: Yap Havai Whitehorse Brazzaville Maseru Nusa
X-POFile-SpellExtra: Dickinson Gilbert Tongatapu Mangghystau Lugansk
X-POFile-SpellExtra: Rankin Moncton Kosrae Pike Danmarkshavn Efate
X-POFile-SpellExtra: Makassar Hobart Pituffik Nairobi KG KE Sukhbaatar KH
X-POFile-SpellExtra: KN KR KP KW Volgograd KZ KY Zaporozhye DM DJ Curacao
X-POFile-SpellExtra: Gogebic DF Amman Winnipeg DZ Celebes Crawford Idaho
X-POFile-SpellExtra: Penh Mankistau Marengo Tashkent Vincennes Lucia Nuevo
X-POFile-SpellExtra: Sergipe Addis Guayaquil Chongqing PR DumontDUrville
X-POFile-SpellExtra: Thule Ruténia Adelie Ponapé Tbilisi HT Govi
X-POFile-SpellExtra: Podgorica Johns QA Saskatchewan Menominee Phnom WF PY
X-POFile-SpellExtra: JY Tarawa JP JM JO WS Glace JE Tegucigalpa
X-POFile-SpellExtra: Longyearbyen Wallis Qyzylorda PG Comoro Antananarivo
X-POFile-SpellExtra: Molucas Turk Nicósia River Midway Bahia Svalbard PL
X-POFile-SpellExtra: Paramaribo KI Roraima Rainy Caracas Urais Ouagadougou
X-POFile-SpellExtra: Kzyl Aleutian Grand Ndjamena PS PW PT Ocid Terre
X-POFile-SpellExtra: Oregon PB PA PF Apia PE Baja PK PH PN DST Inuvik PM
X-POFile-SpellExtra: Southampton Sakhalin Dornod Ashgabat Resolute Fuego
X-POFile-SpellExtra: Bujumbura Araguaina Yucatan Dawson Inlet Aqtobe
X-POFile-SpellExtra: Quintana Ongul PEI Holiday Muscat DK Aqtau RE Manaus
X-POFile-SpellExtra: Khartoum Manágua Queensland Asuncion CK Algiers CH
X-POFile-SpellExtra: CN Taiti CL Ababa CB CA CG CF CE Zavkhan CZ CY CX
X-POFile-SpellExtra: Kuala Guernsey CR Vincent CV CT Scoresbysund Sablon
X-POFile-SpellExtra: Vostok Salem RJ Asmara Vancouver Uvs Godthab Dubai
X-POFile-SpellExtra: Pulaski Tallinn Tell Montevideo Noumea Sófia Nassau
X-POFile-SpellExtra: Urville VA VC VE VG IQ Anchorage Atyrau Nipigon VN
X-POFile-SpellExtra: Gaborone McMurdo IL Iron IN Bayan IE Tijuana Jujuy
X-POFile-SpellExtra: Joanesburgo Durango Jayapura Blantyre Enderbury Port
X-POFile-SpellExtra: BD BE BF BG BA BB BM BN BO BH BI BJ BT Rothera BW
X-POFile-SpellExtra: Irian BR BS BY BZ yev Broken Bering Yancowinna
X-POFile-SpellExtra: Monróvia TK Anvers Kampala Island Pohnpei
X-POFile-SpellExtra: Krasnoyarsk TD OM Ulaanbaatar Creek Dubois Louisville
X-POFile-SpellExtra: Udmurtia Windhoek Dakota Douala Perry Vevay Auckland
X-POFile-SpellExtra: HR Thunder Sinaloa Samarkand HK HN Katmandu Dakar New
X-POFile-SpellExtra: Eirunepe Guadalcanal Saipan Baical Wake AE Karachi
X-POFile-SpellExtra: Majuro Kiritimati Freetown Newfoundland Melbourne UY
X-POFile-SpellExtra: Reiquiavique UZ Galápagos of US Mawson Currie UG UA
X-POFile-SpellExtra: Lusaka GG Vientiane Vladivostok Lumpur Ljubljana NI
X-POFile-SpellExtra: NL Kitts NC NE NF NG Sabah NZ NP NQ Bishkek Mariehamn
X-POFile-SpellExtra: Rangoon Gallegos Anadyr Mbabane Dem Howe Johnston HU
X-POFile-SpellExtra: Banjul Atol Jaya Catamarca Leon Olgiy Amundsen Mayen
X-POFile-SpellExtra: CI au Sarawak TZ TW TT TR Yakutat Cuiabá TN CC TL TM
X-POFile-SpellExtra: TJ Tierra TH TF TG Knox Indianápolis Jenissei TC Man
X-POFile-SpellExtra: Tucuman AD AG AF Vaduz Luhansk AM AL AN AQ Minsk AU
X-POFile-SpellExtra: AT AW AX Almaty AZ Iqaluit Campeche Samara Nouakchott
X-POFile-SpellExtra: Pyongyang Juneau Conacri Orient Coahuila
Plural-Forms: nplurals=2; plural=n != 1;
 Preciso Tarde Lanche Quase meio-dia Aparência Texto em negrito Pequeno-Almoço Jantar Madrugada Oito horas Onze horas Almoço das 11 Final da semana Fim de tarde Cinco horas Oito e cinco Onze e cinco Cinco e cinco Quatro e cinco Nove e cinco Uma e cinco Sete e cinco Seis e cinco Dez e cinco Três e cinco Doze e cinco Duas e cinco Cinco para as oito Cinco para as onze Cinco para as cinco Cinco para as quatro Cinco para as nove Cinco para a uma Cinco para as sete Cinco para as seis Cinco para as dez Cinco para as três Cinco para as doze Cinco para as duas Quatro horas Difuso Difusão Oito e meia Onze e meia Cinco e meia Quatro e meia Nove e meia Uma e meia Sete e meia Seis e meia Dez e meia Três e meia Doze e meia Duas e meia Texto em itálico Noite fora Almoço Meio da semana Manhã Noite Nove horas Meio-dia Uma hora Oito e um-quarto Onze e um-quarto Cinco e um-quarto Quatro e um-quarto Nove e um-quarto Uma e um-quarto Sete e um-quarto Seis e um-quarto Dez e um-quarto Três e um-quarto Doze e um-quarto Duas e um-quarto Um-quarto para as oito Um-quarto para as onze Um-quarto para as cinco Um-quarto para as quatro Um-quarto para as nove Um-quarto para a uma Um-quarto para as sete Um-quarto para as seis Um-quarto para as dez Um-quarto para as três Um-quarto para as doze Um-quarto para as duas Segundo-Almoço Sete horas Seis horas Dormir Início da semana Ceia Dez horas Oito e dez Onze e dez Cinco e dez Quatro e dez Nove e dez Uma e dez Sete e dez Seis e dez Dez e dez Três e dez Doze e dez Duas e dez Dez para as oito Dez para as onze Dez para as cinco Dez para as quatro Dez para as nove Dez para a uma Dez para as sete Dez para as seis Dez para as dez Dez para as três Dez para as doze Dez para as duas Três horas Doze horas Oito e vinte Onze e vinte Cinco e vinte Quatro e vinte Nove e vinte Uma e vinte Sete e vinte Seis e vinte Dez e vinte Três e vinte Doze e vinte Duas e vinte Vinte para as oito Vinte para as onze Vinte para as cinco Vinte para as quatro Vinte para as nove Vinte para a uma Vinte para as sete Vinte para as seis Vinte para as dez Vinte para as três Vinte para as doze Vinte para as duas Oito e vinte-e-cinco Onze e vinte-e-cinco Cinco e vinte-e-cinco Quatro e vinte-e-cinco Nove e vinte-e-cinco Uma e vinte-e-cinco Sete e vinte-e-cinco Seis e vinte-e-cinco Dez e vinte-e-cinco Três e vinte-e-cinco Doze e vinte-e-cinco Duas e vinte-e-cinco Vinte-e-cinco para as oito Vinte-e-cinco para as onze Vinte-e-cinco para as cinco Vinte-e-cinco para as quatro Vinte-e-cinco para as nove Vinte-e-cinco para a uma Vinte-e-cinco para as sete Vinte-e-cinco para as seis Vinte-e-cinco para as dez Vinte-e-cinco para as três Vinte-e-cinco para as doze Vinte-cinco para as duas Duas horas Fim-de-semana! 