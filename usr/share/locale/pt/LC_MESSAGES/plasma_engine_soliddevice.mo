��    �      t  �   �      �  
   �  
   �     �     �     �     �     �                         ,     ?     K     O     ]     k     r     w     }     �     �     �     �     �     �     �     �     �     �          &     :     X     `     o     ~     �     �     �     �     �     �     �     �     �     �     �  	   �     �  
   �     �  	          	   $     .     :     K  
   R     ]     m     {     �  
   �     �     �     �  	   �     �     �     �     �     �     �     �     
     !     '  	   -     7     D     J     Z     h     u     |     �     �     �     �  
   �     �     �     �  
   �     �       	        &     .  
   3     >  	   K  
   U     `     e     j     p     u     �     �     �     �     �     �     �     �     �     �     	  	        $     )     :     F     K     S     c     u     |     �     �     �     �  	   �     �     �     �  �  �  
   �     �     �     �     �     �     �     �  	   �     �     �     �       
        %     2     A     H     M     S     [     n  
   �     �     �     �  
   �     �     �     �     �          "     @     H     V     f     n     u     ~     �     �     �     �     �     �     �     �     �     �     �     �  
             (     B     W     t     }     �     �     �     �  
   �     �     �  	   �               $     +     /     8     A     H     [     s  
   {     �     �     �     �     �     �     �     �                     $     3     ;     Q  
   e     p     w     �     �     �     �     �     �  
   �     �     �     �                         &     >     W     o     ~     �     �     �     �     �     �     �     �               $     1     F     a     p     }  
   �     �     �  
   �     �     �     �        ^           	   +                y   A       2       n   i       /       �   :       b   �   g       w             B   j   �   1   L   6   <   �   �   8       K   �   ]   E   0   Q   s              D       V      Y      %   G      !   R       X       l   3   '   >          $               ;   O       m   �   q       P   u       |   �       {          @   W   .       f   a                  T   c       (   ?       4       ~      *       [   r   �          I   )      \   d   J   7   p                  -   N            F   `           �   h   H      "   M   C       5   ,       
          k   }   e           =   x   9   z             &              �   _   �   Z       S   o   t   U                           v       #        Accessible Appendable Audio Available Content BD BD-R BD-RE Battery Blank Block Blu Ray Recordable Blu Ray Rewritable Blu Ray Rom Bus CD Recordable CD Rewritable CD Rom CD-R CD-RW Camera Camera Battery Can Change Frequency Capacity Cdrom Drive Charge Percent Charge State Charging Compact Flash DVD DVD Plus Recordable DVD Plus Recordable Duallayer DVD Plus Rewritable DVD Plus Rewritable Duallayer DVD Ram DVD Recordable DVD Rewritable DVD Rom DVD+DL DVD+DLRW DVD+R DVD+RW DVD-R DVD-RAM DVD-RW Data Description Device Device Types Disc Type Discharging Drive Type Emblems Encrypted Encrypted Container File Path File System File System Type Floppy Free Space Free Space Text Fully Charged HD DVD Recordable HD DVD Rewritable HD DVD Rom HDDVD HDDVD-R HDDVD-RW Hard Disk Hotpluggable Icon Ide Ieee1394 Ignored In Use Keyboard Battery Keyboard Mouse Battery Label Major Max Speed Memory Stick Minor Monitor Battery Mouse Battery Not Charging Number Operation result Optical Drive OpticalDisc Other PDA Battery Parent UDI Partition Table Phone Battery Platform Plugged In Portable Media Player Primary Battery Processor Product Raid Read Speed Rechargeable Removable Rewritable Sata Scsi SdMmc Size Smart Media State Storage Access Storage Drive Storage Volume Super Video CD Supported Drivers Supported Media Supported Protocols Tape Temperature Temperature Unit Timestamp Type Type Description UPS Battery UUID Unknown Unknown Battery Unknown Disc Type Unused Usage Usb Vendor Video Blu Ray Video CD Video DVD Write Speed Write Speeds Xd Project-Id-Version: plasma_engine_soliddevice
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2015-04-04 15:54+0100
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POFile-SpellExtra: Compact Sound RAID Flash System XD Stick Memory Open
X-POFile-SpellExtra: Smart BD DL Blu DVB Máx HD DVR RW RE Sec OSD AD CA
X-POFile-SpellExtra: Regravável Demux UPS PDA UUID Rom FireWire Gravável
X-POFile-SpellExtra: SATA MAC SD Video UDI MMC Ray AC DLRW
Plural-Forms: nplurals=2; plural=n != 1;
 Acessível Adicionável Áudio Conteúdo Disponível BD BD-R BD-RE Bateria Em Branco Bloquear Blu-Ray Gravável Blu-Ray Regravável Blu-Ray ROM Barramento CD Gravável CD Regravável CD-ROM CD-R CD-RW Câmara Bateria da Câmara Pode Mudar a Frequência Capacidade Unidade de CD-Rom Percentagem de Carga Estado da Carga A Carregar Compact Flash DVD DVD+ Gravável DVD+ Gravável Dupla Camada DVD+ Regravável DVD+ Regravável Dupla Camada DVD-RAM DVD Gravável DVD Regravável DVD-ROM DVD+DL DVD+DLRW DVD+R DVD+RW DVD-R DVD-RAM DVD-RW Dados Descrição Dispositivo Tipos de Dispositivo Tipo de Disco A Descarregar Tipo de Unidade Emblemas Encriptado Contentor Encriptado Localização do Ficheiro Sistema de Ficheiros Tipo de Sistema de Ficheiros Disquete Espaço Livre Texto do Espaço Livre Completamente Carregada HD-DVD Gravável HD-DVD Regravável HD-DVD ROM HD-DVD HD-DVD-R HD-DVD-RW Disco Rígido Removível na Hora Ícone IDE IEEE1394 Ignorada Em Uso Bateria do Teclado Bateria do Teclado-Rato Legenda Importante Velocidade Máx Memory Stick Secundário Bateria do Monitor Bateria do Rato Fora de Carga Número Resultado da operação Unidade Óptica Disco Óptico Outro Bateria do PDA UDI-Pai Tabela de Partições Bateria do Telefone Plataforma Ligado Leitor Multimédia Portátil Bateria Primária Processador Produto RAID Velocidade de Leitura Recarregável Removível Regravável SATA SCSI SD-MMC Tamanho Smart Media Estado Acesso de Armazenamento Unidade de Armazenamento Volume de Armazenamento Super Video CD Controladores Suportados Suporte Suportado Protocolos Suportados Fita Magnética Temperatura Unidade de Temperatura Hora Tipo Descrição do Tipo Bateria da UPS UUID Desconhecida Bateria Desconhecida Tipo de Disco Desconhecido Não utilizado Utilização USB Fabricante Blu Ray de Vídeo Video CD DVD Vídeo Velocidade de Gravação Velocidades de Gravação XD 