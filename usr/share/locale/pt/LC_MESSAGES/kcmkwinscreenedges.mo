��    %      D  5   l      @     A     E     G  	   Y     c     |     �     �     �     �     �     �            S   ,  w   �     �  M         [     |  ?   �     �  	   �     �     
     #  %   2     X     e  F   �  #   �     �  ]     R   f     �     �  �  �     �	     �	     �	  	   �	     �	     �	     �	     �	     �	     
     0
  "   H
     k
     �
  g   �
  }   �
     u  G   �     �     �  8   �     3     D     T     l     �  &   �     �     �  [   �  )   6     `  a   ~  J   �     +     >               !      "          #   %                                                                            $                          	                
                            ms % %1 - All Desktops %1 - Cube %1 - Current Application %1 - Current Desktop %1 - Cylinder %1 - Sphere &Reactivation delay: &Switch desktop on edge: Activation &delay: Active Screen Corners and Edges Activity Manager Always Enabled Amount of time required after triggering an action until the next trigger can occur Amount of time required for the mouse cursor to be pushed against the edge of the screen before the action is triggered Application Launcher Change desktop when the mouse cursor is pushed against the edge of the screen EMAIL OF TRANSLATORSYour emails Lock Screen Maximize windows by dragging them to the top edge of the screen NAME OF TRANSLATORSYour names No Action Only When Moving Windows Open krunnerRun Command Other Settings Quarter tiling triggered in the outer Show Desktop Switch desktop on edgeDisabled Tile windows by dragging them to the left or right edges of the screen Toggle alternative window switching Toggle window switching Trigger an action by pushing the mouse cursor against the corresponding screen edge or corner Trigger an action by swiping from the screen edge towards the center of the screen Window Management of the screen Project-Id-Version: kcmkwinscreenedges
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-30 03:13+0100
PO-Revision-Date: 2017-12-30 15:16+0000
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POFile-SpellExtra: ms
Plural-Forms: nplurals=2; plural=n != 1;
  ms % %1 - Todos os Ecrãs %1 - Cubo %1 - Aplicação Actual %1 - Ecrã Actual %1 - Cilindro %1 - Esfera Atraso na &reactivação: Mudar de ecrã no e&xtremo: A&traso na activação: Cantos e Extremos do Ecrã Activos Gestor de Actividades Sempre Activo A quantidade de tempo necessária após despoletar um acção até poder ocorrer a próxima activação A quantidade de tempo necessária para o cursor do rato ser encostado ao extremo do ecrã, antes de a acção ser despoletada Lançador de Aplicações Mudar de ecrã quando o cursor do rato se aproximar do extremo do ecrã zepires@gmail.com Bloquear o Ecrã Maximizar as janelas, arrastando-as para o topo do ecrã José Nuno Pires Nenhuma Acção Só ao Mover as Janelas Executar um Comando Outras Configurações Lado-a-lado em quartos para o exterior Mostrar o Ecrã Desactivado Colocar as janelas lado-a-lado, arrastando-as para os extremos esquerdo ou direito do ecrã Comutar a mudança alternativa de janelas Comutar a mudança de janelas Active uma acção, enviando o cursor do seu rato para o extremo ou canto do ecrã correspondente Activar uma acção, arrastando do extremo do ecrã para o centro do mesmo Gestão de Janelas do ecrã 