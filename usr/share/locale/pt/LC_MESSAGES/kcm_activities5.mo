��    *      l  ;   �      �     �     �  )   �               /     H     ^  #   ~     �  
   �     �     �  %   �  +        E     Z     m  @   z     �     �     �     �               '     ,     9     ?     _     e  .   m     �  F   �  (   �  	   '  	   1  	   ;  '   E  7   m  "   �  �  �     }	     �	  "   �	     �	     �	     �	     �	     �	     	
     #
     7
     C
     ^
  (   v
  .   �
     �
     �
       X        h     �     �     �     �     �     �     �     �  &        (     .  ,   :     g  O   �  (   �                     !     *     7                   $                                 %                      *                                               	   #   )   "             &           
                        '             (   !    &Do not remember @actionWalk through activities @actionWalk through activities (Reverse) @action:buttonApply @action:buttonCancel @action:buttonChange... @action:buttonCreate @title:windowActivity Settings @title:windowCreate a New Activity @title:windowDelete Activity Activities Activity information Activity switching Are you sure you want to delete '%1'? Blacklist all applications not on this list Clear recent history Create activity... Description: Error loading the QML files. Check your installation.
Missing %1 For a&ll applications Forget a day Forget everything Forget the last hour Forget the last two hours General Icon Keep history Name: O&nly for specific applications Other Privacy Private - do not track usage for this activity Remember opened documents: Remember the current virtual desktop for each activity (needs restart) Shortcut for switching to this activity: Shortcuts Switching Wallpaper for in 'keep history for 5 months'for  unit of time. months to keep the history month  months unlimited number of monthsforever Project-Id-Version: kcm_activities
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-03-30 11:29+0100
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-POFile-SpellExtra: QML
 &Não recordar Percorrer as actividades Percorrer as actividades (Inverso) Aplicar Cancelar Modificar... Criar Configuração da Actividade Criar uma Nova Actividade Apagar a Actividade Actividades Informação da actividade Mudança de actividades Tem a certeza que deseja remover a '%1'? Proibir todas as aplicações fora desta lista Limpar o histórico recente Criar uma actividade... Descrição: Ocorreu um erro ao carregar os ficheiros QML. Verifique a sua instalação.
Falta o '%1' Para &todas as aplicações Esquecer um dia Esquecer tudo Esquecer a última hora Esquecer as últimas duas horas Geral Ícone Manter o histórico Nome: Apenas para aplicações &específicas Outra Privacidade Privada - não seguir o uso desta actividade Recordar os documentos abertos: Recordar o ecrã virtual actual para cada actividade (é necessário reiniciar) Atalho de mudança para esta actividade: Atalhos Mudança Papel de Parede durante   mês  meses para sempre 