��          �      \      �     �  �   �  m   r  `   �     A     N     f     s           �     �  '   �     �               %  ?   2  Y   r  +   �  �  �     �  �   �  j   }  \   �     E     U     u     �     �     �     �  +   �     �  	   	     	     ,	  =   ;	  b   y	  *   �	                                                             	                               
       (c) 2003-2007 Fredrik Höglund <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Cursor Theme Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Fredrik Höglund Get new color schemes from the Internet NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. Project-Id-Version: kcminput
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2011-07-08 13:24+0300
Last-Translator: Rūdofls Mazurs <rudolfs.mazurs@gmail.com>
Language-Team: Latvian <lata-l10n@googlegroups.com>
Language: lv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.1
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2);
 (c) 2003-2007 Fredrik Höglund <qt>Vai tiešām vēlaties izmest <i>%1</i> kursoru tēmu?<br />Šī darbība izdzēsīs arī visus failus, kas pieinstalēti no tēmas.</qt> <qt>Jūs nevarat izdzēst šobrīd lietojamo tēmu.<br />Vispirms jums jāpārslēdzas uz citu tēmu.</qt> Tēma ar nosaukumu "%1" jau atrodas jūsu tēmu mapē. Vai vēlaties to aizvietot ar šo te? Apstiprinājums Kursora iestatījumi izmainīti Kursora tēma Apraksts Ievelc vai ieraksti tēmas URL locale@aleksejs.id.lv Fredrik Höglund Iegūt jaunas krāsu saskaņas no interneta Aleksejs Zosims Nosaukums Pārrakstīt tēmu? Izdzēst tēmu Neizskatās, ka fails %1 būtu derīgs kursora tēmu arhīvs. Nespēju lejupielādēt kursora tēmas arhīvu, lūdzu, pārliecinieties, ka adrese %1 ir pareiza. Nespēju atrast kursora tēmas arhīvu %1. 