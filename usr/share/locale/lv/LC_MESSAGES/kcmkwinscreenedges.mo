��          �            x     y     }     �     �     �  S   �  w   !  M   �     �  	   �     �          %     2     R  �  d     (     ,  (   I     r     �  E   �  J   �  E   0     v     �     �     �     �  	   �     �                                              	          
                        ms &Reactivation delay: &Switch desktop on edge: Activation &delay: Always Enabled Amount of time required after triggering an action until the next trigger can occur Amount of time required for the mouse cursor to be pushed against the edge of the screen before the action is triggered Change desktop when the mouse cursor is pushed against the edge of the screen Lock Screen No Action Only When Moving Windows Other Settings Show Desktop Switch desktop on edgeDisabled Window Management Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-30 03:13+0100
PO-Revision-Date: 2011-07-07 23:08+0300
Last-Translator: Rūdofls Mazurs <rudolfs.mazurs@gmail.com>
Language-Team: Latvian <locale@laka.lv>
Language: lv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.1
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2);
  ms At&k. aktivizesanas aizture: Pār&slēgt darbvirsmu pie ekrāna malas Aktivizēšanas aiz&ture: Vienmēr ieslēgts Laiks pēc darbības aktivizēšanas līdz nākamajai aktivizēšanai Cik ilgi peles kursors jāstumj ekrāna stūrī līdz iedarbināt darbību Izmainīt darbvirsmu, kad peles kursors tiek iestumts ekrāna stūrī Bloķēt ekrānu Nav darbības Tikai pārvietojot logus Citi iestatījumi Parādīt darbvirsmu Atslēgts Logu vadība 