��    
      l      �       �      �            	               .  I   3     }  	   �  �  �     O     h     {     �     �     �  W   �          '               
   	                           &Trigger word: Add Item Alias Alias: Character Runner Config Code Creates Characters from :q: if it is a hexadecimal code or defined alias. Delete Item Hex Code: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2012-07-07 17:47+0300
Last-Translator: Einars Sprugis <einars8@gmail.com>
Language-Team: Latvian <locale@laka.lv>
Language: lv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2);
X-Generator: Lokalize 1.4
 &Aktivizēšanas vārds: Pievienot vienību Aizstājvārds Aizstājvārds: Character Runner konfigurācija Kods Izveido rakstzīmes no :q:, ja tas ir heksadecimāls kods vai definēts aizstājvārds. Dzēst vienību Heksadecimālais kods: 