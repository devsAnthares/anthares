��          �      |      �     �  '     "   0     S     m     �  ?   �     �  &   �        %   ?     e  >        �     �  '   �  !        >     ^     }  3   �  �  �     �     �     �     �     �     �     �     �               '     8     G     ^     o     t     �     �     �     �     �                                                                       	   
                       Degree, unit symbol° Forecast period timeframe1 Day %1 Days High & Low temperatureH: %1 L: %2 High temperatureHigh: %1 Low temperatureLow: %1 Short for no data available- Shown when you have not set a weather providerPlease Configure Wind conditionCalm content of water in airHumidity: %1%2 distance, unitVisibility: %1 %2 ground temperature, unitDewpoint: %1 humidex, unitHumidex: %1 pressure tendency, rising/falling/steadyPressure Tendency: %1 pressure, unitPressure: %1 %2 temperature, unit%1%2 visibility from distanceVisibility: %1 weather warningsWarnings Issued: weather watchesWatches Issued: wind direction, speed%1 %2 %3 windchill, unitWindchill: %1 winds exceeding wind speed brieflyWind Gust: %1 %2 Project-Id-Version: plasma_applet_weather
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-28 05:50+0100
PO-Revision-Date: 2010-01-30 20:04+0200
Last-Translator: Viesturs Zarins <viesturs.zarins@mii.lu.lv>
Language-Team: Latvian <locale@laka.lv>
Language: lv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2);
 ° %1 diena %1 dienas %1 dienas A: %1 Z: %2 Maks: %1 Min: %1 - Lūdzu konfigurējiet Mierīgs Mitrums: %1%2 Redzamība: %1 %2 Rasas punkts: %1 Mitrumaind: %1 Spiediena tendence: %1 Spiediens: %1 %2 %1%2 Redzamība: %1 Izsūtīti brīdinājumi: Izsūtītas patruļas: %1 %2 %3 Vēja auksums: %1 Vējš brāzmās: %1 %2 