��          <      \       p      q   *   �   /   �   �  �   &   �  D   �  8                      Cannot find '%1' using %2. Connection to %1 weather server timed out. Weather information retrieval for %1 timed out. Project-Id-Version: libplasmaweather
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-13 03:17+0100
PO-Revision-Date: 2011-08-04 10:31+0300
Last-Translator: Maris Nartiss <maris.kde@gmail.com>
Language-Team: Latvian
Language: lv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2);
X-Generator: KBabel 1.11.4
 Neizdevās atrast '%1' izmantojot '%2. Savienojumam ar laikapstākļu staciju '%1' ir iestājusies noildze. Laikapstākļu informācijas ielādes laiks %1 noildzis. 