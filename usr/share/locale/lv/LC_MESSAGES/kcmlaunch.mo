��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  �  K       #     `  C  m  �  b   
     u
     �
     �
  #   �
     �
     �
  #        '                                          
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2008-07-04 16:06+0300
Last-Translator: Viesturs Zarins <viesturs.zarins@mii.lu.lv>
Language-Team: Latvian <locale@laka.lv>
Language: lv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2);
  sek &Palaišanas paziņošanas noildze: <H1>Paziņojums uzdevumjoslā</H1>
Jūs varat atļaut otru palaišanas paziņojuma metodi, kura
uzdevumjoslā parāda palaistās programmas ielādes simbolu.
Var gadīties, ka programma nenojauš par palaišanas paziņošanu. Šajā gadījumā,
simbols no uzdevumjoslas pazudīs pēc laika, kāds norādīts
sadaļā 'Palaišanas paziņošanas noildze' <h1>Aizņemts kursors</h1>
KDE piedāvā aizņemtu kursoru programmu palaišanas paziņošanai.
Lai ieslēgtu aizņemtu kursoru, atzīmējiet kādu no piedāvātajām iespējām.
Var gadīties, ka programma nenojauš par palaišanas paziņošanu. Šajā gadījumā,
 kursors pārstās mirgot pēc laika, kāds norādīts
sadaļā 'Palaišanas paziņošanas noildze' <h1>Palaišanas paziņošana</h1> Šeit jūs varat konfigurēt programmu palaišanas paziņošanu. Mirgojošs kursors Lēkājošs kursors &Aizņemts kursors Ieslēgt paziņojumu &uzdevumjoslā Nav aizņemta kursora Pasīvs aizņemts kursors Palaišanas paziņošanas &noildze: &Paziņojums uzdevumjoslā 