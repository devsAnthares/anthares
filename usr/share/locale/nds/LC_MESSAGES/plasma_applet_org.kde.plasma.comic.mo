��    /      �  C           ;        U     s     �     �  6   �  A   �     ,     5  $   9  
   ^     i  #   �     �     �     �  7   �          (     D     Q  $   `  ,   �     �     �     �     �     �               +     @     R  �   _  "        >     E  %   W     }     �     �     �  
   �  7   �     	     1	  �  I	  X   �
     S     r     �     �     �     �  
   �     �  )   �            %   <     b     z     �  8   �     �     �            ,   &  .   S     �     �  	   �     �     �     �     �               )  �   B  (   �          &  #   5  
   Y     d     ~     �  
   �     �     �     �     &   "      (                                  !             )   	          +           .      #       '   -                             /         %                            *   ,   
                  $                                  

Choose the previous strip to go to the last cached strip. &Create Comic Book Archive... &Save Comic As... &Strip Number: *.cbz|Comic Book Archive (Zip) @option:check Context menu of comic image&Actual Size @option:check Context menu of comic imageStore current &Position Advanced All An error happened for identifier %1. Appearance Archiving comic failed Automatically update comic plugins: Check for new comic strips: Comic Comic cache: Could not create the archive at the specified location. Create %1 Comic Book Archive Creating Comic Book Archive Destination: Error Handling Failed adding a file to the archive. Failed creating the file with identifier %1. From beginning to ... From end to ... General Getting comic strip failed: Go to Strip Information Jump to &current Strip Jump to &first Strip Jump to Strip ... Manual range Maybe there is no Internet connection.
Maybe the comic plugin is broken.
Another reason might be that there is no comic for this day/number/string, so choosing a different one might work. No zip file is existing, aborting. Range: Strip identifier: The range of comic strips to archive. Update Visit the comic website Visit the shop &website an abbreviation for Number# %1 dd.MM.yyyy here strip means comic strip&Next Tab with a new Strip in a range: from toFrom: in a range: from toTo: Project-Id-Version: plasma_applet_comic
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-07-18 03:20+0200
PO-Revision-Date: 2011-08-18 06:54+0200
Last-Translator: Manfred Wiese <m.j.wiese@web.de>
Language-Team: Low Saxon <kde-i18n-nds@kde.org>
Language: nds
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=2; plural=n != 1;
 

Söök den verleden Comic ut, wenn Du na den tolest twischenspiekert Comic gahn wullt. Comicbookarchiev &opstellen… Comic &sekern as... &Reegnummer: *.cbz|Comicbookarchiev (Zip) &Orginaalgrött Aktuell &Positschoon sekern Verwiedert All Dat hett en Fehler för Beteker %1 geven. Utsehn Comic lett sik nich archiveren. Comic-Modulen automaatsch opfrischen: Na niege Comics kieken: Comic Comic-Twischenspieker: Dat Archiev lett sik nich an de angeven Steed opstellen. %1-Comicbookarchiev opstellen Comicbookarchiev warrt opstellt Teel: Fehlerverarbeiden En Datei lett sik dat Archiev nich tofögen. Opstellen vun Datei mit Beteker "%1" fehlslaan Vun den Anfang bet… Vun't Enn bet… Allgemeen Comic lett sik nich halen: Na Reeg gahn Informatschonen Na &aktuell Reeg jumpen Na &eerst Reeg jumpen Jump na Reeg... Sülven fastleggt Rebeet Villicht steiht Dien Internetverbinnen nich.
Oder dat Comic-Moduul is schaadhaftig.
Oder för disse(n) Dag/Nummer/Tekenkeed gifft dat gor keen Comic, un en anner löppt villicht. Dat gifft keen Zip-Datei, bi to afbreken Rebeet: Comic-Beteker: Rebeet vun Comics för't Archiveren Opfrischen Na de Comic-Nettsiet gahn Na de Inkööp-&Nettsiet gahn Nr. %1 DD.MM.JJJJ &Nakamen Paneel mit nieg Reeg Vun: Bet: 