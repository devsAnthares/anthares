��          �      l      �     �  �      m   �  �   �  `   �     �     �     
          #      :     [  '   l     �     �     �     �  ?   �  Y     +   p  �  �     C  �   b  u   �     _  h   {     �     �     	     	  "   	  "   @	     c	  -   t	     �	     �	     �	     �	  <   �	  ^   )
  2   �
                                    	                        
                                      (c) 2003-2007 Fredrik Höglund <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Cursor Theme Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Fredrik Höglund Get new color schemes from the Internet NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. Project-Id-Version: kcminput
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2014-09-18 15:57+0200
Last-Translator: Sönke Dibbern <s_dibbern@web.de>
Language-Team: Low Saxon <kde-i18n-nds@kde.org>
Language: nds
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.4
Plural-Forms: nplurals=2; plural=n != 1;
 © 2003-2007: Fredrik Höglund <qt>Büst Du seker, wat Du dat Wiesermuster <i>%1</i> wegdoon wullt?<br />Dat warrt all Dateien wegdoon, de dat Muster tohöört.</qt> <qt>Du kannst dat Muster, wat Du opstunns bruukst, nich wegdoon.<br />Du muttst toeerst en anner Muster anmaken.</qt> (Verföögbor Grötten: %1) Dat gifft al en Muster mit den Naam "%1" binnen Dien Lüttbildmuster-Orner. Wullt Du dat överschrieven? Nafraag Ännert Wieserinstellen Wieserutsehn Beschrieven Muster-URL ingeven oder hertrecken s_dibbern@web.de, m.j.wiese@web.de Fredrik Höglund Nieg Klöörschemas ut dat Internet daalladen Sönke Dibbern, Manfred Wiese Naam Muster överschrieven? Muster wegmaken As dat lett gellt de Datei "%1" nich as Wiesermuster-Archiv. Dat Wiesermuster-Archiv lett sik nich daalladen, bitte prööv, wat de Adress "%1" richtig is. Dat Wiesermuster-Archiv "%1" lett sik nich finnen. 