��    /      �  C           6     M   P     �     �     �     �  	   �  A   �  >   "  9   a  9   �  9   �  W     E   g     �     �      �     �  7         :     [  X   p     �     �     �  �   �     �     �     �     �  
   �  
   �     �     	  E  6	     |
     �
     �
  w   �
     8     F     M     T     d  	   �     �  �  �  =   E  i   �     �     �     �          *     6     B     I     ^     d     p     �     �     �  "   �  %   �  5   �  $   1     V  �   l     �     �       �   .     �     �            
   4  
   ?     J     N  >  R      �     �     �  m   �     R     `     g     l  *   }     �     �                           %                     )              -       *       '                       $                +   !   /                                        
         #       "           .   (   ,   	                &            "Full screen repaints" can cause performance problems. "Only when cheap" only prevents tearing for full screen changes like a video. Accurate Always Animation speed: Author: %1
License: %2 Automatic Category of Desktop Effects, used as section headerAccessibility Category of Desktop Effects, used as section headerAppearance Category of Desktop Effects, used as section headerCandy Category of Desktop Effects, used as section headerFocus Category of Desktop Effects, used as section headerTools Category of Desktop Effects, used as section headerVirtual Desktop Switching Animation Category of Desktop Effects, used as section headerWindow Management Configure filter Crisp EMAIL OF TRANSLATORSYour emails Enable compositor on startup Exclude Desktop Effects not supported by the Compositor Exclude internal Desktop Effects Full screen repaints Hint: To find out or configure how to activate an effect, look at the effect's settings. Instant KWin development team Keep window thumbnails: Keeping the window thumbnail always interferes with the minimized state of windows. This can result in windows not suspending their work when minimized. NAME OF TRANSLATORSYour names Never Only for Shown Windows Only when cheap OpenGL 2.0 OpenGL 3.1 OpenGL Platform InterfaceEGL OpenGL Platform InterfaceGLX OpenGL compositing (the default) has crashed KWin in the past.
This was most likely due to a driver bug.
If you think that you have meanwhile upgraded to a stable driver,
you can reset this protection but be aware that this might result in an immediate crash!
Alternatively, you might want to use the XRender backend instead. Re-enable OpenGL detection Re-use screen content Rendering backend: Scale method "Accurate" is not supported by all hardware and can cause performance regressions and rendering artifacts. Scale method: Search Smooth Smooth (slower) Tearing prevention ("vsync"): Very slow XRender Project-Id-Version: kcmkwincompositing
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2014-08-11 12:15+0200
Last-Translator: Sönke Dibbern <s_dibbern@web.de>
Language-Team: Low Saxon <kde-i18n-nds@kde.org>
Language: nds
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.4
Plural-Forms: nplurals=2; plural=n != 1;
 „Heelschirm-Opfrischen“ kann to Leistenproblemen föhren. „Bloots bi siet Opwand“ verhöödt dat Uteneenrieten bloots bi Heelbild-Övergäng, as binnen Videos. Akraat Jümmers Animeergauigkeit: Autor: %1
Verlööfnis: %2 Automaatsch Toganghülp Utsehn Slickerkraam bavento Fokus Warktüüch Animeert Schriefdischwesseln Finsterpleeg Filter instellen Krosch s_dibbern@web.de, m.j.wiese@web.de Tosamensetten bi't Hoochfohren bruken Vun Tosamensetten nich ünnerstütt Effekten utsluten Intern Schriefdischeffekten utsluten Heelschirm-Opfrischen Henwies: Wenn Du rutkriegen wullt, wodennig Du en Effekt anmaken kannst oder wenn Du em instellen wullt, bekiek Di em sien Optschonen. Fuurts KWin-Schrieverslüüd Finster-Vöransichten wohren: Wohrst Du de Finstervöransicht jümmers, funkscheneert dat nich goot, wenn de Finstern minimeert sünd. Finstern sett ehr Arbeit denn villicht nich ut, wenn Du ehr minimeerst. Sönke Dibbern, Manfred Wiese Nienich Bloots bi wiest Finstern Bloots bi siet Opwand OpenGL 2.0 OpenGL 3.1 EGL GLX OpenGL-Tosamensetten hett KWin al maal afstörten laten.
Dat liggt tomehrst an Fehlers binnen en Driever.
Glöövst Du, Du bruukst nu en stabil Driever, kannst Du disse Schuul
nu torüchsetten, man denk dor bitte an: Dat mag direktemang to en nieg Afstört föhren!
Anners kannst Du ok XRender as Hülpprogramm bruken. "OpenGL-Opdecken" wedder anmaken Schirminholt wedderbruken Dorstell-Hülpprogramm: De Topassmetood „Akkraat“ löppt nich op elkeen Hardware un mag to Leisten- un Dorstellproblemen föhren. Topassmetood: Söken Week Week (langsamer) Verhöden vun Uteneenrieten („vsync“): Bannig langsam XRender 