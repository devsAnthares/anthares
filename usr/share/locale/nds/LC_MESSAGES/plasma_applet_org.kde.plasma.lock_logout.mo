��          �      <      �  &   �  +   �       	             2     8     A     F  (   V          �  ,   �     �     �     �     �  �  �  /   �  0   �  	   �     	          *  
   3     >     G  /   [     �     �  )   �     �     �     �     �        	      
                                                                                       Do you want to suspend to RAM (sleep)? Do you want to suspend to disk (hibernate)? General Hibernate Hibernate (suspend to disk) Leave Leave... Lock Lock the screen Logout, turn off or restart the computer No Sleep (suspend to RAM) Start a parallel session as a different user Suspend Switch User Switch user Yes Project-Id-Version: plasma_applet_lockout
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2014-08-11 12:30+0200
Last-Translator: Sönke Dibbern <s_dibbern@web.de>
Language-Team: Low Saxon <kde-i18n-nds@kde.org>
Language: nds
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.4
Plural-Forms: nplurals=2; plural=n != 1;
 Wullt Du den Reekner inslapen laten (in't RAM)? Wullt Du den Reekner infreren (op de Fastplaat)? Allgemeen Infreren Infreren (op Fastplaat) Verlaten Weggahn... Afsluten Den Schirm afsluten Afmellen, den Reekner utmaken oder nieg starten Nee Slapen (in't RAM) En nieg Törn as en anner Bruker anfangen Utsetten Bruker wesseln Bruker wesseln Jo 