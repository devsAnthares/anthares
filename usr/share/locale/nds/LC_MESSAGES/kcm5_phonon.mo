��    B      ,  Y   <      �  i   �  m        y  b   �     �     	  6        O  7   _     �     �  	   �     �  (   �  )   �  )   (     R     o  Y   u     �     �  �   �      y	  .   �	     �	  
   �	     �	     �	     
     
     !
     5
     B
     J
     c
     r
     w
     �
     �
     �
     �
     �
  	   �
  
   �
     �
     �
  	     
     
   *     5     B  	   `     j     o  
   �  �   �     (  8   8  �   q     �  8   	  ,   B  ,   o  %   �     �  �  �  @   �  l   �     3  N   Q     �     �  2   �     �  1        =     K  	   Y     c  +   �  )   �  )   �  "        $  W   3     �     �  a   �  "     C   :     ~     �     �     �     �     �     �                    7     F     K     i     �     �     �     �     �     �     �     �  
   �       
          $   *  
   O     Z  !   _     �  �   �       =   +  |   i     �  2   �  9   '  :   a     �     �     $   !   %          ;   ,          B                 8   #                 7   9       +              )   ?   -   <         6                                    *   2       >                1       @       .   5   3   :   A         &      /                4              0      "             =   (   '         	   
           @info User changed Phonon backendTo apply the backend change you will have to log out and back in again. A list of Phonon Backends found on your system.  The order here determines the order Phonon will use them in. Apply Device List To... Apply the currently shown device preference list to the following other audio playback categories: Audio Hardware Setup Audio Playback Audio Playback Device Preference for the '%1' Category Audio Recording Audio Recording Device Preference for the '%1' Category Backend Colin Guthrie Connector Copyright 2006 Matthias Kretz Default Audio Playback Device Preference Default Audio Recording Device Preference Default Video Recording Device Preference Default/Unspecified Category Defer Defines the default ordering of devices which can be overridden by individual categories. Device Configuration Device Preference Devices found on your system, suitable for the selected category.  Choose the device that you wish to be used by the applications. EMAIL OF TRANSLATORSYour emails Failed to set the selected audio output device Front Center Front Left Front Left of Center Front Right Front Right of Center Hardware Independent Devices Input Levels Invalid KDE Audio Hardware Setup Matthias Kretz Mono NAME OF TRANSLATORSYour names Phonon Configuration Module Playback (%1) Prefer Profile Rear Center Rear Left Rear Right Recording (%1) Show advanced devices Side Left Side Right Sound Card Sound Device Speaker Placement and Testing Subwoofer Test Test the selected device Testing %1 The order determines the preference of the devices. If for some reason the first device cannot be used Phonon will try to use the second, and so on. Unknown Channel Use the currently shown device list for more categories. Various categories of media use cases.  For each category, you may choose what device you prefer to be used by the Phonon applications. Video Recording Video Recording Device Preference for the '%1' Category  Your backend may not support audio recording Your backend may not support video recording no preference for the selected device prefer the selected device Project-Id-Version: kcm_phonon
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2012-03-17 07:30+0100
Last-Translator: Manfred Wiese <m.j.wiese@web.de>
Language-Team: Low Saxon <kde-i18n-nds@kde.org>
Language: nds
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=2; plural=n != 1;
 Wullt Du de Ännern bruken, muttst Du Di af- un wedder anmellen. De List vun Hülpprogrammen för Phonon op Dien Systeem. Phonon bruukt de Programmen in de hier wieste Reeg. Reedschaplist bruken för ... De opstunns wieste Reedschapreeg ok för disse Klangafspeel-Kategorien bruken: Klangreedschappen instellen Klangafspelen Klangafspeelreedschap-Reeg för de Kategorie "%1": Klangopnehmen Klangopnehmreedschap-Reeg för de Kategorie "%1": Hülpprogramm Colin Guthrie Verbinnen Copyright 2006 Matthias Kretz Standardreeg för Klangafspeel-Reedschappen Standardreeg för Klangopnehmreedschappen Standardreeg för Videoopnehmreedschappen Standard- / Nich angeven Kategorie Torüchtrecken Leggt de Standardreeg för Reedschappen fast, enkel Kategorien köönt dat anners maken Reedschap instellen Vörtrocken Reedschap Reedschappen op Dien Systeem. Du kannst de Reedschap utsöken, de de Programmen bruken schöölt. s_dibbern@web.de, m.j.wiese@web.de De utsöchte Reedschap för de Klangutgaav lett sik nich inrichten. Vörn Merrn Vörn links Vörn links vun de Merrn af Vörn rechts Vörn rechts vun de Merrn af Reedschappen Nich-afhangige Reedschappen Ingaavstopen Leeg KDE-Klangreedschappen instellen Matthias Kretz Mono Sönke Dibbern, Manfred Wiese Phonon-Instellenmoduul Afspelen (%1) Vörtrecken Profil Achtern Merrn Achtern links Achtern rechts Opnehmen (%1) Reedschappen verwiedert wiesen Linke Siet Rechte Siet Klangkoort Klangreedschap Luutsprekers platzeren un utproberen Deeptöner Test De utsöchte Reedschap utproberen %1 warrt utprobeert Phonon bruukt disse Reedschappen in disse Reeg. Lett sik de eerste Reedschap nich bruken, versöcht Phonon dat mit de twete, usw. Nich begäng Kanaal De opstunns wieste Reedschapreeg för mehr Kategorien bruken. Verscheden Kategorien vun Medienbruukbispillen. Du kannst för elk Kategorie de Reedschap utsöken, de Phonon bruken schall. Videoopnehmen Videoopnehmreedschap-Reeg för de Kategorie "%1":  Dien Hülpprogramm ünnerstütt villicht keen Klangopnahm Dien Hülpprogramm ünnerstütt villicht keen Video-Opnahm keen vörtrocken Reedschap utsöcht Reedschap vörtrecken 