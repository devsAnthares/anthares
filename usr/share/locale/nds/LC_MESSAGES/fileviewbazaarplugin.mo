��    !      $  /   ,      �  .   �  1     9   J     �  -   �  &   �  )   �  .   #  &   R  )   y  .   �  &   �  )   �  2   #  5   V  =   �  #   �     �  !     '   /  "   W  0   z  '   �  *   �     �          7     R     j     �     �  &   �  �  �  -   �	  -   �	  6   �	     #
  )   :
     d
      |
  &   �
     �
  !   �
  '         .  "   O  '   r  /   �  8   �          #     3  &   G      n  -   �     �  !   �     �          "     3     D     X     k                                                                   	                                
                                    !                             @info:statusAdded files to Bazaar repository. @info:statusAdding files to Bazaar repository... @info:statusAdding of files to Bazaar repository failed. @info:statusBazaar Log closed. @info:statusCommit of Bazaar changes failed. @info:statusCommitted Bazaar changes. @info:statusCommitting Bazaar changes... @info:statusPull of Bazaar repository failed. @info:statusPulled Bazaar repository. @info:statusPulling Bazaar repository... @info:statusPush of Bazaar repository failed. @info:statusPushed Bazaar repository. @info:statusPushing Bazaar repository... @info:statusRemoved files from Bazaar repository. @info:statusRemoving files from Bazaar repository... @info:statusRemoving of files from Bazaar repository failed. @info:statusReview Changes failed. @info:statusReviewed Changes. @info:statusReviewing Changes... @info:statusRunning Bazaar Log failed. @info:statusRunning Bazaar Log... @info:statusUpdate of Bazaar repository failed. @info:statusUpdated Bazaar repository. @info:statusUpdating Bazaar repository... @item:inmenuBazaar Add... @item:inmenuBazaar Commit... @item:inmenuBazaar Delete @item:inmenuBazaar Log @item:inmenuBazaar Pull @item:inmenuBazaar Push @item:inmenuBazaar Update @item:inmenuShow Local Bazaar Changes Project-Id-Version: fileviewbazaarplugin
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2011-10-03 13:00+0200
Last-Translator: Manfred Wiese <m.j.wiese@web.de>
Language-Team: Low Saxon <kde-i18n-nds@kde.org>
Language: nds
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=2; plural=n != 1;
 Dateien wöörn dat Bazaar-Archiev toföögt. Dateien warrt dat Bazaar-Archiev toföögt... Tofögen vun Dateien na't Bazaar-Archiev is fehlslaan. Bazaar-Logbook tomaakt Inspelen vun Bazaar-Ännern is fehlslaan. Bazaar-Ännern inspeelt Bazaar-Ännern warrt inspeelt... Bazaar-Archiev lett sik nich daalladen Bazaar-Archiev wöör daallaadt Bazaar-Archiev warrt daallaadt… Bazaar-Archiev lett sik nich hoochladen Bazaar-Archiev wöör hoochlaadt Bazaar-Archiev warrt hoochlaadt… Dateien ut dat Bazaar-Archiev wegmaakt. Dateien warrt ut dat Bazaar-Archiev wegmaakt... Wegmaken vun Dateien ut dat Bazaar-Archiev is fehlslaan. Ännern laat sik nich nakieken. Ännern nakeken Ännern nakieken… Utföhren vun Bazaar-Logbook fehlslaan Bazaar-Logbook warrt utföhrt... Opfrischen vun't Bazaar-Archiev is fehlslaan. Bazaar-Archiev opfrischt Bazaar-Archiev warrt opfrischt... Bazaar - Tofögen... Bazaar - Inspelen... Bazaar - Wegdoon Bazaar - Logbook Bazaar - Hoochladen Bazaar - Daalladen Bazaar - Opfrischen Lokaal Bazaar-Ännern wiesen 