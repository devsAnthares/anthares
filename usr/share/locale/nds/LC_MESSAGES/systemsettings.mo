��    *      l  ;   �      �  A   �     �  /        2     ;     O     a     w     �     �  	   �     �  3   �  	   �     �      �  $      *   E     p  	   u  5        �     �  
   �     �          $  5   3  6   i     �  ,   �  /   �     	        O   0  Z   �  b   �  	   >  
   H  P   S     �  �  �  6   `
     �
  2   �
     �
  !   �
  #     %   ;     a     r     x  	   �     �  +   �  	   �     �     �  %   �          "     (  ,   :  "   g     �     �     �     �     �  A   �  1        P  )   `     �  (   �     �  Z   �  ]   &  b   �     �     �  W   �     W            )             &      $   	   %         '                                                !      
       *                     "                                                 (                 #    %1 is an external application and has been automatically launched (c) 2009, Ben Cooksley <i>Contains 1 item</i> <i>Contains %1 items</i> About %1 About Active Module About Active View About System Settings Apply Settings Author Ben Cooksley Configure Configure your system Determines whether detailed tooltips should be used Developer Dialog EMAIL OF TRANSLATORSYour emails Expand the first level automatically General config for System SettingsGeneral Help Icon View Internal module representation, internal module model Internal name for the view used Keyboard Shortcut: %1 Maintainer Mathias Soeken NAME OF TRANSLATORSYour names No views found Provides a categorized icons view of control modules. Provides a classic tree-based view of control modules. Relaunch %1 Reset all current changes to previous values Search through a list of control modulesSearch Show detailed tooltips System Settings System Settings was unable to find any views, and hence has nothing to display. System Settings was unable to find any views, and hence nothing is available to configure. The settings of the current module have changed.
Do you want to apply the changes or discard them? Tree View View Style Welcome to "System Settings", a central place to configure your computer system. Will Stephenson Project-Id-Version: systemsettings
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:08+0100
PO-Revision-Date: 2011-06-22 06:21+0200
Last-Translator: Manfred Wiese <m.j.wiese@web.de>
Language-Team: Low Saxon <kde-i18n-nds@kde.org>
Language: nds
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.0
 %1 is en extern Programm un wöör automaatsch opropen © 2009: Ben Cooksley <i>Bargt een Element</i> <i>Bargt %1 Elementen</i> Vertell wat över %1 Vertell wat över't aktive Moduul Vertell wat över de aktive Ansicht Vertell wat över de Systeeminstellen Instellen bruken Autor Ben Cooksley Instellen Stell Dien Systeem in Leggt fast, wat Kortinfos Enkelheiten wiest Schriever Dialoog s_dibbern@web.de De eerste Evene automaatsch utfoolden Allmeen Hülp Lüttbild-Ansicht Intern Moduuldorstellen, intern Moduulmodell Intern Naam vun de bruukte Ansicht Tastkombinatschoon: %1 Pleger Mathias Soeken Sönke Dibbern Keen Ansichten funnen Wiest de Kuntrullmodulen as Lüttbildansicht, na Kategorien ornt. Wiest de Kuntrullmodulen as klass'sch Boomansicht %1 nieg starten All Ännern op lest Weerten torüchsetten Söken Utföhrliche Knoopinformatschonen wiesen Systeeminstellen För de Systeeminstellen laat sik keen Ansichten finnen, un dorüm lett sik ok nix wiesen. För de Systeeminstellen laat sik keen Ansichten finnen, un dorüm lett sik ok nix instellen. De Instellen binnen dat aktive Moduul wöörn ännert.
Wullt Du de Ännern bruken oder wegsmieten? Boomansicht Ansichtstil Willkamen bi de Systeeminstellen, een zentraal Steed för't Instellen vun Dien Systeem. Will Stephenson 