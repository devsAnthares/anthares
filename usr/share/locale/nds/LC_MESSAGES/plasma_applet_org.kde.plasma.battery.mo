��          �   %   �      @  9   A     {     �     �  7   �  	   �     �               $     <     J     R     f     m     �  #   �  %   �     �  5   �     .     =     K  )   S  �  }     1     C     Y     j     �     �     �     �     �     �  
   �  	   �     �     �          "     3     7     ;  >   R     �     �  	   �     �            
                                                              	                                                    %1 is remaining time, %2 is percentage%1 Remaining (%2%) %1% Battery Remaining %1%. Charging &Configure Power Saving... Battery is currently not present in the bayNot present Capacity: Charging Discharging Display Brightness Enable Power Management Fully Charged General Keyboard Brightness Model: No Batteries Available Not Charging Placeholder is battery capacity%1% Placeholder is battery percentage%1% Power management is disabled The battery applet has enabled system-wide inhibition Time To Empty: Time To Full: Vendor: battery percentage below battery icon%1% Project-Id-Version: plasma_applet_battery
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-03 03:15+0100
PO-Revision-Date: 2014-09-18 16:09+0200
Last-Translator: Sönke Dibbern <s_dibbern@web.de>
Language-Team: Low Saxon <kde-i18n-nds@kde.org>
Language: nds
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.4
Plural-Forms: nplurals=2; plural=n != 1;
 %1 is över (%2%) %1% Batterie is över %1%. Bi to laden Stroomsporen &instellen... nich dor Grött: Bi to laden Bi to utladen Schirmhelligkeit Stroompleeg anmaken Heel laadt Allgemeen Tastatuurhelligkeit Modell: Keen Batterien verföögbor Nich bi to laden %1% %1% Stroompleeg is utmaakt Dat Batterie-Lüttprogramm hett Blockeren systeemwiet anmaakt. Resttiet bet Leddig: Resttiet bet Vull: Leverant: %1% 