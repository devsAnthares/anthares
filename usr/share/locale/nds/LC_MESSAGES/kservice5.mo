��          �   %   �      P     Q  /   e     �     �  6   �  N      E   O  W   �  @   �     .     G      d  !   �  %   �  )   �      �  c     -   |  D   �     �       !   )  B   K  @   �     �  �  �     �  ,   �     �     �     �  .   �  2   ,	  ?   _	  '   �	  $   �	  +   �	  +   
  (   D
  /   m
  /   �
  "   �
  k   �
  3   \  X   �     �  "        *  [   D  9   �     �                                                              
                                                  	             @info:creditAuthor @info:creditCopyright 1999-2014 KDE Developers @info:creditDavid Faure @info:creditWaldo Bastian @info:shell command-line optionCreate global database @info:shell command-line optionDisable incremental update, re-read everything @info:shell command-line optionPerform menu generation test run only @info:shell command-line optionSwitch QStandardPaths to test mode, for unit tests only @info:shell command-line optionTrack menu id for debug purposes Could not launch Browser Could not launch Mail Client Could not launch Terminal Client Could not launch the browser:

%1 Could not launch the mail client:

%1 Could not launch the terminal client:

%1 EMAIL OF TRANSLATORSYour emails Error launching %1. Either KLauncher is not running anymore, or it failed to start the application. Function must be called from the main thread. KLauncher could not be reached via D-Bus. Error when calling %1:
%2
 NAME OF TRANSLATORSYour names No service implementing %1 The provided service is not valid The service '%1' provides no library or the Library key is missing application descriptionRebuilds the system configuration cache. application nameKBuildSycoca Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-02-07 08:40+0100
PO-Revision-Date: 2014-05-11 00:55+0200
Last-Translator: Sönke Dibbern <s_dibbern@web.de>
Language-Team: Low Saxon <kde-i18n-nds@kde.org>
Language: nds
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.4
Plural-Forms: nplurals=2; plural=n != 1;
 Autor Copyright 1999-2014, de KDE-Schrieverslüüd David Faure Waldo Bastian Globale Datenbank opstellen Verscheelopfrischen utmaken, allens nieg lesen Bloots versöken, wat sik dat Menü opstellen lett QStandardPaths op Testbedrief ännern, bloots för Eenheittests Menü-ID för de Fehlersöök verfolgen De Nettkieker lett sik nich starten. Dat Nettpostprogramm lett sik nich starten. Dat Terminalprogramm lett sik nich starten. De Nettkieker lett sik nich starten:

%1 Dat Nettpostprogramm lett sik nich starten:

%1 Dat Terminalprogramm lett sik nich starten:

%1 s_dibbern@web.de, m.j.wiese@web.de Fehler bi't Opropen vun „%1“. KLauncher löppt wull nich mehr, oder dat kann dat Programm nich opropen. Funkschoon mutt vun den Hööftstrang opropen warrn KLauncher lett sik nich över den D-Bus ansnacken. Fehler bi't Opropen vun „%1“:
%2
 Sönke Dibbern, Manfred Wiese Keen Deenst stellt „%1“ praat. De angeven Deenst is leeg De Deenst „%1“ stellt keen Bibliotheek praat, oder dat gifft keen „Library“-Indrag. Stellt den Twischenspieker för Systeeminstellen nieg op. KBuildSycoca 