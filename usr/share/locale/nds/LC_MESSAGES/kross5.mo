��    $      <  5   \      0     1  +   E     q  4   �  &   �     �     �                     5     :     P     X  ,   u  3   �     �     �               !  '   .     V     u     {     �     �     �     �     �     �  &   �       B        b  �  y           &     E  #   U     y     �     �  	   �  
   �  "   �     �     �     	     	  <   -	  :   j	  (   �	  +   �	     �	  
   
     
  '   
     A
     _
     e
  0   �
     �
     �
     �
  '   �
     	          0  >   6     u                                       
                              !                                  $      #                               	                           "             @info:creditAuthor @info:creditCopyright 2006 Sebastian Sauer @info:creditSebastian Sauer @info:shell command-line argumentThe script to run. @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: application descriptionCommand-line utility to run Kross scripts. application nameKross Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2014-05-11 00:55+0200
Last-Translator: Sönke Dibbern <s_dibbern@web.de>
Language-Team: Low Saxon <kde-i18n-nds@kde.org>
Language: nds
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.4
Plural-Forms: nplurals=2; plural=n != 1;
 Autor Copyright 2006 Sebastian Sauer Sebastian Sauer Dat Skript, dat Du utföhren wullt. Allmeen En nieg Skript tofögen Tofögen... Afbreken? Kommentar: s_dibbern@web.de, m.j.wiese@web.de Bewerken Dat utsöchte Skript bewerken Bewerken... Dat utsöchte Skript opropen För den Interpreter „%1“ lett sik keen Skript opstellen Interpreter för't Skript „%1“ lett sik nich fastslaan Interpreter „%1“ lett sik nich laden Skriptdatei „%1“ lett sik nich opmaken. Datei: Lüttbild: Interpreter: Sekerheitstoop vun den Ruby-Interpreter Sönke Dibbern, Manfred Wiese Naam: Keen Funkschoon „%1“ funnen Dat gifft keen Interpreter mit den Naam „%1“ Wegmaken Utsöcht Skript wegmaken. Opropen Dat gifft de Skriptdatei „%1“ nich. Anhollen Dat utsöchte Skript anhollen Text: Befehlsreegprogramm, mit dat sik Kross-Skripten utföhren laat Kross 