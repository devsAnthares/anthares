��    
      l      �       �      �            	               .  I   3     }  	   �  �  �     N     ^     n     t     {     �  Q   �     �     �               
   	                           &Trigger word: Add Item Alias Alias: Character Runner Config Code Creates Characters from :q: if it is a hexadecimal code or defined alias. Delete Item Hex Code: Project-Id-Version: plasma_runner_CharacterRunner
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2010-08-02 05:41+0200
Last-Translator: Manfred Wiese <m.j.wiese@web.de>
Language-Team: Low Saxon <kde-i18n-nds@kde.org>
Language: nds
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.1
Plural-Forms: nplurals=2; plural=n != 1;
 &Utlööswoort: Indrag tofögen Alias Alias: Tekendreger instellen Kode Stellt Tekens ut :q: op, wenn dat en Hexadezimaalkode oder en fastleggt Alias is. Indrag wegmaken Hexadezimaal-Kode: 