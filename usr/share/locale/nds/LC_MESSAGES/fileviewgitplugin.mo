��    2      �  C   <      H     I     a     w     �     �  A   �  9   �  .   5  F   d  :   �  D   �  $   +  W   P  7   �  3   �  (     4   =  .   r  C   �  3   �  k        �     �     �     �  #   �     	     5	     N	  !   n	     �	  "   �	     �	     �	     �	     
     -
     C
     _
     t
  (   �
  +   �
  5   �
  3     7   P  1   �  1   �     �     �  �       �     �     �  	   �  
   �     �  '   �  )     ,   I  (   v  <   �     �  L   �  +   C     o  (   �  -   �  3   �  >     *   Y     �     �     �     �     �     �     �     �     �     �  
                  &  	   /     9     @     H  
   X     c     {     �  &   �  '   �  0   �  (     )   .     X     h     )             0         -                #              2      ,                    %      .   /   
                        1   	          &   *       '                                                "   +             $   (      !       @action:buttonCheckout @action:buttonCommit @action:buttonCreate Tag @action:buttonPull @action:buttonPush @action:button Add Signed-Off line to the message widgetSign off @info:tooltipA branch with the name '%1' already exists. @info:tooltipA tag named '%1' already exists. @info:tooltipAdd Signed-off-by line at the end of the commit message. @info:tooltipBranch names may not contain any whitespace. @info:tooltipCreate a new branch based on a selected branch or tag. @info:tooltipDiscard local changes. @info:tooltipProceed even if the remote branch is not an ancestor of the local branch. @info:tooltipTag names may not contain any whitespace. @info:tooltipThere are no tags in this repository. @info:tooltipThere is nothing to amend. @info:tooltipYou must enter a commit message first. @info:tooltipYou must enter a tag name first. @info:tooltipYou must enter a valid name for the new branch first. @info:tooltipYou must select a valid branch first. @item:intext Prepended to the current branch name to get the default name for a newly created branchbranch @label:listboxBranch: @label:listboxLocal Branch: @label:listboxRemote Branch: @label:listboxRemote branch: @label:listbox a git remoteRemote: @label:textboxTag Message: @label:textboxTag Name: @option:checkAmend last commit @option:checkCreate New Branch:  @option:checkForce @option:radio Git CheckoutBranch: @option:radio Git CheckoutTag: @title:groupAttach to @title:groupBranch Base @title:groupBranches @title:groupCheckout @title:groupCommit message @title:groupOptions @title:groupTag Information @title:group The remote hostDestination @title:group The source to pull fromSource @title:window<application>Git</application> Checkout @title:window<application>Git</application> Commit @title:window<application>Git</application> Create Tag @title:window<application>Git</application> Pull @title:window<application>Git</application> Push Dialog height Dialog width Project-Id-Version: fileviewgitplugin
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2011-01-25 06:38+0100
Last-Translator: Manfred Wiese <m.j.wiese@web.de>
Language-Team: Low Saxon <kde-i18n-nds@kde.org>
Language: nds
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=2; plural=n != 1;
 Utlesen Inspelen Beteker opstellen Daalladen Hoochladen Afmellen Dat gifft al en Telg mit den Naam "%1". Dat gifft al en Beteker mit den Naam '%1. De Inspeel-Naricht en Afmellen-Reeg anhangen Telgnaams dörvt keen Freetekens bargen. Ut den utsöchten Telg oder Beteker en niegen Telg opstellen Lokaal Ännern wegsmieten Ok wiedermaken, wenn de Lokaaltelg nich vun den feernen Telg afstammen deit. Betekernaams dörvt keen Freetekens bargen. Dit Archiev bargt keen Betekers Dat gifft nix, dat afännert warrn kunn. Du muttst toeerst en Inspeel-Naricht ingeven. Du muttst toeerst en Naam för den Beteker ingeven. Du muttst toeerst en gellen Naam för den niegen Telg ingeven. Du muttst toeerst en gellen Telg utsöken. Telg Telg: Lokaal Telg: Feern Telg: Feern Telg: Feern: Naricht tofögen: Betekernaam: Verleden Inspelen afännern Nieg Telg opstellen:  Verdwingen Telg: Beteker: Anhangen Basistelg Telgen Utlesen Inspeel-Naricht Optschonen Beteker-Informatschonen Teel Born <application>Git</application> Utlesen <application>Git</application> Inspelen <application>Git</application> Beteker opstellen <application>Git</application> Daalladen <application>Git</application> Hoochladen Dialooghööchd Dialoogbreed 