��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  �  K     �     �  6  
  4  A  P   v	     �	     �	     �	      �	     
     )
     :
     O
                                          
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2008-04-29 20:04+0200
Last-Translator: Manfred Wiese <m.j.wiese@web.de>
Language-Team: Low Saxon <kde-i18n-nds@kde.org>
Language: nds
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=2; plural=n != 1;
  s &Startanimeren-Tiet: <h1>Dorstellen för Programmbalken</h1>
Du kannst en tweet Oort vun Startanimeren anmaken: Binnen den
Programmbalken dukt en Knoop mit en dreihen Stünnenglas op, wat
dat Laden vun Dien Programm wiest.
En Reeg Programmen kennt dat villicht nich, denn geiht de Knoop eerst
wedder na de "Startanimeren-Tiet" weg. <h1>To-Doon-Wieser</h1>
KDE kann dat wiesen, dat jüst en Programm bi is to starten. Wenn
Du dat anmaken wullt, söök en Startanimeren mit dat Utsöökfeld
ut.
En Reeg Programmen kennt dat villicht nich, denn höört de Muuswieser
eerst na de Tiet, de as "Startanimeren-Tiet" angeven is, mit de
Animeren op. <h1>Startanimeren</h1> Hier kannst Du de Startanimeren vun Programmen instellen. Blinken Lüttbild Jumpen Lüttbild To-&Doon-Wieser Programm&balken-Animeren anmaken Keen Animeren Bloots Lüttbild Sta&rtanimeren-Tiet: Dorstellen för &Programmbalken 