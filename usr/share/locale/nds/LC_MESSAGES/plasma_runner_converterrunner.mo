��          4      L       `   �   a   L   �   �  E  �   �     �                    Converts the value of :q: when :q: is made up of "value unit [>, to, as, in] unit". You can use the Unit converter applet to find all available units. list of words that can used as amount of 'unit1' [in|to|as] 'unit2'in;to;as Project-Id-Version: krunner_converterrunner
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2010-01-17 06:03+0100
Last-Translator: Manfred Wiese <m.j.wiese@web.de>
Language-Team: Low Saxon <kde-i18n-nds@kde.org>
Language: nds
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=2; plural=n != 1;
 Reekt den Weert vun :q: üm, wenn :q: so utsüht: "Weert Eenheit [>/na/as/in] Eenheit" (a.B.: "1 km as m"). Mit dat Eenheitenümreek-Lüttprogramm kannst Du all verföögbor Eenheiten finnen. in;na;as 