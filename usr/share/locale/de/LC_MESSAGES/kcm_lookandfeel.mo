��          �      L      �     �     �     �       #   4      X     y  %   �     �  
   �     �     �  �   �     �  ]   �       p   !  :   �  �  �  &   j  #   �     �     �  *   �           .  2   O     �     �     �     �  �   �     {  z   �  $   	  v   -	  I   �	        
                                                                 	                           Apply a look and feel package Configure Look and Feel details Copyright 2017, Marco Martin Cursor Settings Changed Download New Look And Feel Packages EMAIL OF TRANSLATORSYour emails Get New Looks... List available Look and feel packages Look and feel tool Maintainer Marco Martin NAME OF TRANSLATORSYour names Select an overall theme for your workspace (including plasma theme, color scheme, mouse cursor, window and desktop switcher, splash screen, lock screen etc.) Show Preview This module lets you configure the look of the whole workspace with some ready to go presets. Use Desktop Layout from theme Warning: your Plasma Desktop layout will be lost and reset to the default layout provided by the selected theme. You have to restart KDE for cursor changes to take effect. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-27 05:46+0100
PO-Revision-Date: 2018-01-03 20:21+0100
Last-Translator: Burkhard Lück <lueck@hube-lueck.de>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Ein Erscheinungsbild-Paket übernehmen Erscheinungsbild-Details einrichten Copyright 2017, Marco Martin Zeiger-Einstellungen geändert Neue Erscheinungsbild-Pakete herunterladen schwarzer@kde.org Neues Erscheinungsbild holen ... Alle verfügbaren Erscheinungsbild-Pakete anzeigen Erscheinungsbild-Dienstprogramm Betreuer Marco Martin Frederik Schwarzer Wählen Sie ein globales Design für Ihre Arbeitsfläche einschließlich Plasma-Design, Mauszeiger, Fenster- und Arbeitsflächenwechsel, Startbildschirm, Bildschirmsperre usw. Vorschau anzeigen In diesem Modul können Sie das Erscheinungsbild des gesamten Arbeitsfläche mit einigen mitgelieferten Vorlagen anpassen. Desktop-Layout des Designs verwenden Hinweis: Ihr Desktop-Layout für Plasma wird entfernt und durch das Standard-Layout des ausgewählten Designs ersetzt. Sie müssen KDE neu starten, damit die Cursor-Änderungen wirksam werden. 