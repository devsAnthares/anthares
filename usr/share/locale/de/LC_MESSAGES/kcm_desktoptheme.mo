��          �      �       H     I     a      m     �     �     �  
   �     �  &   �          .     L  1   b  �  �  !   H     j     v     �     �     �     �     �  (   �  0     )   P  2   z  B   �         	                                    
                    Configure Desktop Theme David Rosca EMAIL OF TRANSLATORSYour emails Get New Themes... Install from File... NAME OF TRANSLATORSYour names Open Theme Remove Theme Theme Files (*.zip *.tar.gz *.tar.bz2) Theme installation failed. Theme installed successfully. Theme removal failed. This module lets you configure the desktop theme. Project-Id-Version: kcm_desktopthemedetails
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-22 05:21+0100
PO-Revision-Date: 2017-12-04 17:11+0100
Last-Translator: Burkhard Lück <lueck@hube-lueck.de>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 Arbeitsflächen-Design einrichten David Rosca tr@erdfunkstelle.de Neue Designs herunterladen ... Aus Datei installieren ... Thomas Reitelbach Design öffnen Design entfernen Designdateien (*.zip *.tar.gz *.tar.bz2) Die Installation des Designs ist fehlgeschlagen. Das Design wurde erfolgreich installiert. Die Deinstallation des Designs ist fehlgeschlagen. Mit diesem Modul können Sie das Arbeitsflächen-Design einrichten 