��    ?        Y         p     q  !   �  "   �  &   �  ,   �  #   &  &   J  !   q  &   �  '   �  "   �  #     "   )  '   L     t     �  +   �  2   �  
   �     �               ,     3     G  U   O  7   �     �     �     		     	      	     6	     T	     c	     k	  !   �	     �	  	   �	     �	     �	     �	     �	  &   
     /
     N
     U
     p
     v
     ~
     �
  4   �
  $   �
     �
               /     G     ]     w  &   �     �  �  �     b     �     �     �     �     �  
   �     �     �  
   �     �     �     �  
   �     	     &  @   2  K   s     �     �     �     �  	   	          ,  b   5  :   �     �     �       
     *     &   J     q  	   ~     �  '   �     �  
   �  	   �  $   �          $  5   8      n  	   �      �     �     �     �     �  =   �  +   #     O     f     �     �     �     �     �  9   �      ,     $                               1          "   *   )          :             	         ;   !                          9   =   5   /   3                    (         #   4       8       -   6   
   7          %                                         ?       0   &      +          '         .       ,      >       <   2        &Matching window property:  @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Border @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large @item:inlistbox Button size:Large @item:inlistbox Button size:Normal @item:inlistbox Button size:Small @item:inlistbox Button size:Very Large Active Window Glow Add Add handle to resize windows with no border Allow resizing maximized windows from window edges Animations B&utton size: Border size: Button mouseover transition Center Center (Full Width) Class:  Configure fading between window shadow and glow when window's active state is changed Configure window buttons' mouseover highlight animation Decoration Options Detect Window Properties Dialog Edit Edit Exception - Oxygen Settings Enable/disable this exception Exception Type General Hide window title bar Information about Selected Window Left Move Down Move Up New Exception - Oxygen Settings Question - Oxygen Settings Regular Expression Regular Expression syntax is incorrect Regular expression &to match:  Remove Remove selected exception? Right Shadows Tit&le alignment: Title:  Use the same colors for title bar and window content Use window class (whole application) Use window title Warning - Oxygen Settings Window Class Name Window Drop-Down Shadow Window Identification Window Property Selection Window Title Window active state change transitions Window-Specific Overrides Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-13 05:20+0100
PO-Revision-Date: 2018-01-03 20:34+0100
Last-Translator: Burkhard Lück <lueck@hube-lueck.de>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 &Passende Fenster-Eigenschaft:  Riesig Groß Keine Umrandung Keine seitliche Umrandung Normal Übergroß Winzig Noch riesiger Sehr groß Groß Normal Klein Sehr groß Glühen für aktives Fenster Hinzufügen Griff zur Größenänderung von Fenstern ohne Rahmen hinzufügen Größenänderung von maximierten Fenstern über die Fensterkanten zulassen Animationen Knopfgröße: Umrandungsgröße: Übergang bei Mausberührung Zentriert Zentriert (volle Breite) Klasse:  Übergang zwischen Schatten und Glühen beim Wechsel des Aktivitätsstatus von Fenstern einrichten Animation der Fensterknöpfe bei Mausberührung einrichten Dekorations-Einstellungen Fenster-Eigenschaften ermitteln Dialog Bearbeiten Ausnahme bearbeiten - Oxygen-Einstellungen Diese Ausnahme aktivieren/deaktivieren Ausnahme-Typ Allgemein Titelleiste ausblenden Informationen zum ausgewählten Fenster Links Nach unten Nach oben Neue Ausnahme - Oxygen-Einstellungen Frage - Oxygen-Einstellungen Regulärer Ausdruck Die Syntax des regulären Ausdrucks ist nicht korrekt Passender &regulärer Ausdruck:  Entfernen Ausgewählte Ausnahme entfernen? Rechts Schatten Tite&lausrichtung: Titel:  Dieselben Farben für Titelleiste und Fensterinhalt verwenden Fensterklasse verwenden (gesamte Anwendung) Fenstertitel verwenden Warnung - Oxygen-Einstellungen Name der Fensterklasse Schattenwurf für Fenster Fensterkennung Auswahl der Fenster-Eigenschaft Fenstertitel Übergang beim Wechsel des Aktivitätsstatus von Fenstern Fensterspezifische Einstellungen 