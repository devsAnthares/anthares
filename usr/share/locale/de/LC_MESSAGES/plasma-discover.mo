��    a      $  �   ,      8     9     O     R     Z     g  5   w  #   �  E   �  E   	  >   ]	     �	     �	  7   �	     �	     
     (
     G
     f
     �
     �
     �
     �
     �
     �
     �
     �
     �
          !     &  	   -     7     O     `  !   r  F   �     �     �     	  <        X     `  *   i     �     �  	   �     �  	   �     �     �     �      �       
   %     0     N     [     o  
   w  F   �  :   �                         .     5  8   D     }     �  	   �  
   �     �     �     �     �     �     �               .     ?     H     g     m  &   y     �  
   �     �     �     �     �               .  $   ?  �  d          '     *     2     @  <   Q  1   �  I   �  I   
  B   T     �     �  T   �          =  )   T  !   ~  %   �     �     �     �     �            &   2     Y     n     �     �  	   �  
   �  )   �     �     �  $     [   0  +   �  $   �     �  @   �  	   3  	   =  4   G     |  	   �     �     �     �     �     �     �  !   �           #  #   4     X     l     �     �  ]   �  J     
   R  
   ]  	   h     r  	   �     �  ]   �            
   &     1     =     N     \     o     �      �     �  $   �  
   �     �  $   �           )     9  !   ?     a     u     �     �     �     �  "   �     �  ,        J      =   B             L                     ,   G       >   0      ]   I       P         Z   :   2   4          5                              '       )           ^                 R      O   F         K   Y       W   1      +                   9   A       $   a   M   _       <       ?              8   C   Q   E      D       U   \          `   6   &   @              7          
   ;   V   	           H       %   .   !   *   (   N      T       [   #   3      X   S   /          -   "    
Also available in %1 %1 %1 (%2) %1 (Default) <b>%1</b> by %2 <em>%1 out of %2 people found this review useful</em> <em>Tell us about this review!</em> <em>Useful? <a href='true'><b>Yes</b></a>/<a href='false'>No</a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'><b>No</b></a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'>No</a></em> @infoFetching updates @infoFetching... @infoIt is unknown when the last check for updates was @infoLooking for updates @infoNo updates @infoNo updates are available @infoShould check for updates @infoThe system is up to date @infoUpdates @infoUpdating... Accept Add Source... Addons Aleix Pol Gonzalez An application explorer Apply Changes Available backends:
 Available modes:
 Back Cancel Category: Checking for updates... Comment too long Comment too short Compact Mode (auto/compact/full). Could not close the application, there are tasks that need to be done. Could not find category '%1' Couldn't open %1 Delete the origin Directly open the specified application by its package name. Discard Discover Display a list of entries with a category. Extensions... Help... Homepage: Install Installed Jonathan Thomas Launch License: List all the available backends. List all the available modes. Loading... Local package file to install Make default More Information... More... No Updates Open Discover in a said mode. Modes correspond to the toolbar buttons. Open with a program that can deal with the given mimetype. Proceed Rating: Remove Resources for '%1' Review Reviewing '%1' Running as <em>root</em> is discouraged and unnecessary. Search Search in '%1'... Search... Search: %1 Search: %1 + %2 Settings Short summary... Show reviews (%1)... Size: Sorry, nothing found... Source: Specify the new source for %1 Still looking... Summary: Supports appstream: url scheme Tasks Tasks (%1%) TransactioName TransactionStatus%1 %2 Unable to find resource: %1 Update All Update Selected Update section nameUpdate (%1) Updates Version: unknown reviewer updates not selected updates selected © 2010-2016 Plasma Development Team Project-Id-Version: muon-discover
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:02+0100
PO-Revision-Date: 2018-02-11 22:07+0100
Last-Translator: Frederik Schwarzer <schwarzer@kde.org>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 
Auch verfügbar in %1 %1 %1 (%2) %1 (Standard) <b>%1</b> von %2 <em>%1 von %2 Personen fanden diese Rezension hilfreich</em> <em>Erzählen Sie uns über diese Rezension.</em> <em>Nützlich? <a href='true'><b>Ja</b></a>/<a href='false'>Nein</a></em> <em>Nützlich? <a href='true'>Ja</a>/<a href='false'><b>Nein</b></a></em> <em>Nützlich? <a href='true'>Ja</a>/<a href='false'>Nein</a></em> Aktualisierungen werden geholt Wird abgerufen ... Es kann nicht bestimmt werden, wann zuletzt nach Aktualisierungen gesucht worden ist Suchen nach  Aktualisierungen  Keine Aktualisierungen Es sind keine Aktualisierungen verfügbar Soll auf Aktualisierungen prüfen Das System ist auf dem neuesten Stand Aktualisierungen Aktualisierung läuft ... Annehmen Quelle hinzufügen ... Erweiterungen Aleix Pol Gonzalez Ein Programm, um Anwendungen zu finden Änderungen anwenden Verfügbare Backends:
 Verfügbare Modi:
 Zurück Abbrechen Kategorie: Es wird nach Aktualisierungen gesucht ... Kommentar ist zu lang Kommentar ist zu kurz Kompakter Modus (Auto/Kompakt/Voll). Die Anwendung kann nicht geschlossen werden, da noch Aufgaben abgeschlossen werden müssen. Die Kategorie „%1“ wurde nicht gefunden „%1“ kann nicht geöffnet werden Paketquelle löschen Öffnet die angegebene Anwendung direkt über seinen Paketnamen. Verwerfen Entdecken Zeigt eine Liste von Einträgen mit einer Kategorie. Erweiterungen ... Hilfe ... Internetseite: Installieren Installiert Jonathan Thomas Starten Lizenz: Zeigt alle verfügbaren Backends. Alle verfügbaren Modi anzeigen. Wird geladen ... Zu installierende lokale Paketdatei Als Standard setzen Weitere Informationen ... Weitere ... Keine Aktualisierungen Öffnet Discover im genannten Modus. die Modi entsprechen den Knöpfen in der Werkzeugleiste. Mit einem Programm öffnen, das den angegebenen Mime-Typ verarbeiten kann. Fortfahren Bewertung: Entfernen Ressourcen für „%1“ Rezension „%1“ rezensieren Die Ausführung als Systemverwalter (<em>root</em>) ist unnötig und es wird davon abgeraten. Suchen Suchen in „%1“ ... Suchen ... Suchen : %1 Suchen : %1 + %2 Einstellungen Kurzübersicht ... Rezensionen anzeigen (%1) ... Größe: Leider wurde nichts gefunden ... Quelle: Geben Sie die neue Quelle für %1 an Suchen ... Übersicht: Unterstützt Appstream: Adressschema Aufgaben Aufgaben (%1 %) %1 %2 Ressource %1 wurde nicht gefunden Alles aktualisieren Aktualisierung ausgewählt Aktualisierung (%1) Aktualisierungen Version: Unbekannter Rezensent Aktualisierungen nicht ausgewählt Aktualisierungen ausgewählt Copyright © 2010-2016 Plasma-Entwicklerteam 