��    	      d      �       �      �      �   -        <  -   V  #   �  #   �     �  �  �     �     �  7   �        6        R     X     ]                                   	           Current time is %1 Displays the current date Displays the current date in a given timezone Displays the current time Displays the current time in a given timezone Note this is a KRunner keyworddate Note this is a KRunner keywordtime Today's date is %1 Project-Id-Version: plasma_runner_datetime
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2015-03-10 15:45+0100
Last-Translator: Burkhard Lück <lueck@hube-lueck.de>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 Die aktuelle Zeit ist: %1 Zeigt das aktuelle Datum an Zeigt das aktuelle Datum in der angegebenen Zeitzone an Zeigt die aktuelle Zeit an Zeigt die aktuelle Zeit in der angegebenen Zeitzone an datum zeit Das heutige Datum ist: %1 