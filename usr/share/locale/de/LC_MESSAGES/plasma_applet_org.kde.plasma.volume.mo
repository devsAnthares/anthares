��    &      L  5   |      P     Q  8   S     �     �     �     �     �     �  3   �  >        N     i     y     �  m   �     �          "     2     7     ?  *   O      z     �     �  "   �     �     �          3     :     H     X     e     �  ;   �     �  �  �     �     �     �     �     �  	   �     �     �     �     	     	     +	     B	  	   J	     T	     Z	     w	     �	     �	     �	     �	  4   �	  +   
     8
     J
     \
     p
     �
  &   �
     �
     �
     �
     	     '     /     6     <        #   %                                                              "          $                      
                                           &      !                	        % Accessibility data on volume sliderAdjust volume for %1 Applications Audio Muted Audio Volume Behavior Capture Devices Capture Streams Checkable switch for (un-)muting sound output.Mute Checkable switch to change the current default output.Default Decrease Microphone Volume Decrease Volume Devices General Heading for a list of ports of a device (for example built-in laptop speakers or a plug for headphones)Ports Increase Microphone Volume Increase Volume Maximum volume: Mute Mute %1 Mute Microphone No applications playing or recording audio No output or input devices found Playback Devices Playback Streams Port is unavailable (unavailable) Port is unplugged (unplugged) Raise maximum volume Show additional options for %1 Volume Volume at %1% Volume feedback Volume step: label of device items%1 (%2) label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2018-01-03 20:35+0100
Last-Translator: Burkhard Lück <lueck@hube-lueck.de>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
  % Lautstärke für %1 anpassen Anwendungen Audio stumm Lautstärke Verhalten Aufnahmegeräte Aufnahmestreams Stumm Standard Mikrofonlautstärke verringern Lautstärke verringern Geräte Allgemein Ports Mikrofonlautstärke erhöhen Lautstärke erhöhen Maximale Lautstärke Stummschalten %1 Stummschalten Mikrofon stummschalten Keine Anwendung spielt Audio ab oder nimmt Audio auf Keine Eingabe- oder Ausgabegeräte gefunden Wiedergabegeräte Wiedergabestreams  (Nicht verfügbar) (Nicht angeschlossen) Maximale Lautstärke erhöhen Weitere Einstellungen für %1 anzeigen Lautstärke Lautstärke bei %1 % Rückmeldung der Lautstärke Schrittweite der Lautstärke: %1 (%2) %1: %2 100 % %1 % 