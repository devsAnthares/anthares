��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  �  K     �  %   �  �     �  �  x   �
     �
          !  (   4     ]     v  %   �     �                                          
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2008-02-26 20:41+0100
Last-Translator: Thomas Reitelbach <tr@erdfunkstelle.de>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: LoKalize 0.2
Plural-Forms: nplurals=2; plural=n != 1;
  Sek. &Zeitlimit für Programmstartanzeige: <h1>Anzeige in der Fensterleiste</h1>
Sie können eine zweite Methode der Programmstartanzeige aktivieren,
nämlich die Darstellung einer rotierenden Sanduhr in der Fensterleiste.
Auch diese Animation symbolisiert, dass das von Ihnen gestartete Programm
soeben geladen wird.
Es kann vorkommen, dass Programme diese Anzeige nicht von sich aus abschalten.
In diesen Fällen wird sie nach Ablauf des „Zeitlimits für Programmstartanzeige“
automatisch deaktiviert. <h1>Aktivitätsanzeige</h1>
KDE bietet eine Aktivitätsanzeige des Mauszeigers beim Start von Programmen.
Um diese Funktion zu benutzen, wählen Sie eine Form von visueller Rückmeldung aus dem Kombinationsfeld.
Es kann vorkommen, dass Programme diese Anzeigen nicht von sich aus abschalten.
In diesen Fällen wird sie nach Ablauf des „Zeitlimits für Programmstartanzeige“
automatisch deaktiviert. <h1>Rückmeldung bei Programmstart</h1>Hier können Sie die Rückmeldung festlegen, die ein Programm beim Start ausgibt. Blinkender Cursor Hüpfender Cursor Aktivitätsanzeige Anzeige in der &Fensterleiste aktivieren Keine Aktivitätsanzeige Passive Aktivitätsanzeige &Zeitlimit für Programmstartanzeige: Anzeige in der &Fensterleiste 