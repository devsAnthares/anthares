��          �            x  
   y  	   �     �     �  3   �     �     �                    %     *     F  B   Y     �  �  �     e     v     �     �  3   �     �     �               (     0  $   8  !   ]          �     	                                                  
                        Appearance Browse... Choose an image Fifteen Puzzle Image Files (*.png *.jpg *.jpeg *.bmp *.svg *.svgz) Number color Path to custom image Piece color Show numerals Shuffle Size Solve by arranging in order Solved! Try again. The time since the puzzle started, in minutes and secondsTime: %1 Use custom image Project-Id-Version: plasma_applet_fifteenPuzzle
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-01-12 03:59+0100
PO-Revision-Date: 2017-01-14 15:04+0100
Last-Translator: Burkhard Lück <lueck@hube-lueck.de>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 Erscheinungsbild Durchsuchen ... Wählen Sie ein Bild Fünfzehn Steine Bilddateien (*.png *.jpg *.jpeg *.bmp *.svg *.svgz) Farbe der Zahlen Pfad zum eigenes Bild Farbe der Steine Zahlen anzeigen Mischen Größe Lösen durch Ändern der Reihenfolge Gelöst. Versuchen Sie es erneut. Zeit: %1 Eigenes Bild verwenden 