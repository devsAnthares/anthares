��    ,      |  ;   �      �  (   �     �  �   �     �     �     �     �     �     �     �     �     �               %     1      J     k     s     �     �     �     �     �     �     �               /     4     I     Y     l     y     �     �      �  7   �                     1     6  �  <     �     �     �     	  	   	     	     %	     =	     J	     X	  &   `	     �	     �	     �	     �	     �	     �	  	   �	     �	     
     -
     D
     \
     u
     �
     �
     �
     �
     �
     �
          %     B     S  '   l     �  !   �  C   �       (   
     3     L  	   U     !                 %   +   ,      "   #                               &                  	                           )                             *      '      $      (                                
    %1 is the name of a session%1 (Wayland) ... @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) @title:windowSelect Image Advanced Author Auto &Login Background: Clear Image Commands Could not decompress archive Cursor Theme: Customize theme Default Description Download New SDDM Themes EMAIL OF TRANSLATORSYour emails General Get New Theme Halt Command: Install From File Install a theme. Invalid theme package Load from file... Login screen using the SDDM Maximum UID: Minimum UID: NAME OF TRANSLATORSYour names Name No preview available Reboot Command: Relogin after quit Remove Theme SDDM KDE Config SDDM theme installer Session: The default cursor theme in SDDM The theme to install, must be an existing archive file. Theme Unable to install theme Uninstall a theme. User User: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2017-12-04 17:16+0100
Last-Translator: Burkhard Lück <lueck@hube-lueck.de>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 %1 (Wayland) ... (Verfügbare Größen: %1) Bild auswählen Erweitert Autor Automatische Anme&ldung Hintergrund: Bild löschen Befehle Das Archive kann nicht entpackt werden Zeigerdesign: Design anpassen Voreinstellung Beschreibung Neue SDDM-Designs herunterladen lueck@hube-lueck.de Allgemein Neues Design holen Befehl zum Herunterfahren: Aus Datei installieren Installiert ein Design. Ungültiges Designpakets Aus Datei laden ... Anmeldebildschirm (SDDM) Maximale UID (Benutzerkennung): Minimale UID (Benutzerkennung): Burkhard Lück Name Keine Vorschau verfügbar Befehl zum Neustart: Nach Beenden erneut anmelden Design entfernen KDE-Einrichtung von SDDM Installationsprogramm für SDDM-Designs Sitzung: Das Standard-Zeigerdesign in SDDM Das zu installierende Design muss eine vorhandene Archivdatei sein. Design Das Design kann nicht installiert werden Deinstalliert ein Design Benutzer Benutzer: 