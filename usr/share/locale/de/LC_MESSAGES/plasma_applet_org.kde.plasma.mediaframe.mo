��            )   �      �     �     �     �     �     �     �     �  
           )   (     R     V     \     c     v     �     �     �  3   �     �       <   #  5   `  8   �  8   �                    /  �  1     �     �     �          "     :     M  	   e  	   o  9   y     �     �     �  )   �  -   �  +   !     M     i  1   r     �  '   �  B   �  F   .	  :   u	  :   �	     �	     �	     
     
                                                  
                                                                               	        Add files... Add folder... Background frame Change picture every Choose a folder Choose files Configure plasmoid... Fill mode: General Left click image opens in external viewer Pad Paths Paths: Pause on mouseover Preserve aspect crop Preserve aspect fit Randomize items Stretch The image is duplicated horizontally and vertically The image is not transformed The image is scaled to fit The image is scaled uniformly to fill, cropping if necessary The image is scaled uniformly to fit without cropping The image is stretched horizontally and tiled vertically The image is stretched vertically and tiled horizontally Tile Tile horizontally Tile vertically s Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-16 03:31+0100
PO-Revision-Date: 2017-12-04 17:13+0100
Last-Translator: Burkhard Lück <lueck@hube-lueck.de>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Dateien hinzufügen ... Ordner hinzufügen ... Hintergrundrahmen Bild wechseln alle Einen Ordner auswählen Dateien auswählen Plasmoid einrichten ... Füllart: Allgemein Linksklick auf das Bild öffnet es in externem Betrachter Original Pfade Pfade: Pause beim Überfahren mit dem Mauszeiger Seitenverhältnis beibehalten und zuschneiden Seitenverhältnis beibehalten und einpassen Elemente zufällig anzeigen Strecken Das Bild wird waagerecht und senkrecht dupliziert Das Bild wird nicht verändert Das Bild wird skaliert, sodass es passt Das Bild wird gleichmäßig skaliert und wenn nötig zugeschnitten Das Bild wird gleichmäßig skaliert, sodass es ohne Zuschneiden passt Das Bild wird waagerecht gestreckt und senkrecht gekachelt Das Bild wird senkrecht gestreckt und waagerecht gekachelt Kacheln Waagerecht kacheln Senkrecht kacheln s 