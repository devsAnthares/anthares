��    "      ,  /   <      �  �  �  
   �     �     �       	     %        <     R     Y     _     v     �     �     �  4   �     �     �  !        %     1     N     [     `     w     �  &   �  .   �     �              9   /  C   i  �  �  �  L	     %     2  )   ;     e  
   m  -   x     �  	   �     �      �     �       "        :  R   @     �  	   �  +   �     �  $   �          ,     3     O     \  +   d  4   �  !   �  +   �       I   &     p                    "                    
                                                                                                  !         	           <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body style=" font-family:'hlv'; font-size:9pt; font-weight:400; font-style:normal;">
<p style="-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><br /></p></body></html> Activities Backend: Can not create the mount point Change Configure Configured backend does not exist: %1 Correct version found Create CryFS Device is already open Device is not open Dialog Do not show this notice again EncFS Failed to create directories, check your permissions Failed to execute General Limit to the selected activities: Mount point Mount point is not specified Mount point: Next Open with File Manager Plasma Vault Previous The specified backend is not available This directory already contains encrypted data Unable to detect the version Unable to perform the operation Unknown device Wrong version installed. The required version is %1.%2.%3 formatting the message for a command, as in encfs: not found%1: %2 Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-11 05:53+0100
PO-Revision-Date: 2018-02-11 22:04+0100
Last-Translator: Frederik Schwarzer <schwarzer@kde.org>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body style=" font-family:'hlv'; font-size:9pt; font-weight:400; font-style:normal;">
<p style="-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><br /></p></body></html> Aktivitäten Backend: Einhängepunkt kann nicht erstellt werden Ändern Einrichten Das eingerichtete Backend existiert nicht: %1 Richtige Version gefunden Erstellen CryFS Das Gerät ist bereits geöffnet Das Gerät ist nicht geöffnet Dialog Diesen Hinweis nicht mehr anzeigen EncFS Das Erstellen der Ordners ist fehlgeschlagen, überprüfen Sie Ihre Berechtigungen Ausführung ist fehlgeschlagen Allgemein Auf ausgewählte Aktivitäten beschränken: Einhängepunkt Einhängepunkt wurde nicht angegeben Einhängepunkt: Weiter Mit Dateiverwaltung öffnen Plasma Vault Zurück Das angegebene Backend ist nicht verfügbar Dieser Ordner enthält bereits verschlüsselte Daten Version kann nicht erkannt werden Der Vorgang kann nicht durchgeführt werden Unbekanntes Gerät Es ist die falsche Version installiert. Version %1.%2.%3 ist erforderlich %1: %2 