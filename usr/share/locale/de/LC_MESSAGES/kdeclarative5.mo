��          �            h  ~   i  +   �           5     T     s     �     �  �   �  g   A  0   �  .   �     	  @     �  Z  �   �  .   �  /   �       /   <     l     s     �  �   �  �   V  ;   �  0   #	     T	     n	                                                   	             
                Click on the button, then enter the shortcut like you would in the program.
Example for Ctrl+A: hold the Ctrl key and press A. Conflict with Standard Application Shortcut EMAIL OF TRANSLATORSYour emails KPackage QML application shell NAME OF TRANSLATORSYour names No shortcut definedNone Reassign Reserved Shortcut The '%1' key combination is also used for the standard action "%2" that some applications use.
Do you really want to use it as a global shortcut as well? The F12 key is reserved on Windows, so cannot be used for a global shortcut.
Please choose another one. The key you just pressed is not supported by Qt. The unique name of the application (mandatory) Unsupported Key What the user inputs now will be taken as the new shortcutInput Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-30 03:07+0100
PO-Revision-Date: 2015-07-01 20:17+0200
Last-Translator: Burkhard Lück <lueck@hube-lueck.de>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 Klicken Sie auf den Knopf und drücken Sie dann die Tasten für den Kurzbefehl auf der Tastatur.
Beispielsweise für Strg+A: Halten Sie die Strg-Taste gedrückt und drücken Sie dann „A“. Konflikt mit Standard-Kurzbefehl des Programms tr@erdfunkstelle.de, hunsum@gmx.de, thd@kde.org KPackage-QML-Anwendungs-Shell Thomas Reitelbach, Stephan Johach, Thomas Diehl Keiner Neu zuordnen Reservierter Kurzbefehl Die Tastenkombination „%1“ ist bereits der Standard-Aktion „%2“ zugeordnet, die von einigen Anwendungen benutzt wird.
Möchten Sie sie wirklich als globales Tastenkürzel verwenden? Die Taste „F12“ ist unter Windows reserviert, kann also nicht als globaler Kurzbefehl verwendet werden.
Bitte wählen Sie eine andere Taste. Die gerade gedrückte Taste wird von Qt nicht unterstützt. Der eindeutige Name der Anwendung (erforderlich) Nicht unterstützte Taste Eingabe 