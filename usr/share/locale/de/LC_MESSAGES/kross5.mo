��    $      <  5   \      0     1  +   E     q  4   �  &   �     �     �                     5     :     P     X  ,   u  3   �     �     �               !  '   .     V     u     {     �     �     �     �     �     �  &   �       B        b  �  y          $     <     L  	   g     q     �  
   �  
   �  /   �  
   �  #   �     	  %   #	  K   I	  I   �	  .   �	  0   
     ?
     F
     N
  &   [
  /   �
     �
  "   �
  #   �
  	   �
  "   	  
   ,  )   7     a  #   j     �  8   �     �                                       
                              !                                  $      #                               	                           "             @info:creditAuthor @info:creditCopyright 2006 Sebastian Sauer @info:creditSebastian Sauer @info:shell command-line argumentThe script to run. @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: application descriptionCommand-line utility to run Kross scripts. application nameKross Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2014-04-02 08:05+0200
Last-Translator: Burkhard Lück <lueck@hube-lueck.de>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 Autor © 2006 Sebastian Sauer Sebastian Sauer Das auszuführende Skript. Allgemein Ein neues Skript hinzufügen. Hinzufügen ... Abbrechen? Kommentar: tr@erdfunkstelle.de, hunsum@gmx.de, thd@kde.org Bearbeiten Das ausgewählte Skript bearbeiten. Bearbeiten ... Ausführen des ausgewählten Skripts. Das Erstellen des Skriptes für den Interpreter „%1“ ist fehlgeschlagen Der Interpreter für die Skriptdatei „%1“ kann nicht bestimmt werden. Interpreter „%1“ kann nicht geladen werden Skriptdatei „%1“ kann nicht geöffnet werden Datei: Symbol: Interpreter: Sicherheitsstufe des Ruby-Interpreters Thomas Reitelbach, Stephan Johach, Thomas Diehl Name: Funktion nicht vorhanden: „%1“ Es gibt keinen Interpreter „%1“ Entfernen Das ausgewählte Skript entfernen. Ausführen Die Skriptdatei „%1“ existiert nicht. Anhalten Anhalten des ausgewählten Skripts. Text: Befehlszeilenprogramm zur Ausführung von Kross-Skripten Kross 