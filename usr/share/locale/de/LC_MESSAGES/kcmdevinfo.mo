��    ]           �      �  !   �       	   !     +  S   4  	   �     �     �     �     �     �     �     �     �     �     	  J   $	     o	     }	     �	      �	  	   �	  
   �	     �	     �	     �	     �	     
     
     
  L   
  	   l
  	   v
  
   �
  
   �
  
   �
     �
     �
     �
     �
     �
     �
     �
     �
     	     (  	   +     5     G     S     [     i     m     }     �     �  
   �  	   �     �  
   �     �     �     �     �     �     	          +     ?     \     r     x     |     �  H   �     �     �     �     �     �       
     &        A  "   T     w     �     �     �     �       	     �  (  )   �     �  
             #     @  
   F     Q     W     ^     f     s     �     �  
   �     �  ,   �     �     
                8     G     W     c     t  
   �     �     �  	   �  8   �  	   �  	   �  
   �  
   
  
              )     :     H     a     {     �     �     �     �     �     �     �     �     �               (     0     =     O  	   [     e     j     v     {     �     �     �     �     �     �  #        )     D     J     N     R  G   Y     �     �     �     �     �       
     	             !  	   '  	   1  	   ;  	   E  	   O  	   Y     c           B   "       (   W   #   $   %            [   @      L   M   '   6       >   T           <   9   G   &           A      .   8   R       7       J             ?   4                   H   -                      F          5   )   N   D       ,       K             3       U   =   
       ;       O   :           +           !   Z   /      V   Y              Q         E       1       X             ]       0      P         \   I          	          C       S   2       *    
Solid Based Device Viewer Module (c) 2010 David Hubner AMD 3DNow ATI IVEC Available space out of total partition size (percent used)%1 free of %2 (%3% used) Batteries Battery Type:  Bus:  Camera Cameras Charge Status:  Charging Collapse All Compact Flash Reader Default device tooltipA Device Device Information Device Listing Whats ThisShows all the devices that are currently listed. Device Viewer Devices Discharging EMAIL OF TRANSLATORSYour emails Encrypted Expand All File System File System Type:  Fully Charged Hard Disk Drive Hotpluggable? IDE IEEE1394 Info Panel Whats ThisShows information about the currently selected device. Intel MMX Intel SSE Intel SSE2 Intel SSE3 Intel SSE4 Keyboard Keyboard + Mouse Label:  Max Speed:  Memory Stick Reader Mounted At:  Mouse Multimedia Players NAME OF TRANSLATORSYour names No No Charge No data available Not Mounted Not Set Optical Drive PDA Partition Table Primary Processor %1 Processor Number:  Processors Product:  Raid Removable? SATA SCSI SD/MMC Reader Show All Devices Show Relevant Devices Smart Media Reader Storage Drives Supported Drivers:  Supported Instruction Sets:  Supported Protocols:  UDI:  UPS USB UUID:  Udi Whats ThisShows the current device's UDI (Unique Device Identifier) Unknown Drive Unused Vendor:  Volume Space: Volume Usage:  Yes kcmdevinfo name of something is not knownUnknown no device UDINone no instruction set extensionsNone platform storage busPlatform unknown battery typeUnknown unknown deviceUnknown unknown device typeUnknown unknown storage busUnknown unknown volume usageUnknown xD Reader Project-Id-Version: kcmdevinfo
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-23 03:11+0200
PO-Revision-Date: 2016-06-02 10:59+0100
Last-Translator: Burkhard Lück <lueck@hube-lueck.de>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 
Solid-basiertes Gerätebetrachtungsmodul © 2010, David Hubner AMD 3DNow! ATI IVEC %1 von %2 frei (%3 % belegt) Akkus Akku-Typ:  Bus:  Kamera Kameras Ladestatus:  Wird geladen Alle einklappen Lesegerät (Compact Flash) Ein Gerät Geräteinformationen Zeigt alle derzeit aufgelisteten Geräte an. Gerätebetrachter Geräte Wird entladen johannesobermayr@gmx.de Verschlüsselt Alle aufklappen Dateisystem Dateisystemtyp:  Vollständig geladen Festplatte Hotplug-fähig? IDE IEEE 1394 Zeigt Informationen zum aktuell ausgewählten Gerät an. Intel MMX Intel SSE Intel SSE2 Intel SSE3 Intel SSE4 Tastatur Tastatur && Maus Bezeichnung:  Maximalgeschwindigkeit:  Lesegerät (Memory Stick) Eingebunden bei:  Maus Medienspieler Johannes Obermayr Nein Nicht geladen Keine Daten verfügbar Nicht eingebunden Nicht festgelegt Optisches Laufwerk PDA Partitionstabelle Primär Prozessor %1 Prozessornummer:  Prozessoren Produkt:  RAID Wechselbar? SATA SCSI Lesegerät (SD/MMC) Alle Geräte anzeigen Wichtige Geräte anzeigen Lesegerät (Smart Media) Speichergeräte Unterstützte Treiber:  Unterstützte Instruktions-Sätze:  Unterstützte Protokolle:  UDI:  USV USB UUID:  Zeigt die UDI (eindeutige Gerätebezeichnung) des aktuellen Geräts an. Unbekanntes Laufwerk Nicht verwendet Hersteller:  Datenträgergröße:  Speicherplatzverwendung:  Ja kcmdevinfo Unbekannt Keine Keine Plattform Unbekannt Unbekannt Unbekannt Unbekannt Unbekannt Lesegerät (xD) 