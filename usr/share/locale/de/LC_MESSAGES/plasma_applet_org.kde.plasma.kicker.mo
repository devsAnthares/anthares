��    _                   C   	  /   M  -   }     �     �     �     �      	     	     1	     >	     J	  
   S	     ^	     g	     p	     �	  	   �	     �	     �	     �	  ,   �	  	    
     

  
   )
     4
     L
     `
     u
     �
     �
     �
     �
     �
  	   �
     �
     �
     
               !     (  	   ;     E     ]  
   r     }     �  
   �     �     �     �  
   �     �     �               $     2     @     R     h     y     �     �     �     �  	   �     �     �     �               4     Q     j     �     �     �     �  	   �     �  ,   �          !     0     @     L     S     b     t     �  #   �     �  �  �     i          �     �     �  -   �  $   	     .     ?     G     S  	   h  
   r     }     �     �     �  	   �     �     �     �  4     	   6  (   @     i     y     �     �     �     �     �  &   �  	   %     /     7     C     Q     f     n     v     �     �     �     �     �     �     �          ,     9     S     X     t  
   �     �     �     �     �     �     
          0     C  $   Y     ~     �     �  
   �     �     �     �     �       %   '  $   M  #   r  %   �  $   �  %   �  	                *   7     b     p     ~     �     �     �     �     �  5   �  >        V         Q         Y   5           %       H       J   !              \   *       @   V   4   [       A   (   
       U   B   ]   ^       $   _         '   >       D   K      -                  ?      ,   O   0       R   8   6   W   2   I   =   X   E   #       N   C       T   G   M          3   "         9             +          S          F           ;   :                    &   <   L          P         7          .       /       1                     )            	   Z       @action opens a software center with the applicationManage '%1'... @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Desktop Add to Favorites Add to Panel (Widget) Align search results to bottom All Applications App name (Generic name)%1 (%2) Applications Apps & Docs Behavior Categories Computer Contacts Description (Name) Description only Documents Edit Application... Edit Applications... End session Expand search to bookmarks, files and emails Favorites Flatten menu to a single level Forget All Forget All Applications Forget All Contacts Forget All Documents Forget Application Forget Contact Forget Document Forget Recent Documents General Generic name (App name)%1 (%2) Hibernate Hide %1 Hide Application Icon: Lock Lock screen Logout Name (Description) Name only Often Used Applications Often Used Documents Often used On All Activities On The Current Activity Open with: Pin to Task Manager Places Power / Session Properties Reboot Recent Applications Recent Contacts Recent Documents Recently Used Recently used Removable Storage Remove from Favorites Restart computer Run Command... Run a command or a search query Save Session Search Search results Search... Searching for '%1' Session Show Contact Information... Show In Favorites Show applications as: Show often used applications Show often used contacts Show often used documents Show recent applications Show recent contacts Show recent documents Show: Shut Down Sort alphabetically Start a parallel session as a different user Suspend Suspend to RAM Suspend to disk Switch User System System actions Turn off computer Type to search. Unhide Applications in '%1' Unhide Applications in this Submenu Widgets Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2017-11-27 15:16+0100
Last-Translator: Burkhard Lück <lueck@hube-lueck.de>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 „%1“ verwalten... Auswählen ... Symbol zurücksetzen Zur Arbeitsfläche hinzufügen Zu Favoriten hinzufügen Zur Kontrollleiste hinzufügen (Miniprogramm) Suchergebnisse nach unten ausrichten Alle Anwendungen %1 (%2) Anwendungen Apps & Dokumentation Verhalten Kategorien Rechner Kontakte Beschreibung (Name) Nur Beschreibung Dokumente Anwendung bearbeiten ... Anwendungen bearbeiten ... Sitzung beenden Suche auf Lesezeichen, Dateien und E-Mails erweitern Favoriten Menü auf eine einzelne Ebene verflachen Alles vergessen Alle Anwendungen vergessen Alle Kontakte vergessen Alle Dokumente vergessen Anwendung vergessen Kontakt vergessen Dokument vergessen Zuletzt geöffnete Dokumente vergessen Allgemein %1 (%2) Ruhezustand %1 ausblenden Anwendung ausblenden Symbol: Sperren Bildschirm sperren Abmelden Name (Beschreibung) Nur Name Häufig benutzte Anwendungen Häufig benutzte Dokumente Häufig verwendete In allen Aktivitäten In der aktuellen Aktivität Öffnen mit: An Fensterleiste anheften Orte Energieverwaltung / Sitzung Eigenschaften Neustarten Zuletzt benutzte Anwendungen Zuletzt verwendete Kontakte Zuletzt geöffnete Dokumente Kürzlich benutzt Kürzlich verwendete Wechselmedien Aus Favoriten entfernen Rechner neustarten Befehl ausführen ... Befehl ausführen oder Suche starten Sitzung speichern Suchen Suchergebnisse Suchen ... Es wird nach „%1“ gesucht Sitzung Kontaktinformation anzeigen ... In Favoriten anzeigen Anwendungen anzeigen als: Häufig benutzte Anwendungen anzeigen Häufig verwendete Kontakte anzeigen Häufig benutzte Dokumente anzeigen Zuletzt benutzte Anwendungen anzeigen Zuletzt verwendete Kontakte anzeigen Zuletzt geöffnete Dokumente anzeigen Anzeigen: Herunterfahren Alphabetisch sortieren Weitere Sitzung als anderer Nutzer starten Standby-Modus Standby-Modus Ruhezustand Benutzer wechseln System Systemfunktionen Rechner ausschalten Suchbegriff eingeben … Ausgeblendete Anwendungen in „%1“ wieder anzeigen Ausgeblendete Anwendungen in diesem Untermenü wieder anzeigen Miniprogramme 