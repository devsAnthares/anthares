��          �      L      �     �     �  �   �  T   }  )   �  (   �  0   %  $   V  &   {  &   �  %   �  ?   �  F   /     v     �     �     �     �  �  �      �  !   �  �   �  T   �     �     �     
           (     6     E     Q     i  &   �     �     �  (   �  &   �                                              
                    	                                  Dim screen by half Dim screen totally Lists screen brightness options or sets it to the brightness defined by :q:; e.g. screen brightness 50 would dim the screen to 50% maximum brightness Lists system suspend (e.g. sleep, hibernate) options and allows them to be activated Note this is a KRunner keyworddim screen Note this is a KRunner keywordhibernate Note this is a KRunner keywordscreen brightness Note this is a KRunner keywordsleep Note this is a KRunner keywordsuspend Note this is a KRunner keywordto disk Note this is a KRunner keywordto ram Note this is a KRunner keyword; %1 is a parameterdim screen %1 Note this is a KRunner keyword; %1 is a parameterscreen brightness %1 Set Brightness to %1 Suspend to Disk Suspend to RAM Suspends the system to RAM Suspends the system to disk Project-Id-Version: plasma_runner_powerdevil
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-26 03:33+0200
PO-Revision-Date: 2016-03-15 23:02+0100
Last-Translator: Frederik Schwarzer <schwarzer@kde.org>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 Bildschirm zur Hälfte abdunkeln Bildschirm vollständig abdunkeln Zeigt Einstellungen zur Bildschirmhelligkeit an oder legt sie auf den mit :q: angegebenen Wert fest. Ein Wert von 50 würde die Bildschirmhelligkeit beispielsweise auf 50 % einstellen. Zeigt Optionen zu Standby-Modus und Ruhezustand an und ermöglicht deren Aktivierung Bildschirm abdunkeln Ruhezustand Bildschirm-Helligkeit Standby Standby-Modus auf Festplatte im Speicher Bildschirm abdunkeln %1 Bildschirm-Helligkeit %1 Bildschirmhelligkeit auf %1 einstellen Ruhezustand Standby-Modus Versetzt das System in den Standby-Modus Versetzt das System in den Ruhezustand 