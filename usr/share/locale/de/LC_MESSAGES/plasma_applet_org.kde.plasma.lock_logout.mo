��          �      L      �  &   �  +   �       @     	   ]     g     �     �     �     �  (   �     �     �  ,   �               +     7  �  ;  8   �  6   (  	   _     i     r     ~  	   �     �     �     �  :   �     
       3   .     b     p     �     �        
                          	                                                                  Do you want to suspend to RAM (sleep)? Do you want to suspend to disk (hibernate)? General Heading for list of actions (leave, lock, shutdown, ...)Actions Hibernate Hibernate (suspend to disk) Leave Leave... Lock Lock the screen Logout, turn off or restart the computer No Sleep (suspend to RAM) Start a parallel session as a different user Suspend Switch User Switch user Yes Project-Id-Version: plasma_applet_lockout
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2016-03-15 22:36+0100
Last-Translator: Frederik Schwarzer <schwarzer@kde.org>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 Möchten Sie den Rechner in den Standby-Modus versetzen? Möchten Sie den Rechner in den Ruhezustand versetzen? Allgemein Aktionen Ruhezustand Ruhezustand (Suspend to Disk) Verlassen Verlassen ... Sperren Bildschirm sperren Sitzung abmelden, den Rechner ausschalten oder neu starten Nein Standby-Modus (Suspend to RAM) Eine parallele Sitzung als anderer Benutzer starten Standby-Modus Benutzer wechseln Benutzer wechseln Ja 