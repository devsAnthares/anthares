��          �   %   �      P     Q  $   l  *   �     �     �     �               (  '   @     h  (   �     �  &   �     �  ,     _   2  !   �  \   �  )     J   ;  c   �  >   �  A   )     k  �  n  #     *   8  ?   c     �  *   �     �     	     	  $   )	  +   N	     z	  .   �	  !   �	  /   �	     
  :   6
  r   q
  '   �
  �     E   �  D   �  i   -  Q   �  X   �     B                                                                	                                                         
    

KDE is unable to start.
 $HOME directory (%1) does not exist. $HOME directory (%1) is out of disk space. $HOME not set! Also allow remote connections Halt Without Confirmation Log Out Log Out Without Confirmation Logout canceled by '%1' No read access to $HOME directory (%1). No read access to '%1'. No write access to $HOME directory (%1). No write access to '%1'. Plasma Workspace installation problem! Reboot Without Confirmation Restores the saved user session if available Starts <wm> in case no other window manager is 
participating in the session. Default is 'kwin' Starts the session in locked mode Starts without lock screen support. Only needed if other component provides the lock screen. Temp directory (%1) is out of disk space. The following installation problem was detected
while trying to start KDE: The reliable KDE session manager that talks the standard X11R6 
session management protocol (XSMP). Writing to the $HOME directory (%2) failed with the error '%1' Writing to the temp directory (%2) failed with
    the error '%1' wm Project-Id-Version: ksmserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-05 03:18+0100
PO-Revision-Date: 2017-01-21 14:39+0100
Last-Translator: Burkhard Lück <lueck@hube-lueck.de>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 

KDE kann nicht gestartet werden.
 Der Ordner &HOME (%1) ist nicht vorhanden. Im Ordner $HOME (%1) nicht mehr genug Speicherplatz verfügbar. $HOME ist nicht angegeben Verbindungen mit fremden Rechnern zulassen Ohne Rückfrage herunterfahren Abmelden Ohne Rückfrage abmelden Abmeldung abgebrochen durch „%1“ Kein Lesezugriff auf den Ordner $HOME (%1). Kein Lesezugriff auf „%1“. Kein Schreibzugriff auf den Ordner $HOME (%1). Kein Schreibzugriff auf „%1“. Installationsproblem der Plasma-Arbeitsfläche. Ohne Rückfrage neu starten Stellt die vorherige Sitzung wieder her, falls verfügbar. Startet <wm>, falls keine andere Fensterverwaltung an dieser Sitzung 
beteiligt ist. Voreinstellung ist „kwin“ Startet die Sitzung im gesperrten Modus Startet ohne Unterstützung für die Bildschirmsperre. Dies wird nur benötigt, wenn eine andere Komponente das SPerren des Bildschirms bereitstellt. Im temporären Ordner (%1) nicht mehr genug Speicherplatz verfügbar. Das folgende Installationsproblem wurde beim
Start von KDE entdeckt: Die zuverlässige KDE-Sitzungsverwaltung, der auch das standardisierte X11R6-Protokoll (XSMP) beherrscht. Das Schreiben in den Ordner $HOME (%2) ist mit dem Fehler „%1“ fehlgeschlagen Das Schreiben in den temporären Ordner  (%2) ist mit
dem Fehler „%1“ fehlgeschlagen wm 