��          �      �       0  (   1     Z      q     �     �     �     �     �     �  `     ^   u     �  �  �  (   �     �     �     �     �          -     :     X  :   k  1   �     �        
              	                                    *.kwinscript|KWin scripts (*.kwinscript) Configure KWin scripts EMAIL OF TRANSLATORSYour emails Get New Scripts... Import KWin Script Import KWin script... KWin Scripts KWin script configuration NAME OF TRANSLATORSYour names Placeholder is error message returned from the install serviceCannot import selected script.
%1 Placeholder is name of the script that was importedThe script "%1" was successfully imported. Tamás Krutki Project-Id-Version: kcm-kwin-scripts
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-19 03:12+0100
PO-Revision-Date: 2017-12-04 17:16+0100
Last-Translator: Burkhard Lück <lueck@hube-lueck.de>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 *.kwinscript|KWin-Skripte (*.kwinscript) KWin-Skripte einrichten schwarzer@kde.org Neue Skripte holen ... KWin-Skript importieren KWin-Skript importieren ... KWin-Skripte Einrichtung für KWin-Skripte Frederik Schwarzer Ausgewähltes KWin-Skript kann nicht exportiert werden.
%1 Das Skript „%1“ wurde erfolgreich importiert. Tamás Krutki 