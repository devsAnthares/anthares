��    w      �  �   �      
     
  	   &
     0
     =
     K
     Q
     X
     i
     o
     w
     
     �
     �
     �
     �
  	   �
     �
     �
  
   �
     �
     �
       
             !     (  	   7     A  
   G  
   R     ]     i     x     ~     �     �     �     �  ;   �  -   �  #        ;     T     l  
   �     �     �  
   �     �     �     �               7     K     P  	   k      u     �     �     �  
   �     �     �               .     E     V     f     v     �     �  
   �     �  )   �     �          #     2     A     R     X     `     s  '   �     �     �     �     �     �     
           .  C   <     �  s   �           %     :     Q     f     �     �      �     �  -   �  /        4  	   =  !   G     i      �     �     �     �     �     �  �  �     �  	   �     �     �     �     �     �  	     	     	        #     ?     K     X     m     y     �     �     �     �     �     �     �     �  
             !     .     7     L     \  %   j  
   �     �     �     �  
   �  	   �     �     �     �       
   0     ;  
   J     U      e     �     �  !   �     �     �  &        +     D  8   R     �  /   �     �     �     �     �               2     N     f     |     �     �     �  	   �  /   �            *   .  "   Y  #   |     �     �     �     �     �       5     4   R     �     �  *   �     �     �  !        (     >  V   W     �  }   �  )   C  $   m  %   �  '   �  $   �            $   1     V  @   d  4   �     �     �  /   �  )   %  0   O     �     �     �     �  	   �     o   5   l   W   	             1               R   6          J         >   u   d           V          M   T       X   r   v          G   .   Q   \       a              E   -       O               ^          b   7           +   Y   C      [       4                 H       k   B   c       ]   I   *   P   (   h   w   ;   N   3   
                @      "   S      _                   L   $      9   F      D          m   U   %       K       &                     t   Z   `   !       ,   0   g   f   p          i      2   e       n   /      q       ?      <   A   #   8          =   '   s   )               :   j           %1 &Handbook &About %1 &Actual Size &Add Bookmark &Back &Close &Configure %1... &Copy &Delete &Donate &Edit Bookmarks... &Find... &First Page &Fit to Page &Forward &Go To... &Go to Line... &Go to Page... &Last Page &Mail... &Move to Trash &New &Next Page &Open... &Paste &Previous Page &Print... &Quit &Redisplay &Rename... &Replace... &Report Bug... &Save &Save Settings &Spelling... &Undo &Up &Zoom... @action:button Goes to next tip, opposite to previous&Next @action:button Goes to previous tip&Previous @option:check&Show tips on startup @titleDid you know...?
 @title:windowConfigure @title:windowTip of the Day About &KDE C&lear Check spelling in document Clear List Close document Configure &Notifications... Configure S&hortcuts... Configure Tool&bars... Copy selection to clipboard Create new document Cu&t Cut selection to clipboard Dese&lect EMAIL OF TRANSLATORSYour emails Encodings menuAutodetect Encodings menuDefault F&ull Screen Mode Find &Next Find Pre&vious Fit to Page &Height Fit to Page &Width Go back in document Go forward in document Go to first page Go to last page Go to next page Go to previous page Go up NAME OF TRANSLATORSYour names No Entries Open &Recent Open a document which was recently opened Open an existing document Paste clipboard content Print Previe&w Print document Quit application Re&do Re&vert Redisplay document Redo last undone action Revert unsaved changes made to document Save &As... Save document Save document under a new name Select &All Select zoom level Send document by mail Show &Menubar Show &Toolbar Show Menubar<p>Shows the menubar again after it has been hidden</p> Show St&atusbar Show Statusbar<p>Shows the statusbar, which is the bar at the bottom of the window used for status information.</p> Show a print preview of document Show or hide menubar Show or hide statusbar Show or hide toolbar Switch Application &Language... Tip of the &Day Undo last action View document at its actual size What's &This? You are not allowed to save the configuration You will be asked to authenticate before saving Zoom &In Zoom &Out Zoom to fit page height in window Zoom to fit page in window Zoom to fit page width in window go back&Back go forward&Forward home page&Home show help&Help without name Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-16 03:10+0100
PO-Revision-Date: 2017-09-25 15:13+0100
Last-Translator: Burkhard Lück <lueck@hube-lueck.de>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 &Handbuch zu %1 Ü&ber %1 Origin&algröße Lesezeichen hin&zufügen &Zurück Schl&ießen %1 ein&richten ... &Kopieren &Löschen &Spenden  Lesezeichen &bearbeiten ... &Suchen ... &Erste Seite Auf Seite ein&passen Nach &vorne &Gehe zu ... Gehe zu &Zeile ... Gehe zu &Seite ... &Letzte Seite Ver&senden ... In den &Papierkorb werfen &Neu &Nächste Seite Öff&nen ... &Einfügen V&orherige Seite &Drucken ... Be&enden Anzeige &auffrischen &Umbenennen ... &Ersetzen ... Probleme oder Wünsche be&richten ... S&peichern &Einstellungen speichern Rech&tschreibung ... &Rückgängig Nach &oben &Zoom ... &Weiter &Zurück Tipps beim &Start anzeigen Wussten Sie schon ...?
 Einrichten Tipp des Tages Über &KDE Alles &löschen Rechtschreibprüfung im Dokument Liste leeren Dokument schließen &Benachrichtigungen festlegen ... &Kurzbefehle festlegen ... Werk&zeugleisten einrichten ... Auswahl in die Zwischenablage kopieren Neues Dokument erstellen &Ausschneiden Auswahl ausschneiden und in die Zwischenablage einfügen Auswahl &aufheben tr@erdfunkstelle.de, hunsum@gmx.de, thd@kde.org Autom. feststellen Standard &Vollbildmodus &Weitersuchen Frü&here suchen Auf Seiten&höhe einpassen Auf Seiten&breite einpassen Rückwärts im Dokument Vorwärts im Dokument Zur ersten Seite Zur letzten Seite Zur nächsten Seite Zur vorherigen Seite Nach oben Thomas Reitelbach, Stephan Johach, Thomas Diehl Keine Einträge Zu&letzt geöffnete Dateien Ein kürzlich geöffnetes Dokument öffnen Ein existierendes Dokument öffnen Inhalt der Zwischenablage einfügen Druck&vorschau Dokument drucken Die Anwendung beenden Wieder&herstellen &Zuletzt gespeicherte Fassung Dokument neu anzeigen Letzte rückgängig gemachte Aktion wieder herstellen Nicht gespeicherte Änderungen am Dokument verwerfen Speichern &unter ... Dokument speichern Dokument unter einem neuen Namen speichern A&lles auswählen Vergrößerung auswählen Das Dokument als E-Mail versenden &Menüleiste anzeigen &Werkzeugleiste anzeigen Menüleiste anzeigen<p>Zeigt die Leiste wieder an, nachdem sie ausgeblendet wurde.</p> &Statusleiste anzeigen Statusleiste anzeigen<p>Blendet die Leiste ein, die am unteren Rand eines Fensters Auskunft über Programmvorgänge gibt.</p> Eine Druckvorschau des Dokuments anzeigen Menüleiste anzeigen oder ausblenden Statusleiste anzeigen oder ausblenden Werkzeugleiste anzeigen oder ausblenden Sprache der Anwendung umschalten ... &Tipp des Tages Letzte Aktion zurücknehmen Dokument in Originalgröße anzeigen Was ist &das? Sie haben nicht die Berechtigung, die Konfiguration zu speichern Vor dem Speichern müssen Sie sich authentifizieren. Ver&größern Ver&kleinern Skalieren um Seitenhöhe in Fenster einzupassen Skalieren um Seite in Fenster einzupassen Skalieren um Seitenbreite in Fenster einzupassen &Zurück Nach &vorne &Startseite &Hilfe ohne Name 