��            )   �      �  ;   �  J   �          /  ~   7  A   �     �       )        G     X     i      q     �     �     �  
   �     �  
   �     �             "   <     _  	   y     �     �     �  �  �     j     r     v     �  �   �  W     
   q     |  F   �  "   �     	     %	     -	     E	     Y	     i	     w	     �	  
   �	     �	     �	  $   �	  1   �	     %
  	   B
     L
     a
     u
                                      
                                                                                            	        %1 is the full user name, %2 is the user login name%1 (%2) %1 is the name of a detail about the current action provided by polkit%1: (c) 2009 Red Hat, Inc. Action: An application is attempting to perform an action that requires privileges. Authentication is required to perform this action. Another client is already authenticating, please try again later. Application: Authentication Required Authentication failure, please try again. Click to edit %1 Click to open %1 Details EMAIL OF TRANSLATORSYour emails Former maintainer Jaroslav Reznik Lukáš Tinkl Maintainer NAME OF TRANSLATORSYour names P&assword: Password for %1: Password for root: Password or swipe finger for %1: Password or swipe finger for root: Password or swipe finger: Password: PolicyKit1 KDE Agent Select User Vendor: Project-Id-Version: polkit-kde-authentication-agent-1
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:22+0100
PO-Revision-Date: 2015-01-13 19:21+0100
Last-Translator: Burkhard Lück <lueck@hube-lueck.de>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 %1 (%2) %1: © 2009, Red Hat, Inc. Aktion: Eine Anwendung versucht, eine Aktion auszuführen, die erweiterte Rechte benötigt. Für diese Aktion müssen Sie sich berechtigen. Ein anderer Rechner versucht gerade sich zu berechtigen. Bitte versuchen Sie es erneut. Anwendung: Authentifizierung erforderlich Die Berechtigung ist abgewiesen worden. Bitte versuchen Sie es erneut. Klicken, um „%1“ zu bearbeiten Klicken, um „%1“ zu öffnen Details johannesobermayr@gmx.de Ehemaliger Betreuer Jaroslav Reznik Lukáš Tinkl Betreuer Johannes Obermayr P&asswort: Passwort für %1: Passwort des Systemverwalters: Passwort oder Fingerabdruck für %1: Passwort oder Fingerabdruck des Systemverwalters: Passwort oder Fingerabdruck: Passwort: PolicyKit1-KDE-Agent Benutzer auswählen Hersteller: 