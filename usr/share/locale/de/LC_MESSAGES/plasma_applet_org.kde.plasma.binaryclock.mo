��    	      d      �       �   
   �      �      �   	              "     C  "   ]  �  �     6     G     O     a     q     �     �     �                             	                 Appearance Colors: Display seconds Draw grid Show inactive LEDs: Use custom color for active LEDs Use custom color for grid Use custom color for inactive LEDs Project-Id-Version: plasma_applet_binaryclock
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-31 05:47+0100
PO-Revision-Date: 2017-05-12 20:46+0100
Last-Translator: Burkhard Lück <lueck@hube-lueck.de>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 Erscheinungsbild Farben: Sekunden anzeigen Gitter zeichnen Inaktive LEDs anzeigen: Eigene Farbe für aktive LEDs Eigene Farbe für Gitter Eigene Farbe für inaktive LEDs 