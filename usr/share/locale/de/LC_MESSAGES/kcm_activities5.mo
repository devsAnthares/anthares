��    *      l  ;   �      �     �     �  )   �               /     H     ^  #   ~     �  
   �     �     �  %   �  +        E     Z     m  @   z     �     �     �     �               '     ,     9     ?     _     e  .   m     �  F   �  (   �  	   '  	   1  	   ;  '   E  7   m  "   �  �  �     e	     s	  .   �	     �	  	   �	     �	  	   �	     �	     
     $
     8
     E
     `
  (   v
  B   �
     �
     �
       R   "     u     �     �     �  "   �  	   �     �                      8     @  :   N     �  R   �  2   �     ,     8     A     Q     W  
   f                   $                                 %                      *                                               	   #   )   "             &           
                        '             (   !    &Do not remember @actionWalk through activities @actionWalk through activities (Reverse) @action:buttonApply @action:buttonCancel @action:buttonChange... @action:buttonCreate @title:windowActivity Settings @title:windowCreate a New Activity @title:windowDelete Activity Activities Activity information Activity switching Are you sure you want to delete '%1'? Blacklist all applications not on this list Clear recent history Create activity... Description: Error loading the QML files. Check your installation.
Missing %1 For a&ll applications Forget a day Forget everything Forget the last hour Forget the last two hours General Icon Keep history Name: O&nly for specific applications Other Privacy Private - do not track usage for this activity Remember opened documents: Remember the current virtual desktop for each activity (needs restart) Shortcut for switching to this activity: Shortcuts Switching Wallpaper for in 'keep history for 5 months'for  unit of time. months to keep the history month  months unlimited number of monthsforever Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-03-31 13:35+0100
Last-Translator: Burkhard Lück <lueck@hube-lueck.de>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Nicht &merken Zwischen Aktivitäten wechseln Zwischen Aktivitäten wechseln (Gegenrichtung) Anwenden Abbrechen Ändern ... Erstellen Aktivitäten-Einstellungen Eine neue Aktivität erstellen Aktivität löschen Aktivitäten Aktivitäten-Informationen Aktivitäten wechseln Möchten Sie „%1“ wirklich löschen? Alle nicht in dieser Liste aufgeführten Anwendungen ausschließen Jüngsten Verlauf leeren Aktivität erstellen ... Beschreibung: Fehler beim Laden der QML-Dateien. Überprüfen Sie Ihre Installation.
Es fehlt %1 Für a&lle Anwendungen Einen Tag verwerfen Alles verwerfen Die letzte Stunde verwerfen Die letzten zwei Stunden verwerfen Allgemein Symbol Verlauf behalten Name: &Nur für angegebene Anwendungen Weitere Privatsphäre Privat - Die Benutzung dieser Aktivität nicht aufzeichnen Geöffnete Dokumente merken: Aktuelle virtuelle Arbeitsfläche für jede Aktivität merken (erfordert Neustart) Kurzbefehl für das Wechseln zu dieser Aktivität: Kurzbefehle Wechseln Hintergrundbild für   Monat  Monate Unbegrenzt 