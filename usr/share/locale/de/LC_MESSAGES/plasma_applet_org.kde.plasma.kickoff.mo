��            )   �      �     �  
   �  /   �  -   �          !  
   2     =     J     `  W   i     �  ,   �  	                  !     '     -  
   :     E     W     o     �     �     �     �  1   �       �       �  
   �     �     �     �               &      2     S  �   [     �  4     	   I     S     n     v  	   ~     �     �     �     �     �     �     	      	  (   7	     `	     }	                                                                                  
                                	                       %1@%2 %2@%3 (%1) @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Favorites All Applications Appearance Applications Applications updated. Computer Drag tabs between the boxes to show/hide them, or reorder the visible tabs by dragging. Edit Applications... Expand search to bookmarks, files and emails Favorites Hidden Tabs History Icon: Leave Menu Buttons Often Used On All Activities On The Current Activity Remove from Favorites Show In Favorites Show applications by name Sort alphabetically Switch tabs on hover Type is a verb here, not a nounType to search... Visible Tabs Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2017-12-04 17:49+0100
Last-Translator: Burkhard Lück <lueck@hube-lueck.de>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 %1@%2 %2@%3 (%1) Auswählen ... Symbol zurücksetzen Zu Favoriten hinzufügen Alle Anwendungen Erscheinungsbild Anwendungen Anwendungen wurden aktualisiert. Rechner Verschieben Sie die Karteikarten zwischen den Feldern, um sie anzuzeigen oder auszublenden. Die Reihenfolge der Karteikarten können Sie durch Ziehen ändern Anwendungen bearbeiten ... Suche auf Lesezeichen, Dateien und E-Mails erweitern Favoriten Ausgeblendete Karteikarten Verlauf Symbol: Verlassen Menüknöpfe Häufig verwendete In allen Aktivitäten In der aktuellen Aktivität Aus Favoriten entfernen In Favoriten anzeigen Anwendungsname anzeigen Alphabetisch sortieren Karteikarten bei Mausberührung wechseln Tippen Sie, um zu suchen ... Sichtbare Karteikarten 