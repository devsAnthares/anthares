��            )   �      �  !   �  "   �  '   �  ,     #   ;  &   _  !   �  &   �  '   �     �                 V   $  1   {     �  *   �     �        
     
   "     -     6     ;     D     T     [     a     g  �  p          %     +     =     U  
   \  
   g     r  
   �     �     �     �  
   �  q   �  4   4     i  4   v  +   �  #   �     �     	  
   '	     2	  
   8	     C	     ]	  	   d	     n	     u	                                                                          
                                                    	           @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Borders @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large Application menu Border si&ze: Buttons Close Close by double clicking:
 To open the menu, keep the button pressed until it appears. Close windows by double clicking &the menu button Context help Drag buttons between here and the titlebar Drop here to remove button Get New Decorations... Keep above Keep below Maximize Menu Minimize On all desktops Search Shade Theme Titlebar Project-Id-Version: kcmkwindecoration
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-04-14 02:49+0200
PO-Revision-Date: 2015-10-25 12:33+0100
Last-Translator: Burkhard Lück <lueck@hube-lueck.de>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 Riesig Groß Keine Umrandungen Keine Seitenumrandungen Normal Übergroß Sehr klein Noch riesiger Sehr groß Anwendungsmenü &Umrandungsgröße: Knöpfe Schließen Schließen durch doppelklicken:
 Um das Menü zu öffnen, halten Sie den Knopf gedrückt, bis da Menü erscheint. Fens&ter durch Doppelklick auf Menüknopf schließen Kontexthilfe Ziehen Sie Knöpfe zwischen hier und der Titelleiste Ziehen Sie hierher, um Knöpfe zu entfernen Neue Dekorationen herunterladen ... Im Vordergrund halten Im Hintergrund halten Maximieren Menü Minimieren Auf allen Arbeitsflächen Suchen Einrollen Design Titelleiste 