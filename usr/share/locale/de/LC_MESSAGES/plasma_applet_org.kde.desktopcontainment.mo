��    `        �         (     )     1     B     Q     W     ^     j     {     �     �  /   �  -   �     �     �  
   		  
   	     	     ,	     1	     8	     @	     R	     _	     d	  
   l	     w	     �	     �	     �	  	   �	     �	  	   �	     �	     �	     �	     
     
  	   #
     -
     4
     H
  	   M
     W
     ]
     c
     h
     m
  	   v
     �
     �
     �
     �
     �
     �
     �
     �
  <   �
               /     6     =     X     ^     e     j  
   ~     �     �     �     �     �  )   �               5     :     @     M     U     ]     f  
   x     �     �  	   �     �     �     �     �     �     �     �  E   �  *   A  �  l  	   	          &     @  
   I     T     c     �     �     �     �     �  
   �     �     �     �  
          	        (     0     J     d     j  
   s     ~     �     �  8   �     �     �                         0     >     L  
   `     k     �     �     �     �     �     �     �     �     �     �     �  "   �                      \   &     �     �  	   �     �     �     �     �     �     �     �                4     J     d  B   r     �  3   �     	               #     2     A     M  
   ]     h     o     v          �     �     �     �     �     �  c        i     J       -      M       %   Q           ^              U       L   S   $      .       H       X          Y   8   *         <   &          '       ]   N       3                     `          #   V             /   T   G   9   @   >   =   6   2   O   C   F   ?      0      
      A   E       	      P              _                D          Z               1   R   ,   +                \           I   )   B   K      :   !   W   4          ;               (      5   "       7       [           &Delete &Empty Trash Bin &Move to Trash &Open &Paste &Properties &Refresh Desktop &Refresh View &Reload &Rename @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Align Appearance: Arrange In Arrange in Arrangement: Back Cancel Columns Configure Desktop Custom title Date Default Descending Description Deselect All Desktop Layout Enter custom title here Features: File name pattern: File type File types: Filter Folder preview popups Folders First Folders first Full path Got it Hide Files Matching Huge Icon Size Icons Large Left List Location Location: Lock in place Locked Medium More Preview Options... Name None OK Panel button: Press and hold widgets to move them and reveal their handles Preview Plugins Preview thumbnails Remove Resize Restore from trashRestore Right Rotate Rows Search file type... Select All Select Folder Selection markers Show All Files Show Files Matching Show a place: Show files linked to the current activity Show the Desktop folder Show the desktop toolbox Size Small Small Medium Sort By Sort by Sorting: Specify a folder: Text lines Tiny Title: Tool tips Tweaks Type Type a path or a URL here Unsorted Use a custom icon Widget Handling Widgets unlocked You can press and hold widgets to move them and reveal their handles. whether to use icon or list viewView mode Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:11+0100
PO-Revision-Date: 2017-12-04 17:20+0100
Last-Translator: Burkhard Lück <lueck@hube-lueck.de>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 &Löschen Papierkorb &leeren In den &Papierkorb werfen Ö&ffnen &Einfügen &Eigenschaften Arbeitsfläche neu &aufbauen Ansicht a&ktualisieren A&ktualisieren &Umbenennen Auswählen ... Symbol zurücksetzen Ausrichten Erscheinungsbild: Anordnen in Anordnen in Anordnung: Zurück Abbrechen Spalten Arbeitsfläche einrichten Benutzerdefinierter Titel Datum Standard Absteigend Beschreibung Auswahl aufheben Desktop-Layout Bitte geben Sie hier einen benutzerdefinierten Titel ein Funktionen: Dateinamenmuster: Dateityp Dateitypen: Filter Ordner-Voransicht Ordner zuerst Ordner zuerst Vollständiger Pfad Verstanden Passende Dateien ausblenden Riesig Symbolgröße Symbole Groß Links Liste Ort Ort: Position sperren Gesperrt Mittel Weitere Vorschau-Einstellungen ... Name Kein OK Kontrollleistenknopf: Klicken und halten Sie ein Miniprogramm, um es zu bewegen oder seine Bedienleiste anzuzeigen Modul-Vorschau Vorschaubilder anzeigen Entfernen Größe ändern Wiederherstellen Rechts Drehen Zeilen Dateityp suchen ... Alle auswählen Ordner auswählen Auswahlmarkierungen Alle Dateien anzeigen Passende Dateien anzeigen Ort anzeigen: Dateien anzeigen, die mit der aktuellen Aktivität verknüpft sind Arbeitsflächen-Ordner anzeigen Anzeige des Werkzeugkastens für die Arbeitsfläche Größe Klein Mittelklein Sortieren nach Sortieren nach Sortierung: Ordner angeben: Textzeilen Winzig Titel: Kurzinfo Optimierungen Typ Pfad oder URL hier eingeben Nicht sortiert Eigenes Symbol verwenden Miniprogramm-Handhabung Miniprogramme entsperrt Sie können Miniprogramme klicken und halten, um sie zu bewegen oder ihre Bedienleisten anzuzeigen. Ansichtsmodus 