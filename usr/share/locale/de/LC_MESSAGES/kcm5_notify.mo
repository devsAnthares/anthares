��          �      �       0     1  �  H     �       &         ?     `     n     v     �     �  (   �  �  �     �    �     �     �  -   �     �                (     5     E  -   `         
                 	                                (c) 2002-2006 KDE Team <h1>System Notifications</h1>Plasma allows for a great deal of control over how you will be notified when certain events occur. There are several choices as to how you are notified:<ul><li>As the application was originally designed.</li><li>With a beep or other noise.</li><li>Via a popup dialog box with additional information.</li><li>By recording the event in a logfile without any additional visual or audible alert.</li></ul> Carsten Pfeiffer Charles Samuels Disable sounds for all of these events EMAIL OF TRANSLATORSYour emails Event source: KNotify NAME OF TRANSLATORSYour names Olivier Goffart Original implementation System Notification Control Panel Module Project-Id-Version: kcmnotify
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-09 07:18+0200
PO-Revision-Date: 2017-11-26 14:02+0100
Last-Translator: Burkhard Lück <lueck@hube-lueck.de>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 © 2002–2006 das KDE-Team <h1>Systemnachrichten</h1>Plasma ermöglicht Ihnen eine weitreichende Kontrolle, wie Sie beim Eintritt bestimmter Ereignisse benachrichtigt werden. Es gibt folgende Wahlmöglichkeiten, wie Sie benachrichtigt werden können: <ul> <li>Wie vom Programm vorgesehen</li> <li>Mit einem Signalton oder einem anderen Geräusch</li> <li>Durch das Anzeigen eines Dialogs mit Zusatzinformationen</li> <li>Durch das Aufzeichnen des Ereignisses in einer Protokolldatei ohne weitere visuelle oder akustische Nachricht</li> </ul> Carsten Pfeiffer Charles Samuels Sound für alle diese Ereignisse deaktivieren thd@kde.org Quelle für das Ereignis: KNotify Thomas Diehl Olivier Goffart Ursprüngliche Einrichtung Kontrollleistenbereich für Systemnachrichten 