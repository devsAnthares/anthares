��          �   %   �      0  *   1     \  .   s     �     �     �     �     �     �                 
   $     /     5     =     E     ]     t     �     �     �     �  �  �  -   �     �  4   �            $   .     S     r     �     �     �  	   �  
   �     �     �  	   �     �  "   �  -     #   I     m     �  	   �        
                             	                                                                                          %1 Minimized Window: %1 Minimized Windows: %1 Window: %1 Windows: ...and %1 other window ...and %1 other windows Activity name Activity number Add Virtual Desktop Configure Desktops... Desktop name Desktop number Display: Does nothing General Horizontal Icons Layout: No text Only the current screen Remove Virtual Desktop Selecting current desktop: Show Activity Manager... Shows desktop The pager layoutDefault Vertical Project-Id-Version: plasma_applet_pager
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-19 03:29+0200
PO-Revision-Date: 2016-11-22 20:33+0100
Last-Translator: Frederik Schwarzer <schwarzer@kde.org>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 %1 Minimiertes Fenster: %1 Minimierte Fenster %1 Fenster: %1 Fenster .. und %1 weiteres Fenster .. und %1 weitere Fenster Name der Aktivität Nummer der Aktivität Virtuelle Arbeitsfläche hinzufügen Arbeitsflächen einrichten ... Arbeitsflächenname Arbeitsflächennummer Anzeige: Nichts Allgemein Waagerecht Symbole Layout: Kein Text Nur den aktuellen Bildschirm Virtuelle Arbeitsfläche entfernen Auswahl der aktuellen Arbeitsfläche bewirkt: Aktivitätenverwaltung anzeigen ... Zeigt die Arbeitsfläche an Standard Senkrecht 