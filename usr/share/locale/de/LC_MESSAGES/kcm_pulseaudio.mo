��    $      <  5   \      0     1     6  )   J     t  $   �  &   �  #   �  !   �  "   !     D     T     f     z     �  J   �     �  L        [     c     k      {     �  
   �     �     �     �     �     �  "   �       )   9  <   c     �  ;   �     �  �       �     �     �     �     �      	      	     @	     `	     �	  
   �	  	   �	     �	     �	  ]   �	     
  W   .
     �
     �
     �
     �
  	   �
     �
     �
     �
     �
  
                  ,  1   B  I   t     �     �     �     	                                                                         !       #             "                                        
           $                               100% @info:creditAuthor @info:creditCopyright 2015 Harald Sitter @info:creditHarald Sitter @labelNo Applications Playing Audio @labelNo Applications Recording Audio @labelNo Device Profiles Available @labelNo Input Devices Available @labelNo Output Devices Available @labelProfile: @titlePulseAudio @title:tabAdvanced @title:tabApplications @title:tabDevices Add virtual output device for simultaneous output on all local sound cards Advanced Output Configuration Automatically switch all running streams when a new output becomes available Capture Default Device Profiles EMAIL OF TRANSLATORSYour emails Inputs Mute audio NAME OF TRANSLATORSYour names Notification Sounds Outputs Playback Port Port is unavailable (unavailable) Port is unplugged (unplugged) Requires 'module-gconf' PulseAudio module This module allows to set up the Pulseaudio sound subsystem. label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-03 03:00+0200
PO-Revision-Date: 2017-05-15 12:25+0100
Last-Translator: Burkhard Lück <lueck@hube-lueck.de>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 100 % Autor Copyright 2015 Harald Sitter Harald Sitter Keine Anwendung spielt Audio Keine Anwendung nimmt Audio auf Keine Geräteprofile verfügbar Keine Eingabegeräte verfügbar Keine Ausgabegeräte verfügbar Profil: PulseAudio Erweitert Anwendungen Geräte Virtuelles Ausgabegerät für gleichzeitige Ausgabe auf allen lokalen Soundkarten hinzufügen Erweiterte Ausgabe-Einrichtung Automatisch alle laufenden Streams zur neuen Ausgabe wechseln, wenn sie verfügbar wird Aufnahme Standard Geräteprofile lueck@hube-lueck.de Eingänge Audio stummschalten Burkhard Lück Benachrichtigungsklang Ausgaben Wiedergabe Port  (Nicht verfügbar) (Nicht angeschlossen) Benötigt das PulseAudio-Modul „module-gconf“ Dieses Modul ermöglicht die Einrichtung des PulseAudio-Sound-Subsystems. %1: %2 100 % %1 % 