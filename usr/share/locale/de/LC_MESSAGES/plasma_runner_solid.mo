��          �      L      �  m   �     /  "   <  F   _  F   �  F   �  J   4  N     R   �     !  %   2  $   X  #   }  $   �  %   �  &   �  �        �  �  �     ^     p  )   �  ]   �  T   	  U   ^  U   �  l   
	  p   w	     �	     �	     
     
     
     
     
     %
     :
                                
                          	                                          Close the encrypted container; partitions inside will disappear as they had been unpluggedLock the container Eject medium Finds devices whose name match :q: Lists all devices and allows them to be mounted, unmounted or ejected. Lists all devices which can be ejected, and allows them to be ejected. Lists all devices which can be mounted, and allows them to be mounted. Lists all devices which can be unmounted, and allows them to be unmounted. Lists all encrypted devices which can be locked, and allows them to be locked. Lists all encrypted devices which can be unlocked, and allows them to be unlocked. Mount the device Note this is a KRunner keyworddevice Note this is a KRunner keywordeject Note this is a KRunner keywordlock Note this is a KRunner keywordmount Note this is a KRunner keywordunlock Note this is a KRunner keywordunmount Unlock the encrypted container; will ask for a password; partitions inside will appear as they had been plugged inUnlock the container Unmount the device Project-Id-Version: plasma_runner_solid
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2017-02-27 20:23+0100
Last-Translator: Burkhard Lück <lueck@hube-lueck.de>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 Container sperren Medium auswerfen Findet Geräte, deren Name auf :q: passt. Zeigt alle Geräte an und ermöglicht das Ein- und Aushängen oder das Auswerfen des Mediums. Zeigt alle Geräte an, die ausgeworfen werden können und ermöglicht das Auswerfen. Zeigt alle Geräte an, die eingehängt werden können und ermöglicht das Einhängen. Zeigt alle Geräte an, die ausgehängt werden können und ermöglicht das Aushängen. Zeigt alle verschlüsselten Geräte an, die gesperrt werden können und ermöglicht das Sperren der Geräte. Zeigt alle verschlüsselten Geräte an, die entsperrt werden können und ermöglicht das Entsperren der Geräte. Das Gerät einhängen device eject lock mount unlock unmount Container entsperren Das Gerät aushängen 