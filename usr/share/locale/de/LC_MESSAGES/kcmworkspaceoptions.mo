��    	      d      �       �      �   -   �   *   %      P  '   q  
   �     �     �  �  �     m  5   �  4   �     �  4         5     >     K                   	                           (c) 2009 Marco Martin Display informational tooltips on mouse hover Display visual feedback for status changes EMAIL OF TRANSLATORSYour emails Global options for the Plasma Workspace Maintainer Marco Martin NAME OF TRANSLATORSYour names Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-08 06:27+0100
PO-Revision-Date: 2017-12-04 17:19+0100
Last-Translator: Burkhard Lück <lueck@hube-lueck.de>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 © 2009, Marco Martin Kurzinfos bei Überfahren mit dem Mauszeiger anzeigen Visuelle Rückmeldung bei Statusänderungen anzeigen schwarzer@kde.org Globale Einstellungen für die Plasma-Arbeitsfläche Betreuer Marco Martin Frederik Schwarzer 