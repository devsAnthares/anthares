��    &      L  5   |      P      Q     r     ~  -   �  #   �  #   �  ?     1   S     �     �     �  H   �  L   �     F  V   N  N   �     �  
                   (     >     W  2   _  
   �     �  )   �  8   �  #      .   D  -   s  D   �  6   �  0     A   N  =   �  9   �  �  	  )   �
     �
  *   �
  9   $  
   ^  
   i     t     �     �     �     �     �     �       	        "     '     5     L     b     |  %   �     �     �     �  '   �  1        H  :   [  C   �     �     �                          3                  "                            
       $                      !                &                                      %      	           #                                %1 notification %1 notifications %1 of %2 %3 %1 running job %1 running jobs &Configure Event Notifications and Actions... 10 seconds ago, keep short10 s ago 30 seconds ago, keep short30 s ago A button tooltip; expands the item to show detailsShow Details A button tooltip; hides item detailsHide Details Clear Notifications Copy Copy Link Address Either just 1 dir or m of n dirs are being processed1 dir %2 of %1 dirs Either just 1 file or m of n files are being processed1 file %2 of %1 files History How much many bytes (or whether unit in the locale has been copied over total%1 of %2 Indicator that there are more urls in the notification than previews shown+%1 Information Job Failed Job Finished More Options... No new notifications. No notifications or jobs Open... Placeholder is job name, eg. 'Copying'%1 (Paused) Select All Show a history of notifications Show application and system notifications Speed and estimated time to completion%1 (%2 remaining) Track file transfers and other jobs Use custom position for the notification popup minutes ago, keep short%1 min ago %1 min ago notification was added n days ago, keep short%1 day ago %1 days ago notification was added yesterday, keep shortYesterday notification was just added, keep shortJust now placeholder is row description, such as Source or Destination%1: the job, which can be anything, failed to complete%1: Failed the job, which can be anything, has finished%1: Finished Project-Id-Version: plasma_applet_notifications
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:07+0100
PO-Revision-Date: 2018-02-11 22:06+0100
Last-Translator: Frederik Schwarzer <schwarzer@kde.org>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 %1 Benachrichtigung %1 Benachrichtigungen %1 von %2 %3 %1 laufender Vorgang %1 laufende Vorgänge Ereignis-Bena&chrichtigungen und -Aktionen einrichten ... Vor 10 sec Vor 30 sec Details anzeigen Details ausblenden Benachrichtigungen löschen Kopieren Verknüpfungsadresse kopieren 1 Ordner %2 von %1 Ordnern 1 Datei %2 von %1 Dateien Verlauf %1 von %2 +%1: Informationen Vorgang fehlgeschlagen Vorgang abgeschlossen Weitere Einstellungen ... Keine neuen Benachrichtigungen. Keine Benachrichtigungen und Aufgaben Öffnen ... %1 (Angehalten) Alle auswählen Verlauf der Benachrichtigungen anzeigen Anwendungs- und Systembenachrichtigungen anzeigen %1 (%2 verbleiben) Überwachung von Dateiübertragungen und anderer Vorgänge Benutzerdefinierte Position für Benachrichtigungsfenster verwenden Vor %1 min Vor %1 min Vor %1 Tag Vor %1 Tagen Gestern Gerade eben %1: %1: Fehlgeschlagen %1: Abgeschlossen 