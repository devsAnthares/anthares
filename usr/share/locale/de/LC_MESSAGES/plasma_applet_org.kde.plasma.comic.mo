��    =        S   �      8  ;   9     u     �     �     �  6   �  A   
     L     U  $   Y  
   ~     �  #   �     �     �     �     �     �  7        >     [     w  '   �     �     �  $   �  ,   �          3     C     K     ]     y     �     �     �     �     �  �   �  9   �	  "   �	     �	     �	     
     *
     <
     R
     c
  %   u
     �
     �
     �
     �
     �
  
   �
  7        :     T     l     t  �  �  a   5      �     �     �     �             	   .     8  8   =     v  .   �  (   �     �     �               .  4   =  $   r     �     �  >   �     �       6     @   U     �     �  	   �     �  (   �               (     F     a     w  �   �  @   �  (   �     �  #   �          .     E     \     q  #   �     �     �     �     �     �  
     3        A     F     K     S     !          3      0      (                 1      "          =   .   6                                         	      :          5       ,   #   &   7       ;   '             -   
             8                 *           %           <   4   9                )   /   $          2             +       

Choose the previous strip to go to the last cached strip. &Create Comic Book Archive... &Save Comic As... &Strip Number: *.cbz|Comic Book Archive (Zip) @option:check Context menu of comic image&Actual Size @option:check Context menu of comic imageStore current &Position Advanced All An error happened for identifier %1. Appearance Archiving comic failed Automatically update comic plugins: Cache Check for new comic strips: Comic Comic cache: Configure... Could not create the archive at the specified location. Create %1 Comic Book Archive Creating Comic Book Archive Destination: Display error when getting comic failed Download Comics Error Handling Failed adding a file to the archive. Failed creating the file with identifier %1. From beginning to ... From end to ... General Get New Comics... Getting comic strip failed: Go to Strip Information Jump to &current Strip Jump to &first Strip Jump to Strip ... Manual range Maybe there is no Internet connection.
Maybe the comic plugin is broken.
Another reason might be that there is no comic for this day/number/string, so choosing a different one might work. Middle-click on the comic to show it at its original size No zip file is existing, aborting. Range: Show arrows only on mouse over Show comic URL Show comic author Show comic identifier Show comic title Strip identifier: The range of comic strips to archive. Update Visit the comic website Visit the shop &website an abbreviation for Number# %1 days dd.MM.yyyy here strip means comic strip&Next Tab with a new Strip in a range: from toFrom: in a range: from toTo: minutes strips per comic Project-Id-Version: plasma_applet_comic
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-07-18 03:20+0200
PO-Revision-Date: 2015-03-09 22:13+0100
Last-Translator: Burkhard Lück <lueck@hube-lueck.de>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 

Wählen Sie den vorangegangen Comic aus, um den zuletzt zwischengespeicherten Comic anzuzeigen. Comic-Buch-Archiv &erstellen ... Comic &speichern unter ... &Comic-Nummer: *.cbz|Comic-Buch-Archiv (Zip) &Originalgröße Aktuelle &Position speichern Erweitert Alle Es ist ein Fehler für die Kennung „%1“ aufgetreten. Erscheinungsbild Das Archivieren des Comics ist fehlgeschlagen: Comics-Module automatisch aktualisieren: Zwischenspeicher Nach neuen Comic-Seiten suchen: Comic Comic-Zwischenspeicher: Einrichten ... Am angegebenen Ort kann kein Archiv erstellt werden. „%1“ Comic-Buch-Archiv erstellen Comic-Buch-Archiv erstellen Ziel: Fehler anzeigen, wenn das Holen des Comics fehlgeschlagen ist: Comics herunterladen Fehlerbehandlung Hinzufügen einer Datei zum Archiv ist fehlgeschlagen. Erstellen der Datei mit der Kennung „%1“ ist fehlgeschlagen. Vom Anfang bis ... Vom Ende bis ... Allgemein Neue Comics herunterladen ... Das Holen des Comics ist fehlgeschlagen: Comic anzeigen Informationen Zum &aktuellen Comic springen Zum &ersten Comic springen Zu Comic springen ... Benutzerdefinierter Bereich Möglicherweise besteht keine Internetverbindung
oder das Comic-Modul funktioniert nicht.
Ein weiterer Grund könnte sein, dass für diese(n) Tag/Nummer/Zeichenkette kein Comic vorhanden ist und ein anderer möglicherweise funktioniert. Mittelklick auf den Comic zeigt ihn in seiner Originalgröße an Abbrechen, da keine ZIP-Datei existiert. Bereich: Pfeile nur bei Mauskontakt anzeigen Comic-Adresse anzeigen Comic-Autoren anzeigen Comic-Kennung anzeigen Comic-Titel anzeigen Kennung der Comic-Seite: Die zu archivierenden Comic-Seiten. Aktualisieren Webseite des Comics besuchen &Webseite des Shops besuchen Nr. %1 Tage dd.MM.yyyy &Nächste Registerkarte mit einer neuen Comic-Seite Von: Bis: Minuten Comic-Seiten pro Comic 