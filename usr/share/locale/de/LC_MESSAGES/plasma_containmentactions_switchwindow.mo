��          T      �       �   "   �      �   *   �   3   '  E   [  .   �  �  �  0   �  $   �  5   �       $   4     Y                                        Display a submenu for each desktop Display all windows in one list Display only the current desktop's windows plasma_containmentactions_switchwindowAll Desktops plasma_containmentactions_switchwindowConfigure Switch Window Plugin plasma_containmentactions_switchwindowWindows Project-Id-Version: plasma_containmentactions_switchwindow
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-30 03:13+0200
PO-Revision-Date: 2014-08-03 17:42+0200
Last-Translator: Burkhard Lück <lueck@hube-lueck.de>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 Ein Untermenü für jede Arbeitsfläche anzeigen Alle Fenster in einer Liste anzeigen Nur die Fenster der aktuellen Arbeitsfläche anzeigen Alle Arbeitsflächen Modul für Fensterwechsel einrichten Fenster 