��    %      D  5   l      @     A     E     G  	   Y     c     |     �     �     �     �     �     �            S   ,  w   �     �  M         [     |  ?   �     �  	   �     �     
     #  %   2     X     e  F   �  #   �     �  ]     R   f     �     �  �  �     �	     �	     �	     �	     �	     �	     �	  
   �	     
  "   !
     D
     _
     x
     �
  X   �
  ]   �
     V  H   h     �     �  9   �          !  +   .     Z     l  O   �     �     �  D   �  &   :     a  t   {  X   �     I     [               !      "          #   %                                                                            $                          	                
                            ms % %1 - All Desktops %1 - Cube %1 - Current Application %1 - Current Desktop %1 - Cylinder %1 - Sphere &Reactivation delay: &Switch desktop on edge: Activation &delay: Active Screen Corners and Edges Activity Manager Always Enabled Amount of time required after triggering an action until the next trigger can occur Amount of time required for the mouse cursor to be pushed against the edge of the screen before the action is triggered Application Launcher Change desktop when the mouse cursor is pushed against the edge of the screen EMAIL OF TRANSLATORSYour emails Lock Screen Maximize windows by dragging them to the top edge of the screen NAME OF TRANSLATORSYour names No Action Only When Moving Windows Open krunnerRun Command Other Settings Quarter tiling triggered in the outer Show Desktop Switch desktop on edgeDisabled Tile windows by dragging them to the left or right edges of the screen Toggle alternative window switching Toggle window switching Trigger an action by pushing the mouse cursor against the corresponding screen edge or corner Trigger an action by swiping from the screen edge towards the center of the screen Window Management of the screen Project-Id-Version: kcmkwinscreenedges
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-30 03:13+0100
PO-Revision-Date: 2018-01-03 20:26+0100
Last-Translator: Burkhard Lück <lueck@hube-lueck.de>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
  ms  % %1 - Alle Arbeitsflächen %1 - Würfel %1 - Aktuelle Anwendung %1 - Aktuelle Arbeitsfläche %1 - Zylinder %1 - Kugel &Reaktivierungsverzögerung: &Rand für Arbeitsflächenwechsel: Aktivierungs&verzögerung: Aktive Bildschirmränder Aktivitätenverwaltung Immer aktiviert Erforderliches Zeitfenster nach einer Aktion, bevor die nächste ausgelöst werden kann. Benötigte Zeit zum Anstoßen an einen Arbeitsflächenrand, bevor die Aktion ausgelöst wird. Anwendungsstarter Arbeitsfläche wechseln, wenn die Maus an einen Bildschirmrand anstößt lueck@hube-lueck.de Bildschirm sperren Fenster durch Ziehen zum oberen Bildschirmrand maximieren Burkhard Lück Keine Aktion Nur beim Verschieben von Fenstern aktiviert Befehl ausführen Andere Einstellungen Kachelung der Fenster auf 1/4 der Bildschirmgröße ausgelöst in den äußeren Arbeitsfläche anzeigen Deaktiviert Fenster durch Ziehen zum rechten oder linken Bildschirmrand anordnen Alternativen Fensterwechsel umschalten Fensterwechsel umschalten Um eine Aktion auszulösen, stoßen Sie mit dem Mauszeiger an den zugehörigenBildschirmrand oder die Bildschirmecke Um eine Aktion auszulösen, wischen Sie vom Bildschirmrand in die Mitte des Bildschirms. Fensterverwaltung der Höhe des Bildschirms 