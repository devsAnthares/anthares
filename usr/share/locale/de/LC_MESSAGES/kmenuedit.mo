��    >        S   �      H  	   I  	   S     ]     e     s     y          �     �     �     �     �     �     �  
        "  ?   .     n  >   w  	   �     �     �  S   �      A     b  �  z     	     
	     	  
   '	     2	     B	     R	  
   _	     j	  A   y	     �	     �	  
   �	     �	     �	     
     #
     ,
     ;
     G
     X
     h
     |
     �
     �
     �
     �
     �
     �
               (  T   ;     �  S   �  �  �     �     �  
   �     �     �     �     �     �  '   �  
   &  !   1     S  $   n     �     �     �  @   �  	     G        c     l     �  q   �          $  �  B  	   	          ,     @     S     d     u     �     �  K   �     �            
   &     1     >     Q     _     r     �     �     �     �  '   �     �          ,     J  &   _     �     �     �  x   �     I  X   W            3          1   6       2   4      !   *         #   7                    (   9          &           :   5          +                    "                        '           -          >   =   /   	      )                ;   
      .      $   %       <   ,                    8      0            [Hidden] &Comment: &Delete &Description: &Edit &File &Name: &New Submenu... &Run as a different user &Sort &Sort all by Description &Sort all by Name &Sort selection by Description &Sort selection by Name &Username: &Work path: (C) 2000-2003, Waldo Bastian, Raffaele Sandrini, Matthias Elter Advanced All submenus of '%1' will be removed. Do you want to continue? Co&mmand: Could not write to %1 Current shortcut &key: Do you want to restore the system menu? Warning: This will remove all custom menus. EMAIL OF TRANSLATORSYour emails Enable &launch feedback Following the command, you can have several place holders which will be replaced with the actual values when the actual program is run:
%f - a single file name
%F - a list of files; use for applications that can open several local files at once
%u - a single URL
%U - a list of URLs
%d - the folder of the file to open
%D - a list of folders
%i - the icon
%m - the mini-icon
%c - the caption General General options Hidden entry Item name: KDE Menu Editor KDE menu editor Main Toolbar Maintainer Matthias Elter Menu changes could not be saved because of the following problem: Menu entry to pre-select Montel Laurent Move &Down Move &Up NAME OF TRANSLATORSYour names New &Item... New Item New S&eparator New Submenu Only show in KDE Original Author Previous Maintainer Raffaele Sandrini Restore to System Menu Run in term&inal Save Menu Changes? Show hidden entries Spell Checking Spell checking Options Sub menu to pre-select Submenu name: Terminal &options: Unable to contact khotkeys. Your changes are saved, but they could not be activated. Waldo Bastian You have made changes to the menu.
Do you want to save the changes or discard them? Project-Id-Version: kmenuedit
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2013-09-10 08:57+0200
Last-Translator: Burkhard Lück <lueck@hube-lueck.de>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
  [Ausgeblendet] &Kommentar: &Entfernen &Beschreibung: &Bearbeiten &Datei &Name: &Neues Untermenü ... Mit &anderer Benutzerkennung ausführen &Sortieren &Alle nach Beschreibung sortieren &Alle nach Namen sortieren Auswahl nach &Beschreibung sortieren Auswahl nach Namen &sortieren Be&nutzername: &Arbeitsordner: © 2000–2003, Waldo Bastian, Raffaele Sandrini, Matthias Elter Erweitert Alle Untermenüs von „%1“ werden entfernt. Möchten Sie fortfahren? B&efehl: Schreiben nicht möglich in: %1 Aktuelles T&astenkürzel: Möchten Sie das Systemmenü wiederherstellen? Warnung: Alle eigenen Änderungen am Menü gehen dadurch verloren. thd@kde.org Start&rückmeldung aktivieren Dem Befehl können diverse Platzhalter folgen, die durch folgende Werte ersetzt werden, sobald das Programm tatsächlich läuft:
%f – einzelner Dateiname
%F – Liste von Dateinamen (für Programme, die mehrere Dateien öffnen können)
%u – einzelne Adresse (URL)
%U – Liste von Adressen (URLs)
%d – Ordner der Datei, die geöffnet werden soll
%D – Liste von Ordnern
%i – das Symbol
%m – die Mini-Ausgabe des Symbols
%c – die Überschrift Allgemein Allgemeine Einstellungen Versteckter Eintrag Name des Elements: KDE-Menü-Editor KDE-Menü-Editor Haupt-Werkzeugleiste Betreuer Matthias Elter Menü-Änderungen lassen sich wegen des folgenden Problems nicht speichern: Menüeintrag für Vorauswahl Montel Laurent Nach &unten Nach &oben Thomas Diehl Neues &Element ... Neues Element Neue &Unterteilung Neues Untermenü Nur in KDE anzeigen Ursprünglicher Autor Früherer Betreuer Raffaele Sandrini Systemvoreinstellungen wiederherstellen In &Terminal starten Menü-Änderungen speichern? Versteckte Einträge anzeigen Rechtschreibprüfung Einstellungen zur Rechtschreibprüfung Untermenü für Vorauswahl Name des Untermenüs: Terminal-&Einstellungen: Es ist keine Verbindung mit KHotKeys möglich. Ihre Änderungen wurden gespeichert, können aber nicht aktiviert werden. Waldo Bastian Sie haben Änderungen am Menü vorgenommen.
Möchten Sie diese speichern oder verwerfen? 