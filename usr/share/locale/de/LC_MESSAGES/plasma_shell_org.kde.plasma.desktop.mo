��    F      L  a   |         
     
     
        "     1     5     I     X     ^  	   m     w  	        �     �     �  
   �     �     �  3   �  	   �     �               $     9     @     G     V     f     m  
        �  2   �  ?   �                    )     2     ?     N     S     a     r     �     �     �     �     �     �  	   �     �     �     �     �  b   �  �   [	     �	     �	  	   
     
     (
  
   8
  	   C
     M
     ]
     e
     k
     }
  �  �
     +     8     K     e     �     �     �     �     �     �     �     �     �       	     
     	   &  
   0     ;     =     I     _     x     }     �     �     �     �     �     �     �     �  K   �  Y   I     �     �     �     �     �     �     �     �          +     9     <     [     t     {     �  
   �     �     �     �     �  l   �  �   Y       "   	     ,     ;     W     j     w     �     �     �  .   �  )   �     +       E   C   4       &       D           
   <      *   9           !   F      /   #                A       @   =                     $   ;                  B             :       '           0         1   2                                -   (       6   ?         >              "   .   %   ,              )       8       3   5   	                  7    Activities Add Action Add Spacer Add Widgets... Alt Alternative Widgets Always Visible Apply Apply Settings Apply now Author: Auto Hide Back-Button Bottom Cancel Categories Center Close Concatenation sign for shortcuts, e.g. Ctrl+Shift+ Configure Configure activity Create activity... Ctrl Currently being used Delete Email: Forward-Button Get new widgets Height Horizontal-Scroll Input Here Keyboard shortcuts Layout cannot be changed whilst widgets are locked Layout changes must be applied before other changes can be made Layout: Left Left-Button License: Lock Widgets Maximize Panel Meta Middle-Button More Settings... Mouse Actions OK Panel Alignment Remove Panel Right Right-Button Screen Edge Search... Shift Stop activity Stopped activities: Switch The settings of the current module have changed. Do you want to apply the changes or discard them? This shortcut will activate the applet: it will give the keyboard focus to it, and if the applet has a popup (such as the start menu), the popup will be open. Top Undo uninstall Uninstall Uninstall widget Vertical-Scroll Visibility Wallpaper Wallpaper Type: Widgets Width Windows Can Cover Windows Go Below Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-09 03:10+0200
PO-Revision-Date: 2017-11-26 14:10+0100
Last-Translator: Burkhard Lück <lueck@hube-lueck.de>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Aktivitäten Aktion hinzufügen Abstandhalter hinzufügen Miniprogramme hinzufügen ... Alt Alternative Miniprogramme Immer sichtbar Anwenden Einstellungen anwenden Jetzt anwenden Autor: Automatisch ausblenden Zurück-Knopf Unten Abbrechen Kategorien Zentriert Schließen + Einrichtung Aktivität einrichten Aktivität erstellen ... Strg Zurzeit verwendet Löschen E-Mail: Vorwärts-Knopf Neue Miniprogramme holen Höhe Horizontales Mausrad Eingabe hier Kurzbefehle Das Layout kann nicht geändert werden, solange Miniprogramme gesperrt sind Layout-Änderungen müssen angewendet werden, bevor eine weitere Bearbeitung möglich ist Layout: Links Linke Maustaste Lizenz: Miniprogramme sperren Kontrollleiste maximieren Meta Mittlere Maustaste Weitere Einstellungen ... Maus-Aktionen OK Ausrichtung der Kontrollleiste Kontrollleiste entfernen Rechts Rechte Maustaste Bildschirmkante Suchen ... Umschalttaste Aktivität anhalten Angehaltene Aktivitäten: Wechseln Die Einstellungen im aktuellen Modul wurden geändert. Möchten Sie die Änderungen anwenden oder verwerfen? Dieser Kurzbefehl aktiviert das Miniprogramm: Es erhält den Fokus und wenn das Miniprogramm ein Kontextsymbol wie zum Beispiel ein Startmenü hat, wird dieses geöffnet. Oben Deinstallation rückgängig machen Deinstallieren Miniprogramm deinstallieren Vertikales Mausrad Sichtbarkeit Hintergrundbild Hintergrundbild-Typ: Miniprogramme Breite Fenster können die Kontrollleiste überdecken Fenster rutschen unter die Kontrollleiste 