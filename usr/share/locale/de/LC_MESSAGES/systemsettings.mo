��    6      �  I   |      �  A   �     �     �  /     8   A     z     �     �     �     �     �     �     �     �  $   �  	        #  !   9  3   [  	   �     �      �  $   �     �  *   �     "  	   '  5   1     g     �  
   �     �     �     �     �  5   �  3   (  6   \     �  ,   �  /   �  	   �     	     	     %	     2	  O   B	  Z   �	  b   �	  	   P
  
   Z
  P   e
     �
  �  �
  B   q     �     �  7   �  &        @     I     `     y     �     �     �     �     �  "   �  
   �       )     G   B  
   �     �     �  "   �     �  	   �     �     �  .     %   4     Z     i     r          �     �  X   �  X     O   k     �  <   �       
             9     F     [  b   o  t   �  l   G     �     �  V   �     &     %                    .   ,   !   &       $   6                                        '       /       2         	   *   0                    )      "   1                 +      (   -                   3           5          #              
         4           %1 is an external application and has been automatically launched (c) 2009, Ben Cooksley (c) 2017, Marco Martin <i>Contains 1 item</i> <i>Contains %1 items</i> A search yelded no resultsNo items matching your search About %1 About Active Module About Active View About System Settings All Settings Apply Settings Author Back Ben Cooksley Central configuration center by KDE. Configure Configure your system Contains 1 item Contains %1 items Determines whether detailed tooltips should be used Developer Dialog EMAIL OF TRANSLATORSYour emails Expand the first level automatically Frequently used: General config for System SettingsGeneral Help Icon View Internal module representation, internal module model Internal name for the view used Keyboard Shortcut: %1 Maintainer Marco Martin Mathias Soeken NAME OF TRANSLATORSYour names No views found Provides a categorized icons view of control modules. Provides a categorized sidebar for control modules. Provides a classic tree-based view of control modules. Relaunch %1 Reset all current changes to previous values Search through a list of control modulesSearch Search... Show detailed tooltips Sidebar Sidebar View System Settings System Settings was unable to find any views, and hence has nothing to display. System Settings was unable to find any views, and hence nothing is available to configure. The settings of the current module have changed.
Do you want to apply the changes or discard them? Tree View View Style Welcome to "System Settings", a central place to configure your computer system. Will Stephenson Project-Id-Version: systemsettings
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:08+0100
PO-Revision-Date: 2017-12-06 15:33+0100
Last-Translator: Burkhard Lück <lueck@hube-lueck.de>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 %1 ist eine externe Anwendung und ist automatisch gestartet worden © 2009, Ben Cooksley (c) 2017, Marco Martin <i>Enthält ein Element</i> <i>Enthält %1 Elemente</i> Kein passendes Element für Ihre Suche Über %1 Über das aktive Modul Über die aktive Ansicht Über Systemeinstellungen Alle Einstellungen Einstellungen anwenden Autor Zurück Ben Cooksley Zentrales Kontrollzentrum von KDE. Einrichten System einrichten Enthält ein Element Enthält %1 Elemente Ist diese Einstellung aktiviert, werden detaillierte Kurzinfos gegeben. Entwickler Dialog tr@erdfunkstelle.de Erste Ebene automatisch ausklappen Häufig Verwendete: Allgemein Hilfe Symbolansicht Interne Moduldarstellung, internes Modulmodell Interner Name der verwendeten Ansicht Kurzbefehl: %1 Betreuer Marco Martin Mathias Soeken Thomas Reitelbach Keine Ansichten gefunden Stellt eine Symbolansicht der Kontrollmodule in verschiedenen Kategorien zur Verfügung. Stellt eine Seitenleiste der Kontrollmodule mit verschiedenen Kategorien zur Verfügung. Stellt eine klassische Baum-basierte Ansicht der Kontrollmodule zur Verfügung. %1 neu starten Alle aktuellen Änderungen auf vorherige Werte zurücksetzen Suchen Suchen ... Detaillierte Kurzinfos anzeigen Seitenleiste Seitenleistenansicht Systemeinstellungen Das Programm Systemeinstellungen kann keine Ansichten finden, sodass nichts angezeigt werden kann. Das Programm Systemeinstellungen kann keine Ansichten finden, sodass keine Einstellungen vorgenommen werden können. Die Einstellungen im aktuellen Modul wurden geändert.
Möchten Sie die Änderungen anwenden oder verwerfen? Baumansicht Ansichtsdesign Willkommen in den Systemeinstellungen, dem zentralen Ort zum Einrichten Ihres Systems. Will Stephenson 