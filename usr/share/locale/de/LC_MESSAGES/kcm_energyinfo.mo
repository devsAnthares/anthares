��    7      �  I   �      �     �     �     �     �     �     �               $     -     5     H     T      `     �     �     �     �     �     �     �     �                     )     7  	   C  	   M     W     d     j     �     �     �     �     �     �     �     �     �     �  @   �     7     @     \     c     j     r     �     �     �  4   �     �  �  �     �	     �	     �	     �	     �	  
   �	     �	  
   �	     �	     �	     �	     
     
     
     1
     9
  &   J
     q
     z
     �
     �
     �
     �
     �
     �
     �
               7  
   F     Q     X     g     l          �     �     �     �     �     �  
   �  G   �              	   A     K     M     V     v     x     {  	   ~     �     &   0      7                  #                
          	   '   (   +                       6              3      1                   ,   2                  "       *          $      4   )               %      5                     !          /   .   -       % %1 is value, %2 is unit%1 %2 %1: Application Energy Consumption Battery Capacity Charge Percentage Charge state Charging Current Degree Celsius°C Details: %1 Discharging EMAIL OF TRANSLATORSYour emails Energy Energy Consumption Energy Consumption Statistics Environment Full design Fully charged Has power supply Kai Uwe Broulik Last 12 hours Last 2 hours Last 24 hours Last 48 hours Last 7 days Last full Last hour Manufacturer Model NAME OF TRANSLATORSYour names No Not charging PID: %1 Path: %1 Rechargeable Refresh Serial Number Shorthand for WattsW System Temperature This type of history is currently not available for this device. Timespan Timespan of data to display Vendor VoltV Voltage Wakeups per second: %1 (%2%) WattW Watt-hoursWh Yes current power draw from the battery in WConsumption literal percent sign% Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2016-06-27 21:50+0100
Last-Translator: Burkhard Lück <lueck@hube-lueck.de>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 % %1 %2 %1: Energieverbrauch von Programmen Akku Kapazität Ladung in Prozent Ladestatus Wird geladen Aktuell °C Details: %1 Wird entladen lueck@hube-lueck.de Energie Energieverbrauch Statistiken über den Energieverbrauch Umgebung „Voll“-Design Vollständig geladen Hat Stromversorgung Kai Uwe Broulik Letzte 12 Stunden Letzte 2 Stunden Letzte 24 Stunden Letzte 48 Stunden Letzte 7 Tage Zuletzt vollständig geladen Letzten Stunde Hersteller Modell Burkhard Lück Nein Wird nicht geladen PID: %1 Pfad: %1 Wiederaufladbar Aktualisieren Seriennummer W System Temperatur Diese Art des Verlaufs ist zurzeit für dieses Gerät nicht verfügbar. Zeitraum Zeitraum der anzuzeigenden Daten Lieferant V Spannung Weckrufe pro Sekunde: %1 (%2 %) W Wh Ja Verbrauch  % 