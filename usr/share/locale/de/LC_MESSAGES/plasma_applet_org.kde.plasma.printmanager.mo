��          �      ,      �     �     �     �     �     �     �     �     �  .   �     .     L     \     m     �     �  H   �  �  �     �     �     �     �     �  	   �     �       7   '  '   _     �     �  !   �     �     �  n           	                                                                  
    &Configure Printers... Active jobs only All jobs Completed jobs only Configure printer General No active jobs No jobs No printers have been configured or discovered One active job %1 active jobs One job %1 jobs Open print queue Print queue is empty Printers Search for a printer... There is one print job in the queue There are %1 print jobs in the queue Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2015-02-21 09:37+0000
PO-Revision-Date: 2015-03-09 20:28+0100
Last-Translator: Burkhard Lück <lueck@hube-lueck.de>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 &Drucker einrichten ... Nur aktive Aufträge Alle Aufträge Nur abgeschlossene Aufträge Drucker einrichten Allgemein Keine aktiven Aufträge Keine Aufträge Es sind keine Drucker eingerichtet bzw. gefunden worden Ein aktiver Auftrag %1 aktive Aufträge Ein Auftrag %1 Aufträge Druckerwarteschlange öffnen Die Druckerwarteschlange ist leer Drucker Suche nach einem Drucker ... Es befindet sich ein Druckauftrag in der Warteschlange Es befinden sich %1 Druckaufträge in der Warteschlange 