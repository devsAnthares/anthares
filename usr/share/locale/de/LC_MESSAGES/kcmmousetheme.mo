��          �   %   �      @     A  �   `  m   �  �   P  )   �  `        o     |     �     �     �      �     �     �  '        ,     >     ]     b     s  ?   �  Y   �  +     H   F  �  �     .  �   M  �   �     �	     �	  d   �	     !
     .
     M
     Z
  <   g
     �
     �
     �
  0   �
               )     .     E  Q   V  v   �  5     S   U                                                 	                                                               
              (c) 2003-2007 Fredrik Höglund <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) @item:inlistbox sizeResolution dependent A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Cursor Theme Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Fredrik Höglund Get new Theme Get new color schemes from the Internet Install from File NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. You have to restart the Plasma session for these changes to take effect. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-07-12 20:02+0100
Last-Translator: Frederik Schwarzer <schwarzer@kde.org>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 (c) 2003-2007 Fredrik Höglund <qt>Sind Sie sicher, dass Sie das Zeigerdesign <i>%1</i> entfernen möchten?<br />Dadurch werden alle Dateien gelöscht, die von dem Design installiert wurden.</qt> <qt>Das derzeit verwendete Zeigerdesign kann nicht entfernt werden.<br />Wechseln Sie zuerst das aktuelle Zeigerdesign und versuchen es dann erneut.</qt> (Verfügbare Größen: %1) Auflösungsabhängig Ein Design namens %1 existiert bereits im Ordner für Symboldesigns. Möchten Sie es überschreiben? Bestätigung Zeiger-Einstellungen geändert Zeigerdesign Beschreibung Geben Sie eine Design-Adresse ein oder ziehen Sie sie herein thd@kde.org Fredrik Höglund Neues Design holen Neue Farbschemata aus dem Internet herunterladen Aus Datei installieren Thomas Diehl Name Design überschreiben? Design entfernen Bei der Datei %1 scheint es sich um kein Designarchiv für Mauszeiger zu handeln. Herunterladen der Archivdatei für Zeigerdesign nicht möglich. Überprüfen Sie bitte, ob die Adresse %1 korrekt ist. Das Archiv für das Zeigerdesign %1 nicht auffindbar. Die Plasma-Sitzung muss neu gestartet werden, damit die Änderungen wirksam werden. 