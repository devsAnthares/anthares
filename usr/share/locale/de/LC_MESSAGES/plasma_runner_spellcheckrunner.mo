��          t      �                 '     6     R     Z     w  Q   �     �      �       �       �     �  (   �     "  )   *  &   T     {     �     �     �     
      	                                                 &Require trigger word &Trigger word: Checks the spelling of :q:. Correct Could not find a dictionary. Spell Check Settings Spelling checking runner syntax, first word is trigger word, e.g.  "spell".%1:q: Suggested words: %1 separator for a list of words,  spell Project-Id-Version: plasma_runner_spellcheckrunner
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2016-04-10 19:55+0100
Last-Translator: Frederik Schwarzer <schwarzer@kde.org>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 Schlagwo&rt benötigt &Schlagwort: Überprüft die Rechtschreibung von :q:. Richtig Es kann kein Wörterbuch gefunden werden. Einstellungen zur Rechtschreibprüfung %1:q: Vorgeschlagene Wörter: %1 ,  spell 