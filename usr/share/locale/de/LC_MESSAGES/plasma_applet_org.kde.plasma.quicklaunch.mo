��          �      <      �     �  <   �  
   �     	          &     3     ?  
   G     R     c     q     }     �     �  
   �     �  �  �     c  M   {     �  	   �     �     �       	   %     /     B     [     s     �     �     �     �     �                                                  
   	                                                Add Launcher... Add launchers by Drag and Drop or by using the context menu. Appearance Arrangement Edit Launcher... Enable popup Enter title General Hide icons Maximum columns: Maximum rows: Quicklaunch Remove Launcher Show hidden icons Show launcher names Show title Title Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-02-26 04:09+0100
PO-Revision-Date: 2016-03-12 15:12+0100
Last-Translator: Frederik Schwarzer <schwarzer@kde.org>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Starter hinzufügen ... Starter durch Ziehen und Ablegen oder Verwenden des Kontextmenüs hinzufügen Erscheinungsbild Anordnung Starter bearbeiten ... Aufklappfenster aktivieren Titel eingeben Allgemein Symbole ausblenden Maximale Anzahl Spalten: Maximale Anzahl Zeilen: Schnellstarter Starter entfernen Ausgeblendete Symbole anzeigen Starternamen anzeigen Titel anzeigen Titel 