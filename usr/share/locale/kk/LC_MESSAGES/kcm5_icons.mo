��    :      �  O   �      �     �                    ,     3  �  N  �   )  -   �  )   �  -   	  +   ;	  r   g	  	   �	  	   �	     �	     
     
     
  
   $
     /
     ;
     C
     K
      b
     �
     �
     �
      �
     �
     �
  r   �
  5   ]     �     �     �  	   �     �     �     �  (   �                9     S     n     t  +   �  3   �     �     �     �     �  S     )   _     �  �   �  �  s          (     9     Q     h     t  m  �  �   �  8   �               6  �   C     �  !   �     �  
        !     7     D     U  
   l     w  U   �     �  +   �  
         +  ;   8     t  G   �  �   �  W   �          %  
   C     N     U  
   b     m  C   �      �  C   �  ?   0  I   p     �  '   �  T   �  J   E     �     �     �     �  �   �  V   v  )   �  j  �               2                '   (          1      !            8                                       .               5   	   6   $      )       7   4          *                 0   
             :         ,                    +      %      "   -   /   #       9           &      3    &Amount: &Effect: &Second color: &Semi-transparent &Theme (c) 2000-2003 Geert Jansen <h1>Icons</h1>This module allows you to choose the icons for your desktop.<p>To choose an icon theme, click on its name and apply your choice by pressing the "Apply" button below. If you do not want to apply your choice you can press the "Reset" button to discard your changes.</p><p>By pressing the "Install Theme File..." button you can install your new icon theme by writing its location in the box or browsing to the location. Press the "OK" button to finish the installation.</p><p>The "Remove Theme" button will only be activated if you select a theme that you installed using this module. You are not able to remove globally installed themes here.</p><p>You can also specify effects that should be applied to the icons.</p> <qt>Are you sure you want to remove the <strong>%1</strong> icon theme?<br /><br />This will delete the files installed by this theme.</qt> <qt>Installing <strong>%1</strong> theme</qt> @label The icon rendered as activeActive @label The icon rendered as disabledDisabled @label The icon rendered by defaultDefault A problem occurred during the installation process; however, most of the themes in the archive have been installed Ad&vanced All Icons Antonio Larrosa Jimenez Co&lor: Colorize Confirmation Desaturate Description Desktop Dialogs Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Effect Parameters Gamma Geert Jansen Get new themes from the Internet Icons Icons Control Panel Module If you already have a theme archive locally, this button will unpack it and make it available for KDE applications Install a theme archive file you already have locally Main Toolbar NAME OF TRANSLATORSYour names Name No Effect Panel Preview Remove Theme Remove the selected theme from your disk Set Effect... Setup Active Icon Effect Setup Default Icon Effect Setup Disabled Icon Effect Size: Small Icons The file is not a valid icon theme archive. This will remove the selected theme from your disk. To Gray To Monochrome Toolbar Torsten Rahn Unable to download the icon theme archive;
please check that address %1 is correct. Unable to find the icon theme archive %1. Use of Icon You need to be connected to the Internet to use this action. A dialog will display a list of themes from the http://www.kde.org website. Clicking the Install button associated with a theme will install this theme locally. Project-Id-Version: kcmicons
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-08 06:27+0100
PO-Revision-Date: 2013-05-30 04:15+0600
Last-Translator: Sairan Kikkarin <sairan@computer.org>
Language-Team: Kazakh <kde-i18n-doc@kde.org>
Language: kk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=1; plural=0;




 Ш&амасы: &Эффекті: &Екінші түсі: &Шала мөлдір &Нақыш (c) 2000-2003 Geert Jansen <h1>Таңбашалар</h1> Бұл модуль үстеліңізге таңбашаларды таңдауға мүмкіндік береді.<p> Таңбашалардың нақышын таңдап "Іске асыру" деген төмендегі батырмасын басыңыз. Таңдаудан айнысаңыз "Ысырып тастау" дегенді басыңыз.</p><p>"Нақыш файлын орнату..." дегенді басып, шыққан жолға нақыш файлының адресін жазып не шолып таңдап келтіре аласыз. Орнатуды аяқтау үшін "ОК" батырмасын басыңыз.</p><p> "Нақышты өшіру" деген батырма, осы модулімен орнатылған нақышының біреуін таңдағанда рұқсат етіледі. Бұнымен жалпы жүйелік деңгейде орнатылған нақышты өшіре алмайсыз.</p> <p>Сонымен қатар, таңбашалды бірер эффекттермен безендіруге болады.</p> <qt><strong>%1</strong> деген таңбашалар нақышын өшіргіңіз келгені рас па?<br /><br />Осы нақышымен орнатылған файлдар өшіріледі.</qt> <qt><strong>%1</strong> нақышын орнату</qt> Белсенді Бұғатталған Әдетті Орнату кезде қате пайда болды, бірақ архивтегі нақыштардың көбі орнатылды Қосым&ша Барлық таңбашалар Antonio Larrosa Jimenez Тү&сі: Түрлі түсті Құптау Қанықсыз Сипаттамасы Үстел Диалогтар Нақыштың URL-адресін жазып не сүйреп келтіріңіз sairan@computer.org Эффекттің параметрлері Гамма Geert Jansen Жаңа нақыштарды Интернеттен алу Таңбашалар Таңбашаларды басқару панелінің модулі Егер нақыш архиві компьютеріңізде бар болса, осы батырманы басып оны архивтен шығарып KDE-де қолдануға дайын қыла аласыз Компьютеріңізде бар нақыш архиві файлын орнату Негізгі құралдар Сайран Киккарин Атауы Жоқ Панель Қарау Нақышты өшіру Дискіңізден таңдаған нақышты кетіру Әффектті орнату... Белсенді таңбашалар эффектін орнату Әдетті таңбашалар эффектін орнату Бұғатталған таңбашалар эффектін орнату Өлшемі: Кішкентай таңбашалар Бұл жарамды таңбашалар нақышының архиві емес. Бұл дискіңізден таңдаған нақышты кетіру Сұр түсті Қара мен ақ Құралдар панелі Torsten Rahn Таңбашалар нақышының архиві жүктелмеді.
%1 деген адресі дұрыс екенін тексеріңіз. %1 деген таңбашалар нақышының архиві табылмады. Таңбашаның пайдалануы Бұл амалды жасау үшін Интернетке қосулы болу керек. Нақыштар тізімі http://www.kde.org вебсайтынан алынып ұсынылады. Нақышқа сәйкес "Орнату" батырмасын түртіп керек нақышты компьютеріңізге түсіріп аласыз. 