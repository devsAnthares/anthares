��          �      \      �     �  �   �  m   r  `   �     A     N     f     s           �     �  '   �     �               %  ?   2  Y   r  +   �  �  �     �  �   �  �   �  �        �  4   �     	     .	  i   E	      �	     �	  D   �	  <   &
  
   c
  #   n
     �
  Q   �
  �      C   �                                                             	                               
       (c) 2003-2007 Fredrik Höglund <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Cursor Theme Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Fredrik Höglund Get new color schemes from the Internet NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. Project-Id-Version: kcminput
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2011-12-09 02:19+0600
Last-Translator: Sairan Kikkarin <sairan@computer.org>
Language-Team: Kazakh <kde-i18n-doc@kde.org>
Language: kk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.1
Plural-Forms: nplurals=1; plural=0;


 (c) 2003-2007 Fredrik Höglund <qt>Шынында <i>%1</i> меңзер нақышын өшіргіңіз келеді ме?<br />Бұл нақышымен орналылған барлық файлдар өшіріледі.</qt> <qt>Қолданыстағы нақышты кетіруге болмайды.<br />Алдымен басқа нақышқа ауысыңыз.</qt> %1 деген нақыш таңбаша нақыштар каталогында бар ғой. Бұны шынында жаңа нақышымен ауыстырғыңыз келе ме? Құптау Меңзер параметрлері өзгерді Меңзер нақышы Сипаттамасы Нақыштың URL сілтемесін келтіріңіз немесе сүйреп әкеліңіз ak78@sci.kz, sairan@computer.org Fredrik Höglund Жаңа түстер сұлбасын Интернеттен алу Ақсауле Мамаева, Сайран Киккарин Атауы Нақышты қайта жазу? Нақышты кетіру %1 файлы меңзер нақышының архиві емес сияқты. Меңзер нақышының архиві жүктелмеді; %1 адресі дұрыс екенін тексеріңіз. %1 меңзер нақышының архиві табылмады. 