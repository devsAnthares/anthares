��    B      ,  Y   <      �  i   �  m        y  b   �     �     	  6        O  7   _     �     �  	   �     �  (   �  )   �  )   (     R     o  Y   u     �     �  �   �      y	  .   �	     �	  
   �	     �	     �	     
     
     !
     5
     B
     J
     c
     r
     w
     �
     �
     �
     �
     �
  	   �
  
   �
     �
     �
  	     
     
   *     5     B  	   `     j     o  
   �  �   �     (  8   8  �   q     �  8   	  ,   B  ,   o  %   �     �  �  �  |   �  �     5   �  �   �  (   �     �  S   �       O   7     �     �     �     �  L   �  H   $  H   m  4   �  '   �       +   �     �  �   �     �  R   �             -   5     c  +   |     �     �     �     �  2         3     B     K      i     �  !   �     �     �     �            2   ,     _     u     �     �  =   �       
     0        N    k     r  h   �  �   �     �  P   �  P   %  P   v  ,   �  #   �     $   !   %          ;   ,          B                 8   #                 7   9       +              )   ?   -   <         6                                    *   2       >                1       @       .   5   3   :   A         &      /                4              0      "             =   (   '         	   
           @info User changed Phonon backendTo apply the backend change you will have to log out and back in again. A list of Phonon Backends found on your system.  The order here determines the order Phonon will use them in. Apply Device List To... Apply the currently shown device preference list to the following other audio playback categories: Audio Hardware Setup Audio Playback Audio Playback Device Preference for the '%1' Category Audio Recording Audio Recording Device Preference for the '%1' Category Backend Colin Guthrie Connector Copyright 2006 Matthias Kretz Default Audio Playback Device Preference Default Audio Recording Device Preference Default Video Recording Device Preference Default/Unspecified Category Defer Defines the default ordering of devices which can be overridden by individual categories. Device Configuration Device Preference Devices found on your system, suitable for the selected category.  Choose the device that you wish to be used by the applications. EMAIL OF TRANSLATORSYour emails Failed to set the selected audio output device Front Center Front Left Front Left of Center Front Right Front Right of Center Hardware Independent Devices Input Levels Invalid KDE Audio Hardware Setup Matthias Kretz Mono NAME OF TRANSLATORSYour names Phonon Configuration Module Playback (%1) Prefer Profile Rear Center Rear Left Rear Right Recording (%1) Show advanced devices Side Left Side Right Sound Card Sound Device Speaker Placement and Testing Subwoofer Test Test the selected device Testing %1 The order determines the preference of the devices. If for some reason the first device cannot be used Phonon will try to use the second, and so on. Unknown Channel Use the currently shown device list for more categories. Various categories of media use cases.  For each category, you may choose what device you prefer to be used by the Phonon applications. Video Recording Video Recording Device Preference for the '%1' Category  Your backend may not support audio recording Your backend may not support video recording no preference for the selected device prefer the selected device Project-Id-Version: kcm_phonon
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2012-07-04 04:30+0600
Last-Translator: Sairan Kikkarin <sairan@computer.org>
Language-Team: Kazakh <kde-i18n-doc@kde.org>
Language: kk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=1; plural=0;





 Тетігінің өзгертуін іске асыру үшін жүйеден шығып қайта кіру керек. Жүйеңізде Phonon тетіктер тізімі табылды.  Осында табылған ретімен Phonon оларды пайдаланады. Құрылғылар тізімін қолдану... Көрсетілген құрылғы басымдылықтар тізімі келесі өзге дыбыс ойнатқыш сантатарына қолданылсын: Аудио жабдықты баптау Аудио ойнату '%1' санаттағы әдетті дыбыс ойнатқыш құрылғысы Дыбысты жазу '%1' санаттағы әдетті дыбыс жазғыш құрылғысы Тетігі Colin Guthrie Түйістіргіш Copyright 2006 Matthias Kretz Әдетті дыбыс ойнатқыш құрылғының баптауы Әдетті дыбыс жазғыш құрылғының баптауы Әдетті бейне жазғыш құрылғының баптауы Әдетті/Көрсетілмеген санаты Артықтығын төмендету Дербес санаттарымен ауыстыруға болатын құрылғылардың әдетті кезегі. Құрылғы конфигурациясы Құрылғы баптауы Жүйеңізде табылған құрылғылар таңдаған санатқа жарайды. Қолданбада пайдаланатын құрылғыны таңдаңыз. sairan@computer.org Таңдалған аудио шығыс құрылғысы орнатылмады Қарсы - ортада Сол жақ - қарсы Ортадан сол жақ - қарсыда Оң жақ - қарсы Ортадан оң жақ - қарсыда бдық Жеке құрылғылар Кіріс деңгейлері Жарамсыз KDE аудио жабдықтарын баптау Matthias Kretz Моно Сайран Киккарин Phonon баптау модулі Ойнату (%1) Артықтығын көтеру Профилі Арт жақ - ортада Сол жақ - артта Оң жақ - артта Жазу (%1) Үздік құрылғыларын көрсету Сол жағында Оң жағында Дыбыстық тақша Дыбыс құрылғысы Динамигін орналастыру және сынау Сабвуфер Сынақ Таңдалған құрылғыны сынау %1 дегенді сынау Реті құрылғылардың басымдылықтарын анықтайды. Бір себеппен Phonon бірінші құрылғыны қолдана алмай қалса, екіншісіне көшеді, солай тізім бойынша. Беймәлім арна Назардағы құрылғылар тізімін өзге саннтатарына қолдану. Түрлі медиа қолдану санаттары.  Әрбір санат үшін қай құрылғы Phonon қолданбасы пайдаланатынын таңдауға болады. Бейнені жазу '%1' санаттағы әдетті бейне жазғыш құрылғысы  Тетік бағдарламаңыз дыбыс жазбайтын сияқты Тетік бағдарламаңыз бейне жазбайтын сияқты таңдалғаны - артығы емес таңдалғаны - артығы 