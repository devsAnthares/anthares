��          �      ,      �  &   �  +   �     �  	   �     �     
                 (   .     W     Z  ,   q     �     �     �  �  �  j   :  l   �  
          7   ,     d     x     �     �  [   �       5   #  W   Y  
   �  -   �     �                                                                	   
           Do you want to suspend to RAM (sleep)? Do you want to suspend to disk (hibernate)? General Hibernate Hibernate (suspend to disk) Leave Leave... Lock Lock the screen Logout, turn off or restart the computer No Sleep (suspend to RAM) Start a parallel session as a different user Suspend Switch user Yes Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2012-07-29 10:37+0600
Last-Translator: Sairan Kikkarin
Language-Team: Kazakh <kde-i18n-doc@kde.org>
Language: kk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=1; plural=0;
 Күйін RAM-жадына сақтап қалғу режіміне ауысуды қалайсыз ба? Күйін дискіге сақтап ұйықтау режіміне ауысуды қалайсыз ба? Жалпы Ұйықтау Ұйықтау (күйін дискіге сақтап) Шығып кету Шығып кету... Бұғаттау Экранды бұғаттау Жүйеден шығу, компьютерді сөндіру не жаңадан жегу Жоқ Қалғу (күйін RAM-жадына сақтап) Басқа пайдаланушы ретінде қатар сеансын бастау Қалғу Пайдаланушысын ауыстыру Иә 