��          �      \      �     �     �     �     �              	   <     F     e  &        �     �     �  �   �  �   y  t     7   �  +   �     �  �  �     �     �     �  ,   �  4   �          &     5  6   S  K   �  1   �  9     6   B  �   y  �   D	  �   B
  %      P   F     �                                        
   	                                                         min Act like Always Define a special behavior Don't use special settings EMAIL OF TRANSLATORSYour emails Hibernate NAME OF TRANSLATORSYour names Never shutdown the screen Never suspend or shutdown the computer PC running on AC power PC running on battery power PC running on low battery The Power Management Service appears not to be running.
This can be solved by starting or scheduling it inside "Startup and Shutdown" The activity service is not running.
It is necessary to have the activity manager running to configure activity-specific power management behavior. The activity service is running with bare functionalities.
Names and icons of the activities might not be available. This is meant to be: Act like activity %1Activity "%1" Use separate settings (advanced users only) after Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-09 03:03+0200
PO-Revision-Date: 2011-12-09 03:11+0600
Last-Translator: Sairan Kikkarin <sairan@computer.org>
Language-Team: Kazakh <kde-i18n-doc@kde.org>
Language: kk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.1
Plural-Forms: nplurals=1; plural=0;
  мин. кейін Бұл Әрқашан Әрнаулы тәртібін орнату Арнаулы баптауы қолданбайды sairan@computer.org Ұйықтау Сайран Киккарин Экраны ешқашан сөндірілмесін Компьютер қалғымасын және сөндірілмесін Компьютер токтан істеп тұр Компьютер батареядан істеп тұр Қуаты аз батареядан істеп тұр Қуаттандыруны басқару қызметі жегілмеген сияқты.
Бұл "Бастау мен сөндіру" дегенде жегіп не жоспарлап шешіледі Белсенділік қызметі жегілмеген.
әр белсенділік түріне бөлек құаттандыру тәртібін орнату үшін белсенділік менеджері жегілген болу керек. Белсенділік қызметі шектеулі мүмкіндіктерімен жегілген.
Белсенділіктерінің атау мен таңбашалары көрсетілмеуі мүмкін.  "%1" белсенділігіндей Параметрлері бөлек (тек тәжірибелілер үшін) - 