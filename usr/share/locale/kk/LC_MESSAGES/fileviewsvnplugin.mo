��          �   %   �      0     1  +   G  .   s  6   �  *   �  #     &   (  /   O  2     :   �  K   �  -   9  $   g  '   �     �     �     �     �  #        8     V     j     �  �  �     *  5   9  5   o  D   �  7   �  3   "  1   V  9   �  9   �  H   �  �   E	  5   �	  (   
  &   4
     [
     s
     �
     �
  >   �
  +         ,     D  '   b                                                    	   
                                                                        @action:buttonCommit @info:statusAdded files to SVN repository. @info:statusAdding files to SVN repository... @info:statusAdding of files to SVN repository failed. @info:statusCommit of SVN changes failed. @info:statusCommitted SVN changes. @info:statusCommitting SVN changes... @info:statusRemoved files from SVN repository. @info:statusRemoving files from SVN repository... @info:statusRemoving of files from SVN repository failed. @info:statusSVN status update failed. Disabling Option "Show SVN Updates". @info:statusUpdate of SVN repository failed. @info:statusUpdated SVN repository. @info:statusUpdating SVN repository... @item:inmenuSVN Add @item:inmenuSVN Commit... @item:inmenuSVN Delete @item:inmenuSVN Update @item:inmenuShow Local SVN Changes @item:inmenuShow SVN Updates @labelDescription: @title:windowSVN Commit Show updates Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2011-06-19 05:24+0600
Last-Translator: Sairan Kikkarin <sairan@computer.org>
Language-Team: Kazakh <kde-i18n-doc@kde.org>
Language: kk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.1
Plural-Forms: nplurals=1; plural=0;
 Тапсыру SVN қоймасына файлдар қосылды. SVN қоймасына файлдарды қосу... SVN қоймасына файлдарды қосу жаңылысы. SVN қоймасына тапсыру жаңылысы. Өзгерістер SVN-ге тапсырылды. SVN-ге өзгерістерді жіберу... SVN қоймасынан файлдар өшірілді. SVN қоймасынан файлдарды өшіру... SVN қоймасынан файлдарды өшіру жаңылысы. SVN күй-жайын жаңарту жаңылысы. "SVN жаңартуларын көрсету" параметрі бұғатталады. SVN қоймасын жаңарту жаңылысы. SVN қоймасы жаңартылды. SVN қоймасын жаңарту... SVN қосу амалы SVN тапсыру... SVN өшіру амалы SVN жаңарту амалы Жергілікті SVN өзгерстерін көрсету SVN жаңартуларын көрсету Сипаттамасы: SVN тапсыру амалы Жаңартуларын көрсету 