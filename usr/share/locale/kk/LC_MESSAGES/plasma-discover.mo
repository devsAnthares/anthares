��            )   �      �  5   �  #   �  E   �  E   A  >   �     �     �     �             <        I     Q  *   Z     �  	   �     �     �      �     �  
   �  :   �     3     ;     B     I  	   [     e  
   n  �  y  T     Q   h  W   �  W     P   j     �     �     �  
   	  
   	  g   	     �	     �	  V   �	     �	     �	     
     #
  >   ,
  6   k
     �
  ^   �
       
     
   (  !   3     U     c     u            	                                                      
                                                                       <em>%1 out of %2 people found this review useful</em> <em>Tell us about this review!</em> <em>Useful? <a href='true'><b>Yes</b></a>/<a href='false'>No</a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'><b>No</b></a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'>No</a></em> Aleix Pol Gonzalez Available backends:
 Available modes:
 Back Cancel Directly open the specified application by its package name. Discard Discover Display a list of entries with a category. Install Installed Jonathan Thomas Launch List all the available backends. List all the available modes. Loading... Open with a program that can deal with the given mimetype. Rating: Remove Review Search in '%1'... Search... Summary: Update All Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:02+0100
PO-Revision-Date: 2013-10-03 05:07+0600
Last-Translator: Sairan Kikkarin <sairan@computer.org>
Language-Team: Kazakh <kde-i18n-doc@kde.org>
Language: kk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=1; plural=0;
 <em>%2 адамдан %1 бұл пікірді пайдалы деп тапты</em> <em>Осы пікірге көзқарасыңызды білдіріңіз!</em> <em>Пайдалы ма? <a href='true'><b>Иә</b></a>/<a href='false'>Жоқ</a></em> <em>Пайдалы ма? <a href='true'>Иә</a>/<a href='false'><b>Жоқ</b></a></em> <em>Пайдалы ма? <a href='true'>Иә</a>/<a href='false'>Жоқ</a></em> Aleix Pol Gonzalez Бар режімдері:
 Бар режімдері:
 Артқа Қайту Десте атауы бойынша келтірілген қолданбаны тікелей ашу. Тастау Табу Бір санатына жататын аталым тізімінін қорсету. Орнату Орнатылған Jonathan Thomas Жегу Қол жеткізер орындағыштар тізімі. Қол жеткізер режімдер тізімі. Жүктеу... Көрсеткен MIME түрінмен айналасатын бағдарламен ашу. Ұпайы: Өшіру Пікір '%1' дегенде іздеу... Іздеу... Тұжырымы: Бүкілін жаңарту 