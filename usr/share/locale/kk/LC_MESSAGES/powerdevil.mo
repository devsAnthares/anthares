��    -      �  =   �      �     �  ;   �  9   "  (   \  ;   �  9   �  8   �  $   4     Y     o     �  /   �      �     �  ,   �        N   0  
         �  	   �  �   �     ?     K  e   j     �     �     �  
             '     @     G  &   X  %     f   �  8   	  ;   E	     �	     �	     �	     �	  �   �	  "   j
  C   �
  �  �
     �  6   �  2   �     �  8     4   =  C   r     �  9   �  )   �     )  Z   D  I   �  4   �          -  {   I     �     �     �  �        �     �  �     !   �  %   �  $   �          >  /   ]     �  %   �  7   �  =   �  �   6  �   �  e   E     �  D   �  2   
  <   =  �   z  +   h  x   �                             "         #      
   !   $   	                                   -             %            ,          *          )                                 +   &                '   (                min @action:inmenu Global shortcutDecrease Keyboard Brightness @action:inmenu Global shortcutDecrease Screen Brightness @action:inmenu Global shortcutHibernate @action:inmenu Global shortcutIncrease Keyboard Brightness @action:inmenu Global shortcutIncrease Screen Brightness @action:inmenu Global shortcutToggle Keyboard Backlight @label:slider Brightness levelLevel AC Adapter Plugged In Activity Manager After All pending suspend actions have been canceled. Battery Critical (%1% Remaining) Battery Low (%1% Remaining) Brightness level, label for the sliderLevel Charge Complete Could not connect to battery interface.
Please check your system configuration Do nothing EMAIL OF TRANSLATORSYour emails Hibernate KDE Power Management System could not be initialized. The backend reported the following error: %1
Please check your system configuration Lock screen NAME OF TRANSLATORSYour names No valid Power Management backend plugins are available. A new installation might solve this problem. On Profile Load On Profile Unload Prompt log out dialog Run script Running on AC power Running on Battery Power Script Switch off after The power adaptor has been plugged in. The power adaptor has been unplugged. The profile "%1" has been selected, but it does not exist.
Please check your PowerDevil configuration. This activity's policies prevent screen power management This activity's policies prevent the system from suspending Turn off screen Unsupported suspend method When laptop lid closed When power button pressed Your battery is low. If you need to continue using your computer, either plug in your computer, or shut it down and then change the battery. Your battery is now fully charged. Your battery level is critical, save your work as soon as possible. Project-Id-Version: powerdevil
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-03 03:17+0100
PO-Revision-Date: 2013-06-21 05:51+0600
Last-Translator: Sairan Kikkarin <sairan@computer.org>
Language-Team: Kazakh <kde-i18n-doc@kde.org>
Language: kk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=1; plural=0;














  мин Пернетақта жарықтығын азайту Экранның жарықтығын азайту Ұйықтау Пернетақта жарықтығын үлкейту Экранның жарықтығын үлкейту Перенетақта жарықтық күйін ауыстыру Деңгейі Айнымалы тоқ адаптері қосылған Белсенділік менеджері Мынадан кейін: Барлық күтіп тұрған аялдату әрекеттер доғарылды. Батареясы дағдарысты деңгейде (%1% қалды) Батареясы сарқылды (%1% қалды) Деңгейі Толтыруы бітті Батарея интерфейсі қол жеткізбеді.
Жүйенің баптауларын тексеріңіз Ештеңе істемеу sairan@computer.org Ұйықтау KDE-нің қуаттандыруды басқару жүйесі бастау күйіне келмеді. Тетігі хабарлаған қатесі: %1
Жүйе құрамын түгелдеңіз Экранды бұғаттау Сайран Киккарин Қуаттандыруды басқару тетігінің жарамды плагиндері табылмады. Жаңадан орнатып көріңіз. Профилін жүктенде Профилінен шыққанда Шығу диалогын ұсыну Скриптті орындау Токтан істеп тұр Батареядан қуаттандыруда Скрипт Мынадан кейін өшіру: Қуаттандыру адаптері қосылды. Қуаттандыру адаптері ажыратылды. "%1" деген таңдалды, бірақ ондай профилі жоқ.
PowerDevil баптауларын тексеріңіз. Бұл белсенділік ережелері экранның қуат үнемдеу әрекеттерін болдырмайды Бұл белсенділік ережелері жүйенің аядауын болдырмайды Экранды өшіру Танымайтын аялдау режіміне өту әдісі Ноутбук қақпағы жабылғанда Қуаттандыру батырмасын басқанда Батареяңыз сарқылды. Компьютерді қолдануын жалғастыру үшін оны немесе тоққа қосыңыз немесе шығып, батареяңызды толтырып алыңыз. Батареяңыз әбден толды. Батареяңыз әбден сарқылды, жұмысыңызды мейілінше дереу сақтаңыз. 