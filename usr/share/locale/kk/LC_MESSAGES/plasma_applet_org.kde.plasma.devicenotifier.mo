��          �      ,      �  3   �  $   �  �   �     f  4   r     �  #   �  �   �  �   s  +   2     ^     s     �  �   �  $   *  (   O  �  x  -   0  	   ^     h  %   z  h   �  9   	  G   C  �   �  A  �	  G   �
  ,     A   B  8   �     �  B   �  F                      	                                      
                     1 action for this device %1 actions for this device @info:status Free disk space%1 free Accessing is a less technical word for Mounting; translation should be short and mean 'Currently mounting this device'Accessing... All devices Click to access this device from other applications. Click to eject this disc. Click to safely remove this device. It is currently <b>not safe</b> to remove this device: applications may be accessing it. Click the eject button to safely remove this device. It is currently <b>not safe</b> to remove this device: applications may be accessing other volumes on this device. Click the eject button on these other volumes to safely remove this device. It is currently safe to remove this device. No Devices Available Non-removable devices only Removable devices only Removing is a less technical word for Unmounting; translation shoud be short and mean 'Currently unmounting this device'Removing... This device is currently accessible. This device is not currently accessible. Project-Id-Version: plasma_applet_devicenotifier
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2013-01-02 05:14+0600
Last-Translator: Sairan Kikkarin <sairan@computer.org>
Language-Team: Kazakh <kde-i18n-doc@kde.org>
Language: kk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;


X-Generator: Lokalize 1.2
 осы құрылғының %1 әрекеті %1 бос Қатынау... Барлық құрылғыларын Құрылғысына басқа қолданбалардан қатынау үшін түртіңіз. Дискін алып-шығу үшін түртіңіз. Құрылғысын қауіпсіз алу үшін түртіңіз. Құрылғысын алып тастау қазір <b>қауіпсіз емес</b>: қолданбалар онымен істеп жатқаны мүмкін. Қауіпсіз алу үшін алып-шығару батырмасын басыңыз. Құрылғысын алып тастау қазір <b>қауіпсіз емес</b>: қолданбалар оның басқа томдарымен істеп жатқаны мүмкін. Қауіпсіз алу үшін, сол басқа томдардың алып-шығару батырмасын басыңыз. Құрылғысын алып тастау қазір қауіпсіз. Қосылған құрылғылар жоқ Ауыстырмалы емес құрылғыларын ғана Ауыстырмалы құрылғыларын ғана Алып шығару... Құрылғысына қазір қатынауға болады. Құрылғысына қазір қатынауға болмайды. 