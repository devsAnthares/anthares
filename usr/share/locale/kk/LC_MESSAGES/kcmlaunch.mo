��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  �  K     �  5   �  �    �  �	  �   R  '   �          %  E   >     �  (   �  5   �  A                                             
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2005-11-06 10:36+0600
Last-Translator: Sairan Kikkarin <sairan@computer.org>
Language-Team: Kazakh
Language: kk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.10
Plural-Forms: nplurals=1; plural=0;

  сек &Жегу барысын көрсету уақыты: <H1>Тапсырма панеліндегі хабарландыру</H1>
Қолданбаны жүктеп жегу барысын белгілейтін екінші түрі бар - ол тапсырмалар панелінде бұралып тұрған құмсағат бейнелі
батырманы көрсету. Кейбір қолданбалар бұндай белгіні
бере алмайды. Бұл жағдайда, 'Жегу барысын көрсету уақыты'
деген жүгірткісімен келтірілген уақыт аралықтан кейін осы
батырма ғайып болады. <h1>Күту меңзері</h1>
KDE жүйесінде қолданбаны жегу барысын белгілейтін күту
меңзері бар. Күту меңзерін рұқсат ету үшін осындағы
ашылмалы тізімінен жегу барысының көрнекті белгісінің
бірін таңдап алыңыз. Кейбір қолданбалар бұндай белгіні
бере алмайды. Бұл жағдайда, 'Жегу барысын көрсету уақыты'
деген жүгірткісімен келтірілген уақыт аралықтан кейін меңзер
жыпылықтауын тоқтатады. <h1>Жегу барысы</h1> Мұнда қолданбаны жегу барысының белгілерін баптай аласыз. Жыпылықтайтын меңзер Секірмелі меңзер Күт&у меңзері &Тапсырма панелінде хабарландырылсын Күту меңзері жоқ Пассивті күту меңзері Жег&у барысын көрсету уақыты: Тапсырма панеліндегі &хабарландыру 