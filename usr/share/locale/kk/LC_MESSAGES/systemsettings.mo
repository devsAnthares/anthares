��    *      l  ;   �      �  A   �     �  /        2     ;     O     a     w     �     �  	   �     �  3   �  	   �     �      �  $      *   E     p  	   u  5        �     �  
   �     �          $  5   3  6   i     �  ,   �  /   �     	        O   0  Z   �  b   �  	   >  
   H  P   S     �  �  �  ^   ^
     �
     �
     �
  *     ,   -  0   Z  *   �     �     �     �     �  c   �     a     x     �  T   �  
   �     �  #   
  C   .  A   r  !   �     �     �     �  ,     t   G  _   �  %     �   B  
   �  C   �  !     �   8  �   �  �   r     $     @  �   Z     �            )             &      $   	   %         '                                                !      
       *                     "                                                 (                 #    %1 is an external application and has been automatically launched (c) 2009, Ben Cooksley <i>Contains 1 item</i> <i>Contains %1 items</i> About %1 About Active Module About Active View About System Settings Apply Settings Author Ben Cooksley Configure Configure your system Determines whether detailed tooltips should be used Developer Dialog EMAIL OF TRANSLATORSYour emails Expand the first level automatically General config for System SettingsGeneral Help Icon View Internal module representation, internal module model Internal name for the view used Keyboard Shortcut: %1 Maintainer Mathias Soeken NAME OF TRANSLATORSYour names No views found Provides a categorized icons view of control modules. Provides a classic tree-based view of control modules. Relaunch %1 Reset all current changes to previous values Search through a list of control modulesSearch Show detailed tooltips System Settings System Settings was unable to find any views, and hence has nothing to display. System Settings was unable to find any views, and hence nothing is available to configure. The settings of the current module have changed.
Do you want to apply the changes or discard them? Tree View View Style Welcome to "System Settings", a central place to configure your computer system. Will Stephenson Project-Id-Version: systemsettings
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:08+0100
PO-Revision-Date: 2011-03-04 03:50+0600
Last-Translator: Sairan Kikkarin <sairan@computer.org>
Language-Team: Kazakh <kde-i18n-doc@kde.org>
Language: kk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;


X-Generator: Lokalize 1.0
 %1 деген автоматты түрде жегілген сыртқы бағдарлама (c) 2009, Ben Cooksley <i>%1 нысаны бар</i> %1 туралы Белсенді модуль туралы Белсенді көрініс туралы 'Жүйе параметрлері' туралы Баптауларын іске асыру Авторы Ben Cooksley Баптау Жүйеңізді баптау Егжей-тегжейлі ишараларды көрсету-көрсетпеуін ұйғару Құрастырушы Диалог sairan@computer.org Жоғарғы деңгейін бірден тарқатып көрсетілсін Жалпы Анықтама Таңбашалы көрінісі Модульдің ішкі құрылысы, ішкі үлгісі Қолданыстағы көріністің ішкі атауы Перне тіркесімі: %1 Жетілдіруші Mathias Soeken Сайран Киккарин Көріністер табылған жоқ Баптау модульдерінің санаттарға бөлінген таңбашалар көрінісі. Баптау модульдерінің классикалық бұтақты көрінісі. %1 дегенді қайта жегу Бүкіл енгізілген өзгерістерді ысырып тастап, алдыңғы күйлеріне қайтару Іздеу Егжей-тегжейлі ишаралар көрсетілсін Жүйе параметрлері Баптау бағдарламасы бірдебір көрінісін таба алмады, сондықтан көрсететін ештеңе жоқ. Баптау бағдарламасы бірдебір көрінісін таба алмады, сондықтан баптайтын ештеңе жоқ. Белсенді модулінде сақталмаған өзгерістер бар.
Олар іске асырылсын ба, әлде ысырып тасталсын ба? Бұтақ көрінісі Көрініс стилі "Жүйе параметрлері" деген компьютеріңізді баптау орталығына қош келдіңіз. Will Stephenson 