��    
      l      �       �   $   �   %     :   <     w  '   �  -   �     �       '     �  <  1   �  9     F   E  I   �  a   �  <   8  (   u     �  <   �         	                            
        Could not detect the file's mimetype Could not find all required functions Could not find the provider with the specified destination Error trying to execute script Invalid path for the requested provider It was not possible to read the selected file Service was not available Unknown Error You must specify a URL for this service Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-12-02 06:47+0600
Last-Translator: Sairan Kikkarin <sairan(at)computer.org>
Language-Team: Kazakh <kde-i18n-doc@kde.org>
Language: kk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=1; plural=0;
 Файлдың mime-түрі анқталмады Қажетті функциялары түгел емес Көздеген орны бар провайдер табылмады Скриптті орындау кезде қате пайда болды Сұралған провайдер үшін қапшығы дұрыс келтірілмеген Таңдалған файлды оқу мүмкін емес Қызмет қол жеткізбеді Беймәлім қате Бұл қызметтің URL-ын келтіру керек 