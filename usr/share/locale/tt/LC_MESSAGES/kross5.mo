��            )   �      �  &   �     �     �     �     �      �               .     6  ,   S  3   �     �     �     �     �     �  '        4     S     Y     o     �     �     �     �     �  &   �     �  �  �     �  !   �     �     �     �     �       @         a  @   s  P   �  W     B   ]  @   �  	   �     �     �  E   	     _	  	   n	     x	  5   �	     �	  >   �	     
  :   '
     b
  @   q
     �
                                       
                      	                                                                            @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2011-11-26 15:12+0400
Last-Translator: Ainur Shakirov <ainur.shakirov.tt@gmail.com>
Language-Team: Tatar <>
Language: tt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.2
 Төп Яңа скриптны төзү. Өстәү... Үткәрмәскәме? Тасвирлама: ainur.shakirov.tt@gmail.com Үзгәртү Сайланып алынган скриптны үзгәртү. Үзгәртү... Сайланып алынган скриптны башкару. «%1» интепретаторының скриптын ясап булмады «%1» скриптының интепретаторы билгеләп булмады. «%1» интерпретаторын йөкләп булмады. «%1» скриптының файлын ачып булмады Файл: Билгечек: Интерпретатор: Ruby интепретаторының иминлек дәрәҗәсе Ainur Shakirov Исем: «%1» функциясе юк. «%1» интерпретаторы билгесез. Бетерү Сайланып алынган скриптны бетерү. Җибәрү «%1» скриптының файлы табылмады. Туктату Сайланып алынган скриптны туктату. Текст: 