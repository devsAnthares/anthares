��          �      �           	          &     -     4  
   =     H     Q     Y     i  >   w     �     �     �     �  
   �     �     �     �            U   %  �  {     W     n  
   �     �  	   �  
   �     �     �     �     �  6   �          *     1     G     X     h     o     |  "   �     �  i   �                       	                                                
                                 %1 is running %1 not running &Reset &Start Advanced Appearance Command: Display Execute command Notifications Remaining time left: %1 second Remaining time left: %1 seconds Run command S&top Show notification Show seconds Show title Text: Timer Timer finished Timer is running Title: Use mouse wheel to change digits or choose from predefined timers in the context menu Project-Id-Version: plasma_applet_org.kde.plasma.timer
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2015-04-22 18:27-0300
Last-Translator: André Marcelo Alvarenga <alvarenga@kde.org>
Language-Team: Brazilian Portuguese <kde-i18n-pt_br@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 1.5
 %1 está em execução %1 não está em execução &Reiniciar &Iniciar Avançado Aparência Comando: Visor Executar comando Notificações Tempo restante: %1 segundo Tempo restante: %1 segundos Executar comando &Parar Mostrar notificação Mostrar segundos Mostrar título Texto: Temporizador O temporizador terminou O temporizador está em execução Título: Use a roda do mouse para mudar os dígitos ou escolher os temporizadores predefinidos no menu de contexto 