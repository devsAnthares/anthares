��            )   �      �     �  
   �  /   �  -   �          !  
   2     =     J     `  W   i     �  ,   �  	                  !     '     -  
   :     E     W     o     �     �     �     �  1   �       �       �  
   �                    4  
   I     T     `  
   y  d   �     �  5   �  	   5     ?  
   L     W     _     d     t     �     �     �     �     �     �     	     9	     Q	                                                                                  
                                	                       %1@%2 %2@%3 (%1) @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Favorites All Applications Appearance Applications Applications updated. Computer Drag tabs between the boxes to show/hide them, or reorder the visible tabs by dragging. Edit Applications... Expand search to bookmarks, files and emails Favorites Hidden Tabs History Icon: Leave Menu Buttons Often Used On All Activities On The Current Activity Remove from Favorites Show In Favorites Show applications by name Sort alphabetically Switch tabs on hover Type is a verb here, not a nounType to search... Visible Tabs Project-Id-Version: plasma_applet_org.kde.plasma.kickoff
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2017-09-14 16:31-0300
Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>
Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 %1@%2 %2@%3 (%1) Escolher... Limpar ícone Adicionar aos Favoritos Todos os aplicativos Aparência Aplicativos Aplicativos atualizados. Computador Arraste abas entre as caixas para mostrar/ocultar elas, ou reordene as abas visíveis arrastando-as. Editar aplicativos... Expandir a pesquisa nos favoritos, arquivos e e-mails Favoritos Abas ocultas Histórico Ícone: Sair Botões do menu Usados frequentemente Em todas as atividades Na atividade atual Remover dos Favoritos Exibir nos Favoritos Mostrar aplicativos pelo nome Classificar alfabeticamente Alternar abas ao passar o mouse Digite para procurar... Abas visíveis 