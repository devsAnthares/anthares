��          �      �       0  4   1     f     s      �     �     �     �  Q   �     -  &   L     s     �  �  �  4   `     �     �     �     �  !   �     �  b   �     b     {     �     �         	      
                                           (c) 2006 Hans van Leeuwen
(c) 2008-2010 Armin Berres Armin Berres Current author EMAIL OF TRANSLATORSYour emails Hans van Leeuwen KDE version of ssh-askpass Ksshaskpass Ksshaskpass allows you to interactively prompt users for a passphrase for ssh-add NAME OF TRANSLATORSYour names Name of a prompt for a passwordPrompt Original author Please enter passphrase Project-Id-Version: ksshaskpass
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-22 05:38+0100
PO-Revision-Date: 2015-01-12 07:25-0200
Last-Translator: André Marcelo Alvarenga <alvarenga@kde.org>
Language-Team: Brazilian Portuguese <kde-i18n-pt_br@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 1.5
 (c) 2006 Hans van Leeuwen
(c) 2008-2010 Armin Berres Armin Berres Autor atual alvarenga@kde.org Hans van Leeuwen Versão do ssh-askpass para o KDE Ksshaskpass O Ksshaskpass permite-lhe solicitar a frase-senha de forma interativa para os usuários do ssh-add André Marcelo Alvarenga Linha de comando Autor original Por favor, digite a frase-senha 