��          �   %   �      @     A  �   `  m   �  �   P  )   �  `        o     |     �     �     �      �     �     �  '        ,     >     ]     b     s  ?   �  Y   �  +     H   F  �  �     P     o  e   �     U	     q	  ]   �	     �	  #   �	     
     ,
     8
     X
     j
     {
  (   �
     �
     �
     �
     �
       D     c   W  >   �  O   �                                                 	                                                               
              (c) 2003-2007 Fredrik Höglund <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) @item:inlistbox sizeResolution dependent A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Cursor Theme Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Fredrik Höglund Get new Theme Get new color schemes from the Internet Install from File NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. You have to restart the Plasma session for these changes to take effect. Project-Id-Version: kcmmousetheme
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2017-10-25 11:31-0300
Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>
Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 (c) 2003-2007 Fredrik Höglund <qt>Deseja realmente remover o tema de cursores <i>%1</i>?<br />Isto excluirá todos os arquivos instalados por este tema.</qt> <qt>Não é possível excluir o tema em uso.<br />Você precisa trocar para outro tema primeiro.</qt> (Tamanhos disponíveis: %1) Dependente de resolução Já existe um tema chamado %1 na sua pasta de temas de ícone. Deseja substituí-lo por este? Confirmação Configurações do cursor alteradas Tema de cursores Descrição Arraste ou digite a URL do tema alvarenga@kde.org Fredrik Höglund Baixar novo tema Baixar novo esquema de cores da Internet Instalar a partir do arquivo André Marcelo Alvarenga Nome Sobrescrever o tema? Remover tema O arquivo %1 não parece ser um arquivo de tema de cursores válido. Não foi possível baixar o arquivo de tema de cursores. Verifique se o endereço %1 está correto. Não foi possível encontrar o arquivo de tema de cursores %1. A sessão do Plasma deve ser reiniciada para que as alterações tenham efeito. 