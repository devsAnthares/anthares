��          t      �         :        L     Y     g     {     �     �  2   �  @   �    "  �  $  A   �     '     :     P     g  #   v     �  4   �  8   �  $                        
                      	                Ensuring that the screen gets locked before going to sleep Lock Session Screen Locker Screen lock enabled Screen locked Screen saver timeout Screen unlocked Sets the minutes after which the screen is locked. Sets whether the screen will be locked after the specified time. The screen locker is broken and unlocking is not possible anymore.
In order to unlock switch to a virtual terminal (e.g. Ctrl+Alt+F2),
log in and execute the command:

loginctl unlock-session %1

Afterwards switch back to the running session (Ctrl+Alt+F%2). Project-Id-Version: kscreenlocker
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-03 03:06+0200
PO-Revision-Date: 2017-09-14 16:26-0300
Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>
Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 Garantir que a tela fique bloqueada após o retorno da suspensão Bloquear a sessão Bloqueador de sessão Bloqueio de tela ativo Tela bloqueada Tempo de espera do protetor de tela Tela desbloqueada Define a quantidade de minutos para bloquear a tela. Define se a tela será bloqueada após o tempo indicado. O bloqueio de tela está com problemas e não é possível desbloquear.
Para permitir o desbloqueio, mude para um terminal virtual (p.ex. Ctrl+Alt+F2),
faça a autenticação e execute o comando:

loginctl unlock-sessions %1

Depois disso, retorne para a sessão em execução (Ctrl+Alt+F%2). 