��    Z      �     �      �     �     �     �  5   �  #     E   A  E   �  >   �     	     #	  7   5	     m	     �	     �	     �	     �	     �	     
     
     
     #
     6
     N
     \
     q
     �
     �
     �
     �
     �
  !   �
  F   �
     3     P     a  <   s     �     �  *   �     �      �          #     3  	   ;     E     U     \      e     �  
   �     �     �     �  
   �  F   �  :   ;     v     ~     �     �     �  8   �     �     �  	      
   
          %     .     ?     T     l     t     �     �     �     �     �  &   �       
         +     ;     [     c     t     �  $   �  �  �     �     �     �  5   �  +   �  F     F   ^  ?   �     �  
   �  M        U     o  !   �  !   �     �     �     �     �  
             %     B     W     v     �     �  !   �     �     �  .   �  V     -   s     �     �  J   �  	        #  5   ,     b     p     �     �     �  	   �     �     �  	   �  ,   �  #   
     .  "   <     _     u     }  ]   �  A   �     0     =     E     V     _  D   n  	   �     �     �     �     �                      9     V     ]     |     �  &   �     �     �     �  *   �               ,     ;     J      _     �  0   �                @                    ;   *   !   M   X          ?       9                  
   U   :   B   =      N      8   G          O       7                             6       <   '          +   0   R              -   "              V   C           >   A         2   F   /   4          ,          )      1             (   P   3               I   #   W   %   S   .   Q          T   	   L   5                            Y   D   E   J   &   $   H      K   Z                      
Also available in %1 %1 (%2) <b>%1</b> by %2 <em>%1 out of %2 people found this review useful</em> <em>Tell us about this review!</em> <em>Useful? <a href='true'><b>Yes</b></a>/<a href='false'>No</a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'><b>No</b></a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'>No</a></em> @infoFetching updates @infoFetching... @infoIt is unknown when the last check for updates was @infoLooking for updates @infoNo updates @infoNo updates are available @infoShould check for updates @infoThe system is up to date @infoUpdates @infoUpdating... Accept Addons Aleix Pol Gonzalez An application explorer Apply Changes Available backends:
 Available modes:
 Back Cancel Checking for updates... Comment too long Comment too short Compact Mode (auto/compact/full). Could not close the application, there are tasks that need to be done. Could not find category '%1' Couldn't open %1 Delete the origin Directly open the specified application by its package name. Discard Discover Display a list of entries with a category. Extensions... Failed to remove the source '%1' Help... Improve summary Install Installed Jonathan Thomas Launch License: List all the available backends. List all the available modes. Loading... Local package file to install More Information... More... No Updates Open Discover in a said mode. Modes correspond to the toolbar buttons. Open with a program that can deal with the given mimetype. Rating: Remove Resources for '%1' Review Reviewing '%1' Running as <em>root</em> is discouraged and unnecessary. Search Search in '%1'... Search... Search: %1 Search: %1 + %2 Settings Short summary... Show reviews (%1)... Sorry, nothing found... Source: Specify the new source for %1 Still looking... Summary: Supports appstream: url scheme Tasks Tasks (%1%) TransactioName TransactionStatus%1 %2 Unable to find resource: %1 Update All Update Selected Update section nameUpdate (%1) Updates unknown reviewer updates not selected updates selected © 2010-2016 Plasma Development Team Project-Id-Version: plama-discover
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:02+0100
PO-Revision-Date: 2017-12-12 14:42-0300
Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>
Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 
Também disponível em %1 %1 (%2) <b>%1</b> por %2 <em>%1 em %2 pessoas acharam útil esta análise</em> <em>Diga-nos algo sobre esta análise!</em> <em>Útil? <a href='true'><b>Sim</b></a>/<a href='false'>Não</a></em> <em>Útil? <a href='true'>Sim</a>/<a href='false'><b>Não</b></a></em> <em>Útil? <a href='true'>Sim</a>/<a href='false'>Não</a></em> Obtendo atualizações Obtendo... Não é conhecido quando a última verificação por atualizações foi feita Procurando atualizações Sem atualizações Nenhuma atualização disponível Deve verificar por atualizações O sistema está atualizado Atualizações Atualizando... Aceitar Extensões Aleix Pol Gonzalez Um explorador de aplicativos Aplicar alterações Infraestruturas disponíveis:
 Modos disponíveis:
 Voltar Cancelar Verificando por atualizações... Comentário muito longo Comentário muito curto Modo compacto (automático/compacto/completo). Não foi possível fechar o aplicativo, pois há tarefas que precisam ser concluídas. Não foi possível encontrar a categoria '%1' Não foi possível abrir %1 Excluir a origem Abre diretamente o aplicativo indicado de acordo com o nome do seu pacote. Descartar Discover Mostrar uma lista de itens com determinada categoria. Extensões... Falha ao remover a fonte: '%1' Ajuda... Melhorar resumo Instalar Instalado Jonathan Thomas Iniciar Licença: Lista todas as infraestruturas disponíveis. Listar todos os modos disponíveis. Carregando... Arquivo de pacote local a instalar Mais informações... Mais... Sem atualizações Abre o Discover no modo informado. Os modos correspondem aos botões da barra de ferramentas. Abrir com um programa que consiga lidar com o tipo MIME indicado. Avaliação: Remover Recursos de '%1' Revisão Revisando '%1' A execução como <em>root</em> não é recomendada nem necessária. Pesquisar Pesquisar em '%1'... Pesquisar... Pesquisar: %1 Pesquisar: %1 + %2 Configurações Breve resumo... Exibit revisões (%1)... Desculpe, nada encontrada... Fonte: Indique a nova fonte para '%1' Ainda procurando... Resumo: Suporta o esquema de URLs 'appstream:' Tarefas Tarefas (%1%) %1 %2 Não foi possível encontrar o recurso: %1 Atualizar tudo Atualizar selecionados Atualizar (%1) Atualizações revisor desconhecido atualizações não selecionadas atualizações selecionadas © 2010-2016 equipe de desenvolvimento do Plasma 