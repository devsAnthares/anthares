��    $      <  5   \      0      1     R     ^  -   }  #   �  #   �  ?   �  1   3     e     y  H   ~  L   �       V     N   s     �  
   �     �     �     �       2     
   P     [  )   {  8   �  #   �  .     -   1  D   _  6   �  0   �  A     =   N  9   �  �  �  "   �
     �
  0   �
  6     
   ;  
   F     Q     b     s     �     �     �  
   �     �     �     �     �     �          &     C     L     Y  $   i  6   �     �  8   �  C     "   T     w     �     �     �  
   �     �                                                
       "                                       $                                     #      	           !                                %1 notification %1 notifications %1 of %2 %3 %1 running job %1 running jobs &Configure Event Notifications and Actions... 10 seconds ago, keep short10 s ago 30 seconds ago, keep short30 s ago A button tooltip; expands the item to show detailsShow Details A button tooltip; hides item detailsHide Details Clear Notifications Copy Either just 1 dir or m of n dirs are being processed1 dir %2 of %1 dirs Either just 1 file or m of n files are being processed1 file %2 of %1 files History How much many bytes (or whether unit in the locale has been copied over total%1 of %2 Indicator that there are more urls in the notification than previews shown+%1 Information Job Failed Job Finished No new notifications. No notifications or jobs Open... Placeholder is job name, eg. 'Copying'%1 (Paused) Select All Show a history of notifications Show application and system notifications Speed and estimated time to completion%1 (%2 remaining) Track file transfers and other jobs Use custom position for the notification popup minutes ago, keep short%1 min ago %1 min ago notification was added n days ago, keep short%1 day ago %1 days ago notification was added yesterday, keep shortYesterday notification was just added, keep shortJust now placeholder is row description, such as Source or Destination%1: the job, which can be anything, failed to complete%1: Failed the job, which can be anything, has finished%1: Finished Project-Id-Version: plasma_applet_org.kde.plasma.notifications
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:07+0100
PO-Revision-Date: 2017-09-14 16:32-0300
Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>
Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 %1 notificação %1 notificações %1 de %2 %3 %1 tarefa em execução %1 tarefas em execução &Configurar as notificações e ações dos eventos... 10s atrás 30s atrás Mostrar detalhes Ocultar detalhes Limpar notificações Copiar 1 pasta %2 de %1 pastas 1 arquivo %2 de %1 arquivos Histórico %1 de %2 +%1 Informações Falha na tarefa Tarefa concluída Sem novas notificações. Sem notificações e tarefas Abrir... %1 (Pausado) Selecionar tudo Mostrar histórico de notificações Mostrar as notificações dos aplicativos e do sistema %1 (%2 restantes) Rastrear as transferências de arquivos e outras tarefas Usar uma posição personalizada para as mensagens de notificação %1 minuto atrás %1 minutos atrás %1 dia atrás %1 dias atrás Ontem Agora %1: %1: Falhou %1: Concluído 