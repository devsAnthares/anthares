��          D      l       �   m   �      �   R   	  D   \  �  �  q   s  %   �  R        ^                          Calculates the value of :q: when :q: is made up of numbers and mathematical symbols such as +, -, /, * and ^. Copy to Clipboard The exchange rates could not be updated. The following error has been reported: %1 The result of the calculation is only an approximationApproximation Project-Id-Version: plasma_runner_calculatorrunner
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-28 05:09+0200
PO-Revision-Date: 2017-07-29 20:39-0300
Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>
Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 Calcula o valor do :q: quando o :q: é composto por números e símbolos matemáticos, tais como: +, -, /, * e ^. Copiar para a área de transferência As taxas de câmbio não puderam ser atualizadas. O seguinte erro foi relatado: %1 Aproximação 