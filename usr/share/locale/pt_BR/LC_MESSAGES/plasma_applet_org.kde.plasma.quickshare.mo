��          �      <      �     �     �     �  +   �  @        L     a     i     w     }     �  
   �     �     �     �     �     �  �  
     �     �       1     ?   P      �     �     �     �     �     �     �               -     A  !   ]     
         	                                                                                       <a href='%1'>%1</a> Close Copy Automatically: Don't show this dialog, copy automatically. Drop text or an image onto me to upload it to an online service. Error during upload. General History Size: Paste Please wait Please, try again. Sending... Share Shares for '%1' Successfully uploaded The URL was just shared Upload %1 to an online service Project-Id-Version: plasma_applet_org.kde.plasma.quickshare
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-01 04:04+0100
PO-Revision-Date: 2015-04-25 08:06-0300
Last-Translator: André Marcelo Alvarenga <alvarenga@kde.org>
Language-Team: Brazilian Portuguese <kde-i18n-pt_br@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 1.5
 <a href='%1'>%1</a> Fechar Copiar automaticamente: Não mostrar essa janela, copiar automaticamente. Solte um texto ou imagem para ser enviado a um serviço online. Ocorreu um erro durante o envio. Geral Tamanho do histórico: Colar Por favor, aguarde Tente novamente. Enviando... Compartilhar Compartilhamentos para '%1' Enviado com sucesso A URL só foi compartilhada Enviar %1 para um serviço online 