��          �      L      �  
   �     �  >   �                  
   -     8     @     H     O  	   [     e     s     z  2   �     �     �  �  �     �     �  F   �     �     �     �  
                	   )     3  
   F     Q     b     i  ?   ~     �     �                                   	                                                         
        &Browse... &Search: *.png *.xpm *.svg *.svgz|Icon Files (*.png *.xpm *.svg *.svgz) Actions All Applications Categories Devices Emblems Emotes Icon Source Mimetypes O&ther icons: Places S&ystem icons: Search interactively for icon names (e.g. folder). Select Icon Status Project-Id-Version: kiconthemes5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:11+0100
PO-Revision-Date: 2017-12-12 15:52-0300
Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>
Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 &Navegar... Pesqui&sar: *.png *.xpm *.svg *.svgz|Arquivos de ícone (*.png *.xpm *.svg *.svgz) Ações Todos Aplicativos Categorias Dispositivos Emblemas Emoções Origem dos ícones Tipos MIME Ou&tros ícones: Locais Ícones do s&istema: Pesquisar interativamente por nomes de ícones (p. ex., pasta). Selecionar ícone Estado 