��          |      �          %   !     G     U     c     u     �     �  
   �     �     �  $   �  �  �  ;   �     �     �  %        )     C     I     _     r     �  "   �     
   	                                                   Automatically copy color to clipboard Clear History Color Options Copy to Clipboard Default color format: General Open Color Dialog Pick Color Pick a color Show history When pressing the keyboard shortcut: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-12 03:03+0200
PO-Revision-Date: 2017-03-02 18:49-0300
Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>
Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 Copiar a cor automaticamente para a área de transferência Limpar histórico Opções de cores Copiar para a área de transferência Formato de cores padrão: Geral Abrir janela de cores Selecionar uma cor Selecionar uma cor Mostrar histórico Ao pressionar o atalho de teclado: 