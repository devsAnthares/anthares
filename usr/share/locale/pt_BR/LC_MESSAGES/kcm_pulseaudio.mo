��    $      <  5   \      0     1     6  )   J     t  $   �  &   �  #   �  !   �  "   !     D     T     f     z     �  J   �     �  L        [     c     k      {     �  
   �     �     �     �     �     �  "   �       )   9  <   c     �  ;   �     �  �       �     �     �     �  %   	  !   -	  (   O	  )   x	  )   �	     �	  
   �	  	   �	     �	     �	  `   
  "   c
  _   �
     �
     �
     �
          !     *     =     V     l     t     �     �     �  -   �  ?   �                %     	                                                                         !       #             "                                        
           $                               100% @info:creditAuthor @info:creditCopyright 2015 Harald Sitter @info:creditHarald Sitter @labelNo Applications Playing Audio @labelNo Applications Recording Audio @labelNo Device Profiles Available @labelNo Input Devices Available @labelNo Output Devices Available @labelProfile: @titlePulseAudio @title:tabAdvanced @title:tabApplications @title:tabDevices Add virtual output device for simultaneous output on all local sound cards Advanced Output Configuration Automatically switch all running streams when a new output becomes available Capture Default Device Profiles EMAIL OF TRANSLATORSYour emails Inputs Mute audio NAME OF TRANSLATORSYour names Notification Sounds Outputs Playback Port Port is unavailable (unavailable) Port is unplugged (unplugged) Requires 'module-gconf' PulseAudio module This module allows to set up the Pulseaudio sound subsystem. label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: kcm_pulseaudio
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-03 03:00+0200
PO-Revision-Date: 2017-07-28 11:52-0300
Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>
Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 100% Autor Copyright 2015 Harald Sitter Harald Sitter Nenhum aplicativo reproduzindo áudio Nenhum aplicativo gravando áudio Nenhum perfil de dispositivo disponível Nenhum dispositivo de entrada disponível Nenhum dispositivos de saída disponível Perfil: PulseAudio Avançado Aplicativos Dispositivos Adicionar dispositivo de saída virtual para saída simultânea em todas as placas de som locais Configuração de saída avançada Trocar automaticamente todos os fluxos em execução quando uma nova saída estiver disponível Capturar Padrão Perfis dos dispositivos alvarenga@kde.org Entradas Silenciar o áudio André Marcelo Alvarenga Sons de notificação Saídas Reprodução Porta  (não disponível)  (desconectado) Requer o módulo do PulseAudio 'module-gconf' Este módulo permite configurar o subsistema de som PulseAudio. %1: %2 100% %1% 