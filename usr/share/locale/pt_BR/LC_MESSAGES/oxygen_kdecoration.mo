��    >        S   �      H     I  !   e  "   �  &   �  ,   �  #   �  &   "  !   I  &   k  '   �  "   �  #   �  "     '   $     L     _  +   c  
   �     �     �     �     �     �     �  U   �  7   J     �     �     �     �      �     �     �     	     	  !   &	     H	  	   M	     W	     _	     	     �	  &   �	     �	     �	     �	     
     
     #
     5
  4   =
  $   r
     �
     �
     �
     �
     �
            &   )     P  �  j  *   6     a     h  	   o     y     �     �  
   �     �     �     �     �     �     �     �  	     7        E     Q     g  /   y     �     �     �  �   �  S   V     �     �     �     �  ,   �          =     O  $   U  (   z     �     �     �  *   �  $   �     	  ,     $   I     n  '   v     �     �     �  	   �  A   �  .        B  !   [     }     �     �  "   �     �  /   �  &   &         #          1   &                  	   <          6       =   0   2          +                                 '   ;              5   -             /   !      *               3   )                    
          $             ,   9   8   :           %       "      (       >   4      7      .       &Matching window property:  @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Border @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large @item:inlistbox Button size:Large @item:inlistbox Button size:Normal @item:inlistbox Button size:Small @item:inlistbox Button size:Very Large Active Window Glow Add Add handle to resize windows with no border Animations B&utton size: Border size: Button mouseover transition Center Center (Full Width) Class:  Configure fading between window shadow and glow when window's active state is changed Configure window buttons' mouseover highlight animation Decoration Options Detect Window Properties Dialog Edit Edit Exception - Oxygen Settings Enable/disable this exception Exception Type General Hide window title bar Information about Selected Window Left Move Down Move Up New Exception - Oxygen Settings Question - Oxygen Settings Regular Expression Regular Expression syntax is incorrect Regular expression &to match:  Remove Remove selected exception? Right Shadows Tit&le alignment: Title:  Use the same colors for title bar and window content Use window class (whole application) Use window title Warning - Oxygen Settings Window Class Name Window Drop-Down Shadow Window Identification Window Property Selection Window Title Window active state change transitions Window-Specific Overrides Project-Id-Version: oxygen_kdecoration
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-13 05:20+0100
PO-Revision-Date: 2016-02-05 07:31-0300
Last-Translator: André Marcelo Alvarenga <alvarenga@kde.org>
Language-Team: Brazilian Portuguese <kde-i18n-pt_br@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 &Correspondente à propriedade da janela:  Enorme Grande Sem borda Sem bordas laterais Normal Ainda maior Minúsculo Realmente grande Muito grande Grande Normal Pequeno Muito grande Brilho da janela ativa Adicionar Adicionar alça para redimensionar as janelas sem borda Animações Tamanho dos &botões: Tamanho da borda: Transição da passagem do mouse sobre o botão Centro Centro (Largura total) Classe:  Configura a transição com desaparecimento gradual entre a sombra e o brilho de uma janela, quando o estado de atividade é alterado Configura o realce animado durante a passagem do mouse sobre os botões das janelas Opções de decoração Detectar propriedades da janela Janela Editar Editar exceção - Configurações do Oxygen Ativar/desativar esta exceção Tipo de exceção Geral Ocultar a barra de título da janela Informações sobre a janela selecionada Esquerda Descer Subir Nova exceção - Configurações do Oxygen Pergunta - Configurações do Oxygen Expressão regular A sintaxe da expressão regular é incorreta &Expressão regular a corresponder:  Remover Deseja remover a exceção selecionada? Direita Sombras A&linhamento do título: Título:  Usar as mesmas cores na barra de título e no conteúdo da janela Usar classe da janela (para todo o aplicativo) Usar o título da janela Aviso - Configurações do Oxygen Nome da classe da janela Sombra da janela Identificação da janela Seleção da propriedade da janela Título da janela O estado da janela ativa altera as transições Substituições específicas da janela 