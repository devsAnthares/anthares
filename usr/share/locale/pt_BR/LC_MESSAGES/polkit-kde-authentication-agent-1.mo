��            )   �      �  ;   �  J   �          /  ~   7  A   �     �       )        G     X     i      q     �     �     �  
   �     �  
   �     �             "   <     _  	   y     �     �     �  �  �     �     �     �     �  �   �  A   =          �  )   �     �     �     �     	     	     *	     :	  
   H	     S	     l	     t	     �	  $   �	  (   �	     �	     
     
     !
     5
                                      
                                                                                            	        %1 is the full user name, %2 is the user login name%1 (%2) %1 is the name of a detail about the current action provided by polkit%1: (c) 2009 Red Hat, Inc. Action: An application is attempting to perform an action that requires privileges. Authentication is required to perform this action. Another client is already authenticating, please try again later. Application: Authentication Required Authentication failure, please try again. Click to edit %1 Click to open %1 Details EMAIL OF TRANSLATORSYour emails Former maintainer Jaroslav Reznik Lukáš Tinkl Maintainer NAME OF TRANSLATORSYour names P&assword: Password for %1: Password for root: Password or swipe finger for %1: Password or swipe finger for root: Password or swipe finger: Password: PolicyKit1 KDE Agent Select User Vendor: Project-Id-Version: polkit-kde-authentication-agent-1
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:22+0100
PO-Revision-Date: 2015-01-09 07:54-0200
Last-Translator: André Marcelo Alvarenga <alvarenga@kde.org>
Language-Team: Brazilian Portuguese <kde-i18n-pt_br@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 1.5
 %1 (%2) %1: (c) 2009 Red Hat, Inc. Ação: Um aplicativo está tentando executar uma ação que necessita de privilégios. A autenticação é necessária para executar essa ação. Outro cliente já está autenticando, tente novamente mais tarde. Aplicativo: Autenticação necessária Falha na autenticação, tente novamente. Clique para editar %1 Clique para abrir %1 Detalhes alvarenga@kde.org Mantenedor anterior Jaroslav Reznik Lukáš Tinkl Mantenedor André Marcelo Alvarenga Senh&a: Senha para %1: Senha para o root: Senha ou impressão digital para %1: Senha ou impressão digital para o root: Senha ou impressão digital: Senha: Agente do KDE PolicyKit1 Selecionar usuário Fabricante: 