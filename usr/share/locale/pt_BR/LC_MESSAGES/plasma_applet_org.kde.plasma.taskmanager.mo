��    O      �  k         �     �     �     �     �     �     �     �  .   �  U   (     ~     �      �     �  /   �  /        5     A     J  
   V     a     q  $   �     �     �     �     �     �     �  	   	     	  
   	     )	     <	     O	     g	  	   m	     w	  !   �	     �	     �	  	   �	      �	     �	     �	     
     $
     5
     :
     G
     M
     _
     w
  (   �
     �
  =   �
            "   5     X     s  J   {  1   �  )   �  (   "  '   K  "   s  4   �     �     �     �     �     �          "  U   8  N   �  9   �  J     �  b     >     [     c     q     x     �  
   �     �     �     �     �  (   �       8   /  8   h     �     �     �     �     �     �  (        7     D     Q     Y     y          �     �     �     �     �     �       
   $     /  #   ;     _     s  
   �  #   �     �  %   �     �     �  	        (     8     >     U     h  ;   {     �  	   �     �     �     �             	          ,   <  4   i  '   �  %   �  C   �     0     >     F     S  
   o     z     �     �     �     �  "   �     %   N         @   A       >   +   "       ,       F   /   B   O           9      ;      $                      G          3          K         ?                        2   L   *      !      H   =                   <              '               :       -            .   I   J   6   C           M       1          #   0      4       7       (   	   
           8               D   5   )   &   E    &All Desktops &Close &Fullscreen &Move &New Desktop &Pin &Shade 1 = number of desktop, 2 = desktop name&%1 %2 Activities a window is currently on (apart from the current one)Also available on %1 Add To Current Activity All Activities Allow this program to be grouped Alphabetically Always arrange tasks in columns of as many rows Always arrange tasks in rows of as many columns Arrangement Behavior By Activity By Desktop By Program Name Close Window or Group Cycle through tasks with mouse wheel Do Not Group Do Not Sort Filters Forget Recent Documents General Grouping and Sorting Grouping: Highlight windows Icon size: Keep &Above Others Keep &Below Others Keep launchers separate Large Ma&ximize Manually Mark applications that play audio Maximum columns: Maximum rows: Mi&nimize Minimize/Restore Window or Group More Actions Move &To Current Desktop Move To &Activity Move To &Desktop Mute New Instance On %1 On All Activities On The Current Activity On middle-click: Only group when the task manager is full Open groups in popups Open or bring to the front window of media player appRestore Pause playbackPause Play next trackNext Track Play previous trackPrevious Track Quit media player appQuit Re&size Remove launcher button for application shown while it is not runningUnpin Show all user Places%1 more Place %1 more Places Show only tasks from the current activity Show only tasks from the current desktop Show only tasks from the current screen Show only tasks that are minimized Show progress and status information in task buttons Show tooltips Small Sorting: Start New Instance Start playbackPlay Stop playbackStop The click actionNone Toggle action for showing a launcher button while the application is not running&Pin When clicking it would toggle grouping windows of a specific appGroup/Ungroup Which activities a window is currently onAvailable on %1 Which virtual desktop a window is currently onAvailable on all activities Project-Id-Version: plasma_applet_org.kde.plasma.taskmanager
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2017-11-27 15:08-0300
Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>
Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 Tod&as as áreas de trabalho Fe&char &Tela inteira &Mover &Nova área de trabalho &Fixar &Sombreado &%1 %2 Também disponível em %1 Adicionar à atividade atual Todas as atividades Permitir que este programa seja agrupado Ordem alfabética Sempre organizar as tarefas em colunas com muitas linhas Sempre organizar as tarefas em linhas com muitas colunas Organização Comportamento Por atividade Por área de trabalho Pelo nome do programa Fechar janela ou grupo Percorrer as tarefas com a roda do mouse Não agrupar Não ordenar Filtros Esquecer os documentos recentes Geral Agrupamento e ordenação Agrupamento: Realçar janelas Tamanho do ícone: Manter &acima das outras Manter a&baixo das outras Manter os lançadores separados Grande Ma&ximizar Manualmente Marcar aplicativos que tocam áudio Máximo de colunas: Máximo de linhas: Mi&nimizar Minimizar/restaurar janela ou grupo Mais ações Mover para a área de &trabalho atual Mover para &atividade Mover &para a área de trabalho Silenciar Nova instância Na %1 Em todas as atividades Na atividade atual No botão do meio: Somente grupo quando o gerenciador de tarefas estiver cheio Abrir grupos em mensagens Restaurar Pausar Próxima faixa Faixa anterior Sair Redimen&sionar Desafixar mais %1 lugar mais %1 lugares Mostrar apenas as tarefas da atividade atual Mostrar apenas as tarefas da área de trabalho atual Mostrar apenas as tarefas da tela atual Mostrar apenas as tarefas minimizadas Mostrar informações de progresso e status nos botões das tarefas Mostrar dicas Pequeno Ordenação: Iniciar uma nova instância Reproduzir Parar Nenhuma &Fixar Agrupar/desagrupar Disponível em %1 Disponível em todas as atividades 