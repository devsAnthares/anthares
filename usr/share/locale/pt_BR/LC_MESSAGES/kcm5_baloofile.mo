��          �            h     i     �  #   �      �      �     �  J        [  &   y     �  T   �       *   $     O  �  ]  &         E  &   f     �     �     �  Y   �     4  #   S     w  _   �     �  (         )                            	                                             
       Also index file content Configure File Search Copyright 2007-2010 Sebastian Trüg Do not search in these locations EMAIL OF TRANSLATORSYour emails Enable File Search File Search helps you quickly locate all your files based on their content Folder %1 is already excluded Folder's parent %1 is already excluded NAME OF TRANSLATORSYour names Not allowed to exclude root folder, please disable File Search if you do not want it Sebastian Trüg Select the folder which should be excluded Vishesh Handa Project-Id-Version: kcm_baloofile
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2017-10-25 12:43-0300
Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>
Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 Também indexar o conteúdo do arquivo Configura a Pesquisa de Arquivos Copyright 2007-2010 de Sebastian Trüg Não pesquisar nestes locais alvarenga@kde.org Ativar a Pesquisa de Arquivos A Pesquisa de Arquivos ajuda-lhe a encontrar todos os seus arquivos com base no conteúdo A pasta %1 já está excluída A pasta-mãe %1 já está excluída André Marcelo Alvarenga Não é permitido excluir a pasta raiz, desabilite a Pesquisa de arquivos se você não quiser. Sebastian Trüg Selecione a pasta que deve ser excluída Vishesh Handa 