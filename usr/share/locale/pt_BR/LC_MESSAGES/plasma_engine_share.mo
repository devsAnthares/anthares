��    
      l      �       �   $   �   %     :   <     w  '   �  -   �     �       '     �  <  2   	  <   <  D   y  +   �  .   �  ,     "   F     i  4   {         	                            
        Could not detect the file's mimetype Could not find all required functions Could not find the provider with the specified destination Error trying to execute script Invalid path for the requested provider It was not possible to read the selected file Service was not available Unknown Error You must specify a URL for this service Project-Id-Version: plasma_engine_share
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-01-02 12:05-0200
Last-Translator: André Marcelo Alvarenga <alvarenga@kde.org>
Language-Team: Brazilian Portuguese <kde-i18n-pt_br@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 1.5
 Não foi possível detectar o tipo MIME do arquivo Não foi possível encontrar todas as funções necessárias Não foi possível localizar o fornecedor com o destino especificado Ocorreu um erro ao tentar executar o script Caminho inválido para o fornecedor solicitado Não foi possível ler o arquivo selecionado O serviço não estava disponível Erro desconhecido Você precisa especificar uma URL para este serviço 