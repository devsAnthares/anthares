��    _                   C   	  /   M  -   }     �     �     �     �      	     	     1	     >	     J	  
   S	     ^	     g	     p	     �	  	   �	     �	     �	     �	  ,   �	  	    
     

  
   )
     4
     L
     `
     u
     �
     �
     �
     �
     �
  	   �
     �
     �
     
               !     (  	   ;     E     ]  
   r     }     �  
   �     �     �     �  
   �     �     �               $     2     @     R     h     y     �     �     �     �  	   �     �     �     �               4     Q     j     �     �     �     �  	   �     �  ,   �          !     0     @     L     S     b     t     �  #   �     �  �  �     �     �     �     �     �       3   #     W     l     t     �     �  
   �  
   �     �     �     �  
   �     �          $  5   5  	   k  !   u     �     �     �     �     �          '     <     \     b     j  
   s     ~     �     �     �     �     �     �  !   �           )     ?     V  
   i     t     �     �     �  	   �     �     �     �           	          6     L     c  #   w     �     �     �     �     �     �  #   �          +  ,   H  )   u  +   �     �     �          '     0     9  7   U  	   �     �     �     �     �     �     �            $   9     ^         Q         Y   5           %       H       J   !              \   *       @   V   4   [       A   (   
       U   B   ]   ^       $   _         '   >       D   K      -                  ?      ,   O   0       R   8   6   W   2   I   =   X   E   #       N   C       T   G   M          3   "         9             +          S          F           ;   :                    &   <   L          P         7          .       /       1                     )            	   Z       @action opens a software center with the applicationManage '%1'... @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Desktop Add to Favorites Add to Panel (Widget) Align search results to bottom All Applications App name (Generic name)%1 (%2) Applications Apps & Docs Behavior Categories Computer Contacts Description (Name) Description only Documents Edit Application... Edit Applications... End session Expand search to bookmarks, files and emails Favorites Flatten menu to a single level Forget All Forget All Applications Forget All Contacts Forget All Documents Forget Application Forget Contact Forget Document Forget Recent Documents General Generic name (App name)%1 (%2) Hibernate Hide %1 Hide Application Icon: Lock Lock screen Logout Name (Description) Name only Often Used Applications Often Used Documents Often used On All Activities On The Current Activity Open with: Pin to Task Manager Places Power / Session Properties Reboot Recent Applications Recent Contacts Recent Documents Recently Used Recently used Removable Storage Remove from Favorites Restart computer Run Command... Run a command or a search query Save Session Search Search results Search... Searching for '%1' Session Show Contact Information... Show In Favorites Show applications as: Show often used applications Show often used contacts Show often used documents Show recent applications Show recent contacts Show recent documents Show: Shut Down Sort alphabetically Start a parallel session as a different user Suspend Suspend to RAM Suspend to disk Switch User System System actions Turn off computer Type to search. Unhide Applications in '%1' Unhide Applications in this Submenu Widgets Project-Id-Version: plasma_applet_org.kde.plasma.kicker
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2017-09-14 16:30-0300
Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>
Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 Gerenciar '%1'... Escolher... Limpar ícone Adicionar à área de trabalho Adicionar aos Favoritos Adicionar ao painel (widget) Alinhar os resultados da pesquisa na parte inferior Todos os aplicativos %1 (%2) Aplicativos Aplicativos e documentos Comportamento Categorias Computador Contatos Descrição (Nome) Apenas a descrição Documentos Editar aplicativo... Editar aplicativos... Fechar a sessão Expandir a pesquisa nos favoritos, arquivos e e-mails Favoritos Achatar o menu a um único nível Esquecer todos Esquecer todos os aplicativos Esquecer todos os contatos Esquecer todos os documentos Esquecer o aplicativo Esquecer os contatos Esquecer o documento Esquecer os documentos recentes Geral %1 (%2) Hibernar Ocultar %1 Ocultar o aplicativo Ícone: Bloquear Bloquear a tela Encerrar sessão Nome (Descrição) Apenas o nome Aplicativos frequentemente usados Documentos usados frequentemente Usados frequentemente Em todas as atividades Na atividade atual Abrir com: Fixar no gerenciador de tarefas Locais Desligar / Sessão Propriedades Reiniciar Aplicativos recentes Contatos recentes Documentos recentes Recentes Usados recentemente Armazenamento removível Remover dos Favoritos Reiniciar o computador Executar comando... Executar um comando ou uma pesquisa Salvar sessão Procurar Resultados da pesquisa Procurar... Procurando por '%1' Sessão Mostrar informações do contato... Exibir nos Favoritos Mostrar os aplicativos como: Mostrar os aplicativos frequentemente usados Mostrar os contatos usados frequentemente Mostrar os documentos usados frequentemente Mostrar os aplicativos recentes Mostrar os contatos recentes Mostrar os documentos recentes Mostrar: Desligar Classificar alfabeticamente Iniciar uma sessão paralela como um usuário diferente Suspender Suspender para a RAM Suspender para o disco Trocar usuário Sistema Ações do sistema Desligar o computador Digite para procurar. Mostrar os aplicativos em '%1' Mostrar os aplicativos neste submenu Widgets 