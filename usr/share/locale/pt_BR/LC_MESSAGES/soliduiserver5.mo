��          L      |       �   >   �      �   9   �   A   &  
   h  �  s  D   :          �  @   �     �                                         '%1' needs a password to be accessed. Please enter a password. ... A default name for an action without proper labelUnknown A new device has been detected.<br><b>What do you want to do?</b> Do nothing Project-Id-Version: soliduiserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-01-17 10:00-0200
Last-Translator: André Marcelo Alvarenga <alvarenga@kde.org>
Language-Team: Brazilian Portuguese <kde-i18n-pt_br@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 1.5
 '%1' precisa de uma senha para ser acessado. Por favor, informe uma. ... Desconhecido Um novo dispositivo foi detectado.<br><b>O que deseja fazer?</b> Não fazer nada 