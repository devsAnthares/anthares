��          t      �         C        U     i  3   �     �     �  F   �  5   *     `       �  �  A   \     �     �     �     �     	  K   (  =   t  "   �     �                              	                          
    Attempting to perform the action '%1' failed with the message '%2'. Media playback next Media playback previous Name for global shortcuts categoryMedia Controller Play/Pause media playback Stop media playback The argument '%1' for the action '%2' is missing or of the wrong type. The media player '%1' cannot perform the action '%2'. The operation '%1' is unknown. Unknown error. Project-Id-Version: plasma_engine_mpris2
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-06 03:09+0200
PO-Revision-Date: 2015-02-18 11:13-0200
Last-Translator: André Marcelo Alvarenga <alvarenga@kde.org>
Language-Team: Brazilian Portuguese <kde-i18n-pt_br@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 1.5
 A tentativa de executar a ação '%1' falhou com a mensagem '%2'. Reproduzir a próxima mídia Reproduzir a mídia anterior Controlador de mídia Reproduzir/Pausar a mídia Parar a reprodução da mídia O argumento '%1' para a ação '%2' está faltando ou possui o tipo errado. O reprodutor de mídia '%1' não pode executar a ação '%2'. A operação '%1' é desconhecida. Erro desconhecido. 