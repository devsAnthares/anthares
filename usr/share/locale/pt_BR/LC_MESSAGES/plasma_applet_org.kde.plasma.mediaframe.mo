��            )   �      �     �     �     �     �     �     �     �  
           )   (     R     V     \     c     v     �     �     �  3   �     �       <   #  5   `  8   �  8   �                    /  �  1          "     5     F     ^     p     �     �     �  =   �     �     �  	             ,     L     a     s  1   {     �  )   �  V   �  H   L	  E   �	  E   �	     !
     -
     G
     _
                                                  
                                                                               	        Add files... Add folder... Background frame Change picture every Choose a folder Choose files Configure plasmoid... Fill mode: General Left click image opens in external viewer Pad Paths Paths: Pause on mouseover Preserve aspect crop Preserve aspect fit Randomize items Stretch The image is duplicated horizontally and vertically The image is not transformed The image is scaled to fit The image is scaled uniformly to fill, cropping if necessary The image is scaled uniformly to fit without cropping The image is stretched horizontally and tiled vertically The image is stretched vertically and tiled horizontally Tile Tile horizontally Tile vertically s Project-Id-Version: plasma_applet_org.kde.plasma.mediaframe
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-16 03:31+0100
PO-Revision-Date: 2017-12-12 14:57-0300
Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>
Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 Adicionar arquivos... Adicionar pasta... Fundo da moldura Alterar a imagem a cada Escolha uma pasta Escolha os arquivos Configurar plasmoid... Modo de preenchimento: Geral Abrir num visualizador externo com o botão esquerdo do mouse Normal Caminhos Caminhos: Pausar à passagem do mouse Manter a proporção no recorte Manter a proporção Itens aleatórios Alongar A imagem é duplicada na horizontal e na vertical A imagem não é transformada A escala da imagem é ajustada para caber A escala da imagem é ajustada de forma uniforme para caber, recortando se necessário A escala da imagem é ajustada de forma uniforme para caber sem recortes A imagem é esticada na horizontal e colocada lado a lado na vertical A imagem é esticada na vertical e colocada lado a lado na horizontal Lado a lado Lado a lado na horizontal Lado a lado na vertical s 