��            )   �      �     �     �     �     �     �     �     �     �  0        3     G     ]     c     o     w     �  
   �     �     �     �     �     �               (     A     I     a     m  �  s  	   B  '   L     t     �     �     �     �     �  0   �       !        <     B     U  "   ]     �     �     �     �  &   �     �       
   .     9     P     p     y     �  
   �                                            
                                                                                      	       %1 by %2 Add Custom Wallpaper Add Folder... Add Image... Background: Blur Centered Change every: Directory with the wallpaper to show slides from Download Wallpapers Get New Wallpapers... Hours Image Files Minutes Next Wallpaper Image Open Containing Folder Open Image Open Wallpaper Image Positioning: Recommended wallpaper file Remove wallpaper Restore wallpaper Scaled Scaled and Cropped Scaled, Keep Proportions Seconds Select Background Color Solid color Tiled Project-Id-Version: plasma_applet_org.kde.image
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:39+0100
PO-Revision-Date: 2017-12-12 14:53-0300
Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>
Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 %1 por %2 Adicionar papel de parede personalizado Adicionar pasta... Adicionar imagem... Plano de fundo: Borrado Centralizado Alterar a cada: Pasta com o papel de parede a mostrar como slide Baixar papéis de parede Baixar novos papéis de parede... Horas Arquivos de imagem Minutos Próxima imagem de papel de parede Abrir pasta que contém Abrir imagem Abrir imagem de papel de parede Posicionamento: Arquivo de papel de parede recomendado Remover papel de parede Restaurar papel de parede Escalonado Escalonado e recortado Escalonado, manter proporções Segundos Selecionar cor de fundo Cor sólida Ladrilhado 