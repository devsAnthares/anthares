��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  �  K       .     �  K  �  	  c   �
          "     4  .   G     v     �  .   �  "   �                                          
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-02-23 00:30-0300
Last-Translator: André Marcelo Alvarenga <andrealvarenga@gmx.net>
Language-Team: Brazilian Portuguese <kde-i18n-pt_br@mail.kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=(n > 1);
  s &Tempo limite do indicador de inicialização: <H1>Notificação na barra de tarefas</H1>
Você pode habilitar um segundo método de notificação de inicialização
que é usado pela barra de tarefas, onde aparece um disco girando, o que
indica que o aplicativo que você iniciou está sendo carregado.
Pode acontecer que certos aplicativos não se deem conta desta notificação.
Neste caso o botão desaparece ao fim do período indicado na seção
'Tempo limite da indicação de inicialização' <h1>Cursor de ocupado</h1>
O KDE pode mostrar um cursor de ocupado para notificar a inicialização de um aplicativo.
Para ativar o cursor de ocupado, selecione um tipo de retorno 
visual da caixa de seleção.
Pode ocorrer que certos aplicativos não se deem conta desta 
notificação. Neste caso, o cursor deixa de piscar ao fim do período
indicado na seção 'Tempo limite da indicação de inicialização' <h1>Retorno de lançamento</h1> Você pode configurar aqui o retorno do lançamento de aplicativos. Cursor piscante Cursor saltitante &Cursor de ocupado Habilitar a notificação na barra de &tarefas Nenhum cursor de ocupado Cursor de ocupado passivo Te&mpo limite do indicador de inicialização: &Notificação na barra de tarefas 