��    >        S   �      H  	   I  	   S     ]     e     s     y          �     �     �     �     �     �     �  
        "  ?   .     n  >   w  	   �     �     �  S   �      A     b  �  z     	     
	     	  
   '	     2	     B	     R	  
   _	     j	  A   y	     �	     �	  
   �	     �	     �	     
     #
     ,
     ;
     G
     X
     h
     |
     �
     �
     �
     �
     �
     �
               (  T   ;     �  S   �  �  �  	   �     �     �     �     �     �     �     �  $        2     ;     Z  $   r     �     �     �  ?   �  	     :   (  	   c     m     �  X   �  I   �     E  �  c               #     3     A     X     o  
   �     �  I   �  #   �          %     7  ;   H     �  	   �     �     �     �     �     �     �                6     S     l  &   �     �     �     �  k   �     ^  R   l            3          1   6       2   4      !   *         #   7                    (   9          &           :   5          +                    "                        '           -          >   =   /   	      )                ;   
      .      $   %       <   ,                    8      0            [Hidden] &Comment: &Delete &Description: &Edit &File &Name: &New Submenu... &Run as a different user &Sort &Sort all by Description &Sort all by Name &Sort selection by Description &Sort selection by Name &Username: &Work path: (C) 2000-2003, Waldo Bastian, Raffaele Sandrini, Matthias Elter Advanced All submenus of '%1' will be removed. Do you want to continue? Co&mmand: Could not write to %1 Current shortcut &key: Do you want to restore the system menu? Warning: This will remove all custom menus. EMAIL OF TRANSLATORSYour emails Enable &launch feedback Following the command, you can have several place holders which will be replaced with the actual values when the actual program is run:
%f - a single file name
%F - a list of files; use for applications that can open several local files at once
%u - a single URL
%U - a list of URLs
%d - the folder of the file to open
%D - a list of folders
%i - the icon
%m - the mini-icon
%c - the caption General General options Hidden entry Item name: KDE Menu Editor KDE menu editor Main Toolbar Maintainer Matthias Elter Menu changes could not be saved because of the following problem: Menu entry to pre-select Montel Laurent Move &Down Move &Up NAME OF TRANSLATORSYour names New &Item... New Item New S&eparator New Submenu Only show in KDE Original Author Previous Maintainer Raffaele Sandrini Restore to System Menu Run in term&inal Save Menu Changes? Show hidden entries Spell Checking Spell checking Options Sub menu to pre-select Submenu name: Terminal &options: Unable to contact khotkeys. Your changes are saved, but they could not be activated. Waldo Bastian You have made changes to the menu.
Do you want to save the changes or discard them? Project-Id-Version: kmenuedit
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2016-04-02 14:52-0300
Last-Translator: André Marcelo Alvarenga <alvarenga@kde.org>
Language-Team: Brazilian Portuguese <kde-i18n-pt_br@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
  [Oculto] &Comentário: &Excluir &Descrição: &Editar &Arquivo &Nome: &Novo submenu... Executar como um &usuário diferente &Ordenar Ordenar &todos por descrição Ordenar &todos por nome Ordenar a &seleção por descrição Ordenar a &seleção por nome Nome do &usuário: C&aminho de trabalho: (C) 2000-2003, Waldo Bastian, Raffaele Sandrini, Matthias Elter Avançado Todos submenus de '%1' serão removidos. Deseja continuar? C&omando: Não foi possível gravar %1 Tecla de &atalho atual: Deseja restaurar o menu do sistema? Aviso: Isto removerá todos os menus personalizados. diniz.bortolotto@gmail.com, rodrigo@conectiva.com.br, lisiane@kdemail.net &Ativar o aviso de execução Seguindo o comando, você pode ver vários marcadores, que serão substituídos pelos valores reais, quando o programa real for executado:
%f - um único nome de arquivo
%F - uma lista de arquivos; usado por aplicativos que podem abrir vários arquivos locais de uma só vez
%u - uma única URL
%U - uma lista de URLs
%d - a pasta do arquivo a ser aberto
%D - uma lista de pastas
%i - o ícone
%m - o mini-ícone
%c - legenda Geral Opções gerais Ocultar entrada Nome do item: Editor de menus do KDE Editor de menus do KDE Barra de ferramentas principal Mantenedor Matthias Elter As mudanças no menu não puderam ser salvas devido ao seguinte problema: Entrada de menu para pré-seleção Montel Laurent Mover para &baixo &Mover para cima Diniz Bortolotto, Rodrigo Stulzer, Lisiane Sztoltz Teixeira Novo &item... Novo item Novo &separador Novo submenu Mostrar apenas no KDE Autor original Mantenedor anterior Raffaele Sandrini Restaurar o menu do sistema E&xecutar no terminal Salvar alterações no menu? Mostrar entradas ocultas Verificação ortográfica Opções da verificação ortográfica Submenu para pré-seleção Nome do submenu: &Opções de terminal: Não foi possível contactar o khotkeys. Suas alterações serão salvas, mas elas não podem ser ativadas. Waldo Bastian Você fez alterações no menu.
Deseja salvar essas alterações ou descartá-las? 