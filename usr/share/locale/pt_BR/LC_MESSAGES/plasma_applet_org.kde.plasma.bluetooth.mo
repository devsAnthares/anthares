��    )      d  ;   �      �     �     �     �     �     �     �     �  	   �     �          %     :     G     _     v     ~  
   �  
   �     �     �     �     �     �     �     �     �               .  U   C  Z   �  O   �  D   D     �     �     �  	   �  	   �     �     �  �  �  	   �     �     �  	   �     �     �     	  	   -	     7	     T	     q	     �	     �	     �	     �	     �	  
   �	     
     
     
     2
     L
     T
     k
     p
     u
     �
     �
     �
  #   �
  #        2  3   R     �     �     �     �     �  
   �     �                                             %                   	         (          &   !          '                          )   #          "                          $          
                            Adapter Add New Device Add New Device... Address Audio Audio device Available devices Bluetooth Bluetooth is Disabled Bluetooth is disabled Bluetooth is offline Browse Files Configure &Bluetooth... Configure Bluetooth... Connect Connected devices Connecting Disconnect Disconnecting Enable Bluetooth File transfer Input Input device Network No No Adapters Available No Devices Found No adapters available No connected devices Notification when the connection failed due to FailedConnection to the device failed Notification when the connection failed due to Failed:HostIsDownThe device is unreachable Notification when the connection failed due to NotReadyThe device is not ready Number of connected devices%1 connected device %1 connected devices Other device Paired Remote Name Send File Send file Trusted Yes Project-Id-Version: plasma_applet_org.kde.plasma.bluetooth
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:01+0100
PO-Revision-Date: 2015-10-31 21:08-0200
Last-Translator: André Marcelo Alvarenga <alvarenga@kde.org>
Language-Team: Brazilian Portuguese <kde-i18n-pt_br@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 1.5
 Adaptador Adicionar novo dispositivo Adicionar novo dispositivo... Endereço Áudio Dispositivo de áudio Dispositivos disponíveis Bluetooth O Bluetooth está desativado O Bluetooth está desativado O Bluetooth está desligado Navegar pelos arquivos Configurar o &Bluetooth... Configurar o Bluetooth... Conectar Dispositivos conectados Conectando Desconectar Desconectando Ativar o Bluetooth Transferência de arquivo Entrada Dispositivo de entrada Rede Não Nenhum adaptador disponível Nenhum dispositivo encontrado Nenhum adaptador disponível Nenhum dispositivo conectado Falha na conexão com o dispositivo O dispositivo não está acessível O dispositivo não está pronto %1 dispositivo conectado %1 dispositivos conectados Outro dispositivo Emparelhado Nome remoto Enviar arquivo Enviar arquivo Confiável Sim 