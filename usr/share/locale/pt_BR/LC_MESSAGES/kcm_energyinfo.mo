��    7      �  I   �      �     �     �     �     �     �     �               $     -     5     H     T      `     �     �     �     �     �     �     �     �                     )     7  	   C  	   M     W     d     j     �     �     �     �     �     �     �     �     �     �  @   �     7     @     \     c     j     r     �     �     �  4   �     �  �  �     �	     �	     �	      �	     �	  
   �	     �	     

  
   
     %
     +
     /
     <
     J
     \
     d
  "   w
     �
     �
     �
     �
     �
     �
               )     ;     K     b  
   o     z     �     �     �     �     �     �  	   �     �     �     �     �  Q   	     [     m  
   �     �     �  !   �     �     �     �     �     �     &   0      7                  #                
          	   '   (   +                       6              3      1                   ,   2                  "       *          $      4   )               %      5                     !          /   .   -       % %1 is value, %2 is unit%1 %2 %1: Application Energy Consumption Battery Capacity Charge Percentage Charge state Charging Current Degree Celsius°C Details: %1 Discharging EMAIL OF TRANSLATORSYour emails Energy Energy Consumption Energy Consumption Statistics Environment Full design Fully charged Has power supply Kai Uwe Broulik Last 12 hours Last 2 hours Last 24 hours Last 48 hours Last 7 days Last full Last hour Manufacturer Model NAME OF TRANSLATORSYour names No Not charging PID: %1 Path: %1 Rechargeable Refresh Serial Number Shorthand for WattsW System Temperature This type of history is currently not available for this device. Timespan Timespan of data to display Vendor VoltV Voltage Wakeups per second: %1 (%2%) WattW Watt-hoursWh Yes current power draw from the battery in WConsumption literal percent sign% Project-Id-Version: kcm_energyinfo
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2016-09-12 09:47-0300
Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>
Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 % %1 %2 %1: Consumo de energia do aplicativo Bateria Capacidade Percentual de carga Estado da carga Carregando Atual °C Detalhes: %1 Descarregando alvarenga@kde.org Energia Consumo de energia Estatística do consumo de energia Ambiente Desenho completo Totalmente carregada Tem fonte de alimentação Kai Uwe Broulik Últimas 12 horas Últimas 2 horas Últimas 24 horas Últimas 48 horas Últimos 7 dias Última carga completa Última hora Fabricante Modelo André Marcelo Alvarenga Não Não está carregando PID: %1 Caminho: %1 Recarregável Atualizar Número de série W Sistema Temperatura No momento, este tipo de histórico não está disponível para este dispositivo. Período de tempo Período de tempo a mostrar Fornecedor V Tensão Ativações por segundo: %1 (%2%) W Wh Sim Consumo % 