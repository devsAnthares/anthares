��          �   %   �      P  �  Q          )  6   9  -   p     �     �     �     �  (   �  d        x     �     �     �     �     �     �                7  "   X     {     �     �  �  �  �  �     �
     �
  7   �
  2   �
     $     1     G     ^  1   t  h   �          $     -     B     K     P     U     Z  	   g  	   q     {     �     �  
   �                               	                                                                      
                        <p>You have chosen to open another desktop session.<br />The current session will be hidden and a new login screen will be displayed.<br />An F-key is assigned to each session; F%1 is usually assigned to the first session, F%2 to the second session and so on. You can switch between sessions by pressing Ctrl, Alt and the appropriate F-key at the same time. Additionally, the KDE Panel and Desktop menus have actions for switching between sessions.</p> Lists all sessions Lock the screen Locks the current sessions and starts the screen saver Logs out, exiting the current desktop session New Session Reboots the computer Restart the computer Shutdown the computer Starts a new session as a different user Switches to the active session for the user :q:, or lists all active sessions if :q: is not provided Turns off the computer User sessionssessions Warning - New Session lock screen commandlock log out log out commandLogout log out commandlogout new session restart computer commandreboot restart computer commandrestart shutdown computer commandshutdown switch user switch user commandswitch switch user commandswitch :q: Project-Id-Version: plasma_runner_sessions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-01-17 09:30-0200
Last-Translator: André Marcelo Alvarenga <alvarenga@kde.org>
Language-Team: Brazilian Portuguese <kde-i18n-pt_br@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 1.5
 <p>Você escolheu abrir outra sessão da área de trabalho.<br />A sessão atual será ocultada e será mostrada uma nova tela de autenticação.<br />É atribuída uma tecla de função para cada sessão, sendo a F%1 normalmente atribuída à primeira sessão, F%2 à segunda e assim por diante. Você pode trocar de sessão pressionando ao mesmo tempo Ctrl, Alt e a tecla de função respectiva. Adicionalmente, o painel do KDE e os menus da área de trabalho têm ações para alternar entre as sessões.</p> Lista todas as sessões Bloquear a tela Bloqueia as sessões atuais e inicia o protetor de tela Sai, fechando a sessão atual da área de trabalho Nova sessão Reinicia o computador Reiniciar o computador Desligar o computador Inicia uma nova sessão com um usuário diferente Alterna para a sessão ativa do usuário :q: ou lista todas as sessões ativas se :q: não for fornecido Desliga o computador sessões Aviso - Nova sessão bloquear sair Sair sair nova sessão reiniciar reiniciar desligar trocar usuário trocar trocar :q: 