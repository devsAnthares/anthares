��          �      ,      �     �     �     �     �     �     �     �     �  .   �     .     L     \     m     �     �  H   �  �  �     �     �     �          1     G     M     c  2   s  %   �     �     �            !     -  g   H        	                                                                  
    &Configure Printers... Active jobs only All jobs Completed jobs only Configure printer General No active jobs No jobs No printers have been configured or discovered One active job %1 active jobs One job %1 jobs Open print queue Print queue is empty Printers Search for a printer... There is one print job in the queue There are %1 print jobs in the queue Project-Id-Version: plasma_applet_org.kde.printmanager
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2015-02-21 09:37+0000
PO-Revision-Date: 2015-02-21 09:27-0200
Last-Translator: André Marcelo Alvarenga <alvarenga@kde.org>
Language-Team: Brazilian Portuguese <kde-i18n-pt_br@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 1.5
 &Configurar impressoras... Apenas os trabalhos ativos Todos os trabalhos Apenas os trabalhos concluídos Configurar impressora Geral Nenhum trabalho ativo Nenhum trabalho Nenhuma impressora foi configurada ou identificada Um trabalho ativo %1 trabalhos ativos Um trabalho %1 trabalhos Abrir a fila de impressão A fila de impressão está vazia Impressoras Procurar uma impressora... Existe um trabalho de impressão na fila de espera Existem %1 trabalhos de impressão na fila de espera 