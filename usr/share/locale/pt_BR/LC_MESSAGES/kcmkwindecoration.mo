��            )   �      �  !   �  "   �  '   �  ,     #   ;  &   _  !   �  &   �  '   �     �                 V   $  1   {     �  *   �     �        
     
   "     -     6     ;     D     T     [     a     g  �  p     5     <  
   C     N     b     i  
   u     �     �     �     �     �     �  `   �  8   5     n  2   �      �     �     �     �  	   	     	  	   	     %	  	   D	     N	     W	     \	                                                                          
                                                    	           @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Borders @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large Application menu Border si&ze: Buttons Close Close by double clicking:
 To open the menu, keep the button pressed until it appears. Close windows by double clicking &the menu button Context help Drag buttons between here and the titlebar Drop here to remove button Get New Decorations... Keep above Keep below Maximize Menu Minimize On all desktops Search Shade Theme Titlebar Project-Id-Version: kcmkwindecoration
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-04-14 02:49+0200
PO-Revision-Date: 2017-10-25 11:33-0300
Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>
Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 Enorme Grande Sem bordas Sem bordas laterais Normal Ainda maior Minúscula Realmente grande Muito grande Menu de aplicativos &Tamanho da borda: Botões Fechar Fechar com um clique duplo:
 Para abrir o menu, mantenha o botão pressionado até ele aparecer. Fechar as janelas com um duplo clique no bo&tão do menu Ajuda de contexto Arraste os botões entre a barra de título e aqui Solte aqui para remover o botão Baixar novas decorações... Manter acima Manter abaixo Maximizar Menu Minimizar Em todas as áreas de trabalho Pesquisar Sombrear Tema Barra de título 