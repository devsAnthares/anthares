��    ?        Y         p  �  q  }  J  M  �        
   7     B     K     j     �  <   �     �     �  	   �     �  .     %   A     g     }     �     �     �     �     �     �     �     �  4        B  9   T     �     �     �  �   �  !   �  t   �     8     D     a     n     s  	   �     �  -   �     �  B   �  &     ?   B  '   �  )   �  7   �  .     5   ;  +   q     �     �     �  ,   �          -  9   :  V   t  C   �  �    �  �  �  �  �  �     ?#  
   S#     ^#  ,   n#  .   �#     �#  B   �#     $     ,$  
   ;$     F$  8   Z$  ,   �$     �$     �$     �$     �$     �$     %     =%     D%     d%  &   j%  6   �%     �%  7   �%     &     %&     8&    >&  (   G'  �   p'     �'  +   (     :(     M(  #   V(     z(     �(  %   �(     �(  ?   �(  3    )  O   4)  5   �)  7   �)  A   �)  0   4*  >   e*  9   �*  %   �*  (   +     -+  =   F+     �+     �+  ;   �+  i   �+     M,     !      	   2   ?   
   /                 +   (   *       1            )   >           8          9   $              7             .       '   6          %   <             3   ,          ;       =   &                              -           #      "          :      5      0                                   4               <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body style=" font-family:'hlv'; font-size:9pt; font-weight:400; font-style:normal;">
<p style="-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><br /></p></body></html> <b>Security notice:</b>
                             According to a security audit by Taylor Hornby (Defuse Security),
                             the current implementation of Encfs is vulnerable or potentially vulnerable
                             to multiple types of attacks.
                             For example, an attacker with read/write access
                             to encrypted data might lower the decryption complexity
                             for subsequently encrypted data without this being noticed by a legitimate user,
                             or might use timing analysis to deduce information.
                             <br /><br />
                             This means that you should not synchronize
                             the encrypted data to a cloud storage service,
                             or use it in other circumstances where the attacker
                             can frequently access the encrypted data.
                             <br /><br />
                             See <a href='http://defuse.ca/audits/encfs.htm'>defuse.ca/audits/encfs.htm</a> for more information. <b>Security notice:</b>
                             CryFS encrypts your files, so you can safely store them anywhere.
                             It works well together with cloud services like Dropbox, iCloud, OneDrive and others.
                             <br /><br />
                             Unlike some other file-system overlay solutions,
                             it does not expose the directory structure,
                             the number of files nor the file sizes
                             through the encrypted data format.
                             <br /><br />
                             One important thing to note is that,
                             while CryFS is considered safe,
                             there is no independent security audit
                             which confirms this. @title:windowCreate a New Vault Activities Backend: Can not create the mount point Can not open an unknown vault. Change Choose the encryption system you want to use for this vault: Choose the used cypher: Close the vault Configure Configure Vault... Configured backend can not be instantiated: %1 Configured backend does not exist: %1 Correct version found Create Create a New Vault... CryFS Device is already open Device is not open Dialog Do not show this notice again EncFS Encrypted data location Failed to create directories, check your permissions Failed to execute Failed to fetch the list of applications using this vault Failed to open: %1 Forcefully close  General If you limit this vault only to certain activities, it will be shown in the applet only when you are in those activities. Furthermore, when you switch to an activity it should not be available in, it will automatically be closed. Limit to the selected activities: Mind that there is no way to recover a forgotten password. If you forget the password, your data is as good as gone. Mount point Mount point is not specified Mount point: Next Open with File Manager Password: Plasma Vault Please enter the password to open this vault: Previous The mount point directory is not empty, refusing to open the vault The specified backend is not available The vault configuration can only be changed while it is closed. The vault is unknown, can not close it. The vault is unknown, can not destroy it. This device is already registered. Can not recreate it. This directory already contains encrypted data Unable to close the vault, an application is using it Unable to close the vault, it is used by %1 Unable to detect the version Unable to perform the operation Unknown device Unknown error, unable to create the backend. Use the default cipher Vaul&t name: Wrong version installed. The required version is %1.%2.%3 You need to select empty directories for the encrypted storage and for the mount point formatting the message for a command, as in encfs: not found%1: %2 Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-11 05:53+0100
PO-Revision-Date: 2017-12-12 15:00-0300
Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>
Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body style=" font-family:'hlv'; font-size:9pt; font-weight:400; font-style:normal;">
<p style="-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><br /></p></body></html> <b>Nota de segurança:</b>
                             De acordo com uma auditoria de segurança feita por Taylor Hornby (Defuse Security),
                             a implementação atual do Encfs é  vulnerável ou potencialmente vulnerável
                             a múltiplos tipos de ataques.
                             Por exemplo, um atacante com acesso de leitura/escrita
                             aos dados criptografados pode diminuir acomplexidade da criptografia
                             para subsequentemente criptografar dados semisto ser notado pelo usuário legítimo,
                             ou pode usar análise de tempo para deduzirinformações.
                             <br /><br />
                             Isto significa que você não deve sincronizar
                             os dados criptografados em um serviço de                              armazenamento na nuvem,
                             ou usá-lo em outras circunstâncias onde o atacante
                             pode acessar frequentemente os dados criptografados.
                             <br /><br />
                             Veja <a href='http://defuse.ca/audits/encfs.htm'>defuse.ca/audits/encfs.htm</a> para mais informações. <b>Nota de segurança:</b>
                             O CryFS criptografa seus arquivos, portanto vocêpode armazená-los com segurança em qualquer lugar.
                             Ele funciona bem junto com serviços em nuvem como Dropbox, iCloud, OneDrive e outros.
                             <br /><br />
                             Ao contrário de outras soluções de sobreposiçãode sistema de arquivos,
                             ele não expõe a estrutura das pastas,
                             o número de arquivos ou o tamanho deles
                             através do formato dos dados criptografados.
                             <br /><br />
                             Uma coisa importante a notar é que enquanto
                             o CryFS é considerado seguro,
                             não há auditorias de segurança independentes
                             que confirmem isto. Criar um novo cofre Atividades Infraestrutura: Não foi possível criar o ponto de montagem Não é possível abrir um cofre desconhecido. Alterar Escolha o sistema de criptografia que deseja usar para este cofre: Escolha a cifra usada: Fechar o cofre Configurar Configurar cofre... A infraestrutura configurada não pôde ser iniciada: %1 A infraestrutura configurada não existe: %1 Versão correta encontrada Criar Criar um novo cofre... CryFS O dispositivo já está aberto O dispositivo não está aberto Janela Não exibir esta nota novamente EncFS Localização dos dados criptografados Falha ao criar diretórios, verifique suas permissões Falha ao executar Falha ao obter a lista de aplicativos usando este cofre Falha ao abrir: %1 Forçar fechamento Geral Se você limitar este cofre apenas para certas atividades, ele será exibido no miniaplicativo apenas quando você estiver nestas atividades. Além disso, quando você mudar para uma atividade que ele não deve estar disponível, ele será automaticamente fechado. Limitar para as atividades selecionadas: Tenha em mente de que não há forma de recuperar uma senha esquecida. Se você esquecer a senha, seus dados não poderão ser recuperados. Ponto de montagem O ponto de montagem não está especificado Ponto de montagem: Próximo Abrir com o gerenciador de arquivos Senha: Cofre do Plasma Digite a senha para abrir este cofre: Anterior O ponto de montagem não está vazio, recusando a abrir o cofre A infraestrutura específica não está disponível A configuração do cofre somente pode ser alterada quando ele estiver fechado. O cofre é desconhecido, não é possível fechá-lo. O cofre é desconhecido, não é possível destruí-lo. O dispositivo já está registrado. Não é possível recriá-lo. Este diretório já contém dados criptografados Não é possível fechar o cofre, um aplicativo o está usando Não é possível fechar o cofre, ele está em uso por %1 Não foi possível detectar a versão Não foi possível executar a operação Dispositivo desconhecido Erro desconhecido, não foi possível criar a infraestrutura. Usar cifra padrão &Nome do cofre: Versão errada instalada. A versão necessária é %1.%2.%3 Você precisa selecionar diretórios vazios para o armazenamento criptografado e para o ponto de montagem %1: %2 