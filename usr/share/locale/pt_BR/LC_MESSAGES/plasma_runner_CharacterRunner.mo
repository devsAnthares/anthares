��    
      l      �       �      �            	               .  I   3     }  	   �  �  �     u     �     �     �  *   �     �  Y   �     :     G               
   	                           &Trigger word: Add Item Alias Alias: Character Runner Config Code Creates Characters from :q: if it is a hexadecimal code or defined alias. Delete Item Hex Code: Project-Id-Version: plasma_runner_CharacterRunner
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2010-07-31 00:36-0300
Last-Translator: André Marcelo Alvarenga <andrealvarenga@gmx.net>
Language-Team: Brazilian Portuguese <kde-i18n-pt_br@mail.kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=(n > 1);
 Palavra de a&tivação: Adicionar item Apelido Apelido: Configuração da Execução de Caracteres Código Cria caracteres a partir do :q:, caso seja um código hexadecimal ou um apelido definido. Excluir item Código hexadecimal: 