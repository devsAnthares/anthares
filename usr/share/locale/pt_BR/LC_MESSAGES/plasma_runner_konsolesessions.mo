��          4      L       `   $   a   /   �   �  �   7   �  0   �                    Finds Konsole sessions matching :q:. Lists all the Konsole sessions in your account. Project-Id-Version: plasma_runner_konsolesessions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2016-01-17 10:23-0200
Last-Translator: André Marcelo Alvarenga <alvarenga@kde.org>
Language-Team: Brazilian Portuguese <kde-i18n-pt_br@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 1.5
 Localiza as sessões do Konsole que correspondam a :q:. Lista todas as sessões do Konsole em sua conta. 