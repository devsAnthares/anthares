��          �      l      �  3   �  $     �   :     �  4   �     �  #        =  �   E  �   �  +   �     �     �     �  1     (   3     \  �   s  $   �  (     �  F  ?     	   U     _     l  D   �     �  4   �     	  �   	  �   �	  1   �
     �
     �
  '     #   F  6   j  "   �     �  0   �  5                                                      	   
                                            1 action for this device %1 actions for this device @info:status Free disk space%1 free Accessing is a less technical word for Mounting; translation should be short and mean 'Currently mounting this device'Accessing... All devices Click to access this device from other applications. Click to eject this disc. Click to safely remove this device. General It is currently <b>not safe</b> to remove this device: applications may be accessing it. Click the eject button to safely remove this device. It is currently <b>not safe</b> to remove this device: applications may be accessing other volumes on this device. Click the eject button on these other volumes to safely remove this device. It is currently safe to remove this device. Most Recent Device No Devices Available Non-removable devices only Open auto mounter kcmConfigure Removable Devices Open popup when new device is plugged in Removable devices only Removing is a less technical word for Unmounting; translation shoud be short and mean 'Currently unmounting this device'Removing... This device is currently accessible. This device is not currently accessible. Project-Id-Version: plasma_applet_org.kde.plasma.devicenotifier
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2017-02-17 19:23-0300
Last-Translator: Aracele Torres <aracele@kde.org>
Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 1 ação para este dispositivo %1 ações para este dispositivo %1 livres Acessando... Todos os dispositivos Clique para acessar este dispositivo a partir de outros aplicativos. Clique para ejetar o disco. Clique para remover este dispositivo com segurança. Geral Neste momento <b>não é seguro</b> remover este dispositivo: os aplicativos ainda podem estar acessando-o. Clique no botão de ejeção para remover este dispositivo com segurança. Neste momento <b>não é seguro</b> remover este dispositivo: os aplicativos ainda podem estar acessando outros volumes dentro dele. Clique no botão de ejeção desses volumes para remover este dispositivo com segurança. Neste momento é seguro remover este dispositivo. Dispositivo mais recente Nenhum dispositivo disponível Apenas os dispositivos não-removíveis Configurar dispositivos removíveis Abrir mensagem quando um novo dispositivo é conectado Apenas os dispositivos removíveis Removendo... Este dispositivo está acessível neste momento. Este dispositivo não está acessível neste momento. 