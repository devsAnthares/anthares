��          t      �                 '     6     R     Z     w  Q   �     �      �       �    $   �          .     L  #   T  -   x     �     �     �  
   �     
      	                                                 &Require trigger word &Trigger word: Checks the spelling of :q:. Correct Could not find a dictionary. Spell Check Settings Spelling checking runner syntax, first word is trigger word, e.g.  "spell".%1:q: Suggested words: %1 separator for a list of words,  spell Project-Id-Version: plasma_runner_spellcheckrunner
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2016-04-09 10:38-0300
Last-Translator: André Marcelo Alvarenga <alvarenga@kde.org>
Language-Team: Brazilian Portuguese <kde-i18n-pt_br@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 Necessá&ria a palavra de ativação Palavra de a&tivação: Verifica a ortografia de :q:. Correto Não foi encontrado um dicionário. Configurações da verificação ortográfica %1:q: Palavras sugeridas: %1 ,  ortografia 