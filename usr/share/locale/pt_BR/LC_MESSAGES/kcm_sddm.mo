��    ,      |  ;   �      �  (   �     �  �   �     �     �     �     �     �     �     �     �     �               %     1      J     k     s     �     �     �     �     �     �     �               /     4     I     Y     l     y     �     �      �  7   �                     1     6  �  <     �     	     		     %	  	   7	     A	     G	     Z	     j	     x	  )   �	     �	     �	     �	     �	     �	     �	     
     
     *
     A
     ^
     o
     �
  $   �
     �
     �
     �
     �
  "   �
          6     R     _     {     �  "   �  ;   �     �  #        %     9  	   B     !                 %   +   ,      "   #                               &                  	                           )                             *      '      $      (                                
    %1 is the name of a session%1 (Wayland) ... @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) @title:windowSelect Image Advanced Author Auto &Login Background: Clear Image Commands Could not decompress archive Cursor Theme: Customize theme Default Description Download New SDDM Themes EMAIL OF TRANSLATORSYour emails General Get New Theme Halt Command: Install From File Install a theme. Invalid theme package Load from file... Login screen using the SDDM Maximum UID: Minimum UID: NAME OF TRANSLATORSYour names Name No preview available Reboot Command: Relogin after quit Remove Theme SDDM KDE Config SDDM theme installer Session: The default cursor theme in SDDM The theme to install, must be an existing archive file. Theme Unable to install theme Uninstall a theme. User User: Project-Id-Version: kcm_sddm
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2017-10-25 12:36-0300
Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>
Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 %1 (Wayland) ... (Tamanhos disponíveis: %1) Selecionar imagem Avançado Autor &Login automático Plano de fundo: Limpar imagem Comandos Não foi possível descomprimir o arquivo Tema de cursores: Personalize o tema Padrão Descrição Baixar novos temas SDDM elchevive@opensuse.org Geral Baixar novo tema Comando para desligar: Instalar a partir do arquivo Instala um tema. Pacote de tema inválido Carregar do arquivo... Tela de autenticação usando o SDDM UID máximo: UID mínimo: Luiz Fernando Ranghetti Nome Nenhuma visualização disponível Comando para reiniciar: Entrar novamente após sair Remover tema Configurações do SDDM KDE Instalador de temas SDDM Sessão: O tema de cursores padrão no SDDM O tema a instalar deve ser um arquivo comprimido existente. Tema Não foi possível instalar o tema. Desinstala um tema. Usuário Usuário: 