��          |      �             !  *   <     g  @   �  �   �  B   Q  R   �  &   �  *     ,   9  4   f  �  �  "   v  =   �  +   �  ;     �   ?  ,   �  7   �  2   7  0   j  6   �     �        	       
                                            Could not eject this disc. Could not mount this device as it is busy. Could not mount this device. One or more files on this device are open within an application. One or more files on this device are opened in application "%2". One or more files on this device are opened in following applications: %2. Remove is less technical for unmountCould not remove this device. Remove is less technical for unmountYou are not authorized to remove this device. This device can now be safely removed. You are not authorized to eject this disc. You are not authorized to mount this device. separator in list of apps blocking device unmount,  Project-Id-Version: plasma_engine_devicenotifications
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-01-26 00:09-0300
Last-Translator: André Marcelo Alvarenga <alvarenga@kde.org>
Language-Team: Brazilian Portuguese <kde-i18n-pt_br@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 Não foi possível ejetar o disco. Não foi possível montar este dispositivo por estar ocupado. Não foi possível montar este dispositivo. Um ou mais arquivos estão abertos dentro de um aplicativo. Um ou mais arquivos deste dispositivo estão abertos no aplicativo "%2". Um ou mais arquivos deste dispositivo estão abertos nos aplicativos: %2. Não foi possível remover este dispositivo. Você não está autorizado a remover este dispositivo. Este dispositivo pode ser removido com segurança. Você não está autorizado a ejetar este disco. Você não está autorizado a montar este dispositivo. ,  