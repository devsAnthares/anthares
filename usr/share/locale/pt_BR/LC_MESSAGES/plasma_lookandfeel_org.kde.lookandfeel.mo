��    1      �  C   ,      8     9     =     B  .   Q  5   �     �     �     �     �  	   �     �               &  1   :     l     r          �  
   �     �  '   �     �     �     �            '        @     O     V     ^  5   g     �     �     �     �     �  $   �  A   �  �   @     &     -  C   >  '   �     �     �     �  �  �     �
     �
     �
     �
     �
     �
               %  
   5     @      _     �     �  A   �     �     �  '        ;     L     [  
   z     �     �     �     �  	   �  3   �     �     
  	          1   %     W  	   l     v     }     �     �     �     �     �     �     �                    '               #   /       -                      $                +         (   1          .   !                        %   0      &       *       	         ,                               
         '                    )            "       %1% Back Battery at %1% Button to change keyboard layoutSwitch layout Button to show/hide virtual keyboardVirtual Keyboard Cancel Caps Lock is on Close Close Search Configure Configure Search Plugins Desktop Session: %1 Different User Keyboard Layout: %1 Logging out in 1 second Logging out in %1 seconds Login Login Failed Login as different user Logout Next track No media playing Nobody logged in on that sessionUnused OK Password Play or Pause media Previous track Reboot Reboot in 1 second Reboot in %1 seconds Recent Queries Remove Restart Shutdown Shutting down in 1 second Shutting down in %1 seconds Start New Session Suspend Switch Switch Session Switch User Textfield placeholder textSearch... Textfield placeholder text, query specific KRunnerSearch '%1'... This is the first text the user sees while starting in the splash screen, should be translated as something short, is a form that can be seen on a product. Plasma is the project name so shouldn't be translated.Plasma made by KDE Unlock Unlocking failed User logged in on console (X display number)on TTY %1 (Display %2) User logged in on console numberTTY %1 Username Username (location)%1 (%2) in category recent queries Project-Id-Version: plasma_lookandfeel_org.kde.lookandfeel
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-09 03:21+0100
PO-Revision-Date: 2017-09-14 16:37-0300
Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>
Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 %1% Voltar Bateria em %1% Trocar o layout Teclado virtual Cancelar Caps Lock está ligado Fechar Fechar pesquisa Configurar Configurar plugins de pesquisa Sessão da área de trabalho: %1 Usuário diferente Layout do teclado: %1 Encerrando sessão em 1 segundo Encerrando sessão em %1 segundos Iniciar sessão Falha na autenticação Iniciar sessão como usuário diferente Encerrar sessão Próxima faixa Nenhuma mídia em reprodução Não usado OK Senha Reproduzir ou pausar mídia Faixa anterior Reiniciar Reiniciando em 1 segundo Reiniciando em %1 segundos Pesquisas recentes Remover Reiniciar Desligar Desligando em 1 segundo Desligando em %1 segundos Iniciar nova sessão Suspender Trocar Trocar sessão Trocar usuário Procurar... Procurar '%1'... Plasma feito pelo KDE Desbloquear Falha no desbloqueio em TTY %1 (Monitor %2) TTY %1 Nome de usuário %1 (%2) na categoria pesquisas recentes 