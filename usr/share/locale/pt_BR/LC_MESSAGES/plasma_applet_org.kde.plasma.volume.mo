��    &      L  5   |      P     Q  8   S     �     �     �     �     �     �  3   �  >        N     i     y     �  m   �     �          "     2     7     ?  *   O      z     �     �  "   �     �     �          3     :     H     X     e     �  ;   �     �  �  �     �     �     �  
   �     �     
	     	     0	  	   G	     Q	     Y	     v	     �	     �	     �	     �	     �	     �	  
   �	  
   �	     �	  ,   
  2   4
     g
     �
     �
     �
     �
  #   �
          	          )     :     B     I     N        #   %                                                              "          $                      
                                           &      !                	        % Accessibility data on volume sliderAdjust volume for %1 Applications Audio Muted Audio Volume Behavior Capture Devices Capture Streams Checkable switch for (un-)muting sound output.Mute Checkable switch to change the current default output.Default Decrease Microphone Volume Decrease Volume Devices General Heading for a list of ports of a device (for example built-in laptop speakers or a plug for headphones)Ports Increase Microphone Volume Increase Volume Maximum volume: Mute Mute %1 Mute Microphone No applications playing or recording audio No output or input devices found Playback Devices Playback Streams Port is unavailable (unavailable) Port is unplugged (unplugged) Raise maximum volume Show additional options for %1 Volume Volume at %1% Volume feedback Volume step: label of device items%1 (%2) label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: plasma_applet_org.kde.plasma.volume
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2017-10-25 12:52-0300
Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>
Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 % Ajustar volume para %1 Aplicativos Sem áudio Volume do áudio Comportamento Dispositivos de captura Sequências de captura Silenciar Padrão Diminuir volume do microfone Diminuir volume Dispositivos Geral Portas Aumentar volume do microfone Aumentar volume Volume máximo: Sem áudio Mudo em %1 Silenciar microfone Nenhum aplicativo tocando ou gravando áudio Nenhum dispositivo de entrada ou saída encontrado Dispositivos de reprodução Sequências de reprodução  (não disponível)  (desconectado) Aumentar o volume máximo Mostrar opções adicionais para %1 Volume Volume em %1% Retorno do volume Passo do volume: %1 (%2) %1: %2 100% %1% 