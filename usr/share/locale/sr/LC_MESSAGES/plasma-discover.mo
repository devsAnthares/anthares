��    b      ,  �   <      H     I     _     b     j     w  5   �  #   �  E   �  E   '	  >   m	     �	     �	  7   �	     
     '
     8
     W
     v
     �
     �
     �
     �
     �
     �
     �
     �
          #     (  	   /     9     Q     b  !   t  F   �     �     �       <        Z     b  *   k     �      �     �  	   �     �     �  	   �     �     	                :  
   X     c     �     �     �  
   �  F   �  :   �     7     ?     G     N     a     h  8   w     �     �  	   �  
   �     �     �     �               #     ;     C     a     r     {     �     �  &   �     �  
   �     �     
     *     2     ;     L     a  $   r  N  �  U   �     <     ?     G     g  �   x  G   M  P   �  P   �  I   7      �     �  R   �  $   	     .  (   D  *   m     �     �     �     �     �  "   �  %        ?  #   [       
   �     �     �  $   �  *   �  (   !  ?   J  j   �  ;   �  $   1     V  Z   v     �     �  B   �     1  5   G     }     �     �     �     �     �            8   /  4   h     �  6   �  '   �          -     9  �   O  `   �     7     F     R     _     |     �  g   �       ?   &     f     t     �     �     �  )   �     �  !        $  *   0     [     r  /   �     �     �     �  .   �        !         A      S      `   #   n   !   �      �   4   �      K      >   P             M              -      +   H       ?   1      ^   J       Q         [   ;   3   5          6                              &       (           _                 S          G      *   L   Z       X   2      C               V   :   B       #   b   N   `       =       @              9   D   R   F      E       0   ]          a   7   %   A              8          
   <   W   	          I       $   .       )   '   O      U       \   "   4      Y   T   /          ,   !    
Also available in %1 %1 %1 (%2) %1 (Default) <b>%1</b> by %2 <em>%1 out of %2 people found this review useful</em> <em>Tell us about this review!</em> <em>Useful? <a href='true'><b>Yes</b></a>/<a href='false'>No</a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'><b>No</b></a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'>No</a></em> @infoFetching updates @infoFetching... @infoIt is unknown when the last check for updates was @infoLooking for updates @infoNo updates @infoNo updates are available @infoShould check for updates @infoThe system is up to date @infoUpdates @infoUpdating... Accept Addons Aleix Pol Gonzalez An application explorer Apply Changes Available backends:
 Available modes:
 Back Cancel Category: Checking for updates... Comment too long Comment too short Compact Mode (auto/compact/full). Could not close the application, there are tasks that need to be done. Could not find category '%1' Couldn't open %1 Delete the origin Directly open the specified application by its package name. Discard Discover Display a list of entries with a category. Extensions... Failed to remove the source '%1' Help... Homepage: Improve summary Install Installed Jonathan Thomas Launch License: List all the available backends. List all the available modes. Loading... Local package file to install Make default More Information... More... No Updates Open Discover in a said mode. Modes correspond to the toolbar buttons. Open with a program that can deal with the given mimetype. Proceed Rating: Remove Resources for '%1' Review Reviewing '%1' Running as <em>root</em> is discouraged and unnecessary. Search Search in '%1'... Search... Search: %1 Search: %1 + %2 Settings Short summary... Show reviews (%1)... Size: Sorry, nothing found... Source: Specify the new source for %1 Still looking... Summary: Supports appstream: url scheme Tasks Tasks (%1%) TransactioName TransactionStatus%1 %2 Unable to find resource: %1 Update All Update Selected Update section nameUpdate (%1) Updates Version: unknown reviewer updates not selected updates selected © 2010-2016 Plasma Development Team Project-Id-Version: plasma-discover
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-09 06:04+0100
PO-Revision-Date: 2018-01-06 09:21+0100
Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>
Language-Team: Serbian <kde-i18n-sr@kde.org>
Language: sr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
X-Associated-UI-Catalogs: libdiscover
 
Такође доступно у %1|/|
Такође доступно у $[лок %1] %1 %1 (%2) %1 (подразумевано) <b>%1</b> — %2 <em>%1 од %2 особа сматра ову рецензију корисном</em>|/|<em>%1 од %2 $[множ ^2 особе особе особа] сматра ову рецензију корисном</em> <em>Реците нам нешто о овој рецензији!</em> <em>Корисно? <a href='true'><b>Да</b></a>/<a href='false'>Не</a></em> <em>Корисно? <a href='true'>Да</a>/<a href='false'><b>Не</b></a></em> <em>Корисно? <a href='true'>Да</a>/<a href='false'>Не</a></em> Добављам допуне... Добављам... Не зна се кад је била последња провера допуна Проверавам допуне... Нема допуна Нема доступних допуна Треба проверити допуне Систем је ажуран Допуне Ажурирам... Прихвати Додаци Алекс Пол Гонзалез Истраживач програма Примени измене Доступне позадине:
 Доступни режими:
 Назад Одустани Категорија: Проверавам допуне... Коментар је предугачак Коментар је прекратак Сажети режим (једно од: auto, compact, full). Не могу да затворим програм, има још недовршених задатака. Не могу да нађем категорију „%1“ Не могу да отворим %1 Обриши извориште Непосредно отвори задати програм по имену пакета Одбаци Oткривач Прикажи списак уноса са категоријом Проширења... Не могу да уклоним извор „%1“ Помоћ... Домаћа страница: Побољшај сажетак Инсталирај Инсталирано Џонатан Томас Покрени Лиценца: Наброји све доступне позадине. Наброји све доступне режиме. Учитавам... Локални пакет за инсталирање. Учини подразумеваним Више података... Више... Нема допуна Отвори Oткривач у датом режиму. Режими одговарају дугмадима траке алатки. Отвори програмом који уме да рукује датим МИМЕ типом Настави Оцена: Уклони Ресурси за „%1“ Рецензирај Рецензија „%1“ Извршавање под кореном није препоручљиво нити потребно. Претрага Тражи у „%1“...|/|Тражи у „$[лок %1]“... Тражи... Претрага: %1 Претрага: %1 + %2 Поставке Сажетак... Прикажи рецензије (%1)... Величина: Ништа није нађено. Извор: Задајте нови извор за %1 Још тражим... Сажетак: Подршка за УРЛ шему appstream:. Задаци Задаци (%1%) %1 %2 Не могу да нађем ресурс: %1 Ажурирај све Ажурирај изабрано Допуна (%1) Допуне Издање: непознат рецензент Прескочене допуне Изабране допуне © 2010–2016, развојни тим Плазме 