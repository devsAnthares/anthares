��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  "  K     n  ;   w  z  �  5  .
  �   d  #     )   B      l  A   �  ,   �  0   �  ;   -  4   i                                          
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2017-09-28 17:58+0200
Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>
Language-Team: Serbian <kde-i18n-sr@kde.org>
Language: sr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
  сек. &Прековреме одзива на покретању: <qt><h1>Обавештење у траци задатака</h1>Можете одредити и додатни начин за обавештавање о покретању, где се у траци задатака јавља дугме са ротирајућим пешчаником, симболично указујући на учитавање програма. Неки програми могу не бити свесни оваквог обавештавања, и тада ће дугме нестати после времена датог под <i>Прековреме одзива на покретању:</i>.</qt> <qt><h1>Заузет показивач</h1>КДЕ нуди заузет показивач за обавештавање о покретању програма. Да бисте га укључили, изаберите један од показивача са списка. Неки програми могу не бити свесни оваквог обавештавања, и тада ће показивач престати да трепће после времена датог под <i>Прековреме одзива на покретању:</i>.</qt> <h1>Одзив при покретању</h1>Овде подешавате одзив на основу кога уочавате да је програм управо покренут. Трепћући показивач Скакутајући показивач &Заузет показивач Укључи обавештење у &траци задатака Без заузетог показивача Пасивни заузети показивач Преко&време одзива на покретању: &Обавештење у траци задатака 