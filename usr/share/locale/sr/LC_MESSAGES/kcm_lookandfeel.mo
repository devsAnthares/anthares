��          �      |      �     �  2        B     b       #   �      �     �  %   �       
   &     1     >     ]  �   }       ]   (     �  p   �  :        P  (  \  :   �  m   �  >   .      m  6   �  I   �     	  '   #	  M   K	  .   �	     �	     �	     �	  9   
    E
     G  �   V  1     �   I  r        ~                                                                      
                	           Apply a look and feel package Command line tool to apply look and feel packages. Configure Look and Feel details Copyright 2017, Marco Martin Cursor Settings Changed Download New Look And Feel Packages EMAIL OF TRANSLATORSYour emails Get New Looks... List available Look and feel packages Look and feel tool Maintainer Marco Martin NAME OF TRANSLATORSYour names Reset the Plasma Desktop layout Select an overall theme for your workspace (including plasma theme, color scheme, mouse cursor, window and desktop switcher, splash screen, lock screen etc.) Show Preview This module lets you configure the look of the whole workspace with some ready to go presets. Use Desktop Layout from theme Warning: your Plasma Desktop layout will be lost and reset to the default layout provided by the selected theme. You have to restart KDE for cursor changes to take effect. packagename Project-Id-Version: kcm_lookandfeel
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-27 05:46+0100
PO-Revision-Date: 2017-12-24 21:47+0100
Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>
Language-Team: Serbian <kde-i18n-sr@kde.org>
Language: sr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
 Примени пакет изгледа и осећаја Алатка командне линије за примену пакета изгледа и осећаја. Подесите детаље изгледа и осећаја © 2017, Марко Мартин Измењене поставке показивача Преузмите нове пакете изгледа и осећаја caslav.ilic@gmx.net Добави нове изгледе... Наброји доступне пакете изгледа и осећаја Алатка за изглед и осећај Одржавалац Марко Мартин Часлав Илић Ресетуј распоред плазма површи Изаберите тему за радни простор (укључујући тему Плазме, шему боја, показивач миша, мењач прозора и површи, уводни екран, забравни екран, итд.) Преглед Овај модул вам омогућава подешавање изгледа целог радног простора, уз неколико спремних претподешавања. Распоред површи према теми Упозорење: ваш распоред плазма површи биће изгубљен и ресетован на подразумевани распоред изабране теме. Поново покрените КДЕ да би измене показивача ступиле на снагу. име‑пакета 