��    K      t  e   �      `     a  ^   f  H   �          !     -     D     L  '   [     �  "   �     �     �     �  
   �     �          %  	   .     8     P     f     l          �  "   �     �     �  	   �     �     �  ?   	     D	     Q	     W	     e	     q	     �	     �	  ;   �	  &   �	      
  %   %
     K
     e
     
     �
  >   �
     �
       '   &  !   N     p     �     �     �     �     �     �     �          "     3     E     X     k     }     �     �     �     �     �     �  3     =  G     �     �     �     �     �     �     �     �  +   �     (     E     S     b     y     �  #   �     �     �     �                 .     7     P     f  =   l     �     �     �     �     �     �               .     M     a     z     �     �     �     �     �  #   �          !     3  #   A     e     }     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �                                    %     7     U     '   I   8                            >   5   B           .   6   !      +       -   4       2         "       C         %       9       0   7   @      )         
   H       :         G              F   *      /   =               &                            D   #      A         K   ,       1      (   $          ?   J       <      3      	      ;       E                   min %1 is the weather condition, %2 is the temperature,  both come from the weather provider%1 %2 A weather station location and the weather service it comes from%1 (%2) Beaufort scale bft Celsius °C Degree, unit symbol° Details Fahrenheit °F Forecast period timeframe1 Day %1 Days Hectopascals hPa High & Low temperatureH: %1 L: %2 High temperatureHigh: %1 Inches of Mercury inHg Kelvin K Kilometers Kilometers per Hour km/h Kilopascals kPa Knots kt Location: Low temperatureLow: %1 Meters per Second m/s Miles Miles per Hour mph Millibars mbar N/A No weather stations found for '%1' Notices Percent, measure unit% Pressure: Search Short for no data available- Shown when you have not set a weather providerPlease Configure Temperature: Units Update every: Visibility: Weather Station Wind conditionCalm Wind speed: certain weather condition (probability percentage)%1 (%2%) content of water in airHumidity: %1%2 distance, unitVisibility: %1 %2 ground temperature, unitDewpoint: %1 humidex, unitHumidex: %1 pressure tendencyfalling pressure tendencyrising pressure tendencysteady pressure tendency, rising/falling/steadyPressure Tendency: %1 pressure, unitPressure: %1 %2 temperature, unit%1%2 visibility from distanceVisibility: %1 weather warningsWarnings Issued: weather watchesWatches Issued: wind directionE wind directionENE wind directionESE wind directionN wind directionNE wind directionNNE wind directionNNW wind directionNW wind directionS wind directionSE wind directionSSE wind directionSSW wind directionSW wind directionVR wind directionW wind directionWNW wind directionWSW wind direction, speed%1 %2 %3 wind speedCalm windchill, unitWindchill: %1 winds exceeding wind speed brieflyWind Gust: %1 %2 Project-Id-Version: plasma_applet_org.kde.plasma.weather
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-28 05:50+0100
PO-Revision-Date: 2016-07-03 22:52+0200
Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>
Language-Team: Serbian <kde-i18n-sr@kde.org>
Language: sr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
  мин. %1 %2 %1 (%2) Бофорова скала bft целзијуси °C ° Детаљи фаренхајти °F %1 дан %1 дана %1 дана %1 дан хектопаскали hPa г: %1 д: %2 горња: %1 инчи живе inHg келвини K километри километри на час km/h килопаскали kPa чворови kt Локација: доња: %1 метри у секунди m/s миље миље на час mph милибари mbar нд. Метеостаница за „%1“ није нађена. Обавештења % Притисак: Претражи - Морате подесити. Температура: Јединице Ажурирај сваких: Видљивост: Метеостаница без ветра Брзина ветра: %1 (%2%) влажност: %1 %2 видљивост: %1 %2 рошење: %1 темп. на влажност: %1 у паду у порасту држи се кретање притиска: %1 притисак: %1 %2 %1 %2 видљивост: %1 Упозорења: Праћења: И ИСИ ИЈИ С СИ ССИ ССЗ СЗ Ј ЈИ ЈЈИ ЈЈЗ ЈЗ пр. З ЗСЗ ЗЈЗ %1 %2 %3 без ветра темп. на ветар: %1 налети ветра: %1 %2 