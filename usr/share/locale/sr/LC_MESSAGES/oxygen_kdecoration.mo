��    ?        Y         p     q  !   �  "   �  &   �  ,   �  #   &  &   J  !   q  &   �  '   �  "   �  #     "   )  '   L     t     �  +   �  2   �  
   �     �               ,     3     G  U   O  7   �     �     �     		     	      	     6	     T	     c	     k	  !   �	     �	  	   �	     �	     �	     �	     �	  &   
     /
     N
     U
     p
     v
     ~
     �
  4   �
  $   �
     �
               /     G     ]     w  &   �     �  +  �  :   �     ,     ;     H     Z     w     �     �     �     �     �     �     �     �  ,     
   B  P   M  r   �       !   $     F  >   c     �  &   �     �  �   �  f   �  !   �  ,        @  
   O  K   Z  5   �     �  
   �  1   �  1   1     c     l     �  A   �  4   �       ;   5  8   q     �  3   �  
   �  
   �  #        %  S   4  6   �  "   �  :   �        *   >  +   i  *   �     �  Q   �  9   .     $                               1          "   *   )          :             	         ;   !                          9   =   5   /   3                    (         #   4       8       -   6   
   7          %                                         ?       0   &      +          '         .       ,      >       <   2        &Matching window property:  @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Border @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large @item:inlistbox Button size:Large @item:inlistbox Button size:Normal @item:inlistbox Button size:Small @item:inlistbox Button size:Very Large Active Window Glow Add Add handle to resize windows with no border Allow resizing maximized windows from window edges Animations B&utton size: Border size: Button mouseover transition Center Center (Full Width) Class:  Configure fading between window shadow and glow when window's active state is changed Configure window buttons' mouseover highlight animation Decoration Options Detect Window Properties Dialog Edit Edit Exception - Oxygen Settings Enable/disable this exception Exception Type General Hide window title bar Information about Selected Window Left Move Down Move Up New Exception - Oxygen Settings Question - Oxygen Settings Regular Expression Regular Expression syntax is incorrect Regular expression &to match:  Remove Remove selected exception? Right Shadows Tit&le alignment: Title:  Use the same colors for title bar and window content Use window class (whole application) Use window title Warning - Oxygen Settings Window Class Name Window Drop-Down Shadow Window Identification Window Property Selection Window Title Window active state change transitions Window-Specific Overrides Project-Id-Version: oxygen_kdecoration
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-13 05:20+0100
PO-Revision-Date: 2017-12-17 18:00+0100
Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>
Language-Team: Serbian <kde-i18n-sr@kde.org>
Language: sr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
 &Својство прозора за поклапање:  огромне велике без ивице без бочне ивице нормалне неизмерне сићушне врло огромне врло велике велика нормална мала врло велика Сијање активног прозора Додај Ручка за промену величине прозора без ивица Максимизованим прозорима може да се мења величина преко ивица Анимације Величина &дугмади: Величина ивица: Промена дугмета на прелазак мишем средина средина (пуне ширине) Класа:  Подесите претапање између сенке и сијања прозора када се промени његово стање активности Подесите анимацију истицања дугмади при преласку мишем Опције декорације Откриј својства прозора Дијалог Уреди Уређивање изузетка — поставке Кисеоника Укључи/искључи овај изузетак тип изузетка Опште Без насловне траке прозора Подаци о изабраном прозору лево Помери надоле Помери нагоре Нови изузетак — поставке Кисеоника Питање — поставке Кисеоника регуларни израз Лоша синтакса регуларног израза &Регуларни израз за поклапање:  Уклони Уклонити изабрани изузетак? десно Сенке &Поравнање наслова: Наслов:  Иста боја за насловну траку и садржај прозора По класи прозора (цео програм) По наслову прозора Упозорење — поставке Кисеоника име класе прозора Падајућа сенка прозора Идентификација прозора Избор својства прозора наслов прозора Прелаз при промени стања активности прозора Потискивања посебна по прозору 