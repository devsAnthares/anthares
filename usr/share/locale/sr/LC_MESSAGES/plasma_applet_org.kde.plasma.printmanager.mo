��          �      ,      �     �     �     �     �     �     �     �     �  .   �     .     L     \     m     �     �  H   �  B  �  !   /  &   Q     x  (   �     �  
   �  (   �       P     y   p  ;   �  )   &  )   P     z     �  �   �        	                                                                  
    &Configure Printers... Active jobs only All jobs Completed jobs only Configure printer General No active jobs No jobs No printers have been configured or discovered One active job %1 active jobs One job %1 jobs Open print queue Print queue is empty Printers Search for a printer... There is one print job in the queue There are %1 print jobs in the queue Project-Id-Version: plasma_applet_org.kde.plasma.printmanager
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2015-02-21 09:37+0000
PO-Revision-Date: 2015-03-01 12:45+0100
Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>
Language-Team: Serbian <kde-i18n-sr@kde.org>
Language: sr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
 &Подеси штампаче... само активни послови сви послови само завршени послови Подеси штампач Опште Нема активних послова Нема послова Ниједан штампач није подешен нити откривен. %1 активан посао %1 активна посла %1 активних послова %1 активан посао %1 посао %1 посла %1 послова %1 посао Отвори ред за штампање Ред за штампање празан Штампачи Нађи штампач... %1 посао у реду за штампање. %1 посла у реду за штампање. %1 послова у реду за штампање. Један посао у реду за штампање. 