��    O      �  k         �     �     �     �     �     �     �     �  .   �  U   (     ~     �      �     �  /   �  /        5     A     J  
   V     a     q  $   �     �     �     �     �     �     �  	   	     	  
   	     )	     <	     O	     g	  	   m	     w	  !   �	     �	     �	  	   �	      �	     �	     �	     
     $
     5
     :
     G
     M
     _
     w
  (   �
     �
  =   �
            "   5     X     s  J   {  1   �  )   �  (   "  '   K  "   s  4   �     �     �     �     �     �          "  U   8  N   �  9   �  J     A  b     �     �  #   �     �     �          $  
   4  S   ?  -   �     �  >   �       _   +  _   �     �     �          '      9  -   Z  =   �     �     �     �  2     
   6  "   A     d     x      �  #   �  #   �  0   �     0     =  
   S  >   ^     �     �     �  >   �     (  .   <  '   k  !   �     �     �     �  $   �  &        @  L   \  1   �     �     �     �          5      D     e  S   r  <   �  :     :   >  <   y  U   �               &  (   4  
   ]     h  
   y     �  #   �  9   �  5   �     %   N         @   A       >   +   "       ,       F   /   B   O           9      ;      $                      G          3          K         ?                        2   L   *      !      H   =                   <              '               :       -            .   I   J   6   C           M       1          #   0      4       7       (   	   
           8               D   5   )   &   E    &All Desktops &Close &Fullscreen &Move &New Desktop &Pin &Shade 1 = number of desktop, 2 = desktop name&%1 %2 Activities a window is currently on (apart from the current one)Also available on %1 Add To Current Activity All Activities Allow this program to be grouped Alphabetically Always arrange tasks in columns of as many rows Always arrange tasks in rows of as many columns Arrangement Behavior By Activity By Desktop By Program Name Close Window or Group Cycle through tasks with mouse wheel Do Not Group Do Not Sort Filters Forget Recent Documents General Grouping and Sorting Grouping: Highlight windows Icon size: Keep &Above Others Keep &Below Others Keep launchers separate Large Ma&ximize Manually Mark applications that play audio Maximum columns: Maximum rows: Mi&nimize Minimize/Restore Window or Group More Actions Move &To Current Desktop Move To &Activity Move To &Desktop Mute New Instance On %1 On All Activities On The Current Activity On middle-click: Only group when the task manager is full Open groups in popups Open or bring to the front window of media player appRestore Pause playbackPause Play next trackNext Track Play previous trackPrevious Track Quit media player appQuit Re&size Remove launcher button for application shown while it is not runningUnpin Show all user Places%1 more Place %1 more Places Show only tasks from the current activity Show only tasks from the current desktop Show only tasks from the current screen Show only tasks that are minimized Show progress and status information in task buttons Show tooltips Small Sorting: Start New Instance Start playbackPlay Stop playbackStop The click actionNone Toggle action for showing a launcher button while the application is not running&Pin When clicking it would toggle grouping windows of a specific appGroup/Ungroup Which activities a window is currently onAvailable on %1 Which virtual desktop a window is currently onAvailable on all activities Project-Id-Version: plasma_applet_org.kde.plasma.taskmanager
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2017-04-07 00:51+0200
Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>
Language-Team: Serbian <kde-i18n-sr@kde.org>
Language: sr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
 &све површи &Затвори Преко &целог екрана &Премести &нова површ &Прикачи &Намотај &%1 — %2 Такође доступан у %1|/|Такође доступан у $[лок %1] Додај у текућу активност Све активности Дозволи груписање за овај програм азбучно Увек распореди задатке по колонама са оволико врста Увек распореди задатке по врстама са оволико колона Распоред Понашање по активности по површи по имену програма затвори прозор или групу Кружи кроз задатке на точкић миша без груписања без ређања Филтери Заборави недавне документе Опште Груписање и ређање Груписање: Истакни прозоре Величина иконица: Држи из&над осталих Држи ис&под осталих Држи покретаче раздвојено велике Ма&ксимизуј ручно Означи програме који пуштају звук Највише колона: Највише врста: Ми&нимизуј минимизуј/обнови прозор или групу Више радњи Премести на &текућу површ Премести у &активност Премести на &површ Утишај нови примерак У %1|/|У $[лок %1] У свим активностима У текућој активности На средњи клик: Групиши само када је менаџер задатака пун Отварај групе у искакачима Обнови Паузирај Следећа нумера Претходна нумера Напусти Промени &величину Откачи Још %1 место Још %1 места Још %1 места Још %1 место Само задаци из текуће активности Само задаци на тренутној површи Само задаци на тренутном екрану Само задаци који су минимизовани Подаци о напретку и стању у дугмадима задатака Облачићи мале Ређање: Покрени нови примерак Пусти Заустави ништа &Прикачи групиши/разгрупиши Доступан у %1|/|Доступан у $[лок %1] Доступан у свим активностима 