��          �   %   �      @     A  !   Y      {     �      �     �     �  +        =     N     [  (   z     �  ,   �  7   �     !  7   :     r  ]   �  1   �  	   "     ,  "   ?  5   b  %  �  .   �  8   �  ?   &  ;   f     �  C   �  /   �  %   *	     P	     o	     �	  L   �	  2   �	  M   
  f   k
      �
  Q   �
  1   E  �   w  Q   /     �  "   �  9   �  /   �                    
         	                                                                                                 Cannot start initramfs. Cannot start update-alternatives. Configure Plymouth Splash Screen Download New Splash Screens EMAIL OF TRANSLATORSYour emails Get New Boot Splash Screens... Initramfs failed to run. Initramfs returned with error condition %1. Install a theme. Marco Martin NAME OF TRANSLATORSYour names No theme specified in helper parameters. Plymouth theme installer Select a global splash screen for the system The theme to install, must be an existing archive file. Theme %1 does not exist. Theme corrupted: .plymouth file not found inside theme. Theme folder %1 does not exist. This module lets you configure the look of the whole workspace with some ready to go presets. Unable to authenticate/execute the action: %1, %2 Uninstall Uninstall a theme. update-alternatives failed to run. update-alternatives returned with error condition %1. Project-Id-Version: kcm_plymouth
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-28 03:05+0200
PO-Revision-Date: 2017-05-07 21:01+0200
Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>
Language-Team: Serbian <kde-i18n-sr@kde.org>
Language: sr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
 Не могу да покренем initramfs. Не могу да покренем update-alternatives. Подешавање уводног екрана Плимута Преузимање нових уводних екрана caslav.ilic@gmx.net Добави нове уводне екране подизања... initramfs не може да се изврши. initramfs враћа грешку %1. Инсталирај тему. Марко Мартин Часлав Илић Није задата тема у параметрима помоћника. Инсталатор Плимутових тема Изаберите глобални уводни екран за систем Тема за инсталирање, мора да буде постојећи фајл архиве. Тема %1 не постоји. Тема искварена: нема .plymouth фајла унутар теме. Фасцикла теме %1 не постоји. У овом модулу можете да подесите изглед целог радног простора, уз неколико спремних предефинисаних. Не могу да аутентификујем/извршим радњу: %1, %2 Деинсталирај Деинсталирај тему. update-alternatives не може да се изврши. update-alternatives враћа грешку %1. 