��    B      ,  Y   <      �  i   �  m        y  b   �     �     	  6        O  7   _     �     �  	   �     �  (   �  )   �  )   (     R     o  Y   u     �     �  �   �      y	  .   �	     �	  
   �	     �	     �	     
     
     !
     5
     B
     J
     c
     r
     w
     �
     �
     �
     �
     �
  	   �
  
   �
     �
     �
  	     
     
   *     5     B  	   `     j     o  
   �  �   �     (  8   8  �   q     �  8   	  ,   B  ,   o  %   �     �  $  �  �     �   �  2   H  �   {  0   (     Y  ]   s     �  ]   �     I     Z     p      �  U   �  U   �  U   N  D   �     �  �   �  #   �     �  �   �     �  b   �     /     O  '   e     �  )   �     �  !   �             0        P     h     q  �   �                /     <     Z     n     �     �     �     �     �     �  =        V  
   l  .   w     �  �   �     �  v   �  �        �  ]     K   s  K   �  J     1   V     $   !   %          ;   ,          B                 8   #                 7   9       +              )   ?   -   <         6                                    *   2       >                1       @       .   5   3   :   A         &      /                4              0      "             =   (   '         	   
           @info User changed Phonon backendTo apply the backend change you will have to log out and back in again. A list of Phonon Backends found on your system.  The order here determines the order Phonon will use them in. Apply Device List To... Apply the currently shown device preference list to the following other audio playback categories: Audio Hardware Setup Audio Playback Audio Playback Device Preference for the '%1' Category Audio Recording Audio Recording Device Preference for the '%1' Category Backend Colin Guthrie Connector Copyright 2006 Matthias Kretz Default Audio Playback Device Preference Default Audio Recording Device Preference Default Video Recording Device Preference Default/Unspecified Category Defer Defines the default ordering of devices which can be overridden by individual categories. Device Configuration Device Preference Devices found on your system, suitable for the selected category.  Choose the device that you wish to be used by the applications. EMAIL OF TRANSLATORSYour emails Failed to set the selected audio output device Front Center Front Left Front Left of Center Front Right Front Right of Center Hardware Independent Devices Input Levels Invalid KDE Audio Hardware Setup Matthias Kretz Mono NAME OF TRANSLATORSYour names Phonon Configuration Module Playback (%1) Prefer Profile Rear Center Rear Left Rear Right Recording (%1) Show advanced devices Side Left Side Right Sound Card Sound Device Speaker Placement and Testing Subwoofer Test Test the selected device Testing %1 The order determines the preference of the devices. If for some reason the first device cannot be used Phonon will try to use the second, and so on. Unknown Channel Use the currently shown device list for more categories. Various categories of media use cases.  For each category, you may choose what device you prefer to be used by the Phonon applications. Video Recording Video Recording Device Preference for the '%1' Category  Your backend may not support audio recording Your backend may not support video recording no preference for the selected device prefer the selected device Project-Id-Version: kcm5_phonon
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2017-09-28 17:58+0200
Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>
Language-Team: Serbian <kde-i18n-sr@kde.org>
Language: sr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
 Да би измена позадине ступила на снагу, мораћете да се одјавите и пријавите. Списак позадина знаних Фонону на систему. Фонон ће покушати да их употреби овде наведеним редоследом. Примени списак уређаја на... Примени тренутно приказан списак пожељних уређаја на следеће друге категорије аудио пуштања: Подешавање аудио хардвера Аудио пуштање Пожељни уређај за аудио пуштање за категорију „%1“ Аудио снимање Пожељни уређај за аудио снимање за категорију „%1“ Позадина Колин Гатри Конектор © 2006, Матијас Крец Пожељни подразумевани уређај за аудио пуштање Пожељни подразумевани уређај за аудио снимање Пожељни подразумевани уређај за видео снимање подразумевана/неодређена категорија Нежељен Одређује подразумевани редослед уређаја, који се може потиснути по појединим категоријама. Подешавање уређаја Пожељни уређаји Уређаји нађени на систему који су подесни за изабрану категорију. Изаберите уређај који желите да користе програми. caslav.ilic@gmx.net Неуспело постављање изабраног уређаја за аудио излаз Предњи централни Предњи леви Предњи лево од центра Предњи десни Предњи десно од центра Хардвер Независни уређаји Нивои улаза Лоше Подешавање аудио хардвера Матијас Крец Моно Часлав Илић Модул за подешавање Фонона|/|$[својства дат 'Модулу за подешавање Фонона'] Пуштање (%1) Пожељан Профил Задњи централни Задњи леви Задњи десни Снимање (%1) Напредни уређаји Бочни леви Бочни десни Звучна картица Звучни уређај Размештај звучника и испробавање Бас звучник Проба Испробај изабрани уређај Испробавам %1 Редослед одређује пожељност уређаја. Ако из неког разлог Фонон не може да употреби први, покушаће са другим, итд. Непознат канал Примените тренутно приказани списак уређаја на више категорија. Разне категорије употребе медијума. За сваку од њих можете одабрати уређај који желите да користе програми над Фононом. Видео снимање Пожељни уређај за видео снимање за категорију „%1“ Позадина можда не подржава аудио снимање Позадина можда не подржава видео снимање изабрани уређај нема одређену пожељност изабрани уређај је пожељан 