��            )   �      �     �     �     �     �     �     �     �  
           )   (     R     V     \     c     v     �     �     �  3   �     �       <   #  5   `  8   �  8   �                    /  @  1     r     �     �  "   �     �           '     H  
   a  V   l     �     �     �  )   �  $   $	  $   I	     n	     �	  H   �	  +   �	  /   
  v   F
  \   �
  [     [   v     �  %   �  #        /                                                  
                                                                               	        Add files... Add folder... Background frame Change picture every Choose a folder Choose files Configure plasmoid... Fill mode: General Left click image opens in external viewer Pad Paths Paths: Pause on mouseover Preserve aspect crop Preserve aspect fit Randomize items Stretch The image is duplicated horizontally and vertically The image is not transformed The image is scaled to fit The image is scaled uniformly to fill, cropping if necessary The image is scaled uniformly to fit without cropping The image is stretched horizontally and tiled vertically The image is stretched vertically and tiled horizontally Tile Tile horizontally Tile vertically s Project-Id-Version: plasma_applet_org.kde.plasma.mediaframe
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-16 03:31+0100
PO-Revision-Date: 2017-12-03 22:53+0100
Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>
Language-Team: Serbian <kde-i18n-sr@kde.org>
Language: sr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
 Додај фајлове... Додај фасциклу... Позадински оквир Мењај слику сваких Избор фасцикле Избор фајлова Подеси плазмоид... Режим попуне: Опште Леви клик отвара слику у спољашњем приказивачу попуњене ивице Путање Путање: Пауза на лебдење мишем опсецање по размери уклапање по размери Насумичне ставке развучено Слика се умножава водоравно и усправно. Слика се никако не мења. Слика се скалира да стане. Слика се скалира равномерно да попуни све, ако треба уз опсецање. Слика се скалира равномерно да стане без опсецања. Слика се развлачи водоравно а поплочава усправно. Слика се развлачи усправно а поплочава водоравно. поплочано поплочано водоравно поплочано усправно s 