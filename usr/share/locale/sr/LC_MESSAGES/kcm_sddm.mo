��    ,      |  ;   �      �  (   �     �  �   �     �     �     �     �     �     �     �     �     �               %     1      J     k     s     �     �     �     �     �     �     �               /     4     I     Y     l     y     �     �      �  7   �                     1     6  !  <     ^	     r	  '   v	     �	     �	  
   �	  .   �	     �	     
     )
  4   8
     m
     �
     �
     �
  6   �
       
        &     E  $   b     �     �     �  4   �          -     D     Z  )   a  $   �  ?   �     �  )     +   0     \  I   j  f   �       0   $  "   U     x     �     !                 %   +   ,      "   #                               &                  	                           )                             *      '      $      (                                
    %1 is the name of a session%1 (Wayland) ... @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) @title:windowSelect Image Advanced Author Auto &Login Background: Clear Image Commands Could not decompress archive Cursor Theme: Customize theme Default Description Download New SDDM Themes EMAIL OF TRANSLATORSYour emails General Get New Theme Halt Command: Install From File Install a theme. Invalid theme package Load from file... Login screen using the SDDM Maximum UID: Minimum UID: NAME OF TRANSLATORSYour names Name No preview available Reboot Command: Relogin after quit Remove Theme SDDM KDE Config SDDM theme installer Session: The default cursor theme in SDDM The theme to install, must be an existing archive file. Theme Unable to install theme Uninstall a theme. User User: Project-Id-Version: kcm_sddm
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2017-10-30 23:08+0100
Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>
Language-Team: Serbian <kde-i18n-sr@kde.org>
Language: sr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
 %1 (вејланд) ... (доступне величине: %1) Избор слике Напредно Аутор Аутоматско &пријављивање Позадина: Очисти слику Наредбе Не могу да распакујем архиву Тема показивача: Прилагођена тема Подразумевана опис Преузимање нових тема за СДДМ caslav.ilic@gmx.net Опште Добави нову тему Наредба гашења: Инсталирај из фајла Инсталирај тему. Лош пакет теме Учитај из фајла... Пријавни екран преко СДДМ‑а Највећи УИД: Најмањи УИД: Часлав Илић име Преглед није доступан. Наредба ресетовања: Поновно пријављивање по напуштању Уклони тему КДЕ подешавање СДДМ‑а Инсталатор тема за СДДМ Сесија: Подразумевана тема показивача у СДДМ‑у Тема за инсталирање, мора да буде постојећи фајл архиве. Тема Не могу да инсталирам тему Деинсталирај тему. Корисник Корисник: 