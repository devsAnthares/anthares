��    _                   C   	  /   M  -   }     �     �     �     �      	     	     1	     >	     J	  
   S	     ^	     g	     p	     �	  	   �	     �	     �	     �	  ,   �	  	    
     

  
   )
     4
     L
     `
     u
     �
     �
     �
     �
     �
  	   �
     �
     �
     
               !     (  	   ;     E     ]  
   r     }     �  
   �     �     �     �  
   �     �     �               $     2     @     R     h     y     �     �     �     �  	   �     �     �     �               4     Q     j     �     �     �     �  	   �     �  ,   �          !     0     @     L     S     b     t     �  #   �     �  <  �  E   	     O     a     }     �  '   �  <   �          2     :  .   K     z     �     �     �     �     �     �     �          3  O   I     �  2   �     �  (   �  (     *   E     p     �  !   �  2   �  
               +   2     ^     z     �     �     �     �     �  �   �  �   �     a  &     (   �     �  3   �  
        )     G     X  �   g  �   �  �   }       !     #   @  "   d  *   �     �  <   �          *  #   ;     _     m     �  1   �  $   �  )   �  =     =   R  ?   �  .   �  .   �  0   .     _  
   o     z  Q   �     �  &   �      $  !   E     g     t     �  .   �  _   �  M   ;     �         Q         Y   5           %       H       J   !              \   *       @   V   4   [       A   (   
       U   B   ]   ^       $   _         '   >       D   K      -                  ?      ,   O   0       R   8   6   W   2   I   =   X   E   #       N   C       T   G   M          3   "         9             +          S          F           ;   :                    &   <   L          P         7          .       /       1                     )            	   Z       @action opens a software center with the applicationManage '%1'... @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Desktop Add to Favorites Add to Panel (Widget) Align search results to bottom All Applications App name (Generic name)%1 (%2) Applications Apps & Docs Behavior Categories Computer Contacts Description (Name) Description only Documents Edit Application... Edit Applications... End session Expand search to bookmarks, files and emails Favorites Flatten menu to a single level Forget All Forget All Applications Forget All Contacts Forget All Documents Forget Application Forget Contact Forget Document Forget Recent Documents General Generic name (App name)%1 (%2) Hibernate Hide %1 Hide Application Icon: Lock Lock screen Logout Name (Description) Name only Often Used Applications Often Used Documents Often used On All Activities On The Current Activity Open with: Pin to Task Manager Places Power / Session Properties Reboot Recent Applications Recent Contacts Recent Documents Recently Used Recently used Removable Storage Remove from Favorites Restart computer Run Command... Run a command or a search query Save Session Search Search results Search... Searching for '%1' Session Show Contact Information... Show In Favorites Show applications as: Show often used applications Show often used contacts Show often used documents Show recent applications Show recent contacts Show recent documents Show: Shut Down Sort alphabetically Start a parallel session as a different user Suspend Suspend to RAM Suspend to disk Switch User System System actions Turn off computer Type to search. Unhide Applications in '%1' Unhide Applications in this Submenu Widgets Project-Id-Version: plasma_applet_org.kde.plasma.kicker
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-23 05:56+0100
PO-Revision-Date: 2017-09-25 19:53+0200
Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>
Language-Team: Serbian <kde-i18n-sr@kde.org>
Language: sr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
 Управљај „%1“...|/|Управљај „$[ном %1]“... Изабери... Очисти иконицу Додај на површ Додај у омиљене Додај на панел (виџет) Равнај резултате претраге уз дно Сви програми %1 (%2) Програми Програми и документација Понашање Категорије Рачунар Контакти опис (име) само опис Документи Уреди програм... Уреди програме... Крај сесије Претражуј и обележиваче, фајлове и е‑пошту Омиљено Мени спљоштен на један ниво Заборави све Заборави све програме Заборави све контакте Заборави све документе Заборави програм Заборави контакт Заборави документ Заборави недавне документе Опште %1 (%2) У хибернацију Сакриј %1|/|Сакриј $[аку %1] Сакриј програм Иконица: Закључај Закључај екран Одјави ме име (опис) само име Често коришћени програми|/|$[својства ген 'често коришћених програма' аку 'често коришћене програме'] Често коришћени документи|/|$[својства ген 'често коришћених докумената' аку 'често коришћене документе'] Често коришћено На свим активностима На текућој активности Отвори помоћу: Прикачи на менаџер задатака Места Напајање/сесија Својства Ресетуј Недавни програми|/|$[својства ген 'недавних програма' аку 'недавне програме'] Недавни контакти|/|$[својства ген 'недавних контаката' аку 'недавне контакте'] Недавни документи|/|$[својства ген 'недавних докумената' аку 'недавне документе'] Недавно Недавно коришћено Уклоњиво складиште Уклони из омиљених Поново покрени рачунар Изврши наредбу... Изврши наредбу или упит претраге Сачувај сесију Претрага Резултати претраге Тражи... Тражим „%1“... Сесија Прикажи податке контакта... Прикажи у омиљенима Приказуј програме као: Прикажи често коришћене програме Прикажи често коришћене контакте Прикажи често коришћене документе Прикажи недавне програме Прикажи недавне контакте Прикажи недавне документе Прикажи: Угаси Поређај азбучно Покрени напореду сесију за другог корисника Суспендуј Суспендуј у меморију Суспендуј на диск Пребаци корисника Систем Системске радње Угаси рачунар Унесите нешто за тражење. Откриј програме у „%1“|/|Откриј програме у „$[лок %1]“ Откриј скривене програме у овом подменију Виџети 