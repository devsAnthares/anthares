��          �      �       H     I     a      m     �     �     �  
   �     �  &   �          .     L  1   b  J  �  *   �     
        !   4  '   V     ~     �     �  2   �  /   �  /   '  )   W  R   �         	                                    
                    Configure Desktop Theme David Rosca EMAIL OF TRANSLATORSYour emails Get New Themes... Install from File... NAME OF TRANSLATORSYour names Open Theme Remove Theme Theme Files (*.zip *.tar.gz *.tar.bz2) Theme installation failed. Theme installed successfully. Theme removal failed. This module lets you configure the desktop theme. Project-Id-Version: kcm_desktoptheme
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-22 05:21+0100
PO-Revision-Date: 2017-10-30 23:08+0100
Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>
Language-Team: Serbian <kde-i18n-sr@kde.org>
Language: sr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Associated-UI-Catalogs: plasma
X-Environment: kde
 Подешавање теме површи Давид Росца caslav.ilic@gmx.net Добави нове теме... Инсталирај из фајла... Часлав Илић Отварање теме Уклони тему фајлови тема (*.zip *.tar.gz *.tar.bz2) Инсталирање теме пропало. Тема успешно инсталирана. Уклањање теме пропало. У овом модулу можете да подесите тему површи. 