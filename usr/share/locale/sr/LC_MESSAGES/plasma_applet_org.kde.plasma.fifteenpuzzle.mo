��          �            x  
   y  	   �     �     �  3   �     �     �                    %     *     F  B   Y     �  C  �     �     �          *  A   A     �  +   �     �     �     �       =     (   [     �     �     	                                                  
                        Appearance Browse... Choose an image Fifteen Puzzle Image Files (*.png *.jpg *.jpeg *.bmp *.svg *.svgz) Number color Path to custom image Piece color Show numerals Shuffle Size Solve by arranging in order Solved! Try again. The time since the puzzle started, in minutes and secondsTime: %1 Use custom image Project-Id-Version: plasma_applet_org.kde.plasma.fifteenpuzzle
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-01-12 03:59+0100
PO-Revision-Date: 2017-01-14 19:23+0100
Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>
Language-Team: Serbian <kde-i18n-sr@kde.org>
Language: sr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
 Изглед Прегледај... Избор слике Петнаестица фајлови слика (*.png *.jpg *.jpeg *.bmp *.svg *.svgz) Боја бројева Путања до посебне слике Боја делова Нумериши делове Промешај Величина Реши распоређивањем по редоследу Решено! Пробајте опет. Време: %1 Посебна слика 