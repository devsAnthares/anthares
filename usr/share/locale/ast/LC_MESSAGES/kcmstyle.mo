��          �   %   �      P  &   Q  �   x     �          0     7     E     V      f     �     �     �     �     �     �     �                    #  	   5  C   ?  E   �     �     �  �  �  &   �  �   �     D     R     _     f     u     �     �     �     �  	   �     �      �               3     @     M     a     w  C   �  W   �      	     7	                                                                            	             
                                    (c) 2002 Karol Szwed, Daniel Molkentin <h1>Style</h1>This module allows you to modify the visual appearance of user interface elements, such as the widget style and effects. @title:tab&Applications @title:tab&Fine Tuning Button Con&figure... Daniel Molkentin Description: %1 EMAIL OF TRANSLATORSYour emails KDE Style Module Karol Szwed NAME OF TRANSLATORSYour names No Text No description available. Ralf Nolden Show icons in menus: Tab 1 Tab 2 Text Below Icons Text Beside Icons Text Only There was an error loading the configuration dialog for this style. This page allows you to choose details about the widget style options Toolbars Unable to Load Dialog Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-17 05:44+0100
PO-Revision-Date: 2017-06-28 21:25+0100
Last-Translator: enolp <enolp@softastur.org>
Language-Team: Asturian <alministradores@softastur.org>
Language: ast
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 (c) 2002 Karol Szwed, Daniel Molkentin <h1>Estilu</h1>Esti módulu permítete modificar l'aspeutu visual de los elementos de la interfaz d'usuariu como l'estilu y efeutos de los widgets. &Aplicaciones Axuste &finu Botón Con&figurar... Daniel Molkentin Descripción: %1 alministradores@softastur.org Módulu KDE d'estilu Karol Szwed Softastur Ensin testu Nun hai descripción disponible. Ralf Nolden Amosar iconos nos menús: Llingüeta 1 Llingüeta 2 Testu so los iconos Testu cabo los iconos Namái testu Hebo un fallu cargando'l diálogu de configuración pa esti estilu. Esta páxina permítete escoyer los detalles tocante a les opciones d'estilu de widgets Barres de ferramientes Nun pue cargase'l diálogu 