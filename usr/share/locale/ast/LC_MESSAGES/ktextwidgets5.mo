��    I      d  a   �      0     1     :     @     S     \     k     q     �      �  )   �     �     �     �          (     >     P     d     {  #   �  '   �  *   �  +     -   4  '   b  %   �  !   �  /   �  $   	     '	  %   G	  /   m	  $   �	  "   �	  "   �	     
     "
     9
     K
     Y
     x
     �
  '   �
     �
     �
     �
       /     +   ;     g     s  	   �  	   �     �     �  
   �  	   �     �     �               $     ,     @     H     `     m     �     �     �     �  '   �  �  �  
   �  
   �     �     �     �     �     �     �  <   	  *   F     q     x     �     �     �     �     �     �     �     �     �  
          	             "     +     1     :     >     B     K     T     X     k     ~     �     �     �     �     �     �  *        <     R     n     �  *   �  (   �     �     �          &     6     O     o          �     �      �     �     �     �     �     �  
        $     8  $   P     u     �  $   �     6   +          F          :             !      	   .           A   9       2      )   D   @   '   I   ?                        H      8   &      #                 7   1                      G       B      5      $   ,       /       3           -         ;   0          %       (         
      =   <               E   >   4                 "       *   C    &Edit... &Find &Prompt on replace &Replace &Selected text &Skip &Text to find: &Whole words only 1 match found. %1 matches found. 1 replacement done. %1 replacements done. @action&Font @action&Strike Out @actionFont &Size @actionLeft-to-Right @actionRight-to-Left @actionSubscript @actionSuperscript @actionText &Color... @actionTo Plain Text @action boldify selected text&Bold @action italicize selected text&Italic @action underline selected text&Underline @action:button Replace all occurrences&All @action:button Restart find & replaceRestart @action:button Stop find & replaceStop @item:inmenu circle list styleCircle @item:inmenu disc list styleDisc @item:inmenu lower case roman numeralsi ii iii @item:inmenu lowercase abc listsabc @item:inmenu numbered lists123 @item:inmenu square list styleSquare @item:inmenu upper case roman numeralsI II III @item:inmenu uppercase abc listsABC @label left-to-rightLeft-to-Right @label right-to-leftRight-to-Left @label stroke colorColor @title:menuList Style Allow Tabulations Any Character Beginning of document reached. C&ase sensitive Check Spelling... Click for a menu of available captures. Complete Match Continue from the beginning? Continue from the end? Digit Do you want to restart search at the beginning? Do you want to restart search from the end? End of Line End of document reached. Find Next Find Text Insert Place&holder Invalid regular expression. Link Text: Link URL: Newline No text was replaced. Nothing to spell check. Optional Options Regular e&xpression Replace Replace '%1' with '%2'? Replace With Replace&ment text: Set of Characters Spell Checking Language Start of Line Use p&laceholders You must enter some text to search for. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-28 03:01+0100
PO-Revision-Date: 2016-12-22 13:27+0100
Last-Translator: enolp <enolp@softastur.org>
Language-Team: Asturian <alministradores@softastur.org>
Language: ast
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 &Editar... &Alcontrar &Suxerir al trocar &Trocar Testu &esbilláu &Saltar &Testu p'alcontrar: Namái &toles pallabres Alcontróse 1 coincidencia. Alcontráronse %1 coincidencies. Fixose 1 troquéu. Fixéronse %1 troqueos. &Fonte &Tacháu Tamañu de &fonte Izquierda a drecha Drecha a izquierda Superíndiz Soíndiz &Color del testu... A testu planu &Negrina &Cursiva &Sorrayáu &Too Reaniciar Parar Círculu Discu i ii iii abc 123 Cuadráu I II III ABC Izquierda a drecha Drecha a izquierda Color Estilu de llistáu Permitir tabulaciones Cualesquier caráuter Algamóse l'entamu'l documentu. &Sensible a mayúscules Comprobar ortografía Primi pa un menú de captures disponibles. Coincidencia completa ¿Continuar dende l'entamu? ¿Continuar dende la fin? Díxitu ¿Quies reaniciar la gueta dende l'entamu? ¿Quies reaniciar la gueta dende la fin? Fin de llinia Algamóse la fin del documentu. Alcontrar siguiente Alcontrar testu Inxertar espaciu acutáu Espresión regular non válida. Testu d'enllaz: URL d'enllaz: Llinia nueva Nun se trocó testu. Nada pa comprobar la ortografía Opcional Opciones E&spresión regular Trocar ¿Trocar «%1» con «%2»? Trocar con Testu de &troquéu: Conxuntu de caráuteres Llingua del comprobador ortográficu Entamu de llinia Usar espacios a&cutaos Has introducir dél testu pa guetar. 