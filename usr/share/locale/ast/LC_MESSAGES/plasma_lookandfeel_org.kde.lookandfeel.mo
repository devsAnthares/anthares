��    ,      |  ;   �      �     �     �  .   �  5        7     >     N     T  	   a     k     �     �     �  1   �     �     �                  '   0     X     [     d  '   k     �     �     �     �  5   �     �          
             $   ,  A   Q  �   �     y     �  C   �  '   �     �       �  "     �	     �	     �	     �	  	   �	     
     
      
  
   -
      8
     Y
     r
     �
  =   �
     �
     �
  &   
     1     @  
   `     k     s  	     3   �     �  
   �  	   �     �  -   �       	   3     =     E     X  	   j     t     �     �     �     �     �     �     �             #                    '                +   $       &      )                	                                    !   (      "                %                                    
   *       ,        %1% Back Button to change keyboard layoutSwitch layout Button to show/hide virtual keyboardVirtual Keyboard Cancel Caps Lock is on Close Close Search Configure Configure Search Plugins Desktop Session: %1 Different User Keyboard Layout: %1 Logging out in 1 second Logging out in %1 seconds Login Login Failed Login as different user Logout No media playing Nobody logged in on that sessionUnused OK Password Reboot Reboot in 1 second Reboot in %1 seconds Recent Queries Remove Restart Shutdown Shutting down in 1 second Shutting down in %1 seconds Start New Session Suspend Switch Switch Session Switch User Textfield placeholder textSearch... Textfield placeholder text, query specific KRunnerSearch '%1'... This is the first text the user sees while starting in the splash screen, should be translated as something short, is a form that can be seen on a product. Plasma is the project name so shouldn't be translated.Plasma made by KDE Unlock Unlocking failed User logged in on console (X display number)on TTY %1 (Display %2) User logged in on console numberTTY %1 Username Username (location)%1 (%2) Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-09 03:21+0100
PO-Revision-Date: 2017-09-05 23:36+0100
Last-Translator: enolp <enolp@softastur.org>
Language-Team: Asturian <alministradores@softastur.org>
Language: ast
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 %1% Atrás Camudar de distribución Tecláu virtual Encaboxar Bloq Mayús ta activáu Zarrar Zarrar gueta Configurar Configurar complementos de gueta Sesión d'escritoriu: %1 Usuariu diferente Distribución de tecláu: %1 Zarrando sesión en 1 segundu Zarrando sesión en %1 segundos Aniciar sesión Falló l'aniciu de sesión Aniciar sesión como usuariu diferente Zarrar sesión Nun hai reproduciéndose medios Ensin usar Aceutar Contraseña Reaniciar Reaniciando en 1 segundu Reaniciando en %1 segundos Solicitúes recientes Desaniciar Reaniciar Apagar Apagando en 1 segundu Apagando en %1 segundos Aniciar sesión nueva Suspender Camudar Camudar de sesión Camudar d'usuariu Guetar... Guetar «%1»... Plasma ta fechu por KDE Desbloquiar Falló'l desbloquéu en TTY %1 (Pantalla %2) TTY %1 Nome d'usuariu %1 (%2) 