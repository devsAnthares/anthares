��    #      4  /   L           	       )   "     L  $   g  &   �  #   �  !   �  "   �          ,     >     R     j     }  L   �     �     �     �           )  
   0     ;     Z     n     v       "   �     �  )   �  <   �     -  ;   J     �  �  �     =     B     H     e  (   s  #   �  '   �  %   �  %   	     4	  
   <	     G	     O	     \	  !   d	  U   �	     �	     �	     �	     
     "
     *
  	   9
     C
     S
     [
     i
     p
     �
  .   �
  H   �
     	               #                                              "                 !             	                                                                  
          100% @info:creditAuthor @info:creditCopyright 2015 Harald Sitter @info:creditHarald Sitter @labelNo Applications Playing Audio @labelNo Applications Recording Audio @labelNo Device Profiles Available @labelNo Input Devices Available @labelNo Output Devices Available @labelProfile: @titlePulseAudio @title:tabAdvanced @title:tabApplications @title:tabDevices Advanced Output Configuration Automatically switch all running streams when a new output becomes available Capture Default Device Profiles EMAIL OF TRANSLATORSYour emails Inputs Mute audio NAME OF TRANSLATORSYour names Notification Sounds Outputs Playback Port Port is unavailable (unavailable) Port is unplugged (unplugged) Requires 'module-gconf' PulseAudio module This module allows to set up the Pulseaudio sound subsystem. label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-03 03:00+0200
PO-Revision-Date: 2017-06-30 16:55+0100
Last-Translator: enolp <enolp@softastur.org>
Language-Team: Asturian <alministradores@softastur.org>
Language: ast
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 100% Autor Copyright 2015 Harald Sitter Harald Sitter Nun hai aplicaciones reproduciendo audiu Nun hai aplicaciones grabando audiu Nun hai perfiles disponibles de preseos Nun hai disponibles preseos d'entrada Nun hai disponibles preseos de salida Perfil: PulseAudio Avanzao Aplicaciones Preseos Configuración avanzada de salida Camudar automáticamente tolos fluxos n'execución al tar disponible una salida nueva Captura Por defeutu Perfiles de preseos alministradores@softastur.org Entraes Silencia audiu Softastur Soníos d'avisu Salíes Reproducción Puertu (non disponible) (desenchufóse) Rique'l módulu de PulseAudio «module-gconf» Esti módulu permítete configurar el sosistema de soníu de PulseAudio. %1: %2 100% %1% 