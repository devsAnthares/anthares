��    H      \  a   �         	   !     +     8     ?     P     V     ^     g     v     {     �  	   �     �     �     �     �     �     �  ;   �  -     #   5     Y     r     �  
   �     �  
   �     �     �     �          "     6     ;  	   V      `     �     �  
   �     �     �  
   �     �  )   	     -	     G	     _	     n	     	     �	     �	     �	     �	     �	     �	  C   �	  s   8
     �
     �
     �
      �
       -     /   J     z  	   �     �     �     �     �     �  �  �     }     �     �     �     �     �     �     �     �  	   �     �  
   �       
             /     8     A  
   J     U     ]     y  
   �     �     �  	   �     �     �     �  $   �           ?     W     _     ~     �     �     �     �     �  	   �     �       '        ;  !   W     y     �     �     �     �     �  #   �          #  O   ;  x   �  !        &     8  &   Q     x  /   �  0   �     �  	   �     �                 
        )   =   $   ;       (   1   <       :         3   	            ?                 #   C   B          *   A                @   
      4   +   2          G   "       9       D              '       !             H                      -       0      6            E            >          7               F   %       5   /              8      ,          &   .    &About %1 &Actual Size &Close &Configure %1... &Copy &Donate &Find... &Move to Trash &New &Open... &Paste &Print... &Quit &Replace... &Report Bug... &Save &Undo &Zoom... @action:button Goes to next tip, opposite to previous&Next @action:button Goes to previous tip&Previous @option:check&Show tips on startup @titleDid you know...?
 @title:windowConfigure @title:windowTip of the Day About &KDE C&lear Clear List Close document Configure &Notifications... Configure Tool&bars... Copy selection to clipboard Create new document Cu&t Cut selection to clipboard Dese&lect EMAIL OF TRANSLATORSYour emails Encodings menuAutodetect Encodings menuDefault Find &Next Find Pre&vious NAME OF TRANSLATORSYour names No Entries Open &Recent Open a document which was recently opened Open an existing document Paste clipboard content Print document Quit application Re&do Redo last undone action Save &As... Save document Save document under a new name Select &All Select zoom level Show Menubar<p>Shows the menubar again after it has been hidden</p> Show Statusbar<p>Shows the statusbar, which is the bar at the bottom of the window used for status information.</p> Switch Application &Language... Tip of the &Day Undo last action View document at its actual size What's &This? You are not allowed to save the configuration You will be asked to authenticate before saving Zoom &In Zoom &Out go back&Back go forward&Forward home page&Home show help&Help without name Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-16 03:10+0100
PO-Revision-Date: 2016-12-21 13:40+0100
Last-Translator: enolp <enolp@softastur.org>
Language-Team: Asturian <alministradores@softastur.org>
Language: ast
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 &Tocante a %1 Tamañu &actual &Zarrar &Configurar %1 &Copiar &Donar &Alcontrar... &Mover a la papelera &Nuevo &Abrir... &Apegar &Imprentar &Colar &Trocar... &Infomar de fallu... &Guardar &Defacer &Zoom... &Siguiente &Previu &Amosar conseyos nel aniciu ¿Sabíes que...?
 Configurar Conseyu del día Tocante a &KDE &Llimpiar Llimpiar llistáu Zarra'l documentu Configurar a&visos... Configurar barres de ferramientes... Copia la esbilla al cartefueyu Crea un documentu nuevu &Cortar Corta la esbilla al cartafueyu D&esesbillar alministradores@softastur.org Auto-deteutar Por defeutu Alcontrar &siguiente Alcontrar &anterior Softastur Ensin entraes Abrir &recién Abre un documentu que s'abrió apocayá Abre un documentu esistente Apiega'l conteníu del cartafueyu Imprenta'l documentu Cola de l'aplicación &Refacer Refai l'aición desfecha cabera Guardar &como... Guarda'l documentu Guarda'l documentu so un nome nuevu Esbillar &too Esbilla'l nivel de zoom Amosar barra de menú<p>Vuelve amosar la barra de menú dempués d'anubrila</p> Amosar barra d'estáu<p>Amuesa la barra d'estau que ta na barra baxera de la ventana usada pola información estáu.</p> Camudar &llingua d'aplicación... Conseyu del &día Desfái l'aición cabera Mira'l documentu nel so tamañu actual ¿Qué &ye esto? Nun tienes permisu pa guardar la configuración Pidirásete que t'autentiques enantes de guardar A&verar Allo&ñar &Atrás &Alantre &Aniciu &Ayuda ensin nome 