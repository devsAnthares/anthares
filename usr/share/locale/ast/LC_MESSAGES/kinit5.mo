��          �            h     i  $   �     �      �     �                >     K  '   i  �   �  �   E     �  t     �  �  (   (  )   Q  &   {  *   �  (   �     �          ,  !   9  1   [  �   �  �   <     �  v   	                           
                                	                     Could not find '%1' executable. Could not find 'kdemain' in '%1'.
%2 Could not find service '%1'. Could not find the '%1' plugin.
 Could not open library '%1'.
%2 Error loading '%1'. KDEInit could not launch '%1' Launching %1 Service '%1' is malformatted. Service '%1' must be executable to run. Unable to create new process.
The system may have reached the maximum number of processes possible or the maximum number of processes that you are allowed to use has been reached. Unable to start new process.
The system may have reached the maximum number of open files possible or the maximum number of open files that you are allowed to use has been reached. Unknown protocol '%1'.
 klauncher: This program is not supposed to be started manually.
klauncher: It is started automatically by kdeinit5.
 Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-04 03:08+0100
PO-Revision-Date: 2016-12-20 23:12+0100
Last-Translator: enolp <enolp@softastur.org>
Language-Team: Asturian <alministradores@softastur.org>
Language: ast
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Nun pudo alcontrase l'executable «%1». Nun pudo abrise «kdemain» en «%1».
%2 Nun pudo alcontrase'l serviciu «%1». Nun pudo alcontrase'l complementu «%1».
 Nun pudo abrise la biblioteca «%1»,
%2 Fallu lleendo «%1». KDEInit nun pudo llanzar «%1» Llanzando %1 El serviciu «%1» ta malformáu. El serviciu «%1» ha ser executable pa executase Nun pue aniciase'l procesu nuevu.
El sistema quiciabes algamare'l númberu máximu de procesos posibles o algamose'l númberu máximu de procesos que tienen permisu pa usase. Nun pue aniciase'l procesu nuevu.
El sistema quiciabes algamare'l númberu máximu de ficheros abiertos posibles o algamose'l númberu máximu de ficheros abiertos que tienen permisu pa usase. Protocolu desconocíu «%1».
 klauncher: Esti programa nun se supón que s'anicie automáticamente.
klauncher: Llánzalu automáticamente kdeinit5.
 