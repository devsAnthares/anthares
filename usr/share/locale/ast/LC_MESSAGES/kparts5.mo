��          �   %   �      `     a  :   i     �     �  =   �          *     G     [     t     �  6   �     �     �  #   �           >     F     T     d     �     �  ;   �  V   �  G   %     m  �  v       -        M     [  0   k     �  	   �     �  
   �     �     �  )   �            $         E     c     l     �  	   �     �     �  <   �  O   �  G   =	     �	                                                  
                                                                   	       &Search <qt>Do you want to search the Internet for <b>%1</b>?</qt> @action:inmenuOpen &with %1 @infoOpen '%1'? @info:whatsthisThis is the file name suggested by the server @label File nameName: %1 @label Type of fileType: %1 @label:button&Open @label:button&Open with @label:button&Open with %1 @label:button&Open with... @label:checkboxRemember action for files of this type Accept Close Document Do you really want to execute '%1'? EMAIL OF TRANSLATORSYour emails Execute Execute File? Internet Search NAME OF TRANSLATORSYour names Reject Save As The Download Manager (%1) could not be found in your $PATH  The document "%1" has been modified.
Do you want to save your changes or discard them? Try to reinstall it  

The integration with Konqueror will be disabled. Untitled Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2016-12-20 23:26+0100
Last-Translator: enolp <enolp@softastur.org>
Language-Team: Asturian <alministradores@softastur.org>
Language: ast
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 &Guetar <qt>¿Quies guetar n'internet <b>%1</b>?</qt> Abrir &con %1 ¿Abrir «%1»? Esto ye un nome de ficheru suxeríu pol sirvidor Nome: %1 Triba: %1 &Abrir &Abrir con &Abrir con %1 &Abrir con... Recordar aición pa ficheros d'esta triba Aceutar Zarrar documentu ¿De xuru que quies executar «%1»? alministradores@softastur.org Executar ¿Executar ficheru? Gueta d'internet Softastur Refugar Guardar como Nun pudo alcontrase'l xestor de descargues (%1) nel to $PATH Modificóse'l documentu «%1».
¿Quies guardar o escartar les tos camudancies? Prueba a reinstalalu  

Deshabilitaráse la integración con Konqueror. Ensin títulu 