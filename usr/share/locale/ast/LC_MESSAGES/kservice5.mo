��          �      l      �     �  /   �     %     >     Y     r      �  !   �  %   �  )   �      "  c   C  -   �  D   �          9  !   T  B   v  @   �     �  �       �  .   �     �     �       %   '  &   M  #   t  *   �  +   �     �  [     0   i  B   �  	   �  !   �  "   		  K   ,	  >   x	     �	                                                                           	                
       @info:creditAuthor @info:creditCopyright 1999-2014 KDE Developers @info:creditDavid Faure @info:creditWaldo Bastian Could not launch Browser Could not launch Mail Client Could not launch Terminal Client Could not launch the browser:

%1 Could not launch the mail client:

%1 Could not launch the terminal client:

%1 EMAIL OF TRANSLATORSYour emails Error launching %1. Either KLauncher is not running anymore, or it failed to start the application. Function must be called from the main thread. KLauncher could not be reached via D-Bus. Error when calling %1:
%2
 NAME OF TRANSLATORSYour names No service implementing %1 The provided service is not valid The service '%1' provides no library or the Library key is missing application descriptionRebuilds the system configuration cache. application nameKBuildSycoca Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-02-07 08:40+0100
PO-Revision-Date: 2016-12-22 03:23+0100
Last-Translator: enolp <enolp@softastur.org>
Language-Team: Asturian <alministradores@softastur.org>
Language: ast
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Autor Copyright 1999-2014 Los desendolcadores de KDE David Faure Waldo Bastian Nun pudo llanzase'l restolador Nun pudo llanzase'l veceru de corréu Nun pudo llanzase'l veceru de terminal Nun pudo llanzase'l restolador:

%1 Nun pudo llanzase'l veceru de corréu:

%1 Nun pudo llanzase'l veceru de terminal:

%1 alministradores@softastur.org Fallu al llanzar %1. O KLauncher yá nun ta execución, o falló al aniciase l'aplicación. Ha llamase a la función dende'l filu principal. Nun pudo algamase a KLauncher per D-Bus. Fallu al llamar a %1:
%2
 Softastur Nun hai serviciu qu'implemente %1 El serviciu forníu nun ye válidu El serviciu «%1» nun forne biblioteques o falta la clave de la biblioteca Reconstrúi'l sistema de caché de configuración del sistema. KBuildSycoca 