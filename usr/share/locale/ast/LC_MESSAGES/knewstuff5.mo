��    V      �     |      x     y  -   �     �     �     �        0        E     c  A   �     �     �     �     �     	     	     	  	   *	  
   4	     ?	  #   Q	  %   u	  '   �	     �	  )   �	     
     
     
     &
     8
  "   N
     q
     �
  $   �
  $   �
      �
     �
               %     )     9     A  
   O     Z     h     m     v     �     �     �     �     �  	   �     �       
          	   .     8  	   Q     [     a     h  	   }  )   �  -   �  $   �       
     &     �   >     �       ?     	   K     U     \     e  	   w     �     �  !   �  w   �  5   7  �  m       5   '     ]     p     �     �  %   �     �     �     �     �               *     B     I     M     ]     e      z  3   �  6   �  5        <  A   R     �     �     �     �     �  '   �          /  8   5  8   n  1   �     �     �     	               )     2  
   E     P     f  
   k     v     �     �  "   �     �     �               (  
   .  	   9     C  !   P     r          �     �  	   �     �     �     �     �     �  *   �  �        �     �  L   �     
               &     9  	   I     S     i  ,   �     �            L                 V                 @          (       ;       .   ?   #       /       Q   =                         D           6       P   )       $   R      S   F   4   <       	       -   G   >      B      +       %   !   T              O       9   ,   N   *                "   8      J           0   7   &   I                                    K                 2          1      A      M   '          U              :   
          H              C   E   5   3    1 download %1 downloads <a href="http://opendesktop.org">Homepage</a> <br />Provider: %1 <br />Version: %1 @action:inmenuConfigure... @action:inmenuMore @action:inmenuNo further information available. @action:inmenuNot installed: @action:inmenuVisit homepage A link to the description of this Get Hot New Stuff itemHomepage All Categories All Providers All categories are missing Authentication error. Author: BSD Become a Fan Category: Changelog: Checking login... Configuration file is invalid: "%1" Could not fetch provider information. Could not install "%1": file not found. Could not install %1 Could not verify login, please try again. Description: Details Details for %1 Details view mode Download New Stuff... Download of "%1" failed, error: %2 Enter search phrase here Error Fetching content data from server... Fetching license data from server... Fetching provider information... File not found: %1 File to upload: Finish GPL Icons view mode Install Install Again Installing Invalid item. LGPL License: Loading Preview Loading data Loading data from provider Loading provider information Main section More tools... Move down Move to Main section Move up New Upload Opposite to BackNext Order by: Overwrite existing file? Password: Price Price: Provider information Provider: Request installation of this itemInstall Request uninstallation of this itemUninstall Request updating of this itemUpdate Search: Server: %1 The selected category "%1" is invalid. The server does not recognize the category %2 to which you are trying to upload. The server does not recognize any of the categories to which you are trying to upload: %2 There was a network error. Title: Too many requests to server. Please try again in a few minutes. Uninstall Update Updating Upload failed: %1 Username: Version: You are now a fan. fan as in supporter1 fan %1 fans the price of a download item, parameter 1 is the currency, 2 is the priceThis item costs %1 %2.
Do you want to buy it? voting for an item (good/bad)Your vote was recorded. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-12 03:00+0100
PO-Revision-Date: 2016-12-21 15:13+0100
Last-Translator: enolp <enolp@softastur.org>
Language-Team: Asturian <alministradores@softastur.org>
Language: ast
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 1 descarga %1 descargues <a href="http://opendesktop.org">Páxina d'aniciu</a> <br />Fornidor: %1 <br />Versión: %1 Configurar... Más Nun hai más información disponible. Nun s'instaló: Visitar páxina d'aniciu Páxina d'aniciu Toles estayes Tolos fornidores Falten toles estayes Fallu d'autenticación. Autor: BSD Facese siguidor Estaya: Rexistru de cambeos: Comprobando aniciu de sesión... El ficheru de configuración nun ye válidu: «%1» Nun pudo dise en cata de la información del fornidor. Nun pudo instalase «%1»: nun s'alcontró'l ficheru. Nun pudo instalase %1 Nun pudo verificase l'aniciu de sesión, por favor volvi tentalo. Descripción: Detalles Detalles pa %1 Mou de vista detallada Baxar coses nueves... Falló la descarga de «%1», fallu: %2 Introduz equí la fras de gueta Fallu Diendo en cada de los datos de conteníu del sirvidor... Diendo en cata de los datos de llicencia del sirvidor... Diendo en cata de la información del fornidor... Nun s'alcontró'l ficheru: %1 Ficheru pa xubir: Finar GPL Mou de vista d'iconos Instalar Instalar de nueves Instalando Elementu non válidu. LGPL Llicencia: Cargando previsualización Cargando datos Cargando datos del fornidor Cargando información del fornidor Seición principal Más ferramientes... Baxar Mover a Seición principal Xubir Xuba nueva Siguiente Ordenar per: ¿Sobrescribir ficheru esistente? Contraseña: Preciu Preciu: Información del fornidor Fornidor: Instalar Desinstalar Anovar Guetar: Sirvidor: %1 La estaya esbillada «%1» nun ye válida. El sirvidor nun reconoz la estaya %2 a la que tas tentando de xubir. El sirvidor nun reconoz denguna estaya a les que tas tentando de xubir: %2 Hai un fallu de rede. Títulu: Milenta solicitúes al sirvidor. Volvi tentalo en dellos minutos, por favor. Desinstalar Anovar Anovando Falló la xuba: %1 Nome d'usuariu: Versión: Agora ye un siguidor. 1 siguidor %1 siguidores Esti elementu cuesta %1 %2.
¿Quies mercalu? Grabóse'l to votu. 