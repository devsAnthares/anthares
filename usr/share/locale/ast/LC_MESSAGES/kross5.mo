��             +         �     �  +   �     	  4   &  &   [     �     �     �     �      �     �     �     �     �  ,        :     Z     y          �     �     �     �     �     �     �     �  &   �     &  B   ,     o  �  �     '     -     L     \     s     {  
   �     �     �     �     �     �  	   �     �  /     $   K  '   p     �     �     �  	   �     �  
   �     �     �  #   �     	  '    	     H	  ;   O	     �	                                                         
                                                                	                       @info:creditAuthor @info:creditCopyright 2006 Sebastian Sauer @info:creditSebastian Sauer @info:shell command-line argumentThe script to run. @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: NAME OF TRANSLATORSYour names Name: Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: application descriptionCommand-line utility to run Kross scripts. application nameKross Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2016-12-20 23:32+0100
Last-Translator: enolp <enolp@softastur.org>
Language-Team: Asturian <alministradores@softastur.org>
Language: ast
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Autor Copyright 2006 Sebastian Sauer Sebastian Sauer El script pa executar. Xeneral Amiesta un script nuevu Amestar... ¿Encaboxar? Comentariu: alministradores@softastur.org Editar Edita'l script esbilláu Editar... Executa'l script esbilláu. Fallu al crear el script pal intérprete «%1» Fallu al cargar l'intérprete «%1» Fallu al abrir el ficheru script «%1» Ficheru: Iconu: Intérprete: Softastur Nome: Desaniciar Desanicia'l script esbilláu Executar Nun esiste'l ficheru script «%1». Parar Para la execución del script esbilláu Testu: Utilidá de llinia de comandos pa executar scripts de Kross Kross 