��          �            h  5   i  �  �     �     �     �     �      �  	     
   "     -     L     a     z  7   �  �  �     `  �  h     	     &     A  $   X     }  	   �  
   �  	   �     �     �     �  E   �                               
                                       	          %1 is language name, %2 is language code name%1 (%2) %1 is the language codeThe translation files for the language with the code '%2' could not be found. The language has been removed from your configuration. If you want to add it back, please install the localization files for it and add the language again. The translation files for the languages with the codes '%2' could not be found. These languages have been removed from your configuration. If you want to add them back, please install the localization files for it and the languages again. Applying Language Settings Available &Translations: Available Languages: Configure Plasma translations EMAIL OF TRANSLATORSYour emails John Layt Maintainer NAME OF TRANSLATORSYour names Preferred Languages: Preferred Trans&lations: Translations Your changes will take effect the next time you log in. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2017-02-04 00:21+0100
Last-Translator: enolp <enolp@softastur.org>
Language-Team: Asturian <alministradores@softastur.org>
Language: ast
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 %1 (%2) Nun pudieron alcontrase los ficheros de traducción pa la llingua col códigu «%2», poro desanicióse de la to configuración. Si quies volver amestala, instala los ficheros de llocalización pa ella. Nun pudieron alcontrase los ficheros de traducción pa les llingües colos códigos «%2», poro desaniciáronse de la to configuración. Si quies volver amestales, instala los ficheros de llocalización pa elles. Aplicando axustes de llingua &Traducciones disponibles: Llingües disponibles: Configura les traducciones de Plasma alministradores@softastur.org John Layt Caltenedor Softastur Llingües preferíes: Tra&ducciones preferíes: Traducciones Les camudancies fadrán efeutu la próxima vegada qu'anicies sesión. 