��          �            h  ~   i  +   �           5     T     s     �     �  �   �  g   A  0   �  .   �     	  @     �  Z  }   4  2   �     �     �          #     /     A  �   W  �   �  "   �  +   �     �     �                                                   	             
                Click on the button, then enter the shortcut like you would in the program.
Example for Ctrl+A: hold the Ctrl key and press A. Conflict with Standard Application Shortcut EMAIL OF TRANSLATORSYour emails KPackage QML application shell NAME OF TRANSLATORSYour names No shortcut definedNone Reassign Reserved Shortcut The '%1' key combination is also used for the standard action "%2" that some applications use.
Do you really want to use it as a global shortcut as well? The F12 key is reserved on Windows, so cannot be used for a global shortcut.
Please choose another one. The key you just pressed is not supported by Qt. The unique name of the application (mandatory) Unsupported Key What the user inputs now will be taken as the new shortcutInput Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-30 03:07+0100
PO-Revision-Date: 2015-03-06 22:45+0200
Last-Translator: Lasse Liehu <lasse.liehu@gmail.com>
Language-Team: Finnish <lokalisointi@lists.coss.fi>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
X-POT-Import-Date: 2013-01-13 20:43:21+0000
 Napsauta painiketta ja paina sen jälkeen haluamasi pikanäppäin.
Esim. Ctrl+A:lle: paina Ctrl-näppäintä, sitten paina A. Ristiriita sovelluksen vakiopikanäppäimen kanssa lasse.liehu@gmail.com KPackagen QML-sovelluskuori Lasse Liehu Ei asetettu Sijoita uudelleen Varattu pikanäppäin Näppäinyhdistelmä %1 on jo käytössä vakiotoiminnolle %2.
Haluatko varmasti käyttää näppäinyhdistelmää myös työpöydänlaajuisena pikanäppäimenä? F12-näppäin on Windowsissa varattu, joten sitä ei voi käyttää työpöydänlaajuisena pikanäppäimenä.
Valitse toinen pikanäppäin. Qt ei tue painamaasi näppäintä. Sovelluksen yksilöllinen nimi (pakollinen) Ei-tuettu näppäin Yhdistelmä 