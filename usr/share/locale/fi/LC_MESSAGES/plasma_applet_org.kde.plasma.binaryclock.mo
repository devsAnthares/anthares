��    	      d      �       �   
   �      �      �   	              "     C  "   ]  �  �     j     r     z     �     �  #   �     �  $   �                             	                 Appearance Colors: Display seconds Draw grid Show inactive LEDs: Use custom color for active LEDs Use custom color for grid Use custom color for inactive LEDs Project-Id-Version: plasma_applet_binaryclock
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-31 05:47+0100
PO-Revision-Date: 2017-06-08 21:12+0200
Last-Translator: Tommi Nieminen <translator@legisign.org>
Language-Team: Finnish <kde-i18n-doc@kde.org>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-POT-Import-Date: 2012-12-01 22:25:23+0000
X-Generator: Lokalize 2.0
 Ulkoasu Värit: Näytä sekunnit Piirrä ruudukko Näytä passiiviset valot: Mukauta aktiivisten valojen väriä Mukauta ruudukon väriä Mukauta passiivisten valojen väriä 