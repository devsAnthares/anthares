��    ,      |  ;   �      �  (   �     �  �   �     �     �     �     �     �     �     �     �     �               %     1      J     k     s     �     �     �     �     �     �     �               /     4     I     Y     l     y     �     �      �  7   �                     1     6  �  <     �     �     �     	     	     	     '	     D	     L	     [	      d	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
     !
     /
     I
  &   ^
     �
     �
     �
     �
     �
     �
  (   �
          #     <     P  '   Y  0   �     �     �     �     �     �     !                 %   +   ,      "   #                               &                  	                           )                             *      '      $      (                                
    %1 is the name of a session%1 (Wayland) ... @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) @title:windowSelect Image Advanced Author Auto &Login Background: Clear Image Commands Could not decompress archive Cursor Theme: Customize theme Default Description Download New SDDM Themes EMAIL OF TRANSLATORSYour emails General Get New Theme Halt Command: Install From File Install a theme. Invalid theme package Load from file... Login screen using the SDDM Maximum UID: Minimum UID: NAME OF TRANSLATORSYour names Name No preview available Reboot Command: Relogin after quit Remove Theme SDDM KDE Config SDDM theme installer Session: The default cursor theme in SDDM The theme to install, must be an existing archive file. Theme Unable to install theme Uninstall a theme. User User: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2017-12-03 21:04+0900
Last-Translator: Lasse Liehu <lasse.liehu@gmail.com>
Language-Team: Finnish <kde-i18n-doc@kde.org>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 %1 (Wayland) … (Saatavilla koot: %1) Valitse kuva Lisäasetukset Tekijä &Automaattinen kirjautuminen Tausta: Tyhjennä kuva Komennot Arkiston purkaminen epäonnistui Osoitinteema: Teeman mukauttaminen Oletus Kuvaus Lataa uusia SDDM-teemoja lasse.liehu@gmail.com Yleistä Hae uusi teema Sammutuskomento: Asenna tiedostosta Asenna teema. Virheellinen teemapaketti Lataa tiedostosta… SDDM:ää käyttävä kirjautumisruutu Enimmäis-UID: Vähimmäis-UID: Lasse Liehu Nimi Ei esikatselua saatavilla Uudelleenkäynnistyskomento: Kirjaudu uudelleen lopettamisen jälkeen Poista teema SDDM:n asetukset KDE:ssa SDDM-teeman asennus Istunto: Hiiren osoittimen oletusteema SDDM:ssä Asennettava teema. Täytyy olla arkistotiedosto. Teema Teeman asennus epäonnistui Poista teema. Käyttäjä Käyttäjä: 