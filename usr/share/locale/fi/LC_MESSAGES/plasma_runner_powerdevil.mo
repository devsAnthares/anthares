��          �      L      �     �     �  �   �  T   }  )   �  (   �  0   %  $   V  &   {  &   �  %   �  ?   �  F   /     v     �     �     �     �    �     �       �   *  n   �     B  
   U     `     r  	   �     �     �     �     �     �     �     �  $   	  #   &	                                              
                    	                                  Dim screen by half Dim screen totally Lists screen brightness options or sets it to the brightness defined by :q:; e.g. screen brightness 50 would dim the screen to 50% maximum brightness Lists system suspend (e.g. sleep, hibernate) options and allows them to be activated Note this is a KRunner keyworddim screen Note this is a KRunner keywordhibernate Note this is a KRunner keywordscreen brightness Note this is a KRunner keywordsleep Note this is a KRunner keywordsuspend Note this is a KRunner keywordto disk Note this is a KRunner keywordto ram Note this is a KRunner keyword; %1 is a parameterdim screen %1 Note this is a KRunner keyword; %1 is a parameterscreen brightness %1 Set Brightness to %1 Suspend to Disk Suspend to RAM Suspends the system to RAM Suspends the system to disk Project-Id-Version: plasma_runner_powerdevil
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-26 03:33+0200
PO-Revision-Date: 2011-10-25 14:42+0300
Last-Translator: Lasse Liehu <lasse.liehu@gmail.com>
Language-Team: Finnish <lokalisointi@lists.coss.fi>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-POT-Import-Date: 2012-12-01 22:22:37+0000
X-Generator: MediaWiki 1.21alpha (963ddae); Translate 2012-11-08
 Himmennä näyttö puoliksi Himmennä näyttö kokonaan Luettelee näytön kirkkauden valinnat tai asettaa sen niin kuin :q: määrittää;esim. näytön kirkkaus 50 himmentäisi näytön 50 prosenttiin enimmäiskirkkaudesta Luettelee järjestelmän keskeytyksen (levylle, muistiin jne.) valinnat ja mahdollistaa niiden käyttöönoton himmennä näyttö lepotilaan näytön kirkkaus valmiustilaan keskeytä levylle muistiin näytön himmennys %1 näytön kirkkaus %1 Aseta kirkkaudeksi %1 Keskeytä levylle Keskeytä muistiin Keskeyttää järjestelmän muistiin Keskeyttää järjestelmän levylle 