��    3      �  G   L      h  6   i  M   �  K   �     :  '   C     k     r  �   �     ;  	   R  A   \  >   �  9   �  9     9   Q  W   �  E   �     )     :      @     a  7   ~      �     �     �  X   �     X	     `	     v	  �   �	     '
     F
     L
     c
  
   s
  
   ~
     �
     �
  E  �
          &     <  w   O     �     �     �     �     �  	          �  #  Q     e   X  c   �     "  %   )     O     T  �   e          2     @     O     W  	   ^  	   h  +   r     �     �     �  X   �  )   #  0   M  $   ~  '   �     �  F   �  	   *     4  "   G  �   j  =     
   T     _     |  
   �  
   �     �     �  8  �  $   �  &        2  �   J     �     �     �     �  "        3     ?         /   +   )               ,      .                 	          &                       %   #                       '       0       3                         -       1   2                        (   
      *   !         $                           "    "Full screen repaints" can cause performance problems. "Only when cheap" only prevents tearing for full screen changes like a video. "Re-use screen content" causes severe performance problems on MESA drivers. Accurate Allow applications to block compositing Always Animation speed: Applications can set a hint to block compositing when the window is open.
 This brings performance improvements for e.g. games.
 The setting can be overruled by window-specific rules. Author: %1
License: %2 Automatic Category of Desktop Effects, used as section headerAccessibility Category of Desktop Effects, used as section headerAppearance Category of Desktop Effects, used as section headerCandy Category of Desktop Effects, used as section headerFocus Category of Desktop Effects, used as section headerTools Category of Desktop Effects, used as section headerVirtual Desktop Switching Animation Category of Desktop Effects, used as section headerWindow Management Configure filter Crisp EMAIL OF TRANSLATORSYour emails Enable compositor on startup Exclude Desktop Effects not supported by the Compositor Exclude internal Desktop Effects Full screen repaints Get New Effects... Hint: To find out or configure how to activate an effect, look at the effect's settings. Instant KWin development team Keep window thumbnails: Keeping the window thumbnail always interferes with the minimized state of windows. This can result in windows not suspending their work when minimized. NAME OF TRANSLATORSYour names Never Only for Shown Windows Only when cheap OpenGL 2.0 OpenGL 3.1 OpenGL Platform InterfaceEGL OpenGL Platform InterfaceGLX OpenGL compositing (the default) has crashed KWin in the past.
This was most likely due to a driver bug.
If you think that you have meanwhile upgraded to a stable driver,
you can reset this protection but be aware that this might result in an immediate crash!
Alternatively, you might want to use the XRender backend instead. Re-enable OpenGL detection Re-use screen content Rendering backend: Scale method "Accurate" is not supported by all hardware and can cause performance regressions and rendering artifacts. Scale method: Search Smooth Smooth (slower) Tearing prevention ("vsync"): Very slow XRender Project-Id-Version: kcmkwincompositing
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2017-12-08 16:11+0200
Last-Translator: Tommi Nieminen <translator@legisign.org>
Language-Team: Finnish <kde-i18n-doc@kde.org>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-POT-Import-Date: 2012-12-01 22:22:22+0000
X-Generator: Lokalize 2.0
 ”Piirrettäessä koko näyttö uudelleen” voi aiheuttaa suorituskykyongelmia. ”Vain kun edullista” estää pirstoutumista, kun koko näytön kuva muuttuu esimerkiksi videossa. ”Käytä näytön sisältöä uudelleen” aiheuttaa vakavia suorituskykyongelmia MESA-ajureilla. Tarkka Salli sovellusten estää koostaminen Aina Animointinopeus: Sovellukset voivat vihjata estämään koostamisen ikkunan ollessa auki.
 Tämä parantaa suorituskykyä esim. peleissä.
 Asetuksen voi kiertää ikkunakohtaisella säännöllä. Tekijä: %1
Lisenssi: %2 Automaattinen Käytettävyys Ulkoasu Karkit Kohdistus Työkalut Virtuaalityöpöydän vaihtamisen animaatio Ikkunanhallinta Muokkaa suodatinta Terävä tpr@d5k.net, mikko.piippo@helsinki.fi, translator@legisign.org, karvonen.jorma@gmail.com Ota koostin käyttöön käynnistyksessä Ohita työpöytätehosteet, joita koostin ei tue Ohita sisäiset työpöytätehosteet Piirrettäessä koko näyttö uudelleen Hae uusia tehosteita… Vihje: tehosteen asetuksista löytyy ohje, kuinka tehoste aktivoidaan. Välitön KWin-kehitysryhmä Säilytä ikkunoiden pienoiskuvat: Ikkunoiden pienoiskuvien säilyttäminen aina häiritsee ikkunoiden pienennettyä tilaa. Tästä johtuen jotkin ikkunat eivät keskeytä työtään, kun ne pienennetään. Teemu Rytilahti, Mikko Piippo, Tommi Nieminen, Jorma Karvonen Ei koskaan Vain näytetyille ikkunoille Vain kun edullista OpenGL 2.0 OpenGL 3.1 EGL GLX OpenGL-koostaminen (oletus) on kaatanut KWinin.
Tämä johtui todennäköisesti ajurivirheestä.
Jos arvelet päivittäneesi sittemmin vakaaseen ajuriin,
voit nollata tämän suojauksen, mutta tiedä, että tästä saattaa seurata välitön kaatuminen!
Vaihtoehtoisesti voit käyttää XRender-koostamistyyppiä. Käytä OpenGL-tunnistusta uudestaan Käytä näytön sisältöä uudelleen Renderöinnin toteutus: Skaalausmenetelmää ”Tarkka” ei tueta kaikilla laitteistoilla, ja se saattaa aiheuttaa huonoa suorituskykyä sekä renderöintiartifakteja. Skaalausmenetelmä: Hae Tasainen Tasainen (hitaampi) Pirstoutumisen esto (”vsync”): Hyvin hidas XRender 