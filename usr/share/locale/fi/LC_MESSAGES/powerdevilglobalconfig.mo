��          �      l      �     �     �     �  *         +  >   ?  9   ~     �     �  
   �      �       	        (  !   :     \  $   {  	   �     �  �   �  �  8          "     :     P  "   c  2   �  0   �     �            H   ,     u     �     �  $   �  +   �  ;   �     4     <  �   Q                              
                                                  	                  % &Critical level: &Low level: <b>Battery Levels                     </b> A&t critical level: Battery will be considered critical when it reaches this level Battery will be considered low when it reaches this level Configure Notifications... Critical battery level Do nothing EMAIL OF TRANSLATORSYour emails Enabled Hibernate Low battery level Low level for peripheral devices: NAME OF TRANSLATORSYour names Pause media players when suspending: Shut down Suspend The Power Management Service appears not to be running.
This can be solved by starting or scheduling it inside "Startup and Shutdown" Project-Id-Version: powerdevilglobalconfig
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-11 03:05+0200
PO-Revision-Date: 2017-06-08 21:32+0200
Last-Translator: Tommi Nieminen <translator@legisign.org>
Language-Team: Finnish <kde-i18n-doc@kde.org>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-POT-Import-Date: 2012-12-01 22:22:38+0000
X-Generator: Lokalize 2.0
  % K&riittinen varaustaso: Alhainen &varaustaso: <b>Varaustasot</b> K&un akku on kriittisen vähissä: Varaus on kriittinen, kun se laskee tälle tasolle Varaus on alhainen, kun se laskee tälle tasolle Ilmoitusten asetukset… Kriittinen varaustaso Älä tee mitään karvonen.jorma@gmail.com, lasse.liehu@gmail.com, translator@legisign.org Käytössä Siirry lepotilaan Alhainen varaustaso Alhainen varaustaso oheislaitteille: Jorma Karvonen, Lasse Liehu, Tommi Nieminen Keskeytä mediasoittimet järjestelmää keskeytettäessä: Sammuta Siirry valmiustilaan Virranhallintapalvelu ei ole käynnissä.
Käynnistä tai ajasta se järjestelmäasetusten ”Käynnistys ja sammutus” -osiosta. 