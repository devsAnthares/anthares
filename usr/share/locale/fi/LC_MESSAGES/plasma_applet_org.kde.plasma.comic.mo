��    =        S   �      8  ;   9     u     �     �     �  6   �  A   
     L     U  $   Y  
   ~     �  #   �     �     �     �     �     �  7        >     [     w  '   �     �     �  $   �  ,   �          3     C     K     ]     y     �     �     �     �     �  �   �  9   �	  "   �	     �	     �	     
     *
     <
     R
     c
  %   u
     �
     �
     �
     �
     �
  
   �
  7        :     T     l     t  �  �  V   j     �     �     �          -     >     Z     i  &   p     �  #   �  "   �     �      �  	             3  -   @     n     �     �  /   �     �     �  .   �  1   *     \     c     k     t  #   �     �     �  $   �  !   �          '  �   3  M      *   N     y  ,   �      �     �     �          "     4     P     `          �  	   �  
   �  #   �     �     �  	   �     �     !          3      0      (                 1      "          =   .   6                                         	      :          5       ,   #   &   7       ;   '             -   
             8                 *           %           <   4   9                )   /   $          2             +       

Choose the previous strip to go to the last cached strip. &Create Comic Book Archive... &Save Comic As... &Strip Number: *.cbz|Comic Book Archive (Zip) @option:check Context menu of comic image&Actual Size @option:check Context menu of comic imageStore current &Position Advanced All An error happened for identifier %1. Appearance Archiving comic failed Automatically update comic plugins: Cache Check for new comic strips: Comic Comic cache: Configure... Could not create the archive at the specified location. Create %1 Comic Book Archive Creating Comic Book Archive Destination: Display error when getting comic failed Download Comics Error Handling Failed adding a file to the archive. Failed creating the file with identifier %1. From beginning to ... From end to ... General Get New Comics... Getting comic strip failed: Go to Strip Information Jump to &current Strip Jump to &first Strip Jump to Strip ... Manual range Maybe there is no Internet connection.
Maybe the comic plugin is broken.
Another reason might be that there is no comic for this day/number/string, so choosing a different one might work. Middle-click on the comic to show it at its original size No zip file is existing, aborting. Range: Show arrows only on mouse over Show comic URL Show comic author Show comic identifier Show comic title Strip identifier: The range of comic strips to archive. Update Visit the comic website Visit the shop &website an abbreviation for Number# %1 days dd.MM.yyyy here strip means comic strip&Next Tab with a new Strip in a range: from toFrom: in a range: from toTo: minutes strips per comic Project-Id-Version: plasma_applet_comic
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-07-18 03:20+0200
PO-Revision-Date: 2015-03-11 00:00+0200
Last-Translator: Lasse Liehu <lasse.liehu@gmail.com>
Language-Team: Finnish <lokalisointi@lists.coss.fi>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-POT-Import-Date: 2012-12-01 22:25:23+0000
X-Generator: Lokalize 2.0
 

Valitse edellinen strippi siirtyäksesi viimeiseen välimuistissa olevaan strippiin. &Luo sarjakuva-arkisto… &Tallenna sarjakuva nimellä… &Sarjakuvan numero: *.cbz|Sarjakuva-arkisto (Zip) &Todellinen koko Tallenna nykyinen &sijainti Lisäasetukset Kaikki Tapahtui virhe tunnisteen %1 kohdalla. Ulkoasu Sarjakuvan arkistointi epäonnistui Päivitä sarjakuvaliitännäiset: Välimuisti Tarkista uudet sarjakuvastripit: Sarjakuva Sarjakuvavälimuisti: Asetukset… Arkistoa ei voitu luoda annettuun sijaintiin. Luo %1-sarjakuva-arkisto Sarjakuva-arkiston luonti Kohde: Näytä virhe, kun sarjakuvan haku epäonnistui Lataa sarjakuvia Virheenkäsittely Tiedoston lisääminen arkistoon epäonnistui. Virhe luotaessa tunnisteella %1 olevaa tiedostoa. Alusta Lopusta Yleistä Hae uusia sarjakuvia… Sarjakuvastripin haku epäonnistui: Siirry sarjakuvaan Tiedot &Siirry tämänhetkiseen sarjakuvaan &Siirry ensimmäiseen sarjakuvaan Siirry sarjakuvaan… Muu laajuus Ehkä Internet-yhteyttä ei ole.
Ehkä sarjakuvaliitännäinen on rikki.
Voi myös olla, että tälle päivälle/numerolle/merkkijonolle ei ole sarjakuvaa, joten toisen sarjakuvan valitseminen voi auttaa. Näytä sarjakuva alkuperäiskoossaan napsauttamalla hiiren keskipainikkeella Zip-tiedostoa ei olemassa, keskeytetään. Laajuus: Näytä nuolet vain hiiren ollessa päällä Näytä sarjakuvan verkko-osoite Näytä sarjakuvan tekijä Näytä sarjakuvan tunniste Näytä sarjakuvan otsikko Stripin tunniste: Sarjakuva-arkiston laajuus. Päivittäminen Vieraile sarjakuvan sivustolla Vieraile &kaupan sivustolla # %1 päivää dd.MM.yyyy &Siirry seuraavaan uuteen strippiin Alkaen: Asti: minuuttia strippiä per sarjakuva 