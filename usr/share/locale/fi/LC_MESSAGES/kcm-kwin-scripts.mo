��          �      �       0  (   1     Z      q     �     �     �     �     �     �  `     ^   u     �  �  �  )   �     �  2        9     Q     b     v     �      �  #   �  '   �     	        
              	                                    *.kwinscript|KWin scripts (*.kwinscript) Configure KWin scripts EMAIL OF TRANSLATORSYour emails Get New Scripts... Import KWin Script Import KWin script... KWin Scripts KWin script configuration NAME OF TRANSLATORSYour names Placeholder is error message returned from the install serviceCannot import selected script.
%1 Placeholder is name of the script that was importedThe script "%1" was successfully imported. Tamás Krutki Project-Id-Version: kcm-kwin-scripts
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-19 03:12+0100
PO-Revision-Date: 2017-12-08 15:58+0200
Last-Translator: Tommi Nieminen <translator@legisign.org>
Language-Team: Finnish <kde-i18n-doc@kde.org>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2012-12-01 22:22:19+0000
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=(n != 1);
 *.kwinscript|KWin-skriptit (*.kwinscript) KWin-skriptien asetukset niklas.laxstrom@gmail.com, translator@legisign.org Hae uusia skriptejä… Tuo KWin-skripti Tuo KWin-skripti… KWin-skriptit KWinin skriptiasetukset Niklas Laxström, Tommi Nieminen Valittua skriptiä ei voi tuoda.
%1 Skripti ”%1” tuotiin onnistuneesti. Tamás Krutki 