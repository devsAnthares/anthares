��            )         �     �  "   �     �  !   �  !        4  
   J     U     p     �  !   �     �  0   �  8        ?  !   ]          �     �     �     �                '  
   /  
   :  
   E  &   P     w     �  �  �     8  #   <     `     }     �     �  
   �     �     �     �            '   3  L   [  *   �  0   �  '   	  &   ,	  $   S	  )   x	     �	  '   �	     �	     �	     �	     �	     
     
     0
     A
                                                                                                 	                       
                 ms &Keyboard accelerators visibility: &Top arrow button type: Always Hide Keyboard Accelerators Always Show Keyboard Accelerators Anima&tions duration: Animations Bottom arrow button t&ype: Breeze Settings Center tabbar tabs Drag windows from all empty areas Drag windows from titlebar only Drag windows from titlebar, menubar and toolbars Draw a thin line to indicate focus in menus and menubars Draw focus indicator in lists Draw frame around dockable panels Draw frame around page titles Draw frame around side panels Draw slider tick marks Draw toolbar item separators Enable animations Enable extended resize handles Frames General No Buttons One Button Scrollbars Show Keyboard Accelerators When Needed Two Buttons W&indows' drag mode: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:42+0200
PO-Revision-Date: 2017-12-03 21:03+0900
Last-Translator: Lasse Liehu <lasse.liehu@gmail.com>
Language-Team: Finnish <kde-i18n-doc@kde.org>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
  ms &Valintanäppäinten näyttäminen: &Yläpään nuolipainikkeet: Piilota aina Näytä aina A&nimointien kesto: Animoinnit &Alapään nuolipainikkeet: Breezen asetukset Keskitä välilehdet palkkiin Kaikilta tyhjiltä alueilta Vain otsikkopalkista Otsikko-, valikko- ja työkaluriveiltä Piirrä ohut viiva, joka ilmaisee kohdistusta valikoissa ja valikkoriveissä Piirrä luetteloihin kohdistuksen ilmaisin Piirrä kehys telakoitavien paneelien ympärille Piirrä kehys sivuotsikoiden ympärille Piirrä kehys sivupaneelien ympärille Piirrä liukusäädinten askelmerkit Piirrä työkalurivin kohteiden erottimet Käytä animointeja Käytä laajennettuja koonmuutoskahvoja Kehykset Yleistä Ei painikkeita Yksi painike Vierityspalkit Näytä tarvittaessa Kaksi painiketta Mistä &ikkunoita voi vetää: 