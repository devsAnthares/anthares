��    7      �  I   �      �     �     �     �     �     �     �               $     -     5     H     T      `     �     �     �     �     �     �     �     �                     )     7  	   C  	   M     W     d     j     �     �     �     �     �     �     �     �     �     �  @   �     7     @     \     c     j     r     �     �     �  4   �     �  �  �     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
  	   
      
     6
     >
     N
     j
     v
     �
     �
     �
     �
     �
     �
     �
          !     9  
   E     P     V     b     e     u     }     �  	   �     �     �     �     �  J   �  	          
   >     I     K  #   T     x     z     }     �     �     &   0      7                  #                
          	   '   (   +                       6              3      1                   ,   2                  "       *          $      4   )               %      5                     !          /   .   -       % %1 is value, %2 is unit%1 %2 %1: Application Energy Consumption Battery Capacity Charge Percentage Charge state Charging Current Degree Celsius°C Details: %1 Discharging EMAIL OF TRANSLATORSYour emails Energy Energy Consumption Energy Consumption Statistics Environment Full design Fully charged Has power supply Kai Uwe Broulik Last 12 hours Last 2 hours Last 24 hours Last 48 hours Last 7 days Last full Last hour Manufacturer Model NAME OF TRANSLATORSYour names No Not charging PID: %1 Path: %1 Rechargeable Refresh Serial Number Shorthand for WattsW System Temperature This type of history is currently not available for this device. Timespan Timespan of data to display Vendor VoltV Voltage Wakeups per second: %1 (%2%) WattW Watt-hoursWh Yes current power draw from the battery in WConsumption literal percent sign% Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2016-06-25 13:19+0200
Last-Translator: Lasse Liehu <lasse.liehu@gmail.com>
Language-Team: Finnish <kde-i18n-doc@kde.org>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 % %1 %2 %1: Sovellusten energiankulutus Akku Kapasiteetti Varausprosentti Latauksen tila Latautuu Nykyinen °C Yksityiskohdat: %1 Purkautuu lasse.liehu@gmail.com Energia Energiankulutus Energiankulutuksen tilastot Ympäristö Suunniteltu kapasiteetti Täydessä latauksessa Sisältää virtalähteen Kai Uwe Broulik Viime 12 tuntia Viime 2 tuntia Viime 24 tuntia Viime 48 tuntia Viime 7 päivää Viimeisin täysi lataus Viime tunti Valmistaja Malli Lasse Liehu Ei Ei latautumassa PID: %1 Sijainti: %1 Ladattavissa Päivitä Sarjanumero W Järjestelmä Lämpötila Tämäntyyppistä historiaa ei ole juuri nyt saatavilla tälle laitteelle. Aikaväli Näytettävän tiedon aikaväli Toimittaja V Jännite Herätyksiä sekunnissa: %1 (%2 %) W Wh Kyllä Kulutus % 