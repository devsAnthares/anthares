��          �      <      �     �     �     �  +   �  @        L     a     i     w     }     �  
   �     �     �     �     �     �  �  
     �     �     �  9   �  A        X     g     o          �     �     �     �     �     �     �          
         	                                                                                       <a href='%1'>%1</a> Close Copy Automatically: Don't show this dialog, copy automatically. Drop text or an image onto me to upload it to an online service. Error during upload. General History Size: Paste Please wait Please, try again. Sending... Share Shares for '%1' Successfully uploaded The URL was just shared Upload %1 to an online service Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-01 04:04+0100
PO-Revision-Date: 2015-05-16 18:01+0200
Last-Translator: Lasse Liehu <lasse.liehu@gmail.com>
Language-Team: Finnish <kde-i18n-doc@kde.org>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 1.5
 <a href='%1'>%1</a> Sulje Kopioi automaattisesti: Älä näytä tätä ikkunaa vaan kopioi automaattisesti. Lähetä teksti tai kuva verkkopalveluun pudottamalla se tähän. Lähetysvirhe. Yleiset Historian koko: Liitä Odota hetki Yritä uudelleen. Lähetetään… Jaa Jaot tyypille ”%1” Lähetetty onnistuneesti Verkko-osoite jaettiin juuri Lähetä %1 verkkopalveluun 