��            )   �      �     �     �     �     �     �     �     �     �  0        3     G     ]     c     o     w     �  
   �     �     �     �     �     �               (     A     I     a     m  �  s     Z     i     �     �     �     �  
   �     �      �     �               "  	   0     :     N  	   ]     g          �     �     �  	   �     �     �     
          )  
   8                                            
                                                                                      	       %1 by %2 Add Custom Wallpaper Add Folder... Add Image... Background: Blur Centered Change every: Directory with the wallpaper to show slides from Download Wallpapers Get New Wallpapers... Hours Image Files Minutes Next Wallpaper Image Open Containing Folder Open Image Open Wallpaper Image Positioning: Recommended wallpaper file Remove wallpaper Restore wallpaper Scaled Scaled and Cropped Scaled, Keep Proportions Seconds Select Background Color Solid color Tiled Project-Id-Version: plasma_wallpaper_image
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:39+0100
PO-Revision-Date: 2017-12-08 16:24+0200
Last-Translator: Tommi Nieminen <translator@legisign.org>
Language-Team: Finnish <kde-i18n-doc@kde.org>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-POT-Import-Date: 2012-12-01 22:22:37+0000
X-Generator: Lokalize 2.0
 %1, tekijä %2 Lisää oma taustakuva Lisää kansio… Lisää kuva… Tausta: Sumenna Keskitetty Kuvanvaihtoväli: Kansio, josta kuvia näytetään Lataa taustakuvia Hae uusia taustakuvia… tuntia Kuvatiedostot minuuttia Seuraava taustakuva Avaa emokansio Avaa kuva Avaa taustakuvatiedosto Sijoittelu: Suositeltu taustakuvatiedosto Poista taustakuva Palauta taustakuva Skaalattu Skaalattu ja rajattu Skaalattu, säilytä suhteet sekuntia Valitse taustan väri Tasainen väri Monistettu 