��    ]           �      �  !   �       	   !     +  S   4  	   �     �     �     �     �     �     �     �     �     �     	  J   $	     o	     }	     �	      �	  	   �	  
   �	     �	     �	     �	     �	     
     
     
  L   
  	   l
  	   v
  
   �
  
   �
  
   �
     �
     �
     �
     �
     �
     �
     �
     �
     	     (  	   +     5     G     S     [     i     m     }     �     �  
   �  	   �     �  
   �     �     �     �     �     �     	          +     ?     \     r     x     |     �  H   �     �     �     �     �     �       
     &        A  "   T     w     �     �     �     �       	     �  (  *   �     )  
   >  	   I     S     l     q  	        �     �     �     �     �     �     �     �  7   �     '     5  	   >  H   H     �     �     �     �     �  
   �               "  '   +  
   S  
   ^     i     u     �     �     �     �     �     �     �     �     �  +        =     @     L     a     m     y     �     �     �     �     �     �     �     �     �     �                     1     Q     k     {     �     �     �     �     �     �  =   �          !     1     >     Q     d  
   k  
   v  
   �     �     �  
   �  
   �  
   �  
   �  
   �     �           B   "       (   W   #   $   %            [   @      L   M   '   6       >   T           <   9   G   &           A      .   8   R       7       J             ?   4                   H   -                      F          5   )   N   D       ,       K             3       U   =   
       ;       O   :           +           !   Z   /      V   Y              Q         E       1       X             ]       0      P         \   I          	          C       S   2       *    
Solid Based Device Viewer Module (c) 2010 David Hubner AMD 3DNow ATI IVEC Available space out of total partition size (percent used)%1 free of %2 (%3% used) Batteries Battery Type:  Bus:  Camera Cameras Charge Status:  Charging Collapse All Compact Flash Reader Default device tooltipA Device Device Information Device Listing Whats ThisShows all the devices that are currently listed. Device Viewer Devices Discharging EMAIL OF TRANSLATORSYour emails Encrypted Expand All File System File System Type:  Fully Charged Hard Disk Drive Hotpluggable? IDE IEEE1394 Info Panel Whats ThisShows information about the currently selected device. Intel MMX Intel SSE Intel SSE2 Intel SSE3 Intel SSE4 Keyboard Keyboard + Mouse Label:  Max Speed:  Memory Stick Reader Mounted At:  Mouse Multimedia Players NAME OF TRANSLATORSYour names No No Charge No data available Not Mounted Not Set Optical Drive PDA Partition Table Primary Processor %1 Processor Number:  Processors Product:  Raid Removable? SATA SCSI SD/MMC Reader Show All Devices Show Relevant Devices Smart Media Reader Storage Drives Supported Drivers:  Supported Instruction Sets:  Supported Protocols:  UDI:  UPS USB UUID:  Udi Whats ThisShows the current device's UDI (Unique Device Identifier) Unknown Drive Unused Vendor:  Volume Space: Volume Usage:  Yes kcmdevinfo name of something is not knownUnknown no device UDINone no instruction set extensionsNone platform storage busPlatform unknown battery typeUnknown unknown deviceUnknown unknown device typeUnknown unknown storage busUnknown unknown volume usageUnknown xD Reader Project-Id-Version: kcmdevinfo
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-23 03:11+0200
PO-Revision-Date: 2016-03-10 01:04+0200
Last-Translator: Lasse Liehu <lasse.liehu@gmail.com>
Language-Team: Finnish <kde-i18n-doc@kde.org>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-POT-Import-Date: 2012-12-01 22:22:21+0000
X-Generator: Lokalize 2.0
 
Solid-perustainen laitteenkatselinmoduuli © 2010 David Hubner AMD 3DNow  ATI IVEC  %1/%2 (%3 % käytössä) Akut Akun tyyppi:  Väylä:  Kamera Kamerat Latauksen tila:  Latautuu Supista kaikki Compact Flash -kortinlukija Laite Laitetiedot Näyttää kaikki tällä hetkellä luetellut laitteet. Laitekatselin Laitteet Purkautuu translator@legisign.org, lasse.liehu@gmail.com, karvonen.jorma@gmail.com Salattu Laajenna kaikki Tiedostojärjestelmä Tiedostojärjestelmän tyyppi:  Täydessä latauksessa Kiintolevy Ajonaikaisesti kytkettävä? IDE IEEE1394 Näyttää tietoa valitusta laitteesta. Intel MMX  Intel SSE  Intel SSE2  Intel SSE3  Intel SSE4  Näppäimistö Näppäimistö ja hiiri Nimiö:  Enimmäisnopeus:  Muistitikunlukija Liitettynä kansioon: Hiiri Multimediasoittimet Tommi Nieminen, Lasse Liehu, Jorma Karvonen Ei Ei latausta Tietoa ei saatavilla Ei liitetty Ei asetettu Optinen asema Kämmentietokone Osiotaulukko Ensisijainen Suoritin %1 Suorittimen numero:  Suorittimet Tuote:  RAID Siirrettävä? SATA SCSI SD/MMC-kortinlukija Näytä kaikki laitteet Näytä asiaankuuluvat laitteet Smart Media -kortinlukija Tallennusasemat Tuetut ajurit:  Tuetut käskykannat:  Tuetut yhteyskäytännöt:  UDI:  UPS USB UUID:  Näytä nykyisen laitteen UDIn (ainutkertainen laitetunniste) Tuntematon laite Käyttämätön Valmistaja:  Taltion käyttö:  Taltion käyttö:  Kyllä kcmdevinfo Tuntematon Ei UID:tä Ei mitään Alusta Tuntematon Tuntematon Tuntematon Tuntematon Tuntematon xD-kortinlukija 