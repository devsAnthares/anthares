��          �      �           	               4  	   I     S      c  "   �      �     �     �  	   �     �               )  
   9     D     S     a     g     {  �  �     )     ,  %   4     Z     k     y     �     �      �  
   �     �  
   �                    2  
   @     K     _     s     x  	   �                              	                                     
                                      K (HH:MM) (In minutes - min. 1, max. 600) Activate Night Color Automatic Detect location EMAIL OF TRANSLATORSYour emails Error: Morning not before evening. Error: Transition time overlaps. Latitude Location Longitude NAME OF TRANSLATORSYour names Night Color Night Color Temperature:  Operation mode: Roman Gilg Sunrise begins Sunset begins Times Transition duration and ends Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-12 03:09+0100
PO-Revision-Date: 2017-12-12 18:06+0200
Last-Translator: Tommi Nieminen <translator@legisign.org>
Language-Team: Finnish <kde-i18n-doc@kde.org>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
  K (TT:MM) (minuutteina – väh. 1, enint. 600) Aktivoi yöväri Automaattinen Tunnista sijainti translator@legisign.org Virhe: Aamu ei ole ennen iltaa. Virhe: Siirtymäajat lomittuvat. Leveysaste Sijainti Pituusaste Tommi Nieminen Yöväri Yövärin lämpötila: Toimintatila: Roman Gilg Auringonnousu alkaa Auringonnousu alkaa Ajat Siirtymän kesto ja loppuu 