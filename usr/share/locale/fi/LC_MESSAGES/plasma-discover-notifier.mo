��          t      �         P     )   b  %   �  @   �     �  V   	     `     {     �     �  �  �     K  7   R  3   �  7   �     �  G     $   \     �  	   �     �                         	      
                             %1 is '%1 packages to update' and %2 is 'of which %1 is security updates'%1, %2 1 package to update %1 packages to update 1 security update %1 security updates First part of '%1, %2'1 package to update %1 packages to update No packages to update Second part of '%1, %2'of which 1 is security update of which %1 are security updates Security updates available System up to date Update Updates available Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-15 03:15+0100
PO-Revision-Date: 2018-01-22 17:36+0200
Last-Translator: Tommi Nieminen <translator@legisign.org>
Language-Team: Finnish <kde-i18n-doc@kde.org>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 %1, %2 1 päivitettävä paketti %1 päivitettävää pakettia 1 turvallisuuspäivitys %1 turvallisuuspäivitystä 1 päivitettävä paketti %1 päivitettävää pakettia Ei päivitettäviä paketteja joista 1 on turvallisuuspäivitys joista %1 on turvallisuuspäivitystä Turvallisuuspäivityksiä saatavilla Järjestelmä on ajan tasalla Päivitä Päivityksiä saatavilla 