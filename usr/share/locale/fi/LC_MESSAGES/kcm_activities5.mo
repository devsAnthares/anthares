��    *      l  ;   �      �     �     �  )   �               /     H     ^  #   ~     �  
   �     �     �  %   �  +        E     Z     m  @   z     �     �     �     �               '     ,     9     ?     _     e  .   m     �  F   �  (   �  	   '  	   1  	   ;  '   E  7   m  "   �  �  �     h	     �	  7   �	     �	     �	     �	     �	     �	     �	     
     )
     6
     J
  0   `
  1   �
      �
     �
     �
  B         C     c     x     �     �     �     �     �     �  &   �       
   
  ;        Q  `   j  2   �     �                      	   3                   $                                 %                      *                                               	   #   )   "             &           
                        '             (   !    &Do not remember @actionWalk through activities @actionWalk through activities (Reverse) @action:buttonApply @action:buttonCancel @action:buttonChange... @action:buttonCreate @title:windowActivity Settings @title:windowCreate a New Activity @title:windowDelete Activity Activities Activity information Activity switching Are you sure you want to delete '%1'? Blacklist all applications not on this list Clear recent history Create activity... Description: Error loading the QML files. Check your installation.
Missing %1 For a&ll applications Forget a day Forget everything Forget the last hour Forget the last two hours General Icon Keep history Name: O&nly for specific applications Other Privacy Private - do not track usage for this activity Remember opened documents: Remember the current virtual desktop for each activity (needs restart) Shortcut for switching to this activity: Shortcuts Switching Wallpaper for in 'keep history for 5 months'for  unit of time. months to keep the history month  months unlimited number of monthsforever Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-04-04 19:42+0200
Last-Translator: Lasse Liehu <lasse.liehu@gmail.com>
Language-Team: Finnish <kde-i18n-doc@kde.org>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 &Älä muista tiedostoja Selaa aktiviteetteja Selaa aktiviteetteja (käänteisessä järjestyksessä) Käytä Peru Muuta… Luo Aktiviteetin asetukset Luo uusi aktiviteetti Poista aktiviteetti Aktiviteetit Aktiviteetin tiedot Aktiviteettien vaihto Haluatko varmasti poistaa aktiviteetin ”%1”? Estä kaikki tässä mainitsemattomat sovellukset Tyhjennä viimeaikainen historia Luo aktiviteetti… Kuvaus: Virhe ladattaessa QML-tiedostoja. Tarkista asennuksesi.
%1 puuttuu &Kaikkien sovellusten tiedostot Unohda viime päivä Unohda kaikki Unohda viime tunti Unohda viime kaksi tuntia Yleistä Kuvake Säilytä historiaa Nimi: &Vain valittujen sovellusten tiedostot Muuta Tietosuoja Yksityinen – älä seuraa tämän aktiviteetin käyttöä Muista avatut tiedostot: Muista nykyinen virtuaalityöpöytä kullekin aktiviteetille (tarvitsee uudelleenkäynnistyksen) Pikanäppäin tähän aktiviteettiin vaihtamiseen: Pikanäppäimet Vaihto Tausta    kuukausi  kuukautta ikuisesti 