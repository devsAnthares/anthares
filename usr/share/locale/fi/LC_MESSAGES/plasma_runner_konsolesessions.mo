��          4      L       `   $   a   /   �     �   3   �  0   �                    Finds Konsole sessions matching :q:. Lists all the Konsole sessions in your account. Project-Id-Version: plasma_runner_konsolesessions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2011-01-14 22:21+0200
Last-Translator: Lasse Liehu <lliehu@kolumbus.fi>
Language-Team: Finnish <lokalisointi@lists.coss.fi>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-POT-Import-Date: 2012-12-01 22:25:27+0000
X-Generator: MediaWiki 1.21alpha (963ddae); Translate 2012-11-08
 Etsii Konsole-istuntoja, jotka vastaavat hakua :q:. Luettelee kaikki käyttäjäsi Konsole-istunnot. 