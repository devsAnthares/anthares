��    
      l      �       �   
   �      �        .   0     _     t     �  (   �  8   �  �       �  !   �     �  2   �  $   )     N  
   T     _     {        
                             	        Disk Quota No quota restrictions found. Please install 'quota' Quota tool not found.

Please install 'quota'. Running quota failed e.g.: 12 GiB of 20 GiB%1 of %2 e.g.: 8 GiB free%1 free example: Quota: 83% usedQuota: %1% used usage of quota, e.g.: '/home/bla: 38% used'%1: %2% used Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2016-11-22 23:27+0200
Last-Translator: Tommi Nieminen <translator@legisign.org>
Language-Team: Finnish <kde-i18n-doc@kde.org>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 1.5
 Levykiintiö Kiintiörajoituksia ei löytynyt. Asenna ”quota” Quota-työkalua ei löytynyt.

Asenna ”quota”. quota-komennon suoritus epäonnistui %1/%2 %1 vapaana Kiintiö: %1 % käytössä %1: %2 % käytössä 