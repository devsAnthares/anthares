��    `        �         (     )     1     B     Q     W     ^     j     {     �     �  /   �  -   �     �     �  
   		  
   	     	     ,	     1	     8	     @	     R	     _	     d	  
   l	     w	     �	     �	     �	  	   �	     �	  	   �	     �	     �	     �	     
     
  	   #
     -
     4
     H
  	   M
     W
     ]
     c
     h
     m
  	   v
     �
     �
     �
     �
     �
     �
     �
     �
  <   �
               /     6     =     X     ^     e     j  
   ~     �     �     �     �     �  )   �               5     :     @     M     U     ]     f  
   x     �     �  	   �     �     �     �     �     �     �     �  E   �  *   A  �  l               .     C     I     Q     _     v     �     �  
   �     �     �     �  
   �  
   �     �     �     �               '     3     8  
   ?     J     Q     h     ~     �     �     �     �     �     �            
        )     0     P     X  	   i     s  
   y     �     �  	   �     �  	   �     �     �     �     �     �     �  H        Z     s     �     �     �     �     �     �     �     �     �     �     �          8  3   J     ~  "   �     �     �     �     �  	   �     �     �               )     2  	   B     L  %   S     y     �     �     �  O   �          J       -      M       %   Q           ^              U       L   S   $      .       H       X          Y   8   *         <   &          '       ]   N       3                     `          #   V             /   T   G   9   @   >   =   6   2   O   C   F   ?      0      
      A   E       	      P              _                D          Z               1   R   ,   +                \           I   )   B   K      :   !   W   4          ;               (      5   "       7       [           &Delete &Empty Trash Bin &Move to Trash &Open &Paste &Properties &Refresh Desktop &Refresh View &Reload &Rename @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Align Appearance: Arrange In Arrange in Arrangement: Back Cancel Columns Configure Desktop Custom title Date Default Descending Description Deselect All Desktop Layout Enter custom title here Features: File name pattern: File type File types: Filter Folder preview popups Folders First Folders first Full path Got it Hide Files Matching Huge Icon Size Icons Large Left List Location Location: Lock in place Locked Medium More Preview Options... Name None OK Panel button: Press and hold widgets to move them and reveal their handles Preview Plugins Preview thumbnails Remove Resize Restore from trashRestore Right Rotate Rows Search file type... Select All Select Folder Selection markers Show All Files Show Files Matching Show a place: Show files linked to the current activity Show the Desktop folder Show the desktop toolbox Size Small Small Medium Sort By Sort by Sorting: Specify a folder: Text lines Tiny Title: Tool tips Tweaks Type Type a path or a URL here Unsorted Use a custom icon Widget Handling Widgets unlocked You can press and hold widgets to move them and reveal their handles. whether to use icon or list viewView mode Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:11+0100
PO-Revision-Date: 2017-12-08 16:23+0200
Last-Translator: Tommi Nieminen <translator@legisign.org>
Language-Team: Finnish <kde-i18n-doc@kde.org>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 &Poista &Tyhjennä roskakori Siirrä &roskakoriin &Avaa &Liitä &Ominaisuudet &Päivitä työpöytä &Päivitä näkymä &Lataa uudelleen &Muuta nimeä Valitse… Tyhjennä kuvake Tasaa Ulkoasu Sijoittele Sijoittele Sijoittelu: Takaisin Peru Sarakkeisiin Työpöydän asetukset Oma otsikko Aika Oletus Laskevasti Kuvaus Poista kaikki valinnat Työpöydän asettelu Kirjoita oma otsikko tähän Ominaisuudet Tiedostonimikuvio: Tiedoston tyyppi Tiedostotyypit: Suodatin Kansioiden esikatseluikkunat Kansiot ensin Kansiot ensin Koko polku Selvä Piilota täsmäävät tiedostot Valtava Kuvakkeiden koko Kuvakkeet Suuri Vasemmalle Luettelo Sijainti Sijainti: Lukitse paikoilleen Lukittuna Keskikokoinen Lisää esikatseluvalintoja… Nimi Ei otsikkoa OK Paneelin painike: Siirrä sovelmia ja paljasta niiden kahvat painamalla sovelmia pitkään Esikatseluliitännäiset Esikatselukuvat Poista Muuta kokoa Palauta Oikealle Kierrä Riveihin Etsi tiedostotyyppiä… Valitse kaikki Valitse kansio Valinnan merkit Näytä kaikki tiedostot Näytä täsmäävät tiedostot Näytä sijainti: Näytä nykyiseen aktiviteettiin liitetyt tiedostot Näytä työpöytäkansio Näytä työpöydän työkalupakki Koko Pieni Melko pieni Lajitteluperuste Lajittele Lajitteluperuste: Valitse kansio: Tekstirivejä Pikkuruinen Otsikko: Työkaluvihjeet Säädöt Tyyppi Kirjoita polku tai URL-osoite tähän Lajittelematon Käytä omaa kuvaketta Sovelmien käsittely Sovelmien lukitus avattu Voit liikuttaa sovelmia ja paljastaa niiden kahvat painamalla niitä pitkään. Näkymä 