��          �      |      �     �  2        B     b       #   �      �     �  %   �       
   &     1     >     ]  �   }       ]   (     �  p   �  :        P  �  \       4     .   N  "   }     �     �  .   �       )        F     V     d     q  2   �  �   �     y	  O   �	  #   �	  i    
  M   j
     �
                                                                      
                	           Apply a look and feel package Command line tool to apply look and feel packages. Configure Look and Feel details Copyright 2017, Marco Martin Cursor Settings Changed Download New Look And Feel Packages EMAIL OF TRANSLATORSYour emails Get New Looks... List available Look and feel packages Look and feel tool Maintainer Marco Martin NAME OF TRANSLATORSYour names Reset the Plasma Desktop layout Select an overall theme for your workspace (including plasma theme, color scheme, mouse cursor, window and desktop switcher, splash screen, lock screen etc.) Show Preview This module lets you configure the look of the whole workspace with some ready to go presets. Use Desktop Layout from theme Warning: your Plasma Desktop layout will be lost and reset to the default layout provided by the selected theme. You have to restart KDE for cursor changes to take effect. packagename Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-27 05:46+0100
PO-Revision-Date: 2017-12-26 09:54+0200
Last-Translator: Tommi Nieminen <translator@legisign.org>
Language-Team: Finnish <kde-i18n-doc@kde.org>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 Käytä ulkoasupakettia Komentorivityökalu ulkoasupakettien käyttämiseen. Ulkoasun ja tuntuman yksityiskohtien asetukset Tekijänoikeudet 2017 Marco Martin Osoitinasetuksia muutettiin Lataa uusia ulkoasupaketteja lasse.liehu@gmail.com, translator@legisign.org Hae uusi ulkoasu… Luettele saatavilla olevat ulkoasupaketit Ulkoasutyökalu Ylläpitäjä Marco Martin Lasse Liehu, Tommi Nieminen Palauta Plasma-työpöydän alkuperäinen asettelu Valitse työtilan ulkoasun ja käyttötuntuman yhtenäisteema (sisältää Plasma- ja väriteeman, hiiriosoittimet, ikkuna- ja työpöytävaihtimen, tervetulo- ja lukitusnäytöt jne.) Näytä esikatselu Tästä voit vaihtaa koko työtilan ulkoasun valitsemalla valmiista paketeista. Käytä teeman työpöytäasettelua Varoitus: Plasma-työpöytäsi asettelu menetetään ja palautetaan valitun teeman tarjoamaan oletukseen. KDE on käynnistettävä uudestaan, jotta osoitinmuutokset tulisivat voimaan. paketinnimi 