��    	      d      �       �      �   -   �   *   %      P  '   q  
   �     �     �  �  �     �  .   �  "   �  H        d     �     �  +   �                   	                           (c) 2009 Marco Martin Display informational tooltips on mouse hover Display visual feedback for status changes EMAIL OF TRANSLATORSYour emails Global options for the Plasma Workspace Maintainer Marco Martin NAME OF TRANSLATORSYour names Project-Id-Version: kcmworkspaceoptions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-08 06:27+0100
PO-Revision-Date: 2017-12-08 16:11+0200
Last-Translator: Tommi Nieminen <translator@legisign.org>
Language-Team: Finnish <kde-i18n-doc@kde.org>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-POT-Import-Date: 2012-12-01 22:22:24+0000
X-Generator: Lokalize 2.0
 © 2009 Marco Martin Näytä työkaluvihjeet hiiren leijuessa yllä Näkyvä palaute tilan muutoksista karvonen.jorma@gmail.com, lasse.liehu@gmail.com, translator@legisign.org Plasma-työtilan yleisasetukset Ylläpitäjä Marco Martin Jorma Karvonen, Lasse Liehu, Tommi Nieminen 