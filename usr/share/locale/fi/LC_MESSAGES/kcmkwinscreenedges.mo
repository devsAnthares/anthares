��    %      D  5   l      @     A     E     G  	   Y     c     |     �     �     �     �     �     �            S   ,  w   �     �  M         [     |  ?   �     �  	   �     �     
     #  %   2     X     e  F   �  #   �     �  ]     R   f     �     �  �  �     �	     �	     �	     �	     �	     
     !
     2
     A
  (   O
     x
  $   �
     �
     �
  X   �
  j   0     �  >   �  F   �     5  5   F  +   |     �     �     �     �  \   �     P     d  O   s  2   �  #   �  d     E        �     �               !      "          #   %                                                                            $                          	                
                            ms % %1 - All Desktops %1 - Cube %1 - Current Application %1 - Current Desktop %1 - Cylinder %1 - Sphere &Reactivation delay: &Switch desktop on edge: Activation &delay: Active Screen Corners and Edges Activity Manager Always Enabled Amount of time required after triggering an action until the next trigger can occur Amount of time required for the mouse cursor to be pushed against the edge of the screen before the action is triggered Application Launcher Change desktop when the mouse cursor is pushed against the edge of the screen EMAIL OF TRANSLATORSYour emails Lock Screen Maximize windows by dragging them to the top edge of the screen NAME OF TRANSLATORSYour names No Action Only When Moving Windows Open krunnerRun Command Other Settings Quarter tiling triggered in the outer Show Desktop Switch desktop on edgeDisabled Tile windows by dragging them to the left or right edges of the screen Toggle alternative window switching Toggle window switching Trigger an action by pushing the mouse cursor against the corresponding screen edge or corner Trigger an action by swiping from the screen edge towards the center of the screen Window Management of the screen Project-Id-Version: kcmkwinscreenedges
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-30 03:13+0100
PO-Revision-Date: 2018-01-22 17:28+0200
Last-Translator: Tommi Nieminen <translator@legisign.org>
Language-Team: Finnish <kde-i18n-doc@kde.org>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-POT-Import-Date: 2012-12-01 22:22:23+0000
X-Generator: Lokalize 2.0
  ms  % %1 – Kaikki työpöydät %1 – Kuutio %1 – Nykyinen sovellus %1 – Nykyinen työpöytä %1 – Sylinteri %1 – Lieriö &Toistoviive: &Vaihda työpöytää näytön reunalla: Toiminnon &viive: Aktiiviset näytön kulmat ja reunat Aktiviteettienhallinta Aina käytössä Aika, jonka kuluttua toiminnon käynnistämisestä voidaan käynnistää toinen toiminto Aika, jonka verran hiiriosoitinta on työnnettävä näytön reunaan, ennen kuin toiminto käynnistetään Sovelluskäynnistin Vaihda työpöytää viemällä hiiriosoitin näytön reunalle translator@legisign.org,karvonen.jorma@gmail.com,lasse.liehu@gmail.com Lukitse näyttö Suurenna ikkunat vetämällä ne näytön ylälaitaan Tommi Nieminen, Jorma Karvonen, Lasse Liehu Ei toimintoa Vain siirrettäessä ikkunoita Suorita komento Muut asetukset Ikkunat kiinnittyvät näytön neljäsosiksi, kun ne vedetään näytön laitaan korkeintaan Näytä työpöytä Ei käytössä Aseta ikkunat rinnakkain vetämällä ne näytön vasempaan tai oikeaan reunaan Näytä tai piilota vaihtoehtoinen ikkunanvalitsin Näytä tai piilota ikkunanvalitsin Käynnistä toiminto viemällä hiiriosoitin vasten toimintoon liitettyä näytön reunaa tai kulmaa Käynnistä toiminto pyyhkäisemällä näytön laidalta sen keskelle Ikkunanhallinta lähimmästä kulmasta 