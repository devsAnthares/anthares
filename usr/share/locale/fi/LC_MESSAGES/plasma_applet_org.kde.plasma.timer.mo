��          �      �           	          &     -     4  
   =     H     Q     Y     i  >   w     �     �     �     �  
   �     �     �     �            U   %  �  {     Z     l     �     �     �     �     �     �     �     �  :   �       
   '     2     C     T     d     l     t     �     �  ^   �                       	                                                
                                 %1 is running %1 not running &Reset &Start Advanced Appearance Command: Display Execute command Notifications Remaining time left: %1 second Remaining time left: %1 seconds Run command S&top Show notification Show seconds Show title Text: Timer Timer finished Timer is running Title: Use mouse wheel to change digits or choose from predefined timers in the context menu Project-Id-Version: plasma_applet_timer
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2015-04-23 12:45+0200
Last-Translator: Lasse Liehu <lasse.liehu@gmail.com>
Language-Team: Finnish <kde-i18n-doc@kde.org>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-POT-Import-Date: 2012-12-01 22:25:26+0000
X-Generator: Lokalize 2.0
 %1 on käynnissä %1 ei ole käynnissä &Nollaa &Käynnistä Lisäasetukset Ulkoasu Komento: Näyttö Suorita komento Ilmoitukset Aikaa jäljellä: %1 sekunti Aikaa jäljellä: %1 sekuntia Suorita komento &Pysäytä Näytä ilmoitus Näytä sekunnit Näytä otsikko Teksti: Ajastin Aika loppui Ajastin on käynnissä Otsikko: Muuta numeroita käyttämällä hiiren rullaa tai valitse valmis vaihtoehto kontekstivalikosta 