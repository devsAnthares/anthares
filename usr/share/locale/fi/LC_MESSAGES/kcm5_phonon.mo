��    B      ,  Y   <      �  i   �  m        y  b   �     �     	  6        O  7   _     �     �  	   �     �  (   �  )   �  )   (     R     o  Y   u     �     �  �   �      y	  .   �	     �	  
   �	     �	     �	     
     
     !
     5
     B
     J
     c
     r
     w
     �
     �
     �
     �
     �
  	   �
  
   �
     �
     �
  	     
     
   *     5     B  	   `     j     o  
   �  �   �     (  8   8  �   q     �  8   	  ,   B  ,   o  %   �     �  �  �  ^   �  �     ,   �  U   �     0     L  .   Z  	   �  *   �     �     �     �     �  +     '   1  -   Y  %   �     �  _   �          (  z   F  1   �  0   �     $     -  
   6     A  
   J  
   U     `     s     �  !   �     �     �  -   �     �                 	   !  	   +  	   5     ?     N  	   d  	   n     x     �      �     �     �     �     �  �   �     �  4   �  k   �     =  0   M  0   ~  1   �      �          $   !   %          ;   ,          B                 8   #                 7   9       +              )   ?   -   <         6                                    *   2       >                1       @       .   5   3   :   A         &      /                4              0      "             =   (   '         	   
           @info User changed Phonon backendTo apply the backend change you will have to log out and back in again. A list of Phonon Backends found on your system.  The order here determines the order Phonon will use them in. Apply Device List To... Apply the currently shown device preference list to the following other audio playback categories: Audio Hardware Setup Audio Playback Audio Playback Device Preference for the '%1' Category Audio Recording Audio Recording Device Preference for the '%1' Category Backend Colin Guthrie Connector Copyright 2006 Matthias Kretz Default Audio Playback Device Preference Default Audio Recording Device Preference Default Video Recording Device Preference Default/Unspecified Category Defer Defines the default ordering of devices which can be overridden by individual categories. Device Configuration Device Preference Devices found on your system, suitable for the selected category.  Choose the device that you wish to be used by the applications. EMAIL OF TRANSLATORSYour emails Failed to set the selected audio output device Front Center Front Left Front Left of Center Front Right Front Right of Center Hardware Independent Devices Input Levels Invalid KDE Audio Hardware Setup Matthias Kretz Mono NAME OF TRANSLATORSYour names Phonon Configuration Module Playback (%1) Prefer Profile Rear Center Rear Left Rear Right Recording (%1) Show advanced devices Side Left Side Right Sound Card Sound Device Speaker Placement and Testing Subwoofer Test Test the selected device Testing %1 The order determines the preference of the devices. If for some reason the first device cannot be used Phonon will try to use the second, and so on. Unknown Channel Use the currently shown device list for more categories. Various categories of media use cases.  For each category, you may choose what device you prefer to be used by the Phonon applications. Video Recording Video Recording Device Preference for the '%1' Category  Your backend may not support audio recording Your backend may not support video recording no preference for the selected device prefer the selected device Project-Id-Version: kcm_phonon
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-11-19 12:59+0200
Last-Translator: Tommi Nieminen <translator@legisign.org>
Language-Team: Finnish <kde-i18n-doc@kde.org>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-POT-Import-Date: 2012-12-01 22:22:11+0000
X-Generator: Lokalize 2.0
 Käyttääksesi uutta taustajärjestelmää, sinun on kirjauduttava ulos ja takaisin sisään. Luettelo järjestelmästäsi löytyneistä Phonon-taustajärjestelmistä. Järjestys määrää, missä järjestyksessä Phonon käyttää laitteita. Käytä laiteluetteloa muissakin luokissa… Käytä tämän hetkistä laitejärjestystä seuraaviin muihin äänentoistoluokkiin: Äänilaitteiston asetukset Äänentoisto Äänentoistolaitejärjestys luokalle ”%1” Äänitys Äänityslaitejärjestys luokalle ”%1” Taustajärjestelmä Colin Guthrie Liitin Copyright 2006 Matthias Kretz Oletusarvoinen äänentoistolaitejärjestys Oletusarvoinen äänityslaitejärjestys Oletusarvoinen videontallennuslaitejärjestys Oletus- tai määrittämätön luokka Älä suosi Määrittää laitteiden oletusjärjestyksen, joka voidaan ohittaa eri luokkien omin asetuksin. Laiteasetukset Laitteiden käyttöjärjestys Järjestelmästä löytyneet valittuun luokkaan sopivat äänilaitteet. Valitse laite, jota haluat ohjelmien käyttävän. ,mikko.piippo@helsinki.fi,translator@legisign.org Valitun ääniulostulon asettaminen epäonnistui Etukeski Etuvasen Keskivasen Etuoikea Keskioikea Laitteisto Erilliset laitteet Äänitystasot Virheellinen KDE:n äänilaitteiston asetukset Matthias Kretz Mono Teemu Rytilahti, Mikko Piippo, Tommi Nieminen Phononin asetukset Toisto (%1) Suosi Profiili Takakeski Takavasen Takaoikea Äänitys (%1) Näytä lisälaitteet Sivuvasen Sivuoikea Äänikortti Äänilaite Kaiutinten sijoittelu ja testaus Alibassokaiutin Kokeile Kokeile valittua laitetta Laitteen %1 kokeilu Järjestys määrää laitteiden käytön. Jos jostakin syystä ensimmäistä laitetta ei voida käyttää, Phonon yrittää toista laitetta jne. Tuntematon kanava Käytä nykyistä laiteluetteloa muihinkin luokkiin. Eri luokkia käytön mukaan. Voit valita kullekin luokalle laitteen, jota haluat Phonon-ohjelmien suosivan. Videontallennus Videontallennuslaitejärjestys luokalle ”%1” Ehkei taustajärjestelmäsi tue äänittämistä Ehkei taustajärjestelmäsi tue videontallennusta ei etusijaa valitulle laitteelle suosi valittua laitetta 