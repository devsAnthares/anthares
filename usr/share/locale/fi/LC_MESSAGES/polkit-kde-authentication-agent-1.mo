��            )   �      �  ;   �  J   �          /  ~   7  A   �     �       )        G     X     i      q     �     �     �  
   �     �  
   �     �             "   <     _  	   y     �     �     �  �  �     �     �     �  	   �  o   �  U   ;  	   �     �  &   �     �     �     	  .   	     L	     a	     q	     	     �	  
   �	     �	     �	  /   �	  1   
     K
  	   k
     u
     �
     �
                                      
                                                                                            	        %1 is the full user name, %2 is the user login name%1 (%2) %1 is the name of a detail about the current action provided by polkit%1: (c) 2009 Red Hat, Inc. Action: An application is attempting to perform an action that requires privileges. Authentication is required to perform this action. Another client is already authenticating, please try again later. Application: Authentication Required Authentication failure, please try again. Click to edit %1 Click to open %1 Details EMAIL OF TRANSLATORSYour emails Former maintainer Jaroslav Reznik Lukáš Tinkl Maintainer NAME OF TRANSLATORSYour names P&assword: Password for %1: Password for root: Password or swipe finger for %1: Password or swipe finger for root: Password or swipe finger: Password: PolicyKit1 KDE Agent Select User Vendor: Project-Id-Version: polkit-kde-authentication-agent-1
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:22+0100
PO-Revision-Date: 2015-01-10 15:58+0200
Last-Translator: Lasse Liehu <lasse.liehu@gmail.com>
Language-Team: Finnish <lokalisointi@lists.coss.fi>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-POT-Import-Date: 2012-12-01 22:20:07+0000
X-Generator: Lokalize 1.5
 %1 (%2) %1: © 2009 Red Hat, Inc. Toiminto: Sovellus yrittää suorittaa toiminnon, joka vaatii lisäoikeuksia. Sen suorittamiseksi tarvitsee tunnistautua. Tunnistautuminen on jo meneillään toisessa ohjelmassa. Yritä myöhemmin uudelleen. Sovellus: Tunnistautumista vaaditaan Tunnistautumisvirhe. Yritä uudelleen. Muokkaa %1 napsauttamalla Avaa %1 napsauttamalla Yksityiskohdat lasse.liehu@gmail.com,karvonen.jorma@gmail.com Aiempi ylläpitäjä Jaroslav Reznik Lukáš Tinkl Ylläpitäjä Lasse Liehu,Jorma Karvonen &Salasana: Käyttäjän %1 salasana: Pääkäyttäjän salasana: Käyttäjän %1 salasana tai sormen pyyhkäisy: Pääkäyttäjän salasana tai sormen pyyhkäisy: Salasana tai sormen pyyhkäisy: Salasana: PolicyKit1-KDE-agentti Valitse käyttäjä Toimittaja: 