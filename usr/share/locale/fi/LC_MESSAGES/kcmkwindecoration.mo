��            )   �      �  !   �  "   �  '   �  ,     #   ;  &   _  !   �  &   �  '   �     �                 V   $  1   {     �  *   �     �        
     
   "     -     6     ;     D     T     [     a     g  �  p     M     U     [     g  
   w     �     �     �     �     �     �  
   �     �  \   �  6   =     t  ;   �  &   �     �     �     
	     	      	  	   (	     2	     J	     O	     V	     \	                                                                          
                                                    	           @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Borders @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large Application menu Border si&ze: Buttons Close Close by double clicking:
 To open the menu, keep the button pressed until it appears. Close windows by double clicking &the menu button Context help Drag buttons between here and the titlebar Drop here to remove button Get New Decorations... Keep above Keep below Maximize Menu Minimize On all desktops Search Shade Theme Titlebar Project-Id-Version: kcmkwindecoration
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-04-14 02:49+0200
PO-Revision-Date: 2015-10-07 11:23+0200
Last-Translator: Lasse Liehu <lasse.liehu@gmail.com>
Language-Team: Finnish <kde-i18n-doc@kde.org>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-POT-Import-Date: 2012-12-01 22:22:23+0000
X-Generator: Lokalize 2.0
 Valtava Suuri Ei reunusta Ei sivureunusta Tavallinen Ylisuuri Pieni Aivan valtava Hyvin suuri Sovelluksen valikko Reunuksen koko: Painikkeet Sulje Sulje kaksoisnapsauttamalla:
Avaa valikko pitämällä painike pohjassa, kunnes se ilmestyy. Sulje ikkunat kaksoisnapsauttamalla &valikkopainiketta Kontekstiohje Vedä painikkeita tämän alueen ja otsikkopalkin välillä Poista painike pudottamalla se tähän Hae uusia kehyksiä… Pidä ylinnä Pidä alinna Suurenna Valikko Pienennä Kaikilla työpöydillä Etsi Rullaa Teema Otsikkopalkki 