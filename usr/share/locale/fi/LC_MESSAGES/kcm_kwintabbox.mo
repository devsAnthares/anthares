��    "      ,  /   <      �  
   �               ,     >     J  !   V     x     �     �     �     �     �  L   �     #     +     J     Y     u     z     �     �     �     �  	   �     �     �     �  �   �  F   �     �     �     �  �  �     �     �               /     >     M  	   [     e     {     �     �     �  P   �  
   	  '   $	     L	  '   _	     �	  	   �	  "   �	     �	     �	     �	     �	     �	     
     )
  �   <
  L   �
          ,     >                    !                                            	                                                           "               
                    Activities All other activities All other desktops All other screens All windows Alternative An example Desktop NameDesktop 1 Content Current activity Current application Current desktop Current screen Filter windows by Focus policy settings limit the functionality of navigating through windows. Forward Get New Window Switcher Layout Hidden windows Include "Show Desktop" icon Main Minimization Only one window per application Recently used Reverse Screens Shortcuts Show selected window Sort order: Stacking order The currently selected window will be highlighted by fading out all other windows. This option requires desktop effects to be active. The effect to replace the list window when desktop effects are active. Virtual desktops Visible windows Visualization Project-Id-Version: kcm_kwintabbox
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2014-05-06 13:17+0300
Last-Translator: Lasse Liehu <lasse.liehu@gmail.com>
Language-Team: Finnish <lokalisointi@lists.coss.fi>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-POT-Import-Date: 2012-12-01 22:22:19+0000
X-Generator: Lokalize 1.5
 Aktiviteetit Kaikki muut aktiviteetit Kaikki muut työpöydät Kaikki muut näytöt Kaikki ikkunat Vaihtoehtoinen Työpöytä 1 Sisältö Nykyinen aktiviteetti Nykyinen sovellus Nykyinen työpöytä Nykyinen näyttö Ikkunoiden suodatus Kohdistuskäytänneasetukset rajoittavat ikkunoiden selauksen toiminnallisuutta. Eteenpäin Hae uusia ikkunanvalitsimen asetteluita Piilotetut ikkunat Näytä ”Näytä työpöytä" -kuvake Ensisijainen Pienennys Vain yksi ikkuna sovellusta kohden Äskettäin käytetty Käänteinen Näytöt Pikanäppäimet Näytä valittu ikkuna Lajittelujärjestys: Pinoamisjärjestys Nykyinen valittu ikkuna korostetaan häivyttämällä kaikki muut ikkunat. Tämä valitsin vaatii työpöytätehosteiden olevan käytössä. Luetteloikkunan korvaava tehoste työpöydätehosteiden ollessa käytössä. Virtuaalityöpöydät Näkyvät ikkunat Visualisointi 