��    &      L  5   |      P     Q     l     y     �     �     �     �     �     �     �     �     �  &   �               #     ,     3     ?     M     U     ]     d     s     �     �     �     �     �     �     �     �     �     �  
   �            �       	  	     &        =  	   E     O     X     d     u     �     �     �  7   �     �               %     ,     :     K     S     _     f     u     �     �     �     �     �     �     �     �     �     	     "	     5	     B	                           	      #      $                                                    &                                          !          %                            "   
    Abbreviation for secondss Application: Average clock: %1 MHz Bar Buffers: CPU CPU %1 CPU monitor CPU%1: %2% @ %3 Mhz CPU: %1% CPUs separately Cache Cache Dirty, Writeback: %1 MiB, %2 MiB Cache monitor Cached: Circular Colors Compact Bar Dirty memory: General IOWait: Memory Memory monitor Memory: %1/%2 MiB Monitor type: Nice: Set colors manually Show: Swap Swap monitor Swap: %1/%2 MiB Sys: System load Update interval: Used swap: User: Writeback memory: Project-Id-Version: plasma_applet_systemloadviewer
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-29 03:31+0200
PO-Revision-Date: 2017-12-26 10:03+0200
Last-Translator: Tommi Nieminen <translator@legisign.org>
Language-Team: Finnish <kde-i18n-doc@kde.org>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-POT-Import-Date: 2012-12-01 22:25:25+0000
X-Generator: Lokalize 2.0
  s Sovellus: Keskimääräinen kellotaajuus: %1 MHz Pylväs Puskurit: Suoritin Suoritin %1 Suoritinnäyttö Suoritin %1: %2 % @ %3 Mhz Suoritin: %1 % Suorittimet erillään Välimuisti Välimuisti likainen, takaisinkirjoitus: %1 MiB, %2 MiB Välimuistinäyttö Välimuisti: Ympyrä Värit Tiivis palkki Likainen muisti: Yleiset I/O-odotus: Muisti Muistinäyttö Muisti: %1/%2 MiB Näyttötyyppi: Prioriteetti: Aseta värit käsin Näytä: Sivutus Sivutusnäyttö Sivutus: %1/%2 MiB Järjestelmä: Järjestelmän kuorma Päivitysväli: Käytetty sivutus: Käyttäjä: Takaisinkirjoitusmuisti: 