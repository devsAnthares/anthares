��    ?        Y         p  �  q  }  J  M  �        
   7     B     K     j     �  <   �     �     �  	   �     �  .     %   A     g     }     �     �     �     �     �     �     �     �  4        B  9   T     �     �     �  �   �  !   �  t   �     8     D     a     n     s  	   �     �  -   �     �  B   �  &     ?   B  '   �  )   �  7   �  .     5   ;  +   q     �     �     �  ,   �          -  9   :  V   t  C   �  �    �  �  �  �  L  L     �"     �"  
   �"     �"  !   �"     #  ;   #     C#     ]#  	   i#     s#  )   �#     �#     �#     �#     �#     �#     $     $     3$  &   A$     h$     n$  :   �$     �$  9   �$     %     $%     6%  �   ?%  #   &  p   3&     �&     �&     �&     �&     �&  	   �&     	'  $   '  	   ;'  4   E'  *   z'  6   �'  /   �'  /   (  4   <(     q(  7   �(  1   �(     �(     )     7)  *   H)     s)     �)  6   �)  F   �)     *     !      	   2   ?   
   /                 +   (   *       1            )   >           8          9   $              7             .       '   6          %   <             3   ,          ;       =   &                              -           #      "          :      5      0                                   4               <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body style=" font-family:'hlv'; font-size:9pt; font-weight:400; font-style:normal;">
<p style="-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><br /></p></body></html> <b>Security notice:</b>
                             According to a security audit by Taylor Hornby (Defuse Security),
                             the current implementation of Encfs is vulnerable or potentially vulnerable
                             to multiple types of attacks.
                             For example, an attacker with read/write access
                             to encrypted data might lower the decryption complexity
                             for subsequently encrypted data without this being noticed by a legitimate user,
                             or might use timing analysis to deduce information.
                             <br /><br />
                             This means that you should not synchronize
                             the encrypted data to a cloud storage service,
                             or use it in other circumstances where the attacker
                             can frequently access the encrypted data.
                             <br /><br />
                             See <a href='http://defuse.ca/audits/encfs.htm'>defuse.ca/audits/encfs.htm</a> for more information. <b>Security notice:</b>
                             CryFS encrypts your files, so you can safely store them anywhere.
                             It works well together with cloud services like Dropbox, iCloud, OneDrive and others.
                             <br /><br />
                             Unlike some other file-system overlay solutions,
                             it does not expose the directory structure,
                             the number of files nor the file sizes
                             through the encrypted data format.
                             <br /><br />
                             One important thing to note is that,
                             while CryFS is considered safe,
                             there is no independent security audit
                             which confirms this. @title:windowCreate a New Vault Activities Backend: Can not create the mount point Can not open an unknown vault. Change Choose the encryption system you want to use for this vault: Choose the used cypher: Close the vault Configure Configure Vault... Configured backend can not be instantiated: %1 Configured backend does not exist: %1 Correct version found Create Create a New Vault... CryFS Device is already open Device is not open Dialog Do not show this notice again EncFS Encrypted data location Failed to create directories, check your permissions Failed to execute Failed to fetch the list of applications using this vault Failed to open: %1 Forcefully close  General If you limit this vault only to certain activities, it will be shown in the applet only when you are in those activities. Furthermore, when you switch to an activity it should not be available in, it will automatically be closed. Limit to the selected activities: Mind that there is no way to recover a forgotten password. If you forget the password, your data is as good as gone. Mount point Mount point is not specified Mount point: Next Open with File Manager Password: Plasma Vault Please enter the password to open this vault: Previous The mount point directory is not empty, refusing to open the vault The specified backend is not available The vault configuration can only be changed while it is closed. The vault is unknown, can not close it. The vault is unknown, can not destroy it. This device is already registered. Can not recreate it. This directory already contains encrypted data Unable to close the vault, an application is using it Unable to close the vault, it is used by %1 Unable to detect the version Unable to perform the operation Unknown device Unknown error, unable to create the backend. Use the default cipher Vaul&t name: Wrong version installed. The required version is %1.%2.%3 You need to select empty directories for the encrypted storage and for the mount point formatting the message for a command, as in encfs: not found%1: %2 Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-11 05:53+0100
PO-Revision-Date: 2018-01-04 17:36+0200
Last-Translator: Tommi Nieminen <translator@legisign.org>
Language-Team: Finnish <kde-i18n-doc@kde.org>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body style=" font-family:'hlv'; font-size:9pt; font-weight:400; font-style:normal;">
<p style="-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><br /></p></body></html> <b>Turvallisuustiedote:</b>
                             Taylor Hornbyn (Defuse Security) turvallisuusauditoinnin mukaan
                             EncFS:n nykyinen toteutus on haavoittuvainen tai mahdollisesti haavoittuvainen
                             useamman tyyppisille hyökkäyksille.
                             Jos esimerkiksi hyökkääjällä on luku- ja kirjoitusoikeudet
                             salattuun tietoon, myöhemmin tuotetun salatun tiedon salaustaso voi laskea
                             ilman että asiaan kuuluva käyttäjä sitä huomaa. Hyökkääjä voi käyttää myös
                             aikatietoja salatun tiedon päättelemiseksi.
                             <br /><br />
                             Tästä seuraa, että sinun ei tulisi lähettää salattua

                             tietoa mihinkään pilvipalveluun tai käyttää sitä muuten
                             olosuhteissa, joissa hyökkääjä pääsee usein käsiksi
                             salattuun tietoon.
                             <br /><br />
                             Lisätietoa osoitteesta <a href='http://defuse.ca/audits/encfs.htm'>defuse.ca/audits/encfs.htm</a>. <b>Turvallisuustiedote:</b>
                             CryFS salaa tiedostosi niin, että voit tallentaa ne turvallisesti minne vain.
                             Se toimii hyvin pilvipalvelujen kuten Dropboxin, iCloudin, OneDriven tai muiden kanssa.
                             <br /><br />
                             Muista tiedostojärjestelmäkerrosratkaisuista poiketen
                             se ei paljasta salatun tiedon kansiorakennetta, tiedostojen
                             määrää tai tiedostojen kokoa.
                             <br /><br />
                             On kuitenkin tärkeää huomata, että vaikka
                             CryFS:ää pidetään turvallisena,
                             yksikään riippumaton turvallisuusauditointi
                             ei ole vahvistanut tätä. Luo uusi holvi Aktiviteetit Taustaosa: Liitospistettä ei voida luoda Tuntematonta holvia ei voi avata. Muuta Valitse tässä holvissa käytettävä salausjärjestelmä: Valitse käytetty salaus: Sulje holvi Asetukset Holvin asetukset… Asetettua taustaosaa ei voida alustaa: %1 Asetettua taustaosaa ei ole: %1 Löytyi oikea versio Luo Luo uusi holvi… CryFS Laite on jo käytössä Laite ei ole käytössä Valintaikkuna Älä näytä tätä ilmoitusta toiste EncFS Salatun tiedon sijainti Kansioiden luonti epäonnistui: tarkista käyttöoikeutesi Suoritus epäonnistui Ei voitu noutaa holvia käyttävien sovellusten luetteloa Ei voitu avata: %1 Pakota sulkeminen Yleistä Jos rajoitat holvin vain tietyille aktiviteeteille, se näkyy sovelmana vain ollessasi jossakin niistä. Lisäksi vaihtaessasi aktiviteettiin, jolle holvi ei ole käytössä, holvi suljetaan automaattisesti. Rajoita valituille aktiviteeteille: Huomaa, että unohdettua salasanaa ei mitenkään voi palauttaa. Jos unohdat salasanasi, tietosi ovat mennyttä. Liitospiste Liitospistettä ei ole asetettu Liitospiste: Seuraava Avaa tiedostonhallintaan Salasana: Plasma Vault Anna tämän holvin avaava salasana: Edellinen Liitospistekansio ei ole tyhjä: holvia ei voi avata Asetettu taustaosa ei ole käytettävissä Holvin asetuksia voi muuttaa vain, kun se ei ole auki. Holvi on tuntematon, joten sitä ei voi sulkea. Holvi on tuntematon, joten sitä ei voi tuhota. Laite on jo rekisteröity: ei voida luoda uudestaan. Kansiossa on jo salattua tietoa Holvia ei voida sulkea, koska sovellus käyttää sitä Holvia ei voida sulkea, koska %1 käyttää sitä Versiota ei voitu tunnistaa Toimenpidettä ei voi suorittaa Tuntematon laite Tuntematon virhe: taustaosaa ei voi luoda. Käytä oletussalausta &Holvin nimi: Asennettu väärä versio. Vaadittu versio on %1.%2.%3 Salatulle tiedolle ja sen liitospisteelle on valittava tyhjät kansiot %1: %2 