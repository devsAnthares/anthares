��          �      �       0     1  �  H     �       &         ?     `     n     v     �     �  (   �  �  �      �  �  �     �     �  9   �  6   �     %     5  (   =     f     v  1   �         
                 	                                (c) 2002-2006 KDE Team <h1>System Notifications</h1>Plasma allows for a great deal of control over how you will be notified when certain events occur. There are several choices as to how you are notified:<ul><li>As the application was originally designed.</li><li>With a beep or other noise.</li><li>Via a popup dialog box with additional information.</li><li>By recording the event in a logfile without any additional visual or audible alert.</li></ul> Carsten Pfeiffer Charles Samuels Disable sounds for all of these events EMAIL OF TRANSLATORSYour emails Event source: KNotify NAME OF TRANSLATORSYour names Olivier Goffart Original implementation System Notification Control Panel Module Project-Id-Version: kcmnotify
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-09 07:18+0200
PO-Revision-Date: 2017-12-03 21:07+0900
Last-Translator: Lasse Liehu <lasse.liehu@gmail.com>
Language-Team: Finnish <kde-i18n-doc@kde.org>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-POT-Import-Date: 2012-12-01 22:22:12+0000
X-Generator: Lokalize 2.0
 © 2002–2006 KDE-kehitysryhmä <h1>Järjestelmäilmoitukset</h1> Plasmassa voit määritellä tarkasti, kuinka järjestelmissä tapahtuvista tapahtumista ilmoitetaan. On useita tapoja, joilla käyttäjälle voidaan antaa ilmoituksia:<ul><li>Sovelluksen alkuperäisellä suunnitellulla tavalla.</li><li>Piippauksella tai muulla huomautusäänellä.</li><li>Viesti-ikkunalla jossa on lisätietoja.</li><li>Tapahtuma tallennetaan lokitiedostoon ilman varoitusta.</li></ul> Carsten Pfeiffer Charles Samuels Poista äänet käytöstä kaikilta näiltä tapahtumilta ilpo@iki.fi, eleknader@phnet.fi, lasse.liehu@gmail.com Toimintolähde: KNotify Ilpo Kantonen, Tapio Kautto, Lasse Liehu Olivier Goffart Alkuperäinen toteutus Asetusosio järjestelmäilmoitusten muokkaamiseen 