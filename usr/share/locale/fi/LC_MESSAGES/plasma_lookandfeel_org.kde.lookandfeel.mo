��    2      �  C   <      H     I     M     R  .   a  5   �     �     �     �     �  	   �     �          '     6  1   J     |     �     �     �  
   �     �  '   �     �     �     �          !  '   (     P     _     f     n     �  5   �     �     �     �     �     �  $   �  A   #  �   e     K     R  C   c  '   �     �     �     �  �  	     �
     �
     �
     �
     �
     �
            
   $  	   /     9     W     n     ~  L   �     �     �     
     &     4     E     Z     j     m     v     �     �  R   �               &     <     U  :   ]     �     �     �     �     �     �     �             !        7     T     [     m     u               $   0       .                      %                ,         )   2          /   "                        &   1      '       +   !   	         -                               
         (                    *            #       %1% Back Battery at %1% Button to change keyboard layoutSwitch layout Button to show/hide virtual keyboardVirtual Keyboard Cancel Caps Lock is on Close Close Search Configure Configure Search Plugins Desktop Session: %1 Different User Keyboard Layout: %1 Logging out in 1 second Logging out in %1 seconds Login Login Failed Login as different user Logout Next track No media playing Nobody logged in on that sessionUnused OK Password Play or Pause media Previous track Reboot Reboot in 1 second Reboot in %1 seconds Recent Queries Remove Restart Show media controls: Shutdown Shutting down in 1 second Shutting down in %1 seconds Start New Session Suspend Switch Switch Session Switch User Textfield placeholder textSearch... Textfield placeholder text, query specific KRunnerSearch '%1'... This is the first text the user sees while starting in the splash screen, should be translated as something short, is a form that can be seen on a product. Plasma is the project name so shouldn't be translated.Plasma made by KDE Unlock Unlocking failed User logged in on console (X display number)on TTY %1 (Display %2) User logged in on console numberTTY %1 Username Username (location)%1 (%2) in category recent queries Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-09 03:21+0100
PO-Revision-Date: 2018-01-22 17:36+0200
Last-Translator: Tommi Nieminen <translator@legisign.org>
Language-Team: Finnish <kde-i18n-doc@kde.org>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 %1 % Takaisin Akku tasolla %1 % Vaihda asettelua Virtuaalinäppäimistö Peru Vaihtolukko on käytössä Sulje Sulje haku Asetukset Hakuliitännäisten asetukset Työpöytäistunto: %1 Eri käyttäjä Näppäimistöasettelu: %1 Uloskirjautuminen 1 sekunnin kuluttua Uloskirjautuminen %1 sekunnin kuluttua Kirjaudu Kirjautuminen epäonnistui Kirjaudu eri käyttäjänä Kirjaudu ulos Seuraava kappale Mitään ei toisteta Käyttämätön OK Salasana Toista tai keskeytä media Edellinen kappale Käynnistä uudelleen Uudelleenkäynnistys 1 sekunnin kuluttua Uudelleenkäynnistys %1 sekunnin kuluttua Viimeisimmät haut Poista Käynnistä uudelleen Näytä mediasäätimet: Sammuta Sammutus 1 sekunnin kuluttua Sammutus %1 sekunnin kuluttua Käynnistä uusi istunto Valmiustila Vaihda Vaihda istuntoa Vaihda käyttäjää Hae… Hae ”%1”… Plasma KDE:ltä Avaa Lukituksen avaaminen epäonnistui TTY:ssä %1 (näytöllä %2) TTY %1 Käyttäjätunnus %1 (%2) luokassa viimeisimmät haut 