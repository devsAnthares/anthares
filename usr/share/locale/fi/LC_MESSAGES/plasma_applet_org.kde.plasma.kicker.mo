��    _                   C   	  /   M  -   }     �     �     �     �      	     	     1	     >	     J	  
   S	     ^	     g	     p	     �	  	   �	     �	     �	     �	  ,   �	  	    
     

  
   )
     4
     L
     `
     u
     �
     �
     �
     �
     �
  	   �
     �
     �
     
               !     (  	   ;     E     ]  
   r     }     �  
   �     �     �     �  
   �     �     �               $     2     @     R     h     y     �     �     �     �  	   �     �     �     �               4     Q     j     �     �     �     �  	   �     �  ,   �          !     0     @     L     S     b     t     �  #   �     �  �  �     q  
   �     �     �     �     �     �     
          %     1     J     S  	   Z     d     q       	   �     �     �     �  @   �               :     H     b     }     �     �     �     �     �     �     �  
                   $     ,     =     K  	   Y     c          �     �     �     �     �  	             '     4     J     d          �     �     �     �     �               /     @     E     R     Y     e     m     �     �  #   �  $   �  !     !   )  "   K     n     �     �     �  /   �     �     �     �     	          ,     E     W  2   k  4   �     �         Q         Y   5           %       H       J   !              \   *       @   V   4   [       A   (   
       U   B   ]   ^       $   _         '   >       D   K      -                  ?      ,   O   0       R   8   6   W   2   I   =   X   E   #       N   C       T   G   M          3   "         9             +          S          F           ;   :                    &   <   L          P         7          .       /       1                     )            	   Z       @action opens a software center with the applicationManage '%1'... @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Desktop Add to Favorites Add to Panel (Widget) Align search results to bottom All Applications App name (Generic name)%1 (%2) Applications Apps & Docs Behavior Categories Computer Contacts Description (Name) Description only Documents Edit Application... Edit Applications... End session Expand search to bookmarks, files and emails Favorites Flatten menu to a single level Forget All Forget All Applications Forget All Contacts Forget All Documents Forget Application Forget Contact Forget Document Forget Recent Documents General Generic name (App name)%1 (%2) Hibernate Hide %1 Hide Application Icon: Lock Lock screen Logout Name (Description) Name only Often Used Applications Often Used Documents Often used On All Activities On The Current Activity Open with: Pin to Task Manager Places Power / Session Properties Reboot Recent Applications Recent Contacts Recent Documents Recently Used Recently used Removable Storage Remove from Favorites Restart computer Run Command... Run a command or a search query Save Session Search Search results Search... Searching for '%1' Session Show Contact Information... Show In Favorites Show applications as: Show often used applications Show often used contacts Show often used documents Show recent applications Show recent contacts Show recent documents Show: Shut Down Sort alphabetically Start a parallel session as a different user Suspend Suspend to RAM Suspend to disk Switch User System System actions Turn off computer Type to search. Unhide Applications in '%1' Unhide Applications in this Submenu Widgets Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2017-12-08 16:26+0200
Last-Translator: Tommi Nieminen <translator@legisign.org>
Language-Team: Finnish <kde-i18n-doc@kde.org>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 Hallitse sovellusta %1… Valitse… Poista kuvake Lisää työpöydälle Lisää suosikkeihin Lisää paneeliin (sovelma) Tasaa hakutulokset alareunaan Kaikki sovellukset %1 (%2) Sovellukset Sovellukset ja tiedostot Toiminta Luokat Tietokone Yhteystiedot Kuvaus (Nimi) Vain kuvaus Tiedostot Muokkaa sovellusta… Muokkaa sovelluksia… Lopeta istunto Laajenna haku kirjanmerkkeihin, tiedostoihin ja sähköposteihin Suosikit Tasoita valikko yksitasoiseksi Unohda kaikki Unohda kaikki sovellukset Unohda kaikki yhteystiedot Unohda kaikki tiedostot Unohda sovellus Unohda yhteystieto Unohda tiedosto Unohda viimeisimmät tiedostot Yleiset %1 (%2) Lepotila Piilota %1 Piilota sovellus Kuvake: Lukitse Lukitse näyttö Kirjaudu ulos Nimi (Kuvaus) Vain nimi Usein käytetyt sovellukset Usein käytetyt tiedostot Usein käytetty Kaikissa aktiviteeteissa Nykyisessä aktiviteetissa Avaa ohjelmalla: Kiinnitä tehtävienhallintaan Sijainnit Virta / istunto Ominaisuudet Käynnistä uudelleen Viimeisimmät sovellukset Viimeisimmät yhteystiedot Viimeisimmät tiedostot Viimeisimmät Äskettäin käytetyt Irrotettava tallennusväline Poista suosikeista Käynnistä tietokone uudelleen Suorita komento… Etsi tai suorita Tallenna istunto Haku Hakutulokset Hae… Haetaan: %1 Istunto Näytä yhteystiedon tiedot… Näytä suosikeissa Näytä sovellukset muodossa: Näytä usein käytetyt sovellukset Näytä usein käytetyt yhteystiedot Näytä usein käytetyt tiedostot Näytä viimeisimmät sovellukset Näytä viimeisimmät yhteystiedot Näytä viimeisimmät tiedostot Näytä: Sammuta Aakkosta Aloita rinnakkaisistunto toisena käyttäjänä Valmiustila Keskeytä muistiin Keskeytä levylle Vaihda käyttäjää Järjestelmä Järjestelmän toiminnot Sammuta tietokone Hae kirjoittamalla. Kumoa sovellusten piilottaminen valikossa ”%1” Kumoa sovellusten piilottaminen tässä alivalikossa Sovelmat 