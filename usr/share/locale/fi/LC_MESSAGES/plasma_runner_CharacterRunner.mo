��    
      l      �       �      �            	               .  I   3     }  	   �    �     �     �     �     �      �     �  M   �     I     V               
   	                           &Trigger word: Add Item Alias Alias: Character Runner Config Code Creates Characters from :q: if it is a hexadecimal code or defined alias. Delete Item Hex Code: Project-Id-Version: plasma_runner_CharacterRunner
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2012-05-05 18:37+0300
Last-Translator: Lasse Liehu <lasse.liehu@gmail.com>
Language-Team: Finnish <lokalisointi@lists.coss.fi>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-POT-Import-Date: 2012-12-01 22:25:26+0000
X-Generator: MediaWiki 1.21alpha (963ddae); Translate 2012-11-08
 &Laukaisinsana: Lisää alias Alias Alias: Merkkisuoritusohjelman asetukset Koodi Luo merkkejä :q::sta sen ollessa heksadesimaalikoodi tai määritetty alias. Poista alias Heksakoodi: 