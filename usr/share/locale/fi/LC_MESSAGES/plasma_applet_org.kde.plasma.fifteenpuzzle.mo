��          �            x  
   y  	   �     �     �  3   �     �     �                    %     *     F  B   Y     �  �  �     �     �     �  	   �  5   �     �                      0     8      =     ^     {     �     	                                                  
                        Appearance Browse... Choose an image Fifteen Puzzle Image Files (*.png *.jpg *.jpeg *.bmp *.svg *.svgz) Number color Path to custom image Piece color Show numerals Shuffle Size Solve by arranging in order Solved! Try again. The time since the puzzle started, in minutes and secondsTime: %1 Use custom image Project-Id-Version: plasma_applet_fifteenPuzzle
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-01-12 03:59+0100
PO-Revision-Date: 2017-01-13 22:59+0200
Last-Translator: Lasse Liehu <lasse.liehu@gmail.com>
Language-Team: Finnish <kde-i18n-doc@kde.org>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-POT-Import-Date: 2012-12-01 22:25:23+0000
X-Generator: Lokalize 2.0
 Ulkoasu Selaa… Valitse kuva Puzzle 15 Kuvatiedostot (*.png *.jpg *.jpeg *.bmp *.svg *.svgz) Numeron väri Oman kuvan sijainti Palan väri Näytä numerot Sekoita Koko Ratkaise järjestämällä palat Ratkaistu! Yritä uudelleen. Aika: %1 Käytä omaa kuvaa 