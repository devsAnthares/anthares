��    Q      �  m   ,      �     �     �     �                      .   !  U   P     �     �      �     �  /   �  /   -     ]     i     r  
   ~     �     �  $   �     �     �     �     �     	     	  	   *	     4	  
   F	  7   Q	     �	     �	     �	     �	  	   �	     �	  !   �	     
     
  	   !
      +
     L
     Y
     r
     �
     �
     �
     �
     �
     �
     �
  (   �
       =   '  2   e     �     �  "   �     �       J     1   Y  )   �  (   �  '   �  "     4   )     ^     l     r     {     �     �     �  U   �  N   !  9   p  J   �  �  �      �     �     �     �     �  
   �     �     �  (         /     P  !   i     �  2   �  3   �     �                    0     F  "   ^     �     �     �     �     �     �     �     �     
               )     7     K  	   Q     [  %   b     �     �  
   �  '   �     �  "   �          4     L     U     d     v     �     �  0   �      �               $     *     ;     M     T     a  "   r  -   �  -   �  )   �  #     7   ?     w     �  
   �     �     �  	   �     �  
   �      �  "     )   )        L   )             O                      4              6   $   ?   3         B              (   K   Q               ;       .   '   D   2   -       J   >   !   
   8                       P   5         ,          7   <           F      *   @       H   1                 "          9      +   &   0           /   N          #       E   	             I   A   =   %      G   :   C                   M    &All Desktops &Close &Fullscreen &Move &New Desktop &Pin &Shade 1 = number of desktop, 2 = desktop name&%1 %2 Activities a window is currently on (apart from the current one)Also available on %1 Add To Current Activity All Activities Allow this program to be grouped Alphabetically Always arrange tasks in columns of as many rows Always arrange tasks in rows of as many columns Arrangement Behavior By Activity By Desktop By Program Name Close Window or Group Cycle through tasks with mouse wheel Do Not Group Do Not Sort Filters Forget Recent Documents General Grouping and Sorting Grouping: Highlight windows Icon size: Invalid number of new messages, overlay, keep short— Keep &Above Others Keep &Below Others Keep launchers separate Large Ma&ximize Manually Mark applications that play audio Maximum columns: Maximum rows: Mi&nimize Minimize/Restore Window or Group More Actions Move &To Current Desktop Move To &Activity Move To &Desktop Mute New Instance On %1 On All Activities On The Current Activity On middle-click: Only group when the task manager is full Open groups in popups Open or bring to the front window of media player appRestore Over 9999 new messages, overlay, keep short9,999+ Pause playbackPause Play next trackNext Track Play previous trackPrevious Track Quit media player appQuit Re&size Remove launcher button for application shown while it is not runningUnpin Show all user Places%1 more Place %1 more Places Show only tasks from the current activity Show only tasks from the current desktop Show only tasks from the current screen Show only tasks that are minimized Show progress and status information in task buttons Show tooltips Small Sorting: Start New Instance Start playbackPlay Stop playbackStop The click actionNone Toggle action for showing a launcher button while the application is not running&Pin When clicking it would toggle grouping windows of a specific appGroup/Ungroup Which activities a window is currently onAvailable on %1 Which virtual desktop a window is currently onAvailable on all activities Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2018-01-22 17:34+0200
Last-Translator: Tommi Nieminen <translator@legisign.org>
Language-Team: Finnish <kde-i18n-doc@kde.org>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 Näytä k&aikilla työpöydillä &Sulje &Koko näyttö &Siirrä &Uusi työpöytä K&iinnitä &Rullaa &%1 %2 Käytettävissä myös aktiviteetissa %1 Lisää nykyiseen aktiviteettiin Kaikissa aktiviteeteissa Salli tämän ohjelman ryhmittely Aakkosta Asettele tehtävät aina tasarivisiksi sarakkeiksi Asettele tehtävät aina tasasarakkeisiksi riveiksi Asettelu Toiminta Aktiviteeteittain Työpöydittäin Ohjelman nimen mukaan Sulje ikkuna tai ryhmä Vaihda tehtävää hiiren rullalla Älä ryhmittele Älä lajittele Suodattimet Unohda viimeisimmät tiedostot Yleistä Ryhmittely ja lajittelu Ryhmittely: Korosta ikkunat Kuvakekoko: — Pidä &ylinnä Pidä &alinna Erota käynnistimet Suuri S&uurenna Käsin Osoita ääntä toistavat sovellukset Sarakkeita enintään: Rivejä enintään: P&ienennä Pienennä tai palauta ikkuna tai ryhmä Lisää toimintoja Siirrä &nykyiselle työpöydälle Siirrä &aktiviteettiin Siirrä &työpöydälle Vaimenna Uusi instanssi Aktiviteetissa %1 Kaikkiin aktiviteetteihin Nykyiseen aktiviteettiin Hiiren keskipainikkeella: Ryhmittele vain tehtäväpalkin ollessa täynnä Avaa ryhmät ponnahdusikkunoihin Palauta 9,999+ Tauko Seuraava kappale Edellinen kappale Lopeta &Muuta kokoa Poista kiinnitys Vielä %1 kohde Vielä %1 kohdetta Näytä vain nykyisen aktiviteetin tehtävät Näytä vain nykyisen työpöydän tehtävät Näytä vain nykyisen näytön tehtävät Näytä vain pienennetyt tehtävät Näytä edistyminen ja tilatiedot tehtäväpainikkeissa Näytä työkaluvihjeet Pieni Lajittelu: Aloita uusi instanssi Toista Pysäytä Älä tee mitään K&iinnitä Ryhmittele tai poista ryhmittely Käytettävissä aktiviteetissa %1 Käytettävissä kaikissa aktiviteeteissa 