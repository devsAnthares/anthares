��          t      �                 '     6     R     Z     w  Q   �     �      �       �              #   (     L     S     l     �     �     �     �     
      	                                                 &Require trigger word &Trigger word: Checks the spelling of :q:. Correct Could not find a dictionary. Spell Check Settings Spelling checking runner syntax, first word is trigger word, e.g.  "spell".%1:q: Suggested words: %1 separator for a list of words,  spell Project-Id-Version: plasma_runner_spellcheckrunner
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2016-05-01 00:06+0200
Last-Translator: Lasse Liehu <lasse.liehu@gmail.com>
Language-Team: Finnish <kde-i18n-doc@kde.org>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-POT-Import-Date: 2012-12-01 22:25:27+0000
X-Generator: Lokalize 2.0
 &Vaadi laukaisinsana Laukaisin&sana: Tarkistaa :q::n oikeinkirjoituksen. Oikein Sanakirjaa ei löytynyt. Oikoluvun asetukset %1:q: Ehdotetut sanat: %1 ,  oikolue 