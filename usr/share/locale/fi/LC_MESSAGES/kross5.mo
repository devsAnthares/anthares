��    $      <  5   \      0     1  +   E     q  4   �  &   �     �     �                     5     :     P     X  ,   u  3   �     �     �               !  '   .     V     u     {     �     �     �     �     �     �  &   �       B        b  �  y     M     U     m     }     �     �  
   �     �  
   �  �   �     �	     �	  
   �	     �	  6   
  D   B
  '   �
  7   �
  	   �
     �
     �
       �        �  "   �     �     �     �       ,   !  	   N  (   X     �  8   �     �                                       
                              !                                  $      #                               	                           "             @info:creditAuthor @info:creditCopyright 2006 Sebastian Sauer @info:creditSebastian Sauer @info:shell command-line argumentThe script to run. @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: application descriptionCommand-line utility to run Kross scripts. application nameKross Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2014-04-11 15:59+0300
Last-Translator: Lasse Liehu <lasse.liehu@gmail.com>
Language-Team: Finnish <kde-i18n-doc@kde.org>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 1.5
X-POT-Import-Date: 2013-01-13 20:43:21+0000
 Tekijä © 2006 Sebastian Sauer Sebastian Sauer Suoritettava komentojono. Yleistä Lisää uusi komentojono. Lisää… Perutaanko? Kommentti: kim.enkovaara@iki.fi, eleknader@phnet.fi, ikola@iki.fi, tpr@d5k.net, lasse.liehu@gmail.com, niklas.laxstrom+kdetrans@gmail.com, juippis@roskakori.org, piippo@cc.helsinki.fi, translator@legisign.org, karvonen.jorma@gmail.com Muokkaa Muokkaa valittua komentojonoa. Muokkaa… Suorita valittu komentojono. Komentosarjan luominen tulkille ”%1” epäonnistui. Komentosarjatiedoston ”%1” tulkin määrittäminen epäonnistui. Tulkin ”%1” lataaminen epäonnistui Komentosarjatiedoston ”%1” avaaminen epäonnistui . Tiedosto: Kuvake: Tulkki: Ruby-tulkin turvallisuustaso Kim Enkovaara, Tapio Kautto, Mikko Ikola, Teemu Rytilahti, Lasse Liehu, Niklas Laxström, Joonas Niilola, Mikko Piippo, Tommi Nieminen, Jorma Karvonen Nimi: Funktiota ”%1” ei ole olemassa Tulkkia ”%1” ei ole Poista Poista valittu komentojono. Suorita Komentosarjatiedostoa ”%1” ei löytynyt. Pysäytä Pysäytä valitun komentojonon suoritus. Teksti: Komentoriviohjelma Kross-komentojonojen suorittamiseksi. Kross 