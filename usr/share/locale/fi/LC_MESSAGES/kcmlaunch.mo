��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  �  K        )   #  S  M  X  �  e   �	     `
     q
     �
  !   �
     �
     �
  *   �
                                               
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-03-22 15:49+0200
Last-Translator: Lasse Liehu <lasse.liehu@gmail.com>
Language-Team: Finnish <kde-i18n-doc@kde.org>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-POT-Import-Date: 2012-12-01 22:22:23+0000
X-Generator: Lokalize 2.0
  s Käynnistyksen ilmaisemisen aikakatkaisu: <H1>Tehtäväpalkin ilmoitus</H1>
Tiedon ohjelmien käynnistämisestä voi näyttää paitsi
varatulla osoittimella myös tehtäväpalkissa: käynnistyvän
ohjelman kuvakkeen päällä näkyy pyörivä kiekko. Kaikki
sovellukset eivät osaa lopettaa tehtäväpalkin ilmoitusta
jolloin ilmoitus lopetetaan määritetyn aikaviiveen jälkeen. <h1>Varattu osoitin</h1>
KDE voi näyttää ohjelman käynnistyessä hiiriosoittimen ”varattuna”.
Jos haluat käyttää tätä asetusta, valitse haluamasi esitystapa
monivalintalaatikosta.
Kaikki ohjelmat eivät välttämättä kunnioita tapaa. Tällöin osoitin
lakkaa vilkkumasta sen jälkeen, kun aikakatkaisussa annettu aika on
kulunut. <h1>Käynnistymisen ilmaiseminen</h1> Tässä voit määrittää sovellusten käynnistymispalautteen. Vilkkuva osoitin Pomppiva osoitin &Varattu osoitin Näytä &tehtäväpalkin ilmoitus Ei varattua osoitinta Passiivinen varattu osoitin Kä&ynnistyksen ilmaisemisen aikakatkaisu: Tehtäväpalki&n ilmoitus 