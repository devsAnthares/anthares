��          �   %   �      0  &   1  �   X     �     �     �     �                '  	   H     R     c     o     �     �     �     �     �     �  	   �  C   �     #     9  �  G  r     %  w     �     �     �  )   �  7        @     T     e     �  (   �  1   �  ?   �  (   7	     `	  %   }	     �	     �	  (   �	  �   �	  9   �
  ,   �
                                                                             	              
                                    (c) 2002 Karol Szwed, Daniel Molkentin <h1>Style</h1>This module allows you to modify the visual appearance of user interface elements, such as the widget style and effects. Button Checkbox Combobox Con&figure... Daniel Molkentin Description: %1 EMAIL OF TRANSLATORSYour emails Group Box KDE Style Module Karol Szwed NAME OF TRANSLATORSYour names No description available. Preview Radio button Ralf Nolden Tab 1 Tab 2 Text Only There was an error loading the configuration dialog for this style. Unable to Load Dialog Widget style: Project-Id-Version: kcmstyle
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-17 05:44+0100
PO-Revision-Date: 2009-01-13 17:06+0530
Last-Translator: Runa Bhattacharjee <runab@redhat.com>
Language-Team: Bengali INDIA <fedora-trans-bn_IN@redhat.com>
Language: bn_IN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=(n != 1);
 (c) ২০০২ ক্যারোল সজওয়েড, ড্যানিয়েল মোলকেনটিন <h1>Style</h1>এই মডিউলের সাহায্যে, ইউজার ইন্টারফেসের বিভিন্ন সামগ্রী যেমন উইজেটের বিন্যাস ও ইফেক্ট পরিবর্তন করা যাবে। বাটন চেক-বক্স কম্বো-বক্স কনফিগার করুন...(&f) ড্যানিয়েল মোলকেনটিন বিবরণ: %1 runab@redhat.com গ্রুপ বক্স KDE Style মডিউল ক্যারোল সজওয়েড রুণা ভট্টাচার্য্য কোনো বিবরণ উপস্থিত নেই। পূর্বরূপের ঝলক রেডিও বাটন রাল্ফ নোল্ডেন ট্যাব ১ ট্যাব ২ শুধুমাত্র লেখা এই বিন্যাসের জন্য কনফিগারেশন ডায়লগ লোড করতে সমস্যা দেখা দিয়েছে। ডায়লগ লোড করতে ব্যর্থ উইজেটের বিন্যাস: 