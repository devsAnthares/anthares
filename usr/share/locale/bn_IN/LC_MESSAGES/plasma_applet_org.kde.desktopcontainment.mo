��          �      L      �     �     �     �     �     �     �          
               +     1  
   7     B     Q     i     o     �  �  �  !   h  J   �  O   �     %  !   :  0   \  %   �  '   �  E   �     !     ?     L  5   Y  C   �  X   �     ,  I   B  J   �                                     	      
                                                         &Delete &Empty Trash Bin &Move to Trash &Open &Paste &Properties &Reload &Rename Deselect All File types: Icons Large Select All Show All Files Show the Desktop folder Small Specify a folder: Type a path or a URL here Project-Id-Version: plasma_applet_folderview
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:11+0100
PO-Revision-Date: 2008-12-29 16:21+0530
Last-Translator: Runa Bhattacharjee <runab@redhat.com>
Language-Team: Bengali INDIA <fedora-trans-bn_IN@redhat.com>
Language: bn_IN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=(n != 1);
 মুছে ফেলুন (&D) আবর্জনার বাক্স ফাঁকা করুন (&E) আবর্জনার বাক্সে স্থানান্তর (&M) খুলুন (&O) পেস্ট করুন (&P) বিবিধ বৈশিষ্ট্য (&P) নতুন করে লোড (&R) নাম পরিবর্তন (&R) সমগ্র নির্বাচন বাতিল করুন ফাইলের ধরন: আইকন বৃহৎ সমগ্র নির্বাচন করুন সকল ফাইল প্রদর্শন করা হবে ডেস্কটপ ফোল্ডার প্রদর্শন করা হবে ক্ষুদ্র একটি ফোল্ডার নির্ধারণ করুন: এইখানে একটি পাথ অথবা URL লিখুন 