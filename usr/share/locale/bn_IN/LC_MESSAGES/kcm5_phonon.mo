��          �      <      �  m   �          7     ?     ]     z  Y   �     �      �               ;     W     ^     t  %   y     �  �  �  4  y  ^   �       `   *  D   �     �    �  >   �     =	  .   N	  1   }	  5   �	     �	  �   �	     w
  �   �
  �   3                                                          
                                       	    A list of Phonon Backends found on your system.  The order here determines the order Phonon will use them in. Apply Device List To... Backend Copyright 2006 Matthias Kretz Default/Unspecified Category Defer Defines the default ordering of devices which can be overridden by individual categories. Device Preference EMAIL OF TRANSLATORSYour emails Matthias Kretz NAME OF TRANSLATORSYour names Phonon Configuration Module Prefer Show advanced devices Test no preference for the selected device prefer the selected device Project-Id-Version: kcm_phonon
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2009-01-13 11:36+0530
Last-Translator: Runa Bhattacharjee <runab@redhat.com>
Language-Team: Bengali INDIA <fedora-trans-bn_IN@redhat.com>
Language: bn_IN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=(n != 1);
 সিস্টেমের মধ্যে সনাক্ত Phonon ব্যাক-এন্ডের তালিকা।  এইখানে নির্ধারিত অনুক্রম অনুযায়ী Phonon দ্বারা এইগুলি ব্যবহার করা হবে। ডিভাইসের তালিকা প্রয়োগ করার স্থান... ব্যাক-এন্ড স্বত্বাধিকার ২০০৬ ম্যাথায়েস ক্রেটজ ডিফল্ট/অনির্ধারিত শ্রেণী বিলম্বিত ডিভাইসের ডিফল্ট অনুক্রম নির্ধারণ করতে ব্যবহৃত হয়। পৃথক শ্রেণী দ্বারা এই অনুক্রম অগ্রাহ্য করা হতে পারে। ডিভাইস সংক্রান্ত পছন্দ runab@redhat.com ম্যাথায়েস ক্রেটজ রুভা ভট্টাচার্য্য Phonon কনফিগারেশন মডিউল পছন্দ উন্নত বৈশিষ্ট্য বিশিষ্ট ডিভাইস প্রদর্শন করা হবে পরীক্ষা নির্বাচিত ডিভাইসের ক্ষেত্রে কোনো পছন্দসই মান ধার্য করা হবে না নির্বাচিত ডিভাইসটি পছন্দসই ডিভাইস রূপে ধার্য করুন 