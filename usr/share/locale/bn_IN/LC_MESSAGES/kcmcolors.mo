��    4      �  G   \      x  #   y     �     �     �  J   �               !     6     K     ]     i     p     w     �  	   �     �     �     �     �                <  '   B     j  !   ~     �     �     �     �  	   �     �                '     9     E     M     [     v     �     �     �     �     �  	   �     �     �       0     3   B  �  v  v   4
  9   �
     �
     �
  �   �
     �  8   �  K   "  %   n  %   �     �     �     �  �   �     p     �     �     �  "   �  M   �  D   =     �     �  v   �  B     v   `  (   �  A      T   B  (   �     �  (   �  1   	     ;  .   U  (   �     �  ,   �  H   �  ?   6  >   v  +   �  ,   �  &     +   5  %   a  (   �  (   �  "   �     �  w   |           *              %                0   2       '            4   /   $                               )   #   	                
             "          -   (                              ,   &   +   3   !   .                             1              &Enter a name for the color scheme: (c) 2007 Matthew Woehlke 0 1 A color scheme with that name already exists.
Do you want to overwrite it? Active Text Active Titlebar Active Titlebar Text Alternate Background Button Background Button Text Color: Colors Colorset to view/modify Contrast Contrast: Current color schemeCurrent Default color schemeDefault Disabled color Disabled contrast amount Disabled contrast type EMAIL OF TRANSLATORSYour emails Error Get new color schemes from the Internet Import Color Scheme Import a color scheme from a file Inactive Text Inactive Titlebar Inactive Titlebar Text Jeremy Whiting Link Text Matthew Woehlke NAME OF TRANSLATORSYour names New Row Normal Background Normal Text Options Remove Scheme Remove the selected scheme Save Color Scheme Selection Background Selection Text Tooltip Background Tooltip Text View Background View Text Visited Text Window Background Window Text You do not have permission to delete that scheme You do not have permission to overwrite that scheme Project-Id-Version: kcmcolors
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-08 06:27+0100
PO-Revision-Date: 2009-01-06 18:00+0530
Last-Translator: Runa Bhattacharjee <runab@redhat.com>
Language-Team: Bengali INDIA <fedora-trans-bn_IN@redhat.com>
Language: bn_IN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=(n != 1);
 রঙের স্কিম চিহ্নিত করা জন্য একটি নাম লিখুন: (&E) (c) ২০০৭ ম্যাথিউ ওয়েলকে ০ ১ এই নামের একটি রঙের স্কিম বর্তমানে উপস্থিত রয়েছে।
আপনি কি এটি নতুন করে লিখতে ইচ্ছুক? সক্রিয় লেখা সক্রিয় শিরোনামের বার সক্রিয় শিরোনামের বারের লেখা বিকল্প পটভূমি বাটনের পটভূমি বাটনের লেখা রঙ: রং প্রদর্শন/পরিবর্তনের উদ্দেশ্যে চিহ্নিত রঙের সংকলন বৈপরিত্য বৈপরিত্য: বর্তমান ডিফল্ট নিষ্ক্রিয় রঙ নিষ্ক্রিয় বৈপরিত্যের পরিমাণ নিষ্ক্রিয় বৈপরিত্যের ধরন runab@redhat.com ত্রুটি ইন্টারনেট থেকে, রঙের নতুন স্কিম প্রাপ্ত করুন রঙের স্কিম ইম্পোর্ট করুন ফাইল থেকে একটি নতুন রঙের স্কিম ইম্পোর্ট করুন নিষ্ক্রিয় লেখা নিষ্ক্রিয় শিরোনামের বার নিষ্ক্রিয় শিরোনামের বারের লেখা জেরেমি ওয়াইটিং লিংকের লেখা ম্যাথিউ ওয়েলকে রুণা ভট্টাচার্য্য নতুন সারি স্বাভাবিক পটভূমি স্বাভাবিক লেখা বিকল্প স্কিম মুছে ফেলুন নির্বাচিত স্কিম মুছে ফেলুন রঙের স্কিম সংরক্ষণ করুন নির্বাচিত অংশের পটভূমি নির্বাচনের লেখা টুল-টিপের পটভূমি টুল-টিপের লেখা পটভূমি প্রদর্শন লেখা প্রদর্শন পরিদর্শিত লেখা উইন্ডোর পটভূমি উইন্ডোর লেখা আপনি, চিহ্নিত স্কিম মুছে ফেলার অনুমতিপ্রাপ্ত নন এই স্কিম মুছে নতুন করে লেখার অনুমতি আপনার নেই 