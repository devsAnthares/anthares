��          �   %   �      0     1     7  
   �  +   �  
   �     �               ,  	   5      ?  *   `  H   �     �     �     �  )     ;   1  	   m     w  (   �     �  	   �  �  �  "   �    �     �  �   �     v     �  M   �  %   �     	     3	     D	  S   U	  d  �	       1   $  .   V  �   �  �        �  n   �  �   Q  B   �     $                                     	                                                                                
           msec <h1>Multiple Desktops</h1>In this module, you can configure how many virtual desktops you want and how these should be labeled. Animation: Assigned global Shortcut "%1" to Desktop %2 Desktop %1 Desktop %1: Desktop Effect Animation Desktop Names Desktops Duration: EMAIL OF TRANSLATORSYour emails Here you can enter the name for desktop %1 Here you can set how many virtual desktops you want on your KDE desktop. Layout NAME OF TRANSLATORSYour names No Animation No suitable Shortcut for Desktop %1 found Shortcut conflict: Could not set Shortcut %1 for Desktop %2 Shortcuts Show desktop layout indicators Show shortcuts for all possible desktops Switch to Desktop %1 Switching Project-Id-Version: kcm_kwindesktop
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-06 05:35+0100
PO-Revision-Date: 2009-12-30 00:49+0530
Last-Translator: Runa Bhattacharjee <runab@redhat.com>
Language-Team: Bengali INDIA <anubad@lists.ankur.org.in>
Language: bn_IN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=(n != 1);
  মিলিসেকেন্ড <h1>একাধিক ডেস্কটপ</h1>এই মডিউলের সাহায্যে, ভার্চুয়াল ডেস্কটপের সংখ্যা ও সেগুলির নাম কনফিগার করা যাবে। অ্যানিমেশন: সার্বজনীন শর্ট-কাট "%1" নির্ধারিত হয়েছে ডেস্কটপ %2-র জন্য ডেস্কটপ %1 ডেস্কটপ %1: ডেস্কটপ ইফেক্টের অ্যানিমেশন ডেস্কটপের নাম ডেস্কটপ অবকাল: runab@redhat.com ডেস্কটপ %1-র নাম এইখানে লেখা যাবে KDE ডেস্কটপের মধ্যে উপলব্ধ ভার্চুয়াল ডেস্কটপ সংখ্যা এইখানে নির্ধারণ করা যাবে। সংখ্যা পরিবর্তনের জন্য স্লাইডারের স্থান পরিবর্তন করুন। বিন্যাস রুণা ভট্টাচার্য্য অ্যানিমেশন বিহীন ডেস্কটপ %1-র জন্য কোনো প্রযোজ্য শর্ট-কাট পাওয়া যায়নি শর্ট-কাটের মধ্যে দ্বন্দ্ব: শর্ট-কাট %1, ডেস্কটপ %2-র জন্য নির্ধারণ করা যায়নি শর্ট-কাট ডেস্কটপ বিন্যাসের সংকেত প্রদর্শন করা হবে সকল সম্ভাব্য ডেস্কটপের জন্য শর্ট-কাট প্রদর্শন করা হবে ডেস্কটপ %1-এ পরিবর্তন করুন অদলবদল 