��    b      ,  �   <      H     I     _     b     j     w  5   �  #   �  E   �  E   '	  >   m	     �	     �	  7   �	     
     '
     8
     W
     v
     �
     �
     �
     �
     �
     �
     �
     �
          #     (  	   /     9     Q     b  !   t  F   �     �     �       <        Z     b  *   k     �      �     �  	   �     �     �  	   �     �     	                :  
   X     c     �     �     �  
   �  F   �  :   �     7     ?     G     N     a     h  8   w     �     �  	   �  
   �     �     �     �               #     ;     C     a     r     {     �     �  &   �     �  
   �     �     
     *     2     ;     L     a  $   r  �  �     F     \     _     g     t  5   �  #   �  E   �  E   $  >   j     �     �  1   �     �  
             0     I     b     j     v     }     �     �     �     �     �     �     �  	   �     �          #  !   5  F   W     �     �     �  <   �          $  *   -     X      f     �  	   �     �     �  	   �     �     �     �      �     �  
        %     C     P     d  
   l  F   w  :   �     �          	          #     *  8   9     r     y  	   �  
   �     �     �     �     �     �     �     �          #     4     =     \     b     n     t  
   �     �     �     �     �     �     �     �  $   �     K      >   P             M              -      +   H       ?   1      ^   J       Q         [   ;   3   5          6                              &       (           _                 S          G      *   L   Z       X   2      C               V   :   B       #   b   N   `       =       @              9   D   R   F      E       0   ]          a   7   %   A              8          
   <   W   	          I       $   .       )   '   O      U       \   "   4      Y   T   /          ,   !    
Also available in %1 %1 %1 (%2) %1 (Default) <b>%1</b> by %2 <em>%1 out of %2 people found this review useful</em> <em>Tell us about this review!</em> <em>Useful? <a href='true'><b>Yes</b></a>/<a href='false'>No</a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'><b>No</b></a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'>No</a></em> @infoFetching updates @infoFetching... @infoIt is unknown when the last check for updates was @infoLooking for updates @infoNo updates @infoNo updates are available @infoShould check for updates @infoThe system is up to date @infoUpdates @infoUpdating... Accept Addons Aleix Pol Gonzalez An application explorer Apply Changes Available backends:
 Available modes:
 Back Cancel Category: Checking for updates... Comment too long Comment too short Compact Mode (auto/compact/full). Could not close the application, there are tasks that need to be done. Could not find category '%1' Couldn't open %1 Delete the origin Directly open the specified application by its package name. Discard Discover Display a list of entries with a category. Extensions... Failed to remove the source '%1' Help... Homepage: Improve summary Install Installed Jonathan Thomas Launch License: List all the available backends. List all the available modes. Loading... Local package file to install Make default More Information... More... No Updates Open Discover in a said mode. Modes correspond to the toolbar buttons. Open with a program that can deal with the given mimetype. Proceed Rating: Remove Resources for '%1' Review Reviewing '%1' Running as <em>root</em> is discouraged and unnecessary. Search Search in '%1'... Search... Search: %1 Search: %1 + %2 Settings Short summary... Show reviews (%1)... Size: Sorry, nothing found... Source: Specify the new source for %1 Still looking... Summary: Supports appstream: url scheme Tasks Tasks (%1%) TransactioName TransactionStatus%1 %2 Unable to find resource: %1 Update All Update Selected Update section nameUpdate (%1) Updates Version: unknown reviewer updates not selected updates selected © 2010-2016 Plasma Development Team Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:02+0100
PO-Revision-Date: 2017-12-31 17:04+0000
Last-Translator: Steve Allewell <steve.allewell@gmail.com>
Language-Team: British English <kde-i18n-doc@kde.org>
Language: en_GB
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 
Also available in %1 %1 %1 (%2) %1 (Default) <b>%1</b> by %2 <em>%1 out of %2 people found this review useful</em> <em>Tell us about this review!</em> <em>Useful? <a href='true'><b>Yes</b></a>/<a href='false'>No</a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'><b>No</b></a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'>No</a></em> Fetching updates Fetching... It is unknown when the last check for updates was Looking for updates No updates No updates are available Should check for updates The system is up to date Updates Updating... Accept Addons Aleix Pol Gonzalez An application explorer Apply Changes Available backends:
 Available modes:
 Back Cancel Category: Checking for updates... Comment too long Comment too short Compact Mode (auto/compact/full). Could not close the application, there are tasks that need to be done. Could not find category '%1' Could not open %1 Delete the origin Directly open the specified application by its package name. Discard Discover Display a list of entries with a category. Extensions... Failed to remove the source '%1' Help... Homepage: Improve summary Install Installed Jonathan Thomas Launch Licence: List all the available backends. List all the available modes. Loading... Local package file to install Make default More Information... More... No Updates Open Discover in a said mode. Modes correspond to the toolbar buttons. Open with a program that can deal with the given mimetype. Proceed Rating: Remove Resources for '%1' Review Reviewing '%1' Running as <em>root</em> is discouraged and unnecessary. Search Search in '%1'... Search... Search: %1 Search: %1 + %2 Settings Short summary... Show reviews (%1)... Size: Sorry, nothing found... Source: Specify the new source for %1 Still looking... Summary: Supports appstream: url scheme Tasks Tasks (%1%) %1 %2 Unable to find resource: %1 Update All Update Selected Update (%1) Updates Version: unknown reviewer updates not selected updates selected © 2010-2016 Plasma Development Team 