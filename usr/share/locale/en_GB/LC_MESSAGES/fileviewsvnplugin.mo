��          �   %   �      p     q  +   �  .   �  6   �  *     #   D  &   h  /   �  2   �  :   �  0   -  3   ^  ;   �  K   �  -     $   H  '   m     �     �     �     �     �  #        1     O     c     |  �  �     :     A  !   `  )   �     �     �     �  "   �  %   	  -   D	  #   r	  &   �	  .   �	  >   �	      +
     L
     d
     
     �
  
   �
  
   �
  
   �
     �
     �
     �
  
   �
     �
                                                    	   
                                                                    @action:buttonCommit @info:statusAdded files to SVN repository. @info:statusAdding files to SVN repository... @info:statusAdding of files to SVN repository failed. @info:statusCommit of SVN changes failed. @info:statusCommitted SVN changes. @info:statusCommitting SVN changes... @info:statusRemoved files from SVN repository. @info:statusRemoving files from SVN repository... @info:statusRemoving of files from SVN repository failed. @info:statusReverted files from SVN repository. @info:statusReverting files from SVN repository... @info:statusReverting of files from SVN repository failed. @info:statusSVN status update failed. Disabling Option "Show SVN Updates". @info:statusUpdate of SVN repository failed. @info:statusUpdated SVN repository. @info:statusUpdating SVN repository... @item:inmenuSVN Add @item:inmenuSVN Commit... @item:inmenuSVN Delete @item:inmenuSVN Revert @item:inmenuSVN Update @item:inmenuShow Local SVN Changes @item:inmenuShow SVN Updates @labelDescription: @title:windowSVN Commit Show updates Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2015-11-14 23:03+0000
Last-Translator: Steve Allewell <steve.allewell@gmail.com>
Language-Team: British English <kde-l10n-en_gb@kde.org>
Language: en_GB
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 Commit Added files to SVN repository. Adding files to SVN repository... Adding of files to SVN repository failed. Commit of SVN changes failed. Committed SVN changes. Committing SVN changes... Removed files from SVN repository. Removing files from SVN repository... Removing of files from SVN repository failed. Reverted files from SVN repository. Reverting files from SVN repository... Reverting of files from SVN repository failed. SVN status update failed. Disabling Option "Show SVN Updates". Update of SVN repository failed. Updated SVN repository. Updating SVN repository... SVN Add SVN Commit... SVN Delete SVN Revert SVN Update Show Local SVN Changes Show SVN Updates Description: SVN Commit Show updates 