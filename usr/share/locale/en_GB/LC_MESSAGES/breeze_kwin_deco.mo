��    B      ,  Y   <      �     �     �     �  !   �  "   �  &     ,   /  #   \  &   �  !   �  &   �  '   �  "     "   ;  '   ^     �  +   �  2   �     �  
   �     
          %     ,     @     H     O     b     {  !   �  +   �     �     �      �     	     (	     F	     U	     ]	  !   s	     �	  	   �	     �	     �	     �	     �	  &   �	     !
     @
     G
     b
     h
     p
     w
     |
     �
  $   �
     �
     �
     �
     �
          (     5  >   O  �  �     ?     C     E     a     f  	   l     v     �  	   �     �  	   �  
   �     �     �  
   �     �  +   �  2   �     +  
   A     L     Z     g     n     �     �     �     �     �  !   �  +   �          5      :     [     m     �     �     �  !   �     �  	   �     �     �          ,  &   ?     f     �     �     �     �     �     �     �     �  $   �                +     =     S     m     z  
   �     '             6      8             $       %   <   ,      B                     "   >       #              	   @      =   7   3                          +      
   &   !               0   :      ;           (   4              2                      A       -   )      9          *         1       /          .   ?   5         ms % &Matching window property:  @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Border @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large @item:inlistbox Button size:Large @item:inlistbox Button size:Small @item:inlistbox Button size:Very Large Add Add handle to resize windows with no border Allow resizing maximized windows from window edges Anima&tions duration: Animations B&utton size: Border size: Center Center (Full Width) Class:  Color: Decoration Options Detect Window Properties Dialog Draw a circle around close button Draw separator between Title Bar and Window Draw window background gradient Edit Edit Exception - Breeze Settings Enable animations Enable/disable this exception Exception Type General Hide window title bar Information about Selected Window Left Move Down Move Up New Exception - Breeze Settings Question - Breeze Settings Regular Expression Regular Expression syntax is incorrect Regular expression &to match:  Remove Remove selected exception? Right Shadows Si&ze: Tiny Tit&le alignment: Title:  Use window class (whole application) Use window title Warning - Breeze Settings Window Class Name Window Identification Window Property Selection Window Title Window-Specific Overrides strength of the shadow (from transparent to opaque)S&trength: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-09 03:20+0100
PO-Revision-Date: 2017-12-29 20:02+0000
Last-Translator: Steve Allewell <steve.allewell@gmail.com>
Language-Team: British English <kde-l10n-en_GB@kde.org>
Language: en_GB
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
  ms % &Matching window property:  Huge Large No Border No Side Borders Normal Oversized Tiny Very Huge Very Large Large Small Very Large Add Add handle to resize windows with no border Allow resizing maximised windows from window edges Anima&tions duration: Animations B&utton size: Border size: Centre Centre (Full Width) Class:  Colour: Decoration Options Detect Window Properties Dialogue Draw a circle around close button Draw separator between Title Bar and Window Draw window background gradient Edit Edit Exception - Breeze Settings Enable animations Enable/disable this exception Exception Type General Hide window title bar Information about Selected Window Left Move Down Move Up New Exception - Breeze Settings Question - Breeze Settings Regular Expression Regular Expression syntax is incorrect Regular expression &to match:  Remove Remove selected exception? Right Shadows Si&ze: Tiny Tit&le alignment: Title:  Use window class (whole application) Use window title Warning - Breeze Settings Window Class Name Window Identification Window Property Selection Window Title Window-Specific Overrides S&trength: 