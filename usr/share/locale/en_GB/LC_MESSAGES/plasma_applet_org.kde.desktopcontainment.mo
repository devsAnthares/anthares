��    `        �         (     )     1     B     Q     W     ^     j     {     �     �  /   �  -   �     �     �  
   		  
   	     	     ,	     1	     8	     @	     R	     _	     d	  
   l	     w	     �	     �	     �	  	   �	     �	  	   �	     �	     �	     �	     
     
  	   #
     -
     4
     H
  	   M
     W
     ]
     c
     h
     m
  	   v
     �
     �
     �
     �
     �
     �
     �
     �
  <   �
               /     6     =     X     ^     e     j  
   ~     �     �     �     �     �  )   �               5     :     @     M     U     ]     f  
   x     �     �  	   �     �     �     �     �     �     �     �  E   �  *   A  �  l          #     3     E     K     R     ^     o     }     �  	   �  
   �     �     �  
   �  
   �     �     �     �     �     �     �     
       
        "     .     ;     J  	   b     l  	        �     �     �     �     �  	   �     �     �     �  	   �                           	   !     +     9     @     G     _     d     i     l  <   z     �     �     �     �     �     �     �     �       
        !     /     A     P     d  )   r     �     �     �     �     �     �     �     �     �  
                	   '     1     8     =     W     `     r     �  E   �  	   �     J       -      M       %   Q           ^              U       L   S   $      .       H       X          Y   8   *         <   &          '       ]   N       3                     `          #   V             /   T   G   9   @   >   =   6   2   O   C   F   ?      0      
      A   E       	      P              _                D          Z               1   R   ,   +                \           I   )   B   K      :   !   W   4          ;               (      5   "       7       [           &Delete &Empty Trash Bin &Move to Trash &Open &Paste &Properties &Refresh Desktop &Refresh View &Reload &Rename @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Align Appearance: Arrange In Arrange in Arrangement: Back Cancel Columns Configure Desktop Custom title Date Default Descending Description Deselect All Desktop Layout Enter custom title here Features: File name pattern: File type File types: Filter Folder preview popups Folders First Folders first Full path Got it Hide Files Matching Huge Icon Size Icons Large Left List Location Location: Lock in place Locked Medium More Preview Options... Name None OK Panel button: Press and hold widgets to move them and reveal their handles Preview Plugins Preview thumbnails Remove Resize Restore from trashRestore Right Rotate Rows Search file type... Select All Select Folder Selection markers Show All Files Show Files Matching Show a place: Show files linked to the current activity Show the Desktop folder Show the desktop toolbox Size Small Small Medium Sort By Sort by Sorting: Specify a folder: Text lines Tiny Title: Tool tips Tweaks Type Type a path or a URL here Unsorted Use a custom icon Widget Handling Widgets unlocked You can press and hold widgets to move them and reveal their handles. whether to use icon or list viewView mode Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:11+0100
PO-Revision-Date: 2017-11-26 15:40+0000
Last-Translator: Steve Allewell <steve.allewell@gmail.com>
Language-Team: British English <kde-i18n-doc@kde.org>
Language: en_GB
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 &Delete &Empty Wastebin &Move to Wastebin &Open &Paste &Properties &Refresh Desktop &Refresh View &Reload &Rename Choose... Clear Icon Align Appearance: Arrange In Arrange in Arrangement: Back Cancel Columns Configure Desktop Custom title Date Default Descending Description Deselect All Desktop Layout Enter custom title here Features: File name pattern: File type File types: Filter Folder preview popups Folders First Folders first Full path Got it Hide Files Matching Huge Icon Size Icons Large Left List Location Location: Lock in place Locked Medium More Preview Options... Name None OK Panel button: Press and hold widgets to move them and reveal their handles Preview Plugins Preview thumbnails Remove Resize Restore Right Rotate Rows Search file type... Select All Select Folder Selection markers Show All Files Show Files Matching Show a place: Show files linked to the current activity Show the Desktop folder Show the desktop toolbox Size Small Small Medium Sort By Sort by Sorting: Specify a folder: Text lines Tiny Title: Tool tips Tweaks Type Type a path or a URL here Unsorted Use a custom icon Widget Handling Widgets unlocked You can press and hold widgets to move them and reveal their handles. View mode 