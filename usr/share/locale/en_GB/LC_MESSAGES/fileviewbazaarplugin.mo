��    !      $  /   ,      �  .   �  1     9   J     �  -   �  &   �  )   �  .   #  &   R  )   y  .   �  &   �  )   �  2   #  5   V  =   �  #   �     �  !     '   /  "   W  0   z  '   �  *   �     �          7     R     j     �     �  &   �  �  �  !   u	  $   �	  ,   �	     �	      �	     
     7
  !   T
     v
     �
  !   �
     �
     �
  %     (   ,  0   U     �     �     �     �     �  #   �          4     R     `     q  
        �     �     �     �                                                              	                                
                                    !                             @info:statusAdded files to Bazaar repository. @info:statusAdding files to Bazaar repository... @info:statusAdding of files to Bazaar repository failed. @info:statusBazaar Log closed. @info:statusCommit of Bazaar changes failed. @info:statusCommitted Bazaar changes. @info:statusCommitting Bazaar changes... @info:statusPull of Bazaar repository failed. @info:statusPulled Bazaar repository. @info:statusPulling Bazaar repository... @info:statusPush of Bazaar repository failed. @info:statusPushed Bazaar repository. @info:statusPushing Bazaar repository... @info:statusRemoved files from Bazaar repository. @info:statusRemoving files from Bazaar repository... @info:statusRemoving of files from Bazaar repository failed. @info:statusReview Changes failed. @info:statusReviewed Changes. @info:statusReviewing Changes... @info:statusRunning Bazaar Log failed. @info:statusRunning Bazaar Log... @info:statusUpdate of Bazaar repository failed. @info:statusUpdated Bazaar repository. @info:statusUpdating Bazaar repository... @item:inmenuBazaar Add... @item:inmenuBazaar Commit... @item:inmenuBazaar Delete @item:inmenuBazaar Log @item:inmenuBazaar Pull @item:inmenuBazaar Push @item:inmenuBazaar Update @item:inmenuShow Local Bazaar Changes Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2014-06-20 00:37+0100
Last-Translator: Steve Allewell <steve.allewell@gmail.com>
Language-Team: British English <kde-l10n-en_gb@kde.org>
Language: en_GB
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
 Added files to Bazaar repository. Adding files to Bazaar repository... Adding of files to Bazaar repository failed. Bazaar Log closed. Commit of Bazaar changes failed. Committed Bazaar changes. Committing Bazaar changes... Pull of Bazaar repository failed. Pulled Bazaar repository. Pulling Bazaar repository... Push of Bazaar repository failed. Pushed Bazaar repository. Pushing Bazaar repository... Removed files from Bazaar repository. Removing files from Bazaar repository... Removing of files from Bazaar repository failed. Review Changes failed. Reviewed Changes. Reviewing Changes... Running Bazaar Log failed. Running Bazaar Log... Update of Bazaar repository failed. Updated Bazaar repository. Updating Bazaar repository... Bazaar Add... Bazaar Commit... Bazaar Delete Bazaar Log Bazaar Pull Bazaar Push Bazaar Update Show Local Bazaar Changes 