��          �      <      �     �     �     �  +   �  @        L     a     i     w     }     �  
   �     �     �     �     �     �  �  
     �     �     �  -   �  @        X     m     u     �     �     �  
   �     �     �     �     �     �     
         	                                                                                       <a href='%1'>%1</a> Close Copy Automatically: Don't show this dialog, copy automatically. Drop text or an image onto me to upload it to an online service. Error during upload. General History Size: Paste Please wait Please, try again. Sending... Share Shares for '%1' Successfully uploaded The URL was just shared Upload %1 to an online service Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-01 04:04+0100
PO-Revision-Date: 2017-12-30 12:14+0000
Last-Translator: Steve Allewell <steve.allewell@gmail.com>
Language-Team: British English <kde-l10n-en_GB@kde.org>
Language: en_GB
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 <a href='%1'>%1</a> Close Copy Automatically: Don't show this dialogue, copy automatically. Drop text or an image onto me to upload it to an online service. Error during upload. General History Size: Paste Please wait Please, try again. Sending... Share Shares for '%1' Successfully uploaded The URL was just shared Upload %1 to an online service 