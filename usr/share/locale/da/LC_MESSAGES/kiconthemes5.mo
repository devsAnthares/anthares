��          �      L      �  
   �     �  >   �                  
   -     8     @     H     O  	   [     e     s     z  2   �     �     �  �  �     w     �  >   �  
   �     �  
   �  
   �     �     �  	     	     	             .     5  0   D  
   u     �                                   	                                                         
        &Browse... &Search: *.png *.xpm *.svg *.svgz|Icon Files (*.png *.xpm *.svg *.svgz) Actions All Applications Categories Devices Emblems Emotes Icon Source Mimetypes O&ther icons: Places S&ystem icons: Search interactively for icon names (e.g. folder). Select Icon Status Project-Id-Version: kio4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:11+0100
PO-Revision-Date: 2017-02-25 18:33+0100
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <kde-i18n-doc@kde.org>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 &Gennemse... &Søg: *.png *.xpm *.svg *.svgz|Ikon-filer (*.png *.xpm *.svg *.svgz) Handlinger Alle Programmer Kategorier Enheder Emblemer Smiletegn Ikonkilde Mimetyper A&ndre ikoner: Steder S&ystemikoner: Søg interaktivt efter ikonnavne (f.eks. mappe). Vælg ikon Status 