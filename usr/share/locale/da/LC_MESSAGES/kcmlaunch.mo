��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  �  K     �  #   �  �  #  �  �  Q   E
     �
     �
     �
  )   �
     �
     	  #         C                                          
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-06-01 11:35+0200
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <dansk@dansk-gruppen.dk>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=2; plural=n != 1;
  sek &Tidsudløb for opstartsindikation: Man kan aktivere en anden metode til opstartsbekendtgørelse som
bliver brugt af opgavelinjen hvor en knap med et roterende timeglas vises,
hvilket symboliserer at dit startede program indlæses.
Det kan ske at visse programmer ikke er klar over denne 
opstartsbekendtgørelse. I disse tilfælde vil markøren holde op med at blinke 
efter tiden givet i afsnittet "Tidsudløb for opstartsindikation" <h1>Optagetmarkør</h1>
KDE tilbyder en optagetmarkør til bekendtgørelse af programopstart.
For at aktivere optagetmarkør, vælges en type visuel tilbagemelding
fra kombinationsfeltet
Det kan ske at visse programmer ikke er klar over denne
opstartsbekendtgørelse. I disse tilfælde vil markøren holde op med at blinke 
efter den tid der er angivet i afsnittet "Tidsudløb for opstartsindikation". <h1>Opstartstilbagemelding</h1> Du kan indstille programstart-tilbagemelding her. Blinkende markør Hoppende markør &Optagetmarkør Aktivér bekendtgørelse via &opgavelinje Ingen optagetmarkør Passiv optagetmarkør Tids&udløb for opstartsindikation: Beke&ndtgørelse via opgavelinje 