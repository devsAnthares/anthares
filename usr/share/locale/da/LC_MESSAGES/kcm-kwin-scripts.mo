��          �      �       0  (   1     Z      q     �     �     �     �     �     �  `     ^   u     �  �  �  (   �     �     �     �     �     	     "     /     L  #   ]     �     �        
              	                                    *.kwinscript|KWin scripts (*.kwinscript) Configure KWin scripts EMAIL OF TRANSLATORSYour emails Get New Scripts... Import KWin Script Import KWin script... KWin Scripts KWin script configuration NAME OF TRANSLATORSYour names Placeholder is error message returned from the install serviceCannot import selected script.
%1 Placeholder is name of the script that was importedThe script "%1" was successfully imported. Tamás Krutki Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-19 03:12+0100
PO-Revision-Date: 2018-01-28 14:08+0100
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <kde-i18n-doc@kde.org>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 *.kwinscript|KWin-scripts (*.kwinscript) Indstil KWin-scripts mschlander@opensuse.org Hent nyt scripts... Importér KWin-script... Importér KWin-script... KWin-scripts Konfiguration af KWin-script Martin Schlander Kan ikke importere valgt script.
%1 Scriptet "%1" blev importeret. Tamás Krutki 