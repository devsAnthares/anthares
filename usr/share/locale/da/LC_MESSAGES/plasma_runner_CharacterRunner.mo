��    
      l      �       �      �            	               .  I   3     }  	   �  �  �     9     G     W     ]     d     �  O   �     �  	   �               
   	                           &Trigger word: Add Item Alias Alias: Character Runner Config Code Creates Characters from :q: if it is a hexadecimal code or defined alias. Delete Item Hex Code: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2010-08-02 09:58+0200
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <dansk@dansk-gruppen.dk>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=2; plural=n != 1;
 &Udløserord: Tilføj element Alias Alias: Konfiguration af tegn-runner Kode Opretter tegn fra :q: hvis det er en hexadecimal-kode eller et defineret alias. Slet element Hex-kode: 