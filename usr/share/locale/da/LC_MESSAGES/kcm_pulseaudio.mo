��    $      <  5   \      0     1     6  )   J     t  $   �  &   �  #   �  !   �  "   !     D     T     f     z     �  J   �     �  L        [     c     k      {     �  
   �     �     �     �     �     �  "   �       )   9  <   c     �  ;   �     �  �       �  
   �     �     �     �     	  "   (	  !   K	  "   m	     �	  
   �	  	   �	  
   �	     �	  T   �	     
  N   4
  	   �
     �
     �
     �
     �
     �
     �
     �
     �
  
                  "  )   5  ;   _     �     �     �     	                                                                         !       #             "                                        
           $                               100% @info:creditAuthor @info:creditCopyright 2015 Harald Sitter @info:creditHarald Sitter @labelNo Applications Playing Audio @labelNo Applications Recording Audio @labelNo Device Profiles Available @labelNo Input Devices Available @labelNo Output Devices Available @labelProfile: @titlePulseAudio @title:tabAdvanced @title:tabApplications @title:tabDevices Add virtual output device for simultaneous output on all local sound cards Advanced Output Configuration Automatically switch all running streams when a new output becomes available Capture Default Device Profiles EMAIL OF TRANSLATORSYour emails Inputs Mute audio NAME OF TRANSLATORSYour names Notification Sounds Outputs Playback Port Port is unavailable (unavailable) Port is unplugged (unplugged) Requires 'module-gconf' PulseAudio module This module allows to set up the Pulseaudio sound subsystem. label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-03 03:00+0200
PO-Revision-Date: 2017-05-30 22:17+0100
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <kde-i18n-doc@kde.org>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 100% Ophavsmand Ophavsret 2015 Harald Sitter Harald Sitter Ingen programmer spiller lyd Ingen programmer optager lyd Ingen enhedsprofiler tilgængelige Ingen input-enheder tilgængelige Ingen output-enheder tilgængelige Profil: PulseAudio Avanceret Programmer Enheder Tilføj virtuelle output-enheder for at få output på alle lokale lydkort samtidigt Avanceret output-konfiguration Skift automatisk alle kørende streams når et nyt output bliver tilgængeligt Optagelse Standard Enhedsprofiler mschlander@opensuse.org Input Sæt på lydløs Martin Schlander Bekendtgørelseslyde Output Afspilning Port  (utilgængelig)  (ikke tilsluttet) Kræver PulseAudio-modulet "module-gconf" Dette modul lader dig sætte lyd-subsystemet Pulseaudio op. %1: %2 100% %1% 