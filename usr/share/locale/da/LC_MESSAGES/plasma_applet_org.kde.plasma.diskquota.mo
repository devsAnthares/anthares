��    
      l      �       �   
   �      �        .   0     _     t     �  (   �  8   �  �    	   �  !   �     �  A   �     =     Z     c     l     }        
                             	        Disk Quota No quota restrictions found. Please install 'quota' Quota tool not found.

Please install 'quota'. Running quota failed e.g.: 12 GiB of 20 GiB%1 of %2 e.g.: 8 GiB free%1 free example: Quota: 83% usedQuota: %1% used usage of quota, e.g.: '/home/bla: 38% used'%1: %2% used Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2016-12-15 20:34+0100
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <kde-i18n-doc@kde.org>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Diskkvote Ingen kvotebegrænsninger fundet. Installér venligst "quota" Kvote-værktøjet blev ikke fundet.

Installér venligst "quota". Kørsel af quota mislykkedes %1 af %2 %1 ledig Kvote: %1% brugt %1: %2% brugt 