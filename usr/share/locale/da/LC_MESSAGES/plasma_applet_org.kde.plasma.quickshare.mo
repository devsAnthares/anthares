��          �      <      �     �     �     �  +   �  @        L     a     i     w     }     �  
   �     �     �     �     �     �  �  
     �     �     �  *   �  N        T     g     p     �     �     �  	   �     �     �     �     �     �     
         	                                                                                       <a href='%1'>%1</a> Close Copy Automatically: Don't show this dialog, copy automatically. Drop text or an image onto me to upload it to an online service. Error during upload. General History Size: Paste Please wait Please, try again. Sending... Share Shares for '%1' Successfully uploaded The URL was just shared Upload %1 to an online service Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-01 04:04+0100
PO-Revision-Date: 2015-08-26 20:18+0200
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <kde-i18n-doc@kde.org>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 <a href='%1'>%1</a> Luk Kopiér automatisk: Vis ikke denne dialog, kopiér automatisk. Slip tekst eller et billede på mig for at uploade det til en online tjeneste. Fejl under upload. Generelt Historikstørrelse: Indsæt Vent venligst Prøv igen. Sender... Del Delinger for "%1" Upload gennemført URL'en blev netop delt Upload %1 til en onlinetjeneste 