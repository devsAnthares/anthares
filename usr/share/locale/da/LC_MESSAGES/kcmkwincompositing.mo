��    3      �  G   L      h  6   i  M   �  K   �     :  '   C     k     r  �   �     ;  	   R  A   \  >   �  9   �  9     9   Q  W   �  E   �     )     :      @     a  7   ~      �     �     �  X   �     X	     `	     v	  �   �	     '
     F
     L
     c
  
   s
  
   ~
     �
     �
  E  �
          &     <  w   O     �     �     �     �     �  	          g  #  9   �  \   �  J   "     m  (   u     �     �  �   �     y  
   �     �     �     �     �     �  *   �     �               #     ;  A   [  "   �     �     �  l   �     Y     g     |  �   �     <     M     T     j  
   �  
   �     �     �  n  �          /     E  s   Y     �     �     �     �  $   �     !     /         /   +   )               ,      .                 	          &                       %   #                       '       0       3                         -       1   2                        (   
      *   !         $                           "    "Full screen repaints" can cause performance problems. "Only when cheap" only prevents tearing for full screen changes like a video. "Re-use screen content" causes severe performance problems on MESA drivers. Accurate Allow applications to block compositing Always Animation speed: Applications can set a hint to block compositing when the window is open.
 This brings performance improvements for e.g. games.
 The setting can be overruled by window-specific rules. Author: %1
License: %2 Automatic Category of Desktop Effects, used as section headerAccessibility Category of Desktop Effects, used as section headerAppearance Category of Desktop Effects, used as section headerCandy Category of Desktop Effects, used as section headerFocus Category of Desktop Effects, used as section headerTools Category of Desktop Effects, used as section headerVirtual Desktop Switching Animation Category of Desktop Effects, used as section headerWindow Management Configure filter Crisp EMAIL OF TRANSLATORSYour emails Enable compositor on startup Exclude Desktop Effects not supported by the Compositor Exclude internal Desktop Effects Full screen repaints Get New Effects... Hint: To find out or configure how to activate an effect, look at the effect's settings. Instant KWin development team Keep window thumbnails: Keeping the window thumbnail always interferes with the minimized state of windows. This can result in windows not suspending their work when minimized. NAME OF TRANSLATORSYour names Never Only for Shown Windows Only when cheap OpenGL 2.0 OpenGL 3.1 OpenGL Platform InterfaceEGL OpenGL Platform InterfaceGLX OpenGL compositing (the default) has crashed KWin in the past.
This was most likely due to a driver bug.
If you think that you have meanwhile upgraded to a stable driver,
you can reset this protection but be aware that this might result in an immediate crash!
Alternatively, you might want to use the XRender backend instead. Re-enable OpenGL detection Re-use screen content Rendering backend: Scale method "Accurate" is not supported by all hardware and can cause performance regressions and rendering artifacts. Scale method: Search Smooth Smooth (slower) Tearing prevention ("vsync"): Very slow XRender Project-Id-Version: kcmkwincompositing
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2017-11-06 18:52+0200
Last-Translator: scootergrisen
Language-Team: Danish
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
 "Hele skærmen gentegnes" kan give problemer med ydelsen. "Kun når det er billigt" forhindrer kun tearing ved fuldskærm-ændringer, såsom en video. "Genbrug skærmindhold" giver alvorlige ydelsesproblemer med MESA-drivere. Præcis Tillad programmer at blokere compositing Altid Animationshastighed: Programmer kan give et vink for at blokere compositing når vinduet er åbent.
Det giver ydelsesforbedringer f.eks. til spil.
Denne indstilling kan tilsidesættes af vinduesspecifikke regler. Ophavsmand: %1
Licens: %2 Automatisk Tilgængelighed Udseende Øjeguf Fokus Værktøjer Animation til skift af virtuelt skrivebord Vindueshåndtering Indstil filter Klar mschlander@opensuse.org Aktivér compositor ved opstart Udelad skrivebordseffekter som ikke understøttes af compositoren Udelad interne skrivebordseffekter Hele skærmen gentegnes Hent ny effekter... Tip: For at finde ud af, eller konfigurere, hvordan en effekt aktiveres, så se på effektens indstillinger. Øjeblikkelig KWins udviklingshold Behold miniaturer af vinduer: Altid at beholde vinduesminiaturen interfererer med vinduers minimerede tilstand. Dette kan medføre at vinduer ikke suspenderer deres arbejde når de minimeres. Martin Schlander Aldrig Kun for viste vinduer Kun når det er billigt OpenGL 2.0 OpenGL 3.1 EGL GLX OpenGL compositing (standard) har tidligere fået KWin til at bryde sammen.
Dette skyldtes sandsynligvis en fejl i driveren.
Hvis du tror at du i mellemtiden har opgraderet til en stabil driver,
kan du nulstille denne beskyttelse, men vær opmærksom på at dette kan medføre et øjeblikkeligt sammenbrud!
Alternativt kan overveje at bruge XRender-motoren i stedet. Genaktivér detektion af OpenGL Genbrug skærmindhold Renderings-backend: Skaleringsmetoden "Præcis" understøttes ikke af al hardware og kan give forringet ydelse og renderingsartefakter. Skaleringsmetode: Søg Glat Glat (langsommere) Forhindring af artefakter ("vsync"): Meget langsom XRender 