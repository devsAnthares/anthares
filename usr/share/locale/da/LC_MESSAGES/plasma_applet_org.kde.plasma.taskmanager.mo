��    Q      �  m   ,      �     �     �     �                      .   !  U   P     �     �      �     �  /   �  /   -     ]     i     r  
   ~     �     �  $   �     �     �     �     �     	     	  	   *	     4	  
   F	  7   Q	     �	     �	     �	     �	  	   �	     �	  !   �	     
     
  	   !
      +
     L
     Y
     r
     �
     �
     �
     �
     �
     �
     �
  (   �
       =   '  2   e     �     �  "   �     �       J     1   Y  )   �  (   �  '   �  "     4   )     ^     l     r     {     �     �     �  U   �  N   !  9   p  J   �  �  �     �     �     �     �     �  	   �     �     �     �          4  '   E  
   m  4   x  4   �  
   �  	   �     �               *  %   B     h     v     �     �     �     �     �     �     �     �     �               3  
   8     C  $   K     p     �  	   �  $   �     �     �     �             
        (     /     D     _  &   o     �     �     �     �     �     �     �     �     �     �  *     +   B  '   n  !   �  3   �     �     �  
             !     (     -  	   3     =     Q  !   e        L   )             O                      4              6   $   ?   3         B              (   K   Q               ;       .   '   D   2   -       J   >   !   
   8                       P   5         ,          7   <           F      *   @       H   1                 "          9      +   &   0           /   N          #       E   	             I   A   =   %      G   :   C                   M    &All Desktops &Close &Fullscreen &Move &New Desktop &Pin &Shade 1 = number of desktop, 2 = desktop name&%1 %2 Activities a window is currently on (apart from the current one)Also available on %1 Add To Current Activity All Activities Allow this program to be grouped Alphabetically Always arrange tasks in columns of as many rows Always arrange tasks in rows of as many columns Arrangement Behavior By Activity By Desktop By Program Name Close Window or Group Cycle through tasks with mouse wheel Do Not Group Do Not Sort Filters Forget Recent Documents General Grouping and Sorting Grouping: Highlight windows Icon size: Invalid number of new messages, overlay, keep short— Keep &Above Others Keep &Below Others Keep launchers separate Large Ma&ximize Manually Mark applications that play audio Maximum columns: Maximum rows: Mi&nimize Minimize/Restore Window or Group More Actions Move &To Current Desktop Move To &Activity Move To &Desktop Mute New Instance On %1 On All Activities On The Current Activity On middle-click: Only group when the task manager is full Open groups in popups Open or bring to the front window of media player appRestore Over 9999 new messages, overlay, keep short9,999+ Pause playbackPause Play next trackNext Track Play previous trackPrevious Track Quit media player appQuit Re&size Remove launcher button for application shown while it is not runningUnpin Show all user Places%1 more Place %1 more Places Show only tasks from the current activity Show only tasks from the current desktop Show only tasks from the current screen Show only tasks that are minimized Show progress and status information in task buttons Show tooltips Small Sorting: Start New Instance Start playbackPlay Stop playbackStop The click actionNone Toggle action for showing a launcher button while the application is not running&Pin When clicking it would toggle grouping windows of a specific appGroup/Ungroup Which activities a window is currently onAvailable on %1 Which virtual desktop a window is currently onAvailable on all activities Project-Id-Version: plasma_applet_tasks
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2018-01-28 14:06+0100
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <kde-i18n-doc@kde.org>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 &Alle skriveborde &Luk &Fuldskærm &Flyt &Nyt skrivebord &Fastgør &Skyg &%1 %2 Også tilgængelig på %1 Føj til nuværende aktivitet Alle aktiviteter Tillad dette program at blive grupperet Alfabetisk Opstil altid opgaver i kolonner af så mange rækker Opstil altid opgaver i rækker af så mange kolonner Opstilling Opførsel Efter aktivitet Efter skrivebord Efter programnavn Luk vindue eller gruppe Kør igennem opgaverne med musehjulet Gruppér ikke Sortér ikke Filtre Glem nylige dokumenter Generelt Gruppering og sortering Gruppering: Fremhæv vinduer Ikonstørrelse: — Hold over &andre Hold &under andre Hold programstartere adskilt Stor Ma&ksimér Manuelt Markér programmer som afspiller lyd Maks. kolonner: Maks. rækker: Mi&nimér Minimér/genskab vindue eller gruppe Flere handlinger Flyt &til aktuelt skrivebord Flyt til &aktivitet Flyt til skrivebor&d Lydløs Ny instans På %1 På alle aktiviteter På den aktuelle aktivitet Ved midterklik: Gruppér kun når opgavelinjen er fuld Åbn grupper i pop-op Genskab 9.999+ Pause Næste spor Forrige spor Afslut Ændr &størrelse Frigør %1 sted mere %1 steder mere Vis kun opgaver fra den aktuelle aktivitet Vis kun opgaver fra det aktuelle skrivebord Vis kun opgaver fra den aktuelle skærm Vis kun opgaver som er minimerede Vis fremgangs- og statusinformation i opgaveknapper Vis værktøjstips Lille Sortering: Start ny instans Afspil Stop Ingen &Fastgør Gruppér/afgruppér Tilgængelig på %1 Tilgængelig på alle aktiviteter 