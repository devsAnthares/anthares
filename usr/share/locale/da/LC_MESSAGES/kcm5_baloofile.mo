��          �            h     i     �  #   �      �      �     �  J        [  &   y     �  T   �       *   $     O  U  ]     �     �  #   �          &     >  W   S     �  )   �     �  g        m  !   }     �                            	                                             
       Also index file content Configure File Search Copyright 2007-2010 Sebastian Trüg Do not search in these locations EMAIL OF TRANSLATORSYour emails Enable File Search File Search helps you quickly locate all your files based on their content Folder %1 is already excluded Folder's parent %1 is already excluded NAME OF TRANSLATORSYour names Not allowed to exclude root folder, please disable File Search if you do not want it Sebastian Trüg Select the folder which should be excluded Vishesh Handa Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2017-11-06 18:50+0200
Last-Translator: scootergrisen
Language-Team: Danish
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
 Indeksér også filindhold Indstil filsøgning Ophavsret 2007-2010 Sebastian Trüg Søg ikke på disse placeringer mschlander@opensuse.org Aktivér filsøgning Filsøgning hjælper dig med hurtigt at finde alle dine filer baseret på deres indhold Mappen %1 er allerede undtaget Mappens overmappe %1 er allerede undtaget Martin Schlander Det er ikke tilladt er ekskluderer rodmappen. Deaktivér venligst filsøgning hvis du ikke vil have det Sebastian Trüg Vælg den mappe som skal medtages Vishesh Handa 