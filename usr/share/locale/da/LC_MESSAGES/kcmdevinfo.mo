��    ]           �      �  !   �       	   !     +  S   4  	   �     �     �     �     �     �     �     �     �     �     	  J   $	     o	     }	     �	      �	  	   �	  
   �	     �	     �	     �	     �	     
     
     
  L   
  	   l
  	   v
  
   �
  
   �
  
   �
     �
     �
     �
     �
     �
     �
     �
     �
     	     (  	   +     5     G     S     [     i     m     }     �     �  
   �  	   �     �  
   �     �     �     �     �     �     	          +     ?     \     r     x     |     �  H   �     �     �     �     �     �       
     &        A  "   T     w     �     �     �     �       	     �  (  $   �     �  	               	   8     B     P     V     ]     f     y     �     �     �     �  3   �     �                 	   /     9  	   F     P     d     r     {     �     �  1   �  	   �  	   �  
   �  
   �  
   �     �                     2     I     U     Y     n          �     �     �     �     �     �     �     �     �     �       	        "     '     0     5     :     H     Y     o  	   �     �  !   �     �     �     �     �     �  8   �     2     >     E     Q     i     �  
   �     �     �     �     �     �     �     �     �     �     �           B   "       (   W   #   $   %            [   @      L   M   '   6       >   T           <   9   G   &           A      .   8   R       7       J             ?   4                   H   -                      F          5   )   N   D       ,       K             3       U   =   
       ;       O   :           +           !   Z   /      V   Y              Q         E       1       X             ]       0      P         \   I          	          C       S   2       *    
Solid Based Device Viewer Module (c) 2010 David Hubner AMD 3DNow ATI IVEC Available space out of total partition size (percent used)%1 free of %2 (%3% used) Batteries Battery Type:  Bus:  Camera Cameras Charge Status:  Charging Collapse All Compact Flash Reader Default device tooltipA Device Device Information Device Listing Whats ThisShows all the devices that are currently listed. Device Viewer Devices Discharging EMAIL OF TRANSLATORSYour emails Encrypted Expand All File System File System Type:  Fully Charged Hard Disk Drive Hotpluggable? IDE IEEE1394 Info Panel Whats ThisShows information about the currently selected device. Intel MMX Intel SSE Intel SSE2 Intel SSE3 Intel SSE4 Keyboard Keyboard + Mouse Label:  Max Speed:  Memory Stick Reader Mounted At:  Mouse Multimedia Players NAME OF TRANSLATORSYour names No No Charge No data available Not Mounted Not Set Optical Drive PDA Partition Table Primary Processor %1 Processor Number:  Processors Product:  Raid Removable? SATA SCSI SD/MMC Reader Show All Devices Show Relevant Devices Smart Media Reader Storage Drives Supported Drivers:  Supported Instruction Sets:  Supported Protocols:  UDI:  UPS USB UUID:  Udi Whats ThisShows the current device's UDI (Unique Device Identifier) Unknown Drive Unused Vendor:  Volume Space: Volume Usage:  Yes kcmdevinfo name of something is not knownUnknown no device UDINone no instruction set extensionsNone platform storage busPlatform unknown battery typeUnknown unknown deviceUnknown unknown device typeUnknown unknown storage busUnknown unknown volume usageUnknown xD Reader Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-23 03:11+0200
PO-Revision-Date: 2016-05-12 20:44+0100
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <kde-i18n-doc@kde.org>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 
Solid-baseret enhedsfremviser-modul (c) 2010 David Hubner AMD 3DNow ATI IVEC %1 ledig ud af %2 (%3% brugt) Batterier Batteritype:  Bus:  Kamera Kameraer Opladningsstatus:  Lader op Fold alle sammen Compact Flash-læser En enhed Enhedsinformation Viser alle enhederne der i øjeblikket er oplistet. Enhedsfremviser Enheder Aflader mschlander@opensuse.org Krypteret Fold alle ud Filsystem Type af filsystem:  Fuldt opladet Harddisk Kan hotplugges? IDE IEEE1394 Viser information om den aktuelt markerede enhed. Intel MMX Intel SSE Intel SSE2 Intel SSE3 Intel SSE4 Tastatur Tastatur + mus Etiket:  Maks. hastighed:  Hukommelseskort-læser Mounted i:  Mus Multimedieafspillere Martin Schlander Nej Ingen ladning Ingen data tilgængelige Ikke monteret Ikke angivet Optisk drev PDA Partitionstabel Primær Processor %1 Processornummer:  Processorer Produkt:  Raid Flytbar? SATA SCSI SD/MMC-læser Vis alle enheder Vis relevante enheder Smart Media-læser Lagerdrev Understøttede drivere:  Understøttede instruktionssæt:  Understøttede protokoller:  UDI:  UPS USB UUID:  Viser den aktuelle enheds UDI (Unique Device Identifier) Ukendt drev Ubrugt Producent:  Plads på diskområde:  Forbrug af diskområde:  Ja kcmdevinfo Ukendt Ingen Ingen Platform Ukendt Ukendt Ukendt Ukendt Ukendt xD-kortlæser 