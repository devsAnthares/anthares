��          �      l      �  3   �  $     �   :     �  4   �     �  #        =  �   E  �   �  +   �     �     �     �  1     (   3     \  �   s  $   �  (     �  F  9        @  
   I     T  4   a     �  '   �     �  �   �  �   �	  3   e
     �
     �
     �
     �
  '   �
     "  
   7  *   B  /   m                                                   	   
                                            1 action for this device %1 actions for this device @info:status Free disk space%1 free Accessing is a less technical word for Mounting; translation should be short and mean 'Currently mounting this device'Accessing... All devices Click to access this device from other applications. Click to eject this disc. Click to safely remove this device. General It is currently <b>not safe</b> to remove this device: applications may be accessing it. Click the eject button to safely remove this device. It is currently <b>not safe</b> to remove this device: applications may be accessing other volumes on this device. Click the eject button on these other volumes to safely remove this device. It is currently safe to remove this device. Most Recent Device No Devices Available Non-removable devices only Open auto mounter kcmConfigure Removable Devices Open popup when new device is plugged in Removable devices only Removing is a less technical word for Unmounting; translation shoud be short and mean 'Currently unmounting this device'Removing... This device is currently accessible. This device is not currently accessible. Project-Id-Version: plasma_applet_devicenotifier
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2016-09-24 21:48+0100
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <kde-i18n-doc@kde.org>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 %1 handling for denne enhed %1 handlinger for denne enhed %1 ledig Tilgår... Alle enheder Klik for at tilgå denne enhed fra andre programmer. Klik for at skubbe disken ud. Klik for at fjerne denne enhed sikkert. Generelt Det er i øjeblikket <b>ikke sikkert</b> at fjerne denne enhed: Der er muligvis programmer der tilgår den. Klik på knappen Skud ud for at fjerne enheden sikkert. Det er i øjeblikket <b>ikke sikkert</b> at fjerne denne enhed: Der er muligvis programmer der tilgår andre diskområder på enheden. Klik på knappen Skud ud på disse andre diskområder for at fjerne enheden sikkert. Det er i øjeblikket sikkert at fjerne denne enhed. Mest nylige enhed Ingen enheder tilgængelige Kun ikke-flytbare enheder Indstil flytbare enheder Åbn pop-op når en ny enhed tilsluttes Kun flytbare enheder Fjerner... Denne enhed er tilgængelig i øjeblikket. Denne enhed er ikke tilgængelig i øjeblikket. 