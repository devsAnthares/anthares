��    	      d      �       �   .   �   2     )   C  -   m     �  '   �  H   �  R   *  �  }     #     5  	   K     U     c  .   �  N   �  ]                	                                 Note this is a KRunner keyworddesktop console Note this is a KRunner keyworddesktop console :q: Note this is a KRunner keywordwm console Note this is a KRunner keywordwm console :q: Open KWin interactive console Open Plasma desktop interactive console Opens the KWin interactive console with a file path to a script on disk. Opens the Plasma desktop interactive console with a file path to a script on disk. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2012-06-05 14:11+0200
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <dansk@dansk-gruppen.dk>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=2; plural=n != 1;
 skrivebordskonsol skrivebordskonsol :q: wm-konsol wm-konsol :q: Åbn interaktiv konsol til KWin Åbn interaktiv konsol til Plasma-skrivebordet Åbner den interaktive konsol til KWin med en filsti til et script på disken. Åbner den interaktive konsol til Plasma-skrivebordet med en filsti til et script på disken. 