��    >        S   �      H  	   I  	   S     ]     e     s     y          �     �     �     �     �     �     �  
        "  ?   .     n  >   w  	   �     �     �  S   �      A     b  �  z     	     
	     	  
   '	     2	     B	     R	  
   _	     j	  A   y	     �	     �	  
   �	     �	     �	     
     #
     ,
     ;
     G
     X
     h
     |
     �
     �
     �
     �
     �
     �
               (  T   ;     �  S   �  �  �  	   �     �     �     �  	   �     �     �     �     �     
          3  $   L     q     �     �  ?   �  	   �  ?   �  
   3     >     W  V   k     �     �  �  �     �     �     �     �     �     �     �     �       B     "   ]     �  	   �     �     �     �  	   �     �     �     �     �          "     4     J     [     o     �     �     �     �     �  Q   �     G  E   U            3          1   6       2   4      !   *         #   7                    (   9          &           :   5          +                    "                        '           -          >   =   /   	      )                ;   
      .      $   %       <   ,                    8      0            [Hidden] &Comment: &Delete &Description: &Edit &File &Name: &New Submenu... &Run as a different user &Sort &Sort all by Description &Sort all by Name &Sort selection by Description &Sort selection by Name &Username: &Work path: (C) 2000-2003, Waldo Bastian, Raffaele Sandrini, Matthias Elter Advanced All submenus of '%1' will be removed. Do you want to continue? Co&mmand: Could not write to %1 Current shortcut &key: Do you want to restore the system menu? Warning: This will remove all custom menus. EMAIL OF TRANSLATORSYour emails Enable &launch feedback Following the command, you can have several place holders which will be replaced with the actual values when the actual program is run:
%f - a single file name
%F - a list of files; use for applications that can open several local files at once
%u - a single URL
%U - a list of URLs
%d - the folder of the file to open
%D - a list of folders
%i - the icon
%m - the mini-icon
%c - the caption General General options Hidden entry Item name: KDE Menu Editor KDE menu editor Main Toolbar Maintainer Matthias Elter Menu changes could not be saved because of the following problem: Menu entry to pre-select Montel Laurent Move &Down Move &Up NAME OF TRANSLATORSYour names New &Item... New Item New S&eparator New Submenu Only show in KDE Original Author Previous Maintainer Raffaele Sandrini Restore to System Menu Run in term&inal Save Menu Changes? Show hidden entries Spell Checking Spell checking Options Sub menu to pre-select Submenu name: Terminal &options: Unable to contact khotkeys. Your changes are saved, but they could not be activated. Waldo Bastian You have made changes to the menu.
Do you want to save the changes or discard them? Project-Id-Version: kmenuedit
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2014-03-05 19:58+0100
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <kde-i18n-doc@kde.org>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
  [Skjult] &Kommentar: &Slet &Beskrivelse: &Redigér &Fil &Navn: &Ny undermenu... Kør som en &anden bruger &Sortér &sortér alle efter beskrivelse &Sortér alle efter navn &sortér markering efter beskrivelse &sortér markering efter navn &Brugernavn: &Arbejdssti: (C) 2000-2003, Waldo Bastian, Raffaele Sandrini, Matthias Elter Avanceret Alle undermenuer for "%1" vil blive fjernet. Vil du fortsætte? Ko&mmando: Kunne ikke skrive til %1 Denne &genvejstast: Vil du gendanne systemmenuen? Advarsel: Dette vil fjerne alle brugertilpassede menuer. erik@binghamton.edu Aktivér &opstartsfeedback Efterfølgende kommandoen, kan du have adskillige pladsholdere som vil blive erstattet med de faktiske værdier når det egentlige program køres:
%f - et enkelt filnavn
%F - en liste af filer; brugt til programmer der kan åbne flere lokale filer på én gang
%u - en enkel URL
%U - en liste af URL'er
%d - mappen for filen der skal åbnes
%D - en liste af mapper
%i - ikonet
%m - mini-ikonet
%c - kommentaren Generelt Generelle indstillinger Skjult punkt Punkt-navn: KDE Menueditor KDE Menuredigering Hovedværktøjslinje Vedligeholder Matthias Elter Menuændringerne kunne ikke gemmes på grund af følgende problem: Menuindgang der skal vælges forud Montel Laurent Flyt &ned Flyt &op Erik Kjær Pedersen Nyt &punkt... Nyt punkt Ny &adskiller Ny undermenu Vis kun i KDE Oprindelig forfatter Forrige vedligeholder Raffaele Sandrini Gendan til systemmenu Kør i term&inal Gem menuændringer? Vis skjulte punkter Stavekontrol Indstillinger for stavekontrol Undermenu til at vælge forud Undermenu-navn: Terminalin&dstillinger: Kan ikke kontakte khotkeys. Dine ændringer er gemt, men de kunne ikke aktiveres. Waldo Bastian Du har lavet ændringer i menuen.
Vil du gemme dem eller kassere dem? 