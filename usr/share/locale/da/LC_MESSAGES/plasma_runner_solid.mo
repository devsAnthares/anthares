��          �      L      �  m   �     /  "   <  F   _  F   �  F   �  J   4  N     R   �     !  %   2  $   X  #   }  $   �  %   �  &   �  �        �  �  �     T     d  %   s  O   �  H   �  D   2  H   w  J   �  P   	     \	     l	     r	     z	     	     �	  	   �	     �	     �	                                
                          	                                          Close the encrypted container; partitions inside will disappear as they had been unpluggedLock the container Eject medium Finds devices whose name match :q: Lists all devices and allows them to be mounted, unmounted or ejected. Lists all devices which can be ejected, and allows them to be ejected. Lists all devices which can be mounted, and allows them to be mounted. Lists all devices which can be unmounted, and allows them to be unmounted. Lists all encrypted devices which can be locked, and allows them to be locked. Lists all encrypted devices which can be unlocked, and allows them to be unlocked. Mount the device Note this is a KRunner keyworddevice Note this is a KRunner keywordeject Note this is a KRunner keywordlock Note this is a KRunner keywordmount Note this is a KRunner keywordunlock Note this is a KRunner keywordunmount Unlock the encrypted container; will ask for a password; partitions inside will appear as they had been plugged inUnlock the container Unmount the device Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-11-12 15:15+0100
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <dansk@dansk-gruppen.dk>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=2; plural=n != 1;
 Lås beholderen Skub mediet ud Finder enheder hvis navne matcher :q: Oplister alle enheder og lader dem blive monteret, afmonteret eller skubbet ud. Oplister alle enheder som kan skubbes ud, og lader dem blive skubbet ud. Oplister alle enheder som kan monteres, og lader dem blive monteret. Oplister alle enheder som kan afmonteres, og lader dem blive afmonteret. Oplister alle krypterede enheder som kan låses, og lader dem blive låst. Oplister alle krypterede enheder som kan låses op, og lader dem blive låst op. Montér enheden enhed skub ud lås montér lås op afmontér Lås beholderen op Afmontér enheden 