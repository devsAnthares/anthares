��          �      �           	               4  	   I     S      c  "   �      �     �     �  	   �     �               )  
   9     D     S     a     g     {  c  �     �     �      �       
   &     1     E     G  $   e  
   �  	   �     �     �     �     �     �  
   �     �               +  
   @                              	                                     
                                      K (HH:MM) (In minutes - min. 1, max. 600) Activate Night Color Automatic Detect location EMAIL OF TRANSLATORSYour emails Error: Morning not before evening. Error: Transition time overlaps. Latitude Location Longitude NAME OF TRANSLATORSYour names Night Color Night Color Temperature:  Operation mode: Roman Gilg Sunrise begins Sunset begins Times Transition duration and ends Project-Id-Version: kcm_nightcolor
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-12 03:09+0100
PO-Revision-Date: 2018-01-19 19:43+0200
Last-Translator: scootergrisen
Language-Team: Danish
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
  K (TT:MM) (i minutter - min. 1, maks. 600) Aktivér natfarve Automatisk Registrer placering , Fejl: Morgen ikke før aften. Fejl: Overgangstidspunkt overlapper. Breddegrad Placering Længdegrad scootergrisen Natfarve Natfarvens temperatur:  Handlingstilstand: Roman Gilg Solopgang begynder Solnedgang begynder Tidspunkter Overgangens varighed og slutter 