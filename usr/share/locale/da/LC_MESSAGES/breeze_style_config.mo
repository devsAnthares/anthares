��            )         �     �  "   �     �  !   �  !        4  
   J     U     p     �  !   �     �  0   �  8        ?  !   ]          �     �     �     �                '  
   /  
   :  
   E  &   P     w     �  U  �     �     �          '     C     ]     r     ~     �  #   �  %   �  !   �  =     ?   T     �  "   �     �     �     	  %   /	     U	  3   j	     �	     �	     �	     �	     �	  $   �	  
   �	     
                                                                                                 	                       
                 ms &Keyboard accelerators visibility: &Top arrow button type: Always Hide Keyboard Accelerators Always Show Keyboard Accelerators Anima&tions duration: Animations Bottom arrow button t&ype: Breeze Settings Center tabbar tabs Drag windows from all empty areas Drag windows from titlebar only Drag windows from titlebar, menubar and toolbars Draw a thin line to indicate focus in menus and menubars Draw focus indicator in lists Draw frame around dockable panels Draw frame around page titles Draw frame around side panels Draw slider tick marks Draw toolbar item separators Enable animations Enable extended resize handles Frames General No Buttons One Button Scrollbars Show Keyboard Accelerators When Needed Two Buttons W&indows' drag mode: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:42+0200
PO-Revision-Date: 2017-11-06 18:39+0200
Last-Translator: scootergrisen
Language-Team: Danish
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
  ms &Synlighed af tastaturgenveje: &Knaptype for toppil: Skjul altid tastaturgenveje Vis altid tastaturgenveje Anima&tionsvarighed: Animationer Knapt&ype for bundpil: Breeze-indstillinger Centrér fanebladsbjælke-faneblade Træk vinduer via alle tomme områder Træk kun vinduer via titellinjen Træk vinduer via titellinjen, menulinjen og værktøjslinjer Tegn en tynd linje for at indikere fokus i menuer og menulinjer Tegn fokusindikator i lister Tegn ramme omkring dokbare paneler Tegn ramme omkring sidetitler Tegn ramme omkring sidepaneler Tegn tikmærker på skydeknap Tegn værktøjslinjes punktadskillere Aktivér animationer Aktivér udvidede håndtag til at ændre størrelse Rammer Generelt Ingen knapper En knap Rullebjælker Vis tastaturgenveje når nødvendigt To knapper Trækket&ilstand for vinduer: 