��          t      �         C        U     i  3   �     �     �  F   �  5   *     `       �  �  H   4     }     �     �     �     �  E   �  7   &     ^     z                              	                          
    Attempting to perform the action '%1' failed with the message '%2'. Media playback next Media playback previous Name for global shortcuts categoryMedia Controller Play/Pause media playback Stop media playback The argument '%1' for the action '%2' is missing or of the wrong type. The media player '%1' cannot perform the action '%2'. The operation '%1' is unknown. Unknown error. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-06 03:09+0200
PO-Revision-Date: 2015-08-26 20:22+0200
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <dansk@dansk-gruppen.dk>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 Forsøg på at udføre handlingen "%1" mislykkedes med meddelelsen "%2". Spil næste medie Spil forrige medie Mediekontrol Start/pause medieafspilning Stop medieafspilning Argumentet "%1" for handlingen "%2" mangler eller er af forkert type. Medieafspilleren "%1" kan ikke udføre handlingen "%2". Operationen "%1" er ukendt. Ukendt fejl. 