��    ,      |  ;   �      �  (   �     �  �   �     �     �     �     �     �     �     �     �     �               %     1      J     k     s     �     �     �     �     �     �     �               /     4     I     Y     l     y     �     �      �  7   �                     1     6  U  <     �     �     �     �  	   �  
   �     �  	   �     �  
   	     	     3	     @	     L	     U	     a	     z	     �	     �	     �	     �	     �	     �	     �	     
     "
     0
     =
     N
  #   S
     w
     �
  
   �
     �
  $   �
     �
     �
  A        Y     ^     y     �     �     !                 %   +   ,      "   #                               &                  	                           )                             *      '      $      (                                
    %1 is the name of a session%1 (Wayland) ... @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) @title:windowSelect Image Advanced Author Auto &Login Background: Clear Image Commands Could not decompress archive Cursor Theme: Customize theme Default Description Download New SDDM Themes EMAIL OF TRANSLATORSYour emails General Get New Theme Halt Command: Install From File Install a theme. Invalid theme package Load from file... Login screen using the SDDM Maximum UID: Minimum UID: NAME OF TRANSLATORSYour names Name No preview available Reboot Command: Relogin after quit Remove Theme SDDM KDE Config SDDM theme installer Session: The default cursor theme in SDDM The theme to install, must be an existing archive file. Theme Unable to install theme Uninstall a theme. User User: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2017-11-06 18:48+0200
Last-Translator: scootergrisen
Language-Team: Danish
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
 %1 (Wayland) ... (Tilgængelige størrelser: %1) Vælg billede Avanceret Ophavsmand Auto-&login Baggrund: Ryd billede Kommandoer Kunne ikke dekomprimere arkivet Markørtema: Tilpas tema Standard Beskrivelse Download nye SDDM-temaer mschlander@opensuse.org Generelt Hent nyt tema Sluk-kommando: Installér fra fil Installér et tema. Ugyldig temapakke Indlæs fra fil... Login-skærm med brug af SDDM Maksimum-UID: Minimum-UID: Martin Schlander Navn Ingen forhåndsvisning tilgængelig Genstart-kommando: Log ind igen efter afslutning Fjern tema KDE-konfiguration af SDDM Installationsprogram til SDDM-temaer Session: Standard markørtema i SDDM Temaet som skal installeres, skal være en eksisterende arkivfil. Tema Kan ikke installere temaet Afinstallér et tema. Bruger Bruger: 