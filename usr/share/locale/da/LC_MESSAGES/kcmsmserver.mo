��          �      <      �     �     �     �  7  �  �  #  +   �  ^              �     �  x   �  �   %     �               ;     U  �  r     #	     9	     N	  %  _	  �  �
  -   V  l   �     �          (  }   1  �   �     �     �     �     �     �                          
       	                                                                    &End current session &Restart computer &Turn off computer <h1>Session Manager</h1> You can configure the session manager here. This includes options such as whether or not the session exit (logout) should be confirmed, whether the session should be restored again when logging in and whether the computer should be automatically shut down after session exit by default. <ul>
<li><b>Restore previous session:</b> Will save all applications running on exit and restore them when they next start up</li>
<li><b>Restore manually saved session: </b> Allows the session to be saved at any time via "Save Session" in the K-Menu. This means the currently started applications will reappear when they next start up.</li>
<li><b>Start with an empty session:</b> Do not save anything. Will come up with an empty desktop on next start.</li>
</ul> Applications to be e&xcluded from sessions: Check this option if you want the session manager to display a logout confirmation dialog box. Conf&irm logout Default Leave Option General Here you can choose what should happen by default when you log out. This only has meaning, if you logged in through KDM. Here you can enter a colon or comma separated list of applications that should not be saved in sessions, and therefore will not be started when restoring a session. For example 'xterm:konsole' or 'xterm,konsole'. O&ffer shutdown options On Login Restore &manually saved session Restore &previous session Start with an empty &session Project-Id-Version: kcmsmserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2009-05-06 11:18+0200
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <dansk@dansk-gruppen.dk>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 0.3
Plural-Forms: nplurals=2; plural=n != 1;
 &Afslut denne session &Genstart computeren &Sluk computeren <h1>Sessionshåndtering</h1> Her kan du indstille sessionshåndteringen. Dette inkluderer valg, såsom hvorvidt du vil bekræfte, at du vil logge af, om den sidste session skal genskabes når du logger på igen, og om computeren skal lukke ned automatisk efter sessionsafslutning som standard. <ul>
<li><b>Genskab forrige session:</b> Vil gemme alle programmer der kører ved afslutning og genskabe dem ved næste opstart</li>
<li><b>Genskab manuelt gemt session: </b> Lader sessionen blive gemtnår som helst via "Gem session" i K-Menuen. Dette betyder at de programmer der kører på dette tidspunkt vil komme tilbage ved næste opstart.</li>
<li><b>Start med en tom session:</b> Gem intet. Vil komme op med et tomtskrivebord ved næste opstart.</li>
</ul> Programmer der skal &udelukkes fra sessioner: Markér denne indstilling, hvis du ønsker at sessionshåndteringen skal vise en log ud-bekræftelsesdialog. Bekræft når der logges &ud Standardvalg for "Forlad" Generelt Her kan du vælge hvad der skal ske som standard når du logger ud. Dette har kun betydning hvis du er logget ind gennem KDM. Her kan du indtaste en kolon- eller komma-adskilt liste over programmer der ikke skal gemmes i sessioner, og derfor ikke vil blive startet når sessionen genskabes. For eksempel "xterm:xconsole" eller "xterm,konsole". &Tilbyd nedlukningsmuligheder Når der logges ind Genskab &manuelt gemt session Genskab &forrige session Start med en &tom session 