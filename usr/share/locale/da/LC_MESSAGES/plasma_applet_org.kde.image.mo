��          �   %   �      `     a     j          �     �     �  0   �     �     �                    &     ;  
   R     ]     r          �     �     �     �     �     �     �       k       �  $   �     �     �  	   �     �  7   �     "     =     [     a     m     v     �     �     �     �     �     �          &     /     E     c     l  	   �                                                   
                                                                  	       %1 by %2 Add Custom Wallpaper Add Folder... Add Image... Centered Change every: Directory with the wallpaper to show slides from Download Wallpapers Get New Wallpapers... Hours Image Files Minutes Next Wallpaper Image Open Containing Folder Open Image Open Wallpaper Image Positioning: Recommended wallpaper file Remove wallpaper Restore wallpaper Scaled Scaled and Cropped Scaled, Keep Proportions Seconds Select Background Color Tiled Project-Id-Version: plasma_wallpaper_image
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:39+0100
PO-Revision-Date: 2017-11-06 19:00+0200
Last-Translator: scootergrisen
Language-Team: Danish
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
 %1 af %2 Tilføj brugervalgt baggrundsbillede Tilføj mappe... Tilføj billede... Centreret Skift hver: Mappe med baggrundsbilledet som der skal vises dias fra Download baggrundsbilleder Hent nye baggrundsbilleder... Timer Billedfiler Minutter Næste baggrundsbillede Åbn indeholdende mappe Åbn billede Åbn baggrundsbillede Positionering: Anbefalet baggrundsbillede-fil Fjern baggrundsbillede Gendan baggrundsbillede Skaleret Skaleret og beskåret Skaleret, behold proportioner Sekunder Vælg baggrundsfarve Fliselagt 