��    B      ,  Y   <      �  i   �  m        y  b   �     �     	  6        O  7   _     �     �  	   �     �  (   �  )   �  )   (     R     o  Y   u     �     �  �   �      y	  .   �	     �	  
   �	     �	     �	     
     
     !
     5
     B
     J
     c
     r
     w
     �
     �
     �
     �
     �
  	   �
  
   �
     �
     �
  	     
     
   *     5     B  	   `     j     o  
   �  �   �     (  8   8  �   q     �  8   	  ,   B  ,   o  %   �     �  �  �  ;   �  v   �     @  `   Z     �     �  7   �       6   (     _     e     s     x  ,   �  +   �  -   �          =  ^   D     �     �  }   �     G  ,   _     �     �     �     �     �               )     7     ?     ]     l     q     �     �  	   �     �     �     �     �               )     @     V     ^      g  	   �     �     �  	   �  �   �     b  8   o  �   �     4  8   C  2   |  4   �  &   �          $   !   %          ;   ,          B                 8   #                 7   9       +              )   ?   -   <         6                                    *   2       >                1       @       .   5   3   :   A         &      /                4              0      "             =   (   '         	   
           @info User changed Phonon backendTo apply the backend change you will have to log out and back in again. A list of Phonon Backends found on your system.  The order here determines the order Phonon will use them in. Apply Device List To... Apply the currently shown device preference list to the following other audio playback categories: Audio Hardware Setup Audio Playback Audio Playback Device Preference for the '%1' Category Audio Recording Audio Recording Device Preference for the '%1' Category Backend Colin Guthrie Connector Copyright 2006 Matthias Kretz Default Audio Playback Device Preference Default Audio Recording Device Preference Default Video Recording Device Preference Default/Unspecified Category Defer Defines the default ordering of devices which can be overridden by individual categories. Device Configuration Device Preference Devices found on your system, suitable for the selected category.  Choose the device that you wish to be used by the applications. EMAIL OF TRANSLATORSYour emails Failed to set the selected audio output device Front Center Front Left Front Left of Center Front Right Front Right of Center Hardware Independent Devices Input Levels Invalid KDE Audio Hardware Setup Matthias Kretz Mono NAME OF TRANSLATORSYour names Phonon Configuration Module Playback (%1) Prefer Profile Rear Center Rear Left Rear Right Recording (%1) Show advanced devices Side Left Side Right Sound Card Sound Device Speaker Placement and Testing Subwoofer Test Test the selected device Testing %1 The order determines the preference of the devices. If for some reason the first device cannot be used Phonon will try to use the second, and so on. Unknown Channel Use the currently shown device list for more categories. Various categories of media use cases.  For each category, you may choose what device you prefer to be used by the Phonon applications. Video Recording Video Recording Device Preference for the '%1' Category  Your backend may not support audio recording Your backend may not support video recording no preference for the selected device prefer the selected device Project-Id-Version: kcm_phonon
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2012-06-05 13:41+0200
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <dansk@dansk-gruppen.dk>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=2; plural=n != 1;
 For at anvende skift af motor skal du logge ud og ind igen. En liste over Phonon-motorer fundet på dit system. Rækkefølgen her afgør rækkefølgen hvori Phonon vil bruge dem. Anvend enhedsliste på... Anvend den aktuelt viste enhedspræferenceliste for følgende andre kategorier af lydafspilning: Opsætning af lydhardware Lydafspilning Præference for lydafspilningsenhed for kategorien "%1" Lydoptagelse Præference for lydoptagelsesenhed for kategorien "%1" Motor Colin Guthrie Stik Ophavsret 2006 Matthias Kretz Præference for standard lydafspilningsenhed Præference for standard lydoptagelsesenhed Præference for standard videooptagelsesenhed Standard/uspecificeret kategori Undgå Definerer standardrækkefølge af enheder som kan tilsidesættes efter individuelle kategoier. Enhedskonfiguration Enhedspræference Enheder fundet på dit system, som er egnede til den valgte kategori. Vælg den enhed du ønsker skal bruges af programmerne. mschlander@opensuse.org Kunne ikke angive den valgte lydoutput-enhed Centerhøjttaler, front Venstre fronthøjttaler Midterstillet venstre, front Højre fronthøjttaler Midterstillet højre, front Hardware Uafhængige enheder Inputniveauer Ugyldig KDE opsætning af lydhardware Matthias Kretz Mono Martin Schlander Konfigurationsmodul til Phonon Afspilning (%1) Foretræk Profil Centerhøjttaler, bag Venstre baghøjttaler Højre baghøjttaler Optagelse (%1) Vis avancerede enheder Venstre sidehøjttaler Højre sidehøjttaler Lydkort Lydenhed Placering og test af højttalere Subwoofer Test Test den valgte enhed Tester %1 Rækkefølgen afgør præferencen mht. enhederne. Hvis den første enhed ikke kan bruges af en eller anden årsag, vil Phonon prøve at bruge den anden, og så fremdeles. Ukendt kanal Brug den aktuelt viste enhedsliste til flere kategorier. Diverse kategorier af mediebrugsmønstre. For hver kategori kan du vælge hvilken enhed du foretrækker skal bruges af Phonon-programmerne. Videooptagelse Præference for videooptagelsesenhed for kategorien "%1" Din motor understøtter muligvis ikke lydoptagelse Din motor understøtter muligvis ikke videooptagelse ingen præference for den valgte enhed foretræk den valgte enhed 