��          �      <      �     �  <   �  
   �     	          &     3     ?  
   G     R     c     q     }     �     �  
   �     �  �  �     h  E   �     �  
   �     �     �                    )     ?  	   T     ^     s     �  	   �     �                                                  
   	                                                Add Launcher... Add launchers by Drag and Drop or by using the context menu. Appearance Arrangement Edit Launcher... Enable popup Enter title General Hide icons Maximum columns: Maximum rows: Quicklaunch Remove Launcher Show hidden icons Show launcher names Show title Title Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-02-26 04:09+0100
PO-Revision-Date: 2016-03-09 20:05+0100
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <kde-i18n-doc@kde.org>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Tilføj programstarter... Tilføj programstartere ved hjælp af træk og slip eller højreklik. Udseende Opstilling Redigér programstarter... Aktivér pop-op Angiv titel Generelt Skjul ikoner Maks. antal kolonner: Maks. antal rækker: Kvikstart Fjern programstarter Vis skjulte ikoner Vis navne på programstartere Vis titel Titel 