��          �   %   �      P     Q     b     w     �     �     �  
   �     �     �     �     �     �  /        3     Q     p     v     �     �     �     �     �     �     �       �  %     �     �     �     
     &     3     I  	   R  	   \     f  
   j     u  4   �     �     �     �  	   �  
                  0     J     b     {     �                            
                                                                                             	    %1 file %1 files %1 folder %1 folders %1 of %2 processed %1 of %2 processed at %3/s %1 processed %1 processed at %2/s Appearance Behavior Cancel Clear Configure... Finished Jobs List of running file transfers/jobs (kuiserver) Move them to a different list Move them to a different list. Pause Remove them Remove them. Resume Show all jobs in a list Show all jobs in a list. Show all jobs in a tree Show all jobs in a tree. Show separate windows Show separate windows. Project-Id-Version: kuiserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-05-21 10:12+0200
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <dansk@dansk-gruppen.dk>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.0
 %1 fil %1 filer %1 mappe %1 mapper %1 af %2 behandlet %1 af %2 behandlet ved %3/s %1 behandlet %1 behandlet ved %2/s Udseende Opførsel Annullér Ryd Indstil... Afsluttede jobs Liste over kørende filoverførsler/jobs (kuiserver) Flyt dem til en anden liste Flyt dem til en anden liste. Pause Fjern dem Fjern dem. Genoptag Vis alle jobs i en liste Vis alle jobs i en liste. Vis alle jobs i et træ Vis alle jobs i et træ. Vis separate vinduer Vis separate vinduer. 