��          �   %   �      0  *   1     \  .   s     �     �     �     �     �     �                 
   $     /     5     =     E     ]     t     �     �     �     �  �  �  +   �     �  ,   �     �               ;     R     b     t  
   y     �     �     �     �     �     �     �     �  !   �           3     <        
                             	                                                                                          %1 Minimized Window: %1 Minimized Windows: %1 Window: %1 Windows: ...and %1 other window ...and %1 other windows Activity name Activity number Add Virtual Desktop Configure Desktops... Desktop name Desktop number Display: Does nothing General Horizontal Icons Layout: No text Only the current screen Remove Virtual Desktop Selecting current desktop: Show Activity Manager... Shows desktop The pager layoutDefault Vertical Project-Id-Version: plasma_applet_pager
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-19 03:29+0200
PO-Revision-Date: 2017-02-11 12:25+0100
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <kde-i18n-doc@kde.org>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 %1 minimeret vindue: %1 minimerede vinduer: %1 vindue: %1 vinduer: ...og %1 andet vindue ...og %1 andre vinduer Aktivitetsnavn Aktivitetsnummer Tilføj virtuelt skrivebord Indstil skriveborde... Skrivebordsnavn Skrivebordsnummer Vis: Gør intet Generelt Vandret Ikoner Layout: Ingen tekst Kun den aktuelle skærm Fjern virtuelt skrivebord Valg af aktuelt skrivebord: Vis håndtering af aktiviteter... Viser skrivebordet Standard Lodret 