��          �      �       H     I     i  #   �      �     �     �     �  �        �  ]   �       p   *  :   �  �  �  +   z     �  *   �     �               (  �   9     �  u   �  !   a  v   �  =   �                     
          	                                  Configure Look and Feel details Cursor Settings Changed Download New Look And Feel Packages EMAIL OF TRANSLATORSYour emails Get New Looks... Marco Martin NAME OF TRANSLATORSYour names Select an overall theme for your workspace (including plasma theme, color scheme, mouse cursor, window and desktop switcher, splash screen, lock screen etc.) Show Preview This module lets you configure the look of the whole workspace with some ready to go presets. Use Desktop Layout from theme Warning: your Plasma Desktop layout will be lost and reset to the default layout provided by the selected theme. You have to restart KDE for cursor changes to take effect. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-27 05:46+0100
PO-Revision-Date: 2017-02-11 12:08+0100
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <kde-i18n-doc@kde.org>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Indstil detaljer for udseende og fremtoning Markørindstillinger ændret Download nye udseende og fremtoning-pakker mschlander@opensuse.org Hent nyt udseende... Marco Martin Martin Schlander Vælg et overordnet tema til dit arbejdsområde (inklusiv Plasma-tema, farvetema, musemarkør, vindues- og skrivebordsskifter, opstartsbillede, låseskærm mv.) Forhåndsvisning Dette modul lader dig indstille udseendet for hele arbejdsområdet med nogle forudindstillinger som er klar til brug. Brug skrivebordslayout fra temaet Advarsel: Layoutet af dit Plasma-skrivebord vil gå tabt og blive nulstillet til standardlayoutet for det valgte tema. Du skal genstarte KDE før markørændringer træder i kraft. 