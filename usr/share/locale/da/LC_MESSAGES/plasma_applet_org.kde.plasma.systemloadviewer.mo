��    &      L  5   |      P     Q     l     y     �     �     �     �     �     �     �     �     �  &   �               #     ,     3     ?     M     U     ]     d     s     �     �     �     �     �     �     �     �     �     �  
   �            U       p     r     {     �     �     �     �     �     �     �     �     �  (   �           3  	   ;     E     L     \     p     y  
   �     �     �     �     �     �     �     �     �                    *  
   ?     J     R                           	      #      $                                                    &                                          !          %                            "   
    Abbreviation for secondss Application: Average clock: %1 MHz Bar Buffers: CPU CPU %1 CPU monitor CPU%1: %2% @ %3 Mhz CPU: %1% CPUs separately Cache Cache Dirty, Writeback: %1 MiB, %2 MiB Cache monitor Cached: Circular Colors Compact Bar Dirty memory: General IOWait: Memory Memory monitor Memory: %1/%2 MiB Monitor type: Nice: Set colors manually Show: Swap Swap monitor Swap: %1/%2 MiB Sys: System load Update interval: Used swap: User: Writeback memory: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-29 03:31+0200
PO-Revision-Date: 2017-11-06 19:01+0200
Last-Translator: scootergrisen
Language-Team: Danish
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
 s Program: Gennemsnitlig clock: %1 MHz Bjælke Buffere: CPU CPU %1 CPU-overvågning CPU%1: %2% @ %3 Mhz CPU: %1% CPU'er separat Cachet Cache beskift, writeback: %1 MiB, %2 MiB Cache-overvågning Cachet: Cirkulær Farver Kompakt bjælke Beskidt hukommelse: Generelt IOWait: Hukommelse Hukommelsesovervågning Hukommelse: %1/%2 MiB Overvågningstype: Nice: Angiv farver manuelt Vis: Swap Swap-overvågning Swap: %1/%2 MiB Sys: Systembelastning Opdateringsinterval: Brug swap: Bruger: Writeback-hukommelse: 