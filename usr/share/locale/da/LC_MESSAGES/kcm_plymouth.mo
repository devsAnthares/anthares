��          �   %   �      @     A  !   Y      {     �      �     �     �  +        =     N     [  (   z     �  ,   �  7   �     !  7   :     r  ]   �  1   �  	   "     ,  "   ?  5   b  �  �     <  $   W     |     �     �  "   �     �  (        :     N     [  )   l     �  +   �  A   �     $	  7   ;	     s	  u   �	  /   
     4
     A
  &   W
  2   ~
                    
         	                                                                                                 Cannot start initramfs. Cannot start update-alternatives. Configure Plymouth Splash Screen Download New Splash Screens EMAIL OF TRANSLATORSYour emails Get New Boot Splash Screens... Initramfs failed to run. Initramfs returned with error condition %1. Install a theme. Marco Martin NAME OF TRANSLATORSYour names No theme specified in helper parameters. Plymouth theme installer Select a global splash screen for the system The theme to install, must be an existing archive file. Theme %1 does not exist. Theme corrupted: .plymouth file not found inside theme. Theme folder %1 does not exist. This module lets you configure the look of the whole workspace with some ready to go presets. Unable to authenticate/execute the action: %1, %2 Uninstall Uninstall a theme. update-alternatives failed to run. update-alternatives returned with error condition %1. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-28 03:05+0200
PO-Revision-Date: 2017-11-27 19:46+0100
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <kde-i18n-doc@kde.org>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Kan ikke starte initramfs. Kan ikke starte update-alternatives. Indstil Plymouth opstartsskærm Download nye opstartsskærme mschlander@opensuse.org Hent nye opstartsskærmbilleder... Initramfs kunne ikke køres. Initramfs returnerede fejlbetingelse %1. Installér et tema. Marco Martin Martin Schlander Intet tema angivet i hjælperparametrene. Installation af Plymouth-temaer Vælg en global opstartsskærm til systemet Temaet som skal installeres, skal være en eksisterende arkivfil. Temaet %1 findes ikke. Defekt tema: .plymouth-filen blev ikke fundet i temaet. Temamappen %1 findes ikke. Dette modul lader dig indstille udseendet for hele arbejdsområdet med nogle forudindstillinger som er klar til brug. Kan ikke autentificere/køre handlingen: %1, %2 Afinstallér Afinstallér et tema. update-alternatives kunne ikke køres. update-alternatives returnerede fejlbetingelse %1. 