��          �   %   �      @     A  �   `  m   �  �   P  )   �  `        o     |     �     �     �      �     �     �  '        ,     >     ]     b     s  ?   �  Y   �  +     H   F  �  �     ;  �   Z  p   �     ^	     ~	  c   �	     �	     
     !
     -
     9
     T
     h
     y
  $   �
     �
     �
     �
     �
  
   �
  ;   �
  P   /  '   �  I   �                                                 	                                                               
              (c) 2003-2007 Fredrik Höglund <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) @item:inlistbox sizeResolution dependent A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Cursor Theme Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Fredrik Höglund Get new Theme Get new color schemes from the Internet Install from File NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. You have to restart the Plasma session for these changes to take effect. Project-Id-Version: kcminput
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-09-24 21:49+0100
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <kde-i18n-doc@kde.org>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 (c) 2003-2007 Fredrik Höglund <qt>Er du sikke på at du ønsker at fjerne markør-temaet <strong>%1</strong>?<br />Dette vil slette alle filerne installeret af dette tema.</qt> <qt>Du kan ikke slette det tema du aktuelt bruger.<br />Du er nødt til at skifte til et andet tema først.</qt> (Tilgængelige størrelser: %1) Opløsningsafhængig Et tema ved navn %1 eksisterer allerede i din ikontema-mappe. Ønsker du at erstatte det med dette? Bekræftelse Markørindstillinger ændret Markørtema Beskrivelse Træk eller skriv tema-URL erik@binghamton.edu Fredrik Höglund Hent nyt tema Hent nye farvetemaer fra internettet Installér fra fil Erik Kjær Pedersen Navn Overskriv tema? Fjern tema Filen %1 synes ikke at være et gyldigt markør-tema arkiv. Kan ikke downloade markør-tema arkivet. Tjek venligst at adressen %1 er rigtig. Kan ikke finde markør-tema arkivet %1. Du skal genstarte Plasma-sessionen før disse ændringer træder i kraft. 