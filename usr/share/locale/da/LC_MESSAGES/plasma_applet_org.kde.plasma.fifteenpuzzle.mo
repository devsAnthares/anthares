��          �            x  
   y  	   �     �     �  3   �     �     �                    %     *     F  B   Y     �  �  �     n     w     �     �  3   �     �     �  	   �            
     !        ?     S     [     	                                                  
                        Appearance Browse... Choose an image Fifteen Puzzle Image Files (*.png *.jpg *.jpeg *.bmp *.svg *.svgz) Number color Path to custom image Piece color Show numerals Shuffle Size Solve by arranging in order Solved! Try again. The time since the puzzle started, in minutes and secondsTime: %1 Use custom image Project-Id-Version: plasma_applet_fifteenPuzzle
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-01-12 03:59+0100
PO-Revision-Date: 2017-02-11 12:22+0100
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <dansk@dansk-gruppen.dk>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 Udseende Gennemse... Vælg billede Puslespillet 15 Billedfiler (*.png *.jpg *.jpeg *.bmp *.svg *.svgz) Nummerfarve Brug brugervalgt billede Brikfarve Vis tal Bland Størrelse Saml ved arrangere i rækkefølge Samlet! Prøv igen. Tid: %1 Brug brugervalgt billede 