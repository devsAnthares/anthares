��    &      L  5   |      P     Q     p  	   ~  0   �  .   �     �  7         ?     `     i  \   �  t   �  9   Z  -   �  $   �     �  [   �     P  E   o  G   �  >   �     <     C     Z     g     �     �     
     )  <   .  :   k  s   �  o   	  .   �	  ,   �	  .   �	  ,   
  �  B
     �               #     1  &   ?  9   f     �     �  $   �  W   �  ^   <     �     �  1   �       X        n  
        �     �     �     �     �  g   �     D     S      i     �     �     �  !   �     �     �                                &   %                     !   	      
                       #       "                     $                                                                             Activate Task Manager Entry %1 Activities... Add Panel Bluetooth was disabled, keep shortBluetooth Off Bluetooth was enabled, keep shortBluetooth On Configure Mouse Actions Plugin Do not restart plasma-shell automatically after a crash EMAIL OF TRANSLATORSYour emails Empty %1 Enable QML Javascript debugger Enables test mode and specifies the layout javascript file to set up the testing environment Fatal error message bodyAll shell packages missing.
This is an installation issue, please contact your distribution Fatal error message bodyShell package %1 cannot be found Fatal error message titlePlasma Cannot Start Force loading the given shell plugin Hide Desktop Load plasmashell as a standalone application, needs the shell-plugin option to be specified NAME OF TRANSLATORSYour names OSD informing that some media app is muted, eg. Amarok Muted%1 Muted OSD informing that the microphone is muted, keep shortMicrophone Muted OSD informing that the system is muted, keep shortAudio Muted Plasma Plasma Failed To Start Plasma Shell Plasma is unable to start as it could not correctly use OpenGL 2.
 Please check that your graphic drivers are set up correctly. Show Desktop Stop Current Activity Unable to load script file: %1 file mobile internet was disabled, keep shortMobile Internet Off mobile internet was enabled, keep shortMobile Internet On on screen keyboard was disabled because physical keyboard was plugged in, keep shortOn-Screen Keyboard Deactivated on screen keyboard was enabled because physical keyboard got unplugged, keep shortOn-Screen Keyboard Activated touchpad was disabled, keep shortTouchpad Off touchpad was enabled, keep shortTouchpad On wireless lan was disabled, keep shortWifi Off wireless lan was enabled, keep shortWifi On Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-12 03:10+0100
PO-Revision-Date: 2017-02-11 11:00+0100
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <kde-i18n-doc@kde.org>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Aktivér opgavelinje-indgang %1 Aktiviteter... Tilføj panel Bluetooth fra Bluetooth til Plugin til at indstille musehandlinger Genstart ikke plasma-shell automatisk efter et sammenbrud mschlander@opensuse.org Tom %1 Aktivér QML Javascript-fejlsøgning Aktiverer testtilstand og angiver layout-javascript-filen til at sætte testmiljøet op Alle shell-pakker mangler.
Dette er et installationsproblem, kontakt venligst din distribution Shell-pakken %1 kan ikke findes Plasma kan ikke starte Gennemtving indlæsning af det givne shell-plugin Skjul skrivebord Indlæs plasmashell som selvstændigt program. Kræver at tilvalget shell-plugin angives Martin Schlander %1 lydløs Mikrofon på lydløs Sat på lydløs Plasma Plasma kunne ikke starte Plasma Shell Plasma kan ikke starte, da det ikke kunne bruge OpenGL 2.
Tjek at dine grafikdrivere er sat korrekt op. Vis skrivebord Stop aktuel aktivitet Kan ikke indlæse script-fil: %1 fil Mobilt bredbånd fra Mobilt bredbånd til Tastatur på skærmen deaktiveret Tastatur på skærmen aktiveret Touchpad fra Touchpad til Wifi fra Wifi til 