��          4      L       `   :   a      �   �  �   A   p     �                    Looks for documents recently used with names matching :q:. Open Containing Folder Project-Id-Version: krunner_recentdocuments
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-12-17 03:48+0100
PO-Revision-Date: 2016-09-24 22:02+0100
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <dansk@dansk-gruppen.dk>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 Leder efter nyligt anvendte dokumenter med navne som matcher :q:. Åbn indeholdende mappe 