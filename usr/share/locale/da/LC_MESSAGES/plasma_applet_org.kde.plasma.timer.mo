��          �      �           	          &     -     4  
   =     H     Q     Y     i  >   w     �     �     �     �  
   �     �     �     �            U   %  �  {  	   2     <     K     T  	   [     e  	   n     x     |     �  /   �     �     �     �     �  	                       )     8  [   ?                       	                                                
                                 %1 is running %1 not running &Reset &Start Advanced Appearance Command: Display Execute command Notifications Remaining time left: %1 second Remaining time left: %1 seconds Run command S&top Show notification Show seconds Show title Text: Timer Timer finished Timer is running Title: Use mouse wheel to change digits or choose from predefined timers in the context menu Project-Id-Version: plasma_applet_timer
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2015-11-16 22:03+0100
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <kde-i18n-doc@kde.org>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 %1 kører %1 kører ikke &Nulstil &Start Avanceret Udseende Kommando: Vis Kør en kommando Bekendtgørelser Tid tilbage: %1 sekund Tid tilbage: %1 sekunder Kør kommando S&top Vis bekendtgørelse Vis sekunder Vis titel Tekst: Timer Timer udløbet Timeren kører Titel: Brug musehjulet til at ændre cifre eller vælge fra prædefinerede timere i kontekstmenuen 