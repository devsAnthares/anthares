��    $      <  5   \      0     1  +   E     q  4   �  &   �     �     �                     5     :     P     X  ,   u  3   �     �     �               !  '   .     V     u     {     �     �     �     �     �     �  &   �       B        b  �  y  
   %     0     O     _     y     �  
   �  
   �  
   �  ;   �     �     �     	     "	  .   :	  8   i	  "   �	  !   �	     �	     �	  
   �	  %   �	  2   #
     V
     \
     w
     �
     �
     �
  !   �
     �
  $   �
     �
  0        6                                       
                              !                                  $      #                               	                           "             @info:creditAuthor @info:creditCopyright 2006 Sebastian Sauer @info:creditSebastian Sauer @info:shell command-line argumentThe script to run. @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: application descriptionCommand-line utility to run Kross scripts. application nameKross Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2014-12-29 20:05+0100
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <kde-i18n-doc@kde.org>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 Ophavsmand Ophavsret 2006 Sebastian Sauer Sebastian Sauer Scriptet der skal køres. Generelt Tilføj et nyt script. Tilføj... Annullér? Kommentar: erik@binghamton.edu,mschlander@opensuse.org,keld@keldix.com Redigér Redigér valgt script. Redigér... Kør det valgte script. Kunne ikke oprette script for fortolkeren "%1" Fejl under bestemmelse af fortolker til scriptfilen "%1" Kunne ikke indlæse fortolker "%1" Kunne ikke åbne scriptfilen "%1" Fil: Ikon: Fortolker: Sikkerhedsniveau for Ruby-fortolkeren Erik Kjær Pedersen,Martin Schlander,Keld Simonsen Navn: Ingen sådan funktion "%1" Ingen sådan fortolker "%1" Fjern Fjern valgt script. Kør Scriptfilen "%1" eksisterer ikke. Stop Stop afvikling af det valgte script. Tekst: Kommandolinjeprogram til at køre Kross-scripts. Kross 