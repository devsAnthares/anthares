��          �      l      �     �     �  6     A   9     {     �  
   �     �     �     �     �     �     �     �     �     �       
   !     ,     F  �  ^     8     M     _     w     �     �     �     �     �     �     �     �            	   $     .     <  
   C     N  
   U                                                  	                                    
            &Save Comic As... &Strip Number: @option:check Context menu of comic image&Actual Size @option:check Context menu of comic imageStore current &Position Advanced All Appearance Destination: From beginning to ... From end to ... General Go to Strip Information Manual range Range: Update an abbreviation for Number# %1 dd.MM.yyyy in a range: from toFrom: in a range: from toTo: Project-Id-Version: plasma_applet_comic
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-07-18 03:20+0200
PO-Revision-Date: 2013-10-24 13:50+0300
Last-Translator: Sergiu Bivol <sergiu@ase.md>
Language-Team: Romanian <kde-i18n-ro@kde.org>
Language: ro
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2;
X-Generator: Lokalize 1.5
 &Salvare benzi ca... &Numărul benzii: Scalează la &conținut Păstrează &poziția actuală Avansat Toate Aspect Destinație: De la început până la... De la sfârșit până la... General Mergi la banda Informații Diapazon manual Diapazon: Actualizează nr. %1 zz.LL.aaaa De la: Până la: 