��    '      T  5   �      `     a     g  
   �  +   �  
        )     5     N      \     }     �     �  	   �      �  �   �  e   �  *   �  H        [     b     �  )   �  ;   �  	   �     �  (        F     ^     t     �     �     �     �  	   �       #        B     X  �  x     O
  u   U
  
   �
  9   �
  	     
        %     ?  $   U     z  5   �     �     �     �  �   �  k   �  ,      Q   -  
        �     �  <   �  P   �     6  +   C  2   o     �     �     �     �          !     <     X     a  '   �     �      �           	                 "                     %   
                                          #            &               !                             '         $                   msec <h1>Multiple Desktops</h1>In this module, you can configure how many virtual desktops you want and how these should be labeled. Animation: Assigned global Shortcut "%1" to Desktop %2 Desktop %1 Desktop %1: Desktop Effect Animation Desktop Names Desktop Switch On-Screen Display Desktop Switching Desktop navigation wraps around Desktops Duration: EMAIL OF TRANSLATORSYour emails Enable this option if you want keyboard or active desktop border navigation beyond the edge of a desktop to take you to the opposite edge of the new desktop. Enabling this option will show a small preview of the desktop layout indicating the selected desktop. Here you can enter the name for desktop %1 Here you can set how many virtual desktops you want on your KDE desktop. Layout NAME OF TRANSLATORSYour names No Animation No suitable Shortcut for Desktop %1 found Shortcut conflict: Could not set Shortcut %1 for Desktop %2 Shortcuts Show desktop layout indicators Show shortcuts for all possible desktops Switch One Desktop Down Switch One Desktop Up Switch One Desktop to the Left Switch One Desktop to the Right Switch to Desktop %1 Switch to Next Desktop Switch to Previous Desktop Switching Walk Through Desktop List Walk Through Desktop List (Reverse) Walk Through Desktops Walk Through Desktops (Reverse) Project-Id-Version: kcm_kwindesktop
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-06 05:35+0100
PO-Revision-Date: 2013-01-30 23:00+0200
Last-Translator: Sergiu Bivol <sergiu@ase.md>
Language-Team: Romanian <kde-i18n-ro@kde.org>
Language: ro
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2;

X-Generator: Lokalize 1.5
  msec <h1>Birouri multiple</h1>În acest modul puteți configura câte birouri virtuale doriți și cum să fie etichetate. Animație: Acceleratorul global „%1” a fost atribuit biroului %2 Biroul %1 Biroul %1: Animație efecte de birou Denumirile birourilor Afișaj pe ecran la comutare birouri Comutare birouri Navigarea biroului se încadrează în jurul acestuia Birouri Durată: sergiu@ase.md Activați această opțiune dacă doriți ca navigarea din tastatură sau margini magnetice dincolo marginea ecranului să vă aducă tot în același ecran dar în partea opusă. Activarea acestei opțiuni va afișa o mică previzualizare a așezării birourilor ce indică biroul ales. Aici puteți introduce denumirea biroului %1 Aici puteți stabili câte birouri virtuale doriți pe biroul dumneavoastră KDE. Aranjament Sergiu Bivol Fără animație Nu au fost găsiți acceleratori potriviți pentru biroul %1 Conflict de acceleratori: Nu s-a putut stabili acceleratorul %1 pentru biroul %2 Acceleratori Arată indicatorii de așezare a birourilor Arată scurtături pentru toate birourile posibile Comută un birou în jos Comută un birou în sus Comută un birou la stânga Comută un birou la dreapta Comută la biroul %1 Comută la biroul următor Comută la biroul precedent Comutare Comută prin lista de ferestre Comută prin lista de ferestre (invers) Comută printre birouri Comută printre birouri (invers) 