��          �      <      �     �  
   �  /   �  -   �        
   1     <     I     _     h  	   }     �     �     �     �     �  1   �  �  �     �  
   �     �     �     �       
          
   2     =     W     `     h     u     }     �     �                      
                                             	                                  %1@%2 %2@%3 (%1) @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon All Applications Appearance Applications Applications updated. Computer Edit Applications... Favorites History Icon: Leave Show applications by name Switch tabs on hover Type is a verb here, not a nounType to search... Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2013-11-23 20:35+0200
Last-Translator: Sergiu Bivol <sergiu@ase.md>
Language-Team: Romanian <kde-i18n-ro@kde.org>
Language: ro
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2;
X-Generator: Lokalize 1.5
 %1@%2 %2@%3 (%1) Alegeți... Resetați pictograma Toate aplicațiile Aspect Aplicații Aplicații actualizate. Calculator Editează aplicațiile... Favorite Istoric Pictogramă: Pleacă Arată aplicațiile după nume Comută filele la planare Tastați pentru a căuta... 