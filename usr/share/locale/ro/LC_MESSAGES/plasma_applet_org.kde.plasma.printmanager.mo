��          T      �       �      �      �      �   .   �        H   +  �  t     ;     Q     b  9   |     �  {   �                                        Active jobs only All jobs Completed jobs only No printers have been configured or discovered Print queue is empty There is one print job in the queue There are %1 print jobs in the queue Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2015-02-21 09:37+0000
PO-Revision-Date: 2013-10-24 14:10+0300
Last-Translator: Sergiu Bivol <sergiu@ase.md>
Language-Team: Romanian <kde-i18n-ro@kde.org>
Language: ro
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2;
X-Generator: Lokalize 1.5
 Numai lucrări active Toate lucrările Numai lucrări finalizate Nicio imprimantă nu a fost configurată sau descoperită Coada de tipărire e goală Este o lucrare de tipărire în coadă Sunt %1 lucrări de tipărire în coadă Sunt %1 de lucrări de tipărire în coadă 