��    *      l  ;   �      �  A   �     �  /        2     ;     O     a     w     �     �  	   �     �  3   �  	   �     �      �  $      *   E     p  	   u  5        �     �  
   �     �          $  5   3  6   i     �  ,   �  /   �     	        O   0  Z   �  b   �  	   >  
   H  P   S     �  �  �  9   �
     �
  T   �
  	   0     :     O     k     �     �     �     �     �  6   �               "  "   0     S     [     b  ?   y  1   �     �     	          $     1  M   L  H   �     �  D   �     7     >     W  \   n  `   �  f   ,     �     �  k   �     *            )             &      $   	   %         '                                                !      
       *                     "                                                 (                 #    %1 is an external application and has been automatically launched (c) 2009, Ben Cooksley <i>Contains 1 item</i> <i>Contains %1 items</i> About %1 About Active Module About Active View About System Settings Apply Settings Author Ben Cooksley Configure Configure your system Determines whether detailed tooltips should be used Developer Dialog EMAIL OF TRANSLATORSYour emails Expand the first level automatically General config for System SettingsGeneral Help Icon View Internal module representation, internal module model Internal name for the view used Keyboard Shortcut: %1 Maintainer Mathias Soeken NAME OF TRANSLATORSYour names No views found Provides a categorized icons view of control modules. Provides a classic tree-based view of control modules. Relaunch %1 Reset all current changes to previous values Search through a list of control modulesSearch Show detailed tooltips System Settings System Settings was unable to find any views, and hence has nothing to display. System Settings was unable to find any views, and hence nothing is available to configure. The settings of the current module have changed.
Do you want to apply the changes or discard them? Tree View View Style Welcome to "System Settings", a central place to configure your computer system. Will Stephenson Project-Id-Version: systemsettings
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:08+0100
PO-Revision-Date: 2009-08-12 19:41+0300
Last-Translator: Sergiu Bivol <sergiu@ase.md>
Language-Team: Română <kde-i18n-ro@kde.org>
Language: ro
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2;

X-Generator: Lokalize 1.0
 %1 este o aplicație externă și a fost lansată automat (c) 2009, Ben Cooksley <i>Conține 1 element</i> <i>Conține %1 elemente</i> <i>Conține %1 de elemente</i> Despre %1 Despre modulul activ Despre vizualizarea activă Despre Configurări de sistem Aplică configurările Autor Ben Cooksley Configurează Configurează sistemul Determină dacă să fie utilizate indiciile detaliate Dezvoltator Dialog sergiu@ase.md Desfășoară automat primul nivel General Ajutor Vizualizare pictograme Reprezentarea internă a modulelor, modelul intern al modulelor Denumirea internă pentru vizualizarea utilizată Accelerator de tastatură: %1 Responsabil Mathias Soeken Sergiu Bivol Nicio vizualizare găsită Furnizează o vizualizare categorisită a pictogramelor modulelor de control. Furnizează o vizualizare arborescentă clasică a modulelor de control. Relansează %1 Reinițializează toate modificările actuale la valorile precedente Caută Arată indicii detaliate Configurări de sistem Configurări de sistem nu a putut găsi nicio vizualizare, de aceea nu are nimic de afișat. Configurări de sistem nu a putut găsi nicio vizualizare, de aceea nu este nimic de configurat. Configurările modulului actual s-au modificat.
Doriți să aplicați modificările sau le eliminați? Vizualizare arborescentă Stil vizualizare Bine ați venit în „Configurări de sistem”, locul central de configurare a sistemului dumneavoastră. Will Stephenson 