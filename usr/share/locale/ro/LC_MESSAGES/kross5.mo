��            )   �      �  &   �     �     �     �     �      �               .     6  ,   S  3   �     �     �     �     �     �  '        4     S     Y     o     �     �     �     �     �  &   �     �  �  �     �     �  
   �     �       5        E     M     g     t  :   �  6   �  .   �  *   .     Y     b     o  -   |  >   �     �     �  !   	     .	     7	     P	     Y	     x	  $   �	     �	                                       
                      	                                                                            @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2014-03-26 19:20+0200
Last-Translator: Sergiu Bivol <sergiu@ase.md>
Language-Team: Romanian <kde-i18n-ro@kde.org>
Language: ro
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2;
 General Adaugă un nou script. Adaugă... Renunțați? Comentariu: ,sergiu@ase.md,lbuz@rolix.org,onet.cristian@gmail.com Editare Editează script-ul ales. Editează... Execută script-ul ales. Eșec la crearea script-ului pentru interpretorul „%1” Eșec la determinarea interpretorului pentru „%1”. Eșec la încărcarea interpretorului „%1” Eșec la deschiderea script-ului „%1”. Fișier: Pictogramă: Interpretor: Nivelul de siguranță a interpretorului Ruby Claudiu Costin,Sergiu Bivol,Laurențiu Buzdugan,Cristian Oneț Nume: Nu există funcția „%1” Nu există interpretorul „%1” Elimină Elimină script-ul ales. Rulează Fișierul „%1” nu există. Oprește Oprește execuția script-ului ales. Text: 