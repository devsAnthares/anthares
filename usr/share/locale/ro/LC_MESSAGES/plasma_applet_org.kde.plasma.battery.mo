��          �      <      �  7   �  	   �     �     �               3     A     I     ]  #   d  %   �     �  5   �                 �  &          	          "     /  !   B     d     v     ~     �     �     �  %   �  D   �          )     A                                                   	                       
                           Battery is currently not present in the bayNot present Capacity: Charging Discharging Display Brightness Enable Power Management Fully Charged General Keyboard Brightness Model: Placeholder is battery capacity%1% Placeholder is battery percentage%1% Power management is disabled The battery applet has enabled system-wide inhibition Time To Empty: Time To Full: Vendor: Project-Id-Version: plasma_applet_battery
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-03 03:15+0100
PO-Revision-Date: 2013-11-23 20:31+0200
Last-Translator: Sergiu Bivol <sergiu@ase.md>
Language-Team: Romanian <kde-i18n-ro@kde.org>
Language: ro
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2;
X-Generator: Lokalize 1.5
 Absent Capacitate: Se încarcă Se descarcă Luminozitate ecran Activează gestiunea alimentării Încărcat deplin General Luminozitate tastatură Model: %1% %1% Gestiunea alimentării e dezactivată Miniaplicația de acumulator a activat inhibiția întregului sistem Timp până la epuizare: Timp până la umplere: Vânzător: 