��          �      �        '   	  "   1     T  
   n     y     �     �  ?   �     �     �        %   0     V  >   p     �     �  '   �  !        /     O     n  3   �  �  �     �     �     �  	   �  
   �     �     �     �  	                  )  
   A     L     d     t     y     �     �     �     �     �                                    	   
                                                                 Forecast period timeframe1 Day %1 Days High & Low temperatureH: %1 L: %2 High temperatureHigh: %1 Kilometers Low temperatureLow: %1 Miles Short for no data available- Shown when you have not set a weather providerPlease Configure Units Wind conditionCalm distance, unitVisibility: %1 %2 ground temperature, unitDewpoint: %1 humidex, unitHumidex: %1 pressure tendency, rising/falling/steadyPressure Tendency: %1 pressure, unitPressure: %1 %2 temperature, unit%1%2 visibility from distanceVisibility: %1 weather warningsWarnings Issued: weather watchesWatches Issued: wind direction, speed%1 %2 %3 windchill, unitWindchill: %1 winds exceeding wind speed brieflyWind Gust: %1 %2 Project-Id-Version: plasma_applet_weather
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-28 05:50+0100
PO-Revision-Date: 2009-07-26 12:51+0300
Last-Translator: Sergiu Bivol <sergiu@ase.md>
Language-Team: Romanian <kde-i18n-ro@kde.org>
Language: ro
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2;
X-Generator: Lokalize 1.0
 1 zi %1 zile %1 de zile Î: %1 J: %2 Înaltă: %1 Kilometri Joasă: %1 Mile - Vă rugăm să configurați Unități Calm Vizibilitate: %1 %2 Punct de condensare: %1 Umidex: %1 Tendință presiune: %1 Presiune: %1 %2 %1%2 Vizibilitate: %1 Avertizări emise: Prognoze emise: %1 %2 %3 Răcoare: %1 Rafale: %1 %2 