��          \      �       �      �   -   �        -   +  #   Y  #   }     �  �  �     {  .   �     �  -   �     	                                                 Displays the current date Displays the current date in a given timezone Displays the current time Displays the current time in a given timezone Note this is a KRunner keyworddate Note this is a KRunner keywordtime Today's date is %1 Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2010-07-17 19:39+0300
Last-Translator: Sergiu Bivol <sergiu@ase.md>
Language-Team: Romanian <kde-i18n-ro@kde.org>
Language: ro
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.1
Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2;
 Afișează data curentă Afișează data curentă într-un fus orar dat Afișează ora curentă Afișează ora curentă într-un fus orar dat data ora Astăzi este %1 