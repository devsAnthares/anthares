��    (      \  5   �      p     q     w     �     �     �     �  !   �  '   �     &     5  -   G     u     z     �     �     �     �      �     �                  
   .     9     X     j  A   �     �     �     �     �     �  �     ;   �     �     �       %     b   6  �  �     `	     f	     �	     �	  #   �	  #   �	     �	     
     4
     A
  #   W
     {
     �
     �
     �
     �
     �
     �
     �
     �
          )     <     H     U     l  N   �     �  
   �     �     �       �   !  I   �     '     9  
   L  #   W  �   {                       	          #               "   '                       &   !                           %                      
                   (                $               %1 Hz &Disable All Outputs &Reconfigure (c), 2012-2013 Daniel Vrátil 90° Clockwise 90° Counterclockwise @title:windowDisable All Outputs @title:windowUnsupported Configuration Active profile Advanced Settings Are you sure you want to disable all outputs? Auto Break Unified Outputs Configuration for displays Daniel Vrátil Display Configuration Display: EMAIL OF TRANSLATORSYour emails Enabled Identify outputs KCM Test App Laptop Screen Maintainer NAME OF TRANSLATORSYour names No Primary Output No available resolutions No kscreen backend found. Please check your kscreen installation. Normal Orientation: Primary display: Refresh rate: Resolution: Sorry, your configuration could not be applied.

Common reasons are that the overall screen size is too big, or you enabled more displays than supported by your GPU. Tip: Hold Ctrl while dragging a display to disable snapping Unified Outputs Unify Outputs Upside Down Warning: There are no active outputs! Your system only supports up to %1 active screen Your system only supports up to %1 active screens Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-24 03:18+0200
PO-Revision-Date: 2015-04-20 14:41+0300
Last-Translator: Sergiu Bivol <sergiu@cip.md>
Language-Team: Romanian <kde-i18n-ro@kde.org>
Language: ro
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2;
X-Generator: Lokalize 1.5
 %1 Hz &Dezactivează toate ieșirile &Reconfigurează (c), 2012-2013 Daniel Vrátil 90° în sensul acelor de ceasornic 90° împotriva acelor de ceasornic Dezactivează toate ieșirile Configurare nesusținută Profil activ Configurări avansate Sigur dezactivați toate ieșirile? Automat Desfă ieșirile unificate Configurare pentru afișaje Daniel Vrátil Configurare afișaj Afișaj: sergiu@cip.md Activat Identifică ieșirile Aplicație de test KCM Ecranul laptopului Responsabil Sergiu Bivol Nicio ieșire primară Nicio rezoluție disponibilă Nu a fost găsit niciun suport pentru kscreen. Verificați instalarea kscreen. Normală Orientare: Afișaj primar: Rata de împrospătare: Rezoluție: Ne pare rău, configurarea nu a putut fi aplicată.

Motive posibile pot fi o dimensiune totală prea mare a ecranului, sau ați activat mai multe afișaje decât suportă placa grafică. Sfat: Țineți Ctrl la tragerea unui afișaj pentru a dezactiva bruscarea Ieșiri unificate Unifică ieșirile Răsturnat Avertizare: Nu sunt ieșiri active! Sistemul dumneavoastră susține numai până la %1 ecran activ Sistemul dumneavoastră susține numai până la %1 ecrane active Sistemul dumneavoastră susține numai până la %1 de ecrane active 