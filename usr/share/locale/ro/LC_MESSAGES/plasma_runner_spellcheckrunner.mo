��    	      d      �       �      �      �           "     *  Q   ?     �     �  �  �     �     �     �     �      �            
   "                            	                  &Require trigger word &Trigger word: Checks the spelling of :q:. Correct Spell Check Settings Spelling checking runner syntax, first word is trigger word, e.g.  "spell".%1:q: Suggested words: %1 spell Project-Id-Version: krunner_spellcheckrunner
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2009-07-26 12:52+0300
Last-Translator: Sergiu Bivol <sergiu@ase.md>
Language-Team: Romanian <kde-i18n-ro@kde.org>
Language: ro
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2;
X-Generator: Lokalize 1.0
 &Necesită cuvânt declanșator &Cuvânt declanșator: Verifică ortografia lui :q:. Corect Configurări corector ortografic %1:q: Cuvinte sugerate: %1 ortografie 