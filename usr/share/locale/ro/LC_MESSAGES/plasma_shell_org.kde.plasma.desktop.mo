��    9      �  O   �      �  
   �  
   �  
   �     
               ,     2     A  	   I     S     Z  
   a     l     s     y     �     �     �     �     �     �  
   �     �     �     �     �     �               $     )     7     H     V     Y     i     v     |     �  	   �     �     �     �  b   �  �   #     �  	   �     �  
   �  	   �     �                    %  �  6     �	     

     
     .
     C
     G
     \
     d
     {
     �
     �
  	   �
  	   �
     �
     �
     �
     �
     �
     �
     �
     �
               .     I     U     ]  
   j     u     �     �     �     �     �     �     �     �     �                     ,     2     G  f   O  �   �     y     }     �     �     �  
   �     �     �     �     �     #         6   	            /   5   
             7      9   (                ,   !      3         &   1                      )                8          -          0   .      %       '      *               4       "                              +                       $          2    Activities Add Action Add Spacer Add Widgets... Alt Always Visible Apply Apply Settings Author: Auto Hide Bottom Cancel Categories Center Close Create activity... Ctrl Delete Email: Get new widgets Height Horizontal-Scroll Input Here Keyboard shortcuts Layout: Left Left-Button License: Lock Widgets Maximize Panel Meta Middle-Button More Settings... Mouse Actions OK Panel Alignment Remove Panel Right Right-Button Screen Edge Search... Shift Stopped activities: Switch The settings of the current module have changed. Do you want to apply the changes or discard them? This shortcut will activate the applet: it will give the keyboard focus to it, and if the applet has a popup (such as the start menu), the popup will be open. Top Uninstall Vertical-Scroll Visibility Wallpaper Wallpaper Type: Widgets Width Windows Can Cover Windows Go Below Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-09 03:10+0200
PO-Revision-Date: 2015-04-26 15:16+0300
Last-Translator: Sergiu Bivol <sergiu@cip.md>
Language-Team: Romanian <kde-i18n-ro@kde.org>
Language: ro
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2;
X-Generator: Lokalize 1.5
 Activități Adaugă acțiune Adaugă spațiator Adaugă controale... Alt Întotdeauna vizibil Aplică Aplică configurările Autor: Ascunde automat Jos Renunță Categorii Centru Închide Creează activitate... Ctrl Șterge Email: Obține controale noi Înălțime Derulare orizontală Introduceți aici Acceleratori de tastatură Aranjament: Stânga Buton stâng Licență: Blochează controalele Maximizează panoul Meta Buton mijlociu Mai multe configurări... Acțiuni maus OK Aliniere panou Elimină panoul Dreapta Buton drept Margine ecran Căutare... Shift Activități oprite: Comută Configurările modulului actual s-au modificat. Doriți să aplicați modificările sau le eliminați? Acest accelerator va activa miniaplicația: îi va transfera focalizarea tastaturii și dacă miniaplicația are o fereastră-balon (cum ar fi meniul de pornire), fereastra-balon se va deschide. Sus Dezinstalează Derulare verticală Vizibilitate Tapet Tip tapet: Controale grafice Lățime Ferestrele pot acoperi Ferestrele merg dedesubt 