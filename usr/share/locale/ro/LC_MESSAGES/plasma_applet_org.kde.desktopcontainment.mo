��    !      $  /   ,      �     �     �                         *     ;     I     Q     Y  
   _     j     w          �     �     �     �     �     �     �     �  
                   .     <     T     Z     b     t  �  �     m     v     �  	   �  	   �     �     �      �     �       	        $     3     F     O  %   a     �     �  "   �  
   �     �  '   �                  !   8     Z     i     �     �     �     �                                                                                        
                                        	                           !    &Delete &Empty Trash Bin &Move to Trash &Open &Paste &Properties &Refresh Desktop &Refresh View &Reload &Rename Align Arrange In Custom title Default Deselect All Enter custom title here File name pattern: File types: Hide Files Matching Icons Large More Preview Options... None Select All Show All Files Show Files Matching Show a place: Show the Desktop folder Small Sort By Specify a folder: Type a path or a URL here Project-Id-Version: plasma_applet_folderview
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:11+0100
PO-Revision-Date: 2013-10-13 12:29+0300
Last-Translator: Sergiu Bivol <sergiu@ase.md>
Language-Team: Romanian <kde-i18n-ro@kde.org>
Language: ro
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2;
X-Generator: Lokalize 1.5
 Ș&terge Gol&ește coșul de gunoi &Mută la gunoi &Deschide Li&pește &Proprietăți &Reîmprospătează biroul &Reîmprospătează vizualizarea &Reîncarcă &Redenumește Aliniază Aranjează în Titlu personalizat Implicit Deselectează tot Introduceți aici titlul personalizat Șablon denumire: Tipuri de fișier: Ascunde fișierele ce se potrivesc Pictograme Mare Mai multe opțiuni de previzualizare... Niciunul Selectează tot Arată toate fișierele Arată fișierele ce se potrivesc Arată un loc: Afișează dosarul biroului Mică Sortează după Specifică un dosar: Tastați aici o cale sau un URL 