��          �      <      �     �  �   �  )   d     �     �     �     �     �      �       '        =     \     a  ?   r  Y   �  +     �  8     �          ;  
   S  (   ^     �  	   �  "   �     �     �  '   �               !  B   5  Z   x  )   �                                                                 
                             	       (c) 2003-2007 Fredrik Höglund @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) @item:inlistbox sizeResolution dependent Confirmation Cursor Settings Changed Cursor Theme Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Fredrik Höglund Get new color schemes from the Internet NAME OF TRANSLATORSYour names Name Overwrite Theme? The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2015-04-26 15:01+0300
Last-Translator: Sergiu Bivol <sergiu@cip.md>
Language-Team: Romanian <kde-i18n-ro@kde.org>
Language: ro
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2;
X-Generator: Lokalize 1.5
 (c) 2003-2007 Fredrik Höglund (Dimensiuni disponibile: %1) Dependent de rezoluție Confirmare Configurările cursorului s-au modificat Temă de cursor Descriere Trageți sau scrieți URL-ul temei sergiu@cip.md Fredrik Höglund Preia scheme de culori noi din Internet Sergiu Bivol Denumire Suprascrieți tema? Fișierul %1 nu pare să fie o arhivă validă cu temă de cursor. Nu s-a putut descărca arhiva temei de cursori. Verificați dacă adresa %1 este corectă. Nu s-a găsit arhiva temei de cursori %1. 