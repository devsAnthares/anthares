��    '      T  5   �      `     a     j     s     �     �  -   �  r   �  	   H  	   R     \     d     m  
   z     �     �      �     �     �     �     �          !  	   &     0     6     >     K     Y     r     �     �     �  +   �     �     �  S   �  )   I     s  �         	   %     /     =     D  +   b  g   �     �  	   �     	  
   	     	      	  
   .	     9	     Y	     n	     �	     �	     �	  0   �	     �	     �	     �	     �	     �	     
     
     8
     U
     r
     y
  -   �
     �
  	   �
  N   �
  (        =                            "         #             $              %                    	                   &                              !   '                          
               &Amount: &Effect: &Semi-transparent &Theme (c) 2000-2003 Geert Jansen <qt>Installing <strong>%1</strong> theme</qt> A problem occurred during the installation process; however, most of the themes in the archive have been installed Ad&vanced All Icons Co&lor: Colorize Confirmation Desaturate Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Effect Parameters Gamma Icons Icons Control Panel Module NAME OF TRANSLATORSYour names Name No Effect Panel Preview Remove Theme Set Effect... Setup Active Icon Effect Setup Default Icon Effect Setup Disabled Icon Effect Size: Small Icons The file is not a valid icon theme archive. To Gray Toolbar Unable to download the icon theme archive;
please check that address %1 is correct. Unable to find the icon theme archive %1. Use of Icon Project-Id-Version: kcmicons
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-08 06:27+0100
PO-Revision-Date: 2004-07-03 07:09+0100
Last-Translator: KD at KGyfieithu <kyfieithu@dotmon.com>
Language-Team: Cymraeg <cy@li.org>
Language: cy
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.2

Plural-Forms: nplurals=2; plural=n != 1;
 &Maint: &Effaith: &Lled-dryloyw &Thema (h)(c) 2000-2003 Geert Jansen <qt>Yn gosod thema <strong>%1</strong></qt> Bu problem yn ystod y broses gosod; serch hynny, arsefydlwyd y rhan fwyaf o'r themau oedd yn yr archif. &Uwch Pob Eicon &Lliw: Lliweiddio Gwiriad Dad-ddirlenwi Disgrifiad Llusgwch neu Deipiwch URL Thema kyfieithu@dotmon.com Paramedrau Effaith Gama Eiconau Modwl Panel Rheoli Eiconau Owain Green drwy KGyfieithu - Meddalwedd Gymraeg Enw Dim Effaith Panel Rhagolwg Gwaredu Thema Gosod Effaith... Gosod Effaith Eicon Gweithredol Gosod Effaith Eicon Rhagosod Gosod Effaith Eicon Analluog Maint: Eiconau Bach Nid yw'r ffeil yn archif thema eiconau dilys. I Lwyd Bar Offer Methu lawrlwytho'r archif thema eiconau;
gwiriwch bod y cyfeiriad %1 yn gywir. Methu canfod yr archif thema eiconau %1. Defnydd Eicon 