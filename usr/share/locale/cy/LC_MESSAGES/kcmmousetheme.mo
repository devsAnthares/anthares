��          �      �       H  `   I     �     �     �     �      �          2     7     H  ?   U  Y   �  +   �  �    i   �  	   M     W  
   v     �     �     �     �     �     �  <   �  O   -  *   }     
                           	                                 A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. Project-Id-Version: kcminput
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2004-07-03 07:13+0100
Last-Translator: KD at KGyfieithu <kyfieithu@dotmon.com>
Language-Team: Cymraeg <cy@li.org>
Language: cy
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
www.gyfieithu.co.uk<kyfieithu@dotmon.com>
X-Generator: KBabel 1.2

Plural-Forms: nplurals=2; plural=n != 1;
 Mae thema o'r enw %1 yn bodoli eisoes yn eich plygell temau eicon.  Ydych eisiau ei amnewid efo'r un yma? Cadarnhad Newidwyd Gosodiadau'r Cyrchydd Disgrifiad Llusgo neu Deipio URL y Thema kyfieithu@dotmon.com KD wrth KGyfieithu Enw  Drosysgrifo'r thema?  Gwaredu Thema Ymddengys nad yw'r ffeil %1 yn archif themau cyrchydd dilys. Methu lawrlwytho'r archif themau cyrchydd; gwiriwch bod y cyfeiriad %1 yn iawn. Methu canfod yr archif themau cyrchydd %1. 