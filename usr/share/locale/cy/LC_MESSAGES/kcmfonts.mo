��          �      ,      �     �  /   �     �     �     �     �            I   ,     v  "   z     �  6   �  *   �          )  �  6     �  4   �          $     0  !   O     q       N   �     �  ,   �  #     H   ?  7   �     �     �                                                           
      	              to  A non-proportional font (i.e. typewriter font). Ad&just All Fonts... BGR Click to change all fonts Configure Anti-Alias Settings Configure... E&xclude range: Hinting is a process used to enhance the quality of fonts at small sizes. RGB Used by menu bars and popup menus. Used by the window titlebar. Used for normal text (e.g. button labels, list items). Used to display text beside toolbar icons. Vertical BGR Vertical RGB Project-Id-Version: kcmfonts
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-24 05:54+0100
PO-Revision-Date: 2004-07-03 07:04+0100
Last-Translator: KD at KGyfieithu <kyfieithu@dotmon.com>
Language-Team: Cymraeg <cy@li.org>
Language: cy
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.2
Plural-Forms: nplurals=2; plural=n != 1;
 i  Wynebfath anghyfraneddol (h.y. wynebfath teipiadur). Adda&su Pob Wynebfath... GlGwC (BGR) Cliciwch i newid pob wynebfath Furfweddu Gosodiadau Gwrth-Amgenu Ffurfweddu... Gwa&hardd amrediad: Proses a ddefnyddir i wella ansawdd wynebfathau wrth feintiau bach yw awgrymu. CGwGl (RGB) Defnyddir gan farrau dewislenni a naidlenni. Defnyddir gan far teitl y ffenestr. Defnyddir ar gyfer testun arferol (e.e. labeli botymau, eitemau rhestr). Defnyddir i ddangos testun heblaw am eiconau bar offer. GlGwC Fertigol CGwGl Fertigol 