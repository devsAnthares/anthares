��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  �  K     �     �  �    p  �  L   
     d
     u
     �
     �
     �
     �
     �
     �
                                          
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2003-12-29 08:05+0000
Last-Translator: KD at KGyfieithu <kyfieithu@dotmon.com>
Language-Team: Cymraeg <cy@li.org>
Language: cy
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.2
Plural-Forms: nplurals=2; plural=n != 1;
 eil Goramser &dynodiad ymgychwyn: <h1>Hysbysiad Bar Tasgau</h1>
Gallwch alluogi ail ddull o hysbysiad ymgychwyn sy'n cael ei ddefnyddio 
gan y bar tasgau pan ymddengys botwm efo awrwydr sy'n cylchdroi, 
yn dynodi bod eich cymhwysiant sydd wedi ymgychwyn yn llwytho. 
Gall ddigwydd bod rhai cymwysiadau yn anadnabyddus o'r hybysiad 
ymgychwyn yma.  Os felly, diflana'r botwm ar ôl yr amser a phenodwyd 
yn yr adran 'Goramser dynodiad ymgychywn' <h1>Cyrchydd Prysur</h1>
Cyniga KDE gyrchydd prysur am hysbysiad ymcychwyn cymwysiadau.
I alluogi'r cyrchydd prysur, dewiswch un math o adborth gweledol o'r blwch cyfun.
Gall ddigwydd bod rhai cymwysiadau yn anadnabyddus o'r hybysiad 
ymgychwyn yma.  Os felly, bydd y cyrchydd yn peidio ag amrantu ar ôl 
yr amser a phenodwyd yn yr adran 'Goramser dynodiad ymcychywn' <h1>Adborth Lansio</h1> Gallwch ffurfweddu'r adborth lansio-cymhwysiant yma. Cyrchydd Amrantu Cyrchydd Sboncio Cyrchydd Pr&ysur Galluogi hysbysiad &bar tasgau Dim Cyrchydd Prysur Cyrchydd Prysur Goddefol Goramser dynodiad cych&wyn Hysbysiad &Bar Tasgau 