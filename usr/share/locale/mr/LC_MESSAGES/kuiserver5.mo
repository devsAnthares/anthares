��          �   %   �      P     Q     b     w     �     �     �  
   �     �     �     �     �     �  /        3     Q     p     v     �     �     �     �     �     �     �       �  %  %   �  1   �  I   "  e   l  8   �  F        R     b     r     �  "   �  2   �  �   �  9   �  :   �     �  /   	  0   8	  )   i	  ?   �	  @   �	  U   
  V   j
  /   �
  0   �
                            
                                                                                             	    %1 file %1 files %1 folder %1 folders %1 of %2 processed %1 of %2 processed at %3/s %1 processed %1 processed at %2/s Appearance Behavior Cancel Clear Configure... Finished Jobs List of running file transfers/jobs (kuiserver) Move them to a different list Move them to a different list. Pause Remove them Remove them. Resume Show all jobs in a list Show all jobs in a list. Show all jobs in a tree Show all jobs in a tree. Show separate windows Show separate windows. Project-Id-Version: kuiserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2013-02-23 17:39+0530
Last-Translator: Chetan Khona <chetan@kompkin.com>
Language-Team: Marathi <kde-i18n-doc@kde.org>
Language: mr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n!=1);
X-Generator: Lokalize 1.5
 %1 फाईल %1 फाईल्स %1 संचयीका %1 संचयीका %2 पैकी %1, प्रक्रीया केला गेला %2 पैकी %1, %3/सेकंदाने प्रक्रीया केला गेला %1 प्रक्रीया केला गेला  %1, %2/s ने प्रक्रीया केला गेला दर्शन वर्तन रद्द करा रिकामी करा संयोजीत करा... पूर्ण केलेले कार्य फाईल स्थानांतरण/प्रक्रीया सुरु असलेल्यांची यादी वेगळे यादी करिता हलवा वेगळे यादी करिता हलवा. थांबवा त्यांस काढून टाका त्यांस काढून टाका. पुन्हा सुरु करा यादीत सर्व कार्य दर्शवा यादीत सर्व कार्य दर्शवा. वृक्ष अंतर्गत सर्व कार्य दर्शवा वृक्ष अंतर्गत सर्व कार्य दर्शवा. वेगळे चौकट दर्शवा वेगळे चौकट दर्शवा. 