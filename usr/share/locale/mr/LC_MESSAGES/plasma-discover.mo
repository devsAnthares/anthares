��          �   %   �      `  5   a  #   �  E   �  E     >   G     �     �     �     �  <   �     �     �  *        0  	   8     B     R  
   Y  :   d     �     �     �     �  	   �     �  
   �  �  �  �   �  h     r   q  r   �  k   W  8   �  (   �     %	     2	  �   I	     �	     �	  X   �	  .   N
  7   }
     �
  %   �
  #   �
  �     ,   �     �     �  *        @     P  /   e               
             	                                                                                                <em>%1 out of %2 people found this review useful</em> <em>Tell us about this review!</em> <em>Useful? <a href='true'><b>Yes</b></a>/<a href='false'>No</a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'><b>No</b></a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'>No</a></em> Aleix Pol Gonzalez Available modes:
 Back Cancel Directly open the specified application by its package name. Discard Discover Display a list of entries with a category. Install Installed Jonathan Thomas Launch Loading... Open with a program that can deal with the given mimetype. Rating: Remove Review Search in '%1'... Search... Summary: Update All Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:02+0100
PO-Revision-Date: 2013-04-01 10:16+0530
Last-Translator: Chetan Khona <chetan@kompkin.com>
Language-Team: Marathi <kde-i18n-doc@kde.org>
Language: mr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n!=1);
X-Generator: Lokalize 1.5
 <em>%2 पैकी %1 व्यक्तिंना हि प्रतिक्रिया उपयोगी वाटली</em> <em>या प्रतिक्रियेबद्दल आम्हाला सांगा !</em> <em>उपयोगी आहे का? <a href='true'><b>होय</b></a>/<a href='false'>नाही</a></em> <em>उपयोगी आहे का? <a href='true'>होय</a>/<a href='false'><b>नाही</b></a></em> <em>उपयोगी आहे का? <a href='true'>होय</a>/<a href='false'>नाही</a></em> एलैक्स पोल गोन्झालेझ उपलब्ध पद्धती :
 मागे रद्द करा निर्देशीत अनुप्रयोग त्याच्या पॅकेज नावाने उघडा. सोडून द्या शोधा नोंदींची यादी विभागासहित दर्शवा. प्रतिष्ठापीत करा प्रतिष्ठापीत केलेले जोनाथन थोमस प्रक्षेपण करा दाखल करत आहे... दिलेल्या माइम प्रकाराशी चालू शकेल अशा कार्यक्रमात उघडा. गुणवत्ताश्रेणी : काढून टाका समीक्षा यामध्ये शोधा '%1'... शोधा... सारांश : सर्व अद्ययावत करा 