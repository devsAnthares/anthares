��    $      <  5   \      0     1  +   E     q  4   �  &   �     �     �                     5     :     P     X  ,   u  3   �     �     �               !  '   .     V     u     {     �     �     �     �     �     �  &   �       B        b  �  y       4   *  +   _  M   �     �  6   �  %   &	     L	     d	  /   {	     �	  U   �	  "   !
  E   D
  v   �
  �     V   �  U   �     3     A     R  T   r  A   �  
   	  4     @   I     �  R   �     �  Z   
     e  j   u     �  �   �     u                                       
                              !                                  $      #                               	                           "             @info:creditAuthor @info:creditCopyright 2006 Sebastian Sauer @info:creditSebastian Sauer @info:shell command-line argumentThe script to run. @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: application descriptionCommand-line utility to run Kross scripts. application nameKross Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2014-10-29 10:58+0530
Last-Translator: Chetan Khona <chetan@kompkin.com>
Language-Team: Marathi <kde-i18n-doc@kde.org>
Language: mr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n!=1);
X-Generator: Lokalize 1.5
 लेखक (C) 2006 सेबेस्तियन सॉअर सेबेस्तियन सॉअर चालविण्याकरिताची स्क्रिप्ट. सामान्य नवीन स्क्रिप्ट जोडा. समाविष्ट करा... रद्द करा? टीप्पणी: sandeep.shedmake@gmail.com, 
chetan@kompkin.com संपादित करा निवडलेली स्क्रिप्ट संपादित करा. संपादित करा... निवडलेली स्क्रिप्ट चालवा. इंटरप्रीटर "%1" करिता स्क्रिप्ट बनवू शकला नाही स्क्रिप्टफाईल "%1" करिता इंटरप्रेटर ओळखण्यास अपयशी इंटरप्रीटर "%1" दाखल होण्यास अपयशी स्क्रिप्टफाईल "%1" उघडण्यास अपयशी फाईल: चिन्ह: इंटरप्रेटर: रूबी इंटरप्रेटरचे सुरक्षा स्तर संदिप शेडमाके, 
चेतन खोना नाव: "%1" नुरूप फंक्शन नाही इंटरप्रीटर "%1" आढळला नाही काढून टाका निवडलेली स्क्रिप्ट काढून टाका. चालवा स्क्रिप्ट फाईल "%1" अस्तित्वात नाही. थांबा निवडलेल्या स्क्रिप्ट अकार्यान्वित करा. पाठ्य: क्रोस स्क्रिप्ट चालविण्याकरिता केडीई अनुप्रयोग. क्रोस 