��    #      4  /   L           	       
   �  +   �  
   �     �     �     �           %     7     W  	   `      j  �   �  e   )  *   �  H   �          
     )  )   6  ;   `  	   �     �  (   �     �               ;     [     p     �  	   �  �  �     c	  G  i	     �
  n   �
     8     T  A   r  %   �  W   �  "   2  R   U     �     �     �  R  �  )  E  s   o  �   �     �     �  "   �  q     �   �     3  N   O  �   �  6   %  3   \  9   �  9   �  1     6   6  6   m  	   �        !                	                                                                                   
                #                    "               msec <h1>Multiple Desktops</h1>In this module, you can configure how many virtual desktops you want and how these should be labeled. Animation: Assigned global Shortcut "%1" to Desktop %2 Desktop %1 Desktop %1: Desktop Effect Animation Desktop Names Desktop Switch On-Screen Display Desktop Switching Desktop navigation wraps around Desktops Duration: EMAIL OF TRANSLATORSYour emails Enable this option if you want keyboard or active desktop border navigation beyond the edge of a desktop to take you to the opposite edge of the new desktop. Enabling this option will show a small preview of the desktop layout indicating the selected desktop. Here you can enter the name for desktop %1 Here you can set how many virtual desktops you want on your KDE desktop. Layout NAME OF TRANSLATORSYour names No Animation No suitable Shortcut for Desktop %1 found Shortcut conflict: Could not set Shortcut %1 for Desktop %2 Shortcuts Show desktop layout indicators Show shortcuts for all possible desktops Switch One Desktop Down Switch One Desktop Up Switch One Desktop to the Left Switch One Desktop to the Right Switch to Desktop %1 Switch to Next Desktop Switch to Previous Desktop Switching Project-Id-Version: kcm_kwindesktop
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-06 05:35+0100
PO-Revision-Date: 2013-02-23 17:39+0530
Last-Translator: Chetan Khona <chetan@kompkin.com>
Language-Team: American English <kde-i18n-doc@kde.org>
Language: en_US
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=(n!=1);
  msec <h1>अनेक डेस्कटॉप</h1>या विभागात, आवश्यक आभासी डेस्कटॉप संयोजीत केले जाऊ शकतात व त्यांस कसे लेबल करायचे ते निश्चित केले जाऊ शकते. ऍनीमेशन : %2 या डेस्कटॉप करिता "%1" हा शॉर्टकट लागू केला डेस्कटॉप %1 डेस्कटॉप %1 : डेस्कटॉप परिणाम ऍनीमेशन डेस्कटॉप नावे डेस्कटॉप बदलाचे स्क्रीनवर दृश्य डेस्कटॉप बदल डेस्कटॉप संचारण आसपास आवरण करा डेस्कटॉप्स कालावधी : chetan@kompkin.com जर संचारण करताना एका डेस्कटॉपच्या किनाऱ्यावरुन नवीन डेस्कटॉपच्या विरुद्ध किनाऱ्यावर जायचे असेल तर हा पर्याय कार्यान्वित करा. हा पर्याय कार्यान्वित केल्यास डेस्कटॉप रचनेचे लहान पूर्वावलोकन दिसेल ज्यावर वर्तमान डेस्कटॉप साठी संकेत असेल. येथे डेस्कटॉप %1 करिता नाव दाखल केले जाऊ शकते येथे तुम्ही तुमच्या केडीई डेस्कटॉप करिता किती आभासी डेस्कटॉप पाहिजेत ते निश्चित करू शकता. रचना चेतन खोना ऍनीमेशन नाही %1 डेस्कटॉप करिता योग्य शॉर्टकट सापडला नाही शॉर्टकट मतभेद : %2 या डेस्कटॉप करिता "%1" हा शॉर्टकट लागू केला नाही शॉर्टकट्स डेस्कटॉप रचनेचे संकेत दर्शवा सर्व शक्य असलेल्या डेस्कटॉप्सचे शॉर्टकट्स दर्शवा खालील डेस्कटॉप वर जा वरील डेस्कटॉप वर जा डाव्या डेस्कटॉप वर जा उजव्या डेस्कटॉप वर जा %1 या डेस्कटॉप वर जा  पुढील डेस्कटॉप वर जा मागील डेस्कटॉप वर जा बदल 