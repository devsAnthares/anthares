��    �     L  	  |      �   -   �   &   �   %   �   )   !  (   F!  %   o!  &   �!  &   �!  )   �!  ,   "  %   :"  )   `"     �"     �"  9   �"     �"     �"     �"  	   �"  C   �"  <   <#  :   y#     �#  E   �#     $     $     $  ?   $     U$  :   [$  =   �$     �$     �$  5   �$     %     "%  D   =%  <   �%  @   �%  <    &  :   =&  :   x&  :   �&  B   �&  8   1'  9   j'  H   �'  9   �'  :   '(  F   b(  @   �(  @   �(  B   +)  <   n)  8   �)  9   �)  J   *  )   i*  6   �*  (   �*  .   �*  0   "+  .   S+  *   �+  -   �+  0   �+  *   ,  +   7,  4   c,  ,   �,  ,   �,  )   �,  )   -  '   F-  (   n-  +   �-  .   �-  *   �-  6   .  8   T.  .   �.  .   �.  ,   �.  >   /  .   W/  )   �/  *   �/  /   �/  ,   0  ,   80  .   e0  4   �0  .   �0  ,   �0  *   %1  '   P1  *   x1  '   �1  )   �1  ,   �1  ,   "2  (   O2  *   x2  (   �2  '   �2  '   �2  '   3  +   D3  &   p3  &   �3  .   �3  &   �3  '   4  -   <4  *   j4  *   �4  +   �4  (   �4  &   5  &   <5  /   c5     �5  %   �5     �5  !   �5  "   6  !   <6     ^6      ~6  "   �6     �6      �6  $   7      (7      I7     j7     �7     �7     �7     �7  !   8     '8  %   G8  &   m8  !   �8  !   �8      �8  )   �8  !   #9     E9     d9  !   �9      �9      �9  !   �9  $   
:  !   /:      Q:     r:     �:     �:     �:     �:      ;     -;     A;     U;     l;     �;     �;     �;     �;     �;     �;     <     <     1<     G<     ]<     t<     �<     �<     �<     �<     �<     �<     =     #=     9=     O=     e=     {=     �=     �=     �=     �=     >     $>     ?>     Z>     m>     �>     �>     �>     �>  !   �>     ?     5?      P?     q?     �?     �?     �?     �?     @  "   @     @@     U@     j@     ~@     �@     �@     �@     �@     �@     �@     A     ,A     EA     _A     yA     �A     �A     �A     �A     �A     B     B     1B     GB     ]B     sB     �B     �B     �B     �B     �B     �B     C     #C     9C     OC     eC     }C     �C     �C     �C     �C     �C     �C     D     #D     7D     JD     ^D     rD     �D     �D     �D     �D     �D     �D     �D     E     (E     =E     RE     gE     |E     �E     �E     �E     �E     �E     �E     F     #F     8F     MF     bF     wF     �F     �F     �F     �F     �F     	G     "G     ;G     TG     mG     �G     �G     �G     �G     �G     H     H     6H     OH     hH     �H     �H     �H     �H     �H     �H     I     3I     MI     iI     �I     �I     �I     �I     �I     �I     J     !J     4J     GJ     \J     pJ     �J     �J  %   �J     �J  !   �J     K      0K  $   QK     vK  %   �K  &   �K  !   �K  !   L  !   'L     IL     iL     �L     �L     �L     �L     �L     M     M     6M     OM     eM     }M     �M     �M     �M     �M     �M     	N     N     7N     MN     eN     }N     �N     �N     �N     �N     �N     O      O     8O     PO     eO     }O     �O     �O     �O     �O     �O     P      P  �  9P     �Q     �Q     �Q     �Q     �Q     �Q     R     R     R     R     R     #R     )R  	   ER     OR  	   \R     fR     sR  $   �R     �R  	   �R  	   �R     �R  "   �R  	   S     S  	   "S     ,S  	   9S  	   CS     MS     ZS  	   mS     wS     �S     �S  c   �S  ]   T  Q   dT  ]   �T  ?   U  E   TU  Q   �U  Q   �U  E   >V  K   �V  o   �V  K   @W  Q   �W  W   �W  c   6X  W   �X  Q   �X  W   DY  K   �Y  K   �Y  }   4Z     �Z  C   �Z     [     0[  1   J[  =   |[     �[  +   �[  1   \     8\     R\  =   l\  +   �\  %   �\     �\  %   ]     B]     b]  %   |]  +   �]     �]  7   �]  =    ^  &   ^^  +   �^  %   �^  c   �^  1   ;_     m_     �_  7   �_     �_  %   `  %   +`  +   Q`  %   }`  %   �`  %   �`     �`  "   	a     ,a     Fa  +   fa  1   �a  .   �a  (   �a  .   b     Kb  "   kb  (   �b  (   �b  "   �b  %   c  7   )c  %   ac  (   �c  +   �c  1   �c  +   d  (   :d  +   cd  %   �d  %   �d  >   �d     e  !   'e     Ie     Ye     fe     e     �e     �e     �e     �e     �e  !   �e     f     2f     Ef     Uf     hf     xf     �f     �f     �f     �f     �f     �f     g     "g  1   5g     gg     �g     �g     �g     �g     �g     �g     �g     h     h     1h     Dh     Qh     ah     nh     ~h     �h     �h     �h     �h     �h     �h     �h     �h     �h     �h     �h     �h     �h     �h     �h     �h     �h     �h     �h     �h     �h     �h     �h     �h     i     i     i     i  .   i  +   Gi     si  %   �i  +   �i     �i     j     "j  %   /j  %   Uj  %   {j     �j  "   �j  4   �j  "   k  %   <k  (   bk  .   �k  (   �k  %   �k  (   	l  "   2l  "   Ul  ;   xl     �l     �l     �l     �l     �l     �l     �l     �l     �l     �l     �l     �l     �l     �l     �l     �l     �l     �l     �l     �l     �l     �l     �l     �l      m     m     m     
m     m     m     m     m     m     m     m     !m     $m     )m     ,m     0m     3m     6m     8m     ;m     >m     Am     Dm     Fm     Im     Lm     Om     Rm     Um     Xm     Zm     _m     bm     em     im     lm     om     rm     um     xm     zm     }m     �m     �m     �m     �m     �m     �m     �m     �m     �m     �m     �m     �m     �m     �m     �m     �m     �m     �m     �m     �m     �m     �m     �m     �m     �m     �m     �m     �m     �m     �m     �m     �m     �m     �m     n     n     n     
n     n     n     n     n     n     !n     $n     'n     )n     +n     -n     =n     @n     Bn     Dn     Fn     en  	   rn  	   |n  	   �n     �n  	   �n     �n     �n     �n     o     o     *o     :o     Go     Jo     Lo     Qo     Vo     [o     ^o     bo     fo     io     no     so     xo     {o     �o     �o     �o     �o     �o     �o     �o     �o     �o     �o     �o     �o     �o     �o     �o     �o     �o     �o     �o     �o     �o     �o     �o     �o     �o     �o     8      �   	            :  �              V        �          �   u   �           �       �     "  /  V   S    �   G   �       &   �   `  %       �               X   B  <       5   s  (       3     |         Y   �         �          �   8  	          �   �       K   L   �   �   O   $   �   0      q   �  S      2   _           i  2      !    A  �       �                 �       
  �               x      _  =       �   q  E         P   9   /   �   p  '         {   �   �   L              Q           >      ;  �   t   R  >       U  K    X      �             �   �   +   Q  a   �   �   �   �       I   �   �   #   
   �   e  w   �   b              R     m     �   p   �       k     #  [   N   �      <        6  �   �       @  �   �   �   Z     H   �   '   d  y  `       n   6   \  F   }  �   �   .       �       l      7               k   T  %  �       �   9  �             !   $      �   ?        �   �      v      5  g     y           c   �       0   a     �   g      h  �       o   "   �           f  �   j  �      �         r      �   �   j   �          P  �     �               �           ,    �   �   �    C      i   F  *   G  m  �   B   �   �   A       �   +  M   D  -   s   �   �           �           4   1  Y  ;   |   �        �       3  �         )           4      �       �       @   W   l   (                  w  ~   W      �   �  ]   e   x  c          �   -  �       J      O  N     f   ^            �   *      b   �                        d   t             �  �   Z      ~  �   �   v   �       u  �   �       �         �   ]  C         T   z   �   [  �   U   �   �   ?   �       �   �   �             }           \       �  �         =  �       7  o  h   .  �   {  ,   �   �       �               M  z  �       E          �       �   �   n  H      :   D       J   �         �   �   �   r   &  �   �   �   ^   1   �   �   �       �   I  )    �        �   �   �         %1 value, %2 unit symbol (acceleration)%1 %2 %1 value, %2 unit symbol (angle)%1 %2 %1 value, %2 unit symbol (area)%1 %2 %1 value, %2 unit symbol (currency)%1 %2 %1 value, %2 unit symbol (density)%1 %2 %1 value, %2 unit symbol (force%1 %2 %1 value, %2 unit symbol (length%1 %2 %1 value, %2 unit symbol (power)%1 %2 %1 value, %2 unit symbol (pressure)%1 %2 %1 value, %2 unit symbol (temperature)%1 %2 %1 value, %2 unit symbol (time)%1 %2 %1 value, %2 unit symbol (velocity)%1 %2 Acceleration Angle Chinese Yuan - unit synonyms for matching user inputyuan Currency Density Force Frequency HUF hungarian Forint - unit synonyms for matching user inputforint JPY Japanese Yen - unit synonyms for matching user inputyen KRW Korean Won - unit synonyms for matching user inputwon Length MTL Maltese Lira - unit synonyms for matching user inputMaltese lira Mass Power Pressure South African Rand - unit synonyms for matching user inputrand Speed THB Thai Baht - unit synonyms for matching user inputbaht TRY Turkish Lira - unit synonyms for matching user inputlira Temperature Time Unit Category: two dimensional size of a surfaceArea Volume acceleration unit symbolg amount in units (integer)%1 Australian dollar %1 Australian dollars amount in units (integer)%1 Belgian franc %1 Belgian francs amount in units (integer)%1 Canadian dollar %1 Canadian dollars amount in units (integer)%1 Cypriot pound %1 Cypriot pounds amount in units (integer)%1 Czech koruna %1 Czech korunas amount in units (integer)%1 Danish krone %1 Danish kroner amount in units (integer)%1 French franc %1 French francs amount in units (integer)%1 Hong Kong dollar %1 Hong Kong dollars amount in units (integer)%1 Irish pound %1 Irish pounds amount in units (integer)%1 Italian lira %1 Italian lira amount in units (integer)%1 Luxembourgish franc %1 Luxembourgish francs amount in units (integer)%1 Maltese lira %1 Maltese lira amount in units (integer)%1 Mexican peso %1 Mexican pesos amount in units (integer)%1 New Zealand dollar %1 New Zealand dollars amount in units (integer)%1 Norwegian krone %1 Norwegian kroner amount in units (integer)%1 Philippine peso %1 Philippine pesos amount in units (integer)%1 Singapore dollar %1 Singapore dollars amount in units (integer)%1 Slovak koruna %1 Slovak korunas amount in units (integer)%1 Swiss franc %1 Swiss francs amount in units (integer)%1 Turkish lira %1 Turkish lira amount in units (integer)%1 United States dollar %1 United States dollars amount in units (integer)%1 baht %1 baht amount in units (integer)%1 centimeter %1 centimeters amount in units (integer)%1 day %1 days amount in units (integer)%1 degree %1 degrees amount in units (integer)%1 drachma %1 drachmas amount in units (integer)%1 escudo %1 escudos amount in units (integer)%1 euro %1 euros amount in units (integer)%1 forint %1 forint amount in units (integer)%1 guilder %1 guilders amount in units (integer)%1 hour %1 hours amount in units (integer)%1 inch %1 inches amount in units (integer)%1 kilometer %1 kilometers amount in units (integer)%1 krona %1 kronor amount in units (integer)%1 kroon %1 kroons amount in units (integer)%1 kuna %1 kune amount in units (integer)%1 lats %1 lati amount in units (integer)%1 leu %1 lei amount in units (integer)%1 lev %1 leva amount in units (integer)%1 litas %1 litai amount in units (integer)%1 markka %1 markkas amount in units (integer)%1 mile %1 miles amount in units (integer)%1 millimeter %1 millimeters amount in units (integer)%1 millisecond %1 milliseconds amount in units (integer)%1 minute %1 minutes amount in units (integer)%1 peseta %1 pesetas amount in units (integer)%1 pound %1 pounds amount in units (integer)%1 pound sterling %1 pounds sterling amount in units (integer)%1 radian %1 radians amount in units (integer)%1 rand %1 rand amount in units (integer)%1 real %1 reais amount in units (integer)%1 ringgit %1 ringgit amount in units (integer)%1 ruble %1 rubles amount in units (integer)%1 rupee %1 rupees amount in units (integer)%1 rupiah %1 rupiahs amount in units (integer)%1 schilling %1 schillings amount in units (integer)%1 second %1 seconds amount in units (integer)%1 tolar %1 tolars amount in units (integer)%1 week %1 weeks amount in units (integer)%1 won %1 won amount in units (integer)%1 year %1 years amount in units (integer)%1 yen %1 yen amount in units (integer)%1 yuan %1 yuan amount in units (integer)%1 zloty %1 zlotys amount in units (real)%1 Australian dollars amount in units (real)%1 Belgian francs amount in units (real)%1 Canadian dollars amount in units (real)%1 Cypriot pounds amount in units (real)%1 Czech korunas amount in units (real)%1 Danish kroner amount in units (real)%1 French francs amount in units (real)%1 Hong Kong dollars amount in units (real)%1 Irish pounds amount in units (real)%1 Italian lira amount in units (real)%1 Luxembourgish francs amount in units (real)%1 Maltese lira amount in units (real)%1 Mexican pesos amount in units (real)%1 New Zealand dollars amount in units (real)%1 Norwegian kroner amount in units (real)%1 Philippine pesos amount in units (real)%1 Singapore dollars amount in units (real)%1 Slovak korunas amount in units (real)%1 Swiss francs amount in units (real)%1 Turkish lira amount in units (real)%1 United States dollars amount in units (real)%1 baht amount in units (real)%1 centimeters amount in units (real)%1 days amount in units (real)%1 degrees amount in units (real)%1 drachmas amount in units (real)%1 escudos amount in units (real)%1 euros amount in units (real)%1 forint amount in units (real)%1 guilders amount in units (real)%1 hours amount in units (real)%1 inches amount in units (real)%1 kilometers amount in units (real)%1 kronor amount in units (real)%1 kroons amount in units (real)%1 kune amount in units (real)%1 lati amount in units (real)%1 lei amount in units (real)%1 leva amount in units (real)%1 litas amount in units (real)%1 markkas amount in units (real)%1 miles amount in units (real)%1 millimeters amount in units (real)%1 milliseconds amount in units (real)%1 minutes amount in units (real)%1 pesetas amount in units (real)%1 pounds amount in units (real)%1 pounds sterling amount in units (real)%1 radians amount in units (real)%1 rand amount in units (real)%1 reais amount in units (real)%1 ringgit amount in units (real)%1 rubles amount in units (real)%1 rupees amount in units (real)%1 rupiahs amount in units (real)%1 schillings amount in units (real)%1 seconds amount in units (real)%1 tolars amount in units (real)%1 weeks amount in units (real)%1 won amount in units (real)%1 year amount in units (real)%1 yen amount in units (real)%1 yuan amount in units (real)%1 zlotys angle unit symbol" angle unit symbol' angle unit symbolgrad angle unit symbolrad angle unit symbol° area unit symbolEm² area unit symbolGm² area unit symbolMm² area unit symbolPm² area unit symbolTm² area unit symbolYm² area unit symbolZm² area unit symbolam² area unit symbolcm² area unit symboldam² area unit symboldm² area unit symbolfm² area unit symbolft² area unit symbolhm² area unit symbolin² area unit symbolkm² area unit symbolmm² area unit symbolm² area unit symbolnm² area unit symbolpm² area unit symbolym² area unit symbolzm² area unit symbolµm² currency nameAustralian Dollar currency nameBelgian Franc currency nameBritish Pound currency nameCanadian Dollar currency nameCypriot Pound currency nameCzech Koruna currency nameDanish Krone currency nameEuro currency nameFrench Franc currency nameGreek Drachma currency nameHong Kong Dollar currency nameIrish Pound currency nameItalian Lira currency nameLuxembourgish Franc currency nameMaltese Lira currency nameMexican Peso currency nameNew Zealand Dollar currency nameNorwegian Krone currency namePhilippine Peso currency nameSingapore Dollar currency nameSlovak Koruna currency nameSwiss Franc currency nameTurkish Lira currency nameUnited States Dollar energy unit symbolJ force unit symbolEN force unit symbolN force unit symbolYN force unit symbolZN force unit symbolcN force unit symboldN force unit symboldaN force unit symbolhN force unit symbolkN frequency unit symbolGHz frequency unit symbolHz frequency unit symbolMHz frequency unit symbolcHz frequency unit symboldHz frequency unit symbolmHz length unit symbolEm length unit symbolGm length unit symbolMm length unit symbolPm length unit symbolTm length unit symbolYm length unit symbolZm length unit symbolam length unit symbolcm length unit symboldam length unit symboldm length unit symbolft length unit symbolhm length unit symbolin length unit symbolkm length unit symbolm length unit symbolmi length unit symbolmm length unit symbolnm length unit symbolpm length unit symbolthou length unit symbolyd length unit symbolµm length unit symbolÅ mass unit symbolCD mass unit symbolN mass unit symbolag mass unit symbolcg mass unit symboldg mass unit symbolfg mass unit symbolg mass unit symbolkN mass unit symbollb mass unit symbolmg mass unit symbolng mass unit symboloz mass unit symbolpg mass unit symbolt mass unit symbolt oz mass unit symbolyg mass unit symbolzg mass unit symbolµg power unit symbolEW power unit symbolGW power unit symbolMW power unit symbolPW power unit symbolTW power unit symbolW power unit symbolYW power unit symbolZW power unit symbolaW power unit symbolfW power unit symbolhp power unit symbolmW power unit symbolnW power unit symbolpW power unit symbolyW power unit symbolzW power unit symbolµW pressure unit symbolEPa pressure unit symbolGPa pressure unit symbolMPa pressure unit symbolPPa pressure unit symbolPa pressure unit symbolTPa pressure unit symbolYPa pressure unit symbolZPa pressure unit symbolaPa pressure unit symbolat pressure unit symbolatm pressure unit symbolcPa pressure unit symboldPa pressure unit symboldaPa pressure unit symbolfPa pressure unit symbolhPa pressure unit symbolinHg pressure unit symbolkPa pressure unit symbolmPa pressure unit symbolmmHg pressure unit symbolnPa pressure unit symbolpPa pressure unit symbolpsi pressure unit symbolyPa pressure unit symbolzPa pressure unit symbolµPa temperature unit symbolK temperature unit symbolR temperature unit symbol°C temperature unit symbol°De temperature unit symbol°F temperature unit symbol°N time unit symbolEs time unit symbolMs time unit symbolYs time unit symbolZs time unit symbola time unit symbold time unit symbolh time unit symbolmin time unit symbolms time unit symbols time unit symbolw time unit symboly unit description in listscentimeters unit description in listsdays unit description in listsdegrees unit description in listshours unit description in listsinches unit description in listskilometers unit description in listsmiles unit description in listsmillimeters unit description in listsmilliseconds unit description in listsminutes unit description in listsradians unit description in listsseconds unit description in listsweeks unit description in listsyear velocity unit symbolMa velocity unit symbolc velocity unit symbolft/s velocity unit symbolin/s velocity unit symbolkm/h velocity unit symbolkt velocity unit symbolm/s velocity unit symbolmph volume unit symbolEl volume unit symbolEm³ volume unit symbolGm³ volume unit symbolMm³ volume unit symbolPl volume unit symbolPm³ volume unit symbolTl volume unit symbolTm³ volume unit symbolYl volume unit symbolYm³ volume unit symbolZl volume unit symbolZm³ volume unit symbolam³ volume unit symbolcl volume unit symbolcm³ volume unit symboldam³ volume unit symboldl volume unit symboldm³ volume unit symbolfl volume unit symbolfm³ volume unit symbolhm³ volume unit symbolkm³ volume unit symboll volume unit symbolmm³ volume unit symbolm³ volume unit symbolnm³ volume unit symbolpl volume unit symbolpm³ volume unit symbolpt volume unit symbolym³ volume unit symbolzm³ volume unit symbolµm³ Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2014-10-29 10:45+0530
Last-Translator: Chetan Khona <chetan@kompkin.com>
Language-Team: American English <kde-i18n-doc@kde.org>
Language: en_US
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n!=1);
X-Generator: Lokalize 1.5
 %1 %2 %1 %2 %1 %2 %1 %2 %1 %2 %1 %2 %1 %2 %1 %2 %1 %2 %1 %2 %1 %2 %1 %2 गतिवृद्धि कोन यूआन चलन घनता बळपूर्वक फ्रीक्वेन्सी फोरिंट येन वोन लांबी माल्टीज लिरा मास पॉवर दाब रेंड वेग बाथ लिरा तापमान वेळ क्षेत्र परिमाण g %1 ऑस्ट्रेलियन डॉलर %1 ऑस्ट्रेलियन डॉलर %1 बेल्जियन फ्रांक %1 बेल्जियन फ्रांक %1 केनेडियन डॉलर %1 केनेडियन डॉलर %1 सायप्रिओट पाउंड %1 सायप्रिओट पाउंड %1 झेक कोरुना %1 झेक कोरुना %1 डेनिश क्रोन %1 डेनिश क्रोन %1 फ्रेंच फ्रांक %1 फ्रेंच फ्रांक %1 हांगकांग डॉलर %1 हांगकांग डॉलर %1 आयरीश पाउंड %1 आयरीश पाउंड %1 इटालीयन लिरा %1 इटालीयन लिरा %1 लक्समबर्गीश फ्रांक %1 लक्समबर्गीश फ्रांक %1 माल्टीज लिरा %1 माल्टीज लिरा %1 मेक्सिकन पेसो %1 मेक्सिकन पेसो %1 न्यूझीलँड डॉलर %1 न्यूझीलँड डॉलर %1 नोर्वेजियन क्रोन %1 नोर्वेजियन क्रोन %1 फिलिपाइनी पेसो %1 फिलिपाइनी पेसो %1 सिंगापुर डॉलर %1 सिंगापुर डॉलर %1 स्लोवाक कोरुना %1 स्लोवाक कोरुना %1 स्विस फ्रांक %1 स्विस फ्रांक %1 तुर्किश लिरा %1 तुर्किश लिरा %1 अमेरिका संघराज्य डॉलर %1 अमेरिका संघराज्य डॉलर %1 बाथ %1 बाथ %1 सेन्टीमीटर %1 सेन्टीमीटर %1 दिवस %1 दिवस %1 अंश %1 अंश %1 ड्राचमा %1 ड्राचमा %1 एस्क्युडो %1 एस्क्युडो %1 युरो %1 युरो %1 फोरिंट %1 फोरिंट %1 गिल्डर %1 गिल्डर्स %1 तास %1 तास %1 इंच %1 इंच %1 किलोमीटर %1 किलोमीटर्स %1 क्रोना %1 क्रोना %1 क्रून %1 क्रून %1 कुना %1 कुना %1 लाट्स %1 लाट्स %1 लियु %1 लियु %1 लेव %1 लेव %1 लिटास %1 लिटास %1 मार्का %1 मार्का %1 मैल %1 मैल %1 मिलीमिटर %1 मिलीमिटर %1 मिलीसेकंद %1 मिलीसेकंद  मिनिट %1 मिनिटे %1 पेसेटा %1 पेसेटा %1 पाउंड %1 पाउंड %1 पाउण्ड स्टर्लिंग %1 पाउण्ड स्टर्लिंग %1 रेडियन %1 रेडियन्स %1 रेंड %1 रेंड %1 रीआल %1 रीआल %1 रिंग्गिट %1 रिंग्गिट %1 रुबल %1 रुबल %1 रुपया %1 रुपये %1 रुपया %1 रुपया %1 शिलिंग %1 शिलिंग %1 सेकंद %1 सेकंद %1 टोलार %1 टोलार %1 आठवडा %1 आठवडे %1 वोन %1 वोन %1 वर्ष %1 वर्षे %1 येन %1 येन %1 यूआन %1 यूआन %1 ज्लोटी %1 ज्लोटी %1 ऑस्ट्रेलियन डॉलर %1 बेल्जियन फ्रांक %1 केनेडियन डॉलर %1 सायप्रिओट पाउंड %1 झेक कोरुना %1 डेनिश क्रोन %1 फ्रेंच फ्रांक %1 हांगकांग डॉलर %1 आयरीश पाउंड %1 इटालीयन लिरा %1 लक्समबर्गीश फ्रांक %1 माल्टीज लिरा %1 मेक्सिकन पेसो %1 न्यूझीलँड डॉलर %1 नोर्वेजियन क्रोन %1 फिलिपाइनी पेसो %1 सिंगापुर डॉलर %1 स्लोवाक कोरुना %1 स्विस फ्रांक %1 तुर्किश लिरा %1 अमेरिका संघराज्य डॉलर %1 बाथ %1 सेन्टीमीटर %1 दिवस %1 अंश %1 ड्राचमा %1 एस्क्युडो %1 युरो %1 फोरिंट %1 गिल्डर्स %1 तास %1 इंच %1 किलोमीटर्स %1 क्रोना %1 क्रून %1 कुना %1 लाट्स %1 लियु %1 लेव %1 लिटास %1 मार्का %1 मैल %1 मिलीमिटर %1 मिलीसेकंद %1 मिनिटे %1 पेसेटा %1 पाउंड %1 पाउण्ड स्टर्लिंग %1 रेडियन्स %1 रेंड %1 रीआल %1 रिंग्गिट %1 रुबल %1 रुपये %1 रुपया %1 शिलिंग %1 सेकंद %1 टोलार %1 आठवडे %1 वोन %1 वर्ष %1 येन %1 यूआन %1 ज्लोटी " ' grad rad ° Em² Gm² Mm² Pm² Tm² Ym² Zm² am² cm² dam² dm² fm² ft² hm² in² km² mm² m² nm² pm² ym² zm² µm² ऑस्ट्रेलियन डॉलर बेल्जियन फ्रांक आयरीश पाउंड केनेडियन डॉलर सायप्रिओट पाउंड झेक कोरुना डेनिश क्रोन युरो फ्रेंच फ्रांक ग्रीक ड्राचमा हांगकांग डॉलर आयरीश पाउंड इटालीयन लिरा लक्समबर्गीश फ्रांक माल्टीज लिरा मेक्सिकन पेसो न्यूझीलँड डॉलर नोर्वेजियन क्रोन फिलिपाइनी पेसो सिंगापुर डॉलर स्लोवाक कोरुना स्विस फ्रांक तुर्किश लिरा अमेरिका संघराज्य डॉलर J EN N YN ZN cN dN daN hN kN GHz Hz MHz cHz dHz mHz Em Gm Mm Pm Tm Ym Zm am cm dam dm ft hm in km m mi mm nm pm thou yd µm Å CD N ag cg dg fg g kN lb mg ng oz pg t t oz yg zg µg EW GW MW PW TW W YW ZW aW fW hp mW nW pW yW zW µW EPa GPa MPa PPa Pa TPa YPa ZPa aPa at atm cPa dPa daPa fPa hPa inHg kPa mPa mmHg nPa pPa psi yPa zPa µPa K R °C °De °F °N Es Ms Ys Zs a d h मिनिट ms s w y सेन्टीमीटर दिवस अंश तास इंच किलोमीटर्स मैल मिलीमिटर मिलीसेकंद मिनिटे रेडियन्स सेकंद आठवडे वर्ष Ma c ft/s in/s km/h kt m/s mph El Em³ Gm³ Mm³ Pl Pm³ Tl Tm³ Yl Ym³ Zl Zm³ am³ cl cm³ dam³ dl dm³ fl fm³ hm³ km³ l mm³ m³ nm³ pl pm³ pt ym³ zm³ µm³ 