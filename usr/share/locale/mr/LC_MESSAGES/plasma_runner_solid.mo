��          �      L      �  m   �     /  "   <  F   _  F   �  F   �  J   4  N     R   �     !  %   2  $   X  #   }  $   �  %   �  &   �  �        �  �  �  8   V  /   �  j   �  �   *  �   �  �   o	  �   �	  �   
  �   R          9     F     f          �     �  5   �     �                                
                          	                                          Close the encrypted container; partitions inside will disappear as they had been unpluggedLock the container Eject medium Finds devices whose name match :q: Lists all devices and allows them to be mounted, unmounted or ejected. Lists all devices which can be ejected, and allows them to be ejected. Lists all devices which can be mounted, and allows them to be mounted. Lists all devices which can be unmounted, and allows them to be unmounted. Lists all encrypted devices which can be locked, and allows them to be locked. Lists all encrypted devices which can be unlocked, and allows them to be unlocked. Mount the device Note this is a KRunner keyworddevice Note this is a KRunner keywordeject Note this is a KRunner keywordlock Note this is a KRunner keywordmount Note this is a KRunner keywordunlock Note this is a KRunner keywordunmount Unlock the encrypted container; will ask for a password; partitions inside will appear as they had been plugged inUnlock the container Unmount the device Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2013-01-17 15:06+0530
Last-Translator: Chetan Khona <chetan@kompkin.com>
Language-Team: American English <kde-i18n-doc@kde.org>
Language: en_US
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n!=1);
X-Generator: Lokalize 1.5
 कंटेनरच कुलूपबंद करा माध्यम बाहेर काढा :q: ला नाव किंवा वर्णन जुळणारी साधने शोधतो सर्व साधनांची यादी करतो व ते जोडतो, काढतो किंवा बाहेर काढतो. सर्व बाहेर काढता येणाऱ्या साधनांची यादी करतो व ते बाहेर काढतो. सर्व जोडता येणाऱ्या साधनांची यादी करतो व ते जोडतो. सर्व काढता येणाऱ्या साधनांची यादी करतो व ते काढतो. सर्व कुलूप लावता येणाऱ्या कुटलिपी साधनांची यादी करतो व त्यांना कुलूपबंद करतो. सर्व कुलूप काढता येणाऱ्या कुटलिपी साधनांची यादी करतो व त्यांचे कुलूप काढतो. साधन जोडा साधन बाहेर काढ़ा कुलूपबंद जोडा कुलूप काढा काढा कंटेनरचे कुलूप काढा साधन काढा 