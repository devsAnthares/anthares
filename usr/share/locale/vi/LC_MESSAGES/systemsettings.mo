��    )      d  ;   �      �  A   �     �  /   �     "     +     ?     U     d     k  	   x     �  3   �  	   �     �      �  $   �  *   #     N  	   S  5   ]     �     �  
   �     �     �       5     6   G     ~  ,   �  /   �     �     �  O     Z   ^  b   �  	     
   &  P   1     �  �  �  M   6
     �
     �
     �
     �
  )   �
       
   '     2     I  $   U     z     �     �     �  0   �                 <   .  5   k     �     �     �     �     �  i     J   m     �  H   �       "        @  h   [  V   �  z        �  
   �  �   �     5            (             %      #      $         &                                                        	       )                     !                             
                   '                 "    %1 is an external application and has been automatically launched (c) 2009, Ben Cooksley <i>Contains 1 item</i> <i>Contains %1 items</i> About %1 About Active Module About System Settings Apply Settings Author Ben Cooksley Configure Configure your system Determines whether detailed tooltips should be used Developer Dialog EMAIL OF TRANSLATORSYour emails Expand the first level automatically General config for System SettingsGeneral Help Icon View Internal module representation, internal module model Internal name for the view used Keyboard Shortcut: %1 Maintainer Mathias Soeken NAME OF TRANSLATORSYour names No views found Provides a categorized icons view of control modules. Provides a classic tree-based view of control modules. Relaunch %1 Reset all current changes to previous values Search through a list of control modulesSearch Show detailed tooltips System Settings System Settings was unable to find any views, and hence has nothing to display. System Settings was unable to find any views, and hence nothing is available to configure. The settings of the current module have changed.
Do you want to apply the changes or discard them? Tree View View Style Welcome to "System Settings", a central place to configure your computer system. Will Stephenson Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:08+0100
PO-Revision-Date: 2012-07-17 10:54+0800
Last-Translator: Lê Hoàng Phương <herophuong93@gmail.com>
Language-Team: Vietnamese <kde-i18n-vi@kde.org>
Language: vi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 1.5
 %1 là một ứng dụng ngoài và đã được khởi chạy tự động (c), 2009 Didier Hoarau <i>Chứa %1 mục</i> Giới thiệu %1 Giới thiệu mô-đun Active Giới thiệu Thiết lập hệ thống Áp dụng thiết lập Tác giả c), 2009 Didier Hoarau Cấu hình Cấu hình hệ thống của bạn Cho phép xem mẹo công cụ Nhà phát triển Hộp thoại herophuong93@gmail.com Tự động mở rộng cấp cây đầu tiên Tổng quan Trợ giúp Xem biểu tượng Biểu diễn mô-đun bên trong, mẫu mô-đun bên trong Tên hệ thống cho kiểu xem được sử dụng Phím tắt: %1 Người duy trì Mathias Soeken Lê Hoàng Phương Không có mục cấu hình Cho phép xem các mô-đun điều khiển theo dạng biểu tượng được nhóm theo thể loại. Cho phép xem các mô-đun điều khiển theo dạng cây cổ điển. Chạy lại %1 Thiết lập lại tất cả thay đổi về giá trị trước đó Tìm kiếm Hiện mẹo công cụ chi tiết Thiết lập hệ thống Thiết lập hệ thống không thể tìm thấy bất kỳ mục cấu hình nào để hiển thị Thiết lập hệ thống không thể tìm thấy bất kỳ mục cấu hình nào. Thiết lập của mô-đun hiện tại đã thay đổi.
Bạn có muốn áp dụng thay đổi hay huỷ bỏ chúng? Xem dạng cây Kiểu xem Chào mừng bạn đến với "Thiết lập hệ thống", một trung tâm cấu hình cho hệ thống máy tính của bạn. Will Stephenson 