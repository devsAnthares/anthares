��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  �  K     �  +   �    $  �  @	  p   �
     k     �     �  "   �     �     �  +         ,                                          
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2006-04-15 22:20+0930
Last-Translator: Phan Vĩnh Thịnh <teppi82@gmail.com>
Language-Team: Vietnamese <kde-l10n-vi@kde.org>
Language: vi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: KBabel 1.10
  giây Thời &gian chờ dấu hiệu khi chạy: <H1>Thông báo thanh tác vụ</H1> 
Bạn có thể bật dùng phương pháp thứ hai của thông báo khi chạy
được dùng bởi thanh tác vụ khi một nút có đồng hồ quay xuất hiện
cho biết chương trình mà bạn đã khởi động đang được tải.
Có thể xảy ra trường hợp là một số chương trình không biết thông
báo khi chạy này. Trong trường hợp như vậy, nút sẽ biến mất sau
khoảng thời gian ghi trong phần 'Chỉ định thời gian chờ khi chạy' <h1>Con trỏ bận</h1> 
KDE dùng hình con trỏ bận để thông báo chương trình đang chạy.
Để dùng con trỏ bận, chọn một dạng con trỏ từ hộp.
Có thể xảy ra trường hợp là một số chương trình không biết
thông báo khi chạy này. Trong trường hợp như vậy, con trỏ sẽ
dừng nhấp nháy sau khoảng thời gian ghi trong phần 'Chỉ định thời gian chờ khi chạy' <h1>Phản hồi về chạy</h1> Bạn có thể cấu hình thông báo về chạy chương trình ở đây. Con trỏ nhấp nháy Con trỏ nảy lên &Con trỏ bận &Dùng thông báo thanh tác vụ Con trỏ không bận Con trỏ bận thụ động Thời gian chờ dấ&u hiệu khi chạy: Thông &báo thanh tác vụ 