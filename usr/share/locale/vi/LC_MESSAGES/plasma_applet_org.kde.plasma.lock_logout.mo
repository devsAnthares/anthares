��          �            x  &   y  +   �  	   �     �     �     �            (        ?     B  ,   Y     �     �     �  �  �  %   )  %   O     u  0   �     �  	   �     �     �  =   �       '      N   H     �     �     �                                
                                    	           Do you want to suspend to RAM (sleep)? Do you want to suspend to disk (hibernate)? Hibernate Hibernate (suspend to disk) Leave Leave... Lock Lock the screen Logout, turn off or restart the computer No Sleep (suspend to RAM) Start a parallel session as a different user Suspend Switch user Yes Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2013-02-04 09:23+0800
Last-Translator: Lê Hoàng Phương
Language-Team: Vietnamese <kde-i18n-vi@kde.org>
Language: vi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=1; plural=0;
 Bạn có muốn tạm ngưng không? Bạn có muốn ngủ đông không? Ngủ đông Ngủ đông (lưu thông tin lên đĩa cứng) Thoát Thoát... Khoá Khoá màn hình Đăng xuất, tắt máy hay khởi động lại máy tính Không Tạm ngưng (lưu thông tin vào RAM) Chạy một phiên làm việc song song dưới quyền người dùng khác Tạm ngưng Đổi người dùng Có 