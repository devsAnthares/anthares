��          �   %   �      0  &   1  �   X     �     �     �     �                #  	   D  �   N  c        {     �     �     �     �     �     �  	   �  C   �  j   4     �  �  �  A   x  �   �     }     �     �     �     �     �     �     �  �   �  �   �	     i
     �
     �
  	   �
     �
     �
     �
     �
  F   �
  �   1      �                                                                            	               
                                    (c) 2002 Karol Szwed, Daniel Molkentin <h1>Style</h1>This module allows you to modify the visual appearance of user interface elements, such as the widget style and effects. Button Checkbox Combobox Con&figure... Configure %1 Description: %1 EMAIL OF TRANSLATORSYour emails Group Box Here you can choose from a list of predefined widget styles (e.g. the way buttons are drawn) which may or may not be combined with a theme (additional information like a marble texture or a gradient). If you enable this option, KDE Applications will show small icons alongside some important buttons. KDE Style Module NAME OF TRANSLATORSYour names No description available. Preview Radio button Tab 1 Tab 2 Text Only There was an error loading the configuration dialog for this style. This area shows a preview of the currently selected style without having to apply it to the whole desktop. Unable to Load Dialog Project-Id-Version: kcmstyle
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-17 05:44+0100
PO-Revision-Date: 2007-06-23 21:08+0930
Last-Translator: Clytie Siddall <clytie@riverland.net.au>
Language-Team: Vietnamese <gnomevi-list@lists.sourceforge.net>
Language: vi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: LocFactoryEditor 1.6.4a2
 Bản quyền © năm 2002 của Karol Szwed và Daniel Molkentin <h1>Kiểu dáng</h1>Mô-đun này cho bạn khả năng sửa đổi diện mạo của yếu tố của giao diện người dùng, như kiểu dáng và hiệu ứng của ô điều khiển. Nút Hộp kiểm tra Hộp tổ hợp &Cấu hính... Cấu hình %1 Mô tả: %1 kde-l10n-vi@kde.org Hộp nhóm Ở đây bạn có thể chọn trong danh sách các kiểu dáng ô điều khiển xác định sẵn (v.d. cách vẽ cái nút) mà có thể được tổ hợp với sắc thái (thông tin thêm như hoạ tiết cẩm thạch hay dốc). Nếu bạn bật tùy chọn này, ứng dụng KDE sẽ hiển thị biểu tượng nhỏ bên cạnh cái nút quan trọng. Mô-đun kiểu dáng KDE Nhóm Việt hoá KDE Không có mô tả. Xem thử Nút chọn một Thanh 1 Thanh 2 Chỉ có nhãn Gặp lỗi khi tải hộp thoại cấu hình cho kiểu dáng này. Vùng này hiển thị ô xen thử kiểu dáng đã chọn hiện thời, không cần áp dụng nó vào toàn màn hình nền. Không thể tải hộp thoại 