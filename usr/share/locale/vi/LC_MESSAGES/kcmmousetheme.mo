��          �      �       H  `   I     �     �     �     �      �          2     7     H  ?   U  Y   �  +   �  �    �   �     b  )   n     �  2   �     �     �     �            P   0  i   �  1   �     
                           	                                 A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. Project-Id-Version: kcminput
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2007-06-23 19:58+0930
Last-Translator: Clytie Siddall <clytie@riverland.net.au>
Language-Team: Vietnamese <kde-l10n-vi@kde.org>
Language: vi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: LocFactoryEditor 1.6.4a2
 Sắc thái tên %1 đã có trong thư mục sắc thái biểu tượng của bạn. Bạn có muốn thay thế nó bằng điều này không? Xác nhận Thiết lập con chạy đã thay đổi Mô tả Kéo hay gõ địa chỉ Mạng của sắc thái kde-l10n-vi@kde.org Nhóm Việt hoá KDE Tên Ghi đè sắc thái ? Gỡ bỏ sắc thái Hình như tập tin %1 không phải là kho sắc thái con chạy hợp lệ. Không thể tải về kho sắc thái con chạy; hãy kiểm tra xem địa chỉ %1 là đúng chưa. Không tìm thấy kho sắc thái con chạy %1. 