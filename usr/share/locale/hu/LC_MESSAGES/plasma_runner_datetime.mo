��    	      d      �       �      �      �   -        <  -   V  #   �  #   �     �  �  �     �  "   �  =   �     �  6        G     N     S                                   	           Current time is %1 Displays the current date Displays the current date in a given timezone Displays the current time Displays the current time in a given timezone Note this is a KRunner keyworddate Note this is a KRunner keywordtime Today's date is %1 Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2015-03-23 16:05+0100
Last-Translator: Kristóf Kiszel <ulysses@kubuntu.org>
Language-Team: Hungarian <kde-l10n-hu@kde.org>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=(n != 1);
 A pontos idő %1 Megjeleníti az aktuális dátumot Megjeleníti az aktuális dátumot egy megadott időzónában Megjeleníti a pontos időt Megjeleníti a pontos időt egy megadott időzónában dátum idő A mai dátum: %1 