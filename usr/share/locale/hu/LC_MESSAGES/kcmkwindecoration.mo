��            )   �      �  !   �  "   �  '   �  ,     #   ;  &   _  !   �  &   �  '   �     �                 V   $  1   {     �  *   �     �        
     
   "     -     6     ;     D     T     [     a     g  �  p          (     -     B     \     d     t     z     �     �     �     �  	   �  a   �  N   *     y  .   �  $   �     �     �     	     	     $	     *	     :	     N	     W	     f	     l	                                                                          
                                                    	           @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Borders @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large Application menu Border si&ze: Buttons Close Close by double clicking:
 To open the menu, keep the button pressed until it appears. Close windows by double clicking &the menu button Context help Drag buttons between here and the titlebar Drop here to remove button Get New Decorations... Keep above Keep below Maximize Menu Minimize On all desktops Search Shade Theme Titlebar Project-Id-Version: KDE 4.2
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-04-14 02:49+0200
PO-Revision-Date: 2017-02-08 16:06+0100
Last-Translator: Kiszel Kristóf <kiszel.kristof@gmail.com>
Language-Team: Hungarian <kde-l10n-hu@kde.org>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 Hatalmas Nagy Nincsenek szegélyek Nincsenek oldalszegélyek Normál Túlméretezett Kicsi Óriási Nagyon nagy Alkalmazásmenü S&zegélyméret: Nyomógombok Bezárás Bezárás dupla kattintással:
 A menü megnyitásához tartsa nyomva a gombot, amíg megjelenik. Az ablakok bezárha&tók legyenek a menügombra történő dupla kattintással Helyi súgó Húzza a gombokat a itt és a címsor között Ejtse ide a gomb eltávolításához Új dekorációk letöltése… Mindig felül Mindig alul Maximalizálás Menü Minimalizálás Az összes asztalra Keresés Felgördítés Téma Címsor 