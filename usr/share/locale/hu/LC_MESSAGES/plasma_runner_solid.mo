��          �      L      �  m   �     /  "   <  F   _  F   �  F   �  J   4  N     R   �     !  %   2  $   X  #   }  $   �  %   �  &   �  �        �  �  �     Q     h  6   z  `   �  N     T   a  V   �  _   	  _   m	     �	     �	     �	  	   �	     �	  	   
     
     #
     ;
                                
                          	                                          Close the encrypted container; partitions inside will disappear as they had been unpluggedLock the container Eject medium Finds devices whose name match :q: Lists all devices and allows them to be mounted, unmounted or ejected. Lists all devices which can be ejected, and allows them to be ejected. Lists all devices which can be mounted, and allows them to be mounted. Lists all devices which can be unmounted, and allows them to be unmounted. Lists all encrypted devices which can be locked, and allows them to be locked. Lists all encrypted devices which can be unlocked, and allows them to be unlocked. Mount the device Note this is a KRunner keyworddevice Note this is a KRunner keywordeject Note this is a KRunner keywordlock Note this is a KRunner keywordmount Note this is a KRunner keywordunlock Note this is a KRunner keywordunmount Unlock the encrypted container; will ask for a password; partitions inside will appear as they had been plugged inUnlock the container Unmount the device Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2011-08-08 14:05+0200
Last-Translator: Kristóf Kiszel <ulysses@kubuntu.org>
Language-Team: Hungarian <kde-i18n-hu@kde.org>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=2; plural=(n != 1);
 A konténer zárolása Médium kidobása A(z) :q: kifejezésre illeszkedő eszközök keresése Az összes eszköz kilistázása és engedélyezése csatolásra, leválasztása és kidobásra. Az összes kidobható eszköz kilistázása, és kidobásra engedélyezésük. Az összes felcsatolható eszköz kilistázása, és felcsatolásra engedélyezése. Az összes leválasztható eszköz kilistázása, és leválasztásra engedélyezése. Kilistázza az összes zárolható titkosított eszközt, és lehetővé teszi a zárolásukat. Kilistázza az összes feloldható titkosított eszközt, és lehetővé teszi a feloldásukat. Az eszköz csatolása eszköz kidobás zárolás felcsatolás feloldás leválasztás A konténer felnyitása Az eszköz leválasztása 