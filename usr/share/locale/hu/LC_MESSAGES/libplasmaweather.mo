��          <      \       p      q   *   �   /   �   �  �   6   �  <   �  S   �                   Cannot find '%1' using %2. Connection to %1 weather server timed out. Weather information retrieval for %1 timed out. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-13 03:17+0100
PO-Revision-Date: 2010-12-06 14:35+0100
Last-Translator: Kristóf Kiszel <ulysses@kubuntu.org>
Language-Team: Magyar <kde-i18n-doc@kde.org>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 1.0
 A(z) „%1” nem található a(z) %2 használatával. Időtúllépés történt a(z) %1 időjárás-kiszolgálón. Időtúllépés történt a(z) %1 időjárási információinak lekérése közben. 