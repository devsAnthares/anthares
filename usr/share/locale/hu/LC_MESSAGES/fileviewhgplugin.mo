��    9      �  O   �      �     �     �          )     ?     U     l     �  #   �  '   �      �               #     /     =     W     k  #   �  #   �     �     �          #     8  #   V     z     �     �  "   �  "   �          #  2   <  2   o  1   �  2   �  /   	  2   7	  S   j	     �	     �	     �	     �	     �	     

     
  (   %
     N
     U
     ^
     t
  
   |
  
   �
     �
     �
  �  �
     I  
   g  	   r     |     �     �     �     �     �  !   �     �  
             +     0     8     L     X  %   o  $   �     �     �     �               '     0     C     H      g     �     �     �  -   �  *   �  )   '  )   Q  $   {  )   �  a   �     ,     0     C     \     z     �     �  '   �     �  	   �     �          !     8     I     P     +   .           %   &              #                  8                  /         2   	   0   3   4       5   !                     '   -                   ,         "      $       1   9            *                     (           6                    )      
                  7    ## Starting Server ## @action:buttonClone @action:buttonClose @action:buttonCommit @action:buttonImport @action:buttonOptions @action:buttonRename @action:buttonUpdate @action:inmenuClose current branch @action:inmenuCommit to current branch @action:inmenuCreate new branch @buttonBrowse @labelOptions @labelPort @labelSource @label:buttonAdd Patches @label:buttonMerge @label:buttonRemove Patches @label:buttonShow Incoming Changes @label:buttonShow Outgoing Changes @label:buttonStart Server @label:buttonStop Server @label:groupIncoming Changes @label:groupOptions @label:groupOutgoing Changes @label:label to source fileSource  @label:renameRename to  @lobelDestination @messageNo changes found! @message:infoNo incoming changes! @message:infoNo outgoing changes! @title:groupCommit Message @title:groupFile Status @title:window<application>Hg</application> Commit @title:window<application>Hg</application> Import @title:window<application>Hg</application> Merge @title:window<application>Hg</application> Rename @title:window<application>Hg</application> Tag @title:window<application>Hg</application> Update A KDE text-editor component could not be found;
please check your KDE installation. Branch Copy Message Create New Tag Created tag successfully! Current Parent Dialog height Dialog width Do not update the new working directory. Error! Filename New working directory Options Remove Tag Switch Tag Tag URLs Project-Id-Version: fileviewhgplugin
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-01 02:48+0200
PO-Revision-Date: 2012-07-24 13:38+0200
Last-Translator: Balázs Úr <urbalazs@gmail.com>
Language-Team: Hungarian <kde-l10n-hu@kde.org>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 1.4
 ## Kiszolgáló indítása ## Klónozás Bezárás Véglegesítés Importálás Beállítások Átnevezés Frissítés Jelenlegi ág lezárása Véglegesítés a jelenlegi ágba Új ág létrehozása Tallózás Beállítások Port Forrás Foltok hozzáadása Egyesítés Foltok eltávolítása Bejövő változások megjelenítése Kimenő változások megjelenítése Kiszolgáló indítása Kiszolgáló leállítása Bejövő változások Beállítások Kimenő változások Forrás  Átnevezése erre  Cél Nem találhatók változások! Nincsenek bejövő változások! Nincsenek kimenő változások! Véglegesítés üzenet Fájlállapot <application>Hg</application> véglegesítés <application>Hg</application> importálás <application>Hg</application> egyesítés <application>Hg</application> átnevezés <application>Hg</application> címke <application>Hg</application> frissítés Nem található KDE-s szövegszerkesztő komponens;
Ellenőrizze, rendben telepítve van-e a KDE. Ág Üzenet másolása Új címke létrehozása Címke létrehozása sikeres! Jelenlegi szülő Párbeszédablak magassága Párbeszédablak szélessége Ne frissítse az új munkakönyvtárat. Hiba! Fájlnév Új munkakönyvtár Beállítások Címke eltávolítása Címke váltása Címke URL-ek 