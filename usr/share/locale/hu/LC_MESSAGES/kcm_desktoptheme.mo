��          �      �       H     I     a      m     �     �     �  
   �     �  &   �          .     L  1   b  �  �     C     _     k          �     �     �     �  )   �  $        3  '   P  =   x         	                                    
                    Configure Desktop Theme David Rosca EMAIL OF TRANSLATORSYour emails Get New Themes... Install from File... NAME OF TRANSLATORSYour names Open Theme Remove Theme Theme Files (*.zip *.tar.gz *.tar.bz2) Theme installation failed. Theme installed successfully. Theme removal failed. This module lets you configure the desktop theme. Project-Id-Version: KDE 4.2
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-22 05:21+0100
PO-Revision-Date: 2017-12-10 08:55-0500
Last-Translator: Kiszel Kristóf <kiszel.kristof@gmail.com>
Language-Team: Hungarian <kde-l10n-hu@kde.org>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 Asztali téma beállítása David Rosca ulysses@kubuntu.org Új témák letöltése… Telepítés fájlból… Kiszel Kristóf Téma megnyitása Eltávolítás Témafájlok (*.zip, *.tar.gz, *.tar.bz2) Nem sikerült telepíteni a témát. A téma sikeresen települt. Nem sikerült eltávolítani a témát. Ez a modul lehetővé teszi az asztali téma beállítását. 