��          t      �                 '     6     R     Z     w  Q   �     �      �       �       �     �  .   �          !  (   ;     d     j     y     |     
      	                                                 &Require trigger word &Trigger word: Checks the spelling of :q:. Correct Could not find a dictionary. Spell Check Settings Spelling checking runner syntax, first word is trigger word, e.g.  "spell".%1:q: Suggested words: %1 separator for a list of words,  spell Project-Id-Version: KDE 4.3
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2017-02-06 21:57+0100
Last-Translator: Kiszel Kristóf <kiszel.kristof@gmail.com>
Language-Team: Hungarian <kde-l10n-hu@kde.org>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 K&ulcsszó szükséges &Kulcsszó: Ellenőrzi a következő helyesírását: :q:. Helyes Nem található szótár. Helyesírás-ellenőrzés beállításai %1:q: Javaslatok: %1 ,  helyesírás 