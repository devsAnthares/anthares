��    $      <  5   \      0     1  +   E     q  4   �  &   �     �     �                     5     :     P     X  ,   u  3   �     �     �               !  '   .     V     u     {     �     �     �     �     �     �  &   �       B        b  �  y     #     +     E     U     q     }     �     �     �  &   �     �  &   �     	  '   %	  A   M	  I   �	  2   �	  5   
     B
     I
     O
  &   \
     �
     �
      �
  !   �
     �
  $   �
  	      #   *     N  &   [     �  3   �     �                                       
                              !                                  $      #                               	                           "             @info:creditAuthor @info:creditCopyright 2006 Sebastian Sauer @info:creditSebastian Sauer @info:shell command-line argumentThe script to run. @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: application descriptionCommand-line utility to run Kross scripts. application nameKross Project-Id-Version: KDE 4.4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2014-04-22 14:52+0200
Last-Translator: Kristóf Kiszel <ulysses@kubuntu.org>
Language-Team: Hungarian <kde-l10n-hu@kde.org>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 1.5
 Szerző © Sebastian Sauer, 2006. Sebastian Sauer A futtatandó parancsfájl. Általános Új szkript felvétele. Hozzáadás… Mégsem? Megjegyzés: ulysses@kubuntu.org,taszanto@gmail.com Szerkesztés A kiválasztott szkript szerkesztése. Szerkesztés… A kiválasztott szkript végrehajtása. Nem sikerült szkriptet létrehozni a(z) „%1” értelmezőhöz Nem sikerült meghatározni a szkripthez tartozó értelmezőt: „%1”. Nem sikerült betölteni az értelmezőt: „%1” Nem sikerült megnyitni ezt a szkriptfájlt: „%1” Fájl: Ikon: Értelmező: A Ruby-értelmező biztonsági szintje Kiszel Kristóf,Szántó Tamás Név: Nincs ilyen függvény: „%1” Nincs ilyen értelmező: „%1” Eltávolítás A kijelölt szkript eltávolítása. Futtatás Nem létezik ez a szkript: „%1” Leállítás A kiválasztott szkript leállítása. Szöveg: Kross szkripteket futtató parancssori alkalmazás. Kross 