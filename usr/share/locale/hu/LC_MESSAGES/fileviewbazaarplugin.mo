��    !      $  /   ,      �  .   �  1     9   J     �  -   �  &   �  )   �  .   #  &   R  )   y  .   �  &   �  )   �  2   #  5   V  =   �  #   �     �  !     '   /  "   W  0   z  '   �  *   �     �          7     R     j     �     �  &   �  �  �  (   �	  ,   �	  9   �	      
  5   :
  %   p
  '   �
  +   �
     �
       ,   "     O     j  6   �  0   �  >   �  +   0     \     v  '   �     �  -   �          $     D     Z     t     �     �     �     �  )   �                                                              	                                
                                    !                             @info:statusAdded files to Bazaar repository. @info:statusAdding files to Bazaar repository... @info:statusAdding of files to Bazaar repository failed. @info:statusBazaar Log closed. @info:statusCommit of Bazaar changes failed. @info:statusCommitted Bazaar changes. @info:statusCommitting Bazaar changes... @info:statusPull of Bazaar repository failed. @info:statusPulled Bazaar repository. @info:statusPulling Bazaar repository... @info:statusPush of Bazaar repository failed. @info:statusPushed Bazaar repository. @info:statusPushing Bazaar repository... @info:statusRemoved files from Bazaar repository. @info:statusRemoving files from Bazaar repository... @info:statusRemoving of files from Bazaar repository failed. @info:statusReview Changes failed. @info:statusReviewed Changes. @info:statusReviewing Changes... @info:statusRunning Bazaar Log failed. @info:statusRunning Bazaar Log... @info:statusUpdate of Bazaar repository failed. @info:statusUpdated Bazaar repository. @info:statusUpdating Bazaar repository... @item:inmenuBazaar Add... @item:inmenuBazaar Commit... @item:inmenuBazaar Delete @item:inmenuBazaar Log @item:inmenuBazaar Pull @item:inmenuBazaar Push @item:inmenuBazaar Update @item:inmenuShow Local Bazaar Changes Project-Id-Version: fileviewbazaarplugin
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2012-07-23 09:34+0200
Last-Translator: Balázs Úr <urbalazs@gmail.com>
Language-Team: Hungarian <kde-l10n-hu@kde.org>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 1.4
 Fájlok hozzáadva a Bazaar adattárhoz. Fájlok hozzáadása a Bazaar adattárhoz… A fájok hozzáadása a Bazaar adattárhoz nem sikerült. A Bazaar napló lezárva. A Bazaar változások véglegesítése nem sikerült. A Bazaar változások véglegesítve. Bazaar változások véglegesítése… A Bazaar adattár lekérése nem sikerült. Bazaar adattár lekérve. Bazaar adattár lekérése… A Bazaar adattár elküldése nem sikerült. Bazaar adattár elküldve. Bazaar adattár elküldése… A fájlok el lettek távolítva a Bazaar adattárból. Fájlok eltávolítása a Bazaar adattárból… A fájlok eltávolítása a Bazaar adattárból nem sikerült. A változások áttekintése nem sikerült. Változások áttekintve. Változások áttekintése… A Bazaar napló futtatása meghiúsult. Bazaar napló futtatása… A Bazaar adattár frissítése nem sikerült. A Bazaar adattár frissítve. Bazaar adattár frissítése… Bazaar hozzáadás… Bazaar véglegesítés… Bazaar törlés Bazaar napló Bazaar lekérés Bazaar elküldés Bazaar frissítés Helyi Bazaar változások megjelenítése 