��          �      �       H     I     i  #   �      �     �     �     �  �        �  ]   �       p   *  :   �  �  �     ~  ,   �  $   �     �                &  �   6     �  u     ,   {  �   �  M   0                     
          	                                  Configure Look and Feel details Cursor Settings Changed Download New Look And Feel Packages EMAIL OF TRANSLATORSYour emails Get New Looks... Marco Martin NAME OF TRANSLATORSYour names Select an overall theme for your workspace (including plasma theme, color scheme, mouse cursor, window and desktop switcher, splash screen, lock screen etc.) Show Preview This module lets you configure the look of the whole workspace with some ready to go presets. Use Desktop Layout from theme Warning: your Plasma Desktop layout will be lost and reset to the default layout provided by the selected theme. You have to restart KDE for cursor changes to take effect. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-27 05:46+0100
PO-Revision-Date: 2017-02-03 15:38+0100
Last-Translator: Kiszel Kristóf <kiszel.kristof@gmail.com>
Language-Team: Hungarian <kde-l10n-hu@kde.org>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 A megjelenés beállítása Az egérmutató beállításai megváltoztak Új megjelenési témák letöltése ulysses@kubuntu.org Új téma letöltése… Marco Martin Kiszel Kristóf Válasszon ki egy teljes témát a környezetének (beleértve a Plasma témát, színsémát, egérkurzort, ablak- és asztalváltót, töltőképernyőt, záróképernyőt, stb) Előnézet megjelenítése Ez  a modul lehetővé teszi a teljes környezet megjelenésének beállítását néhány kész összeállítással. A téma asztali elrendezésének használata Figyelmeztetés: A Plasma asztal elrendezése el fog veszni és visszaáll az alapértelmezett értékekre a kiválasztott téma miatt. A KDE-t újra kell indítani, hogy a kurzormódosítások érvényesüljenek. 