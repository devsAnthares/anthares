��            )   �      �     �  
   �  /   �  -   �          !  
   2     =     J     `  W   i     �  ,   �  	                  !     '     -  
   :     E     W     o     �     �     �     �  1   �       �       �  
   �     �     �     �          &     2     @     Z     i     �  A     	   J     T     b     o  	   u          �     �     �     �     �  *   	     3	     L	  #   g	     �	                                                                                  
                                	                       %1@%2 %2@%3 (%1) @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Favorites All Applications Appearance Applications Applications updated. Computer Drag tabs between the boxes to show/hide them, or reorder the visible tabs by dragging. Edit Applications... Expand search to bookmarks, files and emails Favorites Hidden Tabs History Icon: Leave Menu Buttons Often Used On All Activities On The Current Activity Remove from Favorites Show In Favorites Show applications by name Sort alphabetically Switch tabs on hover Type is a verb here, not a nounType to search... Visible Tabs Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2017-12-04 13:56-0500
Last-Translator: Kiszel Kristóf <kiszel.kristof@gmail.com>
Language-Team: Hungarian <kde-l10n-hu@kde.org>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 %1@%2 %2@%3 (%1) Választás… Ikon visszaállítása Hozzáadás a kedvencekhez Minden alkalmazás Megjelenés Alkalmazások Alkalmazások frissítve. Számítógép Húzza a lapokat a dobozok között a megjelenításükhöz/elrejtésükhöz, vagy rendezze át a látható lapokat húzással. Alkalmazások szerkesztése… Keresés kiterjesztése könyvjelzőkre, fájlokra és e-mailekre Kedvencek Rejtett lapok Előzmények Ikon: Kilépés Menügombok Gyakran használt Minden aktivitáson A jelenlegi aktivitáson Eltávolítás a kedvencekből Megjelenítés a Kedvenceknél Alkalmazások megjelenítése név szerint Rendezés ABC sorrendben Lapváltás rámutatáskor Írja be a keresési kifejezést… Látható lapok 