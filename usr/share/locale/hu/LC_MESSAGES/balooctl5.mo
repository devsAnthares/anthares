��    (      \  5   �      p     q  !   �     �     �  ,   �          4      M     n     �     �     �     �     �     �     �          $     <     O     `  
   n  "   y     �     �     �     �          -     F     ^     u     �  	   �     �     �      �     �     �  �       �     �     �     �  .   �     .	     M	     l	     �	  	   �	     �	     �	     �	     �	     	
  '   !
     I
      c
     �
     �
     �
     �
  '   �
  '   �
     "     2  &   I  &   p      �     �     �     �  !        2     A  
   [  %   f     �     �                 
                          	   &      (               !                           "                    %                                 '      #   $                    A value must be provided Baloo File Indexer is not running Baloo File Indexer is running Baloo Index could not be opened Check for any unindexed files and index them Checking for unindexed files Disable the file indexer EMAIL OF TRANSLATORSYour emails Enable the file indexer File: %1 Forget the specified files Idle Index the specified files Indexed %1 / %2 files Indexer state: %1 Indexing Extended Attributes Indexing file content Indexing modified files Indexing new files Initial Indexing Invalid value Maintainer Manipulate the Baloo configuration Modify the Baloo configuration NAME OF TRANSLATORSYour names Possible Commands: Print the status of the Indexer Print the status of the indexer Restart the file indexer Resume the file indexer Start the file indexer Stop the file indexer Suspend the file indexer Suspended The command to execute Unknown Usage: balooctl config <command> Vishesh Handa balooctl Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-28 03:17+0100
PO-Revision-Date: 2017-02-14 23:07+0100
Last-Translator: Kiszel Kristóf <kiszel.kristof@gmail.com>
Language-Team: Hungarian <kde-l10n-hu@kde.org>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 Meg kell adni egy értéket A Baloo nem fut A Baloo fut A Baloo nem nyitható meg Nem indexelt fájlok keresése és indexelése Nem indexelt fájlok keresése A fájlindexelő kikapcsolása ulysses@kubuntu.org A fájlindexelő bekapcsolása Fájl: %1 A megadott fájlok elfelejtése Inaktív A megadott fájlok indexelése %1 / %2 fájl indexelve Indexelő állapota: %1 Kiterjesztett attribútumok indexelése Fájltartalom indexelése Módosított fájlok indexelése Új fájlok indexelése Kezdeti indexelés Érvénytelen érték Karbantartó A Baloo beállításainak módosítása A Baloo beállításainak módosítása Kiszel Kristóf Lehetséges parancsok: Az indexelő állapotának nyomtatása Az indexelő állapotának nyomtatása A fájlindexelő újraindítása A fájlindexelő felkeltése A fájlindexelő indítása A fájlindexelő leállítása A fájlindexelő felfüggesztése Felfüggesztve A végrehajtandó parancs Ismeretlen Használat: balooctl config <parancs> Vishesh Handa balooctl 