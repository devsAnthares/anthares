��    *      l  ;   �      �     �     �  )   �               /     H     ^  #   ~     �  
   �     �     �  %   �  +        E     Z     m  @   z     �     �     �     �               '     ,     9     ?     _     e  .   m     �  F   �  (   �  	   '  	   1  	   ;  '   E  7   m  "   �  �  �     p	     �	  /   �	     �	     �	     �	     �	     �	     
     0
     E
     R
     h
     {
  7   �
  !   �
     �
  	     L        g          �     �     �     �     �     �                6     =  3   J  %   ~  X   �  .   �     ,     @     I     V     X  	   l                   $                                 %                      *                                               	   #   )   "             &           
                        '             (   !    &Do not remember @actionWalk through activities @actionWalk through activities (Reverse) @action:buttonApply @action:buttonCancel @action:buttonChange... @action:buttonCreate @title:windowActivity Settings @title:windowCreate a New Activity @title:windowDelete Activity Activities Activity information Activity switching Are you sure you want to delete '%1'? Blacklist all applications not on this list Clear recent history Create activity... Description: Error loading the QML files. Check your installation.
Missing %1 For a&ll applications Forget a day Forget everything Forget the last hour Forget the last two hours General Icon Keep history Name: O&nly for specific applications Other Privacy Private - do not track usage for this activity Remember opened documents: Remember the current virtual desktop for each activity (needs restart) Shortcut for switching to this activity: Shortcuts Switching Wallpaper for in 'keep history for 5 months'for  unit of time. months to keep the history month  months unlimited number of monthsforever Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2017-02-27 19:56+0100
Last-Translator: Kiszel Kristóf <kiszel.kristof@gmail.com>
Language-Team: Hungarian <kde-l10n-hu@kde.org>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 &Ne jegyezze meg Aktivitások bejárása Aktivitások bejárása (fordított sorrendben) Alkalmazás Mégsem Módosítás… Létrehozás Aktivitásbeállítások Új aktivitás létrehozása Aktivitás törlése Aktivitások Aktivitás jellemzői Aktivitásváltás Biztosan törli ezt: „%1”? A listán nem szereplő alkalmazások feketelistázása Legutóbbi előzmények törlése Aktivitás létrehozása… Leírás: Hiba a QML-fájlok betöltésekor. Ellenőrizze a telepítést.
Hiányzó %1 Minden a&lkalmazásnál Egy nap elfelejtése Minden elfelejtése Utolsó egy óra elfelejtése Utolsó két óra elfelejtése Általános Ikon Előzmények megőrzése Név: Csak bizo&nyos alkalmazásoknál Egyéb Adatvédelem Privát - ne kövesse ezen aktivitás használatát Megnyitott dokumentumok megjegyzése: Jelenlegi virtuális asztal megjegyzése minden aktivitáshoz (újraindítást igényel) Gyorsbillentyű ezen aktivitásra váltáshoz: Billentyűparancsok Váltás Háttérkép    hónapig  hónapig örökké 