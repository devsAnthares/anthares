��    B      ,  Y   <      �     �     �     �     �     �     �                     /     ?     P     e     u     �     �     �     �     �     �     �     �               '     ;     N     a     p     |     �     �     �     �     �     �           -     M     m     �     �  #   �     �     	     !	  "   A	     d	  $   �	     �	     �	     �	     
      
  3   >
     r
     �
  &   �
  <   �
  !     8   *  0   c  !   �      �  +   �  �       �  	   �     �     �     �     �     �  
   �     �                 
   4  
   ?     J     P  	   _     i     n     z     �  
   �  	   �     �     �     �     �     �     �     �     �     �     �          "     .     >      Y     z     �     �     �      �     �          &     @     _  !   w     �     �     �     �     �          !     '     .     7     N     W     n     {     �     �                          /   7   8   :                  "   (                 =   '            ;   0       	   B         !                 ,         )   %   *       #   5          $                  1   4       
   >   @      +      -       6      3   2                <          .                ?   9       &      A            @labelAlbum Artist @labelArchive @labelArtist @labelAspect Ratio @labelAudio @labelAuthor @labelBitrate @labelChannels @labelComment @labelComposer @labelCopyright @labelCreation Date @labelDocument @labelDuration @labelFolder @labelFrame Rate @labelHeight @labelImage @labelKeywords @labelLanguage @labelLyricist @labelPage Count @labelPresentation @labelPublisher @labelRelease Year @labelSample Rate @labelSpreadsheet @labelSubject @labelText @labelTitle @labelVideo @labelWidth @label EXIFImage Date Time @label EXIFImage Make @label EXIFImage Model @label EXIFImage Orientation @label EXIFPhoto Aperture Value @label EXIFPhoto Exposure Bias @label EXIFPhoto Exposure Time @label EXIFPhoto F Number @label EXIFPhoto Flash @label EXIFPhoto Focal Length @label EXIFPhoto Focal Length 35mm @label EXIFPhoto GPS Altitude @label EXIFPhoto GPS Latitude @label EXIFPhoto GPS Longitude @label EXIFPhoto ISO Speed Rating @label EXIFPhoto Metering Mode @label EXIFPhoto Original Date Time @label EXIFPhoto Saturation @label EXIFPhoto Sharpness @label EXIFPhoto White Balance @label EXIFPhoto X Dimension @label EXIFPhoto Y Dimension @label date of template creation8Template Creation @label music albumAlbum @label music genreGenre @label music track numberTrack Number @label number of fuzzy translated stringsDraft Translations @label number of linesLine Count @label number of translatable stringsTranslatable Units @label number of translated stringsTranslations @label number of wordsWord Count @label translation authorAuthor @label translations last updateLast Update Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:36+0200
PO-Revision-Date: 2015-04-15 09:40+0200
Last-Translator: Kristóf Kiszel <ulysses@kubuntu.org>
Language-Team: Hungarian <kde-l10n-hu@kde.org>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 1.5
 Album előadó Archívum Előadó Méretarány Hang Szerző Bitsebesség Csatornák Megjegyzés Zeneszerző Szerzői jog Létrehozás dátuma Dokumentum Időtartam Mappa Képkockaszám Magasság Kép Kulcsszavak Nyelv Lírikus Oldalszám Bemutató Kiadó Kiadás éve Mintavételezési ráta Munkafüzet Tárgy Szöveg Cím Videó Szélesség Kép dátum és idő Kép készítő Kép modell Kép tájolása Fénykép apertúraérték Fénykép expozíció torzítás Fénykép exponálási idő Fénykép F-szám Fénykép vaku Fénykép fókusztávolság Fénykép 35mm fókusztávolság Fénykép GPS magasság Fénykép GPS szélesség Fénykép GPS hosszúság Fénykép ISO-sebesség arány Fénykép mérési mód Fénykép eredeti dátum és idő Fénykép telítettség Fénykép élesség Fénykép fehéregyensúly Fénykép X dimenzió Fénykép Y dimenzió Sablon létrehozása Album Műfaj Sorszám Félkész fordítások Sorszám Fordítható egységek Fordítások Szavak száma Szerző Utolsó frissítés 