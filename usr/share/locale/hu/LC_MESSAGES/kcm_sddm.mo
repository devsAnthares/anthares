��    ,      |  ;   �      �  (   �     �  �   �     �     �     �     �     �     �     �     �     �               %     1      J     k     s     �     �     �     �     �     �     �               /     4     I     Y     l     y     �     �      �  7   �                     1     6  �  <     �     �     �     	  
   #	     .	     6	  	   R	     \	  	   k	  &   u	     �	     �	     �	     �	     �	     �	     

     
     ,
     B
     X
     l
     �
  .   �
     �
     �
     �
     �
            #   5     Y     o     �     �  '   �  E   �               :     Q     _     !                 %   +   ,      "   #                               &                  	                           )                             *      '      $      (                                
    %1 is the name of a session%1 (Wayland) ... @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) @title:windowSelect Image Advanced Author Auto &Login Background: Clear Image Commands Could not decompress archive Cursor Theme: Customize theme Default Description Download New SDDM Themes EMAIL OF TRANSLATORSYour emails General Get New Theme Halt Command: Install From File Install a theme. Invalid theme package Load from file... Login screen using the SDDM Maximum UID: Minimum UID: NAME OF TRANSLATORSYour names Name No preview available Reboot Command: Relogin after quit Remove Theme SDDM KDE Config SDDM theme installer Session: The default cursor theme in SDDM The theme to install, must be an existing archive file. Theme Unable to install theme Uninstall a theme. User User: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2017-12-10 08:55-0500
Last-Translator: Kiszel Kristóf <kiszel.kristof@gmail.com>
Language-Team: Hungarian <kde-l10n-hu@kde.org>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 %1 (Wayland) … (Elérhető méretek: %1) Kép kiválasztása Speciális Szerző Automatikus beje&lentkezés Háttér: Kép törlése Parancsok Nem sikerült kibontani az archívumot Kurzortéma: Téma testre szabása Alapértelmezett Leírás Új SDDM témák letöltése ulysses@kubuntu.org Általános Új téma letöltése Leállítás parancs: Telepítés fájlból Téma telepítése. Érvénytelen témacsomag Betöltés fájlból… Az SDDM-et használó bejelentkező képernyő Maximális UID: Minimális UID: Kiszel Kristóf Név Nem érhető el előnézet Újraindítás parancs: Újrabejelentkezés kilépés után Téma eltávolítása SDDM KDE beállítás SDDM tématelepítő Munkamenet: Az SDDM alapértelmezett kurzortémája A telepítendő témának egy létező archívumfájlnak kell lennie. Téma Nem lehet telepíteni a témát Téma eltávolítása. Felhasználó Felhasználó: 