��          �      �       0     1  #   G      k      �     �  J   �       &   )     P     o  *        �  �  �     [     w     �  &   �     �  O   �      G  (   h     �     �  9   �     �                                  	                 
       Configure File Search Copyright 2007-2010 Sebastian Trüg Do not search in these locations EMAIL OF TRANSLATORSYour emails Enable File Search File Search helps you quickly locate all your files based on their content Folder %1 is already excluded Folder's parent %1 is already excluded NAME OF TRANSLATORSYour names Sebastian Trüg Select the folder which should be excluded Vishesh Handa Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2014-10-18 12:11+0200
Last-Translator: Kristóf Kiszel <ulysses@kubuntu.org>
Language-Team: Hungarian <kde-l10n-hu@kde.org>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 1.5
 Fájlkeresés beállítása © Sebastian Trüg, 2007-2010. Ne keressen ezeken a helyeken ulysses@kubuntu.org,urbalazs@gmail.com Fájlkeresés bekapcsolása A Fájlkeresés segít önnek megtalálni minden fájlját a tartalmuk alapján A(z) %1 mappa már ki van zárva %1, a mappa szülője már ki van zárva Kiszel Kristóf,Úr Balázs Sebastian Trüg Válassza ki azokat a mappákat, amelyeket ki kell zárni Vishesh Handa 