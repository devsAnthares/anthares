��          �   %   �      @  9   A     {     �     �  7   �  	   �     �               $     <     J     R     f     m     �  #   �  %   �     �  5   �     .     =     K  )   S  �  }     '     ;     J  )   X     �     �     �  	   �     �     �     �     �               $     =     L     P      T  G   u     �     �  	   �     �            
                                                              	                                                    %1 is remaining time, %2 is percentage%1 Remaining (%2%) %1% Battery Remaining %1%. Charging &Configure Power Saving... Battery is currently not present in the bayNot present Capacity: Charging Discharging Display Brightness Enable Power Management Fully Charged General Keyboard Brightness Model: No Batteries Available Not Charging Placeholder is battery capacity%1% Placeholder is battery percentage%1% Power management is disabled The battery applet has enabled system-wide inhibition Time To Empty: Time To Full: Vendor: battery percentage below battery icon%1% Project-Id-Version: KDE 4.2
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-03 03:15+0100
PO-Revision-Date: 2014-09-08 09:21+0200
Last-Translator: Kristóf Kiszel <ulysses@kubuntu.org>
Language-Team: Hungarian <kde-l10n-hu@kde.org>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 1.5
 %1 van hátra (%2%) %1% van hátra %1%. Töltés &Energiatakarékossági beállítások… Nincs jelen Kapacitás: Töltés Kisütés Kijelző fényereje Energiakezelés engedélyezése Teljesen feltöltött Általános Billentyűzet fényereje Modell: Nincsenek akkumulátorok Nincs töltés %1% %1% Az energiakezelés le van tiltva Az akkumulátor kisalkalmazás engedélyezte a rendszerszintű tiltást Idő a kisülésig: Idő a feltöltésig: Gyártó: %1% 