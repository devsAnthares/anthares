��          �      ,      �     �     �     �     �     �     �     �     �  .   �     .     L     \     m     �     �  H   �  �  �     �     �     �     �     �               /  -   C  $   q     �     �     �  
   �     �  b           	                                                                  
    &Configure Printers... Active jobs only All jobs Completed jobs only Configure printer General No active jobs No jobs No printers have been configured or discovered One active job %1 active jobs One job %1 jobs Open print queue Print queue is empty Printers Search for a printer... There is one print job in the queue There are %1 print jobs in the queue Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2015-02-21 09:37+0000
PO-Revision-Date: 2015-02-24 15:28+0100
Last-Translator: Kristóf Kiszel <ulysses@kubuntu.org>
Language-Team: Hungarian <kde-l10n-hu@kde.org>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 1.5
 Nyomtatók &beállítása… Csak az aktív feladatok Minden feladat Csak a befejezett feladatok Nyomtató beállítása Általános Nincsenek aktív feladatok Nincsenek feladatok Nincs beállított vagy felfedezett nyomtató Egy aktív feladat %1 aktív feladat Egy feladat %1 feladat Nyomtatási sor megnyitása A nyomtatási sor üres Nyomtatók Nyomtató keresése… Egy nyomtatási feladat van a várakozási sorban %1 nyomtatási feladat van a várakozási sorban 