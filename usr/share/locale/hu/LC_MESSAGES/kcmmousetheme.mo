��          �   %   �      @     A  �   `  m   �  �   P  )   �  `        o     |     �     �     �      �     �     �  '        ,     >     ]     b     s  ?   �  Y   �  +     H   F  �  �      >  �   _  l   �     b	     |	  V   �	     �	  ,   �	     !
     -
  (   6
  (   _
     �
     �
  ,   �
     �
     �
       !        9  .   O  p   ~  6   �  U   &                                                 	                                                               
              (c) 2003-2007 Fredrik Höglund <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) @item:inlistbox sizeResolution dependent A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Cursor Theme Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Fredrik Höglund Get new Theme Get new color schemes from the Internet Install from File NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. You have to restart the Plasma session for these changes to take effect. Project-Id-Version: KDE 4.1
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2017-02-14 22:37+0100
Last-Translator: Kiszel Kristóf <kiszel.kristof@gmail.com>
Language-Team: Hungarian <kde-l10n-hu@kde.org>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 (C) Fredrik Höglund, 2003-2007. <qt>Biztosan el szeretné távolítani ezt az egérmutató-témát: <strong>%1</strong>?<br />A témához tartozó összes fájl törlődni fog.</qt> <qt>Nem lehet törölni az éppen használt témát.<br />Törlés előtt váltson át másik témára.</qt> (Elérhető méretek: %1) Felbontásfüggő Már létezik %1 nevű téma az ikontéma-mappában. Felül szeretné írni az újjal? Megerősítés Az egérmutató beállításai megváltoztak Kurzortéma Leírás Írja be vagy ejtse ide a téma URL-jét tszanto@interware.hu,ulysses@kubuntu.org Fredrik Höglund Új téma letöltése Új színsémák letöltése az internetről Telepítés fájlból Szántó Tamás,Kiszel Kristóf Név Felül szeretné írni a témát? Téma eltávolítása A(z) %1 fájl nem egérmutató-téma csomagja. Nem sikerült letölteni az egérmutató-téma archívumát. Ellenőrizze, hogy valóban ez-e a helyes cím: %1. Nem található a(z) %1 egérmutató-téma archívuma. A Plasma munkamenetet újra kell indítani, hogy a módosítások érvényesüljenek. 