��          �   %   �      P  �  Q          )  6   9  -   p     �     �     �     �  (   �  d        x     �     �     �     �     �     �                7  "   X     {     �     �  �  �  �  l  !   7
     Y
  E   m
  &   �
     �
      �
     
  	     7   #  }   [     �     �               $     3     B     I     X     _     g     p     �  
   �                               	                                                                      
                        <p>You have chosen to open another desktop session.<br />The current session will be hidden and a new login screen will be displayed.<br />An F-key is assigned to each session; F%1 is usually assigned to the first session, F%2 to the second session and so on. You can switch between sessions by pressing Ctrl, Alt and the appropriate F-key at the same time. Additionally, the KDE Panel and Desktop menus have actions for switching between sessions.</p> Lists all sessions Lock the screen Locks the current sessions and starts the screen saver Logs out, exiting the current desktop session New Session Reboots the computer Restart the computer Shutdown the computer Starts a new session as a different user Switches to the active session for the user :q:, or lists all active sessions if :q: is not provided Turns off the computer User sessionssessions Warning - New Session lock screen commandlock log out log out commandLogout log out commandlogout new session restart computer commandreboot restart computer commandrestart shutdown computer commandshutdown switch user switch user commandswitch switch user commandswitch :q: Project-Id-Version: KDE 4.3
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2011-04-15 20:57+0200
Last-Translator: Kristóf Kiszel <ulysses@kubuntu.org>
Language-Team: Hungarian <kde-i18n-doc@kde.org>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 1.2
 <p>Új munkamenet megnyitását kérte.<br />A jelenlegi munkamenet el lesz rejtve, új bejelentkezési ablak jelenik meg.<br />Minden munkamenethez külön funkcióbillentyű tartozik. Általában F%1 az első, F%2 a második munkamenethez és így tovább. Ha más munkamenetre szeretne váltani, nyomja meg egyszerre a Ctrl, az Alt és a megfelelő funkcióbillentyűt. A KDE panelről és az asztali menüből is át lehet váltani más munkamenetre.</p> Az összes munkamenet listázása Képernyőzárolás Zárolja az aktuális munkamenetet és elindítja a képernyővédőt Kijelentkezik az aktív munkamenetből Új munkamenet Újraindítja a számítógépet Újraindítás Lezárás Elindít egy új munkamenetet más felhasználónévvel Átvált a(z) :q: felhasználó aktív munkamenetére, vagy kilistázza az összes aktív munkamenetet, ha a kifejezés üres Kikapcsolja a számítógépet munkamenetek Figyelem - Új munkamenet lock kijelentkezés Kijelentkezés logout új munkamenet reboot restart shutdown váltás más felhasználóra switch switch :q: 