��    I      d  a   �      0     1     9  5   I  #     E   �  E   �  >   /     n  7   �     �     �     �          +     9     K     R     Y     l     �     �     �     �     �  !   �  F   �     .	     K	     \	  <   n	     �	     �	  *   �	     �	     �	     �	  	   
     
     
      &
     G
  
   e
     p
     �
  
   �
  F   �
  :   �
     #     +     2     E     L  8   [     �  	   �  
   �     �     �     �     �               +     1     =  
   Y     d     t     �     �     �  $   �  �  �     �     �  <   �  +   �  H     G   `  @   �     �  ;        ?  "   W  #   z     �     �     �     �     �     �               8     W     k     r  *   z  F   �  $   �          +  C   <     �     �  9   �     �  	   �     �     �       	     2     '   P     x      �     �     �  ]   �  =   &     d     r     �  
   �     �  M   �          .     :     G     Y     h  "   ~     �  $   �  	   �     �     �          "     ;     L     Z     w  #   �        )                                A      @      "   <   	                           %       '      ;           3       4      >   5   =         ,       B       #   7       !   $      H   /   I          E      ?          F       6   D   C   &          *              0   2   +   -   .      
      1   8              G          9      :   (                 %1 (%2) <b>%1</b> by %2 <em>%1 out of %2 people found this review useful</em> <em>Tell us about this review!</em> <em>Useful? <a href='true'><b>Yes</b></a>/<a href='false'>No</a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'><b>No</b></a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'>No</a></em> @infoFetching updates @infoIt is unknown when the last check for updates was @infoNo updates @infoNo updates are available @infoShould check for updates @infoThe system is up to date @infoUpdates @infoUpdating... Accept Addons Aleix Pol Gonzalez An application explorer Apply Changes Available backends:
 Available modes:
 Back Cancel Compact Mode (auto/compact/full). Could not close the application, there are tasks that need to be done. Could not find category '%1' Couldn't open %1 Delete the origin Directly open the specified application by its package name. Discard Discover Display a list of entries with a category. Extensions... Help... Install Installed Jonathan Thomas Launch List all the available backends. List all the available modes. Loading... Local package file to install More... No Updates Open Discover in a said mode. Modes correspond to the toolbar buttons. Open with a program that can deal with the given mimetype. Rating: Remove Resources for '%1' Review Reviewing '%1' Running as <em>root</em> is discouraged and unnecessary. Search in '%1'... Search... Search: %1 Search: %1 + %2 Settings Short summary... Specify the new source for %1 Summary: Supports appstream: url scheme Tasks Tasks (%1%) Unable to find resource: %1 Update All Update Selected Update section nameUpdate (%1) Updates updates not selected updates selected © 2010-2016 Plasma Development Team Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:02+0100
PO-Revision-Date: 2017-02-27 23:48+0100
Last-Translator: Kiszel Kristóf <kiszel.kristof@gmail.com>
Language-Team: Hungarian <kde-l10n-hu@kde.org>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 %1 (%2) <b>%1</b>, szerző: %2 <em>%1 / %2 ember hasznosnak találta ezt a véleményt</em> <em>Meséljen erről a véleményről!</em> <em>Hasznos? <a href='true'><b>Igen</b></a>/<a href='false'>Nem</a></em> <em>Useful? <a href='true'>Igen</a>/<a href='false'><b>Nem</b></a></em> <em>Hasznos? <a href='true'>Igen</a><a href='false'>Nem</a></em> Frissítések letöltése A frissítések utolsó keresésének időpontja ismeretlen Nincsenek frissítések Nincsenek elérhető frissítések Frissítések keresése szükséges A rendszer naprakész Frissítések Frissítés… Jóváhagyás Bővítmények Aleix Pol Gonzalez Egy alkalmazásfelfedező Változtatások alkalmazása Elérhető háttérprogramok:
 Elérhető módok:
 Vissza Mégsem Kompakt mód (automatikus/kompakt/teljes). Nem lehet bezárni az alkalmazás, még vannak befejezendő feladatok. Nem található kategória: „%1” A(z) %1 nem nyitható meg Eredet törlése A megadott alkalmazás közvetlen megnyitása a csomagnév szerint. Eldobás Felfedezés Bejegyzések listájának megjelenítése kategóriával. Bővítmények… Súgó… Telepítés Telepített Jonathan Thomas Indítás Az összes elérhető háttérprogram listázása. Az összes elérhető mód listázása. Betöltés… Helyi csomagfájlok telepítése Több… Nincsenek frissítések A Discover megnyitása biztonságos módban. A módoknak az eszköztár gombjai felelnek meg. Megnyitás a megadott MIME-típust kezelni képes programmal. Értékelés: Eltávolítás Erőforrások ehhez: „%1” Vélemény A(z) „%1” értékelése A <em>root</em> felhasználóként futtatás nem javasolt és szükségtelen. Keresés itt: „%1”… Keresés… Keresés: %1 Keresés: %1 + %2 Beállítások Rövid összegzés… Adja meg az új forrást ehhez: %1 Összegzés: Az appstream: url séma támogatása Feladatok Feladatok (%1%) Nem található erőforrás: %1 Összes frissítése Kijelöltek frissítése Frissítés (%1) Frissítések frissítés nincs kijelölve frissítés kijelölve © A Plasma fejlesztői, 2010-2016. 