��          �   %   �      0  U   1     �     �  
   �     �     �     �     �     �     �  	   �                     .  )   4  (   ^  '   �  "   �     �     �  9   �  J   #  �  n          0     =     O     ]     q     �     �     �     �     �     �     �     �       9     5   T  9   �  .   �     �  
          "   2                                                                            
                           	                        Activities a window is currently on (apart from the current one)Also available on %1 Alphabetically By Activity By Desktop By Program Name Do Not Group Do Not Sort Filters General Grouping and Sorting Grouping: Highlight windows Manually Maximum rows: On %1 Show only tasks from the current activity Show only tasks from the current desktop Show only tasks from the current screen Show only tasks that are minimized Show tooltips Sorting: Which activities a window is currently onAvailable on %1 Which virtual desktop a window is currently onAvailable on all activities Project-Id-Version: KDE 4.2
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2013-10-06 12:01+0200
Last-Translator: Balázs Úr <urbalazs@gmail.com>
Language-Team: Hungarian <kde-l10n-hu@kde.org>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 1.5
 Szintén elérhető ezen: %1 Betűrendben Aktivitásonként Asztalonként Programnév szerint Csoportosítás nélkül Rendezés nélkül Szűrők Általános Csoportosítás és rendezés Csoportosítás: Ablakok kiemelése Kézzel A sorok max. száma: Ezen: %1 Csak feladatok megjelenítése a jelenlegi aktivitásról Csak feladatok megjelenítése a jelenlegi asztalról Csak feladatok megjelenítése a jelenlegi képernyőről Csak a minimalizált feladatok megjelenítése Buboréksúgók megjelenítése Rendezés: Elérhető ezen: %1 Elérhető az összes aktivitáson 