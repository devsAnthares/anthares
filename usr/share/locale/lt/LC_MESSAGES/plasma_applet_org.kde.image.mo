��          �   %   �      0     1     :     O     X  0   f     �     �     �     �     �     �  
   �     �               8     I     [     b     u     �     �     �    �     �  "   �     �     �  1   
     <  !   Z     |     �     �  $   �     �  &   �  	      '   
     2     O     i     |  (   �  	   �     �     �                                                        	                                                            
           %1 by %2 Add Custom Wallpaper Centered Change every: Directory with the wallpaper to show slides from Download Wallpapers Get New Wallpapers... Hours Image Files Minutes Next Wallpaper Image Open Image Open Wallpaper Image Positioning: Recommended wallpaper file Remove wallpaper Restore wallpaper Scaled Scaled and Cropped Scaled, Keep Proportions Seconds Select Background Color Tiled Project-Id-Version: plasma_wallpaper_image
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:39+0100
PO-Revision-Date: 2015-08-25 10:48+0200
Last-Translator: Liudas Ališauskas <liudas@akmc.lt>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 2.0
 %1, sukūrė: %2 Pridėti savitą darbalaukio foną Centre Keisti kas: Katalogas su darbalaukio fonais skaidrių rodymui Atsisiųsti darbalaukio fonų Gauti naujų darbalaukio fonų... Val. Paveikslėlių failai Min. Kitas darbalaukio fono paveikslėlis Atidaryti paveikslėlį Atverti darbalaukio fono paveikslėlį Pozicija: Rekomenduojamas darbalaukio fono failas Pašalinti darbalaukio foną Atkurti darbalaukio foną Keičiamo mastelio Pakeistu masteliu ir apkarpytas Pakeistu masteliu, išlaikyta proporcija Sekundės Pasirinkti fono spalvą Mozaika 