��    #      4  /   L           	               $     1     :     J  3   Z  >   �     �     �     �        m        v     �     �     �     �  *   �      �          #  "   4     W     v     �     �     �     �     �     �  ;   �     4     J     k  	   m  	   w     �     �     �     �  	   �     �     �  
   �     �     �  	   	     	  
   "	     -	  	   D	     N	  4   c	  /   �	     �	     �	     �	     �	     
     !
     *
     7
     G
     \
     d
     k
     p
     #                          
                                    !                 "               	                                                          % Applications Audio Muted Audio Volume Behavior Capture Devices Capture Streams Checkable switch for (un-)muting sound output.Mute Checkable switch to change the current default output.Default Decrease Microphone Volume Decrease Volume Devices General Heading for a list of ports of a device (for example built-in laptop speakers or a plug for headphones)Ports Increase Microphone Volume Increase Volume Maximum volume: Mute Mute Microphone No applications playing or recording audio No output or input devices found Playback Devices Playback Streams Port is unavailable (unavailable) Port is unplugged (unplugged) Raise maximum volume Volume Volume at %1% Volume feedback Volume step: label of device items%1 (%2) label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: trunk-kf 5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2017-06-24 23:56+0200
Last-Translator: Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: Lithuanian
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 2.0
 % Programos Nutildyta Garsumas Elgsena Įrašymo įrenginiai Įrašymo srautai Nutiltyti Numatyta Pritylinti mikrofoną Pritylinti Įrenginiai Bendros Prievadai Pagarsinti mikrofoną Pagarsinti Didžiausias garsumas: Nutiltyti Nutildyti mikrofoną Šiuo metu programos negroja ir neįrašinėja garso Nerasta nei įvedimo, nei išvedimo įrenginių Grojimo įrenginiai Grojimo srautai  (nėra)  (atjungta) Padidinti didžiausią garsumą Garsumas %1% garsumas Garsumo atsakas Garsinimo žingsnis: %1 (%2) %1: %2 100% %1% 