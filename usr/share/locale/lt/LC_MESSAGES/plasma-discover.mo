��    O      �  k         �     �     �     �  5   �  #     E   A  E   �  >   �       7   #     [     l     �     �     �     �     �     �     �     
	     "	     0	     E	     W	     \	  !   t	  F   �	     �	     �	     
  <   
     Z
     b
  *   k
     �
     �
     �
  	   �
     �
     �
     �
      �
     �
  
        (     F  
   N  F   Y  :   �     �     �     �     �       8        L  	   ^  
   h     s     �     �     �     �     �     �     �     �               &  
   B     M     ]     }     �     �     �  $   �    �     �            ;     '   V  H   ~  H   �  A        R  <   g     �     �     �     �                    &     2     E     b     x     �     �     �  +   �  :   �  '   %     M     c  D   u     �     �  &   �     �  
   
               '     7  
   @  !   K  "   m     �     �  
   �     �  q   �  8   J     �  
   �     �  	   �     �  ?   �  !        /     ;     Q  	   o     y     �  
   �  %   �     �  
   �     �  	        #      3     T     f     ~     �     �     �     �  #   �     A             O   :   9          5                 3              ;          6   -   >   E      #   B   +   =   (                   @   H             !   J   4                            %   1                  F       
              /              <   7   D   M   ,   *       $   I      "      .   C         L           G      &      )       	      N       2   '       ?   8      0           K    
Also available in %1 %1 (%2) <b>%1</b> by %2 <em>%1 out of %2 people found this review useful</em> <em>Tell us about this review!</em> <em>Useful? <a href='true'><b>Yes</b></a>/<a href='false'>No</a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'><b>No</b></a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'>No</a></em> @infoFetching updates @infoIt is unknown when the last check for updates was @infoNo updates @infoNo updates are available @infoShould check for updates @infoThe system is up to date @infoUpdates @infoUpdating... Accept Addons Aleix Pol Gonzalez An application explorer Apply Changes Available backends:
 Available modes:
 Back Checking for updates... Compact Mode (auto/compact/full). Could not close the application, there are tasks that need to be done. Could not find category '%1' Couldn't open %1 Delete the origin Directly open the specified application by its package name. Discard Discover Display a list of entries with a category. Extensions... Help... Install Installed Jonathan Thomas Launch License: List all the available backends. List all the available modes. Loading... Local package file to install More... No Updates Open Discover in a said mode. Modes correspond to the toolbar buttons. Open with a program that can deal with the given mimetype. Rating: Remove Resources for '%1' Review Reviewing '%1' Running as <em>root</em> is discouraged and unnecessary. Search in '%1'... Search... Search: %1 Search: %1 + %2 Settings Short summary... Sorry, nothing found... Source: Specify the new source for %1 Still looking... Summary: Supports appstream: url scheme Tasks Tasks (%1%) Unable to find resource: %1 Update All Update Selected Update section nameUpdate (%1) Updates unknown reviewer updates not selected updates selected © 2010-2016 Plasma Development Team Project-Id-Version: l 10n
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:02+0100
PO-Revision-Date: 2017-06-25 01:44+0200
Last-Translator: Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 2.0
 
%1 irgi turi %1 (%2) <b>%1</b> / %2 <em>%1 iš %2 asmenys mano, kad ši apžvalga naudinga</em> <em>Įvertinkite šią apžvalgą!</em> <em>Naudinga? <a href='true'><b>Taip</b></a>/<a href='false'>Ne</a></em> <em>Naudinga? <a href='true'>Taip</a>/<a href='false'><b>Ne</b></a></em> <em>Naudinga? <a href='true'>Taip</a>/<a href='false'>Ne</a></em> Gaunami atnaujinimai Nežinoma, kada buvo paskutinį kartą ieškota atnaujinimų Atnaujinimų nėra Atnaujinimų nėra Bandyti ieškoti atnaujinimų Sistema pilnai atnaujinta Atnaujinimai Atnaujinama... Priimti Papildiniai Aleix Pol Gonzalez Programų paieškos įrankis Pritaikyti pakeitimus Galimos sąsajos:
 Galimos veiksenos:
 Atgal Ieškoma atnaujinimų... Glaustoji veiksena (auto/glaustoji/pilnoji) Nepavyksta užverti programos: liko nebaigtų užduočių. Nepavyksta atverti kategorijos „%1“ Nepavyksta atverti %1 Pašalinti kilmę Tiesiogiai atverti nurodytą programą pagal jos paketo pavadinimą. Atmesti Programų paieška Rodyti kategorijos įrašų sąrašą. Plėtiniai... Pagalba... Įdiegti Įdiegta Jonathan Thomas Paleisti Licencija: Rodyti visas prieinamas sąsajas. Rodyti visas prieinamas veiksenas. Įkeliama... Įdiegtinas vietinis paketas Daugiau... Atnaujinimų nėra „Discover“ programų radim Atverti „said“ veiksenoje. Veiksenos derinasi prie įrankių juostos mygtukų. Atverti su programa, galinčia apdoroti šį MIME tipą. Įvertinimas: Pašalinti „%1“ ištekliai Apžvalga „%1“ apžvalga Paleisti <em>root</em> teisėmis nereikia ir netgi nepatartina. Ieškoti kategorijoje „%1“... Ieškoti... Ieškoti: „%1“... Ieškoti: „%1“ + „%2“ Nuostatos Trumpa santrauka... Atleiskite, nieko nerasta... Šaltinis: Nurodyti naują šaltinį, skirtą %1 Vis dar ieškoma... Santrauka: Palaiko appstream: url schemą Užduotys Užduotys (%1%) Nepavyksta rasti ištekliaus: %1 Atnaujinti viską Pasirinkti atnaujinimai Atnaujinti (%1) Atnaujinimai nežinomas tikrintojas atnaujinimų nepasirinkta pasirinkti atnaujinimai © 2010-2016 Plasma vystymo komanda 