��    E      D  a   l      �  
   �  
   �  
             !     %     9     H     N  	   ]     g  	   o     y     �     �  
   �     �     �  	   �     �     �     �     �     �     �               "     )  
   ;     F  2   Y  ?   �     �     �     �     �     �     �     
               .     <     ?     O     \     b     o  	   {     �     �     �     �  b   �  �   	     �	     �	  	   �	     �	     �	  
   �	  	   �	     	
     
     !
     '
     9
    J
     ^     f     x     �     �     �     �  
   �     �     �  	         
        	   7  	   A     K     W     ^     g     u     �     �     �     �     �     �     �           	          -  6   A  I   x     �     �     �  
   �     �     
          #     6     L     \     b     v  
   �     �     �     �     �     �     �  	   �  R   �  �   K     �     �  	   	          (  	   ;     E     W  
   o     z     �     �     *       D   B   3       %       C           
   ;      )   8               E      .   "                @       ?   <                        :                  A             9       &           /         0   1         !                       ,   '       5   >         =              #   -   $   +              (       7       2   4   	                  6    Activities Add Action Add Spacer Add Widgets... Alt Alternative Widgets Always Visible Apply Apply Settings Apply now Author: Auto Hide Back-Button Bottom Cancel Categories Center Close Configure Configure activity Create activity... Ctrl Currently being used Delete Email: Forward-Button Get new widgets Height Horizontal-Scroll Input Here Keyboard shortcuts Layout cannot be changed whilst widgets are locked Layout changes must be applied before other changes can be made Layout: Left Left-Button License: Lock Widgets Maximize Panel Meta Middle-Button More Settings... Mouse Actions OK Panel Alignment Remove Panel Right Right-Button Screen Edge Search... Shift Stop activity Stopped activities: Switch The settings of the current module have changed. Do you want to apply the changes or discard them? This shortcut will activate the applet: it will give the keyboard focus to it, and if the applet has a popup (such as the start menu), the popup will be open. Top Undo uninstall Uninstall Uninstall widget Vertical-Scroll Visibility Wallpaper Wallpaper Type: Widgets Width Windows Can Cover Windows Go Below Project-Id-Version: l 10n
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-09 03:10+0200
PO-Revision-Date: 2017-06-25 00:06+0200
Last-Translator: Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 2.0
 Veiklos Pridėti veiksmą Pridėti skirtuką Pridėti valdiklių... Alt Alternatyvūs valdikliai Visuomet matoma Pritaikyti Pritaikyti nuostatas Pritaikyti dabar Autorius: Automatiškai slėpti Mygtukas ėjimui atgal Apačioje Atšaukti Kategorijos Centre Užverti Konfigūruoti Konfigūruoti veiklą Sukurti veiklą... Vald Naudojama dabar Šalinti El. paštas: Mygtukas ėjimui toliau Parsisiųsti naujų valdiklių Aukštis Horizontali slinktis Įveskite čia Spartieji klavišai Negalite keisti išdėstymo, jei valdikliai užrakinti Prieš ką nors toliau keisdami, turite pritaikyti išdėstymo pakeitimus Išdėstymas: Kairėje Kairys mygtukas Licencija: Užrakinti valdiklius Išdidinti skydelį Meta Vidurinis mygtukas Daugiau nustatymų... Pelės veiksmai Gerai Skydelio lygiavimas Pašalinti pultą Dešinėje Dešinys mygtukas Ekrano kraštas Ieškoti... Lyg2 Sustabdyti veiklą Sustabdytos veiklos: Perjungti Šio modulio nuostatos pasikeitė. Norite šiuos pakeitimus pritaikyti ar atmesti? Šis trumpinys aktyvuos valdiklį: sufokusuos klaviatūrą į save ir jei valdiklis turi iššokantį meniu (tokį kaip paleidimo meniu), meniu bus atvertas. Viršuje Atšaukti pašalinimą Išdiegti Pašalinti valdiklį Vertikali slinktis Matomumas Darbalaukio fonas Darbalaukio fono tipas: Valdikliai Plotis Langai gali uždengti Langai palenda 