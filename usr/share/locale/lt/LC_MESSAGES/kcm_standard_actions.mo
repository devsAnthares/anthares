��          L      |       �       �      �   #   �        �        �     �       ,      $   M  �   r                                         EMAIL OF TRANSLATORSYour emails NAME OF TRANSLATORSYour names Standard Actions successfully saved Standard Shortcuts The changes have been saved. Please note that:<ul><li>Applications need to be restarted to see the changes.</li>    <li>This change could introduce shortcut conflicts in some applications.</li></ul> Project-Id-Version: kcm_standard_actions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2011-05-08 14:36+0300
Last-Translator: Tomas Straupis <tomasstraupis@gmail.com>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 1.2
 tomasstraupis@gmail.com Tomas Straupis Standartiniai veiksmai sėkmingai išsaugoti Standartinės klavišų kombinacijos Pakeitimai išsaugoti. Pastebėtina, kad:<ul><li>Tam, kad pakeitimai suveiktų, programas reikia paleisti iš naujo.</li> <li>Šis pakeitimas kai kuriose programose gali sukelti klaviatūros kombinacijų konfliktus.</li></ul> 