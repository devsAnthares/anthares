��          <      \       p   8   q   2   �   ?   �       9   8     r  O   �                   Finds Plasma widgets whose name or description match :q: Note this is a KRunner keywordmobile applications list all Plasma widgets that can run as standalone applications Project-Id-Version: plasma_runner_windowedwidgets
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2011-01-26 11:06+0200
Last-Translator: Tomas Straupis <tomasstraupis@gmail.com>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 1.1
 Randa Plasma valdiklius, kurių pavadinimas atitinka :q:. mobiliosios programos rodyti visus Plasma valdiklius, kurie gali veikti kaip nepriklausomos programos 