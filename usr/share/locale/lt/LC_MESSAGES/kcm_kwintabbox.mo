��    "      ,  /   <      �  
   �               ,     >     J  !   V     x     �     �     �     �     �  L   �     #     +     J     Y     u     z     �     �     �     �  	   �     �     �     �  �   �  F   �     �     �     �    �               ,     C     U     a     n     |     �     �     �     �     �  J   �     6	  +   =	     i	  .   z	     �	     �	     �	     �	     �	     �	     
     
     3
     G
  �   [
  K   �
     0     H     V                    !                                            	                                                           "               
                    Activities All other activities All other desktops All other screens All windows Alternative An example Desktop NameDesktop 1 Content Current activity Current application Current desktop Current screen Filter windows by Focus policy settings limit the functionality of navigating through windows. Forward Get New Window Switcher Layout Hidden windows Include "Show Desktop" icon Main Minimization Only one window per application Recently used Reverse Screens Shortcuts Show selected window Sort order: Stacking order The currently selected window will be highlighted by fading out all other windows. This option requires desktop effects to be active. The effect to replace the list window when desktop effects are active. Virtual desktops Visible windows Visualization Project-Id-Version: kcm_kwintabbox
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2015-12-29 20:26+0200
Last-Translator: Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>
Language-Team: lt <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 1.5
 Veiklos Visos kitos veiklos Visi kiti darbalaukiai Visi kiti ekranai Visi langai Alternatyvus Darbalaukis 1 Turinys Dabartinė veikla Dabartinė programa Dabartinis darbalaukis Dabartinis ekranas Filtruoti langus pagal Fokuso politikos nuostatos apriboja navigavimo tarp langų funkcionalumą. Pirmyn Gauti naują langų perjungimo išdėstymą Paslėpti langai Įtraukti „Rodyti darbalaukį“ ženkliuką Pagrindinis Sumažinimas Tik vienas langas programai Neseniai naudoti Atvirkščiai Ekranai Spartieji klavišai Rodyti pažymėtą langą Išdėstymo tvarka: Sudėliojimo tvarka Šiuo metu pažymėtas langas bus išskirtas visus kitus langus išblukinant. Šiai parinkčiai reikalingi įjungti darbalaukio efektai. Efektas skirtas pakeisti langų sąrašą kai darbalaukio efektai įjungti. Virtualūs darbalaukiai Matomi langai Vizualizacija 