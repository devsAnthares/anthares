��    "      ,  /   <      �      �          &  -   E  #   s  #   �  ?   �  1   �     -     A  H   F  L   �  V   �  N   3     �  
   �     �     �     �     �  2   �  
     )     8   E  #   ~  .   �  -   �  D   �  6   D  0   {  A   �  =   �  9   ,  )  f  ?   �
     �
  V   �
  2   4  
   g  
   r     }     �     �  	   �  3   �  ?   �  	   :     D     H     T     g     y      �  
   �     �     �  (   �       )     +   B  ;   n  8   �     �     �     �     �  
                                                              !       	                      
                                                           "           %1 notification %1 notifications %1 of %2 %3 %1 running job %1 running jobs &Configure Event Notifications and Actions... 10 seconds ago, keep short10 s ago 30 seconds ago, keep short30 s ago A button tooltip; expands the item to show detailsShow Details A button tooltip; hides item detailsHide Details Clear Notifications Copy Either just 1 dir or m of n dirs are being processed1 dir %2 of %1 dirs Either just 1 file or m of n files are being processed1 file %2 of %1 files How much many bytes (or whether unit in the locale has been copied over total%1 of %2 Indicator that there are more urls in the notification than previews shown+%1 Information Job Failed Job Finished No new notifications. No notifications or jobs Open... Placeholder is job name, eg. 'Copying'%1 (Paused) Select All Show application and system notifications Speed and estimated time to completion%1 (%2 remaining) Track file transfers and other jobs Use custom position for the notification popup minutes ago, keep short%1 min ago %1 min ago notification was added n days ago, keep short%1 day ago %1 days ago notification was added yesterday, keep shortYesterday notification was just added, keep shortJust now placeholder is row description, such as Source or Destination%1: the job, which can be anything, failed to complete%1: Failed the job, which can be anything, has finished%1: Finished Project-Id-Version: plasma_applet_notifications
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:07+0100
PO-Revision-Date: 2017-06-25 01:29+0200
Last-Translator: Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 2.0
 Vienas pranešimas %1 pranešimai %1 pranešimų %1 pranešimas %1 iš %2 %3 1 vykdoma užduotis %1 vykdomos užduotys %1 vykdomų užduočių %1 vykdoma užduotis &Konfigūruoti įvykių pranešimus ir veiksmus... prieš 10s prieš 30s Rodyti išsamiau Nerodyti taip išsamiai Išvalyti pranešimus Kopijuoti 1 apl. %2 iš %1 apl. %2 iš %1 apl. %2 iš %1 apl. %1 failas  %2 iš %1 failų   %2 iš %1 failų  %2 iš %1 failo %1 iš %2 +%1 Informacija Užduotis nepavyko Užduotis atlikta Nėra naujų pranešimų. Nėra pranešimų ar užduočių Atverti... %1 (pristabdyta) Pažymėti viską Rodyti programų ir sistemos pranešimus %1 (liko %2) Sekti failų siuntimus ir kitas užduotis Naudoti savitą pranešimų lango poziciją prieš %1 min. prieš %1 min. prieš %1 min. prieš %1 min. Vakar Prieš %1 dienas Prieš %1 dienų Prieš %1 dieną Vakar Ką tik %1: %1: nepavyko %1: baigta 