��    
      l      �       �   L   �   +   >  #   j     �  P   �  	   �  (     I   -  K   w    �     �  	   �  *   �  "        9     X     f     �     �                                      	   
    %1 is name of the newly connected displayA new display %1 has been detected Disables the newly connected screenDisable Failed to connect to KScreen daemon Failed to load root object Makes the newly conencted screen a clone of the primary oneClone Primary Output No Action Opens KScreen KCMAdvanced Configuration Places the newly connected screen left of the existing oneExtend to Left Places the newly connected screen right of the existing oneExtend to Right Project-Id-Version: trunk-kf 5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2015-08-25 11:02+0200
Last-Translator: Liudas Ališauskas <liudas@akmc.lt>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 2.0
 Aptiktas naujas ekranas %1 Išjungti Nepavyko prisijungti prie KScreen tarnybos Nepavyko įkelti šakninio objekto Klonuoti pagrindinę išvestį Jokio veiksmo Išsamesnė konfigūracija Išplėsti į kairę Išplėsti į dešinę 