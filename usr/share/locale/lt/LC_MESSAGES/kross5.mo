��            )   �      �  &   �     �     �     �     �      �               .     6  ,   S  3   �     �     �     �     �     �  '        4     S     Y     o     �     �     �     �     �  &   �     �    �          	     %     1     =  ^   I     �  !   �  	   �     �  6   �  @   2  ,   s  -   �     �     �     �  $   �  \   	     w	     �	  &   �	  
   �	  "   �	     �	  )    
     *
  *   /
     Z
                                       
                      	                                                                            @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2014-03-16 20:46+0200
Last-Translator: Liudas Ališauskas <liudas@aksioma.lt>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 1.5
 Bendros Pridėti naują scenarijų. Pridėti... Atsisakyti? Komentaras: rch@richard.eu.org, dgvirtual@akl.lt, gintautas@miselis.lt, stikonas@gmail.com, liudas@akmc.lt Keisti Redaguoti pasirinktą scenarijų. Keisti... Vykdyti pasirinktą scenarijų. Nepavyko sukurti scenarijaus interpretatoriui „%1“ Nepavyko nustatyti interpretatoriaus scenarijaus failui „%1“ Nepavyko paleisti interpretatoriaus „%1“ Nepavyko atidaryti scenarijaus failo „%1“ Failas: Ženkliukas: Interpretatorius: Ruby interpretatoriaus saugumo lygis Ričardas Čepas, Donatas Glodenis, Gintautas Miselis, Andrius Štikonas, Liudas Ališauskas Pavadinimas: Nėra tokio funkcijos „%1“ Nėra tokio interpretatoriaus „%1“ Pašalinti Pašalinti pasirinktą scenarijų. Vykdyti Scenarijaus failas „%1“ neegzistuoja. Stop Nutraukti pasirinkto scenarijaus vykdymą. Tekstas: 