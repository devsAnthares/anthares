��          �      L      �     �     �  N   �     F     W      r     �     �  
   �     �     �     �                8     V  $   m     �    �     �     �  \   �     F     W     r     �      �     �     �     �     �     �               0     K     k                                                                                    	   
             Arguments for the module Configuration module to open Could not find module '%1'. See kcmshell5 --list for the full list of modules. Daniel Molkentin Do not display main window EMAIL OF TRANSLATORSYour emails Frans Englich List all possible modules Maintainer Matthias Elter Matthias Ettrich Matthias Hoelzer-Kluepfel NAME OF TRANSLATORSYour names No description available Specify a particular language System Settings Module The following modules are available: Waldo Bastian Project-Id-Version: kcmshell
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-07 06:55+0200
PO-Revision-Date: 2017-06-25 01:19+0200
Last-Translator: Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 2.0
 Modulio parametrai Atverti konfigūravimo modulį Nerastas modulis „%1“. Pilną modulių sąrašą galite gauti įvykdę kcmshell5 --list. Daniel Molkentin Nerodyti pagrindinio lango dgvirtual@akl.lt Frans Englich Visų galimų modulių sąrašas Prižiūrėtojas Matthias Elter Matthias Ettrich Matthias Hoelzer-Kluepfel Donatas Glodenis Nėra aprašymo Nurodykite kalbą Sistemos nuostatų modulis Naudoti galima šiuos modulius: Waldo Bastian 