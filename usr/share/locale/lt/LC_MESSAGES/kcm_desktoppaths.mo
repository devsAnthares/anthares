��            )   �      �  �   �  	   G     Q     a     w       	   �     �  	   �     �  %   �     �     �                    %     .  X   =  S   �  �   �  I   �  F   �  E   >  H   �  B   �  :     7   K    �  �   �	     p
     �
     �
     �
     �
  
   �
     �
     �
                     )     0     ?     F     V     e  T   x  T   �  �   "  X   �  A   E  D   �  G   �  Q     
   f  
   q                                                        	                                                                 
                 <h1>Paths</h1>
This module allows you to choose where in the filesystem the files on your desktop should be stored.
Use the "Whats This?" (Shift+F1) to get help on specific options. Autostart Autostart path: Confirmation Required Desktop Desktop path: Documents Documents path: Downloads Downloads path: Move files from old to new placeMove Move the directoryMove Movies Movies path: Music Music path: Pictures Pictures path: The path for '%1' has been changed.
Do you want the files to be moved from '%2' to '%3'? The path for '%1' has been changed.
Do you want to move the directory '%2' to '%3'? This folder contains all the files which you see on your desktop. You can change the location of this folder if you want to, and the contents will move automatically to the new location as well. This folder will be used by default to load or save documents from or to. This folder will be used by default to load or save movies from or to. This folder will be used by default to load or save music from or to. This folder will be used by default to load or save pictures from or to. This folder will be used by default to save your downloaded items. Use the new directory but do not move anythingDo not Move Use the new directory but do not move filesDo not Move Project-Id-Version: kcm_desktoppaths
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-09 07:18+0200
PO-Revision-Date: 2015-12-30 18:09+0200
Last-Translator: Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>
Language-Team: lt <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 1.5
 <h1>Keliai</h1>
Šis modulis leis jums nustatyti, kurioje failų sistemos vietoje saugomi darbalaukio failai.
Pasinaudokite „Kas tai yra?“ (Lyg2+F1) pagalba norėdami daugiau sužinoti apie atskiras parinktis. Automatinis paleidimas Automatinio paleidimo kelias: Reikia patvirtinimo Darbalaukis Darbalaukio kelias: Dokumentai Dokumentų kelias: Atsiuntimai Atsiuntimų kelias: Perkelti Perkelti Filmai Filmų kelias: Muzika Muzikos kelias: Paveikslėliai Nuotraukų kelias: „%1“ kelias pasikeitė.
Ar Jūs norite perkelti failus iš „%2“ į „%3“? „%1“ kelias pasikeitė.
Ar Jūs norite perkelti failus iš „%2“ į „%3“? Šiame aplanke yra saugomi viso failai, kuriuos matote darbalaukyje. Jei pageidaujate, galite pakeisti šito aplanko vietą. Tada ir visas turinys taip pat bus automatiškai perkeltas į naują vietą. Šis aplankas kitaip nenurodžius yra naudojamas dokumentų įkrovimui ir išsaugojimui. Šis aplankas kitaip nenurodžius yra naudojamas filmams saugoti. Šis aplankas kitaip nenurodžius yra naudojamas muzikos saugojimui. Šis aplankas kitaip nenurodžius yra naudojamas nuotraukų saugojimui. Šis aplankas kitaip nenurodžius yra naudojamas atsiųstų failų išsaugojimui. Neperkelti Neperkelti 