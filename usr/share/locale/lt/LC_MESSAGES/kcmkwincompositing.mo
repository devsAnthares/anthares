��    "      ,  /   <      �     �          	  	     A   $  >   f  9   �  9   �  9     E   S     �     �     �     �  X   �     ;     C     Y     q     w  
   �  
   �     �     �     �     �               #     *     :  	   X     b    j     �     �     �     �     �  	   �     �     �  	   �     �     	     	  #   %	     I	  i   c	     �	     �	     �	     
     
  
   )
  
   4
     ?
     C
     G
     f
     �
     �
     �
     �
  "   �
     �
     �
                      	                                               "                                !                                   
                      Accurate Always Animation speed: Automatic Category of Desktop Effects, used as section headerAccessibility Category of Desktop Effects, used as section headerAppearance Category of Desktop Effects, used as section headerCandy Category of Desktop Effects, used as section headerFocus Category of Desktop Effects, used as section headerTools Category of Desktop Effects, used as section headerWindow Management Configure filter Crisp Enable compositor on startup Full screen repaints Hint: To find out or configure how to activate an effect, look at the effect's settings. Instant KWin development team Keep window thumbnails: Never Only for Shown Windows OpenGL 2.0 OpenGL 3.1 OpenGL Platform InterfaceEGL OpenGL Platform InterfaceGLX Re-enable OpenGL detection Rendering backend: Scale method: Search Smooth Smooth (slower) Tearing prevention ("vsync"): Very slow XRender Project-Id-Version: kcmkwincompositing
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2015-12-29 20:05+0200
Last-Translator: Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>
Language-Team: lt <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 1.5
 Tikslus Visada Animacijos greitis: Automatinis Pritaikymas neįgaliesiems Išvaizda Saldainiukai Fokusas Įrankiai Langų tvarkymas Konfigūruoti filtrą Aiškus Įjungti kompozitorių paleidžiant Pilno ekrano perpiešimai Užuomina: norėdami rasti, kaip aktyvuoti efektą arba jį konfigūruoti, žiūrėkite efekto nuostatas. Akimirksniu KWin vystymo komanda Laikyti langų miniatiūras: Niekada Tik rodomiems langams OpenGL 2.0 OpenGL 3.1 EGL GLX Vėl įjungti OpenGL aptikimą Atvaizdavimas galinė sąsaja: Mastelio keitimo būdas: Ieškoti Glotnus Glotnus (lėtesnis) Plyšimo prevencija („VSync“): Labai lėtai XRender 