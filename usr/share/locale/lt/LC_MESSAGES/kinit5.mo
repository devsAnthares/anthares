��          �      �       H     I  $   i     �      �     �     �     
       '   5  �   ]  �        �  t   �     S  (   T  /   }  !   �     �  )   �  %        >  '   M  C   u  �   �  �   V     	  x   %	                                                      
   	        Could not find '%1' executable. Could not find 'kdemain' in '%1'.
%2 Could not find service '%1'. Could not find the '%1' plugin.
 Could not open library '%1'.
%2 KDEInit could not launch '%1' Launching %1 Service '%1' is malformatted. Service '%1' must be executable to run. Unable to create new process.
The system may have reached the maximum number of processes possible or the maximum number of processes that you are allowed to use has been reached. Unable to start new process.
The system may have reached the maximum number of open files possible or the maximum number of open files that you are allowed to use has been reached. Unknown protocol '%1'.
 klauncher: This program is not supposed to be started manually.
klauncher: It is started automatically by kdeinit5.
 Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-04 03:08+0100
PO-Revision-Date: 2015-01-27 23:44+0200
Last-Translator: Liudas Ališauskas <liudas@akmc.lt>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 1.5
 Nepavyko rasti „%1“ vykdomojo failo. Nepavyko rasti „kdemain“ „%1“ faile.
%2 Nepavyko rasti tarnybos „%1“. Nepavyko rasti papildinio %1.
 Nepavyko atverti bibliotekos „%1“.
%2 KDEInit nesugebėjo paleisti „%1“ Startuojama %1 Tarnyba „%1“ yra blogai suformuota. Norint paleisti servisą „%1“ rekia jo failą padaryti vykdomu. Nepavyko sukurti naujo proceso.
Sistema pasiekė maksimalų galimų procesų skaičių, arba Jūs pasiekėte Jums leidžiamą maksimalų procesų skaičių. Nepavyko pradėti naujo proceso.
Sistema pasiekė maksimalų galimų atverti failų skaičių, arba Jūs pasiekėte Jums leidžiamą maksimalų galimų atverti failų skaičių. Nežinomas protokolas '%1'.
 klauncher: Ši programa neturėtų būti paleidžiama rankiniu būdu.
klauncher: Ją automatiškai paleidžia kdeinit5.
 