��          |      �             !     9      E     f  
   �     �  &   �     �     �     �  1     %  E  *   k     �  #   �  %   �     �       1   !  $   S  &   x  $   �  A   �                             	      
                        Configure Desktop Theme David Rosca EMAIL OF TRANSLATORSYour emails NAME OF TRANSLATORSYour names Open Theme Remove Theme Theme Files (*.zip *.tar.gz *.tar.bz2) Theme installation failed. Theme installed successfully. Theme removal failed. This module lets you configure the desktop theme. Project-Id-Version: kcm_desktopthemedetails
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-22 05:21+0100
PO-Revision-Date: 2017-06-25 00:59+0200
Last-Translator: Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 2.0
 Konfigūruoti darbalaukio apipavidalinimą David Rosca andrius@stikonas.eu, liudas@akmc.lt Andrius Štikonas, Liudas Ališauskas Atverti apipavidalinimą Pašalinti apipavidalinimą Apipavidalinimo failai (*.zip *.tar.gz *.tar.bz2) Apipavidalinimo įdiegimas nepavyko. Apipavidalinimas įdiegtas sėkmingai. Apipavidalinimo pašalinti nepavyko. Šis modulis leidžia konfigūruoti darbalaukio apipavidalinimą. 