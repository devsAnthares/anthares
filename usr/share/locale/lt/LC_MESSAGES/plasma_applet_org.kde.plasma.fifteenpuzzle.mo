��          t      �         
             +     8     D     R     Z     _     {  B   �    �  	   �     �                     0     9     ?     ^  
   z                                                     
   	    Appearance Fifteen Puzzle Number color Piece color Show numerals Shuffle Size Solve by arranging in order Solved! Try again. The time since the puzzle started, in minutes and secondsTime: %1 Project-Id-Version: plasma_applet_fifteenPuzzle
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-01-12 03:59+0100
PO-Revision-Date: 2014-11-05 19:14+0200
Last-Translator: liudas@aksioma.lt <liudas@aksioma.lt>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 1.5
 Išvaizda Penkiolika dalių Numerio spalva Dalies spalva Rodyti numerius Maišyti Dydis Išspręsti rikiuojant eilėje Išspręsta! Mėginti vėl. Laikas: %1 