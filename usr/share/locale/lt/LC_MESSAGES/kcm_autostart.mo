��            )   �      �     �     �     �     �     �     �               "     .     E  /   M     }     �      �     �  
   �     �     �     �  P        S     _     r     z     �     �  !   �    �     �       !        <     V     c     o     �     �     �     �  7   �            -   /     ]     j     {  "   �     �  ]   �     	     %	     B	     Q	     Y	  	   f	  
   p	                
                                                                                                                	          "%1" does not exist. "%1" is not a file. "%1" is not an absolute path. "%1" is not readable. &Properties... &Remove Add Program... Add Script... Advanced... Before session startup Command Copyright © 2006–2010 Autostart Manager team Create as symlink Desktop File EMAIL OF TRANSLATORSYour emails Logout Maintainer Montel Laurent NAME OF TRANSLATORSYour names Name Only files with “.sh” extensions are allowed for setting up the environment. Script File Shell script path: Startup Status Stephen Leaf The program will be runEnabled The program won't be runDisabled Project-Id-Version: kcm_autostart
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-09 07:18+0200
PO-Revision-Date: 2017-06-25 00:57+0200
Last-Translator: Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 2.0
 „%1“ neegzistuoja. „%1“ nėra failas. „%1“ nėra absoliutus kelias. „%1“ yra neskaitomas. &Savybės... &Pašalinti Pridėti programą... Pridėti scenarijų... Išsamiau... Prieš pradedant seansą Komanda (c) 2006-2008 Automatinio paleidimo tvarkyklės komanda Sukurti simbolinę nuorodą *.desktop failas matasbbb@gmail.com, antanas.ursulis@gmail.com Atsijungiant Prižiūrėtojas Montel Laurent Matas Brazdeikis, Antanas Uršulis Vardas Nustatydama sistemos aplinkos pasirinkimus KDE skaito tiktai failus su „.sh“ plėtiniais. Scenarijaus failas Apvalkalo scenarijus kelias: Paleidimo metu Būsena Stephen Leaf Įjungtas Išjungtas 