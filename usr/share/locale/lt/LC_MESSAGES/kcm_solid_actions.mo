��    .      �  =   �      �  (   �          1     Q     ]     j  *   q  *   �     �     �  �   �  	   �     �     �                7  <   D      �     �     �     �  "   �     �     �       r   &  ;   �  
   �     �     �          "     3     B     I     Y  #   p  )   �     �  !   �  &   �  $   #	  _   H	     �	  	  �	  1   �     �  6        >     R     g  '   s  *   �     �     �    �  	   �          	          1     O  8   a  #   �     �  	   �     �  *   �     "  '   '     O  v   c  8   �       %   $     J     f     w     �  
   �     �     �  -   �  3     &   B     i  %   �  (   �  j   �     A                                      -                     '   *   &   $   !                                                ,             +   
       	   %   (       )                           "             .   #    (c) 2009, 2014 Solid Device Actions team (c) 2009, Ben Cooksley Action icon, click to change it Action name Action name: Add... All of the contained properties must match Any of the contained properties must match Ben Cooksley Cannot be deleted Command that will trigger the action
This can include any or all of the following case insensitive expands:

%f: The mountpoint for the device - Storage Access devices only
%d: Path to the device node - Block devices only
%i: The identifier of the device Command:  Contains Content Conjunction Content Disjunction Device Interface Match Device type: Devices must match the following parameters for this action: EMAIL OF TRANSLATORSYour emails Edit Parameter Edit... Editing Action '%1' Enter the name for your new action Equals Error Parsing Device Conditions Invalid action It appears that the action name, command, icon or condition are not valid.
Therefore, changes will not be applied. It appears that the predicate for this action is not valid. Maintainer NAME OF TRANSLATORSYour names No Action Selected Parameter type: Port to Plasma 5 Property Match Remove Reset Parameter Save Parameter Changes Solid Action Desktop File Generator Solid Device Actions Control Panel Module Solid Device Actions Editor The device must be of the type %1 The device property %1 must contain %2 The device property %1 must equal %2 Tool to automatically generate Desktop Files from Solid DeviceInterface classes for translation Value name: Project-Id-Version: kcm_solid_actions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-02-08 04:00+0100
PO-Revision-Date: 2015-01-25 12:15+0200
Last-Translator: Liudas Ališauskas <liudas@akmc.lt>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 1.5
 (c) 2009, 2014 Solid įrenginių veiksmų komanda (c) 2009, Ben Cooksley Veiksmo ženkliukas, spragtelėkite, kad pakeistumėte Veiksmo pavadinimas Veiksmo pavadinimas: Pridėti... Visos iš turimų savybių turi sutapti Bet kuri iš turimų savybių turi sutapti Ben Cooksley Negali būti ištrintas Komanda, kuri paleis veiksmą
Tai gali būti vienas arba visi iš žemiau pateikiamų plėtinių (raidžių dydis nesvarbus):

%f: Įrenginio prijungimo vieta - tik kaupikliams
%d: Kelias iki įrenginio mazgo - tik blokiniams įrenginiams
%i: įrenginio identifikatorius Komanda:  Turi Turinio konjunkcija Turinio disjunkcija Įrenginio sąsajos atitikmuo Įrenginio tipas: Įrenginys veiksmui turi atitikti sekančius parametrus: andrius@stikonas.eu, liudas@akmc.lt Keisti parametrą Keisti... Keičiamas veiksmas „%1“ Įveskite jūsų naujo veiksmo pavadinimą Lygu Klaida analizuojant įrenginio sąlygas Netinkamas veiksmas Panašu, kad nekorektiškas veiksmo pavadinimas, komanda, ženkliukas arba sąlyga.
Taigi pakeitimai nebus pritaikyti. Panašu, kad šio veiksmo predikatas yra nekorektiškas. Prižiūrėtojas Andrius Štikonas, Liudas Ališauskas Nepasirinktas joks veiksmas Parametro tipas: Perkėlimas į Plasma 5 Savybės atitikmuo Pašalinti Atkurti parametrą Įrašyti parametrų pakeitimus Solid veiksmų darbalaukio failo generatorius Solid įrenginių veiksmų valdymo skydelio modulis Solid įrenginių veiksmų redaktorius Įrenginys turi būti tipo %1 Įrenginio savybė %1 turi turėti %2 Įrenginio savybė %1 turi būti lygi %2 Įrankinis automatiškam darbalaukio failų vertimui generavimui pagal solid įrenginių interfeiso klases Vertės pavadinimas: 