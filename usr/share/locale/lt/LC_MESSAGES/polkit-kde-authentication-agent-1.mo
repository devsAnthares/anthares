��          �      �        ;   	  J   E     �     �  ~   �  A   .     p  )   }     �     �      �     �  
   �          $     5      H  "   i     �  	   �     �     �    �     �     �     �  	     }     M   �  	   �  6   �          6  #   S     w     �  %   �     �     �  5   �  7   	  '   P	     x	     �	  
   �	                    
                                	                                                    %1 is the full user name, %2 is the user login name%1 (%2) %1 is the name of a detail about the current action provided by polkit%1: (c) 2009 Red Hat, Inc. Action: An application is attempting to perform an action that requires privileges. Authentication is required to perform this action. Another client is already authenticating, please try again later. Application: Authentication failure, please try again. Click to edit %1 Click to open %1 EMAIL OF TRANSLATORSYour emails Jaroslav Reznik Maintainer NAME OF TRANSLATORSYour names Password for %1: Password for root: Password or swipe finger for %1: Password or swipe finger for root: Password or swipe finger: Password: Select User Vendor: Project-Id-Version: polkit-kde-authentication-agent-1
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:22+0100
PO-Revision-Date: 2012-04-21 19:34+0300
Last-Translator: Liudas Ališauskas <liudas@akmc.lt>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 1.4
 %1 (%2) %1: (c) 2009 Red Hat, Inc. Veiksmas: Programa mėgina įvykdyti veiksmą, kuris reikalauja leidimų. Tapatybės nustatymas yra privalomas įvykdyti šį veiksmą. Kitas klientas jau nustato tapatybę, prašome pamėginti dar kartą vėliau. Programa: Autentifikavimosi klaida, prašome bandyti dar kartą. Spragtelėkite redagavimui %1 Spragtelėkite atidarymui %1 andrius@stikonas.eu, liudas@akmc.lt Jaroslav Reznik Prižiūrėtojas Andrius Štikonas, Liudas Ališauskas %1 slaptažodis: root slaptažodis: Slaptažodis arba perbraukimas pirštu naudotojui %1: Slaptažodis arba perbraukimas pirštu root naudotojui: Slaptažodis arba perbraukimas pirštu: Slaptažodis: Pasirinkite naudotoją Tiekėjas: 