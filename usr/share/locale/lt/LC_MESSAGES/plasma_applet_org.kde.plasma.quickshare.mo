��          �      <      �     �     �     �  +   �  @        L     a     i     w     }     �  
   �     �     �     �     �     �    
          !     *  /   C  O   s     �     �     �     �     �     	           .     7     S     i  &   �     
         	                                                                                       <a href='%1'>%1</a> Close Copy Automatically: Don't show this dialog, copy automatically. Drop text or an image onto me to upload it to an online service. Error during upload. General History Size: Paste Please wait Please, try again. Sending... Share Shares for '%1' Successfully uploaded The URL was just shared Upload %1 to an online service Project-Id-Version: trunk-kf 5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-01 04:04+0100
PO-Revision-Date: 2015-08-30 15:27+0200
Last-Translator: Liudas Ališauskas <liudas@akmc.lt>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 2.0
 <a href='%1'>%1</a> Užverti Automatiškai kopijuoti: Nerodyti šio dialogo, automatiškai kopijuoti. Numeskite tekstą ar paveikslėlį ant manęs, kad nusiųsti jį į internetą. Išsiuntimo klaida. Bendra Istorijos dydis: Padėti Prašome palaukti Prašome bandyti vėl. Siunčiama... Dalintis Bendrinimai skirti „%1“ Sėkmingai išsiųsta Ką tik pasidalinta URL Nusiųsti %1 į internetinę paslaugą 