��    `        �         (  %   )  $   O     t     y     �     �     �     �     �  
   �     �     �     	     #	     1	     A	     H	     Q	     _	     f	  
   v	     �	     �	     �	     �	  	   �	     �	     �	     �	     �	     �	     �	     �	  
   �	     
     
     (
     =
     M
  
   T
  
   _
     j
     z
     �
     �
     �
     �
     �
     �
     �
                    +  	   9  
   C     N     S  	   a     k     t     �     �     �     �     �     �     �     �     �                     &  
   5     @     X     _  	   k     u     }     �     �     �     �     �     �     �               >     W     n     �     �     �     �     �     �     �     �     
          8     V     e     w     �  !   �     �     �     �                    !  	   5     ?     T     `     s     |  	   �     �     �     �     �     �     �     �     �          4     S     e     |     �     �     �     �     �     �          !     )     :     P     X     o     �     �     �     �  	   �     �     �     �     �          #     +     J     X     `     v          �     �     �     �     �     �     �     �               !     A     X     a      t     �     �     �     �     �     �     �     �     �     �        @   6           ^   ;      I      4   L   J   &           .              <       G   	          0       -          R   W   !   E      (       A   `       M   $             +                  O      [       5   /                         7   _          Q             Z          V   *   ?           K            2   3       B       F                       :   S   D       U          
   Y      %       #   >   X   "   9   8   N   1          T   ,   P   C   =   )   ]   '   H   \               %1 is a host nameMessage from %1:
%2 @item sensor descriptionSystem Load ACPI Active Devices Active Memory Application Memory Average CPU Temperature Average Clock Frequency Battery Battery %1 Battery Capacity Battery Charge Battery Discharge Rate Battery Usage Battery Voltage CPU %1 CPU Load CPU LoadLoad Change Clock Frequency Collisions Cooling Device Cores Current State Data Data Rate Disk %1 Disk Information Disk Throughput Errors Fan Fan %1 Free Memory Free Space Hardware Sensors Host %1 not found Idle Processes Count Inactive Memory Int %1 Interfaces Interrupts Last Process ID Load Average (1 min) Load Average (15 min) Load Average (5 min) Locked Processes Count Memory Network Number of Blocks Number of Devices Packets Partition Usage Physical Memory Process Count Processes Processors Rate Read Accesses Read Data Receiver Remaining Time Running Processes Count Sleeping Processes Count State Stopped Processes Count Swap Memory System System Calls Table Temperature Temperature %1 Thermal Zone Total Total Accesses Total Load Total Number of Devices Uptime Used Memory User Load Waiting Waiting Processes Count Working Devices Write Accesses Written Data Zombie Processes Count a percentage% kBytes the frequency unitMHz the unit 1 per second1/s the unit milliampere hoursmAh the unit milliamperesmA the unit millivoltsmV the unit milliwatt hoursmWh the unit milliwattsmW the unit minutesmin Project-Id-Version: l 10n
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2014-03-19 08:59+0200
Last-Translator: Liudas Ališauskas <liudas@aksioma.lt>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 1.5
 Pranešimas iš %1:
%2 Sistemos apkrova ACPI Aktyvūs įrenginiai Aktyvi atmintis Programos atmintis Vidutinė CPU temperatūra Vidutinis laikrodžio dažnis Akumuliatorius Akumuliatorius %1 Akumuliatoriaus talpa Akumuliatoriaus įkrovimas Akumuliatoriaus iškrovimo sparta Akumuliatoriaus naudojimas Akumuliatoriaus įtampa CPU %1 CPU apkrova Įkelti Pakeisti Laikrodžio dažnis Kolizijos Aušinimo įrenginys Branduoliai Dabartinė būsena Duomenys Duomenų sparta Diskas %1 Disko informacija Disko našumas Klaidos Ventiliatorius Ventiliatorius %1 Laisva atmintis Laisva vieta Aparatinės įrangos sensoriai Mazgo %1 rasti nepavyko Neveiksnių procesų skaičius Neaktyvi atmintis Sveikasis skaičius %1 Sąsajos Pertrauktys Paskutinio proceso ID Vidutinė apkrova (1 min.) Vidutinė apkrova (15 min.) Vidutinė apkrova (5 min.) Užrakintų procesų skaičius Atmintis Tinklas Blokų skaičius Įrenginių skaičius Paketai Skaidinio panaudojimas Fizinė atmintis Procesų skaičius Procesas Procesoriai Sparta Skaitymas Nuskaityti duomenys Gavėjas Likęs laikas Veikiančių procesų skaičius Miegančių procesų skaičius Būsena Sustabdytų procesų skaičius Swap atmintis Sistema Sistemos iškvietimai Lentelė Temperatūra Temperatūra %1 Terminė zona Iš viso Bendras pasiekimas Visa apkrova Viso įrenginių Veikimo laikas Naudojama atmintis Naudotojo apkrova Laukiama Laukiančių procesų skaičius Veikiantys įrenginiai Rašymas Įrašyti duomenys Procesų „Zombių“ skaičius % KBaitai MHz 1/s mAh mA mV mWh mW min. 