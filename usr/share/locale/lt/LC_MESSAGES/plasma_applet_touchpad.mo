��    	      d      �       �      �      �      �   E   
     P     f     o     �     �  	   �     �     �  B   �     %     D      Z     {        	                                      Disable Disable touchpad Enable touchpad No mouse was detected.
Are you sure you want to disable the touchpad? No touchpad was found Touchpad Touchpad is disabled Touchpad is enabled Project-Id-Version: l 10n
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2014-03-22 08:56+0200
Last-Translator: Liudas Ališauskas <liudas@aksioma.lt>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 1.5
 Išjungti Išjungti jutiklinį kilimėlį Įjungti jutiklinį kilimėlį Neaptikta pelė.
Ar tikrai norite išjungti jutiklinį kilimėlį? Nerastas jutiklinis kilimėlis Jutiklinis kilimėlis Jutiklinis kilimėlis išjungtas Jutiklinis kilimėlis įjungtas 