��          �      ,      �     �     �     �     �     �     �     �     �  .   �     .     L     \     m     �     �  H   �    �                 7     G     `     |     �     �  2   �  X   �  8   =     v     �     �     �  �   �        	                                                                  
    &Configure Printers... Active jobs only All jobs Completed jobs only Configure printer General No active jobs No jobs No printers have been configured or discovered One active job %1 active jobs One job %1 jobs Open print queue Print queue is empty Printers Search for a printer... There is one print job in the queue There are %1 print jobs in the queue Project-Id-Version: l 10n
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2015-02-21 09:37+0000
PO-Revision-Date: 2016-08-30 16:19+0200
Last-Translator: Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 2.0
 &Konfigūruoti spausdintuvus... Tik aktyvios užduotys Visos užduotys Tik užbaigtos užduotys Konfigūruoti spausdintuvą Bendros Aktyvių užduočių nėra Užduočių nėra Spausdintuvai dar nesukonfigūruoti ar jų aptikta Viena aktyvi užduotis %1 aktyvios užduotys %1 aktyvių užduočių %1 aktyvi užduotis Viena užduotis %1 užduotys %1 užduočių %1 užduotis Atverti spausdinimo eilę Spausdinimo eilė yra tuščia Spausdintuvai Ieškoti spausdintuvo... Viena spausdinimo užduotis eilėje %1 spausdinimo užduotys eilėje %1 spausdinimo užduočių eilėje %1 spausdinimo užduotis eilėje 