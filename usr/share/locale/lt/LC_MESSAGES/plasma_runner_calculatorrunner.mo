��          4      L       `   m   a   R   �     "  t   >  <   �                    Calculates the value of :q: when :q: is made up of numbers and mathematical symbols such as +, -, /, * and ^. The exchange rates could not be updated. The following error has been reported: %1 Project-Id-Version: plasma_runner_calculatorrunner
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-28 05:09+0200
PO-Revision-Date: 2011-01-20 23:35+0200
Last-Translator: Tomas Straupis <tomasstraupis@gmail.com>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 1.1
 Suskaičiuoja :q: reikšmę, kai :q: sudarytas iš skaičių ir matematinių simbolių, tokių kaip +, -, /, * ir ^. Nepavyko atnaujinti valiutų kursų. Klaidos pranešimas: %1 