��          |      �             !     1  �   8  �   �  Z   B     �     �     �     �     �     �    �     �     
  �     �   �  �   [     �     �                    6                        
              	                     Animate buttons Center Check this option if the window border should be painted in the titlebar color. Otherwise it will be painted in the background color. Check this option if you want the buttons to fade in when the mouse pointer hovers over them and fade out again when it moves away. Check this option if you want the titlebar text to have a 3D look with a shadow behind it. Colored window border Config Dialog Left Right Title &Alignment Use shadowed &text Project-Id-Version: kwin_clients
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-02 03:16+0100
PO-Revision-Date: 2013-04-29 00:35+0300
Last-Translator: Liudas Ališauskas <liudas@akmc.lt>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 1.5
 Animuoti ženkliukus Centre Pažymėkite šią parinktį jei norite, kad langų rėmeliai būtų piešiami naudojant antraštės juostos spalvas. Priešingu atveju, jie piešiami naudojant įprastas kraštų spalvas. Pažymėkite šią parinktį jei norite, kad mygtukai išryškėtų užvedus ant jų pelės klavišą ir po to vėl išnyktų jį patraukus. Pažymėkite šią parinktį, jei norite, kad antraštės juostos tekstas turėtų trimatę išvaizdą su šešėliu už teksto. Spalvotas lango rėmelis Konfigūravimo dialogas Kairė Dešinė Antraščių &lygiavimas Naudoti tekstą &su šešėliu 