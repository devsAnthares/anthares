��          �      ,      �     �  $  �     �  	   �  	   �  F   �     %  
   ,     7  
   >     I  	   R     \     a  	   {     �    �  '   �  �  �     Q	  
   Z	     e	  D   s	     �	     �	     �	     �	     �	  
   �	     
  "   
  
   /
     :
            
                                    	                              &Select test picture: <h1>Monitor Gamma</h1> This is a tool for changing monitor gamma correction. Use the four sliders to define the gamma correction either as a single value, or separately for the red, green and blue components. You may need to correct the brightness and contrast settings of your monitor for good results. The test images help you to find proper settings.<br> You can save them system-wide to XF86Config (root access is required for that) or to your own KDE settings. On multi head systems you can correct the gamma values separately for all screens. Blue: CMY Scale Dark Gray Gamma correction is not supported by your graphics hardware or driver. Gamma: Gray Scale Green: Light Gray Mid Gray RGB Scale Red: Save settings system wide Screen %1 Sync screens Project-Id-Version: kcmkgamma
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-18 03:02+0200
PO-Revision-Date: 2015-12-29 20:05+0200
Last-Translator: Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>
Language-Team: lt <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 1.5
 &Pasirinkite bandomąjį paveikslėlį: <h1>Ekrano gama</h1> Šiuo įrankiu atliekamos monitoriaus spalvų gamos korekcijos. Naudokite keturias slinkties juostas arba kartu, arba atskirai keičiant raudonos, žalios ir mėlynos spalvų intensyvumą. Norėdami pasiekti geresnių rezultatų gal būt turėsite pakoreguoti savo monitoriaus ryškumo ir kontrasto nuostatas. Teisingas nuostatas Jums padės rasti bandymo paveikslėliai.<br> Galite išsaugoti šias nuostatas visai sistemai į XF86Config (tam reikės root - „Administratoriaus veiksenos“), arba tik į savo KDE nuostatas. Sistemoms, turinčioms kelis monitorius, šias reikšmes galite koreguoti kiekvienam jų atskirai. Mėlyna: CMY skalė Tamsiai pilka Jūsų grafinė plokštė arba valdyklė nepalaiko gamma korekcijos. Gama: Pilkio skalė Žalia: Šviesiai pilka Vidutiniškai pilka RGB skalė Raudona: Įrašyti nuostatas visai sistemai Ekranas %1 Sinchronizuoti ekranus 