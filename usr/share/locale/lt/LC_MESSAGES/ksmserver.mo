��    	      d      �       �      �      �           !     >  ,   Z  !   �  c   �      )        <  
   V      a     �  1   �  *   �  c   �                                    	          Also allow remote connections Halt Without Confirmation Log Out Log Out Without Confirmation Reboot Without Confirmation Restores the saved user session if available Starts the session in locked mode The reliable KDE session manager that talks the standard X11R6 
session management protocol (XSMP). Project-Id-Version: ksmserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-05 03:18+0100
PO-Revision-Date: 2014-11-11 07:15+0200
Last-Translator: Liudas Ališauskas <liudas@aksioma.lt>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 1.5
 Taip pat leisti nuotolinius prisijungimus Išjungti be patvirtinimo Atsijungti Išsiregistruoti be patvirtinimo Perkrauti be patvirtinimo Atstato išsaugotą naudotojo sesiją, jei ji yra Paleidžia sesiją užrakintoje veiksenoje Patikima KDE sesijos tvarkyklė, naudojanti standartinį X11R6 
sesijų tvarkymo protokolą (XSMP). 