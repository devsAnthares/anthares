��          �      �       0     1  V   6  /   �     �  '   �       5     6   H  +     D   �     �  �       �     �     �       #     '   =     e  O   y     �  .   �       3     A   P                  	                   
                      MiB Allows the user to configure the warning notification being shownConfigure Warning... Allows the user to hide this notifier itemHide Enable low disk space warning Is the free space notification enabled. Low Disk Space Minimum free space before user starts being notified. Opens a file manager like dolphinOpen File Manager... Remaining space in your Home folder: %1 MiB The settings dialog main page name, as in 'general settings'General Warn when free space is below: Warns the user that the system is running low on space on his home folder, indicating the percentage and absolute MiB size remainingYour Home folder is running out of disk space, you have %1 MiB remaining (%2%) Project-Id-Version: freespacenotifier
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2014-11-05 18:40+0200
Last-Translator: liudas@aksioma.lt <liudas@aksioma.lt>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 1.5
  MiB Konfigūruoti įspėjimą... Slėpti Įjungti laisvos vietos įspėjimus Ar įjungti laisvos vietos pranešimai. Mažai vietos diske Minimalus laisvos vietos kiekis, po kurio naudotojas pradeda gauti pranešimus. Atverti failų tvarkyklę... Likusi vieta Jūsų „Namų aplanke“: %1 MB Bendri Įspėti, kai laisvos vietos diske yra mažiau nei: Jūsų „Namų aplanke“ mažėja vietos. Jums liko %1 MB (%2%) 