��          �   %   �      p     q     �     �     �     �     �     �     �     �     �     �     �                         &     8     F     Z     `     e     r     �     �  
   �     �  $  �     �  	   �  %   �       	                  #     3     G     P  	   \     f     n     u     ~     �     �      �     �     �     �               '     ?     O                                                                                      
                       	              Abbreviation for secondss Application: Average clock: %1 MHz Bar Buffers: CPU CPU %1 CPU monitor CPU%1: %2% @ %3 Mhz CPU: %1% Cached: Circular Colors General Memory Memory monitor Memory: %1/%2 MiB Monitor type: Set colors manually Show: Swap Swap monitor Swap: %1/%2 MiB System load Update interval: Used swap: User: Project-Id-Version: plasma_applet_systemloadviewer
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-29 03:31+0200
PO-Revision-Date: 2015-12-29 10:58+0200
Last-Translator: Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>
Language-Team: lt <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 1.5
 s Programa: Vidutinis laikrodžio dažnis: %1 MHz Juosta Buferiai: CPU CPU %1 CPU stebėtojas CPU%1: %2% @ %3 Mhz CPU: %1% Talpykloje: Žiedinis Spalvos Bendri Atmintis Atminties stebėtojas Atmintis: %1/%2 MB Stebėtojo tipas: Nustatyti spalvas rankiniu būdu Rodyti: Swap Mažas CPU/RAM/SWAP stebėtojas Swap: %1/%2 MB Sistemos apkrova Atnaujinimo intervalas: Naudojama swap: Naudotojas: 