��    K      t  e   �      `     a  ^   f  H   �          !     -     D     L  '   [     �  "   �     �     �     �  
   �     �          %  	   .     8     P     f     l          �  "   �     �     �  	   �     �     �  ?   	     D	     Q	     W	     e	     q	     �	     �	  ;   �	  &   �	      
  %   %
     K
     e
     
     �
  >   �
     �
       '   &  !   N     p     �     �     �     �     �     �     �          "     3     E     X     k     }     �     �     �     �     �     �  3     #  G     k     p     v     ~     �     �  	   �     �  %   �     �               $     )  
   ;     F     e     y  	   �     �     �     �     �     �     �     �     	                     )     +     =     K     T  
   d     o     {     �     �     �     �     �     �     �     �     �     �                    &  	   =     G     I     N     R     U     Y     _     e     i     k     n     r     v     y     {     }     �     �     �     �     �     '   I   8                            >   5   B           .   6   !      +       -   4       2         "       C         %       9       0   7   @      )         
   H       :         G              F   *      /   =               &                            D   #      A         K   ,       1      (   $          ?   J       <      3      	      ;       E                   min %1 is the weather condition, %2 is the temperature,  both come from the weather provider%1 %2 A weather station location and the weather service it comes from%1 (%2) Beaufort scale bft Celsius °C Degree, unit symbol° Details Fahrenheit °F Forecast period timeframe1 Day %1 Days Hectopascals hPa High & Low temperatureH: %1 L: %2 High temperatureHigh: %1 Inches of Mercury inHg Kelvin K Kilometers Kilometers per Hour km/h Kilopascals kPa Knots kt Location: Low temperatureLow: %1 Meters per Second m/s Miles Miles per Hour mph Millibars mbar N/A No weather stations found for '%1' Notices Percent, measure unit% Pressure: Search Short for no data available- Shown when you have not set a weather providerPlease Configure Temperature: Units Update every: Visibility: Weather Station Wind conditionCalm Wind speed: certain weather condition (probability percentage)%1 (%2%) content of water in airHumidity: %1%2 distance, unitVisibility: %1 %2 ground temperature, unitDewpoint: %1 humidex, unitHumidex: %1 pressure tendencyfalling pressure tendencyrising pressure tendencysteady pressure tendency, rising/falling/steadyPressure Tendency: %1 pressure, unitPressure: %1 %2 temperature, unit%1%2 visibility from distanceVisibility: %1 weather warningsWarnings Issued: weather watchesWatches Issued: wind directionE wind directionENE wind directionESE wind directionN wind directionNE wind directionNNE wind directionNNW wind directionNW wind directionS wind directionSE wind directionSSE wind directionSSW wind directionSW wind directionVR wind directionW wind directionWNW wind directionWSW wind direction, speed%1 %2 %3 wind speedCalm windchill, unitWindchill: %1 winds exceeding wind speed brieflyWind Gust: %1 %2 Project-Id-Version: plasma_applet_weather
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-28 05:50+0100
PO-Revision-Date: 2016-08-30 17:24+0200
Last-Translator: Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 2.0
  min %1 %2 %1 (%2) Boforto skalė, bft pagal Celsijų, °C ° Išsamiau pagal Farenheitą, °F %1 diena %1 dienos %1 dienų %1 diena hektopaskaliais, hPa A: %1 Ž: %2 Aukščiausia: %1 inHg pagal Kelviną, K kilometrai kilometrais per valandą, km/h kilopaskaliais, kPa mazgais Vietovė: Žemiausia: %1 metrais per sekundę, m/s mylios myliomis per valandą, mph millibarais, mbar ? Orų stočių „%1“ nerasta Pranešimai % Slėgis: Ieškoti - Sukonfigūruokite Temperatūra: Vienetai Atnaujinti kas: Matomumas: Orų stotis Ramu Vėjo greitis: %1 (%2%) Drėgnumas: %1%2 Matomumas: %1 %2 Rasos taškas: %1 Humidex: %1 krenta kyla pastovus Slėgio tendencija: %1 Slėgis: %1 %2 %1%2 Matomumas: %1 Įspėjimas pateiktas: Stebėta: R RŠR RPR Š ŠR ŠŠR ŠŠV ŠV P PR PPR PPV PV * V VŠV VPV %1 %2 %3 Ramus Vėjo šaltis: %1 Vėjo gūsiai: %1 %2 