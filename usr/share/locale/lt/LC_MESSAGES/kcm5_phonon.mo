��    B      ,  Y   <      �  i   �  m        y  b   �     �     	  6        O  7   _     �     �  	   �     �  (   �  )   �  )   (     R     o  Y   u     �     �  �   �      y	  .   �	     �	  
   �	     �	     �	     
     
     !
     5
     B
     J
     c
     r
     w
     �
     �
     �
     �
     �
  	   �
  
   �
     �
     �
  	     
     
   *     5     B  	   `     j     o  
   �  �   �     (  8   8  �   q     �  8   	  ,   B  ,   o  %   �     �    �  [   �  o   ;  #   �  f   �  %   6     \  :   k     �  <   �     �            &     -   A  /   o  0   �     �     �  Z        _     y  �   �  =     .   V     �     �     �     �     �     �     
     $     4  )   @     j     y  6   ~     �     �     �     �     �                3  "   C     f     u     �     �  %   �     �     �     �       �        �  @   �  �   �     �  =   �  3   �  4   !  ,   V  )   �     $   !   %          ;   ,          B                 8   #                 7   9       +              )   ?   -   <         6                                    *   2       >                1       @       .   5   3   :   A         &      /                4              0      "             =   (   '         	   
           @info User changed Phonon backendTo apply the backend change you will have to log out and back in again. A list of Phonon Backends found on your system.  The order here determines the order Phonon will use them in. Apply Device List To... Apply the currently shown device preference list to the following other audio playback categories: Audio Hardware Setup Audio Playback Audio Playback Device Preference for the '%1' Category Audio Recording Audio Recording Device Preference for the '%1' Category Backend Colin Guthrie Connector Copyright 2006 Matthias Kretz Default Audio Playback Device Preference Default Audio Recording Device Preference Default Video Recording Device Preference Default/Unspecified Category Defer Defines the default ordering of devices which can be overridden by individual categories. Device Configuration Device Preference Devices found on your system, suitable for the selected category.  Choose the device that you wish to be used by the applications. EMAIL OF TRANSLATORSYour emails Failed to set the selected audio output device Front Center Front Left Front Left of Center Front Right Front Right of Center Hardware Independent Devices Input Levels Invalid KDE Audio Hardware Setup Matthias Kretz Mono NAME OF TRANSLATORSYour names Phonon Configuration Module Playback (%1) Prefer Profile Rear Center Rear Left Rear Right Recording (%1) Show advanced devices Side Left Side Right Sound Card Sound Device Speaker Placement and Testing Subwoofer Test Test the selected device Testing %1 The order determines the preference of the devices. If for some reason the first device cannot be used Phonon will try to use the second, and so on. Unknown Channel Use the currently shown device list for more categories. Various categories of media use cases.  For each category, you may choose what device you prefer to be used by the Phonon applications. Video Recording Video Recording Device Preference for the '%1' Category  Your backend may not support audio recording Your backend may not support video recording no preference for the selected device prefer the selected device Project-Id-Version: kcm_phonon
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2013-01-05 16:11+0200
Last-Translator: Liudas Alisauskas <liudas@akmc.lt>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 1.5
 Kad pritaikyti galinės sąsajos pakeitimus, jūs turėsite atsijungti ir prisijungti vėl. Sąrašas Phonon galinių sąsajų rastų sistemoje. Eiliškumas nurodo tvarką kokia jie bus naudojami Phonon. Pritaikyti įrenginių sąrašą... Pritaikyti dabar rodomą įrenginių pirmenybės sąrašą šioms kitoms garso išvedimo kategorijoms: Garso aparatinės įrangos nustatymas Garso grojimas Garso grojimo įrenginių pirmenybė kategorijoje „%1“ Garso įrašymas Garso įrašymo įrenginių pirmenybė kategorijoje „%1“ Galinė sąsaja Colin Guthrie Jungtis Autorinės teisės 2006 Matthias Kretz Numatyta garso grojimo įrenginių pirmenybė Numatyta garso įrašymo įrenginių pirmenybė Numatyta vaizdo įrašymo įrenginių pirmenybė Numatyta/nenurodyta kategorija Neteikti pirmenybės Nustato numatytą tvarką įrenginių, kuri gali būti pakeista individualių kategorijų. Įrenginio konfigūracija Įrenginių pasirinkimai Jūsų sistemoje rasti įrenginiai, tinkami parinktai kategorijai. Pasirinkite įrenginį kuris turėtų būti naudojamas programose. Matasbbb@gmail.com, antanas.ursulis@gmail.com, liudas@akmc.lt Nepavyko nustatyti garso išvesties įrenginio Priekinis centrinis Priekinis kairysis Priekinis centro kairysis Priekinis dešinysis Priekinis centro dešinysis Aparatinė įranga Nepriklausomi įrenginiai Įėjimo lygiai Neteisingas KDE garso aparatinės įrangos nustatymas Matthias Kretz Mono Matas Brazdeikis, Antanas Uršulis, Liudas Ališauskas Phonon konfigūracijos modulis Grojimas (%1) Teikti pirmenybę Profilis Galinis centrinis Galinis kairysis Galinis dešinysis Įrašymas (%1) Rodyti sudėtingesnius įrenginius Šono kairysis Šono dešinysis Garso plokštė Garso įrenginys Garsiakalbio talpinimas ir tikrinimas Žemų dažnių generatorius Testuoti Patikrinti parinktą įrenginį Bandomas %1 Tvarka nulems įrenginių pirmenybę. Jei dėl kokių nors priežasčių neveiks pirmas įrenginys, Phonon bandys antrą ir t.t. Nežinomas kanalas Naudoti dabar rodomą įrenginių sąrašą kitoms kategorijoms. Įvairios išvedimo kategorijos skirtingiems panaudojimams. Kiekvienai kategorijai jūs galite pasirinkti įrenginį, kuris turėtų būti naudojamas Phonon programose. Vaizdo įrašymas Vaizdo įrašymo įrenginių pirmenybė kategorijoje „%1“ Jūsų parinktas variklis nepalaiko garso įrašymo Jūsų parinktas variklis nepalaiko vaizdo įrašymo neteikti pirmenybės pasirinktam įrenginiui teikti pirmenybę pasirinktam įrenginiui 