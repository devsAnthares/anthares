��          �   %   �      P     Q     b     w     �     �     �  
   �     �     �     �     �     �  /        3     Q     p     v     �     �     �     �     �     �     �         %  '   ,  /   T     �     �  
   �     �  	   �     �  	   �  	   �     �     �  3        A     ]     y     �     �     �     �     �     �     �          )                            
                                                                                             	    %1 file %1 files %1 folder %1 folders %1 of %2 processed %1 of %2 processed at %3/s %1 processed %1 processed at %2/s Appearance Behavior Cancel Clear Configure... Finished Jobs List of running file transfers/jobs (kuiserver) Move them to a different list Move them to a different list. Pause Remove them Remove them. Resume Show all jobs in a list Show all jobs in a list. Show all jobs in a tree Show all jobs in a tree. Show separate windows Show separate windows. Project-Id-Version: kuiserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2011-01-08 21:21+0200
Last-Translator: Tomas Straupis <tomasstraupis@gmail.com>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 1.1
 %1 failas %1 failai %1 failų %1 failas %1 aplankas %1 aplankai %1 aplankų %1 aplankas %1iš %2 atlikta %1 iš %2 atlikta %3/s %1 atlikta %1 atlikta  %2/s Išvaizda Elgsena Atsisakau Išvalyti Konfigūruoti... Baigti darbai Veikiančių siuntimų/darbų sąrašas (kuiserver) Perkelti į kitą sąrašą Perkelti į kitą sąrašą Pauzė Pašalinti. Pašalinti. Tęsti Rodyti visus darbus sąraše Rodyti visus darbus sąraše. Rodyti visus darbus medyje Rodyti visus darbus medyje. Rodyti atskirtus langus Rodyti atskirtus langus. 