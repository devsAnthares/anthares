��          �   %   �      `     a  
   g  /   r  -   �     �     �  
   �     �     
        W   )     �  ,   �  	   �     �     �     �     �     �  
   �               5     I  1   ^     �    �     �  
   �     �     �     �     �  	   
  	             5  O   A     �  7   �  	   �     �  	                  !  
   0     ;  "   T     w  #   �     �     �                     
                                                                         	                              %1@%2 %2@%3 (%1) @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Favorites All Applications Appearance Applications Applications updated. Computer Drag tabs between the boxes to show/hide them, or reorder the visible tabs by dragging. Edit Applications... Expand search to bookmarks, files and emails Favorites Hidden Tabs History Icon: Leave Menu Buttons Often Used Remove from Favorites Show applications by name Sort alphabetically Switch tabs on hover Type is a verb here, not a nounType to search... Visible Tabs Project-Id-Version: l 10n
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2017-06-25 01:27+0200
Last-Translator: Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 2.0
 %1@%2 %2@%3 (%1) Pasirinkti... Atkurti ženkliuką Įtraukti į žymeles Visos programos Išvaizda Programos Programos atnaujintos. Kompiuteris Nutempkite langelius prie rodomų/slepiamųjų arba pakeiskite jų eiliškumą. Keisti programų meniu... Į paiešką įtraukti žymeles, failus ir el. laiškus Žymelės Paslėptos kortelės Žurnalas Ženkliukas: Išeiti Meniu mygtukai Dažniausi Pašalinti iš žymelių Programas rodyti pagal pavadinimą Rikiuoti pagal abėcėlę Kortelę perjungti ant jos užvedus Įveskite užklausą... Matomos kortelės 