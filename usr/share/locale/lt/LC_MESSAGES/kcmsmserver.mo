��          �      <      �     �     �     �  7  �  �  #  +   �  ^              �     �  x   �  �   %     �               ;     U    r     v	     �	     �	  o  �	    8  .   G  |   v     �          1  �   8  �   �     �     �  -   �  &        8                          
       	                                                                    &End current session &Restart computer &Turn off computer <h1>Session Manager</h1> You can configure the session manager here. This includes options such as whether or not the session exit (logout) should be confirmed, whether the session should be restored again when logging in and whether the computer should be automatically shut down after session exit by default. <ul>
<li><b>Restore previous session:</b> Will save all applications running on exit and restore them when they next start up</li>
<li><b>Restore manually saved session: </b> Allows the session to be saved at any time via "Save Session" in the K-Menu. This means the currently started applications will reappear when they next start up.</li>
<li><b>Start with an empty session:</b> Do not save anything. Will come up with an empty desktop on next start.</li>
</ul> Applications to be e&xcluded from sessions: Check this option if you want the session manager to display a logout confirmation dialog box. Conf&irm logout Default Leave Option General Here you can choose what should happen by default when you log out. This only has meaning, if you logged in through KDM. Here you can enter a colon or comma separated list of applications that should not be saved in sessions, and therefore will not be started when restoring a session. For example 'xterm:konsole' or 'xterm,konsole'. O&ffer shutdown options On Login Restore &manually saved session Restore &previous session Start with an empty &session Project-Id-Version: kcmsmserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2015-01-28 06:57+0200
Last-Translator: Liudas Ališauskas <liudas@akmc.lt>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 1.5
 &Baigti dabartinę sesiją Pa&leisti iš naujo kompiuterį &Išjungti kompiuterį <h1>Sesijų tvarkyklė</h1>Čia Jūs galite konfigūruoti sesijų tvarkyklę. Galite nustatyti, ar reikia patvirtinti sesijos pabaigą (išsiregistravimą), ir ar turi būti praėjusi sesija išsaugoma išsiregistruojant ir atstatoma registruojantis iš naujo kitaip nenurodžius, ir ar kompiuteris turi būti automatiškai išjungiamas numatytu būdu baigus sesiją. <ul>
<li><b>Atstatyti prieš tai buvusią sesiją:</b> Išsaugos programų nustatymus išjungimo metu ir atstatys jas kitą kartą įjungus kompiuterį</li>
<li><b>Atstatyti rankiniu būdu išsaugotą sesiją: </b> Leidžia išsaugoti sesiją bet kuriuo metu pasirinkus „Išsaugoti sesiją“ įrašą K-Meniu. Tai reiškia, kad išsaugota sesija iš naujo pasirodys kito kompiuterio paleidimo metu.</li>
<li><b>Pradėti tuščią sesiją:</b> Neišsaugoti nieko. Įjungus kompiuterį jokia sesija nebus tęsiama.</li>
</ul> Programos, kurių &nereikia paleisti sesijose: Pažymėkite šią parinktį, jei Jūs norite, kad sesijų tvarkyklė rodytų išsiregistravimo patvirtinimo dialogo langą. Patvirtinti išsi&registravimą Numatyta išjungimo parinktis Bendri Čia Jūs galite pasirinkti, kas turi įvykti kitaip nenurodžius po išsiregistravimo.  Tai turi prasmę tik tuo atveju, jei Jūs įsiregistravote KDM pagalba. Čia galite įrašyti programas, atskirdami jas dvitaškiu arba kableliu, kurių nereikia išsaugoti sesijose, ir todėl jos nebus paleistos atstatant sesiją. Pavyzdžiui, „xterm:konsole“ arba „xterm,konsole“. &Siūlyti išjungimo parinktis Registruojantis Atstatyti &rankiniu būdu išsaugotą sesiją Atstatyti &prieš tai buvusią sesiją Pradėti &tuščią sesiją 