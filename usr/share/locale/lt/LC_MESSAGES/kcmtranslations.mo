��          �      ,      �  5   �  +   �     �               4      R     s     �  	   �  
   �     �     �     �       7       K     d  9   l     �     �     �     �               -  	   G     Q     b     u     �     �  F   �                                         	                    
                 %1 is language name, %2 is language code name%1 (%2) <p>Click here to install more languages</p> Applying Language Settings Available &Translations: Available Languages: Configure Plasma translations EMAIL OF TRANSLATORSYour emails Install more languages Install more translations John Layt Maintainer NAME OF TRANSLATORSYour names Preferred Languages: Preferred Trans&lations: Translations Your changes will take effect the next time you log in. Project-Id-Version: trunk-kf 5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-08-30 16:55+0200
Last-Translator: Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 2.0
 %1 (%2) <p>Spauskite čia, jei norite įdiegti daugiau kalbų</p> Pritaikomi kalbos nustatymai Prieinami &vertimai: Galimos kalbos: Konfigūruoti Plasma vertimus liudas@akmc.lt Įdiegti daugiau kalbų Įdiegti daugiau vertimų John Layt Prižiūrėtojas Liudas Ališauskas Pageidautinos kalbos: Pageidaujami vertimai: Vertimai Jūsų padaryti pasikeitimai įsigalios kitą kartą Jums prisijungus. 