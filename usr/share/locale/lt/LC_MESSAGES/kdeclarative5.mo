��          t      �         ~     +   �     �     �     �  �   �  g   �  0   �     #  @   3    t  �   �  B        ^     d     x  �   �  �   T  8   �          +                                       
       	              Click on the button, then enter the shortcut like you would in the program.
Example for Ctrl+A: hold the Ctrl key and press A. Conflict with Standard Application Shortcut No shortcut definedNone Reassign Reserved Shortcut The '%1' key combination is also used for the standard action "%2" that some applications use.
Do you really want to use it as a global shortcut as well? The F12 key is reserved on Windows, so cannot be used for a global shortcut.
Please choose another one. The key you just pressed is not supported by Qt. Unsupported Key What the user inputs now will be taken as the new shortcutInput Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-30 03:07+0100
PO-Revision-Date: 2015-12-30 18:08+0200
Last-Translator: Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>
Language-Team: lt <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 1.5
 Spauskite mygtuką, tada įveskite klavišų derinį taip, tarsi būtumėte programoje.
Pavyzdžiui kombinacijai Vald+A: laikykite Vald ir spauskite A. Konfliktas su standartiniu programos iššaukimo klavišų deriniu Jokio Priskirti iš naujo Rezervuotas klavišų derinys Klavišų derinys „%1“ jau priskirtas standartiniam veiksmui „%2“, kurį naudoja kai kurios programos.
Ar tikrai norite jį naudoti ir kaip globalųjį spartųjį klavišų derinį? F12 klavišas yra rezervuotas Windows sistemoje, taigi negali būti naudojamas trumpuoju klavišų deriniu.
Prašome pasirinkti kitą. Klavišas, kurį nuspaudėte, nepalaikomas Qt sistemoje. Nepalaikomas klavišas Įvestis 