��    %      D  5   l      @  Z   A  *   �     �     �     �          /     I     O     d     l      �     �     �     �     �          -     F     _     x  !   �     �     �     �     �          6     M     e     z  (   �     �  >   �  &     &   2    Y     ]	  -   e	     �	  !   �	     �	     �	  $   
     ,
     9
     P
  "   \
     
     �
  #   �
  #   �
       "   !     D     b     ~     �  %   �     �  "   �  0     -   P  !   ~     �     �     �     �  &        +  Z   F  (   �  /   �                         #   $                                      	      
                 !                                    %                         "                             %1 is 'the slot asked for foo arguments', %2 is 'but there are only bar available'%1, %2. %1 is not a function and cannot be called. %1 is not an Object type '%1' is not a valid QLayout. '%1' is not a valid QWidget. Action takes 2 args. ActionGroup takes 2 args. Alert Call to '%1' failed. Confirm Could not construct value Could not create temporary file. Could not open file '%1' Could not open file '%1': %2 Could not read file '%1' Failed to create Action. Failed to create ActionGroup. Failed to create Layout. Failed to create Widget. Failed to load file '%1' File %1 not found. First argument must be a QObject. Incorrect number of arguments. Must supply a filename. Must supply a layout name. Must supply a valid parent. Must supply a widget name. No classname specified No classname specified. No such method '%1'. Not enough arguments. There was an error reading the file '%1' Wrong object type. but there is only %1 available but there are only %1 available include only takes 1 argument, not %1. library only takes 1 argument, not %1. Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2014-03-16 20:46+0200
Last-Translator: Liudas Ališauskas <liudas@aksioma.lt>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 1.5
 %1, %2. %1 nėra funkcija ir jos negalima iškviesti. %1 nėra Objekto tipas „%1“ nėra teisingas QLayout. „%1“ yra tikras QWidget. Veiksmui reikia 2 parametrų. Veiksmų grupei reikia 2 parametrų. Perspėjimas %1 kvietimas nepavyko. Patvirtinti Nepavyksta sukonstruoti reikšmės Nepavyko sukurti laikino failo. Nepavyko atverti failo „%1“ Nepavyko atverti failo „%1“: %2 Nepavyko perskaityti failo „%1“ Nepavyko sukurti veiksmo. Nepavyko sukurti veiksmų grupės. Nepavyko sukurti išdėstymo. Nepavyko sukurti valdiklio. Nepavyko įkelti failo „%1“ Nepavyko rasti failo %1. Pirmas parametras turi būti QObject. Neteisingas parametrų kiekis. Turite pateikti failo pavadinimą. Turite pateikti išdėstymo(layout) pavadinimą. Turite nurodyti tikrą pagrindinį elementą. Turite pateikti valdiklio vardą. Nenurodytas klasės vardas Nenurodytas klasės vardas. Nėra metodo „%1“. Nepakanka parametrų. Įvyko klaida skaitant failą „%1“ Neteisingas objekto tipas. bet yra tik %1 galimas bet yra tik %1 galimi bet yra tik %1 galimų bet yra tik %1 galimas intarpui reikia tik 1 argumento, o ne %1 bibliotekai reikia tik vieno argumento, o ne %1 