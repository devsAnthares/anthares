��          �   %   �      p     q  +   �  .   �  6   �  *     #   D  &   h  /   �  2   �  :   �  0   -  3   ^  ;   �  K   �  -     $   H  '   m     �     �     �     �     �  #        1     O     c     |    �     �  !   �  $   �  *   �  #   #	     G	     _	  #   {	  %   �	  -   �	  (   �	  .   
  0   K
  V   |
  "   �
     �
          +     :     K     \     l     }     �     �     �     �                                                    	   
                                                                    @action:buttonCommit @info:statusAdded files to SVN repository. @info:statusAdding files to SVN repository... @info:statusAdding of files to SVN repository failed. @info:statusCommit of SVN changes failed. @info:statusCommitted SVN changes. @info:statusCommitting SVN changes... @info:statusRemoved files from SVN repository. @info:statusRemoving files from SVN repository... @info:statusRemoving of files from SVN repository failed. @info:statusReverted files from SVN repository. @info:statusReverting files from SVN repository... @info:statusReverting of files from SVN repository failed. @info:statusSVN status update failed. Disabling Option "Show SVN Updates". @info:statusUpdate of SVN repository failed. @info:statusUpdated SVN repository. @info:statusUpdating SVN repository... @item:inmenuSVN Add @item:inmenuSVN Commit... @item:inmenuSVN Delete @item:inmenuSVN Revert @item:inmenuSVN Update @item:inmenuShow Local SVN Changes @item:inmenuShow SVN Updates @labelDescription: @title:windowSVN Commit Show updates Project-Id-Version: fileviewsvnplugin
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2016-11-05 08:12+0200
Last-Translator: Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 1.5
 Įkelti Failai į SVN saugyklą pridėti. Į SVN saugyklą pridedami failai... Nepavyko pridėti failų į SVN saugyklą. Nepavyko įkelti pakeitimų į SVN. SVN pakeitimai įkelti. Įkeliami SVN pakeitimai... Failai iš SVN saugyklos pašalinti Iš SVN saugyklos šalinami failai... Iš SVN saugyklos failų pašalinti nepavyko. Failai, įkelti į saugyklą, atšaukti. Atšaukiami į SVN saugyklą įkelti failai... Nepavyko atšaukti į saugyklą įkeltų failų. Nepavyko atnaujinti SVN būsenos. Parinktis „rodyti SVN atnaujinimus“ išjungiama. SVN saugyklos atnaujinti nepavyko. SVN saugykla atnaujinta. Atnaujinama SVN saugykla... SVN - pridėti SVN - įkelti... SVN - pašalinti SVN - atšaukti SVN - atnaujinti Rodyti vietinius SVN pakeitimus Rodyti SVN atnaujinimus Aprašymas: SVN pakeitimų įkėlimas Rodyti atnaujinimus 