��          �      l      �     �     �     �  *         +  >   ?  9   ~     �     �  
   �      �       	        (  !   :     \  $   {  	   �     �  �   �  $  8     ]     _       (   �  )   �  D   �  5   0     f     �     �  '   �  	   �     �     �  #     "   2  K   U  	   �     �  v   �                              
                                                  	                  % &Critical level: &Low level: <b>Battery Levels                     </b> A&t critical level: Battery will be considered critical when it reaches this level Battery will be considered low when it reaches this level Configure Notifications... Critical battery level Do nothing EMAIL OF TRANSLATORSYour emails Enabled Hibernate Low battery level Low level for peripheral devices: NAME OF TRANSLATORSYour names Pause media players when suspending: Shut down Suspend The Power Management Service appears not to be running.
This can be solved by starting or scheduling it inside "Startup and Shutdown" Project-Id-Version: powerdevilglobalconfig
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-11 03:05+0200
PO-Revision-Date: 2017-06-25 01:48+0200
Last-Translator: Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 2.0
 % &Kritinis akumuliatorius lygis: &Žemas įkrovos lygmuo: <b>Akumuliatoriaus įkrovos lygmenys</b> Akumuliatoriaus lygis yra kritinis &ties: Akumuliatoriaus lygis bus laikomas kritiniu, kai pasiekia šį lygį Akumuliatoriaus įkrova bus laikoma žema kai pasieks Konfigūruoti pranešimus... Kritinis akumuliatorius lygis Nieko nedaryti tomasstraupis@gmail.com, liudas@akmc.lt Įgalinta Sustabdyti į diską Žemas įkrovos lygmuo Žemo lygio išoriniai įrenginiai: Tomas Straupis, Liudas Ališauskas Sustabdant kompiuterį į RAM, pristabdyti įvairialypes terpės leistuves: Išjungti Sustabdyti į RAM Energijos valdymo tarnyba atrodo neveikia.
Ši problema gali būti išspręsta „Paleidimo ir išjungimo“ modulyje. 