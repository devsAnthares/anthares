��            )   �      �     �     �     �     �     �          $     :     P  3   f     �     �     �     �     �     �            #   6     Z     s  S   �     �     �     �     �       
                 )     2     ;     L  
   X  	   c  
   m     x  
   �  $   �     �  	   �  	   �  	   �     �     �  	   �     �  
             -  P   5     �     �     �     �  	   �     �     �               
                                 	                                                                                         @action:buttonClone @action:buttonClose @action:buttonCommit @action:buttonExport @action:buttonImport @action:buttonOptions @action:buttonRename @action:buttonSelect @action:buttonUpdate @action:inmenu<application>Mercurial</application> @buttonBrowse @labelOptions @labelPort @labelSource @label:buttonMerge @label:groupGeneral Settings @label:groupOptions @label:groupPlugin Settings @label:label to source fileSource  @label:renameRename to  @lobelDestination A KDE text-editor component could not be found;
please check your KDE installation. Copy Message Create New Tag Error! Filename Options Remove Tag Tag Project-Id-Version: l 10n
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-01 02:48+0200
PO-Revision-Date: 2015-12-29 21:14+0200
Last-Translator: Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>
Language-Team: lt <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 1.5
 Klonuoti Užverti Įkelti (commit) Eksportuoti Importuoti Parinktys Pervadinti Žymėti Atnaujinti <application>Mercurial</application> Naršyti Parinktys Prievadas Šaltinis Sulieti Bendri nustatymai Parinktys Papildinio nustatymai Šaltinis  Pervadinti  į  Tikslas Nerastas KDE tekstų redaktoriaus komponentas.
Patikrinkite savo KDE įdiegimą. Kopijuoti laišką Sukurti naują žymę Klaida! Failo pavadinimas Parinktys Pašalinti žymą Žymė 