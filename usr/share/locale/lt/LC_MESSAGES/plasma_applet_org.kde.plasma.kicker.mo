��    Y      �     �      �  C   �     �     �          #     B     S     s     �     �  
   �     �     �     �     �  	   �     �     �     		  ,   	  	   B	     L	  
   k	     v	     �	     �	     �	     �	     �	     �	     
     	
  	   )
     3
     ;
     L
     Q
     ]
     d
  	   w
     �
     �
  
   �
  
   �
     �
     �
     �
  
   �
     �
               %     6     D     R     d     z     �     �     �     �     �  	   �     �     �               4     Q     j     �     �     �     �  	   �     �  ,   �          !     0     @     L     S     b     t     �  #   �     �  9  �               4  !   J  '   l     �     �  	   �     �     �     �     �  	   �     �       
         +     ?     S  5   b  	   �      �     �     �     �          #     7     K  *   `     �     �     �     �     �  
   �     �  
   �     �               6     L     b  &   n     �     �     �     �     �     �     �               3     K     d     �  )   �     �     �     �     �     �       "   
     -  &   F  &   m  '   �     �     �     �       	        &  2   @     s     �     �     �     �     �     �     �  *     !   ,  
   N         K   
      S   0           #               D                     '       ;   P   /   U       <   %          O   =   W           "   Y         ,   9       ?   E      *                  :      )   I   C       L   3   1   Q   -   V   8   R   @   !       H   >       N   B   G          .             4             (          M   X       A           6   5                    $   7   F          J          2          +                   	                 &               T       @action opens a software center with the applicationManage '%1'... Add to Desktop Add to Favorites Add to Panel (Widget) Align search results to bottom All Applications App name (Generic name)%1 (%2) Applications Apps & Docs Behavior Categories Computer Contacts Description (Name) Description only Documents Edit Application... Edit Applications... End session Expand search to bookmarks, files and emails Favorites Flatten menu to a single level Forget All Forget All Applications Forget All Contacts Forget All Documents Forget Application Forget Contact Forget Document Forget Recent Documents General Generic name (App name)%1 (%2) Hibernate Hide %1 Hide Application Lock Lock screen Logout Name (Description) Name only Often Used Applications Often Used Documents Often used Open with: Pin to Task Manager Places Power / Session Properties Reboot Recent Applications Recent Contacts Recent Documents Recently Used Recently used Removable Storage Remove from Favorites Restart computer Run Command... Run a command or a search query Save Session Search Search results Search... Searching for '%1' Session Show Contact Information... Show applications as: Show often used applications Show often used contacts Show often used documents Show recent applications Show recent contacts Show recent documents Show: Shut Down Sort alphabetically Start a parallel session as a different user Suspend Suspend to RAM Suspend to disk Switch User System System actions Turn off computer Type to search. Unhide Applications in '%1' Unhide Applications in this Submenu Widgets Project-Id-Version: KDE Frameworks 5 Applications
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2017-06-25 01:26+0200
Last-Translator: Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
language/lt/)
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 2.0
 Tvarkyti „%1“... Įtraukti į darbalaukį Įtraukti į žymeles Įtraukti į skydelį (valdiklį) Paieškos rezultatus pateikti apačioje Visos programos %1 (%2) Programos Programos ir dokumentai Elgesys Kategorijos Kompiuteris Kontaktai Aprašymas (pavadinimas) TIk aprašymas Dokumentai Keisti programą... Keisti programas... Baigti sesiją Į paiešką įtraukti žymeles, failus ir el.paštą Žymelės Sumažinti meniu iki vieno lygio Viską pamiršti Pamiršti visas programas Pamiršti visus kontaktus Pamiršti visus dokumentus Pamiršti programą Pamiršti kontaktą Pamiršti dokumentą Išvalyti paskiausių dokumentų sąrašą Bendra %1 (%2) Sustabdyti į diską Slėpti: %1 Slėpti programą Užrakinti Užrakinti ekraną Atsijungti Pavadinimas (aprašymas) Tik pavadinimas Dažniausios programos Dažniausi dokumentai Dažniausiai naudotus Atverti su: Prisegti prie užduočių tvarkytuvės Vietos Maitinimas / sesija Savybės Paleisti iš naujo Paskiausios programos Paskiausi kontaktai Paskiausi dokumentai Paskiausiai naudota Paskiausiai naudota Keičiamosios laikmenos Pašalinti iš žymelių Iš naujo paleisti kompiuterį Vykdyti komandą... Vykdyti komandą arba ieškoti užklausos Išsaugoti sesiją Paieška Radinys Ieškoti... Ieškoma „%1“ Sesija Rodyti kontaktinę informaciją... Programų rodymo būdas: Rodyti dažniausiai atvertas programas Rodyti dažniausiai naudotus kontaktus Rodyti dažniausiai atvertus dokumentus Rodyti paskiausias programas Rodyti paskiausius kontaktus Rodyti paskiausius dokumentus Rodyti: Išjungti Rikiuoti pagal abėcėlę Prisijungti kitu naudotoju lygiagrečioje sesijoje Sustabdyti į RAM Sustabdyti į RAM Sustabdyti į diską Pakeisti naudotoją Sistema Sistemos veiksmai Išjungti kompiuterį Paieškos tipas. Nebeslėpti „%1“ kategorijos programų Rodyti visus kategorijos įrašus Valdikliai 