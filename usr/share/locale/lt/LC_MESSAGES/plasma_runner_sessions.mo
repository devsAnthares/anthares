��          �   %   �      P  �  Q          )  6   9  -   p     �     �     �     �  (   �  d        x     �     �     �     �     �     �                7  "   X     {     �     �    �  t  �     S
     f
  =   y
  6   �
     �
      �
          ;  %   Q  e   w     �     �     �  
     
   $  
   /  
   :     E     R     e     x     �     �     �                               	                                                                      
                        <p>You have chosen to open another desktop session.<br />The current session will be hidden and a new login screen will be displayed.<br />An F-key is assigned to each session; F%1 is usually assigned to the first session, F%2 to the second session and so on. You can switch between sessions by pressing Ctrl, Alt and the appropriate F-key at the same time. Additionally, the KDE Panel and Desktop menus have actions for switching between sessions.</p> Lists all sessions Lock the screen Locks the current sessions and starts the screen saver Logs out, exiting the current desktop session New Session Reboots the computer Restart the computer Shutdown the computer Starts a new session as a different user Switches to the active session for the user :q:, or lists all active sessions if :q: is not provided Turns off the computer User sessionssessions Warning - New Session lock screen commandlock log out log out commandLogout log out commandlogout new session restart computer commandreboot restart computer commandrestart shutdown computer commandshutdown switch user switch user commandswitch switch user commandswitch :q: Project-Id-Version: plasma_runner_sessions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2015-12-30 18:08+0200
Last-Translator: Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>
Language-Team: lt <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 1.5
 <p>Jūs pasirinkote atverti kitą ekrano sesiją.<br />Sena sesija bus paslėpta, ir naujas prisijungimo langas bus parodytas.<br /> F mygtukas yra susiejamas su sesija; F%1 dažniausia susiejamas su pirma sesija, F%2 su antra ir t.t. Jūs galite perjungti sesijas spausdami Vald Alt ir F mygtuką vienu metu. KDE skydelis ir darbalaukis turi mygtukus keisti sesijoms.</p> Rodo visas sesijas Užrakinti ekraną Užrakina dabartines sesijas ir paleidžia ekrano užsklandą Atsijungia, išjungiant dabartinę darbalaukio sesiją Nauja sesija Paleidžia kompiuterį iš naujo Paleisti kompiuterį iš naujo Išjungti kompiuterį Pradeda naują kito naudotojo sesiją Persijungia į aktyvią naudotojo :q: sesiją, arba parodo visas aktyvias sesijas, jei :q: nenurodyta Išjungia kompiuterį sesijos Perspėjimas – nauja sesija užrakinti atsijungti Atsijungti atsijungti nauja sesija paleisti iš naujo paleisti iš naujo išjungti kompiuterį sukeisti naudotojus sukeisti sukeisti :q: 