��          �   %   �      @     A  �   `  m   �  �   P  )   �  `        o     |     �     �     �      �     �     �  '        ,     >     ]     b     s  ?   �  Y   �  +     H   F    �     �  �   �  p   H	     �	  #   �	  �   �	     }
     �
     �
  
   �
  /   �
              $   1  ,   V     �  #   �     �     �     �  ;   �  ^   6  -   �  J   �                                                 	                                                               
              (c) 2003-2007 Fredrik Höglund <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) @item:inlistbox sizeResolution dependent A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Cursor Theme Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Fredrik Höglund Get new Theme Get new color schemes from the Internet Install from File NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. You have to restart the Plasma session for these changes to take effect. Project-Id-Version: kcminput
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2017-06-25 01:18+0200
Last-Translator: Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 2.0
 (c) 2003-2007 Fredrik Höglund <qt>Ar tikrai norite pašalinti <i>%1</i> žymeklių apipavidalinimą?<br />Bus ištrinti visi kartu su tema įdiegti failai.</qt> <qt>Negalima ištrinti šiuo metu naudojamos schemos.<br />Visų pirma turite persijungti į kitą schemą.</qt> (Galimi dydžiai: %1) Priklausomai nuo skiriamosios gebos Apipavidalinimas, pavadinta %1, jau egzistuoja Jūsų ženkliukų apipavidalinimų aplanke. Ar norite jį pakeisti šiuo apipavidalinimu? Patvirtinimas Žymeklio nustatymai pakeisti Žymeklių apipavidalinimas Aprašymas Nutempkite arba įrašykite apipavidalinimo URL dgvirtual@akl.lt,liudas@akmc.lt Fredrik Höglund Parsisiųsti naują apipavidalinimą Gaukite naujų spalvų schemų iš interneto Įdiegti iš failo Donatas Glodenis,Liudas Ališauskas Vardas Perrašyti apipavidalinimą? Pašalinti apipavidalinimą Failas %1 atrodo nėra teisingas žymeklių temos archyvas. Nepavyksta atsisiųsti žymeklių temos archyvo; prašome patikrinti, ar teisingas %1 adresas. Nepavyksta rasti žymeklių temos archyvo %1. Jei norite, kad pakeitimai įsigaliotų, turite iš naujo paleisti Plasma. 