��          �      �           	          &     -     4  
   =     H     Q     Y     i  >   w     �     �     �     �  
   �     �     �     �            U   %    {  	   �     �  
   �  	   �  	   �  	   �     �     �     �     �  o   �     d     u     ~     �     �     �  	   �     �     �     �  t   �                       	                                                
                                 %1 is running %1 not running &Reset &Start Advanced Appearance Command: Display Execute command Notifications Remaining time left: %1 second Remaining time left: %1 seconds Run command S&top Show notification Show seconds Show title Text: Timer Timer finished Timer is running Title: Use mouse wheel to change digits or choose from predefined timers in the context menu Project-Id-Version: plasma_applet_timer
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2015-08-25 15:06+0200
Last-Translator: Liudas Ališauskas <liudas@akmc.lt>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 2.0
 %1 veikia %1 neveikia &Iš naujo &Pradėti Išsamiau Išvaizda Komanda: Rodymas Vykdyti komandą Pranešimai Likęs laikas: %1 sekundė Likęs laikas: %1 sekundės Likęs laikas: %1 sekundžių Likęs laikas: %1 sekundė Vykdyti komandą Stabdyti Rodyti pranešimą Rodyti sekundes Rodyti pavadinimą Tekstas: Laikmatis Laikas pasibaigė Laikmatis veikia Pavadinimas: Naudokite pelės ratuką, kad pakeisti skaičius arba pasirinkti iš jau nustatytų laikmačių kontekstiniame meniu 