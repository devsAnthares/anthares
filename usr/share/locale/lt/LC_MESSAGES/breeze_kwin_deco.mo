��    <      �  S   �      (     )     +  !   G  "   i  &   �  ,   �  #   �  &     !   +  &   M  '   t  "   �  "   �  '   �     
  +     
   :     E     S     `     g     {     �     �     �     �  !   �     �      �               5     D     L  !   b     �  	   �     �     �     �     �  &   �     	     /	     6	     Q	     W	     _	     d	     v	  $   ~	     �	     �	     �	     �	     �	     
     
  >   7
    v
     y     {     �     �     �     �     �     �     �     �     �     	               &  5   /  
   e     p     �     �     �     �     �     �     �     �  -   �     &  #   -     Q  !   e     �     �     �  #   �     �     �     �  "        +     I  ,   a  "   �  
   �      �  
   �     �     �     �       $   "     G      `     �     �     �     �  "   �     �         "      0   )   %            &      
   <         5   2       /             *                                  :   9                 ,             .                       	   3           1                       #   6          +       7   8      4   $   (   !      '       ;                 -       % &Matching window property:  @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Border @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large @item:inlistbox Button size:Large @item:inlistbox Button size:Small @item:inlistbox Button size:Very Large Add Add handle to resize windows with no border Animations B&utton size: Border size: Center Center (Full Width) Class:  Color: Decoration Options Detect Window Properties Dialog Draw a circle around close button Edit Edit Exception - Breeze Settings Enable animations Enable/disable this exception Exception Type General Hide window title bar Information about Selected Window Left Move Down Move Up New Exception - Breeze Settings Question - Breeze Settings Regular Expression Regular Expression syntax is incorrect Regular expression &to match:  Remove Remove selected exception? Right Shadows Tiny Tit&le alignment: Title:  Use window class (whole application) Use window title Warning - Breeze Settings Window Class Name Window Identification Window Property Selection Window Title Window-Specific Overrides strength of the shadow (from transparent to opaque)S&trength: Project-Id-Version: trunk-kf 5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-09 03:20+0100
PO-Revision-Date: 2015-08-25 14:23+0200
Last-Translator: Liudas Ališauskas <liudas@akmc.lt>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 2.0
 % &Atitinkanti lango savybė:  Milžiniška Didelė Be rėmelio Be krašto rėmelio Normali Per didelė Smulki Labai milžiniška Labai didelė Didelis Mažas Labai didelis Pridėti Pridėti rankenėlę keisti langų be rėmų dydžiui Animacijos Mygtukų dydis: Kraštinės plotis: Centre Vidurys (pilnas plotis) Klasė:  Spalva: Išvaizdos parinktys Aptikti lango savybes Dialogas Rodyti apskritimą aplink išjungimo mygtuką Keisti Keisti išimtį - Breeze nustatymai Įjungti animacijas Įjungti/išjungti šią išimtį Išimtinės tipas Bendra Slėpti lango antraštę Informacija apie pasirinktą langą Kairėje Perkelti žemyn Perkelti aukštyn Nauja išimtis - Breeze nustatymai Klausimas - Breeze nustatymai Reguliarusis reiškinys Neteisinga reguliarios išraiškos sintaksė Atitinkanti reguliari išraiška:  Pašalinti Pašalinti pažymėtą išimtį? Dešinėje Šešėliai Smulki Antraščių &lygiavimas: Antraštė:  Naudoti lango klasę (visa programa) Naudoti lango antraštę Perspėjimas - Breeze nustatymai Lango klasė Lango atpažinimas Lango savybių parinkimas: Lango pavadinimas Nuo lango priklausantys pakeitimai S&tiprumas: 