��            )   �      �  &   �     �     �     �     �      �               .     6  ,   S  3   �     �     �     �     �     �  '        4     S     Y     o     �     �     �     �     �  &   �     �  &  �     $  .   3     b     x     �     �     �  ;   �       8   #  `   \  <   �  B   �  2   =	  
   p	  	   {	     �	  A   �	     �	     �	  7   
  &   ?
  
   f
  2   q
     �
  1   �
     �
  P   �
     K                                       
                      	                                                                            @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2013-01-31 01:08+0400
Last-Translator: Davit <nikdavnik@mail.ru>
Language-Team: Armenian Language: hy
Language: hy
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.4
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Գլխավոր Ստեղծել նոր հրահանգաշար։ Ավելացնել... Չեղյալ համարե՞լ Մեկնաբանություն: nikdavnik@mail.ru Ճշտում Փոփոխել ընտրված հրահանգաշարերը. Փոփոխել... Կատարել ընտրված հրահանգաշարը։ Հնարավոր չէ ստեղծել հրահանգաշար թարգմանչի համար «%1» Հնարավոր չէ որոշել թարքմանիչ «%1» Հնարավոր չէ ներբեռնել թարքմանիչ «%1» Հնարավոր չէ բացել նիշքը «%1» Նիշք։ Նշան: Թարգմանիչ: Ruby թարգնամչի անվտանգության աստճանը Դավիթ Նիկողոսյան Անուն: Ֆունկցիա «%1» գոյություն չունի Անհայտ թարգմանիչ «%1» Ջնջել Ջնջել ընտրված հրահանգաշար։ Կատարել «%1» նիշքը հայտնաբերված չէ։ Կանգնեցնել Կանգնեցնել ընտրված հրահանգաշարի կատարումը. ՏԵքստ: 