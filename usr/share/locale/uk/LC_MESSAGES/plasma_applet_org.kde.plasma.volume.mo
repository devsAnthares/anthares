��    &      L  5   |      P     Q  8   S     �     �     �     �     �     �  3   �  >        N     i     y     �  m   �     �          "     2     7     ?  *   O      z     �     �  "   �     �     �          3     :     H     X     e     �  ;   �     �    �     	  1   		     ;	     L	     f	     w	  %   �	  ,   �	     �	     �	  ;   
  !   B
     d
     u
  
   �
  =   �
  #   �
  (   �
          6  !   S  S   u  ^   �  '   (  .   P          �  :   �  @   �     .     ?  0   Y     �     �     �     �     �        #   %                                                              "          $                      
                                           &      !                	        % Accessibility data on volume sliderAdjust volume for %1 Applications Audio Muted Audio Volume Behavior Capture Devices Capture Streams Checkable switch for (un-)muting sound output.Mute Checkable switch to change the current default output.Default Decrease Microphone Volume Decrease Volume Devices General Heading for a list of ports of a device (for example built-in laptop speakers or a plug for headphones)Ports Increase Microphone Volume Increase Volume Maximum volume: Mute Mute %1 Mute Microphone No applications playing or recording audio No output or input devices found Playback Devices Playback Streams Port is unavailable (unavailable) Port is unplugged (unplugged) Raise maximum volume Show additional options for %1 Volume Volume at %1% Volume feedback Volume step: label of device items%1 (%2) label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: plasma_applet_org.kde.plasma.volume
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2017-09-20 20:57+0200
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <kde-i18n-uk@kde.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 2.0
 % Скоригувати гучність для %1 Програми Звук вимкнено Гучність Поведінка Пристрої захоплення Потоки захоплення даних Вимкнути звук Типовий Зменшити гучність для мікрофона Зменшити гучність Пристрої Загальне Порти Збільшити гучність для мікрофона Збільшити гучність Максимальна гучність: Вимкнути звук Вимкнути звук %1 Вимкнути мікрофон Жодна програма не відтворює і не записує звук Не знайдено пристроїв отримання та виведення даних Пристрої відтворення Потоки відтворення даних  (недоступний)  (від’єднано) Збільшити максимальну гучність Показати додаткові параметри для %1 Гучність Гучність на %1% Відгук керування гучністю Крок гучності: %1 (%2) %1: %2 100% %1% 