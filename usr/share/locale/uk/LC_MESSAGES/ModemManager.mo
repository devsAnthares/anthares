��          �            h  1   i      �  ?   �  6   �  2   3  ,   f  M   �  5   �  K     C   c  N   �  K   �  L   B  ,   �  �  �  �   �  >   6  �   u  �     f   �     	  �   �	  Z   O
  �   �
  �   6  �   �  �   }  �   0  �   �            	   
                                                                 Add, modify, and delete mobile broadband contacts Control the Modem Manager daemon Enable and view geographic location and positioning information Query and manage firmware on a mobile broadband device Query and utilize network information and services Send, save, modify, and delete text messages System policy prevents adding, modifying, or deleting this device's contacts. System policy prevents controlling the Modem Manager. System policy prevents enabling or viewing geographic location information. System policy prevents querying or managing this device's firmware. System policy prevents querying or utilizing network information and services. System policy prevents sending or maniuplating this device's text messages. System policy prevents unlocking or controlling the mobile broadband device. Unlock and control a mobile broadband device Project-Id-Version: Modem Manager
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-10-19 18:13+0300
PO-Revision-Date: 2013-10-19 18:17+0300
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <kde-i18n-uk@kde.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Lokalize 1.5
 Додати, внести зміни і вилучити контакти пристрою мобільних широкосмугових мереж Керування фоновою службою Modem Manager Увімкнути або переглянути дані щодо географічного розташування і позиціювання Опитування та керування мікропрограмою на пристрої мобільної широкосмугової мережі Надіслати запит і використати дані щодо мережі і служби Надіслати, зберегти, внести зміни або вилучити текстові повідомлення Правила системи перешкоджають додаванню, внесенню змін та вилученню записів контактів на цьому пристрої. Правила системи перешкоджають керування Modem Manager. Правила системи забороняють вмикання або перегляд даних щодо розташування. Правила системи перешкоджають опитуванню або керування мікропрограмою цього пристрою. Правила системи забороняють надсилання запитів і використання даних щодо мережі і служб. Правила системи забороняють надсилання або керування текстовими повідомленнями цього пристрою. Правила системи забороняють розблокування і керування пристроями широкосмугових мобільних мереж. Розблокувати пристрій мобільної широкосмугової мережі і керувати ним 