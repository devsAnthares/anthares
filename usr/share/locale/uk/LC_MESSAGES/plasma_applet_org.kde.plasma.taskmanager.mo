��    Q      �  m   ,      �     �     �     �                      .   !  U   P     �     �      �     �  /   �  /   -     ]     i     r  
   ~     �     �  $   �     �     �     �     �     	     	  	   *	     4	  
   F	  7   Q	     �	     �	     �	     �	  	   �	     �	  !   �	     
     
  	   !
      +
     L
     Y
     r
     �
     �
     �
     �
     �
     �
     �
  (   �
       =   '  2   e     �     �  "   �     �       J     1   Y  )   �  (   �  '   �  "     4   )     ^     l     r     {     �     �     �  U   �  N   !  9   p  J   �    �          *     :     T  $   j     �     �     �  !   �  <   �       H   <     �  z   �  z        �     �     �     �  "   �  +     o   G     �  !   �     �  G         H  2   Y     �  %   �  "   �     �  -   �  )     8   G     �     �     �  K   �  ?     7   D     |  H   �     �  <   �  2   6  -   i     �     �     �  '   �  /   �  )   /  P   Y  F   �     �       
   
  %     '   ;  
   c     n     �  ]   �  `   	  U   j  Q   �  G     f   Z  %   �  
   �     �  0        ?     H     Q     p  +   �     �  6   �        L   )             O                      4              6   $   ?   3         B              (   K   Q               ;       .   '   D   2   -       J   >   !   
   8                       P   5         ,          7   <           F      *   @       H   1                 "          9      +   &   0           /   N          #       E   	             I   A   =   %      G   :   C                   M    &All Desktops &Close &Fullscreen &Move &New Desktop &Pin &Shade 1 = number of desktop, 2 = desktop name&%1 %2 Activities a window is currently on (apart from the current one)Also available on %1 Add To Current Activity All Activities Allow this program to be grouped Alphabetically Always arrange tasks in columns of as many rows Always arrange tasks in rows of as many columns Arrangement Behavior By Activity By Desktop By Program Name Close Window or Group Cycle through tasks with mouse wheel Do Not Group Do Not Sort Filters Forget Recent Documents General Grouping and Sorting Grouping: Highlight windows Icon size: Invalid number of new messages, overlay, keep short— Keep &Above Others Keep &Below Others Keep launchers separate Large Ma&ximize Manually Mark applications that play audio Maximum columns: Maximum rows: Mi&nimize Minimize/Restore Window or Group More Actions Move &To Current Desktop Move To &Activity Move To &Desktop Mute New Instance On %1 On All Activities On The Current Activity On middle-click: Only group when the task manager is full Open groups in popups Open or bring to the front window of media player appRestore Over 9999 new messages, overlay, keep short9,999+ Pause playbackPause Play next trackNext Track Play previous trackPrevious Track Quit media player appQuit Re&size Remove launcher button for application shown while it is not runningUnpin Show all user Places%1 more Place %1 more Places Show only tasks from the current activity Show only tasks from the current desktop Show only tasks from the current screen Show only tasks that are minimized Show progress and status information in task buttons Show tooltips Small Sorting: Start New Instance Start playbackPlay Stop playbackStop The click actionNone Toggle action for showing a launcher button while the application is not running&Pin When clicking it would toggle grouping windows of a specific appGroup/Ungroup Which activities a window is currently onAvailable on %1 Which virtual desktop a window is currently onAvailable on all activities Project-Id-Version: plasma_applet_org.kde.plasma.taskmanager
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2018-01-17 10:22+0200
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <kde-i18n-uk@kde.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 2.0
 &Всі стільниці &Закрити &На весь екран &Пересунути С&творити стільницю При&шпилити З&горнути &%1 %2 Також доступне у %1 Додати до поточного простору дій Усі простори дій Дозволити групування для цієї програми За алфавітом Завжди компонувати завдання у стовпчики із такою кількістю рядків Завжди компонувати завдання у рядки із такою кількістю стовпчиків Компонування Поведінка За простором дій За стільницею За назвою програми Закрити вікно або групу Циклічний перехід завданнями прокручуванням коліщатка миші Не групувати Не впорядковувати Фільтри Спорожнити список недавніх документів Загальне Групування і впорядкування Групування: Підсвічування вікон Розмір піктограми: — Утримувати &понад іншими Утримувати п&ід іншими Відокремлювати засоби запуску Великий М&аксимізувати Вручну Позначати програми, які відтворюють звук Максимальна кількість стовпчиків: Максимальна кількість рядків: &Мінімізувати Мінімізувати/Відновити вікно або групу Додаткові дії Пересунути &на поточну стільницю Пересунути до п&ростору дій Пересунути на с&тільницю Вимкнути звук Новий екземпляр На %1 На усіх просторах дій На поточному просторі дій На клацання середньою: Групувати, лише якщо панель задач заповнено Відкривати групи у контекстних вікнах Відновити 9999+ Пауза Наступна композиція Попередня композиція Вийти Змінити &розмір Прибрати шпильку І ще %1 місце І ще %1 місця І ще %1 місць І ще одне місце Показувати тільки завдання з поточного простору дій Показувати лише завдання з поточної стільниці Показувати лише завдання з поточного екрана Показувати лише мінімізовані завдання Показувати на кнопках завдань дані щодо поступу і стану Показувати підказки Малий Впорядкування: Запустити новий екземпляр Пуск Стоп Нічого не робити При&шпилити Групувати/Розгрупувати Доступне у %1 Доступне у всіх просторах дій 