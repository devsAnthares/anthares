��          |      �             !     6     I     P     c     }     �     �     �     �       �         	   .     8  )   E     o  Q   �     �  U   �  G   D     �     �        	   
                                                @title:columnE-mail @title:columnName Avatar Duplicates Manager E-mail field labelE-mail Merge with Selected Contacts Phone details titlePhone Select contacts to be merged Show Merge Suggestions... name: merge reasons%1: %2 reasons join,  Project-Id-Version: kpeople5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2015-02-24 19:51+0200
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <kde-i18n-uk@kde.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 1.5
 Адреса ел. пошти Ім’я Аватар Керування дублікатами Адреса ел. пошти Об’єднати з позначеними записами контактів Телефон Виберіть записи контактів, які слід об’єднати Показати пропозиції щодо об’єднання… %1: %2 ,  