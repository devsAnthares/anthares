��          �      l      �     �     �            
   #     .     :     I     Q     ]     e     v     }     �  #   �     �     �     �     �  
           2   ,  5   _     �     �     �  4   �     �          #     C  )   T     ~  +   �     �  
   �  8   �           #     D     `                                                       
   	                                         (Automatic load) Always show all entries Application Status Auto Categories Close popup Communications Entries Extra Items General Hardware Control Hidden Keyboard Shortcut Miscellaneous Name of the system tray entryEntry Show hidden icons Shown Status & Notifications System Services Visibility Project-Id-Version: plasma_applet_org.kde.plasma.private.systemtray
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-03 03:15+0100
PO-Revision-Date: 2017-01-10 18:49+0200
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <kde-i18n-uk@kde.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 1.5
  (автоматичне завантаження) Завжди показувати усі записи Стан програм Авто Категорії Закрити контекстну підказку Обмін даними Пункти Додаткові пункти Загальне Керування обладнанням Приховувати Клавіатурне скорочення Інше Пункт Показати приховані піктограми Показувати Стан і сповіщення Служби системи Видимість 