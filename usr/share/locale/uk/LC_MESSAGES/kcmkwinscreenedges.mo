��    %      D  5   l      @     A     E     G  	   Y     c     |     �     �     �     �     �     �            S   ,  w   �     �  M         [     |  ?   �     �  	   �     �     
     #  %   2     X     e  F   �  #   �     �  ]     R   f     �     �    �     �	     �	      �	     
  &   
  (   ;
     d
     z
  )   �
  7   �
     �
  2     .   9     h  ~   �  �     (   �  |   �     3  !   C  u   e     �     �  8        >     ^  i   z  #   �       �     `   �  E   �  �   B  �   �  !   �     �               !      "          #   %                                                                            $                          	                
                            ms % %1 - All Desktops %1 - Cube %1 - Current Application %1 - Current Desktop %1 - Cylinder %1 - Sphere &Reactivation delay: &Switch desktop on edge: Activation &delay: Active Screen Corners and Edges Activity Manager Always Enabled Amount of time required after triggering an action until the next trigger can occur Amount of time required for the mouse cursor to be pushed against the edge of the screen before the action is triggered Application Launcher Change desktop when the mouse cursor is pushed against the edge of the screen EMAIL OF TRANSLATORSYour emails Lock Screen Maximize windows by dragging them to the top edge of the screen NAME OF TRANSLATORSYour names No Action Only When Moving Windows Open krunnerRun Command Other Settings Quarter tiling triggered in the outer Show Desktop Switch desktop on edgeDisabled Tile windows by dragging them to the left or right edges of the screen Toggle alternative window switching Toggle window switching Trigger an action by pushing the mouse cursor against the corresponding screen edge or corner Trigger an action by swiping from the screen edge towards the center of the screen Window Management of the screen Project-Id-Version: kcmkwinscreenedges
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-30 03:13+0100
PO-Revision-Date: 2017-12-30 09:00+0200
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <kde-i18n-uk@kde.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
  мс % %1 — усі стільниці %1 — куб %1 — поточна програма %1 — поточна стільниця %1 — циліндр %1 — куля &Затримка реактивації: П&еремикати стільниці на краю: &Затримка дії: Активні кути та краї екрана Керування просторами дій Увімкнено завжди Час, після використання дії до можливого використання наступної дії. Час, який слід утримувати вказівник миші на краю екрана для вмикання дії. Засіб запуску програм Змінити стільницю, якщо вказівник миші буде наведено на край екрана yurchor@ukr.net Заблокувати екран Максимізувати вікна перетягуванням їх до верхнього краю екрана Юрій Чорноіван Без дії Лише під час пересування вікон Виконати команду Інші параметри Розташування плиткою за чвертями, увімкнене на зовнішніх Показати стільницю Вимкнено Розкладати вікна перетягуванням їх до лівого або правого країв екрана Увімкнути/Вимкнути альтернативне перемикання вікон Увімкнути/Вимкнути перемикання вікон Щоб увімкнути дію, пересуньте вказівник миші вашого комп’ютера до відповідного краю або кута екрана Щоб увімкнути дію, виконайте на сенсорній панелі рух від краю екрана до центру Керування вікнами екрана 