��          �      L      �     �     �     �     �          )     1     9     P     h     t  K   �     �     �     �     �                 (   8  !   a  "   �  8   �  &   �  	     	     1     C   L     �     �     �     �  	   �  $   �  1        I  !   V                                                  
   	                                               Change the barcode type Clear history Clipboard Contents Clipboard history is empty. Clipboard is empty Code 39 Code 93 Configure Clipboard... Creating barcode failed Data Matrix Edit contents Indicator that there are more urls in the clipboard than previews shown+%1 Invoke action QR Code Remove from history Return to Clipboard Search Show barcode Project-Id-Version: plasma_applet_org.kde.plasma.clipboard
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-10 03:07+0200
PO-Revision-Date: 2015-02-17 21:07+0200
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <kde-i18n-uk@kde.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 1.5
 Змінити тип штрихкоду Спорожнити журнал Вміст буфера даних Журнал буфера обміну порожній. Буфер даних порожній Код 39 Код 93 Налаштувати буфер обміну… Створення штрихкоду зазнало невдачі Матриця даних Редагувати вміст +%1 Викликати дію Код QR Вилучити із журналу Повернути до буфера обміну Шукати Показати штрихкод 