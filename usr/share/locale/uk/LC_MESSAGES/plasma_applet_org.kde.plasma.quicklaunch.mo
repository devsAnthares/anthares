��          �      <      �     �  <   �  
   �     	          &     3     ?  
   G     R     c     q     }     �     �  
   �     �    �  3   �  �        �     �  5   �  8     !   Q     s  '   �  ?   �  7   �     $  4   @  8   u     �  '   �     �                                                  
   	                                                Add Launcher... Add launchers by Drag and Drop or by using the context menu. Appearance Arrangement Edit Launcher... Enable popup Enter title General Hide icons Maximum columns: Maximum rows: Quicklaunch Remove Launcher Show hidden icons Show launcher names Show title Title Project-Id-Version: plasma_applet_org.kde.plasma.quicklaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-02-26 04:09+0100
PO-Revision-Date: 2016-01-19 16:52+0200
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <kde-i18n-uk@kde.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 1.5
 Додати інструмент запуску… Додайте піктограми запуску перетягуванням зі скиданням або за допомогою контекстного меню. Вигляд Компонування Змінити інструмент запуску… Увімкнути контекстні підказки Введіть заголовок Загальне Приховати піктограми Максимальна кількість стовпчиків: Максимальна кількість рядків: Швидкий запуск Вилучити інструмент запуску Показати приховані піктограми Показати назви Показувати заголовок Заголовок 