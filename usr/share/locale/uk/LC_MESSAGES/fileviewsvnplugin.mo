��          �   %   �      p     q  +   �  .   �  6   �  *     #   D  &   h  /   �  2   �  :   �  0   -  3   ^  ;   �  K   �  -     $   H  '   m     �     �     �     �     �  #        1     O     c     |    �     �  0   �  :   �  c   	  R   o	  '   �	  )   �	  4   
  :   I
  c   �
  A   �
  C   *  q   n  �   �  Q   �  $   �  (   �     %     <     Z     q     �  3   �  '   �  	          #   &                                                    	   
                                                                    @action:buttonCommit @info:statusAdded files to SVN repository. @info:statusAdding files to SVN repository... @info:statusAdding of files to SVN repository failed. @info:statusCommit of SVN changes failed. @info:statusCommitted SVN changes. @info:statusCommitting SVN changes... @info:statusRemoved files from SVN repository. @info:statusRemoving files from SVN repository... @info:statusRemoving of files from SVN repository failed. @info:statusReverted files from SVN repository. @info:statusReverting files from SVN repository... @info:statusReverting of files from SVN repository failed. @info:statusSVN status update failed. Disabling Option "Show SVN Updates". @info:statusUpdate of SVN repository failed. @info:statusUpdated SVN repository. @info:statusUpdating SVN repository... @item:inmenuSVN Add @item:inmenuSVN Commit... @item:inmenuSVN Delete @item:inmenuSVN Revert @item:inmenuSVN Update @item:inmenuShow Local SVN Changes @item:inmenuShow SVN Updates @labelDescription: @title:windowSVN Commit Show updates Project-Id-Version: fileviewsvnplugin
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2015-11-08 13:28+0200
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <kde-i18n-uk@kde.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
 Надіслати Додано файли до сховища SVN. Додавання файлів до сховища SVN… Спроба додавання файлів до сховища SVN зазнала невдачі. Спроба надсилання змін до SVN зазнала невдачі. Надіслано зміни до SVN. Надсилання змін до SVN… Вилучено файли зі сховища SVN. Вилучення файлів зі сховища SVN… Спроба вилучення файлів зі сховища SVN зазнала невдачі. Зміни у файлах сховища SVN скасовано. Скасовуємо зміни у файла сховища SVN… Спроба скасування змін у файлах зі сховища SVN зазнала невдачі. Спроба оновлення стану у SVN зазнала невдачі. Не позначено пункт «Показувати оновлення SVN». Спроба оновлення сховища SVN зазнала невдачі. Оновлено сховище SVN. Оновлення сховища SVN… SVN-додавання SVN-передавання… SVN-вилучення SVN-скасовування SVN-оновлення Показати локальні зміни у SVN Показати SVN-оновлення Опис: SVN-передавання Показати оновлення 