��          �      �           	               4  	   I     S      c  "   �      �     �     �  	   �     �               )  
   9     D     S     a     g     {  �  �     �     �  2   �  (   �     �  +        1  <   A  G   ~     �     �     �     �       5   1     g  
   �  *   �  ,   �     �  +   �                                   	                                     
                                      K (HH:MM) (In minutes - min. 1, max. 600) Activate Night Color Automatic Detect location EMAIL OF TRANSLATORSYour emails Error: Morning not before evening. Error: Transition time overlaps. Latitude Location Longitude NAME OF TRANSLATORSYour names Night Color Night Color Temperature:  Operation mode: Roman Gilg Sunrise begins Sunset begins Times Transition duration and ends Project-Id-Version: kcm_nightcolor
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-12 03:09+0100
PO-Revision-Date: 2017-12-12 08:49+0200
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <kde-i18n-uk@kde.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 2.0
  К (HH:MM) (у хвилинах — мін. 1, макс. 600) Задіяти нічні кольори Автоматичний Визначити розташування yurchor@ukr.net Помилка: ранок не передує вечору. Помилка: перекриття часів кульмінації. Широта Розташування Довгота Юрій Чорноіван Нічні кольори Температура нічних кольорів: Режим роботи: Roman Gilg Схід сонця починається Захід сонця починається Терміни Тривалість кульмінації і завершується 