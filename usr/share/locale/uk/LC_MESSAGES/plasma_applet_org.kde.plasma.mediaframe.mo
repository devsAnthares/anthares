��            )   �      �     �     �     �     �     �     �     �  
           )   (     R     V     \     c     v     �     �     �  3   �     �       <   #  5   `  8   �  8   �                    /    1     J     e     ~  2   �     �     �  *   �      *     K  �   \     �  
   	     	  E   	  7   ^	  9   �	  #   �	     �	  ^   	
  6   h
  K   �
  �   �
  z   y  �   �  �   �       /     +   K     w                                                  
                                                                               	        Add files... Add folder... Background frame Change picture every Choose a folder Choose files Configure plasmoid... Fill mode: General Left click image opens in external viewer Pad Paths Paths: Pause on mouseover Preserve aspect crop Preserve aspect fit Randomize items Stretch The image is duplicated horizontally and vertically The image is not transformed The image is scaled to fit The image is scaled uniformly to fill, cropping if necessary The image is scaled uniformly to fit without cropping The image is stretched horizontally and tiled vertically The image is stretched vertically and tiled horizontally Tile Tile horizontally Tile vertically s Project-Id-Version: plasma_applet_org.kde.plasma.mediaframe
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-16 03:31+0100
PO-Revision-Date: 2017-11-17 07:33+0200
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <kde-i18n-uk@kde.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 2.0
 Додати файли… Додати теку… Фонова рамка Змінювати зображення кожні Виберіть теку Виберіть файли Налаштувати плазмоїд… Режим заповнення: Загальне Клацання лівою кнопкою на зображенні відкриває його у зовнішньому переглядачі Підкладка Шляхи Шляхи: Призупиняти при наведенні вказівника Зберегти формат із обрізанням Зберегти формат із заповненням Випадковий порядок Розтягнути Зображення дублюється горизонтально і вертикально Зображення не перетворюється Масштабування зображення для заповнення Рівномірне масштабування зображення для заповнення з обрізанням, якщо треба Рівномірне масштабування зображення для заповнення без обрізання Зображення буде розтягнуто горизонтально і розкладено плиткою вертикально Зображення буде розтягнуто вертикально і розкладено плиткою горизонтально Мозаїка Розставити горизонтально Розставити вертикально с 