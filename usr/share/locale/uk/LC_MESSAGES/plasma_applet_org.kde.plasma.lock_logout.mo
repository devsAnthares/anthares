��          �      L      �  &   �  +   �       @     	   ]     g     �     �     �     �  (   �     �     �  ,   �               +     7    ;  �   U     �     V     g     n  0     
   �  "   �     �  !   �  w        �  +   �  S   �       +   +  +   W     �        
                          	                                                                  Do you want to suspend to RAM (sleep)? Do you want to suspend to disk (hibernate)? General Heading for list of actions (leave, lock, shutdown, ...)Actions Hibernate Hibernate (suspend to disk) Leave Leave... Lock Lock the screen Logout, turn off or restart the computer No Sleep (suspend to RAM) Start a parallel session as a different user Suspend Switch User Switch user Yes Project-Id-Version: plasma_applet_org.kde.plasma.lock_logout
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2015-07-05 14:19+0300
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <kde-i18n-uk@kde.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 1.5
 Бажаєте призупинити роботу системи зі збереженням до RAM (призупинити)? Бажаєте призупинити роботу системи зі збереженням на диск (приспати)? Загальне Дії Приспати Сон зі збереженням на диск Вийти Завершити роботу... Заблокувати Заблокувати екран Вийти з системи, вимкнути комп’ютер або перезавантажити систему Ні Сон зі збереженням до RAM Почати паралельний сеанс як інший користувач Призупинити Перемкнути користувача Перемкнути користувача Так 