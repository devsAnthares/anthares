��          �   %   �      @     A  �   `  m   �  �   P  )   �  `        o     |     �     �     �      �     �     �  '        ,     >     ]     b     s  ?   �  Y   �  +     H   F  �  �      �  �   �  �   i	  %   
  0   ;
  �   l
     �
  ?   	     I     g  I   p     �     �  "   �  B   �  "   A     d  
   �  "   �     �  P   �  �     H   �  �                                                    	                                                               
              (c) 2003-2007 Fredrik Höglund <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) @item:inlistbox sizeResolution dependent A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Cursor Theme Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Fredrik Höglund Get new Theme Get new color schemes from the Internet Install from File NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. You have to restart the Plasma session for these changes to take effect. Project-Id-Version: kcmmousetheme
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-07-12 19:38+0300
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <kde-i18n-uk@kde.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 1.5
 © Fredrik Höglund, 2003–2007 <qt>Ви дійсно хочете вилучити тему курсорів <strong>%1</strong>? <br />Це вилучить всі файли, встановлені темою.</qt> <qt>Не можна вилучити тему, яку ви зараз використовуєте.<br />Спочатку перемкніть на іншу тему.</qt> (Доступні розміри: %1) залежний від роздільності Тема з назвою %1 вже існує у вашій теці тем піктограм. Замінити її новою? Підтвердження Параметри вказівника було змінено Тема вказівника Опис Перетягніть або введіть адресу для теми yurchor@ukr.net Fredrik Höglund Отримати нову тему Отримати схему кольорів з інтернету Встановити з файла Юрій Чорноіван Назва Перезаписати тему? Вилучити тему Файл %1, здається, не є архівом теми курсорів. Неможливо отримати архів теми курсорів. Будь ласка, перевірте, що адреса %1 є правильною. Неможливо знайти архів теми курсорів %1. Вам потрібно перезапустити сеанс Плазми, щоб внесені зміни набули чинності. 