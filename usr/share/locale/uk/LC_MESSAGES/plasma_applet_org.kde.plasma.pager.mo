��          �   %   �      0  *   1     \  .   s     �     �     �     �     �     �                 
   $     /     5     =     E     ]     t     �     �     �     �    �  �   �  ;   �  �   �  "   N  "   q  4   �  ,   �     �          2     >     R     c     ~     �     �  $   �  8   �  /   	  B   O	  !   �	     �	     �	        
                             	                                                                                          %1 Minimized Window: %1 Minimized Windows: %1 Window: %1 Windows: ...and %1 other window ...and %1 other windows Activity name Activity number Add Virtual Desktop Configure Desktops... Desktop name Desktop number Display: Does nothing General Horizontal Icons Layout: No text Only the current screen Remove Virtual Desktop Selecting current desktop: Show Activity Manager... Shows desktop The pager layoutDefault Vertical Project-Id-Version: plasma_applet_org.kde.plasma.pager
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-19 03:29+0200
PO-Revision-Date: 2016-10-25 17:33+0300
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <kde-i18n-uk@kde.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 1.5
 %1 мінімізоване вікно: %1 мінімізованих вікна: %1 мінімізованих вікон: %1 мінімізоване вікно: %1 вікно: %1 вікна: %1 вікон: %1 вікно: …та %1 інше вікно …та %1 інших вікна …та %1 інших вікон …та ще одне вікно Назва простору дій Номер простору дій Додати віртуальну стільницю Налаштувати стільниці… Назва стільниці Номер стільниці Показ: Ніяких дій Загальне Горизонтальне Піктограми Компонування: Без тексту Лише поточний екран Вилучити віртуальну стільницю Вибір поточної стільниці: Показати керування просторами дій… Показує стільницю Типове Вертикальне 