��    ?        Y         p  �  q  }  J  M  �        
   7     B     K     j     �  <   �     �     �  	   �     �  .     %   A     g     }     �     �     �     �     �     �     �     �  4        B  9   T     �     �     �  �   �  !   �  t   �     8     D     a     n     s  	   �     �  -   �     �  B   �  &     ?   B  '   �  )   �  7   �  .     5   ;  +   q     �     �     �  ,   �          -  9   :  V   t  C   �       �      �  �  �!  !   �&     �&     �&  D   �&  D   /'     t'  ~   �'     (     (     ;(  (   R(  r   {(  P   �(  ,   ?)     l)  "   })     �)  (   �)  &   �)     �)  D   *     Y*  <   _*  �   �*  $   %+  w   J+  (   �+  "   �+     ,  �  ,  @   �-  �   0.     /  3   $/      X/     y/  M   �/     �/     �/  e   �/  
   `0  �   k0  C   �0  m   61  D   �1  D   �1  r   .2  K   �2  �   �2  g   n3  3   �3  +   
4  #   64  u   Z4  6   �4     5  |   #5  �   �5     :6     !      	   2   ?   
   /                 +   (   *       1            )   >           8          9   $              7             .       '   6          %   <             3   ,          ;       =   &                              -           #      "          :      5      0                                   4               <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body style=" font-family:'hlv'; font-size:9pt; font-weight:400; font-style:normal;">
<p style="-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><br /></p></body></html> <b>Security notice:</b>
                             According to a security audit by Taylor Hornby (Defuse Security),
                             the current implementation of Encfs is vulnerable or potentially vulnerable
                             to multiple types of attacks.
                             For example, an attacker with read/write access
                             to encrypted data might lower the decryption complexity
                             for subsequently encrypted data without this being noticed by a legitimate user,
                             or might use timing analysis to deduce information.
                             <br /><br />
                             This means that you should not synchronize
                             the encrypted data to a cloud storage service,
                             or use it in other circumstances where the attacker
                             can frequently access the encrypted data.
                             <br /><br />
                             See <a href='http://defuse.ca/audits/encfs.htm'>defuse.ca/audits/encfs.htm</a> for more information. <b>Security notice:</b>
                             CryFS encrypts your files, so you can safely store them anywhere.
                             It works well together with cloud services like Dropbox, iCloud, OneDrive and others.
                             <br /><br />
                             Unlike some other file-system overlay solutions,
                             it does not expose the directory structure,
                             the number of files nor the file sizes
                             through the encrypted data format.
                             <br /><br />
                             One important thing to note is that,
                             while CryFS is considered safe,
                             there is no independent security audit
                             which confirms this. @title:windowCreate a New Vault Activities Backend: Can not create the mount point Can not open an unknown vault. Change Choose the encryption system you want to use for this vault: Choose the used cypher: Close the vault Configure Configure Vault... Configured backend can not be instantiated: %1 Configured backend does not exist: %1 Correct version found Create Create a New Vault... CryFS Device is already open Device is not open Dialog Do not show this notice again EncFS Encrypted data location Failed to create directories, check your permissions Failed to execute Failed to fetch the list of applications using this vault Failed to open: %1 Forcefully close  General If you limit this vault only to certain activities, it will be shown in the applet only when you are in those activities. Furthermore, when you switch to an activity it should not be available in, it will automatically be closed. Limit to the selected activities: Mind that there is no way to recover a forgotten password. If you forget the password, your data is as good as gone. Mount point Mount point is not specified Mount point: Next Open with File Manager Password: Plasma Vault Please enter the password to open this vault: Previous The mount point directory is not empty, refusing to open the vault The specified backend is not available The vault configuration can only be changed while it is closed. The vault is unknown, can not close it. The vault is unknown, can not destroy it. This device is already registered. Can not recreate it. This directory already contains encrypted data Unable to close the vault, an application is using it Unable to close the vault, it is used by %1 Unable to detect the version Unable to perform the operation Unknown device Unknown error, unable to create the backend. Use the default cipher Vaul&t name: Wrong version installed. The required version is %1.%2.%3 You need to select empty directories for the encrypted storage and for the mount point formatting the message for a command, as in encfs: not found%1: %2 Project-Id-Version: plasmavault-kde
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-11 05:53+0100
PO-Revision-Date: 2017-12-10 10:34+0200
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <kde-i18n-uk@kde.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 2.0
 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body style=" font-family:'hlv'; font-size:9pt; font-weight:400; font-style:normal;">
<p style="-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><br /></p></body></html> <b>Зауваження щодо безпеки:</b>
                             Відповідно до аудиту захисту, який виконано Тейлором Горнбі (Defuse Security),
                             поточна реалізація Encfs є вразливою або потенційно вразливою
                             до декількох типів нападів.
                             Наприклад, нападник із доступом до читання-запису
                             зашифрованих даних може зменшити складність шифрування
                             послідовно зашифрованих даних без відома уповноваженого на це користувача
                             або може скористатися аналізом часового розподілу доступу до даних для отримання інформації.
                             <br /><br />
                             Це означає, що вам не слід синхронізувати зашифровані
                             дані зі службами зберігання даних у «хмарі»
                             або використовувати шифрування за обставин, коли
                             нападник може здійснювати частий доступ до зашифрованих даних.
                             <br /><br />
                             Докладніший опис можна знайти тут: <a href='http://defuse.ca/audits/encfs.htm'>defuse.ca/audits/encfs.htm</a> <b>Security notice:</b>
                             CryFS шифрує ваші дані. Отже, ви можете безпечно зберігати їх будь-де.
                             Ця файлова система добре працює із «хмарними» службами, зокрема Dropbox, iCloud, OneDrive тощо.
                             <br /><br />
                             На відміну від інших файлових систем-накладок,
                             ця файлова система не надає доступу до структури каталогів,
                             кількості файлів або розміру файлів
                             через шифрований формат даних.
                             <br /><br />
                             Важливо знати, що хоча
                             CryFS вважається безпечною,
                             аудиторська перевірка, яка б це підтвердила,
                             ніколи не проводилася. Створення сховища Дії Модуль: Не вдалося створити точку монтування Неможливо відкрити невідоме сховище. Змінити Виберіть систему шифрування, яку слід використати для цього сховища: Виберіть шифр: Закрити сховище Налаштувати Налаштувати сховище… Не вдалося створити екземпляр налаштованого модуля обробки: %1 Не знайдено налаштованого модуля обробки: %1 Виявлено належну версію Створити Створити сховище… CryFS Пристрій вже відкрито Пристрій не відкрито Діалогове вікно Більше не показувати це повідомлення EncFS Розташування зашифрованих даних Не вдалося створити каталоги. Перевірте, чи є у вас достатні права доступу. Не вдалося виконати Не вдалося отримати список програм, що використовують це сховище Не вдалося відкрити: %1 Примусово закрити  Загальне Якщо ви обмежите дію цього сховища лише певними просторами дій, його буде показано у аплеті, лише тоді, коли ви користуєтеся цими просторами дій. Крім того, якщо ви перемкнетеся на простір дій, з якого сховище є недоступним, його автоматично буде закрито. Обмежити вибраними просторами дій: Зауважте, що способів відновлення забутого пароля не передбачено. Якщо ви забудете пароль, ваші дані буде втрачено. Точка монтування Не вказано точку монтування Точка монтування: Далі Відкрити у програмі для керування файлами Пароль: Сховище Плазми Будь ласка, введіть пароль для відкриття цього сховища: Назад Точка монтування каталогу є непорожньою. У відкритті сховища відмовлено. Вказаний модуль обробки недоступний Налаштування сховища можна змінити, лише коли його закрито. Невідоме сховище. Не вдалося закрити. Невідоме сховище. Не вдалося знищити. Цей пристрій вже зареєстровано. Повторне створення неможливе. Цей каталог уже містить зашифровані дані Не вдалося закрити сховище, оскільки ним користується якась програма. Не вдалося закрити сховище, оскільки ним користується %1. Не вдалося визначити версію Не вдалося виконати дію Невідомий пристрій Невідома помилка, не вдалося створити екземпляр модуля обробки. Використовувати типовий шифр Назва с&ховища: Встановлено помилкову версію. Для роботи потрібна така версія: %1.%2.%3 Вам слід вибрати порожні каталоги для зашифрованого сховища і для точки монтування %1: %2 