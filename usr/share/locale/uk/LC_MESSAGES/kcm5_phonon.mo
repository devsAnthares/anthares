��    B      ,  Y   <      �  i   �  m        y  b   �     �     	  6        O  7   _     �     �  	   �     �  (   �  )   �  )   (     R     o  Y   u     �     �  �   �      y	  .   �	     �	  
   �	     �	     �	     
     
     !
     5
     B
     J
     c
     r
     w
     �
     �
     �
     �
     �
  	   �
  
   �
     �
     �
  	     
     
   *     5     B  	   `     j     o  
   �  �   �     (  8   8  �   q     �  8   	  ,   B  ,   o  %   �     �  �  �  �   �    }  >   �  �   �  @   |  !   �  ^   �     >  T   T  !   �     �     �     �  V     L   e  L   �  8   �  #   8  �   \  )     %   2  �   X     F  �   e  '   �       /   0     `  1   ~     �  #   �     �       G        `     o     x  ,   �     �  %   �       #        8     P     j  4   z     �     �     �  !     E   #  -   i     �  8   �     �  �   �     �  j   �  �   d     D  T   Z  �   �  �   ?  J   �  E         $   !   %          ;   ,          B                 8   #                 7   9       +              )   ?   -   <         6                                    *   2       >                1       @       .   5   3   :   A         &      /                4              0      "             =   (   '         	   
           @info User changed Phonon backendTo apply the backend change you will have to log out and back in again. A list of Phonon Backends found on your system.  The order here determines the order Phonon will use them in. Apply Device List To... Apply the currently shown device preference list to the following other audio playback categories: Audio Hardware Setup Audio Playback Audio Playback Device Preference for the '%1' Category Audio Recording Audio Recording Device Preference for the '%1' Category Backend Colin Guthrie Connector Copyright 2006 Matthias Kretz Default Audio Playback Device Preference Default Audio Recording Device Preference Default Video Recording Device Preference Default/Unspecified Category Defer Defines the default ordering of devices which can be overridden by individual categories. Device Configuration Device Preference Devices found on your system, suitable for the selected category.  Choose the device that you wish to be used by the applications. EMAIL OF TRANSLATORSYour emails Failed to set the selected audio output device Front Center Front Left Front Left of Center Front Right Front Right of Center Hardware Independent Devices Input Levels Invalid KDE Audio Hardware Setup Matthias Kretz Mono NAME OF TRANSLATORSYour names Phonon Configuration Module Playback (%1) Prefer Profile Rear Center Rear Left Rear Right Recording (%1) Show advanced devices Side Left Side Right Sound Card Sound Device Speaker Placement and Testing Subwoofer Test Test the selected device Testing %1 The order determines the preference of the devices. If for some reason the first device cannot be used Phonon will try to use the second, and so on. Unknown Channel Use the currently shown device list for more categories. Various categories of media use cases.  For each category, you may choose what device you prefer to be used by the Phonon applications. Video Recording Video Recording Device Preference for the '%1' Category  Your backend may not support audio recording Your backend may not support video recording no preference for the selected device prefer the selected device Project-Id-Version: kcm5_phonon
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2013-10-28 18:26+0200
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <kde-i18n-uk@kde.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
 Для застосування зміни модуля обробки вам слід вийти з облікового запису і увійти знову. У вашій системі знайдено декілька серверних програм (модулів) для Phonon. Цей порядок визначатиме, у якій послідовності Phonon їх використовуватиме. Застосувати список пристроїв до... Застосувати показаний список пріоритетів пристроїв до наступних інших категорій відтворення звуку: Налаштування звукового обладнання Відтворення звуку Вибір пристрою відтворення звуку для категорії «%1» Запис звуку Вибір пристрою запису звуку для категорії «%1» Серверна програма Colin Guthrie Лінія з'єднання © Matthias Kretz, 2006 Пріоритет типових пристроїв відтворення звуку Пріоритет типових пристроїв запису звуку Пріоритет типових пристроїв запису відео Типова / невизначена категорія Зменшити пріоритет Визначає типове впорядкування пристроїв, яке може бути змінене індивідуальними категоріями. Налаштування пристрою Пріоритет пристроїв Знайдені у вашій системі пристрої, які відповідають вказаній категорії. Виберіть пристрій, який має бути використано програмами. fr.ivsn@ukrainian-orthodox.org Не вдалося встановити вибраний вами пристрій відтворення звукових даних Передній центральний Передній лівий Передній лівоцентральний Передній правий Передній правоцентральний Обладнання Незалежні пристрої Вхідні рівні Некоректний Налаштування звукового обладнання у KDE Matthias Kretz Моно о. Іван Петрущак Модуль налаштування Phonon Відтворення (%1) Збільшити пріоритет Профіль Задній центральний Задній лівий Задній правий Запис (%1) Показати додаткові пристрої Боковий лівий Боковий правий Звукова картка Звуковий пристрій Розташування гучномовців і перевірка Низькочастотний динамік Перевірити Випробувати вказаний пристрій Перевірка %1 Порядок визначає пріоритет пристроїв. Якщо з якоїсь причини перший пристрій не може бути задіяний, Phonon спробує наступний. Невідомий канал Інші категорії можна знайти у поточному списку пристроїв. Різноманітні категорії відтворення. Для кожної категорії можна вибрати пристрій, який використовуватимуть програми Phonon. Запис відео Вибір пристрою запису відео для категорії «%1» У модулі, яким ви користуєтеся, може бути не передбачено запису звукових даних У модулі, яким ви користуєтеся може бути не передбачено запису відеоданих не надавати переваги вибраному пристрою надавати перевагу вибраному пристрою 