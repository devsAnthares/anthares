��          �      �       0     1  �  H     �       &         ?     `     n     v     �     �  (   �  �  �  #   �  1       9	     J	  I   Z	  $   �	     �	     �	     �	     	
  '   
  R   A
         
                 	                                (c) 2002-2006 KDE Team <h1>System Notifications</h1>Plasma allows for a great deal of control over how you will be notified when certain events occur. There are several choices as to how you are notified:<ul><li>As the application was originally designed.</li><li>With a beep or other noise.</li><li>Via a popup dialog box with additional information.</li><li>By recording the event in a logfile without any additional visual or audible alert.</li></ul> Carsten Pfeiffer Charles Samuels Disable sounds for all of these events EMAIL OF TRANSLATORSYour emails Event source: KNotify NAME OF TRANSLATORSYour names Olivier Goffart Original implementation System Notification Control Panel Module Project-Id-Version: kcm5_notify
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-09 07:18+0200
PO-Revision-Date: 2017-08-09 10:35+0200
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <kde-i18n-uk@kde.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
 © Команда KDE, 2002–2006  <h1>Системні сповіщення</h1> Плазма надає вам змогу визначити спосіб попередження вас про певні системні події. Існує декілька варіантів для вибору щодо того, як вас буде повідомлено:<ul><li>Спосіб, визначений самою програмою.</li> <li>Системним гудком чи іншим звуком.</li><li>За допомогою контекстного (вигулькного) вікна з додатковою інформацією.</li><li>Записом події у файл реєстрації системних подій без зайвого звукового чи візуального повідомлення.</li></ul> Carsten Pfeiffer Charles Samuels Вимкнути озвучування для усіх цих подій rysin@kde.org, kov@tokyo.email.ne.jp Джерело події: KNotify Andriy Rysin, Dmytro Kovalov Olivier Goffart Початкова реалізація Модуль панелі керування системних сповіщень 