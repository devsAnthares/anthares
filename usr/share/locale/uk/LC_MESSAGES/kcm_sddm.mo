��    ,      |  ;   �      �  (   �     �  �   �     �     �     �     �     �     �     �     �     �               %     1      J     k     s     �     �     �     �     �     �     �               /     4     I     Y     l     y     �     �      �  7   �                     1     6  �  <     6	     C	  %   G	  %   m	     �	  
   �	     �	     �	  )   �	     �	  5   
     :
  !   Y
     {
     �
  '   �
     �
     �
  "   �
  /   �
  "   -     P  .   o  '   �  <   �          !     =  
   Y  <   d  0   �  7   �     
  (   $  /   M     }  4   �  b   �     !  1   *     \     w     �     !                 %   +   ,      "   #                               &                  	                           )                             *      '      $      (                                
    %1 is the name of a session%1 (Wayland) ... @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) @title:windowSelect Image Advanced Author Auto &Login Background: Clear Image Commands Could not decompress archive Cursor Theme: Customize theme Default Description Download New SDDM Themes EMAIL OF TRANSLATORSYour emails General Get New Theme Halt Command: Install From File Install a theme. Invalid theme package Load from file... Login screen using the SDDM Maximum UID: Minimum UID: NAME OF TRANSLATORSYour names Name No preview available Reboot Command: Relogin after quit Remove Theme SDDM KDE Config SDDM theme installer Session: The default cursor theme in SDDM The theme to install, must be an existing archive file. Theme Unable to install theme Uninstall a theme. User User: Project-Id-Version: kcm_sddm
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2017-10-17 18:23+0200
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <kde-i18n-uk@kde.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 2.0
 %1 (Wayland) … (Доступні розміри: %1) Виберіть зображення Додатково Автор Автов&хід Тло: Спорожнити зображення Команди Не вдалося розпакувати архів Тема вказівника: Налаштування теми Типова Опис Отримати нові теми SDDM yurchor@ukr.net Загальне Отримати нову тему Команда зупинення роботи: Встановити з файла Встановити тему. Некоректний пакунок теми Завантажити з файла… Вікно вітання з використанням SDDM Максимальний UID: Мінімальний UID: Юрій Чорноіван Назва Попередній перегляд недоступний Команда перезавантаження: Повторно входити після виходу Вилучити тему Налаштування SDDM для KDE Засіб встановлення тем SDDM Сеанс: Типова тема вказівників у SDDM Пакунок теми для встановлення має бути файлом архіву. Тема Не вдалося встановити тему Вилучити тему. Користувач Користувач: 