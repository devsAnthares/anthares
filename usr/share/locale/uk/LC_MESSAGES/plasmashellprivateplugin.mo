��            )   �      �     �     �     �     �     �     �            !   $     F  !   [     }     �     �     �     �     �     �     �     �     �  2     @   >  	     4   �  8   �  $   �          *  	  .     8     J     `  C   t  5   �     �  +   �     +  G   :  4   �  V   �     	     %	  (   ;	     d	     w	  "   �	  (   �	  4   �	  4   	
  ,   >
  �   k
  �        �     �     �  L   �  *   ,     W     
                                                             	                                                                       &Execute All Widgets Categories: Desktop Shell Scripting Console Download New Plasma Widgets Editor Executing script at %1 Filters Install Widget From Local File... Installation Failure Installing the package %1 failed. Load New Session Open Script File Output Running Runtime: %1ms Save Script File Screen lock enabled Screen saver timeout Select Plasmoid File Sets the minutes after which the screen is locked. Sets whether the screen will be locked after the specified time. Templates Toolbar Button to switch to KWin Scripting ModeKWin Toolbar Button to switch to Plasma Scripting ModePlasma Unable to load script file <b>%1</b> Uninstallable Use Project-Id-Version: plasmashellprivateplugin
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-23 03:23+0100
PO-Revision-Date: 2016-08-30 18:52+0300
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <kde-i18n-uk@kde.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 1.5
 &Виконати Всі віджети Категорії: Консоль скриптів оболонки стільниці Отримати нові віджети Плазми Редактор Скрипт виконується на %1 Фільтри Встановити віджет з локального файла... Невдала спроба встановлення Спроба встановлення пакунка %1 зазнала невдачі. Завантажити Новий сеанс Відкрити файл скрипту Виведення Запущено Час виконання: %1 мс Зберегти файл скрипту Блокування екрана увімкнено Очікування зберігача екрана Виберіть файл плазмоїда Визначає проміжок часу бездіяльності у хвилинах, перш ніж буде заблоковано екран. Встановлює, чи буде заблоковано екран, якщо реєструватиметься бездіяльність протягом вказаного часу. Шаблони KWin Плазма Не вдалося завантажити файл скрипту <b>%1</b> Придатний до вилучення Використати 