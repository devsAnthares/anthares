��          �      �           	          &     -     4  
   =     H     Q     Y     i  >   w     �     �     �     �  
   �     �     �     �            U   %    {     �     �     �  	   �     �     �     �  
     !        3  �   H     �  	     )   $  #   N  '   r     �     �     �  #   �     �  �                          	                                                
                                 %1 is running %1 not running &Reset &Start Advanced Appearance Command: Display Execute command Notifications Remaining time left: %1 second Remaining time left: %1 seconds Run command S&top Show notification Show seconds Show title Text: Timer Timer finished Timer is running Title: Use mouse wheel to change digits or choose from predefined timers in the context menu Project-Id-Version: plasma_applet_org.kde.plasma.timer
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2015-04-22 17:06+0300
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <kde-i18n-uk@kde.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 1.5
 %1 запущено %1 не запущено С&кинути &Пуск Додатково Вигляд Команда: Показ Виконання команди Сповіщення Лишилося часу: %1 секунда Лишилося часу: %1 секунди Лишилося часу: %1 секунд Лишилося часу: %1 секунда Виконати команду &Стоп Показувати сповіщення Показувати секунди Показувати заголовок Текст: Таймер Відлік завершено Виконується відлік Заголовок: Скористайтеся коліщатком миші для зміни цифр або виберіть один з попередньо визначених таймерів з контекстного меню 