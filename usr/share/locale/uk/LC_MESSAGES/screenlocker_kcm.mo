��          �            x     y  
   �  
   �     �  .   �     �     �     �        *   )      T  ,   u  ,   �  0   �            @        T     g     t  e   �     �  ,   �  !   #  N   E  f   �  ;   �     7  #   S  f   w  .   �                        
                                            	           &Lock screen on resume: Activation Appearance Error Failed to successfully test the screen locker. Immediately Keyboard shortcut: Lock Session Lock screen automatically after: Lock screen when waking up from suspension Re&quire password after locking: Spinbox suffix. Short for minutes min  mins Spinbox suffix. Short for seconds sec  secs The global keyboard shortcut to lock the screen. Wallpaper &Type: Project-Id-Version: screenlocker_kcm
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:10+0100
PO-Revision-Date: 2018-01-09 09:01+0200
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <kde-i18n-uk@kde.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 2.0
 &Блокувати екран після пробудження Активація Вигляд Помилка Не вдалося успішно перевірити засіб блокування екрана. одразу Клавіатурне скорочення: Заблокувати сеанс Інтервал автоматичного блокування екрана: Блокувати екран після пробудження зі стану присипляння Ви&мога пароля після блокування:  хв.  хв.  хв.  хв.  сек.  сек.  сек.  сек. Загальне клавіатурне скорочення для блокування екрана. &Тип фонового зображення: 