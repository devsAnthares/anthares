��          �      ,      �     �     �     �     �     �     �     �     �  .   �     .     L     \     m     �     �  H   �    �  3     (   ;     d  ,   |  %   �     �  *   �       X   %  �   ~  S     1   f  $   �     �     �  �   �        	                                                                  
    &Configure Printers... Active jobs only All jobs Completed jobs only Configure printer General No active jobs No jobs No printers have been configured or discovered One active job %1 active jobs One job %1 jobs Open print queue Print queue is empty Printers Search for a printer... There is one print job in the queue There are %1 print jobs in the queue Project-Id-Version: plasma_applet_org.kde.plasma.printmanager
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2015-02-21 09:37+0000
PO-Revision-Date: 2015-02-21 15:32+0200
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <kde-i18n-uk@kde.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 1.5
 &Налаштовування принтерів… Лише активні завдання Всі завдання Лише завершені завдання Налаштувати принтер Загальне Немає активних завдань Немає завдань Не налаштовано або не виявлено жодного принтера %1 активне завдання %1 активних завдання %1 активних завдань Одне активне завдання %1 завдання %1 завдання %1 завдань Одна завдання Відкрити вікно черги друку Черга друку порожня Принтери Пошук принтера… У черзі перебуває %1 завдання У черзі перебуває %1 завдання У черзі перебуває %1 завдань У черзі перебуває одне завдання 