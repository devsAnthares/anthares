��    ]           �      �  !   �       	   !     +  S   4  	   �     �     �     �     �     �     �     �     �     �     	  J   $	     o	     }	     �	      �	  	   �	  
   �	     �	     �	     �	     �	     
     
     
  L   
  	   l
  	   v
  
   �
  
   �
  
   �
     �
     �
     �
     �
     �
     �
     �
     �
     	     (  	   +     5     G     S     [     i     m     }     �     �  
   �  	   �     �  
   �     �     �     �     �     �     	          +     ?     \     r     x     |     �  H   �     �     �     �     �     �       
     &        A  "   T     w     �     �     �     �       	     �  (  T   $     y  	   �     �  2   �     �     �  
             ,     C     [     p  6   �     �  ,   �  J   �  '   H     p     �     �     �     �     �  (   �  #   "     F  %   `     �     �  f   �  	   �  	     
     
     
   $     /  "   D     g     t  6   �     �     �  /   �          ;     @     T     j     �  =   �     �     �            )   *     T     g     x      }     �     �     �  (   �  6   �  7      0   X  +   �  <   �  -   �           &     *     .  Q   5  #   �  %   �     �  "   �  (        0  
   7     B  
   S  
   ^     i     |     �     �     �     �      �           B   "       (   W   #   $   %            [   @      L   M   '   6       >   T           <   9   G   &           A      .   8   R       7       J             ?   4                   H   -                      F          5   )   N   D       ,       K             3       U   =   
       ;       O   :           +           !   Z   /      V   Y              Q         E       1       X             ]       0      P         \   I          	          C       S   2       *    
Solid Based Device Viewer Module (c) 2010 David Hubner AMD 3DNow ATI IVEC Available space out of total partition size (percent used)%1 free of %2 (%3% used) Batteries Battery Type:  Bus:  Camera Cameras Charge Status:  Charging Collapse All Compact Flash Reader Default device tooltipA Device Device Information Device Listing Whats ThisShows all the devices that are currently listed. Device Viewer Devices Discharging EMAIL OF TRANSLATORSYour emails Encrypted Expand All File System File System Type:  Fully Charged Hard Disk Drive Hotpluggable? IDE IEEE1394 Info Panel Whats ThisShows information about the currently selected device. Intel MMX Intel SSE Intel SSE2 Intel SSE3 Intel SSE4 Keyboard Keyboard + Mouse Label:  Max Speed:  Memory Stick Reader Mounted At:  Mouse Multimedia Players NAME OF TRANSLATORSYour names No No Charge No data available Not Mounted Not Set Optical Drive PDA Partition Table Primary Processor %1 Processor Number:  Processors Product:  Raid Removable? SATA SCSI SD/MMC Reader Show All Devices Show Relevant Devices Smart Media Reader Storage Drives Supported Drivers:  Supported Instruction Sets:  Supported Protocols:  UDI:  UPS USB UUID:  Udi Whats ThisShows the current device's UDI (Unique Device Identifier) Unknown Drive Unused Vendor:  Volume Space: Volume Usage:  Yes kcmdevinfo name of something is not knownUnknown no device UDINone no instruction set extensionsNone platform storage busPlatform unknown battery typeUnknown unknown deviceUnknown unknown device typeUnknown unknown storage busUnknown unknown volume usageUnknown xD Reader Project-Id-Version: kcmdevinfo
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-23 03:11+0200
PO-Revision-Date: 2016-03-09 16:34+0200
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <kde-i18n-uk@kde.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
 
Модуль перегляду пристроїв, заснований на Solid © David Hubner, 2010 AMD 3DNow ATI IVEC Вільно %1 з %2 (використано %3%) Акумулятори Тип акумулятора:  Шина:  Фотоапарат Фотоапарати Стан заряду:  Заряджання Згорнути всі Пристрій читання флеш-пам'яті Пристрій Інформація про пристрій Показує всі пристрої з поточного списку. Переглядач пристроїв Пристрої Розряджання yurchor@ukr.net Зашифровано Розгорнути всі Файлова система Тип файлової системи:  Повністю заряджено Жорсткий диск «Гаряче» з’єднання? IDE IEEE1284 Показує відомості щодо поточного позначеного пристрою. Intel MMX Intel SSE Intel SSE2 Intel SSE3 Intel SSE4 Клавіатура Клавіатура і мишка Мітка:  Макс. швидкість:  Зчитувач карток флеш-пам’яті Змонтовано до:  Миша Мультимедійні програвачі Юрій Чорноіван Ні Без заряду Немає даних Не змонтовано Не встановлено Пристрій читання оптичних дисків КПК Таблиця розділів Основний Процесор %1 Кількість процесорів:  Процесори Продукт:  RAID Змінний пристрій? SATA SCSI Зчитувач SD/MMC Показати всі пристрої Показати відповідні пристрої Пристрій читання смарт-носіїв Пристрої зберігання даних Підтримувані драйвери:  Підтримувані набори інструкцій:  Підтримувані протоколи:  UDI:  UPS USB UUID:  Показує UDI (Unique Device Identifier) поточних пристроїв Невідомий пристрій Не використовується Виробник:  Заповненість тому: Використання об’єму:  Так kcmdevinfo Невідомо Немає Немає Платформа Невідомий Невідомий Невідомий Невідомий Невідоме Зчитувач карток xD 