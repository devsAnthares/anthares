��    7      �  I   �      �     �     �     �     �     �     �               $     -     5     H     T      `     �     �     �     �     �     �     �     �                     )     7  	   C  	   M     W     d     j     �     �     �     �     �     �     �     �     �     �  @   �     7     @     \     c     j     r     �     �     �  4   �     �  �  �     �	     �	     �	  8    
     9
     N
     a
  !   
     �
  
   �
     �
     �
     �
     �
       !     F   8          �  #   �  &   �     �        &   %  "   L      o     �  !   �     �     �               *     /     M     U      b     �     �     �     �     �  |   �     V  D   v     �     �     �  2   �               +     2     G     &   0      7                  #                
          	   '   (   +                       6              3      1                   ,   2                  "       *          $      4   )               %      5                     !          /   .   -       % %1 is value, %2 is unit%1 %2 %1: Application Energy Consumption Battery Capacity Charge Percentage Charge state Charging Current Degree Celsius°C Details: %1 Discharging EMAIL OF TRANSLATORSYour emails Energy Energy Consumption Energy Consumption Statistics Environment Full design Fully charged Has power supply Kai Uwe Broulik Last 12 hours Last 2 hours Last 24 hours Last 48 hours Last 7 days Last full Last hour Manufacturer Model NAME OF TRANSLATORSYour names No Not charging PID: %1 Path: %1 Rechargeable Refresh Serial Number Shorthand for WattsW System Temperature This type of history is currently not available for this device. Timespan Timespan of data to display Vendor VoltV Voltage Wakeups per second: %1 (%2%) WattW Watt-hoursWh Yes current power draw from the battery in WConsumption literal percent sign% Project-Id-Version: kcm_energyinfo
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2016-06-17 18:39+0300
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <kde-i18n-uk@kde.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 1.5
 % %1 %2 %1: Споживання енергії програмами Акумулятор Місткість Відсоток заряду Стан зарядженості Заряджається Струм °C Подробиці: %1 Розряджання yurchor@ukr.net Енергія Витрачена енергія Статистика споживання електроенергії Середовище Заповнення Повністю заряджено Має джерело живлення Kai Uwe Broulik Попередні 12 годин Попередні дві години Попередні 24 години Попередні 48 годин Попередні 7 днів Востаннє повністю Попередня година Виробник Модель Юрій Чорноіван Ні Не заряджається PID: %1 Шлях: %1 Перезаряджуваний Оновити Серійний номер Вт Система Температура Журнал цього типу у поточній версії для цього пристрою недоступний. Часовий проміжок Часовий проміжок для показаних даних Постачальник В Напруга Пробуджень за секунду: %1 (%2%) Вт Вт-год. Так Споживання % 