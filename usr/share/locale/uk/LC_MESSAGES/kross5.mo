��    $      <  5   \      0     1  +   E     q  4   �  &   �     �     �                     5     :     P     X  ,   u  3   �     �     �               !  '   .     V     u     {     �     �     �     �     �     �  &   �       B        b  �  y  
   q     |     �  3   �     �  %   �     	      	     4	  .   F	     u	  3   �	     �	  1   �	  [   

  i   f
  L   �
  D     	   b     l     �  D   �  9   �       ;   )  .   e     �  /   �     �  1   �       F   )     p  [   |     �                                       
                              !                                  $      #                               	                           "             @info:creditAuthor @info:creditCopyright 2006 Sebastian Sauer @info:creditSebastian Sauer @info:shell command-line argumentThe script to run. @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: application descriptionCommand-line utility to run Kross scripts. application nameKross Project-Id-Version: kross5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2014-04-02 08:45+0300
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <kde-i18n-uk@kde.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
 Автор © Sebastian Sauer, 2006 Sebastian Sauer Скрипт, який слід запустити. Загальні Додати новий скрипт. Додати... Скасувати? Коментар: fr.ivan@ukrainian-orthodox.org,yurchor@ukr.net Редагування Редагувати вибраний скрипт. Редагувати... Запустити вибраний скрипт. Неможливо створити скрипт для інтерпретатора «%1» Не вдалося визначити інтерпретатор для файла скрипту «%1» Не вдалося завантажити інтерпретатор «%1» Не вдалося відкрити файл скрипту «%1». Файл: Піктограма: Інтерпретатор: Рівень безпеки для інтерпретатора Ruby о. Іван Петрущак,Юрій Чорноіван Назва: Функції з назвою «%1» не знайдено Немає інтерпретатора «%1» Вилучити Вилучити вибраний скрипт. Виконати Файла скрипту «%1» не існує. Зупинити Зупинити виконання вибраного скрипту. Текст: Засіб для запуску скриптів Kross з командного рядка. Kross 