��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  �  K     K  -   O  t  }    �	  �     !   �  !   �     �  C   	  &   M  .   t  -   �  -   �                                          
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2009-12-25 17:27+0200
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <translation@linux.org.ua>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
  с Тайм-аут показу &запуску: <H1>Сповіщення у смужці задач</H1>
Ви можете увімкнути інший метод сповіщення про запуск,
при цьому на смужці задач з'явиться кнопка з клепсидрою,
яка обертатиметься, що символізує завантаження
програми. Можливо, деякі програми не знають про сповіщення
запуску. В цьому разі, кнопка зникне після часу,
вказаного у розділі «Тайм-аут показу запуску» <h1>Курсор «працюю»</h1>
KDE пропонує курсор «працюю» для сповіщення про старт програми.
Щоб ввімкнути, виберіть тип сповіщення у комбосписку.
Можливо, деякі програми не знають про сповіщення запуску. В цьому випадку,
курсор перестане блимати через час,
вказаний у розділі «Тайм-аут показу запуску» <h1>Показ запуску</h1> Тут ви можете налаштувати відображення процесу запуску програм. Миготливий курсор Стрибаючий курсор Курсор «прац&юю» Увімкнути сповіщення у &смужці задач Без курсора «працюю» Пасивний курсор «працюю» Тайм-аут показу &запуску: &Сповіщення смужки задач 