��          �      <      �     �     �     �  7  �  �  #  +   �  ^              �     �  x   �  �   %     �               ;     U    r  /   t	  1   �	  #   �	  �  �	  '  �  O   �  �   ?  (   �  <   #     `  
  q  t  |  9   �     +  F   =  9   �  6   �                          
       	                                                                    &End current session &Restart computer &Turn off computer <h1>Session Manager</h1> You can configure the session manager here. This includes options such as whether or not the session exit (logout) should be confirmed, whether the session should be restored again when logging in and whether the computer should be automatically shut down after session exit by default. <ul>
<li><b>Restore previous session:</b> Will save all applications running on exit and restore them when they next start up</li>
<li><b>Restore manually saved session: </b> Allows the session to be saved at any time via "Save Session" in the K-Menu. This means the currently started applications will reappear when they next start up.</li>
<li><b>Start with an empty session:</b> Do not save anything. Will come up with an empty desktop on next start.</li>
</ul> Applications to be e&xcluded from sessions: Check this option if you want the session manager to display a logout confirmation dialog box. Conf&irm logout Default Leave Option General Here you can choose what should happen by default when you log out. This only has meaning, if you logged in through KDM. Here you can enter a colon or comma separated list of applications that should not be saved in sessions, and therefore will not be started when restoring a session. For example 'xterm:konsole' or 'xterm,konsole'. O&ffer shutdown options On Login Restore &manually saved session Restore &previous session Start with an empty &session Project-Id-Version: kcmsmserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2009-12-25 19:11+0200
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <translation@linux.org.ua>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
 &Завершити поточний сеанс &Перезавантажити комп'ютер &Вимкнути комп'ютер <h1>Менеджер сеансів</h1> Тут ви можете налаштувати менеджер сеансів. Налаштувати можна такі параметри, як підтвердження виходу із системи, відновлення попереднього сеансу при вході та чи потрібно автоматично вимикати комп'ютер після завершення сеансу. <ul>
<li><b>Відновлювати попередній сеанс:</b> Зберігає всі працюючі програми при виході та відновлює їх при наступному запуску</li>
<li><b>Відновлювати збережений вручну сеанс:</b> Дозволяє зберігати сеанси в будь-який момент за допомогою пункту «Зберегти сеанс» K-меню. Це означає, що програми, що зараз працюють, з'являться знову при наступному запуску.</li>
<li><b>Запускати з порожнім сеансом:</b> Не зберігати нічого. Запускається порожня стільниця.</li>
</ul> Програми, що мають бути ви&лучені з сеансів: Позначте цей пункт, якщо хочете, щоб менеджер сеансу показував вікно підтвердження виходу із системи. Підтверджувати &вихід Типовий варіант виходу з системи Загальне Тут ви можете вибрати, яку дію буде виконано після того, як ви вийдете з системи. Це має значення, лише якщо реєстрація відбувалася за допомогою KDM. Тут ви можете ввести розділений двокрапками або комами список програм, які не слід зберігати у сеансах, тобто ці програми не будуть запускатися при відновленні сеансу. Наприклад, «xterm:xconsole» або «xterm,konsole». І&нші варіанти виходу з системи При вході Відновлювати збережений &вручну сеанс Відновлювати &попередній сеанс Запускати з &порожнім сеансом 