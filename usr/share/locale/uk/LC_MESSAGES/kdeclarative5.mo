��          �            h  ~   i  +   �           5     T     s     �     �  �   �  g   A  0   �  .   �     	  @     �  Z  (  Y  T   �     �  ,   �       
   0     ;  /   X  4  �  
  �	  i   �
  H   2  +   {     �                                                   	             
                Click on the button, then enter the shortcut like you would in the program.
Example for Ctrl+A: hold the Ctrl key and press A. Conflict with Standard Application Shortcut EMAIL OF TRANSLATORSYour emails KPackage QML application shell NAME OF TRANSLATORSYour names No shortcut definedNone Reassign Reserved Shortcut The '%1' key combination is also used for the standard action "%2" that some applications use.
Do you really want to use it as a global shortcut as well? The F12 key is reserved on Windows, so cannot be used for a global shortcut.
Please choose another one. The key you just pressed is not supported by Qt. The unique name of the application (mandatory) Unsupported Key What the user inputs now will be taken as the new shortcutInput Project-Id-Version: kdeclarative5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-30 03:07+0100
PO-Revision-Date: 2015-03-05 19:48+0200
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <kde-i18n-uk@kde.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
 Натисніть кнопку, а потім введіть клавіатурне скорочення, яке ви бажаєте використовувати у програмі.
Наприклад, для Ctrl+A: натисніть клавішу Ctrl, а потім клавішу «A». Конфлікт зі скороченнями стандартних програм yurchor@ukr.net QML-оболонка програм KPackage Юрій Чорноіван Немає Перепризначити Зарезервоване скорочення Комбінація клавіш «%1» вже прив'язана до стандартної дії «%2», яку використовують багато інших програм.
Ви дійсно хочете використовувати її також як загальне скорочення? Клавішу F12 у Windows зарезервовано. Її не можна використовувати для створення загального клавіатурного скорочення.
Будь ласка, оберіть іншу клавішу. Клавіша, яку ви тільки-но використали, не підтримується Qt. Унікальна назва програми (обов’язкова) Непідтримувана клавіша Ввід 