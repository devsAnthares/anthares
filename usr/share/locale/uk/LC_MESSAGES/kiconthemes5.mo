��          �      L      �  
   �     �  >   �                  
   -     8     @     H     O  	   [     e     s     z  2   �     �     �  �  �     �     �  Q   �     C     J     Q     b     u     �     �  #   �     �     �  
   �  '   �  W   '  #        �                                   	                                                         
        &Browse... &Search: *.png *.xpm *.svg *.svgz|Icon Files (*.png *.xpm *.svg *.svgz) Actions All Applications Categories Devices Emblems Emotes Icon Source Mimetypes O&ther icons: Places S&ystem icons: Search interactively for icon names (e.g. folder). Select Icon Status Project-Id-Version: kiconthemes5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:11+0100
PO-Revision-Date: 2016-11-19 10:52+0200
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <kde-i18n-uk@kde.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
 &Навігація... &Пошук: *.png *.xpm *.svg *.svgz|Файли піктограм (*.png *.xpm *.svg *.svgz) Дії Усі Програми Категорії Пристрої Емблеми Емоції Джерело піктограми Типи MIME &Інші піктограми: Місця С&истемні піктограми: Інтерактивний пошук назв піктограм (напр. тека). Вибрати піктограму Стан 