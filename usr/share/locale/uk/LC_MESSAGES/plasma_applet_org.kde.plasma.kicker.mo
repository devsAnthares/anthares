��    _                   C   	  /   M  -   }     �     �     �     �      	     	     1	     >	     J	  
   S	     ^	     g	     p	     �	  	   �	     �	     �	     �	  ,   �	  	    
     

  
   )
     4
     L
     `
     u
     �
     �
     �
     �
     �
  	   �
     �
     �
     
               !     (  	   ;     E     ]  
   r     }     �  
   �     �     �     �  
   �     �     �               $     2     @     R     h     y     �     �     �     �  	   �     �     �     �               4     Q     j     �     �     �     �  	   �     �  ,   �          !     0     @     L     S     b     t     �  #   �     �    �     �     �  )     $   8  &   ]  -   �  A   �     �            ,   %     R     e     x     �     �     �     �  -   �  -        6  h   T     �  ;   �     
  +     +   J  -   v  $   �  "   �  $   �  .        @     Q     Y     j  #   |     �     �  !   �  
   �     �       :   $  <   _  )   �  '   �  /   �  )     N   H  
   �     �     �     �     �       !   4  %   V  %   |     �  (   �  2   �  "     A   A     �  
   �  !   �     �     �  
   �  6   �  ,   4     a  O   |  O   �  Q     4   n  4   �  6   �          !  0   2  S   c     �  +   �  0   �  +   +     W     f  $   ~  .   �  D   �  W        o         Q         Y   5           %       H       J   !              \   *       @   V   4   [       A   (   
       U   B   ]   ^       $   _         '   >       D   K      -                  ?      ,   O   0       R   8   6   W   2   I   =   X   E   #       N   C       T   G   M          3   "         9             +          S          F           ;   :                    &   <   L          P         7          .       /       1                     )            	   Z       @action opens a software center with the applicationManage '%1'... @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Desktop Add to Favorites Add to Panel (Widget) Align search results to bottom All Applications App name (Generic name)%1 (%2) Applications Apps & Docs Behavior Categories Computer Contacts Description (Name) Description only Documents Edit Application... Edit Applications... End session Expand search to bookmarks, files and emails Favorites Flatten menu to a single level Forget All Forget All Applications Forget All Contacts Forget All Documents Forget Application Forget Contact Forget Document Forget Recent Documents General Generic name (App name)%1 (%2) Hibernate Hide %1 Hide Application Icon: Lock Lock screen Logout Name (Description) Name only Often Used Applications Often Used Documents Often used On All Activities On The Current Activity Open with: Pin to Task Manager Places Power / Session Properties Reboot Recent Applications Recent Contacts Recent Documents Recently Used Recently used Removable Storage Remove from Favorites Restart computer Run Command... Run a command or a search query Save Session Search Search results Search... Searching for '%1' Session Show Contact Information... Show In Favorites Show applications as: Show often used applications Show often used contacts Show often used documents Show recent applications Show recent contacts Show recent documents Show: Shut Down Sort alphabetically Start a parallel session as a different user Suspend Suspend to RAM Suspend to disk Switch User System System actions Turn off computer Type to search. Unhide Applications in '%1' Unhide Applications in this Submenu Widgets Project-Id-Version: plasma_applet_org.kde.plasma.kicker
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2017-09-09 08:40+0200
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <kde-i18n-uk@kde.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 2.0
 Керувати «%1»… Вибрати… Спорожнити піктограму Додати на стільницю Додати до улюбленого Додати на панель (віджет) Вирівнювати результати пошуку вниз Всі програми %1 (%2) Програми Програми і документація Поведінка Категорії Комп’ютер Контакти опис (назва) лише описи Документи Змінити запис програми… Змінити список програм… Завершити сеанс Розширення пошуку на закладки, файли та електронну пошту Улюблене Спрощення меню до однорівневого Забути всі Забути про усі програми Забути про усі контакти Забути про усі документи Забути про програму Забути про контакт Забути про документ Забути недавні документи Загальне %1 (%2) Приспати Сховати %1 Приховати програму Піктограма: Заблокувати Заблокувати екран Вийти назва (опис) лише назви Часто використовувані програми Часто використовувані документи Часто використовувані На усіх просторах дій На поточному просторі дій Відкрити за допомогою: Пришпилити до панелі керування завданнями Місця Живлення/Сеанс Властивості Перезавантажити Недавні програми Недавні контакти Недавні документи Недавно використані Недавно використані Портативні носії Вилучити з улюбленого Перезавантажити комп’ютер Виконати команду… Виконати команду або запит на пошук Зберегти сеанс Пошук Результати пошуку Шукати… Шукаємо «%1» Сеанс Показати дані щодо контакту… Показати в «Улюбленому» Показ програм: Показувати часто використовувані програми Показувати часто використовувані контакти Показувати часто використовувані документи Показувати недавні програми Показувати недавні контакти Показувати недавні документи Показати: Вимкнути Впорядкувати за алфавітом Почати паралельний сеанс як інший користувач Призупинити Сон зі збереженням до RAM Сон зі збереженням на диск Перемкнути користувача Система Системні дії Вимкнути комп’ютер Введіть текст для пошуку. Скасувати приховування програм у «%1» Скасувати приховування програм у цьому підменю Віджети 