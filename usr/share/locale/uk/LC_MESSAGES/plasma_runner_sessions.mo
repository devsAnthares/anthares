��          �   %   �      P  �  Q          )  6   9  -   p     �     �     �     �  (   �  d        x     �     �     �     �     �     �                7  "   X     {     �     �    �  �  �  $   �  !   �  b   �  ?   V     �  0   �  .   �  $     \   1  �   �  "   M     p  $   }     �  
   �  
   �     �     �     �     �     �  +   �       
   %                               	                                                                      
                        <p>You have chosen to open another desktop session.<br />The current session will be hidden and a new login screen will be displayed.<br />An F-key is assigned to each session; F%1 is usually assigned to the first session, F%2 to the second session and so on. You can switch between sessions by pressing Ctrl, Alt and the appropriate F-key at the same time. Additionally, the KDE Panel and Desktop menus have actions for switching between sessions.</p> Lists all sessions Lock the screen Locks the current sessions and starts the screen saver Logs out, exiting the current desktop session New Session Reboots the computer Restart the computer Shutdown the computer Starts a new session as a different user Switches to the active session for the user :q:, or lists all active sessions if :q: is not provided Turns off the computer User sessionssessions Warning - New Session lock screen commandlock log out log out commandLogout log out commandlogout new session restart computer commandreboot restart computer commandrestart shutdown computer commandshutdown switch user switch user commandswitch switch user commandswitch :q: Project-Id-Version: plasma_runner_sessions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2011-01-11 18:06+0200
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <translation@linux.org.ua>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
 <p>Ви забажали відкрити новий стільничний сеанс.<br />Поточний сеанс буде сховано, і ви побачите нове вікно входу до системи.<br />Кожному сеансу відповідає F-клавіша; F%1, зазвичай, відповідає першому сеансу, F%2 — другому і так далі. Ви можете перемикатися між сеансами одночасним натисканням клавіш Ctrl, Alt і відповідної F-клавіші. Крім того, у меню панелі і стільниці KDE є пункти для перемикання між сеансами.</p> Список усіх сеансів Заблокувати екран Заблоковує поточні сеанси і запускає зберігач екрана Завершує поточний сеанс стільниці Новий сеанс Перезавантажує комп’ютер Перезапустити комп’ютер Вимкнути комп’ютер Запускає новий сеанс від імені іншого користувача Перемикає активний сеанс для користувача :q: або показує список всіх активних сеансів, якщо :q: не вказано Вимикає комп’ютер сеанси Увага — новий сеанс lock вийти Вийти logout новий сеанс reboot restart shutdown перемкнути користувача switch switch :q: 