��          �      \      �     �     �  
   �                :      N     o  
   }     �     �     �  	   �  (   �  \   	     f  2   t     �     �  �  �     �     �     �  ?        H  (   X  ?   �     �     �     �  9     *   J     u  T   �  �   �     �  �   �  !   n	     �	     	                                                                                 
             %1 Terms: %2 (c) 2012, Vishesh Handa Baloo Show Device id for the files EMAIL OF TRANSLATORSYour emails File Name Terms: %1 Inode number of the file to show Internal Info Maintainer NAME OF TRANSLATORSYour names No index information found Print internal info Terms: %1 The Baloo data Viewer - A debugging tool The Baloo index could not be opened. Please run "%1" to see if Baloo is enabled and working. The file urls The fileID is not equal to the actual Baloo fileID This is a bug Vishesh Handa Project-Id-Version: balooshow5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-28 03:17+0100
PO-Revision-Date: 2018-01-28 10:57+0200
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <kde-i18n-uk@kde.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 2.0
 Терміни %1: %2 © Vishesh Handa, 2012 Показ даних Baloo Ідентифікатор пристрою для файлів yurchor@ukr.net Терміни назв файлів: %1 Номер inode файла, який слід показати Внутрішні дані Супровідник Юрій Чорноіван Не знайдено даних індексування Вивести внутрішні дані Терміни: %1 Засіб перегляду даних Baloo — засіб діагностики Не вдалося відкрити покажчик Baloo. Будь ласка, віддайте команду «%1», щоб дізнатися, чи увімкнено Baloo і чи працює система. Адреси файлів Значення ідентифікатора файла не дорівнює дійсному значенню ідентифікатора файла Baloo Це вада у програмі Vishesh Handa 