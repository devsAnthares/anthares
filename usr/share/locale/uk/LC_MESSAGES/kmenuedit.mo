��    >        S   �      H  	   I  	   S     ]     e     s     y          �     �     �     �     �     �     �  
        "  ?   .     n  >   w  	   �     �     �  S   �      A     b  �  z     	     
	     	  
   '	     2	     B	     R	  
   _	     j	  A   y	     �	     �	  
   �	     �	     �	     
     #
     ,
     ;
     G
     X
     h
     |
     �
     �
     �
     �
     �
     �
               (  T   ;     �  S   �  �  �     �     �       
   $     /  	   ;     E     R  8   n     �  2   �  4   �  >   )  @   h      �     �  @   �     +  Z   >     �  .   �  <   �  �        �  C   �  �       �  #   �          !     >     \     z     �     �  h   �  @        `      o     �     �     �     �      �       (   -     V  '   n     �  ,   �  )   �  (   �  0   (  %   Y  8     <   �     �  '     �   8     �  n   �            3          1   6       2   4      !   *         #   7                    (   9          &           :   5          +                    "                        '           -          >   =   /   	      )                ;   
      .      $   %       <   ,                    8      0            [Hidden] &Comment: &Delete &Description: &Edit &File &Name: &New Submenu... &Run as a different user &Sort &Sort all by Description &Sort all by Name &Sort selection by Description &Sort selection by Name &Username: &Work path: (C) 2000-2003, Waldo Bastian, Raffaele Sandrini, Matthias Elter Advanced All submenus of '%1' will be removed. Do you want to continue? Co&mmand: Could not write to %1 Current shortcut &key: Do you want to restore the system menu? Warning: This will remove all custom menus. EMAIL OF TRANSLATORSYour emails Enable &launch feedback Following the command, you can have several place holders which will be replaced with the actual values when the actual program is run:
%f - a single file name
%F - a list of files; use for applications that can open several local files at once
%u - a single URL
%U - a list of URLs
%d - the folder of the file to open
%D - a list of folders
%i - the icon
%m - the mini-icon
%c - the caption General General options Hidden entry Item name: KDE Menu Editor KDE menu editor Main Toolbar Maintainer Matthias Elter Menu changes could not be saved because of the following problem: Menu entry to pre-select Montel Laurent Move &Down Move &Up NAME OF TRANSLATORSYour names New &Item... New Item New S&eparator New Submenu Only show in KDE Original Author Previous Maintainer Raffaele Sandrini Restore to System Menu Run in term&inal Save Menu Changes? Show hidden entries Spell Checking Spell checking Options Sub menu to pre-select Submenu name: Terminal &options: Unable to contact khotkeys. Your changes are saved, but they could not be activated. Waldo Bastian You have made changes to the menu.
Do you want to save the changes or discard them? Project-Id-Version: kmenuedit
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2013-11-09 09:57+0200
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <kde-i18n-uk@kde.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
  [сховано] &Коментар: &Вилучити &Опис: З&міни &Файл &Назва: &Нове підменю... Запускати &як інший користувач &Впорядкувати Впорядкувати все за о&писом Впорядкувати вс&е за назвами Впорядкувати позначене за &описом Впорядкувати позначене за &назвами Ім'я &користувача: &Робочий каталог: © Waldo Bastian, Raffaele Sandrini, Matthias Elter, 2000–2003 Додатково Всі підменю «%1» буде вилучено. Хочете продовжити? Ко&манда: Не вдається записати до %1 Поточне &клавіатурне скорочення: Хочете відновити системне меню? Попередження: ця дія вилучить всі налаштовані вами меню. rysin@kde.org Ввімкнути зворотній зв'язок &запуску Наступна команда може містити декілька підставних параметрів, які буде замінено їхніми фактичними значеннями при запуску програми:
%f — назва одного файла
%F — список файлів; для програм, які можуть відкривати декілька файлів відразу
%u — одна адреса (URL)
%U — список адрес (URL)
%d — тека з файлом, який потрібно відкрити
%D — список тек
%i — піктограма
%m — мініпіктограма
%c — заголовок вікна Загальне Загальні параметри Прихований запис Назва елемента: Редактор меню KDE Редактор меню KDE Головний пенал Супровід Matthias Elter Зміни в меню неможливо зберегти через наступну проблему: Пункт меню для попереднього вибору Montel Laurent Пересунути &нижче Пересунути &вище Andriy Rysin Новий &елемент... Новий елемент Новий &роздільник Нове підменю Показувати тільки в KDE Перший автор Колишній супровідник Raffaele Sandrini Відновити системне меню Запускати в терміна&лі Зберегти зміни в меню? Показати приховані записи Перевірка правопису Параметри перевірки правопису Під меню для попереднього вибору Назва підменю: &Параметри термінала: Неможливо зв’язатися з khotkeys. Ваші зміни було збережено, але програма не змогла їх задіяти. Waldo Bastian В меню було внесено зміни.
Зберегти ці зміни чи відкинути їх? 