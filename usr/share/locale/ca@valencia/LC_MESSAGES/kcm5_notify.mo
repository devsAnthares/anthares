��          �      �       0     1  �  H     �       &         ?     `     n     v     �     �  (   �  �  �     �  �  �     �     �  1   �     �               $     9     I  -   a         
                 	                                (c) 2002-2006 KDE Team <h1>System Notifications</h1>Plasma allows for a great deal of control over how you will be notified when certain events occur. There are several choices as to how you are notified:<ul><li>As the application was originally designed.</li><li>With a beep or other noise.</li><li>Via a popup dialog box with additional information.</li><li>By recording the event in a logfile without any additional visual or audible alert.</li></ul> Carsten Pfeiffer Charles Samuels Disable sounds for all of these events EMAIL OF TRANSLATORSYour emails Event source: KNotify NAME OF TRANSLATORSYour names Olivier Goffart Original implementation System Notification Control Panel Module Project-Id-Version: kcm5_notify
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-09 07:18+0200
PO-Revision-Date: 2017-08-09 21:36+0100
Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca@valencia
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
 (c) 2002-2006 l'equip del KDE <h1>Notificacions del sistema</h1>El Plasma permet un gran control sobre com se vos notificarà l'ocurrència de certs esdeveniments. Hi ha diverses opcions quant a com se vos notifica:<ul><li>Tal com es va dissenyar originalment a l'aplicació.</li><li>Amb un so d'avís o un altre so.</li><li> Via un diàleg emergent amb informació addicional.</li><li>Gravant l'esdeveniment a un fitxer de registre sense cap alerta addicional visual o auditiva.</li></ul> Carsten Pfeiffer Charles Samuels Desactiva els sons per a tots estos esdeveniments sps@sastia.com Font de l'esdeveniment: KNotify Sebastià Pla i Sanz Olivier Goffart Implementació original Mòdul de control del sistema de notificació 