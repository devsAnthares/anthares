��          4      L       `   $   a   /   �   �  �   5   �  8   �                    Finds Konsole sessions matching :q:. Lists all the Konsole sessions in your account. Project-Id-Version: plasma_runner_konsolesessions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2009-05-09 18:05+0200
Last-Translator: Joan Maspons <joanmaspons@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca@valencia
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 0.3
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
 Busca sessions del Konsole que coincidisquen amb :q:. Llista totes les sessions del Konsole del vostre compte. 