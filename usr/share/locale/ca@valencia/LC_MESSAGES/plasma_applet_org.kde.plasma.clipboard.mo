��          �      L      �     �     �     �     �          )     1     9     P     h     t  K   �     �     �     �     �            �     !        &     9  )   U          �     �     �  (   �     �     �               '     /     F     ]     c                                                  
   	                                               Change the barcode type Clear history Clipboard Contents Clipboard history is empty. Clipboard is empty Code 39 Code 93 Configure Clipboard... Creating barcode failed Data Matrix Edit contents Indicator that there are more urls in the clipboard than previews shown+%1 Invoke action QR Code Remove from history Return to Clipboard Search Show barcode Project-Id-Version: plasma_applet_org.kde.plasma.clipboard
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-10 03:07+0200
PO-Revision-Date: 2015-02-21 17:49+0100
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca@valencia
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Generator: Lokalize 1.4
 Canvia el tipus de codi de barres Neteja l'historial Contingut del porta-retalls L'historial del porta-retalls està buit. El porta-retalls està buit Codi 39 Codi 93 Configura el porta-retalls... Ha fallat la creació del codi de barres Data Matrix Edita el contingut +%1 Invoca una acció Codi QR Elimina de l'historial Torna al porta-retalls Busca Mostra el codi de barres 