��          �      ,      �     �     �     �     �     �     �     �     �  .   �     .     L     \     m     �     �  H   �  �  �     �     �               =     U     ]     x  -   �  #   �     �     �     	     '     3  P   H        	                                                                  
    &Configure Printers... Active jobs only All jobs Completed jobs only Configure printer General No active jobs No jobs No printers have been configured or discovered One active job %1 active jobs One job %1 jobs Open print queue Print queue is empty Printers Search for a printer... There is one print job in the queue There are %1 print jobs in the queue Project-Id-Version: plasma_applet_org.kde.plasma.printmanager
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2015-02-21 09:37+0000
PO-Revision-Date: 2015-02-21 19:55+0100
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca@valencia
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Generator: Lokalize 1.4
 &Configura les impressores... Només els treballs actius Tots els treballs Només els treballs completats Configura la impressora General No hi ha cap treball actiu Sense treballs No s'ha descobert o configurat cap impressora Un treball actiu %1 treballs actius Un treball %1 treballs Obri la cua d'impressió La cua d'impressió és buida Impressores Busca impressores... Hi ha un treball d'impressió en la cua Hi ha %1 treballs d'impressió en la cua 