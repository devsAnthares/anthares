��          �      L      �  &   �  +   �       @     	   ]     g     �     �     �     �  (   �     �     �  ,   �               +     7  �  ;  "   !  #   D     h     p     x     �     �  	   �     �     �      �     �     �  7        :     B     S     d        
                          	                                                                  Do you want to suspend to RAM (sleep)? Do you want to suspend to disk (hibernate)? General Heading for list of actions (leave, lock, shutdown, ...)Actions Hibernate Hibernate (suspend to disk) Leave Leave... Lock Lock the screen Logout, turn off or restart the computer No Sleep (suspend to RAM) Start a parallel session as a different user Suspend Switch User Switch user Yes Project-Id-Version: plasma_applet_org.kde.plasma.lock_logout
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2015-07-05 15:28+0200
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca@valencia
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Generator: Lokalize 1.5
 Voleu suspendre a la RAM (dormir)? Voleu suspendre al disc (hibernar)? General Accions Hiberna Hiberna (suspèn al disc) Ix Eixida... Bloqueja Bloqueja la pantalla Ix, apaga o reinicia l'ordinador No Adorm (suspèn a la RAM) Inicia una sessió en paral·lel amb un usuari diferent Suspèn Commuta d'usuari Commuta d'usuari Sí 