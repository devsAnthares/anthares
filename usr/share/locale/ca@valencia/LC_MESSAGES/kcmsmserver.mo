��          �      <      �     �     �     �  7  �  �  #  +   �  ^              �     �  x   �  �   %     �               ;     U  �  r     ;	     X	     u	    �	    �
  -   �  a   �     @     S     p  �   x  �        �       (   )     R     p                          
       	                                                                    &End current session &Restart computer &Turn off computer <h1>Session Manager</h1> You can configure the session manager here. This includes options such as whether or not the session exit (logout) should be confirmed, whether the session should be restored again when logging in and whether the computer should be automatically shut down after session exit by default. <ul>
<li><b>Restore previous session:</b> Will save all applications running on exit and restore them when they next start up</li>
<li><b>Restore manually saved session: </b> Allows the session to be saved at any time via "Save Session" in the K-Menu. This means the currently started applications will reappear when they next start up.</li>
<li><b>Start with an empty session:</b> Do not save anything. Will come up with an empty desktop on next start.</li>
</ul> Applications to be e&xcluded from sessions: Check this option if you want the session manager to display a logout confirmation dialog box. Conf&irm logout Default Leave Option General Here you can choose what should happen by default when you log out. This only has meaning, if you logged in through KDM. Here you can enter a colon or comma separated list of applications that should not be saved in sessions, and therefore will not be started when restoring a session. For example 'xterm:konsole' or 'xterm,konsole'. O&ffer shutdown options On Login Restore &manually saved session Restore &previous session Start with an empty &session Project-Id-Version: kcmsmserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2011-09-30 20:56+0200
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca@valencia
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
 &Finalitza la sessió actual &Torna a engegar l'ordinador Apa&ga l'ordinador <h1>Gestor de sessió</h1> Ací podeu configurar el gestor de sessió. Això inclou opcions com ara si s'ha de confirmar o no l'eixida de sessió, si s'ha de tornar a restaurar la sessió en tornar-se a connectar i si l'ordinador s'ha d'apagar automàticament en eixir de la sessió. <ul>
<li><b>Restaura la sessió anterior:</b> Quan isca desarà l'estat de totes les aplicacions en execució i les restaurarà en el següent inici.</li>
<li><b>Restaura la sessió guardada manualment: </b> Permet guardar la sessió en qualsevol moment mitjançant l'element del menú K «Guarda la sessió». Això vol dir que les aplicacions en execució apareixeran en el següent inici.</li>
<li><b>Inicia amb una sessió buida:</b> No guarda res. En el següent inici es començarà amb un escriptori buit.</li>
</ul> Aplicacions que s'e&xclouran de les sessions: Marqueu esta opció si voleu que el gestor de sessió mostre un diàleg de confirmació d'eixida. Con&firma l'eixida Opció d'eixida per omissió General Ací podeu triar que hauria de passar per omissió quan vos desconnecteu. Això només té significat si vos heu connectat mitjançant KDM. Ací podeu introduir, separades per dos punts o per comes, una llista de les aplicacions que no s'haurien de guardar a les sessions, i que, per tant, no s'iniciaran quan es restauri una sessió. Per exemple «xterm:konsole» o «xterm,konsole». O&fereix les opcions d'aturada En connectar-se Restaura la sessió guardada &manualment Restaura la sessió &anterior Comença amb una s&essió buida 