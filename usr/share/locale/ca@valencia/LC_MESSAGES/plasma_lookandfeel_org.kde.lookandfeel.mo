��    2      �  C   <      H     I     M     R  .   a  5   �     �     �     �     �  	   �     �          '     6  1   J     |     �     �     �  
   �     �  '   �     �     �     �          !  '   (     P     _     f     n     �  5   �     �     �     �     �     �  $   �  A   #  �   e     K     R  C   c  '   �     �     �     �  �  	     �
     �
     �
          $  
   3  (   >     g     m  	   |  !   �     �     �     �  M   �     :     B     U     s     z      �  	   �     �     �     �     �     �  E   �     C     U     ]      f     �  7   �     �     �     �     �               !     1     A     M     f     ~     �     �  #   �               $   0       .                      %                ,         )   2          /   "                        &   1      '       +   !   	         -                               
         (                    *            #       %1% Back Battery at %1% Button to change keyboard layoutSwitch layout Button to show/hide virtual keyboardVirtual Keyboard Cancel Caps Lock is on Close Close Search Configure Configure Search Plugins Desktop Session: %1 Different User Keyboard Layout: %1 Logging out in 1 second Logging out in %1 seconds Login Login Failed Login as different user Logout Next track No media playing Nobody logged in on that sessionUnused OK Password Play or Pause media Previous track Reboot Reboot in 1 second Reboot in %1 seconds Recent Queries Remove Restart Show media controls: Shutdown Shutting down in 1 second Shutting down in %1 seconds Start New Session Suspend Switch Switch Session Switch User Textfield placeholder textSearch... Textfield placeholder text, query specific KRunnerSearch '%1'... This is the first text the user sees while starting in the splash screen, should be translated as something short, is a form that can be seen on a product. Plasma is the project name so shouldn't be translated.Plasma made by KDE Unlock Unlocking failed User logged in on console (X display number)on TTY %1 (Display %2) User logged in on console numberTTY %1 Username Username (location)%1 (%2) in category recent queries Project-Id-Version: plasma_lookandfeel_org.kde.lookandfeel
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-09 03:21+0100
PO-Revision-Date: 2018-01-09 20:26+0100
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca@valencia
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Generator: Lokalize 2.0
 %1% Arrere Bateria al %1% Commuta la disposició Teclat virtual Cancel·la El bloqueig de majúscules està activat Tanca Tanca la busca Configura Configura els connectors de busca Sessió d'escriptori: %1 Usuari diferent Disposició del teclat: %1 La sessió es tancarà d'ací 1 segon La sessió es tancarà d'ací %1 segons Entrada L'accés ha fallat Accés com un usuari diferent Eixida Peça següent No s'està reproduint cap suport Sense ús D'acord Contrasenya Reprodueix o pausa el suport Peça anterior Reinicia Es tornarà a arrencar en 1 segon Es tornarà a arrencar en %1 segons Consultes recents Elimina Reinicia Mostra els controls multimèdia: Atura Es tancarà d'ací 1 segon Es tancarà d'ací %1 segons Inicia una sessió nova Suspèn Commuta Commutació de la sessió Commuta d'usuari Busca... Busca «%1»... Plasma, del KDE Desbloqueja Ha fallat el desbloqueig al TTY %1 (pantalla %2) TTY %1 Nom d'usuari %1 (%2) a la categoria de consultes recents 