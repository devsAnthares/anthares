��          �      l      �     �     �     �  *         +  >   ?  9   ~     �     �  
   �      �       	        (  !   :     \  $   {  	   �     �  �   �  �  8                    -     J  G   a  D   �     �          *     7     H     P     X  (   r     �  0   �     �     �  �   �                              
                                                  	                  % &Critical level: &Low level: <b>Battery Levels                     </b> A&t critical level: Battery will be considered critical when it reaches this level Battery will be considered low when it reaches this level Configure Notifications... Critical battery level Do nothing EMAIL OF TRANSLATORSYour emails Enabled Hibernate Low battery level Low level for peripheral devices: NAME OF TRANSLATORSYour names Pause media players when suspending: Shut down Suspend The Power Management Service appears not to be running.
This can be solved by starting or scheduling it inside "Startup and Shutdown" Project-Id-Version: powerdevilglobalconfig
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-11 03:05+0200
PO-Revision-Date: 2017-12-17 23:30+0100
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca@valencia
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
 % Nivell &crític: Nivell &baix: <b>Nivells de la bateria</b> E&n el nivell crític: Es considerarà que la bateria està crítica quan arribe a este nivell Es considerarà que la bateria està baixa quan arribe a este nivell Configura les notificacions... Nivell crític de la bateria No faces res txemaq@gmail.com Activat Hiberna Nivell baix de la bateria Nivell baix per dispositius perifèrics: Josep Ma. Ferrer Pausa els reproductors multimèdia en suspendre: Atura Suspèn Pareix que el servei de gestió d'energia no s'està executant.
Això es pot solucionar engegant o planificant-lo des d'«Inici i aturada». 