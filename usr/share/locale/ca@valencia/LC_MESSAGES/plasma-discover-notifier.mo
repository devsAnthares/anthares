��          t      �         P     )   b  %   �  @   �     �  V   	     `     {     �     �  �  �     |  0   �  =   �  /   �  #   "  `   F  (   �     �  
   �     �                         	      
                             %1 is '%1 packages to update' and %2 is 'of which %1 is security updates'%1, %2 1 package to update %1 packages to update 1 security update %1 security updates First part of '%1, %2'1 package to update %1 packages to update No packages to update Second part of '%1, %2'of which 1 is security update of which %1 are security updates Security updates available System up to date Update Updates available Project-Id-Version: plasma-discover-notifier
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-15 03:15+0100
PO-Revision-Date: 2018-01-15 19:04+0100
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca@valencia
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Generator: Lokalize 2.0
 %1, %2 %1 paquet a actualitzar %1 paquets a actualitzar 1 actualització de seguretat %1 actualitzacions de seguretat 1 paquet a actualitzar %1 paquets a actualitzar No hi ha cap paquet per actualitzar dels quals 1 és una actualització de seguretat dels quals %1 són actualitzacions de seguretat Actualitzacions de seguretat disponibles El sistema està actualitzat Actualitza Actualitzacions disponibles 