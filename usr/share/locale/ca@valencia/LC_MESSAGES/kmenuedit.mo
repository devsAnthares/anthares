��    >        S   �      H  	   I  	   S     ]     e     s     y          �     �     �     �     �     �     �  
        "  ?   .     n  >   w  	   �     �     �  S   �      A     b  �  z     	     
	     	  
   '	     2	     B	     R	  
   _	     j	  A   y	     �	     �	  
   �	     �	     �	     
     #
     ,
     ;
     G
     X
     h
     |
     �
     �
     �
     �
     �
     �
               (  T   ;     �  S   �  �  �     �     �  
   �     �     �     �     �               5     =     \  $   s     �     �     �  ?   �       ;         \     c       ]   �     �  (     �  /     �     �     �     �          $     9  
   Q     \  I   k  !   �     �     �     �     �     	          %     4     A     Y     h     y     �     �     �     �     �  '        :     T     f  `   }     �  B   �            3          1   6       2   4      !   *         #   7                    (   9          &           :   5          +                    "                        '           -          >   =   /   	      )                ;   
      .      $   %       <   ,                    8      0            [Hidden] &Comment: &Delete &Description: &Edit &File &Name: &New Submenu... &Run as a different user &Sort &Sort all by Description &Sort all by Name &Sort selection by Description &Sort selection by Name &Username: &Work path: (C) 2000-2003, Waldo Bastian, Raffaele Sandrini, Matthias Elter Advanced All submenus of '%1' will be removed. Do you want to continue? Co&mmand: Could not write to %1 Current shortcut &key: Do you want to restore the system menu? Warning: This will remove all custom menus. EMAIL OF TRANSLATORSYour emails Enable &launch feedback Following the command, you can have several place holders which will be replaced with the actual values when the actual program is run:
%f - a single file name
%F - a list of files; use for applications that can open several local files at once
%u - a single URL
%U - a list of URLs
%d - the folder of the file to open
%D - a list of folders
%i - the icon
%m - the mini-icon
%c - the caption General General options Hidden entry Item name: KDE Menu Editor KDE menu editor Main Toolbar Maintainer Matthias Elter Menu changes could not be saved because of the following problem: Menu entry to pre-select Montel Laurent Move &Down Move &Up NAME OF TRANSLATORSYour names New &Item... New Item New S&eparator New Submenu Only show in KDE Original Author Previous Maintainer Raffaele Sandrini Restore to System Menu Run in term&inal Save Menu Changes? Show hidden entries Spell Checking Spell checking Options Sub menu to pre-select Submenu name: Terminal &options: Unable to contact khotkeys. Your changes are saved, but they could not be activated. Waldo Bastian You have made changes to the menu.
Do you want to save the changes or discard them? Project-Id-Version: kmenuedit
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2016-07-18 14:26+0100
Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca@valencia
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
  [Ocult] &Comentari: Su&primeix &Descripció: E&dita &Fitxer &Nom: Submenú &nou... Executa com a usuari &diferent &Ordena &Ordena-ho tot per descripció &Ordena-ho tot per nom &Ordena la selecció per descripció &Ordena la selecció per nom Nom d'&usuari: Camí de &treball: (C) 2000-2003, Waldo Bastian, Raffaele Sandrini, Matthias Elter Avançat S'eliminaran tots els submenús de «%1». Voleu continuar? &Orde: No s'ha pogut escriure a %1 &Tecla de drecera actual: Voleu restaurar el menú del sistema? Avís: això eliminarà tots els menús personalitzats. sps@sastia.com Habilita la &confirmació de llançament A continuació de l'orde podeu tindre diverses reserves de lloc que se substituiran amb els valors reals quan s'execute el programa:
%f - un sol nom de fitxer
%F - una llista de fitxers; useu-ho per a les aplicacions que poden obrir diversos fitxers locals a la vegada
%u - un sol URL
%U - una llista d'URL
%d - el directori del fitxer a obrir
%D - una llista de directoris
%i - la icona
%m - la mini icona
%c - el títol General Opcions generals Entrada oculta Nom de l'element: Editor del menú KDE Editor del menú KDE Barra d'eines principal Mantenidor Matthias Elter No s'han pogut guardar els canvis al menú a causa del problema següent: Entrada de menú a preseleccionar Montel Laurent A&vall Am&unt Sebastià Pla i Sanz &Element nou... Element nou &Separador nou Submenú nou Mostra només en el KDE Autor original Mantenidor previ Raffaele Sandrini Restaura al menú del sistema Executa en un &terminal Deso els canvis del menú? Mostra les entrades ocultes Verificació ortogràfica Opcions de la verificació ortogràfica Submenú a preseleccionar Nom del submenú: &Opcions del terminal: No s'ha pogut contactar amb el «khotkeys». Els canvis s'han guardat però no es poden activar. Waldo Bastian Heu fet canvis al menú.
Voleu guardar els canvis o descartar-los? 