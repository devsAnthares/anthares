��    3      �  G   L      h  6   i  M   �  K   �     :  '   C     k     r  �   �     ;  	   R  A   \  >   �  9   �  9     9   Q  W   �  E   �     )     :      @     a  7   ~      �     �     �  X   �     X	     `	     v	  �   �	     '
     F
     L
     c
  
   s
  
   ~
     �
     �
  E  �
          &     <  w   O     �     �     �     �     �  	          �  #  ?   �  j   3  k   �     
  1        C     J  �   d     I  
   b     m  	   |     �     �     �  *   �     �     �     �        *     ;   <  '   x     �     �  p   �     A  !   N  (   p  �   �     N     _  (   c     �  
   �  
   �     �     �  m  �  (   3  &   \     �  �   �     '     9  	   ?     I  *   Z  
   �     �         /   +   )               ,      .                 	          &                       %   #                       '       0       3                         -       1   2                        (   
      *   !         $                           "    "Full screen repaints" can cause performance problems. "Only when cheap" only prevents tearing for full screen changes like a video. "Re-use screen content" causes severe performance problems on MESA drivers. Accurate Allow applications to block compositing Always Animation speed: Applications can set a hint to block compositing when the window is open.
 This brings performance improvements for e.g. games.
 The setting can be overruled by window-specific rules. Author: %1
License: %2 Automatic Category of Desktop Effects, used as section headerAccessibility Category of Desktop Effects, used as section headerAppearance Category of Desktop Effects, used as section headerCandy Category of Desktop Effects, used as section headerFocus Category of Desktop Effects, used as section headerTools Category of Desktop Effects, used as section headerVirtual Desktop Switching Animation Category of Desktop Effects, used as section headerWindow Management Configure filter Crisp EMAIL OF TRANSLATORSYour emails Enable compositor on startup Exclude Desktop Effects not supported by the Compositor Exclude internal Desktop Effects Full screen repaints Get New Effects... Hint: To find out or configure how to activate an effect, look at the effect's settings. Instant KWin development team Keep window thumbnails: Keeping the window thumbnail always interferes with the minimized state of windows. This can result in windows not suspending their work when minimized. NAME OF TRANSLATORSYour names Never Only for Shown Windows Only when cheap OpenGL 2.0 OpenGL 3.1 OpenGL Platform InterfaceEGL OpenGL Platform InterfaceGLX OpenGL compositing (the default) has crashed KWin in the past.
This was most likely due to a driver bug.
If you think that you have meanwhile upgraded to a stable driver,
you can reset this protection but be aware that this might result in an immediate crash!
Alternatively, you might want to use the XRender backend instead. Re-enable OpenGL detection Re-use screen content Rendering backend: Scale method "Accurate" is not supported by all hardware and can cause performance regressions and rendering artifacts. Scale method: Search Smooth Smooth (slower) Tearing prevention ("vsync"): Very slow XRender Project-Id-Version: kcmkwincompositing
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2017-10-17 20:26+0100
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca@valencia
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
 «Repinta tota la pantalla» pot causar problemes de rendiment. «Només quan siga econòmic» només prevé el parpelleig dels canvis a pantalla completa, com un vídeo. «Reutilitza el contingut de la pantalla» causa greus problemes de rendiment amb els controladors de MESA. Acurat Permet que les aplicacions bloquin la composició Sempre Velocitat de l'animació: Les aplicacions poden establir un senyal per blocar la composició quan la finestra és oberta.
 Això aporta millores de rendiment als jocs, per exemple.
 El paràmetre es pot sobreescriure amb regles específiques de finestra. Autor: %1
Llicència: %2 Automàtic Accessibilitat Aparença Llaminadures Focus Eines Animació del canvi d'escriptoris virtuals Gestió de les finestres Configura el filtre Remarcat txemaq@gmail.com Activa el compositor en la inicialització Exclou els efectes d'escriptori no acceptats pel compositor Exclou els efectes d'escriptori interns Repinta tota la pantalla Obtén efectes nous... Consell: per trobar els detalls o configurar la manera d'activar un efecte, examineu l'arranjament dels efectes. Instantània Equip de desenvolupament del KWin Mantén les miniatures de les finestres: Mantindre la miniatura de la finestra sempre interfereix amb l'estat minimitzat de les finestres. Això pot resultar en finestres que no suspenen el seu treball quan es minimitzen. Josep Ma. Ferrer Mai Només per a les finestres visualitzades Només quan siga econòmic OpenGL 2.0 OpenGL 3.1 EGL GLX La composició OpenGL (per defecte) ha trencat el KWin en el passat.
Probablement era degut a una fallada en el controlador.
Si penseu que mentrestant l'heu actualitzat a un controlador estable, podeu reiniciar esta protecció però sigueu conscient que podria produir-se una fallada immediata!
De manera alternativa, en el seu lloc podríeu usar el dorsal XRender. Torna a activar la detecció de l'OpenGL Reutilitza el contingut de la pantalla Dorsal de renderització: El mètode d'escala «Exacta» no és admés per tot el maquinari i pot provocar regressions al rendiment i objectes a la representació. Mètode d'escala: Busca Suavitzat Suau (més lent) Prevenció de l'esquinçament («vsync»): Molt lenta XRender 