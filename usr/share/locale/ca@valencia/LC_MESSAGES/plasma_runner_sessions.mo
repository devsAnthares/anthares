��          �   %   �      P  �  Q          )  6   9  -   p     �     �     �     �  (   �  d        x     �     �     �     �     �     �                7  "   X     {     �     �  �  �  �  �     n
     �
  <   �
  0   �
               5     J  .   \  i   �     �               %     .     5     <     C     P     Y     a     i     z     �                               	                                                                      
                        <p>You have chosen to open another desktop session.<br />The current session will be hidden and a new login screen will be displayed.<br />An F-key is assigned to each session; F%1 is usually assigned to the first session, F%2 to the second session and so on. You can switch between sessions by pressing Ctrl, Alt and the appropriate F-key at the same time. Additionally, the KDE Panel and Desktop menus have actions for switching between sessions.</p> Lists all sessions Lock the screen Locks the current sessions and starts the screen saver Logs out, exiting the current desktop session New Session Reboots the computer Restart the computer Shutdown the computer Starts a new session as a different user Switches to the active session for the user :q:, or lists all active sessions if :q: is not provided Turns off the computer User sessionssessions Warning - New Session lock screen commandlock log out log out commandLogout log out commandlogout new session restart computer commandreboot restart computer commandrestart shutdown computer commandshutdown switch user switch user commandswitch switch user commandswitch :q: Project-Id-Version: plasma_runner_sessions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2011-04-09 19:44+0200
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca@valencia
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
 <p>Heu triat d'obrir una altra sessió d'escriptori.<br />La sessió actual s'amagarà i es mostrarà una nova pantalla de connexió.<br />S'assigna una tecla Fn a cada sessió; F%1 normalment s'assigna a la primera sessió, F%2 a la segona sessió i així successivament. Podeu commutar entre les sessions prement simultàniament Ctrl, Alt i la tecla Fn apropiada. Addicionalment, el plafó del KDE i els menús d'escriptori tenen accions per commutar entre sessions.</p> Llista totes les sessions Bloqueja la pantalla Bloqueja les sessions actuals i engega l'estalvi de pantalla Ix, desconnectant la sessió actual d'escriptori Sessió nova Torna a arrencar l'ordinador Reinicia l'ordinador Atura l'ordinador Inicia una sessió nova amb un usuari diferent Commuta a la sessió activa de l'usuari :q:, o llista totes les sessions actives si no es proporciona :q: Apaga l'ordinador sessions Avís - Sessió nova bloqueja eixida Eixida eixida sessió nova rearranc reinici aturada commuta d'usuari commuta commuta :q: 