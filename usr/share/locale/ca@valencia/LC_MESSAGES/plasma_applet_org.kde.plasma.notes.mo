��          �   %   �      0     1     E     X     l          �     �     �     �  
   �     �     �                                    "     0  	   <     F     L  �  S     3     K     c     {     �     �     �     �     �  	        !     '     ,     4     9     A     I     N     S     Z  
   f     q     w     	                                                                          
                                                A black sticky note A blue sticky note A green sticky note A pink sticky note A red sticky note A translucent sticky note A white sticky note A yellow sticky note An orange sticky note Appearance Black Blue Bold Green Italic Orange Pink Red Strikethrough Translucent Underline White Yellow Project-Id-Version: plasma_applet_org.kde.plasma.notes
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-07-09 03:12+0200
PO-Revision-Date: 2015-07-25 13:34+0200
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca@valencia
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
 Una nota adhesiva negra Una nota adhesiva blava Una nota adhesiva verda Una nota adhesiva rosa Una nota adhesiva vermella Una nota adhesiva translúcida Una nota adhesiva blanca Una nota adhesiva groga Una nota adhesiva taronja Aparença Negre Blau Negreta Verd Cursiva Taronja Rosa Roig Barrat Translúcid Subratllat Blanc Groc 