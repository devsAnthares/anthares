��          �            h     i     �  #   �      �      �     �  J        [  &   y     �  T   �       *   $     O  �  ]  '   )  $   Q  #   v     �     �     �  e   �     K  '   j     �  U   �     �  -   	     7                            	                                             
       Also index file content Configure File Search Copyright 2007-2010 Sebastian Trüg Do not search in these locations EMAIL OF TRANSLATORSYour emails Enable File Search File Search helps you quickly locate all your files based on their content Folder %1 is already excluded Folder's parent %1 is already excluded NAME OF TRANSLATORSYour names Not allowed to exclude root folder, please disable File Search if you do not want it Sebastian Trüg Select the folder which should be excluded Vishesh Handa Project-Id-Version: kcm5_baloofile
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2017-10-17 20:26+0100
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca@valencia
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Generator: Lokalize 2.0
 Indexa també el contingut dels fitxers Configuració de la busca de fitxers Copyright 2007-2010 Sebastian Trüg No cerquis en estes ubicacions txemaq@gmail.com Activa la busca de fitxers La busca de fitxers vos ajuda a localitzar ràpidament tots els fitxers basant-se en el seu contingut La carpeta %1 ja està exclosa La carpeta superior %1 ja està exclosa Josep Ma. Ferrer No es permet excloure la carpeta arrel. Desactiveu la busca de fitxers si no ho voleu Sebastian Trüg Seleccioneu les carpetes que s'han d'excloure Vishesh Handa 