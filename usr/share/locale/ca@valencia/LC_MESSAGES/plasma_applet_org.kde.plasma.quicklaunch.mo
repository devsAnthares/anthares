��          �      <      �     �  <   �  
   �     	          &     3     ?  
   G     R     c     q     }     �     �  
   �     �  �  �     �  M   �  	             #      7     X     m     u     �     �     �     �     �      �                                                            
   	                                                Add Launcher... Add launchers by Drag and Drop or by using the context menu. Appearance Arrangement Edit Launcher... Enable popup Enter title General Hide icons Maximum columns: Maximum rows: Quicklaunch Remove Launcher Show hidden icons Show launcher names Show title Title Project-Id-Version: plasma_applet_org.kde.plasma.quicklaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-02-26 04:09+0100
PO-Revision-Date: 2016-01-19 21:09+0100
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca@valencia
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Generator: Lokalize 1.5
 Afig llançador... Afig llançadors arrossegant i deixant anar o utilitzant el menú contextual. Aparença Organització Edita llançador... Habilita les finestres emergents Introduïu el títol General Oculta les icones Màxim de columnes: Màxim de files: Quicklaunch Elimina llançador Mostra les icones ocultes Mostra els noms dels llançadors Mostra el títol Títol 