��          �      \      �     �     �  
   �                :      N     o  
   }     �     �     �  	   �  (   �  \   	     f  2   t     �     �  �  �     �     �     �     �     �     �  $        3  
   G     R  &   c      �  
   �  <   �  p   �     d  @   w     �     �     	                                                                                 
             %1 Terms: %2 (c) 2012, Vishesh Handa Baloo Show Device id for the files EMAIL OF TRANSLATORSYour emails File Name Terms: %1 Inode number of the file to show Internal Info Maintainer NAME OF TRANSLATORSYour names No index information found Print internal info Terms: %1 The Baloo data Viewer - A debugging tool The Baloo index could not be opened. Please run "%1" to see if Baloo is enabled and working. The file urls The fileID is not equal to the actual Baloo fileID This is a bug Vishesh Handa Project-Id-Version: balooshow5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-28 03:17+0100
PO-Revision-Date: 2018-01-28 12:26+0100
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca@valencia
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Generator: Lokalize 2.0
 Termes %1: %2 (c) 2012, Vishesh Handa Vista del Baloo ID del dispositiu dels fitxers txemaq@gmail.com Termes del nom de fitxer: %1 Número d'inode del fitxer a mostrar Informació interna Mantenidor Josep Ma. Ferrer No s'ha trobat informació de l'índex Imprimeix la informació interna Termes: %1 El visualitzador de dades del Baloo - Una eina de depuració No s'ha pogut obrir l'índex del Baloo. Executeu «%1» per veure si el Baloo està habilitat i en funcionament. Els URL del fitxer L'ID del fitxer no és igual que l'ID del fitxer real del Baloo. Això és un error Vishesh Handa 