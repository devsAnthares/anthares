��    _                   C   	  /   M  -   }     �     �     �     �      	     	     1	     >	     J	  
   S	     ^	     g	     p	     �	  	   �	     �	     �	     �	  ,   �	  	    
     

  
   )
     4
     L
     `
     u
     �
     �
     �
     �
     �
  	   �
     �
     �
     
               !     (  	   ;     E     ]  
   r     }     �  
   �     �     �     �  
   �     �     �               $     2     @     R     h     y     �     �     �     �  	   �     �     �     �               4     Q     j     �     �     �     �  	   �     �  ,   �          !     0     @     L     S     b     t     �  #   �     �  �  �     �     �     �     �     �     �  3        J     `     h     t     �  
   �  	   �  	   �     �     �  	   �     �             H   .  	   w  !   �     �     �     �     �               )     <     Y     a     i  	   q     {     �     �     �     �     �     �     �     �               ,  	   A     K     e     k  
   }     �     �     �     �     �     �     �               8  (   L     u     �     �     �     �     �  %   �     �       $   !  !   F  !   h     �     �     �     �     �     �  7        @     H     Y     i     z     �     �     �     �  &   �              Q         Y   5           %       H       J   !              \   *       @   V   4   [       A   (   
       U   B   ]   ^       $   _         '   >       D   K      -                  ?      ,   O   0       R   8   6   W   2   I   =   X   E   #       N   C       T   G   M          3   "         9             +          S          F           ;   :                    &   <   L          P         7          .       /       1                     )            	   Z       @action opens a software center with the applicationManage '%1'... @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Desktop Add to Favorites Add to Panel (Widget) Align search results to bottom All Applications App name (Generic name)%1 (%2) Applications Apps & Docs Behavior Categories Computer Contacts Description (Name) Description only Documents Edit Application... Edit Applications... End session Expand search to bookmarks, files and emails Favorites Flatten menu to a single level Forget All Forget All Applications Forget All Contacts Forget All Documents Forget Application Forget Contact Forget Document Forget Recent Documents General Generic name (App name)%1 (%2) Hibernate Hide %1 Hide Application Icon: Lock Lock screen Logout Name (Description) Name only Often Used Applications Often Used Documents Often used On All Activities On The Current Activity Open with: Pin to Task Manager Places Power / Session Properties Reboot Recent Applications Recent Contacts Recent Documents Recently Used Recently used Removable Storage Remove from Favorites Restart computer Run Command... Run a command or a search query Save Session Search Search results Search... Searching for '%1' Session Show Contact Information... Show In Favorites Show applications as: Show often used applications Show often used contacts Show often used documents Show recent applications Show recent contacts Show recent documents Show: Shut Down Sort alphabetically Start a parallel session as a different user Suspend Suspend to RAM Suspend to disk Switch User System System actions Turn off computer Type to search. Unhide Applications in '%1' Unhide Applications in this Submenu Widgets Project-Id-Version: plasma_applet_org.kde.plasma.kicker
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2017-12-17 23:13+0100
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca@valencia
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Generator: Lokalize 2.0
 Gestiona «%1»... Tria... Neteja la icona Afig a l'escriptori Afig als preferits Afig al plafó (estri) Alinea els resultats de la busca a la part inferior Totes les aplicacions %1 (%2) Aplicacions Aplicacions i documents Comportament Categories Ordinador Contactes Descripció (Nom) Només la descripció Documents Edita l'aplicació... Edita les aplicacions... Finalitza la sessió Amplia la busca a les adreces d'interés, fitxers i correus electrònics Preferits Aplana el menú a un únic nivell Oblida-ho tot Oblida totes les aplicacions Oblida tots els contactes Oblida tots els documents Oblida l'aplicació Oblida el contacte Oblida el document Oblida els documents recents General %1 (%2) Hiberna Oculta %1 Oculta l'aplicació Icona: Bloqueja Bloqueja la pantalla Ix Nom (Descripció) Només el nom Aplicacions usades sovint Documents usats sovint Usats sovint A totes les activitats A l'activitat actual Obri amb: Fixa al gestor de tasques Llocs Energia / Sessió Propietats Reinicia Aplicacions recents Contactes recents Documents recents Usats recentment Usats recentment Emmagatzematge extraïble Elimina dels preferits Torna a engegar l'ordinador Executa una orde... Executa una orde o una consulta de busca Guarda la sessió Busca Resultat de la busca Busca... S'està cercant «%1» Sessió Mostra la informació del contacte... Mostra als preferits Mostra les aplicacions per: Mostra les aplicacions usades sovint Mostra els contactes usats sovint Mostra els documents usats sovint Mostra les aplicacions recents Mostra els contactes recents Mostra els documents recents Mostra: Atura Ordena alfabèticament Inicia una sessió en paral·lel amb un usuari diferent Suspèn Suspèn a la RAM Suspèn al disc Commuta d'usuari Sistema Accions del sistema Apaga l'ordinador Escriviu per buscar. Aplicacions a mostrar en «%1» Aplicacions a mostrar en este submenú Estris 