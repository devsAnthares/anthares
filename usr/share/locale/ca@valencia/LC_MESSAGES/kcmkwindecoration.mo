��            )   �      �  !   �  "   �  '   �  ,     #   ;  &   _  !   �  &   �  '   �     �                 V   $  1   {     �  *   �     �        
     
   "     -     6     ;     D     T     [     a     g  �  p     H     O     T     `     u     |  	   �     �  	   �     �     �     �     �  W   �  2   4     g  6   x  &   �     �     �     	  	   	     	  	   %	     /	     F	     L	     R	     W	                                                                          
                                                    	           @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Borders @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large Application menu Border si&ze: Buttons Close Close by double clicking:
 To open the menu, keep the button pressed until it appears. Close windows by double clicking &the menu button Context help Drag buttons between here and the titlebar Drop here to remove button Get New Decorations... Keep above Keep below Maximize Menu Minimize On all desktops Search Shade Theme Titlebar Project-Id-Version: kcmkwindecoration
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-04-14 02:49+0200
PO-Revision-Date: 2017-02-15 17:23+0100
Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca@valencia
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
 Enorme Gran Sense vores Sense vores laterals Normal Fora de mida Minúscul Molt enorme Molt gran Menú d'aplicacions Mida de la &vora: Botons Tanca Tanca amb doble clic:
 Per obrir el menú, mantingueu el botó premut fins que aparega. &Tanca les finestres amb doble clic al botó menú Ajuda contextual Arrossegueu els botons entre ací i la barra de títol Deixeu anar ací per eliminar el botó Obtén decoracions noves... Mantén al damunt Mantén per sota Maximitza Menú Minimitza A tots els escriptoris Busca Plega Tema Barra de títol 