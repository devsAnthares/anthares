��          �   %   �      p     q  +   �  .   �  6   �  *     #   D  &   h  /   �  2   �  :   �  0   -  3   ^  ;   �  K   �  -     $   H  '   m     �     �     �     �     �  #        1     O     c     |  �  �     X  +   `  0   �  2   �  (   �  "   	  '   <	  .   d	  3   �	  5   �	  .   �	  3   ,
  5   `
  l   �
  .     #   2  )   V     �     �     �     �     �  !   �  #   �     	          "                                                    	   
                                                                    @action:buttonCommit @info:statusAdded files to SVN repository. @info:statusAdding files to SVN repository... @info:statusAdding of files to SVN repository failed. @info:statusCommit of SVN changes failed. @info:statusCommitted SVN changes. @info:statusCommitting SVN changes... @info:statusRemoved files from SVN repository. @info:statusRemoving files from SVN repository... @info:statusRemoving of files from SVN repository failed. @info:statusReverted files from SVN repository. @info:statusReverting files from SVN repository... @info:statusReverting of files from SVN repository failed. @info:statusSVN status update failed. Disabling Option "Show SVN Updates". @info:statusUpdate of SVN repository failed. @info:statusUpdated SVN repository. @info:statusUpdating SVN repository... @item:inmenuSVN Add @item:inmenuSVN Commit... @item:inmenuSVN Delete @item:inmenuSVN Revert @item:inmenuSVN Update @item:inmenuShow Local SVN Changes @item:inmenuShow SVN Updates @labelDescription: @title:windowSVN Commit Show updates Project-Id-Version: fileviewsvnplugin
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2015-11-08 15:32+0100
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca@valencia
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
 Entrega S'han afegit els fitxers al repositori SVN. S'estan afegint els fitxers al repositori SVN... Ha fallat en afegir els fitxers al repositori SVN. Ha fallat l'entrega dels canvis a l'SVN. S'han entregat els canvis a l'SVN. S'estan entregant els canvis a l'SVN... S'han eliminat els fitxers del repositori SVN. S'estan eliminant els fitxers del repositori SVN... Ha fallat en eliminar els fitxers del repositori SVN. S'han revertit els fitxers del repositori SVN. S'estan revertint els fitxers del repositori SVN... Ha fallat en revertir els fitxers del repositori SVN. Ha fallat l'actualització de l'SVN. S'està deshabilitant l'opció «Mostra les actualitzacions de l'SVN». Ha fallat l'actualització del repositori SVN. S'ha actualitzat el repositori SVN. S'està actualitzant el repositori SVN... Afig SVN Entrega SVN... Suprimeix SVN Reverteix SVN Actualitza SVN Mostra els canvis locals de l'SVN Mostra les actualitzacions de l'SVN Descripció: Entrega SVN Mostra les actualitzacions 