��    &      L  5   |      P     Q  8   S     �     �     �     �     �     �  3   �  >        N     i     y     �  m   �     �          "     2     7     ?  *   O      z     �     �  "   �     �     �          3     :     H     X     e     �  ;   �     �  �  �     �     �     �     �     	     	     '	     >	     P	     X	  !   e	     �	     �	     �	     �	     �	     �	     �	     �	     �	     

  3    
  0   T
     �
     �
     �
     �
     �
  $   �
               &     E     T     \     c     h        #   %                                                              "          $                      
                                           &      !                	        % Accessibility data on volume sliderAdjust volume for %1 Applications Audio Muted Audio Volume Behavior Capture Devices Capture Streams Checkable switch for (un-)muting sound output.Mute Checkable switch to change the current default output.Default Decrease Microphone Volume Decrease Volume Devices General Heading for a list of ports of a device (for example built-in laptop speakers or a plug for headphones)Ports Increase Microphone Volume Increase Volume Maximum volume: Mute Mute %1 Mute Microphone No applications playing or recording audio No output or input devices found Playback Devices Playback Streams Port is unavailable (unavailable) Port is unplugged (unplugged) Raise maximum volume Show additional options for %1 Volume Volume at %1% Volume feedback Volume step: label of device items%1 (%2) label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: plasma_applet_org.kde.plasma.volume
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2017-09-20 23:32+0100
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca@valencia
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Generator: Lokalize 2.0
 % Ajusta el volum de %1 Aplicacions Àudio silenciat Volum de l'àudio Comportament Dispositius de captura Fluxos de captura Silenci Per omissió Disminueix el volum del micròfon Disminueix el volum Dispositius General Ports Augmenta el volum del micròfon Augmenta el volum Volum màxim: Silencia Silencia %1 Silencia el micròfon No hi ha cap aplicació reproduint o gravant àudio No s'ha trobat cap dispositiu d'eixida o entrada Dispositius de reproducció Fluxos de reproducció  (no disponible)  (desendollat) Eleva el volum màxim Mostra les opcions addicionals de %1 Volum Volum al %1% Reacció del control del volum Pas del volum: %1 (%2) %1: %2 100% %1% 