��    "      ,  /   <      �  &   �  �         �     �     �     �     �     �     �                -  	   N  �   X  c   !     �     �     �     �     �     �     �     �          
          !  	   3  C   =  j   �  E   �     2     H  �  V  &   �  2  #	  *   V
     �
     �
  "   �
  %   �
     �
          &     7     Q     d  �  �  �   W  &   E     l  "   x     �  8   �     �  %        *     6     E  6   T  0   �     �  �   �  �   t  �   Y  F   �  &   8                  "       	                                                                             
                                          !              (c) 2002 Karol Szwed, Daniel Molkentin <h1>Style</h1>This module allows you to modify the visual appearance of user interface elements, such as the widget style and effects. @title:tab&Applications @title:tab&Fine Tuning Button Checkbox Combobox Con&figure... Configure %1 Daniel Molkentin Description: %1 EMAIL OF TRANSLATORSYour emails Group Box Here you can choose from a list of predefined widget styles (e.g. the way buttons are drawn) which may or may not be combined with a theme (additional information like a marble texture or a gradient). If you enable this option, KDE Applications will show small icons alongside some important buttons. KDE Style Module Karol Szwed NAME OF TRANSLATORSYour names No Text No description available. Preview Radio button Ralf Nolden Tab 1 Tab 2 Text Below Icons Text Beside Icons Text Only There was an error loading the configuration dialog for this style. This area shows a preview of the currently selected style without having to apply it to the whole desktop. This page allows you to choose details about the widget style options Unable to Load Dialog Widget style: Project-Id-Version: kcmstyle
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-17 05:44+0100
PO-Revision-Date: 2009-12-14 10:10+0530
Last-Translator: Danishka Navin <danishka@gmail.com>
Language-Team: Sinhala <danishka@gmail.com>
Language: si
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 0.3
Plural-Forms: nplurals=2; plural=(n != 1);
 (c) 2002 Karol Szwed, Daniel Molkentin <h1>රටා</h1> මෙම මොඩියුලය පරිශීලක අතුරුමුහුණත් මූලයන් හී දෘශ්‍ය පෙනුම වෙනස් කිරීමට ඉඩදෙයි, විජෙට්ටු රටා හා සැරසිලි වැනි. භාවිත යෙදවුම් (&A) සුමටනය (&F) බොත්තම සලකුණු කොටුව සංයුක්ත කොටුව සකසන්න... (&f) %1 සකසන්න Daniel Molkentin විස්තරය: %1 danishka@gmail.com කාණ්ඩ කොටුව මෙහිදී ඔබට කලින් නිවේශනය කල විජෙට්ටු රටා ලැයිස්තුවකින් තෝරාගත හැක (උදා. බොත්තම් ඇඳෙන ආකාරය) මේවා තේමාව සමග සංයුක්ත හෝ අසංයුක්ත විය හැක (මාබල රටා හා අනුක්‍රමික වැනි අමතර තොරතුරු). මෙම වික්ල්පය සක්‍රීය විට, KDE භාවිතයෙදවුම් වල වැදගත් බොත්තම් පසෙකින් කුඩා අයිකන පෙන්වනු ඇත. KDE රටා මොඩියුලය Karol Szwed ඩනිෂ්ක නවින් පෙළ රහිත විස්තර කිරීමක් නොමැත පෙරදැක්ම රේඩියෝ බොත්තම Ralf Nolden 1 ටැබය 2 ටැබය අයිකන වලට පහළින් පෙළ අයිකන වලට පිටත පෙළ පෙළ පමණක් මෙම රටාව සඳහා සැකසුම් සංවාදය පූර්ණය කිරීමේ වරදක් හට ගැණිනි. මම ප්‍රදේශය දැනට තෝරාඇති රටාව මුළු වැඩතලයට ඇතුළු කිරීමෙන් තොරව පෙරදැක්මකින් පෙන්වයි. මෙම පිටුව විජෙට්ටු රටා පිළිබඳ තොරතුරු තෝරාගැනීමට ඉඩදෙයි සංවාදය පූරණය කල නොහැකි විය විජෙට්ටු රටාව: 