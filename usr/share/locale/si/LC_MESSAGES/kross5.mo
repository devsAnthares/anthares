��            )   �      �  &   �     �     �     �     �      �               .     6  ,   S  3   �     �     �     �     �     �  '        4     S     Y     o     �     �     �     �     �  &   �     �  �  �     �  I   �     
  #   '     K     Y     l  J   �     �  a   �  �   E  �   �  l   o	  v   �	     S
     d
     x
  I   �
  "   �
     �
  +     :   3     n  R   �  1   �          +  w   E  
   �                                       
                      	                                                                            @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2011-07-25 07:45+0530
Last-Translator: Danishka Navin <danishka@gmail.com>
Language-Team: Sinhala <danishka@gmail.com>
Language: si
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 
 
X-Generator: Lokalize 1.2
 සාමාන්‍ය නව ස්ක්‍රිප්ටයක් එක් කරන්න. එක් කරන්න... අහෝසි කරන්නද? සටහන: danishka@gmail.com සැකසුම් තෝරාගත් ස්ක්‍රිප්ටය සකසන්න සකසන්න... තේරූ ස්ක්‍රිප්ටය ක්‍රියාත්මක කරන්න. "%1" පරිවර්තකයා සඳහා ස්ක්‍රිප්ට් සෑදීම අසාර්ථක විය. "%1" ස්ක්‍රිප්ට් ගොනුව සඳහා පරිවර්තකයක් තීරණය කිරීම අසාර්ථක විය "%1" පරිවර්තකය ප්‍රවේශනය කිරීම අසාර්ථක විය "%1" ස්ක්‍රිප්ට් ගොනුව විවෘත කිරීම අසාර්ථක විය ගොනුව: අයිකනය: පරිවර්තක: Ruby පරිවර්තකයේ ආරාක්ෂා මට්ටම ඩනිෂ්ක නවින් නම: '%1' ආකාරයක් නොමැත. "%1" ලෙස පරිවර්තකයක් නැත ඉවත් කරන්න තෝරාගත් සක්‍රිප්ටය ඉවත් කරන්න. ක්‍රියාත්මක කරන්න "%1" Scriptfile නැත. නවත් වන්න තේරූ ස්ක්‍රිප්ටයේ ක්‍රියාත්මකවීම නතර කරන්න. පෙළ: 