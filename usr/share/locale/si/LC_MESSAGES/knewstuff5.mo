��    B      ,  Y   <      �  -   �     �     �  �   �  �   �  �   U  [   �  v   F  R   �     	     	     	  	   )	  
   3	     >	     S	     l	     y	  "   �	  3   �	     �	     �	     �	     
     '
     .
     2
     D
     W
     g
  	   o
     y
     �
     �
     �
     �
     �
     �
     �
  	   �
  	   �
                 ?   $     d  	   y     �     �     �     �     �     �  
   �     �     �  &   �     $     ?  	   F     P  	   W     a  ;   j  !   �  �  �  A   r  (   �     �  �  �  �  �  e  1  �   �  �   G  ;   �  
   &     1  %   5     [  
   u  <   �  N   �          #  F   6  y   }  H   �     @  @   M  4   �     �     �  9   �  :     /   L     |     �  M   �     �     �  G     R   ]  (   �     �     �     �     
  )   !  	   K  
   U  %   `  4   �     �     �     �  5   �     2  +   F  <   r     �  ?   �  J     Q   Z  0   �     �     �          /     M  s   ^  6   �     4      5       <                    A      -   )   "   8   B   '   ?      #              ,       	      .                       >       9   !       :          $                      %       +      =                    &       @              7      1   (                 2   ;   
   6      *   /   3          0                <a href="http://opendesktop.org">Homepage</a> <br />Provider: %1 <br />Version: %1 <qt>Cannot start <i>gpg</i> and check the validity of the file. Make sure that <i>gpg</i> is installed, otherwise verification of downloaded resources will not be possible.</qt> <qt>Cannot start <i>gpg</i> and retrieve the available keys. Make sure that <i>gpg</i> is installed, otherwise verification of downloaded resources will not be possible.</qt> <qt>Cannot start <i>gpg</i> and sign the file. Make sure that <i>gpg</i> is installed, otherwise signing of the resources will not be possible.</qt> <qt>Enter passphrase for key <b>0x%1</b>, belonging to<br /><i>%2&lt;%3&gt;</i><br />:</qt> A link to the knowledgebase (like a forum) (opens a web browser)Knowledgebase (no entries) Knowledgebase (%1 entries) A link to the website where the get hot new stuff upload can be seenVisit website Author: BSD Become a Fan Category: Changelog: Could not install %1 Create content on server Description: Details Download of "%1" failed, error: %2 Downloaded file was a HTML file. Opened in browser. Enter search phrase here Error Error initializing provider. File not found: %1 Finish GPL Get Hot New Stuff Get Hot New Stuff! Icons view mode Install Installed Key used for signing: LGPL License: Loading Preview Loading data from provider Most downloads Newest Opposite to BackNext Order by: Password: Preview Images Price Price: Program name followed by 'Add On Installer'%1 Add-On Installer Provider information Provider: Rating Re: %1 Reset Search: Select Preview... Select Signing Key Server: %1 Set a price for this item Share Hot New Stuff The selected category "%1" is invalid. There was a network error. Title: Uninstall Update Username: Version: Your account balance is too low:
Your balance: %1
Price: %2 fan as in supporter1 fan %1 fans Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-12 03:00+0100
PO-Revision-Date: 2011-07-25 07:45+0530
Last-Translator: Danishka Navin <danishka@gmail.com>
Language-Team: Sinhala <danishka@gmail.com>
Language: si
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 
 
X-Generator: Lokalize 1.2
 <a href="http://opendesktop.org">මුල් පිටුව</a> <br />ප්‍රතිපාදක: %1 <br />වෙළුම: %1 <qt><i>gpg</i> ආරම්භ කර ගොනුවේ වලංගුබව පරීක්ෂාකළ නොහැක. <i>gpg</i> ස්ථාපනය කර ඇති බවට සහතික කරන්න, එසේ නොවේ නම් බාගත කළ සම්පත් තහවුරු කිරීමට හැකියාවක් නොලැබෙනු ඇත.</qt> <qt><i>gpg</i> ආරම්භ කර ප්‍රයෝජනයට ගත හැකි යතුරු සොයා ලබා ගැනීමට නොහැක. <i>gpg</i> ස්ථාපනය කර ඇති බවට සහතික කරන්න, එසේ නොවේ නම් බාගත කළ සම්පත් තහවුරු කිරීමට හැකියාවක් නොලැබෙනු ඇත.</qt> <qt><i>gpg</i> ආරම්භ කර ගොනුව අත්සන් කිරීමට නොහැක. <i>gpg</i> ස්ථාපනය කර ඇති බවට සහතික කරන්න, එසේ නොවේ නම් සම්පත් අත්සන් කිරීමට හැකියාවක් නොලැබෙනු ඇත.</qt> <qt><br /><i>%2&lt;%3&gt;</i><br />ට අයත් <b>0x%1</b> යතුර සඳහා අවසර වාක්‍ය ඛණ්ඩය ඇතුල් කරන්න:</qt> දැණුම් ගබඩාව (ඇතුළත් කිරීම් නැත) දැණුම් ගබඩාව (ඇතුළත් කිරීම් %1) වෙබ් අඩවියට පිවිසෙන්න කතෘ: BSD රසිකයෙක් වන්න ප්‍රභේදය: Changelog: %1 ස්ථාපනය කළ නොහැකි විය සේවාදායකය මත අන්තර්ගතය තනන්න විස්තරය: විස්තර "%1" බාගැනීම අසමත් විය, දෝෂය: %2 බාගත් ගොනුව HTML ගොනුවකි. ගවේෂකය තුළ විවෘත කරන්න. සෙවුම් වැකිකඩ ඇතුළත් කරන්න දෝෂය සැපසුම ඇරඹීම දෝෂ සහිතයි. ගොනුව හමුවූයේ නැත: %1  අවසන් GPL උණුඋණුවේ නවතම දේ ගන්න උණුඋණුවේ නවතම දේ ගන්න! අයිකන පෙනෙන ආකාරය ස්ථාපනය ස්ථාපනය අත්සන් කිරීමට භාවිතා කළ යතුර: LGPL බලපත්‍රය: පූර්‍වදසුන පූර්‍ණය වෙමින් සැපයුම මඟින් දත්ත පූරණය කරමින් වැඩිම බාගැනීම් නවතම මීළඟ පිළිවෙල: රහස්පදය: පිංතූර පෙර දසුන මිල මිල: %1 ඇඩෝන ස්ථාපකය ප්‍රතිපාදක තොරතුරු ප්‍රතිපාදක: ඇගැයුම යළි: %1 මුල්ම තත්වයට සකසන්න සොයන්න: පෙරදසුන තෝරන්න... අත්සන් කරන යතුර තෝරන්න සේවාදායකය: %1 මෙම අයිතමයට මිලක් දෙන්න උණුසුම් සහ නවතම දේ ගෙදා ගන්න තෝරාගත් "%1" ප්‍රභේදය වලංගු නොවේ. ජාල දෝෂයක් තිබුණි. සිරස්තලය: අස්ථාපනය යාවත්කාලීන පරිශීලක නම: වෙළුම: ඔබේ ගිණුම් ශේෂය ඉතා අවමයි:
ඔබේ ශේෂ්‍ය: %1
මිළ: %2 රසිකයින් 1 රසිකයින් %1 