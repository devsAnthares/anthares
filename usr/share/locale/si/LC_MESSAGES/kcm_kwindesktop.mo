��             +         �     �     �  
   O  +   Z  
   �     �     �     �      �     �     �  	          
  e   +  *   �     �     �     �  )   �  ;     	   U     _  (   ~     �     �     �     �          )     @  	   [  �  e     �  �         �  S   	     g	     }	  ;   �	     �	  F   �	  %   0
     V
     f
     �
  �   �
  v   d     �  "   �  (     \   =  �   �     !  H   4  h   }  E   �  E   ,  B   r  H   �  4   �  ;   3  ;   o     �                                                                                               	                                  
                msec <h1>Multiple Desktops</h1>In this module, you can configure how many virtual desktops you want and how these should be labeled. Animation: Assigned global Shortcut "%1" to Desktop %2 Desktop %1 Desktop %1: Desktop Effect Animation Desktop Names Desktop Switch On-Screen Display Desktop Switching Desktops Duration: EMAIL OF TRANSLATORSYour emails Enabling this option will show a small preview of the desktop layout indicating the selected desktop. Here you can enter the name for desktop %1 Layout NAME OF TRANSLATORSYour names No Animation No suitable Shortcut for Desktop %1 found Shortcut conflict: Could not set Shortcut %1 for Desktop %2 Shortcuts Show desktop layout indicators Show shortcuts for all possible desktops Switch One Desktop Down Switch One Desktop Up Switch One Desktop to the Left Switch One Desktop to the Right Switch to Desktop %1 Switch to Next Desktop Switch to Previous Desktop Switching Project-Id-Version: kcm_kwindesktop
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-06 05:35+0100
PO-Revision-Date: 2009-10-31 14:32+0530
Last-Translator: gayankalhara <gkgkalhara@gmail.com>
Language-Team: Sinhala <kde-i18n-doc@kde.org>
Language: si
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
  msec <h1>බහු වැඩතල</h1>මෙම මොඩියුලයේ, ඔබට ඇවැසි අතත්‍ය වැඩතල ගණන හා ඒවායේ ලේබල විය යිතු ආකාරය සැකසිය හැක. සජීවීකරණ: වැඩතලය %2 සඳහා "%1" කෙටිමඟක් සකසන්න වැඩතලය %1 වැඩතලය %1 වැඩතල අලංකරණ සජීවීකරණ වැඩතල නාම වැඩතල මාරුව තිරය මත දර්ශනය වැඩතල මාරුවීම වැඩතල කාල සීමාව: danishka@gmail.com මෙම විකල්පය තෝරාගැනීමෙන් ඔබට තෝරාගත් වැඩතලයේ ආකෘතියේ කුඩා පෙරදැක්මක් දක්වයි මෙහිද් ඔබට %1 වැඩතලය සඳහා නාමයක් ඇතුළත් කල හැක සැකැස්ම ඩනිෂ්ක නවින් සජීවීකරණ නොමැත %1 වැඩතලය සඳහා සුදුසු කෙට මඟක් නොමැත කෙට්මං ගැටුමක්: %2  වැඩතලය සඳහා %1 කෙටිමඟ සැකසිය නොහැක කෙටිමං වැඩතල ආකෘති දර්ශක පෙන්වන්න හැකි සියළුම වැඩතල සඳහා කෙටිමං පෙන්වන්න පහළට වැඩතල එකක් මාරුවෙන්න ඉහළට වැඩතල එකක් මාරුවෙන්න වමට වැඩතල එකක් මාරුවෙන්න දකුණට වැඩතල එකක් මාරුවෙන්න %1 වැඩතලයට මාරුවෙන්න ඊලහ වැඩතලයට මාරුවෙන්න පෙර වැඩතලයට මාරුවෙන්න මාරුවීම 