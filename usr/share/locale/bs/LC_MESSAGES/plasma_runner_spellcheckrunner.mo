��    
      l      �       �      �                2     :     W  Q   l     �     �    �     �          "     =     E     ]     y          �     	                      
                  &Require trigger word &Trigger word: Checks the spelling of :q:. Correct Could not find a dictionary. Spell Check Settings Spelling checking runner syntax, first word is trigger word, e.g.  "spell".%1:q: Suggested words: %1 spell Project-Id-Version: kdeplasma-addons
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2012-09-03 13:54+0000
Last-Translator: Samir Ribić <Unknown>
Language-Team: Bosnian <bs@li.org>
Language: bs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2014-10-21 06:51+0000
X-Generator: Launchpad (build 17203)
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
 &Zahtijevaj okidačku riječ &Okidačka riječ: Provjerava pravopis u :q:. Ispravi Ne mogu naći rječnik. Postavke provjere pravopisa %1:q: Predložene riječi: %1 pravopis 