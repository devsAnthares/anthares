��    6      �  I   |      �  
   �  
   �  
   �     �     �     �     �     �     �  	          
             $     *     =     B     I     P     `     g  
   y     �     �     �     �     �     �     �     �     �     �     �          
          '     -     :  	   F     P     V     j  b   q  	   �     �  
   �  	   �                    !     3    D  
   ]	     h	     u	     �	     �	     �	     �	     �	     �	     �	     �	  
   �	     
     	
     
     &
     +
     3
     :
     Y
     `
     s
     
  	   �
     �
     �
     �
     �
     �
     �
     �
     �
               "     2     ?     E     Q  	   ^     h     q  	   �  c   �     �       
             (     6     I     Q     f     "                 *   (   /         %         #         .            6                    3          0                      	             +                    '         
           !   5                -       1           2   &   ,   4          $          )    Activities Add Action Add Spacer Add Widgets... Alt Always Visible Apply Apply Settings Author: Auto Hide Cancel Categories Center Close Create activity... Ctrl Delete Email: Get new widgets Height Horizontal-Scroll Input Here Keyboard shortcuts Layout: Left Left-Button License: Lock Widgets Maximize Panel Meta Middle-Button More Settings... Mouse Actions OK Panel Alignment Remove Panel Right Right-Button Screen Edge Search... Shift Stopped activities: Switch The settings of the current module have changed. Do you want to apply the changes or discard them? Uninstall Vertical-Scroll Visibility Wallpaper Wallpaper Type: Widgets Width Windows Can Cover Windows Go Below Project-Id-Version: plasma
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-09 03:10+0200
PO-Revision-Date: 2014-12-28 11:53+0000
Last-Translator: Ajla Alic <aalic2@etf.unsa.ba>
Language-Team: bosanski <bs@li.org>
Language: bs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2014-12-29 06:15+0000
X-Generator: Launchpad (build 17286)
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
 Aktivnosti Dodaj akciju Dodaj razmaknicu Dodaj grafičke kontrole... Alt Uvijek vidljivo Primjeni Primijeni postavke Autor: Automatsko sakrivanje Otkaži Kategorije Centar ZatvorI Kreiraj aktivnost... Ctrl Obriši Email: Dobavi nove grafičke elemente Visina Vodoravno klizanje Unesi ovdje Prečice na tastaturi Raspored: Lijevo Lijevo dugme Dozvola: Zaključaj grafičku kontrolu Uvećaj panel Meta Srednje dugme Više podešavanja... Radnje mišem OK Ravnanje panela Ukloni panel Desno Desno dugme Ivica ekrana Traži... Pomjeraj Stopirane aktivnosti: Prekidač Postavke trenutnog modula su promijenjene. Da li želite da primjenite promjene ili da ih odbacite? Deinstaliraj Uspravno klizanje Vidljivost Pozadina Tip pozadine: Grafičke kontrole Širina Prozor može pokriti Prozor ide ispod 