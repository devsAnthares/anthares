��    %      D  5   l      @     A     P     a     �     �     �     �     �     �  ,   �     #     B     Z     o     �     �     �  	   �     �     �     �     �  	   �  
   �     	  
        $     8     I     _     l  	   s     }     �     �  #   �    �     �     �  #        (  
   1  
   <  	   G     Q     k  5   �     �     �     �     	     	     )	     0	     9	     E	  
   W	     b	  
   i	     t	  
   }	     �	     �	     �	     �	     �	     �	     �	  	   
     
  	   '
     1
     L
               !       %                  
                 "                                      	                                                      #               $               Add to Desktop Add to Favorites Align search results to bottom App name (Generic name)%1 (%2) Behavior Description (Name) Description only Edit Application... Edit Applications... Expand search to bookmarks, files and emails Flatten menu to a single level Forget All Applications Forget All Documents Forget Application Forget Document General Generic name (App name)%1 (%2) Hibernate Hide Application Lock Logout Name (Description) Name only Open with: Power / Session Properties Recent Applications Recent Documents Remove from Favorites Save Session Search Search... Show applications as: Suspend Unhide Applications in '%1' Unhide Applications in this Submenu Project-Id-Version: kde 49i410
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2015-02-04 15:12+0000
Last-Translator: Samir Ribić <Unknown>
Language-Team: Bosnian
Language: bs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2015-02-05 07:28+0000
X-Generator: Launchpad (build 17331)
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
 Dodaj na površ Dodaj u omiljene Svrstajte rezultate pretrage na dno %1 ( %2) Ponašanje Opis (ime) Samo opis Podesavanje aplikacije... Izmijeni programe... Proširi pretragu na zabilješke, datoteke i e-poštu Izravnajte menu na jedan nivo Zaboravi sve aplikacije Zaboravi sve dokumente Zaboravi aplikaciju Zaboravi dokument Opšte %1 ( %2) Hibernacija Sakrij aplikaciju Zaključaj Odjava Ime (Opis) Samo ime Otvori sa: Napajanje / Sesija Podešavanja Skorašnji programi Skorašnji dokumenti Ukloni iz omiljenih Sačuvaj sesiju Pretraživanje Traži... Prikaži aplikaciju kao: Suspenduj Prikaži aplikacije u '%1' Prikaži aplikacije u podmenu-u 