��          \      �       �      �   -   �        -   +  #   Y  #   }     �    �     �  2   �       4   5     j     o     t                                       Displays the current date Displays the current date in a given timezone Displays the current time Displays the current time in a given timezone Note this is a KRunner keyworddate Note this is a KRunner keywordtime Today's date is %1 Project-Id-Version: kdeplasma-addons
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2012-09-03 15:48+0000
Last-Translator: Samir Ribić <Unknown>
Language-Team: Bosnian <bs@li.org>
Language: bs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2012-12-21 01:41+0000
X-Generator: Launchpad (build 16378)
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
 Prikazuje trenutni datum Prikazuje trenutni datum u zadatoj vremenskoj zoni Prikazuje trenutno vrijeme Prikazuje trenutno vrijeme u zadatoj vremenskoj zoni date time Današnji datum je %1 