��    -      �  =   �      �  6   �  M        f     o     v     �  	   �  A   �  >   �  9   )  9   c  9   �  W   �  E   /     u     �     �  7   �      �       X        p     x     �  �   �     ?     E     \  
   l  
   w     �     �  E  �     
     
     5
  w   H
     �
     �
     �
     �
     �
  	   
       �    J   �  `   �     T     ]     d     v  
   �     �     �  	   �     �     �  0   �     �     
  	     #   "  M   F  /   �     �  Q   �     3     <     N  �   f     �            
   2  
   =     H     L  D  P      �     �     �  �   �     y  	   �     �     �     �     �     �                          '         &                     $   (                              	       #         *       !   ,            %                +                    
            )   "                     -    "Full screen repaints" can cause performance problems. "Only when cheap" only prevents tearing for full screen changes like a video. Accurate Always Animation speed: Author: %1
License: %2 Automatic Category of Desktop Effects, used as section headerAccessibility Category of Desktop Effects, used as section headerAppearance Category of Desktop Effects, used as section headerCandy Category of Desktop Effects, used as section headerFocus Category of Desktop Effects, used as section headerTools Category of Desktop Effects, used as section headerVirtual Desktop Switching Animation Category of Desktop Effects, used as section headerWindow Management Configure filter Crisp Enable compositor on startup Exclude Desktop Effects not supported by the Compositor Exclude internal Desktop Effects Full screen repaints Hint: To find out or configure how to activate an effect, look at the effect's settings. Instant KWin development team Keep window thumbnails: Keeping the window thumbnail always interferes with the minimized state of windows. This can result in windows not suspending their work when minimized. Never Only for Shown Windows Only when cheap OpenGL 2.0 OpenGL 3.1 OpenGL Platform InterfaceEGL OpenGL Platform InterfaceGLX OpenGL compositing (the default) has crashed KWin in the past.
This was most likely due to a driver bug.
If you think that you have meanwhile upgraded to a stable driver,
you can reset this protection but be aware that this might result in an immediate crash!
Alternatively, you might want to use the XRender backend instead. Re-enable OpenGL detection Re-use screen content Rendering backend: Scale method "Accurate" is not supported by all hardware and can cause performance regressions and rendering artifacts. Scale method: Search Smooth Smooth (slower) Tearing prevention ("vsync"): Very slow XRender Project-Id-Version: kcmkwincompositing
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2015-02-04 15:48+0000
Last-Translator: Samir Ribić <Unknown>
Language-Team: Bosnian <bs@li.org>
Language: bs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2015-02-05 06:28+0000
X-Generator: Launchpad (build 17331)
X-Associated-UI-Catalogs: kcmkwm desktop_kdebase
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
 "Prefarbavanje cijelog zaslona" može uzrokovati probleme u performansama. "Samo kada je jeftino" samo sprječava cijepanje za promjene cijelog zaslona, kao što je video. Precizno Uvijek Brzina animacije: Autor: %1
Licenca: %2 Automatski Pristupačnost Prikaz Slatkiši Fokus Alati Animacija zamjene virtualne pozadinske površine Upravljanje prozorima Podesi filter Diskretno Omogući kompozitora pri pokretanju Isključivi efekti pozadinske površine nisu pozdržani od strane Kompozitora Isključivi interni efekti pozadinske površine Prefarbavanje cijelog ekrana Savjet: Pogledajte postavke efekta da saznate kako da ga podesite ili aktivirate. Trenutno KWin razvojni tim Drži sličice prozora: Držanje ikonice prozora je uvijek posredovano minimiziranim stanjem prozora. Ovo može rezultovati da prozor ne obustavlja posao dok je minimiziran. Nikada Samo za prikazane prozore Samo kada je jeftino OpenGL 2.0 OpenGL 3.1 EGL GLX OtvoriGL kompoziranje (zadano) je krahiralo KWin u prošlosti.
Ovo je najvjerovatnije zbog greške drajvera.
Ako misliš da si u međuvremenu nadogradio na stabilniji drajver,
možes resetovati ovu zaštitu, budi svjestan da ovo može rezultovati skorim krahiranjem!
Alternativno, možeš koristiti  XRender pozadinski kraj. Ponovo omogući detekciju OpenGL Ponovo koristi sadržaj ekrana Pozadinski softver za obradu: Metod skaliranja "Približno" nije podržan od strane svih hardvera i može uzrokvati regresiju perfomransi i prevođenje artefakata. Način skaliranja: Pretraži Glatko Glatko (sporije) Prevencija cijepanja ("vsync"): Veoma sporo XRender 