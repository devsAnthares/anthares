��    *      l  ;   �      �  A   �     �  /        2     ;     O     a     w     �     �  	   �     �  3   �  	   �     �      �  $      *   E     p  	   u  5        �     �  
   �     �          $  5   3  6   i     �  ,   �  /   �     	        O   0  Z   �  b   �  	   >  
   H  P   S     �  V  �  2        >  J   U     �     �     �     �     �     �     �            0     	   O     Y  $   a     �     �     �     �  ;   �  %   �     !     :     F     T     g  ;   }  1   �     �  5   �     3     <     P  \   c  c   �  X   $     }     �  R   �     �            )             &      $   	   %         '                                                !      
       *                     "                                                 (                 #    %1 is an external application and has been automatically launched (c) 2009, Ben Cooksley <i>Contains 1 item</i> <i>Contains %1 items</i> About %1 About Active Module About Active View About System Settings Apply Settings Author Ben Cooksley Configure Configure your system Determines whether detailed tooltips should be used Developer Dialog EMAIL OF TRANSLATORSYour emails Expand the first level automatically General config for System SettingsGeneral Help Icon View Internal module representation, internal module model Internal name for the view used Keyboard Shortcut: %1 Maintainer Mathias Soeken NAME OF TRANSLATORSYour names No views found Provides a categorized icons view of control modules. Provides a classic tree-based view of control modules. Relaunch %1 Reset all current changes to previous values Search through a list of control modulesSearch Show detailed tooltips System Settings System Settings was unable to find any views, and hence has nothing to display. System Settings was unable to find any views, and hence nothing is available to configure. The settings of the current module have changed.
Do you want to apply the changes or discard them? Tree View View Style Welcome to "System Settings", a central place to configure your computer system. Will Stephenson Project-Id-Version: systemsettings
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:08+0100
PO-Revision-Date: 2013-10-18 20:13+0000
Last-Translator: Samir Ribić <Unknown>
Language-Team: Bosnian <bs@li.org>
Language: bs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Launchpad (build 17341)
X-Launchpad-Export-Date: 2015-02-15 06:05+0000
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
 %1 je spoljašnji program i automatski je pokrenut (c) 2009, Ben Cooksley <i>Sadrži %1 stavku</i> <i>Sadrži %1 stavke</i> <i>Sadrži %1 stavki</i> O %1 O aktivnom modulu O aktivnom prikazu O Sistemskim postavkama Primjena postavki Autor Ben Cooksley Podesi Podesite sistem Određuje da li se prikazuju detaljni oblačići Programer Dijalog Mirza@etf.ba,samir.ribic@etf.unsa.ba Automatski proširi prvi nivo Opšte Pomoć Prikaz ikona Unutrašnje predstavljanje modula, unutrašnji model modula Unutrašnje ime za korišćeni prikaz Prečica s tastature: %1 Održavalac Matijas Seken Mirza,Samir Ribić Nema nijednog prikaza Pruža prikaz kontrolnih modula po kategorizovanim ikonama. Pruža klasični prikaz stabla kontrolnih modula. Pokreni ponovo %1 Vratite sve tekuće izmjene na prethodne vrijednosti. Pretraga Detaljni oblačići Sistemske postavke Ne može se naći nijedan prikaz za Sistemske postavke, stoga nema ničijeg za prikazivanje. Ne može se naći nijedan prikaz za Sistemske postavke, stoga ništa nije dostupno za podešavanje. Postavke tekućeg modula su izmijenjene.
Želite li da primijenite ili odbacite izmjene? Prikaz stabla Stil prikaza Dobro došli u „Sistemske postavke“, centralno mjesto za podešavanje sistema. Will Stephenson 