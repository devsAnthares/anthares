��    2      �  C   <      H     I     a     w     �     �  A   �  9   �  .   5  F   d  :   �  D   �  $   +  W   P  7   �  3   �  (     4   =  .   r  C   �  3   �  k        �     �     �     �  #   �     	     5	     N	  !   n	     �	  "   �	     �	     �	     �	     
     -
     C
     _
     t
  (   �
  +   �
  5   �
  3     7   P  1   �  1   �     �     �      
        "     )     8     ?     E  !   L  "   n  3   �  &   �  :   �     '  >   ?  '   ~      �     �  #   �       /   %  $   U     z     �     �     �     �  	   �     �     �     �     �                    #     0     =  
   C     N     [     b  
   w     �  )   �  &   �  -   �  *     &   2     Y     t     )             0         -                #              2      ,                    %      .   /   
                        1   	          &   *       '                                                "   +             $   (      !       @action:buttonCheckout @action:buttonCommit @action:buttonCreate Tag @action:buttonPull @action:buttonPush @action:button Add Signed-Off line to the message widgetSign off @info:tooltipA branch with the name '%1' already exists. @info:tooltipA tag named '%1' already exists. @info:tooltipAdd Signed-off-by line at the end of the commit message. @info:tooltipBranch names may not contain any whitespace. @info:tooltipCreate a new branch based on a selected branch or tag. @info:tooltipDiscard local changes. @info:tooltipProceed even if the remote branch is not an ancestor of the local branch. @info:tooltipTag names may not contain any whitespace. @info:tooltipThere are no tags in this repository. @info:tooltipThere is nothing to amend. @info:tooltipYou must enter a commit message first. @info:tooltipYou must enter a tag name first. @info:tooltipYou must enter a valid name for the new branch first. @info:tooltipYou must select a valid branch first. @item:intext Prepended to the current branch name to get the default name for a newly created branchbranch @label:listboxBranch: @label:listboxLocal Branch: @label:listboxRemote Branch: @label:listboxRemote branch: @label:listbox a git remoteRemote: @label:textboxTag Message: @label:textboxTag Name: @option:checkAmend last commit @option:checkCreate New Branch:  @option:checkForce @option:radio Git CheckoutBranch: @option:radio Git CheckoutTag: @title:groupAttach to @title:groupBranch Base @title:groupBranches @title:groupCheckout @title:groupCommit message @title:groupOptions @title:groupTag Information @title:group The remote hostDestination @title:group The source to pull fromSource @title:window<application>Git</application> Checkout @title:window<application>Git</application> Commit @title:window<application>Git</application> Create Tag @title:window<application>Git</application> Pull @title:window<application>Git</application> Push Dialog height Dialog width Project-Id-Version: kdesdk
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2015-02-04 14:18+0000
Last-Translator: Samir Ribić <Unknown>
Language-Team: Bosnian <bs@li.org>
Language: bs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2015-02-05 06:27+0000
X-Generator: Launchpad (build 17331)
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
 Izdvajanje Predaj Kreiraj oznaku Povuci Gurni Odjavi Grana s imenom '%1' već postoji. Oznaka s imenom '%1' već postoji. Dodaj Signed-off-by liniju na kraju poruke potvrde. Imena grana ne mogu sadržati razmake. Kreiraj novu granu bazirano na izabranoj grani ili oznaci. Odbaci lokalne izmjene. Nastavite čak i ako udaljena grana nije predak lokalne grane. Imena oznaka ne mogu sadržati razmake. Nema oznaka u ovom repozitoriju. Nema ništa z apopraviti. Morate unijeti poruku potvrde prvo. Morate prvo unijeti ime oznake. Morate prvo unijeti važeće ime za novu granu. Morate prvo odabrati važeću granu. grana Grana: Lokalna grana: Udaljena grana: Udaljena grana: Udaljeno: Poruka oznake: Ime oznake: Izmijeni zadnje upisivanje Kreiraj novu granu:  Forsiraj Grana: Oznaka: Pridruži na Osnova grane Grane Izdvajanje Poruka upisa Opcije Informacija o oznaci Odredište Izvor <application>Git</application> izdvajanje <application>Git</application> Potvrda <application>Git</application> Kreiraj oznaku <application>Git</application> povlačenje <application>Git</application> guranje Visina dijaloškog prozora Širina dijaloškog prozora 