��    B      ,  Y   <      �  i   �  m        y  b   �     �     	  6        O  7   _     �     �  	   �     �  (   �  )   �  )   (     R     o  Y   u     �     �  �   �      y	  .   �	     �	  
   �	     �	     �	     
     
     !
     5
     B
     J
     c
     r
     w
     �
     �
     �
     �
     �
  	   �
  
   �
     �
     �
  	     
     
   *     5     B  	   `     j     o  
   �  �   �     (  8   8  �   q     �  8   	  ,   B  ,   o  %   �     �  S  �  S   1  n   �     �  Z        o     �  8   �     �  6   �          ,  
   :     E  6   [  4   �  4   �  &   �  	   #  a   -     �     �  S   �       8   $     ]     o     ~     �     �     �     �     �  
   �     �               )     6     T  	   d     n     u     �     �     �     �     �     �     �     �          /     8     >     X  �   d     �  @   �  w   <     �  7   �  ;   �  E   7  +   }     �     $   !   %          ;   ,          B                 8   #                 7   9       +              )   ?   -   <         6                                    *   2       >                1       @       .   5   3   :   A         &      /                4              0      "             =   (   '         	   
           @info User changed Phonon backendTo apply the backend change you will have to log out and back in again. A list of Phonon Backends found on your system.  The order here determines the order Phonon will use them in. Apply Device List To... Apply the currently shown device preference list to the following other audio playback categories: Audio Hardware Setup Audio Playback Audio Playback Device Preference for the '%1' Category Audio Recording Audio Recording Device Preference for the '%1' Category Backend Colin Guthrie Connector Copyright 2006 Matthias Kretz Default Audio Playback Device Preference Default Audio Recording Device Preference Default Video Recording Device Preference Default/Unspecified Category Defer Defines the default ordering of devices which can be overridden by individual categories. Device Configuration Device Preference Devices found on your system, suitable for the selected category.  Choose the device that you wish to be used by the applications. EMAIL OF TRANSLATORSYour emails Failed to set the selected audio output device Front Center Front Left Front Left of Center Front Right Front Right of Center Hardware Independent Devices Input Levels Invalid KDE Audio Hardware Setup Matthias Kretz Mono NAME OF TRANSLATORSYour names Phonon Configuration Module Playback (%1) Prefer Profile Rear Center Rear Left Rear Right Recording (%1) Show advanced devices Side Left Side Right Sound Card Sound Device Speaker Placement and Testing Subwoofer Test Test the selected device Testing %1 The order determines the preference of the devices. If for some reason the first device cannot be used Phonon will try to use the second, and so on. Unknown Channel Use the currently shown device list for more categories. Various categories of media use cases.  For each category, you may choose what device you prefer to be used by the Phonon applications. Video Recording Video Recording Device Preference for the '%1' Category  Your backend may not support audio recording Your backend may not support video recording no preference for the selected device prefer the selected device Project-Id-Version: kcm_phonon
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2013-10-18 20:24+0000
Last-Translator: Samir Ribić <Unknown>
Language-Team: bosanski <bs@li.org>
Language: bs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Launchpad (build 17341)
X-Launchpad-Export-Date: 2015-02-15 05:58+0000
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
 Da primijenite promjenu pozadinskog programa morate se odjaviti i ponovo prijaviti. Spisak pozadina znanih Phononu na sistemu. Phonon će pokušati da ih upotrijebi ovdje navedenim redoslijedom. Primijeni spisak uređaja na... Primijeni trenutno prikazanu listu postavki uređaja za druge kategorija izvođenja zvuka. Postavka zvučnog hardvera Izvođenje zvuka Postavke uređaja za izvođenje zvuka za '%1' kategoriju Snimanje zvuka Postavke uređaja za snimanje zvuka za '%1' kategoriju Pozadinski sistem Colin Guthrie Povezivač © 2006, Matijas Krec Postavke podrazumijevanog uređaja za izvođenje zvuka Postavke podrazumijevanog uređaja za snimanje zvuka Postavke podrazumijevanog uređaja za snimanje videa Podrazumijevana/neodređena kategorija Neželjen Određuje podrazumijevani redoslijed uređaja, koji se može potisnuti po pojedinim kategorijama. Podešavanje uređaja Poželjni uređaji Uređaji nađeni na sistemu. Izaberite uređaj kojeg želite da aplikacije koriste. Samir.ribic@etf.unsa.ba Neuspjelo postavljanje izabranog audio izlaznog uređaja Prednji centralni Prednji lijevi Prednji lijevo od centra Prednji desni Prednji desno od centra Hardver Nezavisni uređaji Ulazni nivoi Nevažeće KDE postavka zvučnog hardvera Matijas Krec Jednokanalno Samir Ribić Modul za podešavanje Phonona Izvođenje (%1) Poželjan Profil Pozadinski centralni Pozadinski lijevi Pozadinski desni Snimanje (%1) Prikaži napredne uređaje Lijeva strana Desna strana Zvučna karta Zvučni uređaj Postavka zvučnika i testiranje Subvufer Proba Testiraj izabrani uređaj Testiram %1 Redoslijed određuje poželjnost izlaznih uređaja. Ako iz nekog razlog Phonon ne može da upotrijebi prvi, pokušaće sa drugim, itd. Nepoznati kanal Primijeni trenutno prikazan spisak uređaja za više kategorija. Razne kategorije upotrebe medija. Za svaku možete odabrati uređaj za koji želite da da ga koriste Phonoh aplikacije. Snimanje video Postavke uređaja za snimanje videa za '%1' kategoriju  Vaš pozadinski program možda ne podržava snimanje zvuka. Vaš pozadinski program možda ne podržava snimanje video sadržaja. izabrani uređaj nema određenu poželjnost izabrani uređaj je poželjan 