��          �      \      �     �  '   �  "        3     M     e  ?   �     �  &   �      �  %        E  >   _     �     �  '   �     �       3   9    m     �     �     �  
   �  	   �     �     �     �     �     �                    3     C     H     W     `     n                                                                         	   
                       Degree, unit symbol° Forecast period timeframe1 Day %1 Days High & Low temperatureH: %1 L: %2 High temperatureHigh: %1 Low temperatureLow: %1 Short for no data available- Shown when you have not set a weather providerPlease Configure Wind conditionCalm content of water in airHumidity: %1%2 distance, unitVisibility: %1 %2 ground temperature, unitDewpoint: %1 humidex, unitHumidex: %1 pressure tendency, rising/falling/steadyPressure Tendency: %1 pressure, unitPressure: %1 %2 temperature, unit%1%2 visibility from distanceVisibility: %1 wind direction, speed%1 %2 %3 windchill, unitWindchill: %1 winds exceeding wind speed brieflyWind Gust: %1 %2 Project-Id-Version: kdeplasma-addons
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-28 05:50+0100
PO-Revision-Date: 2012-09-03 13:02+0000
Last-Translator: Samir Ribić <Unknown>
Language-Team: Bosnian <bs@li.org>
Language: bs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Launchpad-Export-Date: 2014-10-21 06:51+0000
X-Generator: Launchpad (build 17203)
 ° %1 dan %1 dana %1 dana N: %1 V: %2 Visoka: %1 Niska: %1 - Molimo konfigurirajte Mirno Vlažnost: %1%2 Vidljivost: %1 %2 Dewpoint: %1 Humidex: %1 Tendencija pritiska: %1 Pritisak: %1 %2 %1%2 Vidljivost: %1 %1 %2 %3 Windchill: %1 Nalet vjetra: %1 %2 