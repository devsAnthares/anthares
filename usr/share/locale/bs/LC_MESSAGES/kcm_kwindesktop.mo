��    *      l  ;   �      �     �     �     �  
   D  +   O  
   {     �     �     �      �     �     �       	           �   @  e   �  *   D  H   o     �     �     �     �       )     ;   <  	   x     �  (   �     �     �     �          7     L     c  	   ~     �  #   �     �     �  W  �     T     X  |   p  
   �  1   �  	   *  
   4     ?     ^  (   l     �     �     �  	   �  +   �  �     e   �     �  2        E     W     `     n     �  *   �  <   �            (   )     R     g     {     �     �     �     �     �  #   �  .   	     8  '   U        
          *   &      #      '         $                               (            %                       	       !                            )              "                                          msec &Number of desktops: <h1>Multiple Desktops</h1>In this module, you can configure how many virtual desktops you want and how these should be labeled. Animation: Assigned global Shortcut "%1" to Desktop %2 Desktop %1 Desktop %1: Desktop Effect Animation Desktop Names Desktop Switch On-Screen Display Desktop Switching Desktop navigation wraps around Desktops Duration: EMAIL OF TRANSLATORSYour emails Enable this option if you want keyboard or active desktop border navigation beyond the edge of a desktop to take you to the opposite edge of the new desktop. Enabling this option will show a small preview of the desktop layout indicating the selected desktop. Here you can enter the name for desktop %1 Here you can set how many virtual desktops you want on your KDE desktop. KWin development team Layout N&umber of rows: NAME OF TRANSLATORSYour names No Animation No suitable Shortcut for Desktop %1 found Shortcut conflict: Could not set Shortcut %1 for Desktop %2 Shortcuts Show desktop layout indicators Show shortcuts for all possible desktops Switch One Desktop Down Switch One Desktop Up Switch One Desktop to the Left Switch One Desktop to the Right Switch to Desktop %1 Switch to Next Desktop Switch to Previous Desktop Switching Walk Through Desktop List Walk Through Desktop List (Reverse) Walk Through Desktops Walk Through Desktops (Reverse) Project-Id-Version: kcm_kwindesktop
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-06 05:35+0100
PO-Revision-Date: 2015-01-06 23:57+0000
Last-Translator: Samir Ribić <Unknown>
Language-Team: Bosnian <bs@li.org>
Language: bs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Launchpad (build 17341)
X-Launchpad-Export-Date: 2015-02-15 05:58+0000
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
  ms &Broj radnih površina: <h1>Višestruke površi</h1><p>U ovom modulu podešavate koliko će biti virtuelnih površi, i kako će biti označene.</p>. Animacija: Globalna prečica „%1“ dodijeljena površi %2 Površ %1 Površ %1: Animirano prebacivanje površi Imena površi Ekranski prikaz pri prebacivanju površi Prebacivanje površi Kretanje kroz površi omotava Površi Trajanje: Caslav.ilic@gmx.net,samir.ribic@etf.unsa.ba Uključite ovu opciju ako želite da kretanje kroz površi tastaturom ili preko aktivnih ivica površi vodi na suprotnu stranu nove površi. Ako uključite ovu opciju, dobićete mali pregled rasporeda površi koji ukazuje na izabranu površi. Unesite ime za površ %1 Zadajte koliko virtuelnih površi želite da bude. KWin razvojni tim Raspored B&roj redova: Chusslove Illich,Samir Ribić Bez animacije Nije nađena pogodna prečica za površ %1 Sukob prečica: ne mogu da postavim prečicu %1 za površ %2 Prečice Pokazatelji rasporeda površi Prikaži prečice za sve moguće površi Jednu površ nadolje Jednu površ nagore Jednu površ ulijevo Jednu površ udesno Na površ %1 Na narednu površ Na prethodnu površi Prebacivanje Kreći se kroz listu radnih površi Kreći se kroz listu radnih površi (inverzno) Kreći se kroz radne površi Kreći se kroz radne površi (inverzno) 