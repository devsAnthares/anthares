��          �      l      �     �  �      m   �  �   �  `   �     �     �     
          #      :     [  '   l     �     �     �     �  ?   �  Y     +   p    �     �  x   �  d   G     �  N   �     	     	     :	     G	     L	     i	     �	  #   �	     �	     �	     �	     �	  9   �	  T   
  &   s
                                    	                        
                                      (c) 2003-2007 Fredrik Höglund <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Cursor Theme Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Fredrik Höglund Get new color schemes from the Internet NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. Project-Id-Version: kcminput
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2015-01-07 00:17+0000
Last-Translator: Samir Ribić <Unknown>
Language-Team: bosanski <bs@li.org>
Language: bs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Launchpad (build 17341)
X-Launchpad-Export-Date: 2015-02-15 05:58+0000
 (c) 2003-2007 Fredrik Höglund <qt>Želite li zaista da uklonite temu pokazivača <i>%1</i>? Ovo će obrisati sve datoteke instalirane ovom temom.</qt> <qt>Ne možete obrisati temu koju trenutno koristite. Prvo morate prebaciti na neku drugu temu.</qt> (Dostupne veličine: %1) Tema pod imenom %1 već postoji u direktoriju tema ikona.  Zamijeniti je ovom? Potvrda Postavke kursora promijenjene Tema kursora Opis Privuci ili otkucaj URL teme Samir.ribic@etf.unsa.ba Fredrik Höglund Preuzmi nove šeme boja s Interneta Samir Ribić Ime Prebrisati temu? Ukloni temu Izgleda da datoteka %1 nije ispravna arhiva teme kursora. Ne mogu da preuzmem arhivu teme pokazivača. Provjerite da li je adresa %1 ispravna. Ne mogu nađi arhivu tema kursora %1!. 