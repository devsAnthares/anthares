��    
      l      �       �   $   �   %     :   <     w  '   �  -   �     �       '       <  #   W  #   {  .   �  &   �  &   �  .        K     e      w         	                            
        Could not detect the file's mimetype Could not find all required functions Could not find the provider with the specified destination Error trying to execute script Invalid path for the requested provider It was not possible to read the selected file Service was not available Unknown Error You must specify a URL for this service Project-Id-Version: kdebase-workspace
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2012-09-03 13:51+0000
Last-Translator: Samir Ribić <Unknown>
Language-Team: Bosnian <bs@li.org>
Language: bs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2014-10-21 06:51+0000
X-Generator: Launchpad (build 17203)
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
 Ne mogu prepoznati mimetip datoteke Ne mogu naći sve potrebne funckije Ne mogu naći pružaca s navedenim odredištem Greška u pokušaju izvršenja skripte Pogrešna staza do odabranog pružaoca Nije bilo moguće pročitati odabranu datoteku Usluga nije bila dostupna Nepoznata greška Morate navesti URL za ovu uslugu 