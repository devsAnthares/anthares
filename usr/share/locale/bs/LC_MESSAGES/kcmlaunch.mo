��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5    K     f     l  �  �  �  L	  O   �
     &     8     K  #   \     �     �     �  #   �                                          
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kdebase-workspace
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2012-09-03 12:46+0000
Last-Translator: Samir Ribić <Unknown>
Language-Team: Bosnian <bs@li.org>
Language: bs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2014-10-21 06:45+0000
X-Generator: Launchpad (build 17203)
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
  sek. &Trajanje oznake pokretanja: <H1>Obavještenje na traci sa zadacima</H1>
Možete uključiti i drugu metodu obavještenja o pokretanju koja se
koristi u traci sa zadacima. U ovoj traci se pojavljuje dugme sa
rotirajućim pješčanim satom, što simbolizira da se učitava pokrenuta
aplikacija. Može se desiti da neke aplikacije nisu svjesne ovog
obavještenja o pokretanju. U ovom slučaju, dugme nestaje tek
nakon isteka vremena navedenog u odjeljku 'Trajanje oznake
pokretanja' <h1>Kursor zauzeća</h1>
KDE omogućuje kursor zauzeća kao obavještenje o pokretanju
aplikacije. Da uključite kursor zauzeća, izaberite neku vrstu
vizuelnog odziva sa kombiniranog polja.
Može se desiti da neke aplikacije nisu svjesne ovog obavještenja
o pokretanju. U tom slučaju, kursor će prestati treperiti tek nakon
isteka vremena navedenog u odjeljku 'Trajanje oznake pokretanja'. <h1>Odziv pokretanja</h1> Ovdje možete podesiti odziv programa pri pokretanju. Trepćući kursor Odbijajući kursor Kursor &zauzeća Uključi &obavještenje na taskbaru Bez kursora zauzeća Pasivni kursor zauzeća &Trajanje oznake pokretanja: &Obavještenje na traci sa zadacima 