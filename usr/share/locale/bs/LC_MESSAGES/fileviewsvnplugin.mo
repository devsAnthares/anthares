��          �   %   �      0     1  +   G  .   s  6   �  *   �  #     &   (  /   O  2     :   �  K   �  -   9  $   g  '   �     �     �     �     �  #        8     V     j     �    �     �  #   �  '   �  1      %   2     X     p  '   �  *   �  5   �  R   	  )   g	     �	     �	  	   �	     �	     �	     �	     
     %
     =
     C
     O
                                                    	   
                                                                        @action:buttonCommit @info:statusAdded files to SVN repository. @info:statusAdding files to SVN repository... @info:statusAdding of files to SVN repository failed. @info:statusCommit of SVN changes failed. @info:statusCommitted SVN changes. @info:statusCommitting SVN changes... @info:statusRemoved files from SVN repository. @info:statusRemoving files from SVN repository... @info:statusRemoving of files from SVN repository failed. @info:statusSVN status update failed. Disabling Option "Show SVN Updates". @info:statusUpdate of SVN repository failed. @info:statusUpdated SVN repository. @info:statusUpdating SVN repository... @item:inmenuSVN Add @item:inmenuSVN Commit... @item:inmenuSVN Delete @item:inmenuSVN Update @item:inmenuShow Local SVN Changes @item:inmenuShow SVN Updates @labelDescription: @title:windowSVN Commit Show updates Project-Id-Version: fileviewsvnplugin
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2015-02-04 15:43+0000
Last-Translator: Samir Ribić <Unknown>
Language-Team: bosanski <bs@li.org>
Language: bs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2015-02-05 06:27+0000
X-Generator: Launchpad (build 17331)
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
 Izvrši Dodane datoteke na SVN repozitorij. Dodavanje datoteka u SVN repozitorij... Dodavanje datoteka na SVN repozitorij nije uspio. Izvršenja SVN promjena nisu uspjela. SVN promjene izvršene. Izvršavam SVN promjene... Uklonjene datoteke sa SVN repozitorija. Uklanjanje datoteka sa SVN repozitorija... Uklanjanje datoteka sa SVN repozitorija nije uspjelo. Ažuriranje SVN statusa neuspjelo. Onemogućujem opciju "Pokaži SVN ažuriranja". Nadogradnja SVN repozitorija nije uspela. SVN repozitorij nadograđen. Nadogradnja SVN repozitorija... SVN Dodaj SVN se izvršava... SVN Obriši SVN Ažuriranje Pokaži lokalne SVN promjene Pokaži SVN ažuriranja Opis: SVN Izvrši Prikaži nadogradnje 