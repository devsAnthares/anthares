��    9      �  O   �      �     �     �     �     
          #  �   >  -   �  )   �  -   "  +   P  r   |  	   �  	   �               #     ,  
   9     D     P     X     `      w     �     �     �      �     �     �  r   �  5   r     �     �     �  	   �     �     �     �  (   �     '	     5	     N	     h	     �	     �	  +   �	  3   �	     �	     �	     
     
  S    
  )   t
     �
  �   �
  �  �     ?     H     P     ^     l     r  ~   �  *        7  	   =     G  e   O  	   �  
   �     �     �     �  	   �  
   �                         6     I     Y     _      l     �     �  v   �  1   #     U     f     �     �     �     �  
   �  #   �     �     �     �          8  
   >  $   I  0   n     �     �     �     �  B   �  !        1  �   ?               1                &              0   '                7                            
          -               4      5   #      (       6   3          )                 /   	             9         +                    *      $      !   ,   .   "       8           %      2    &Amount: &Effect: &Second color: &Semi-transparent &Theme (c) 2000-2003 Geert Jansen <qt>Are you sure you want to remove the <strong>%1</strong> icon theme?<br /><br />This will delete the files installed by this theme.</qt> <qt>Installing <strong>%1</strong> theme</qt> @label The icon rendered as activeActive @label The icon rendered as disabledDisabled @label The icon rendered by defaultDefault A problem occurred during the installation process; however, most of the themes in the archive have been installed Ad&vanced All Icons Antonio Larrosa Jimenez Co&lor: Colorize Confirmation Desaturate Description Desktop Dialogs Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Effect Parameters Gamma Geert Jansen Get new themes from the Internet Icons Icons Control Panel Module If you already have a theme archive locally, this button will unpack it and make it available for KDE applications Install a theme archive file you already have locally Main Toolbar NAME OF TRANSLATORSYour names Name No Effect Panel Preview Remove Theme Remove the selected theme from your disk Set Effect... Setup Active Icon Effect Setup Default Icon Effect Setup Disabled Icon Effect Size: Small Icons The file is not a valid icon theme archive. This will remove the selected theme from your disk. To Gray To Monochrome Toolbar Torsten Rahn Unable to download the icon theme archive;
please check that address %1 is correct. Unable to find the icon theme archive %1. Use of Icon You need to be connected to the Internet to use this action. A dialog will display a list of themes from the http://www.kde.org website. Clicking the Install button associated with a theme will install this theme locally. Project-Id-Version: kcmicons
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-08 06:27+0100
PO-Revision-Date: 2009-08-05 21:58+0800
Last-Translator: Sharuzzaman Ahmat Raslan <sharuzzaman@myrealbox.com>
Language-Team: Malay <kedidiemas@yahoogroups.com>
Language: ms
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=1;
X-Generator: KBabel 1.11.4
 Juml&ah: K&esan: &Warna kedua: &Separa-telus &Tema (c) 2000-2003 Geert Jansen <qt>Anda pasti mahu membuang tema ikon <strong>%1</strong>?<br /><br />Ini akan memadam fail yang dipasang oleh tema ini.</qt> <qt>Memasang <strong>%1</strong> tema</qt> Aktif Dimatikan Default Masalah berlaku semasa proses pemasangan; namun begitu, kebanyakan tema di dalam arkib telah dipasang &Lanjutan Semua Ikon Antonio Larrosa Jimenez Wa&rna: Warnai Kepastian Nyah-larut Huraian Desktop Dialog Tarik atau Taip URL Tema md_najmi@yahoo.com Parameter Kesan Gamma Geert Jansen Dapatkan tema baru dari Internet Ikon Modul Panel Kawalan Ikon  Jika anda telah mempunyai arkib tema disini, butang ini akan menyahbungkusnya dan menjadikannya ada untuk aplikasi KDE Pasang arkib fail tema yang anda telah ada disini Bar Alatan Utama Muhammad Najmi bin Ahmad Zabidi Nama Tiada Kesan Panel Prebiu Buang Tema Buang tema dipilih dari cakera anda Tetapka kesan... Tetapkan Kesan Ikon Aktif Tetapkan Kesan Ikon Default Tetapkan Kesan Ikon Dimatikan Saiz: Ikon Kecil Fail bukan arkib tema ikon yang sah. Ini akan membuang tema dipilih dari cakera anda. Kelabu Ke Monokrom Papan Alatan Torsten Rahn Gagal memuatturun  arkib tema ikon;
sila pastikan alamat %1 betul. Gagal mencari arkib tema ikon %1. Kegunaan ikon Anda perlu tersambung ke Internet untuk menggunakan tindakan ini. Dialog akan memaparkan senarai tema dari laman web http://www.kde.org . Menklik butang Pasang berkaitan dengan tema akan memasang tema ini disini. 