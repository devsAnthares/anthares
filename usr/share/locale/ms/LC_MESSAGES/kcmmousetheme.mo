��          �      L      �     �  �   �  m   b  `   �     1     >     V     c     o      �     �     �     �     �     �  ?   �  Y   :  +   �  �  �     s  �   �  |     d   �  	   �     �                (     A     [     l     �     �  
   �  ,   �  L   �  )    	                                                             	                                
       (c) 2003-2007 Fredrik Höglund <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Cursor Theme Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Fredrik Höglund NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. Project-Id-Version: kcminput
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2011-01-12 20:48+0800
Last-Translator: Sharuzzaman Ahmat Raslan <sharuzzaman@gmail.com>
Language-Team: Malay <kedidiemas@yahoogroups.com>
Language: ms
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=1;
 (c) 2003-2007 Fredrik Höglund <qt>Anda pasti anda ingin membuang tema kursor <i>%1</i> ?<br />Ini akan memadamkan semua fail yang dipasang oleh tema ini.</qt> <qt>Anda tidak boleh memadam tema yang anda sedang gunakan.<br />Anda perlu untuk menukar ke tema lain terlebih dahulu.</qt> Tema bernama %1 sudah wujud dalam folder tema ikon anda. Anda ingin menggantikannya dengan yang ini? Kepastian Seting Kursor Diubah Tema Kursor Huraian Seret atau Taip URL Tema sharuzzaman@myrealbox.com Fredrik Höglund Sharuzzaman Ahmat Raslan Nama Tulisganti Tema? Buang Tema Fail %1 bukannya arkib tema kursor yang sah. Tidak dapat memuat turun arkib tema kursor; pastikan alamat %1 adalah betul. Tidak dapat mencari arkib tema kursor %1. 