��          �      �       0  m   1  F   �  J   �  N   1  R   �     �  %   �  $   
  #   /  $   S  �   x        �       �  V   �  W   4  O   �  W   �     4     E     M     U     [  
   a     l                                      	   
                 Close the encrypted container; partitions inside will disappear as they had been unpluggedLock the container Lists all devices which can be ejected, and allows them to be ejected. Lists all devices which can be unmounted, and allows them to be unmounted. Lists all encrypted devices which can be locked, and allows them to be locked. Lists all encrypted devices which can be unlocked, and allows them to be unlocked. Mount the device Note this is a KRunner keyworddevice Note this is a KRunner keywordeject Note this is a KRunner keywordlock Note this is a KRunner keywordmount Unlock the encrypted container; will ask for a password; partitions inside will appear as they had been plugged inUnlock the container Unmount the device Project-Id-Version: plasma_runner_solid
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2011-05-04 13:47+0800
Last-Translator: Sharuzzaman Ahmat Raslan <sharuzzaman@gmail.com>
Language-Team: Malay <kedidiemas@yahoogroups.com>
Language: ms
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=1;
 Kunci bekas Senaraikan semua peranti yang boleh dilenting, dan benarkan mereka untuk dilentingkan. Senaraikan semua peranti yang boleh dinyahlekap, dan benarkan mereka untuk dinyahlekap. Senaraikan semua peranti yang boleh dikunci, dan benarkan mereka untuk dikunci. Senaraikan semua peranti terenkrip yang boleh dibuka, dan benarkan mereka untuk dibuka. Lekapkan peranti peranti lenting kunci lekap Buka bekas Nyahlekap peranti 