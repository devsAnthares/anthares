��          |      �          %   !     G     U     c     u     �     �  
   �     �     �  $   �  �  �  &   �               %     <     S  "   \          �     �      �     
   	                                                   Automatically copy color to clipboard Clear History Color Options Copy to Clipboard Default color format: General Open Color Dialog Pick Color Pick a color Show history When pressing the keyboard shortcut: Project-Id-Version: plasma_applet_kolourpicker
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-12 03:03+0200
PO-Revision-Date: 2017-03-31 21:27+0100
Last-Translator: Andrej Mernik <andrejm@ubuntu.si>
Language-Team: Slovenian <lugos-slo@lugos.si>
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
 Samodejno kopiraj barvo v odložišče Počisti zgodovino Možnosti barve Kopiraj v odložišče Privzeta oblika barve: Splošno Odpri pogovorno okno za izbor barv Izberite barvo Izberi barvo Pokaži zgodovino Ob pritisku tipkovne bližnjice: 