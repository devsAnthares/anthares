��            )   �      �     �     �     �     �     �     �     �  
           )   (     R     V     \     c     v     �     �     �  3   �     �       <   #  5   `  8   �  8   �                    /  �  1               %     2  
   H     S     a     v     �  8   �     �     �     �     �     �          7     M  1   V     �  #   �  K   �  ?   	  D   O	  D   �	  
   �	     �	     �	     
                                                  
                                                                               	        Add files... Add folder... Background frame Change picture every Choose a folder Choose files Configure plasmoid... Fill mode: General Left click image opens in external viewer Pad Paths Paths: Pause on mouseover Preserve aspect crop Preserve aspect fit Randomize items Stretch The image is duplicated horizontally and vertically The image is not transformed The image is scaled to fit The image is scaled uniformly to fill, cropping if necessary The image is scaled uniformly to fit without cropping The image is stretched horizontally and tiled vertically The image is stretched vertically and tiled horizontally Tile Tile horizontally Tile vertically s Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-16 03:31+0100
PO-Revision-Date: 2018-01-11 21:19+0100
Last-Translator: Andrej Mernik <andrejm@ubuntu.si>
Language-Team: Slovenian <lugos-slo@lugos.si>
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
X-Generator: Lokalize 2.0
 Dodaj datoteke ... Dodaj mapo ... Okvir ozadja Spremeni sliko vsakih Izbor mape Izbor datotek Nastavi plasmoid ... Način zapolnitve: Splošno Levi miškin klik odpre sliko v zunanjem pregledovalniku Oblazini Poti Poti: Premor ob prehodu miške Obreži z ohranitvijo razmerja Raztegni z ohranitvijo razmerja Naključen vrstni red Raztegni Slika je podvojena v vodoravni in navpični smeri Slika ni preoblikovana Slika je raztegnjena, da se prilega Slika je raztegnjena z ohranitvijo razmerja in je obrezana, če je potrebno Slika je raztegnjena z ohranitvijo razmerja in brez obrezovanja Slika je raztegnjena v vodoravni in razpostavljena v navpični smeri Slika je raztegnjena v navpični in razpostavljena v vodoravni smeri Razpostavi Razpostavi vodoravno Razpostavi navpično  s 