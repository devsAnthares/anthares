��            )   �      �  !   �  "   �  '   �  ,     #   ;  &   _  !   �  &   �  '   �     �                 V   $  1   {     �  *   �     �        
     
   "     -     6     ;     D     T     [     a     g  �  p     S     [     b     n  	   �  	   �     �  
   �     �     �     �     �     �  N   �  -   5     c  9   t  !   �     �     �     �     	     	     	     	     ,	     5	     :	     ?	                                                                          
                                                    	           @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Borders @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large Application menu Border si&ze: Buttons Close Close by double clicking:
 To open the menu, keep the button pressed until it appears. Close windows by double clicking &the menu button Context help Drag buttons between here and the titlebar Drop here to remove button Get New Decorations... Keep above Keep below Maximize Menu Minimize On all desktops Search Shade Theme Titlebar Project-Id-Version: kcmkwindecoration
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-04-14 02:49+0200
PO-Revision-Date: 2015-11-10 14:30+0100
Last-Translator: Andrej Mernik <andrejm@ubuntu.si>
Language-Team: Slovenian <lugos-slo@lugos.si>
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
 Ogromna Velika Brez obrobe Brez stranskih obrob Običajna Prevelika Zelo majhna Velikanska Zelo velika Meni programa Velikos&t obrobe: Gumbi Zapri Zaprite z dvoklikom:
 Da odprete meni, držite gumb, dokler se meni ne pojavi. &Zapri okna z dvojnim klikom na menijski gumb Vsebinska pomoč Povlecite tukajšnje gumbe do naslovne vrstice in obratno Spustite sem za odstranitev gumba Dobi nove okraske ... Ohrani nad vsemi Ohrani pod vsemi Razpni Meni Skrči Na vseh namizjih Poišči Zvij Tema Naslovna vrstica 