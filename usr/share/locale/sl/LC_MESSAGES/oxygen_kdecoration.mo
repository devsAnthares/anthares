��    ?        Y         p     q  !   �  "   �  &   �  ,   �  #   &  &   J  !   q  &   �  '   �  "   �  #     "   )  '   L     t     �  +   �  2   �  
   �     �               ,     3     G  U   O  7   �     �     �     		     	      	     6	     T	     c	     k	  !   �	     �	  	   �	     �	     �	     �	     �	  &   
     /
     N
     U
     p
     v
     ~
     �
  4   �
  $   �
     �
               /     G     ]     w  &   �     �  �  �     �     �     �     �     �  	   �  	   �     �  
               	   !     +     2     >     V  8   \  =   �  	   �     �     �        
        $     A  W   J  2   �     �     �     �                4     R     _     h     �     �     �     �     �     �     �  '   	      1     R     [     u     {     �     �  6   �  $   �     �          ,  
   =     H     \     q  $   }  "   �     $                               1          "   *   )          :             	         ;   !                          9   =   5   /   3                    (         #   4       8       -   6   
   7          %                                         ?       0   &      +          '         .       ,      >       <   2        &Matching window property:  @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Border @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large @item:inlistbox Button size:Large @item:inlistbox Button size:Normal @item:inlistbox Button size:Small @item:inlistbox Button size:Very Large Active Window Glow Add Add handle to resize windows with no border Allow resizing maximized windows from window edges Animations B&utton size: Border size: Button mouseover transition Center Center (Full Width) Class:  Configure fading between window shadow and glow when window's active state is changed Configure window buttons' mouseover highlight animation Decoration Options Detect Window Properties Dialog Edit Edit Exception - Oxygen Settings Enable/disable this exception Exception Type General Hide window title bar Information about Selected Window Left Move Down Move Up New Exception - Oxygen Settings Question - Oxygen Settings Regular Expression Regular Expression syntax is incorrect Regular expression &to match:  Remove Remove selected exception? Right Shadows Tit&le alignment: Title:  Use the same colors for title bar and window content Use window class (whole application) Use window title Warning - Oxygen Settings Window Class Name Window Drop-Down Shadow Window Identification Window Property Selection Window Title Window active state change transitions Window-Specific Overrides Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-13 05:20+0100
PO-Revision-Date: 2018-01-11 21:10+0100
Last-Translator: Andrej Mernik <andrejm@ubuntu.si>
Language-Team: Slovenian <lugos-slo@lugos.si>
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
X-Generator: Lokalize 2.0
 Uje&majoča lastnost okna:  Ogromna Velika Brez obrobe Brez stranskih obrob Običajna Prevelika Zelo majhna Velikanska Zelo velika Velika Običajna Majhna Zelo velika Žarenje dejavnega okna Dodaj Dodaj ročico za spreminjanje velikosti oken brez obrobe Dovoli spremembo velikosti razpetih oken preko njihovih robov Animacije Velikost g&umbov: Velikost obrobe: Prehod miške čez gumbe Na sredini Na sredini (celotna širina) Razred:  Nastavite postopen prehod med senco in žarenjem, ko se spremeni stanje dejavnosti okna Nastavite animacijo prehoda miške čez gumbe okna Možnosti okraskov Zaznaj lastnosti okna Pogovorno okno Uredi Uredi izjemo - nastavitve Kisika Omogoči/onemogoči to izjemo Vrsta izjeme Splošno Skrij nazivno vrstico okna Podatki o izbranem oknu Levo Premakni dol Premakni gor Nova izjema - nastavitve Kisika Vprašanje - nastavitve Kisika Regularni izraz Skladnja regularnega izraza ni pravilna Ujemanje &z regularnim izrazom:  Odstrani Odstranim izbrano izjemo? Desno Sence Poravnava nas&lova: Naslov:  Uporabi enake barve za nazivno vrstico in vsebino okna Uporabi okenski razred (cel program) Uporabi naslov okna Opozorilo - nastavitve Kisika Ime razreda okna Senca okna Identifikacija oken Izbor lastnosti okna Naslov okna Prehod pri spremembi dejavnosti okna Nastavitve glede na določeno okno 