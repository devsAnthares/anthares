��          �            x  
   y  	   �     �     �  3   �     �     �                    %     *     F  B   Y     �  �  �     �     �     �     �  8   �            
   (     3  	   C     M  $   V     {     �     �     	                                                  
                        Appearance Browse... Choose an image Fifteen Puzzle Image Files (*.png *.jpg *.jpeg *.bmp *.svg *.svgz) Number color Path to custom image Piece color Show numerals Shuffle Size Solve by arranging in order Solved! Try again. The time since the puzzle started, in minutes and secondsTime: %1 Use custom image Project-Id-Version: plasma_applet_fifteenPuzzle
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-01-12 03:59+0100
PO-Revision-Date: 2017-04-01 15:28+0100
Last-Translator: Andrej Mernik <andrejm@ubuntu.si>
Language-Team: Slovenian <lugos-slo@lugos.si>
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
 Videz Prebrskaj ... Izberi sliko Petnajst kosov Datoteke s sliko (*.png *.jpg *.jpeg *.bmp *.svg *.svgz) Barva številke Pot do slike po meri Barva kosa Pokaži števke Premešaj Velikost Rešite s preurejanjem vrstnega reda Rešeno! Poskusite znova. Čas: %1 Uporabi sliko po meri 