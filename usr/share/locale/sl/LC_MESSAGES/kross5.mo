��    $      <  5   \      0     1  +   E     q  4   �  &   �     �     �                     5     :     P     X  ,   u  3   �     �     �               !  '   .     V     u     {     �     �     �     �     �     �  &   �       B        b  �  y     S  %   Y          �     �     �  	   �     �     �  h   �     B	     H	  	   ^	     h	  4   	  :   �	  $   �	  1   
  	   F
     P
     W
      `
  C   �
     �
     �
     �
     �
          !  $   )     N  %   W  	   }  8   �     �                                       
                              !                                  $      #                               	                           "             @info:creditAuthor @info:creditCopyright 2006 Sebastian Sauer @info:creditSebastian Sauer @info:shell command-line argumentThe script to run. @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: application descriptionCommand-line utility to run Kross scripts. application nameKross Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2014-06-12 14:52+0200
Last-Translator: Andrej Mernik <andrejm@ubuntu.si>
Language-Team: Slovenian <lugos-slo@lugos.si>
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
 Avtor Avtorske pravice 2006 Sebastian Sauer Sebastian Sauer Skript za zagon. Splošno Dodaj nov skript. Dodaj ... Prekličem? Opomba: roman.maurer@amis.net,gregor.rakar@kiss.si,andrej.vernekar@gmail.com,jlp@holodeck1.com,andrejm@ubuntu.si Uredi Uredi izbrani skript. Uredi ... Izvedi izbrani skript. Ni bilo mogoče ustvariti skripta za tolmač »%1«. Določitev tolmača za skriptno datoteko »%1« ni uspela. Nalaganje tolmača »%1« ni uspelo. Ni bilo mogoče odpreti skriptne datoteke »%1«. Datoteka: Ikona: Tolmač: Nivo varnosti za tolmač za Ruby Roman Maurer,Gregor Rakar,Andrej Vernekar,Jure Repinc,Andrej Mernik Ime: Funkcija »%1« ne obstaja Tolmač »%1« ne obstaja Odstrani Odstrani izbrani skript. Zaženi Skriptna datoteka »%1« ne obstaja. Zaustavi Zaustavi izvajanje izbranega skripta. Besedilo: KDE-jev ukazni pripomoček za zaganjanje skriptov Kross. Kross 