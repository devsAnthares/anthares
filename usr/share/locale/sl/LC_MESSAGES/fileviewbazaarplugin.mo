��    !      $  /   ,      �  .   �  1     9   J     �  -   �  &   �  )   �  .   #  &   R  )   y  .   �  &   �  )   �  2   #  5   V  =   �  #   �     �  !     '   /  "   W  0   z  '   �  *   �     �          7     R     j     �     �  &   �  �  �  -   �	  *   �	  5   	
     ?
  *   \
  &   �
  !   �
  ,   �
  #   �
  !   !  -   C  &   q  "   �  3   �  0   �  ;      %   \     �     �  )   �     �  -   �  (   *      S     t     �     �     �     �     �     �  %   �                                                              	                                
                                    !                             @info:statusAdded files to Bazaar repository. @info:statusAdding files to Bazaar repository... @info:statusAdding of files to Bazaar repository failed. @info:statusBazaar Log closed. @info:statusCommit of Bazaar changes failed. @info:statusCommitted Bazaar changes. @info:statusCommitting Bazaar changes... @info:statusPull of Bazaar repository failed. @info:statusPulled Bazaar repository. @info:statusPulling Bazaar repository... @info:statusPush of Bazaar repository failed. @info:statusPushed Bazaar repository. @info:statusPushing Bazaar repository... @info:statusRemoved files from Bazaar repository. @info:statusRemoving files from Bazaar repository... @info:statusRemoving of files from Bazaar repository failed. @info:statusReview Changes failed. @info:statusReviewed Changes. @info:statusReviewing Changes... @info:statusRunning Bazaar Log failed. @info:statusRunning Bazaar Log... @info:statusUpdate of Bazaar repository failed. @info:statusUpdated Bazaar repository. @info:statusUpdating Bazaar repository... @item:inmenuBazaar Add... @item:inmenuBazaar Commit... @item:inmenuBazaar Delete @item:inmenuBazaar Log @item:inmenuBazaar Pull @item:inmenuBazaar Push @item:inmenuBazaar Update @item:inmenuShow Local Bazaar Changes Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2012-06-29 17:08+0200
Last-Translator: Andrej Mernik <andrejm@ubuntu.si>
Language-Team: Slovenian <lugos-slo@lugos.si>
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
 Datoteke so bile dodane v skladišče Bazaar. Dodajanje datotek v skladišče Bazaar ... Dodajanje datotek v skladišče Bazaar je spodletelo. Dnevnik Bazaar je bil zaprt. Uveljavitev sprememb Bazaar je spodletelo. Spremembe Bazaar so bile uveljavljene. Uveljavljanje sprememb Bazaar ... Prejemanje skladišča Bazaar je spodletelo. Skladišče Bazaar je bilo prejeto. Prejemanje skladišča Bazaar ... Objavljanje skladišča Bazaar je spodletelo. Skladišče Bazaar je bilo objavljeno. Objavljanje skladišča Bazaar ... Datoteke so bile odstranjene iz skladišča Bazaar. Odstranjevanje datotek iz skladišča Bazaar ... Odstranjevanje datotek iz skladišča Bazaar je spodletelo. Pregledovanje sprememb je spodletelo. Pregledane spremembe. Pregledovanje sprememb ... Zaganjanje dnevnika Bazaar je spodletelo. Zaganjanje dnevnika Bazaar ... Posodobitev skladišča Bazaar je spodletela. Skladišče Bazaar je bilo posodobljeno. Posodabljanje skladišča Bazaar Bazaar dodaj ... Bazaar uveljavi ... Bazaar izbriši Bazaar dnevnik Bazar prejmi Bazaar objavi Bazaar posodobi Prikaži krajevne spremembe Bazaar-ja 