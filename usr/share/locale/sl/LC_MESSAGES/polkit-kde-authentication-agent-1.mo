��            )   �      �  ;   �  J   �          /  ~   7  A   �     �       )        G     X     i      q     �     �     �  
   �     �  
   �     �             "   <     _  	   y     �     �     �  �  �     ~     �     �     �  o   �  6        Q     Z  $   p     �     �     �     �     �     �     	     	     &	     4	     <	     Q	  C   a	  >   �	     �	     �	     
     !
     3
                                      
                                                                                            	        %1 is the full user name, %2 is the user login name%1 (%2) %1 is the name of a detail about the current action provided by polkit%1: (c) 2009 Red Hat, Inc. Action: An application is attempting to perform an action that requires privileges. Authentication is required to perform this action. Another client is already authenticating, please try again later. Application: Authentication Required Authentication failure, please try again. Click to edit %1 Click to open %1 Details EMAIL OF TRANSLATORSYour emails Former maintainer Jaroslav Reznik Lukáš Tinkl Maintainer NAME OF TRANSLATORSYour names P&assword: Password for %1: Password for root: Password or swipe finger for %1: Password or swipe finger for root: Password or swipe finger: Password: PolicyKit1 KDE Agent Select User Vendor: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:22+0100
PO-Revision-Date: 2015-01-18 11:59+0100
Last-Translator: Andrej Mernik <andrejm@ubuntu.si>
Language-Team: Slovenian <lugos-slo@lugos.si>
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
 %1 (%2) %1: (c) 2009 Red Hat, Inc. Dejanje: Program poskuša izvesti dejanje za katerega potrebuje dovoljenja. Za izvršitev dejanja je zahtevana overitev. Drug odjemalec se že overja. Poskusite znova kasneje. Program: Zahtevana je overitev Overitev ni uspela. Poskusite znova. Kliknite za urejanje %1 Kliknite za odpiranje %1 Podrobnosti andrejm@ubuntu.si Prejšnji vzdrževalec Jaroslav Reznik Lukáš Tinkl Vzdrževalec Andrej Mernik &Geslo: Geslo uporabnika %1: Geslo skrbnika: Vnesite geslo ali povlecite prst, da se prijavite kot uporabnik %1: Vnesite geslo ali povlecite prst, da se prijavite kot skrbnik: Geslo ali povlecite prst: Geslo: Posrednik PolicyKit1 za KDE Izberi uporabnika Proizvajalec: 