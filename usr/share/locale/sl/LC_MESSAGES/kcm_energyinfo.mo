��    7      �  I   �      �     �     �     �     �     �     �               $     -     5     H     T      `     �     �     �     �     �     �     �     �                     )     7  	   C  	   M     W     d     j     �     �     �     �     �     �     �     �     �     �  @   �     7     @     \     c     j     r     �     �     �  4   �     �  �  �     �	     �	     �	     �	     �	     �	      
     
  	   ,
     6
     :
     >
  
   N
     Y
     k
     t
     �
     �
     �
     �
     �
     �
     �
     �
     �
               %  
   4     ?     L     R     `     c     o     w          �     �     �     �     �  6   �     �  +   	  
   5     @     B     K     i     k     n     q     x     &   0      7                  #                
          	   '   (   +                       6              3      1                   ,   2                  "       *          $      4   )               %      5                     !          /   .   -       % %1 is value, %2 is unit%1 %2 %1: Application Energy Consumption Battery Capacity Charge Percentage Charge state Charging Current Degree Celsius°C Details: %1 Discharging EMAIL OF TRANSLATORSYour emails Energy Energy Consumption Energy Consumption Statistics Environment Full design Fully charged Has power supply Kai Uwe Broulik Last 12 hours Last 2 hours Last 24 hours Last 48 hours Last 7 days Last full Last hour Manufacturer Model NAME OF TRANSLATORSYour names No Not charging PID: %1 Path: %1 Rechargeable Refresh Serial Number Shorthand for WattsW System Temperature This type of history is currently not available for this device. Timespan Timespan of data to display Vendor VoltV Voltage Wakeups per second: %1 (%2%) WattW Watt-hoursWh Yes current power draw from the battery in WConsumption literal percent sign% Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2016-07-11 18:00+0200
Last-Translator: Andrej Mernik <andrejm@ubuntu.si>
Language-Team: Slovenian <lugos-slo@lugos.si>
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
X-Generator: Lokalize 1.5
 % %1 %2 %1: Poraba energije programa Baterija Zmogljivost Odstotek napolnjenosti Stanje napolnjenosti Polnjenje Tok °C Podrobnosti: %1 Praznjenje andrejm@ubuntu.si Energija Poraba energije Statistika porabe energije Okolje Celotna zasnova Povsem polna Ima napajalnik Kai Uwe Broulik Zadnjih 12 ur Zadnji 2 uri Zadnjih 24 ur Zadnja 2 dni Zadnji teden Nazadnje polna Zadnja ura Proizvajalec Model Andrej Mernik Ne Se ne polni PID: %1 Pot: %1 Znova napolnljiva Osveži Zaporedna številka W Sistem Temperatura Ta vrsta zgodovina trenutno ni na voljo za to napravo. Časovni razpon Časovni razpon podatkov, ki bodo prikazani Prodajalec V Napetost Zbujanj na sekundo: %1 (%2 %) W Wh Da Poraba % 