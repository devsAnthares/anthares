��          �   %   �      @     A  !   Y      {     �      �     �     �  +        =     N     [  (   z     �  ,   �  7   �     !  7   :     r  ]   �  1   �  	   "     ,  "   ?  5   b  �  �  #   j  -   �  "   �     �     �  '        4  "   T     w     �     �  *   �     �  *   �  <   	     O	  6   c	     �	  i   �	  /   
     M
     V
  )   e
  ,   �
                    
         	                                                                                                 Cannot start initramfs. Cannot start update-alternatives. Configure Plymouth Splash Screen Download New Splash Screens EMAIL OF TRANSLATORSYour emails Get New Boot Splash Screens... Initramfs failed to run. Initramfs returned with error condition %1. Install a theme. Marco Martin NAME OF TRANSLATORSYour names No theme specified in helper parameters. Plymouth theme installer Select a global splash screen for the system The theme to install, must be an existing archive file. Theme %1 does not exist. Theme corrupted: .plymouth file not found inside theme. Theme folder %1 does not exist. This module lets you configure the look of the whole workspace with some ready to go presets. Unable to authenticate/execute the action: %1, %2 Uninstall Uninstall a theme. update-alternatives failed to run. update-alternatives returned with error condition %1. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-28 03:05+0200
PO-Revision-Date: 2017-06-04 13:15+0100
Last-Translator: Andrej Mernik <andrejm@ubuntu.si>
Language-Team: Slovenian <lugos-slo@lugos.si>
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
X-Generator: Lokalize 2.0
 Ni mogoče zagnati ukaza initramfs. Ni mogoče zagnati ukaza update-alternatives. Nastavi pozdravno okno za Plymouth Prejmi nova pozdravna okna andrejm@ubuntu.si Prejmi nova zagonska pozdravna okna ... Zagon ukaza initramfs ni uspel. Ukaz initramfs je vrnil napako %1. Namesti temo. Marco Martin Andrej Mernik V parametrih pomagalnika ni navedene teme. Namestilnik tem za Plymouth Izberite splošno pozdravno okno za sistem Tema za namestitev. Mora biti obstoječa datoteka z arhivom. Tema %1 ne obstaja. Tema je pokvarjena, saj ne vsebuje datoteke .plymouth. Mapa teme %1 ne obstaja. Ta modul vam omogoči nastaviti izgled celotne delovne površine s pomočjo nekaterih predlog nastavitev. Ni bilo mogoče overiti/izvesti dejanja: %1, %2 Odstrani Odstrani temo. Zagon ukaza update-alternatives ni uspel. Ukaz update-alternatives je vrnil napako %1. 