��          t      �         P     )   b  %   �  @   �     �  V   	     `     {     �     �  �  �     x  _     d   �  _   D      �  �   �  !   J     l     �     �                         	      
                             %1 is '%1 packages to update' and %2 is 'of which %1 is security updates'%1, %2 1 package to update %1 packages to update 1 security update %1 security updates First part of '%1, %2'1 package to update %1 packages to update No packages to update Second part of '%1, %2'of which 1 is security update of which %1 are security updates Security updates available System up to date Update Updates available Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-15 03:15+0100
PO-Revision-Date: 2018-01-15 17:13+0100
Last-Translator: Andrej Mernik <andrejm@ubuntu.si>
Language-Team: Slovenian <lugos-slo@lugos.si>
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
X-Generator: Lokalize 2.0
 %1, %2 %1 paketov za posodobiti %1 paket za posodobiti %1 paketa za posodobiti %1 paketi za posodobiti %1 varnostnih posodobitev %1 varnostna posodobitev %1 varnostni posodobitvi %1 varnostne posodobitve %1 paketov za posodobiti %1 paket za posodobiti %1 paketa za posodobiti %1 paketi za posodobiti Za posodobiti ni nobenega paketa od tega %1 varnostnih posodobitev od tega %1 varnostna posodobitev od tega %1 varnostni posodobitvi od tega %1 varnostne posodobitve Na voljo so varnostne posodobitve Sistem je posodobljen Posodobi Na voljo so posodobitve 