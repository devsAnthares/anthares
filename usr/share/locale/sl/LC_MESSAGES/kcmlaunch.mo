��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  �  K     &  $   ,  �  Q  {  �  G   Z
     �
     �
     �
  '   �
     	     %  $   D     i                                          
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2013-06-16 16:37+0200
Last-Translator: Andrej Mernik <andrejm@ubuntu.si>
Language-Team: Slovenian <lugos-slo@lugos.si>
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
  sek. Ča&sovni pretek obvestila o zagonu: <H1>Obvestilo v opravilni vrstici</H1>
Omogočite lahko tudi drugo metodo za obvestilo o zagonu, ki jo
uporablja opravilna vrstica, pri kateri se prikaže vrteča se peščena ura,
ki simbolizira zagon programa.
Zgodi se lahko, da se nekateri programi ne zavedajo tega obvestila
ob zagonu. V tem primeru kazalka preneha utripati po času
navedenem v odseku »Časovni pretek obvestila o zagonu«. <h1>Kazalka za zaposlenost</h1>
KDE ponuja kazalko za zaposlenost kot obvestilo ob zagonu
programa. Da bi kazalko za zaposlenost omogočili, izberite
eno vrsto vidnega odziva iz spustnega polja.
Zgodi se lahko, da se nekateri programi ne zavedajo tega obvestila
ob zagonu. V tem primeru kazalka preneha utripati po času
navedenem v odseku »Časovni pretek obvestila o zagonu«. <h1>Odziv zagona</h1> Tu lahko nastavite odziv programa, ki se zaganja. Utripajoča kazalka Skakajoča kazalka Kazalka za za&poslenost Omogoči obves&tilo v opravilni vrstici Brez kazalke za zaposlenost Pasivna kazalka za zaposlenost Časovni pretek obvestila o zagon&u: Obvestilo v opravil&ni vrstici 