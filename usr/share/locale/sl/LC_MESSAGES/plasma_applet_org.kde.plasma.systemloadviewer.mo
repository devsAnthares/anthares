��    &      L  5   |      P     Q     l     y     �     �     �     �     �     �     �     �     �  &   �               #     ,     3     ?     M     U     ]     d     s     �     �     �     �     �     �     �     �     �     �  
   �            �       �     �     �               +     /     6     K  	   `     j     v  5   �     �     �     �     �     �                !  	   2     <     Q     f     x     �     �     �  #   �  !   �     �     	     	  "   3	  
   V	  (   a	                           	      #      $                                                    &                                          !          %                            "   
    Abbreviation for secondss Application: Average clock: %1 MHz Bar Buffers: CPU CPU %1 CPU monitor CPU%1: %2% @ %3 Mhz CPU: %1% CPUs separately Cache Cache Dirty, Writeback: %1 MiB, %2 MiB Cache monitor Cached: Circular Colors Compact Bar Dirty memory: General IOWait: Memory Memory monitor Memory: %1/%2 MiB Monitor type: Nice: Set colors manually Show: Swap Swap monitor Swap: %1/%2 MiB Sys: System load Update interval: Used swap: User: Writeback memory: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-29 03:31+0200
PO-Revision-Date: 2017-04-01 16:13+0100
Last-Translator: Andrej Mernik <andrejm@ubuntu.si>
Language-Team: Slovenian <lugos-slo@lugos.si>
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
X-Generator: Lokalize 2.0
 s Program: Povprečna frekvenca: %1 MHz Stolpec Medpomnilniki: CPE CPE %1 Nadzornik procesorja CPE%1: %2 % @ %3 MHz CPE: %1 % Ločene CPE Predpomnilnik Neočiščen predpomnilnik, Writeback: %1 MiB, %2 MiB Nadzornik predpomnilnika Predpomnjeno: Krožno Barve Strnjena vrstica Neočiščen pomnilnik: Splošno Čakanje na V/I: Pomnilnik Nadzornik pomnilnika Pomnilnik: %1/%2 MiB Vrsta nadzornika: Prijaznost: Ročno nastavi barve Pokaži: Izmenjevalni pomnilnik Nadzornik izmenjevalnega pomnilnika Izmenjevalni pomnilnik: %1/%2 MiB Sist: Obremenitev sistema Razmik med posodobitvami: Uporabljen izmenjevalni pomnilnik: Uporabnik: Pomnilnik »zapiši nazaj« (writeback): 