��          �      �       0     1  �  H     �       &         ?     `     n     v     �     �  (   �  �  �     �  �  �     |     �  "   �  N   �            3   $     X     h  %   x         
                 	                                (c) 2002-2006 KDE Team <h1>System Notifications</h1>Plasma allows for a great deal of control over how you will be notified when certain events occur. There are several choices as to how you are notified:<ul><li>As the application was originally designed.</li><li>With a beep or other noise.</li><li>Via a popup dialog box with additional information.</li><li>By recording the event in a logfile without any additional visual or audible alert.</li></ul> Carsten Pfeiffer Charles Samuels Disable sounds for all of these events EMAIL OF TRANSLATORSYour emails Event source: KNotify NAME OF TRANSLATORSYour names Olivier Goffart Original implementation System Notification Control Panel Module Project-Id-Version: kcmnotify
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-09 07:18+0200
PO-Revision-Date: 2018-01-11 20:50+0100
Last-Translator: Andrej Mernik <andrejm@ubuntu.si>
Language-Team: Slovenian <lugos-slo@lugos.si>
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
 © 2002-2006 Ekipa KDE <h1>Sistemska obvestila</h1>Plasma dovoljuje precej nadzora nad tem, kako boste obveščeni ob določenih dogodkih. Obstaja nekaj možnosti o tem, kako ste obveščeni:<ul><li>kot je bil program prvotno zasnovan.</li><li>s piskom ali drugim zvokom.</li><li>s pogovornim oknom z dodatnimi podrobnostmi.</li><li>z beleženjem dogodka v datoteko z dnevnikom brez vsakršnih dodatnih vidnih ali zvočnih opozoril.</li></ul> Carsten Pfeiffer Charles Samuels Onemogoči zvoke za vse te dogodke roman.maurer@amis.net,gregor.rakar@kiss.si,jlp@holodeck1.com,andrejm@ubuntu.si Vir dogodka: KNotify Roman Maurer,Gregor Rakar,Jure Repinc,Andrej Mernik Olivier Goffart Izvirna izvedba Nadzorni modul za sistemska obvestila 