��          �            x     y  
   �  
   �     �  .   �     �     �     �        *   )      T  ,   u  ,   �  0   �        �        �          
       5        M     S     h     u  1   �     �     �     �  /        8                        
                                            	           &Lock screen on resume: Activation Appearance Error Failed to successfully test the screen locker. Immediately Keyboard shortcut: Lock Session Lock screen automatically after: Lock screen when waking up from suspension Re&quire password after locking: Spinbox suffix. Short for minutes min  mins Spinbox suffix. Short for seconds sec  secs The global keyboard shortcut to lock the screen. Wallpaper &Type: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:10+0100
PO-Revision-Date: 2018-01-11 21:29+0100
Last-Translator: Andrej Mernik <andrejm@ubuntu.si>
Language-Team: Slovenian <lugos-slo@lugos.si>
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
X-Generator: Lokalize 2.0
 Zak&leni zaslon ob nadaljevanju: Vklop Videz Napaka Ni bilo mogoče uspešno preizkusiti zaklepa zaslona. Takoj Tipkovna bližnjica: Zakleni sejo Samodejno zakleni zaslon po: Zakleni zaslon med prebujanjem iz pripravljenosti Po zaklepu z&ahtevaj geslo:  min  min  min  min  sek  sek  sek  sek Splošna tipkovna bližnjica za zaklep zaslona. Vrs&ta slike ozadja: 