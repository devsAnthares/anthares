��    *      l  ;   �      �     �     �  )   �               /     H     ^  #   ~     �  
   �     �     �  %   �  +        E     Z     m  @   z     �     �     �     �               '     ,     9     ?     _     e  .   m     �  F   �  (   �  	   '  	   1  	   ;  '   E  7   m  "   �  �  �     �	     �	  !   �	     �	  	   �	     �	     
     
     !
     8
  
   K
     V
     o
  1   �
  8   �
     �
          )  K   /     {  
   �  
   �     �     �     �     �     �     �     �       	     (        D  P   a  &   �  
   �     �     �     �          #                   $                                 %                      *                                               	   #   )   "             &           
                        '             (   !    &Do not remember @actionWalk through activities @actionWalk through activities (Reverse) @action:buttonApply @action:buttonCancel @action:buttonChange... @action:buttonCreate @title:windowActivity Settings @title:windowCreate a New Activity @title:windowDelete Activity Activities Activity information Activity switching Are you sure you want to delete '%1'? Blacklist all applications not on this list Clear recent history Create activity... Description: Error loading the QML files. Check your installation.
Missing %1 For a&ll applications Forget a day Forget everything Forget the last hour Forget the last two hours General Icon Keep history Name: O&nly for specific applications Other Privacy Private - do not track usage for this activity Remember opened documents: Remember the current virtual desktop for each activity (needs restart) Shortcut for switching to this activity: Shortcuts Switching Wallpaper for in 'keep history for 5 months'for  unit of time. months to keep the history month  months unlimited number of monthsforever Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-04-02 20:02+0200
Last-Translator: Andrej Mernik <andrejm@ubuntu.si>
Language-Team: Slovenian <lugos-slo@lugos.si>
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
X-Generator: Lokalize 1.5
 &Ne zapomni si Sprehod med dejavnostmi Sprehod med dejavnostmi (obratno) Uveljavi Prekliči Spremeni ... Ustvari Nastavitve dejavnosti Ustvari novo dejavnost Izbriši dejavnost Dejavnosti Podrobnosti o dejavnosti Preklapljanje med dejavnostmi Ali ste prepričani, da želite izbrisati »%1«? Na črni seznam daj vse programe, ki niso na tem seznamu Počisti nedavno zgodovino Ustvari dejavnost ... Opis: Napaka med nalaganjem datotek QML. Preverite vašo namestitev.
Manjkajo: %1 Za &vse programe Pozabi dan Pozabi vse Pozabi zadnjo uro Pozabi zadnji dve uri Splošno Ikona Ohrani zgodovino Ime: Sa&mo za določene programe Drugo Zasebnost Zasebno - ne sledi uporabi te dejavnosti Zapomni si odprte dokumente: Zapomni si trenutno navidezno namizje za vsako dejavnost (zahteva ponovni zagon) Bližnjica za preklop na to dejavnost: Bližnjice Preklapljanje Slika ozadja za   mesecev  mesec  meseca  mesece trajno 