��          �      <      �     �     �     �     �     �  '   �  >        L     f     �     �     �  )   �  7   �  '        B     T  �  s     E     X     a     n  	   }     �     �     �  %   �  !   �     �       !   $  	   F     P     W     s                                       
                      	                                        Current user General Layout Lock Screen New Session Nobody logged in on that sessionUnused Show a dialog with options to logout/shutdown/restartLeave... Show both avatar and name Show full name (if available) Show login username Show only avatar Show only name Show technical information about sessions User logged in on console (X display number)on %1 (%2) User logged in on console numberTTY %1 User name display You are logged in as <b>%1</b> Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-11 03:18+0100
PO-Revision-Date: 2016-05-15 10:26+0200
Last-Translator: Andrej Mernik <andrejm@ubuntu.si>
Language-Team: Slovenian <lugos-slo@lugos.si>
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
X-Generator: Lokalize 1.5
 Trenutni uporabnik Splošno Razporeditev Zakleni zaslon Nova seja Neuporabljeno Zapusti ... Pokaži podobo in ime Pokaži celotno ime (če je na voljo) Pokaži prijavno uporabniško ime Pokaži samo podobo Pokaži samo ime Pokaži tehnične podatke o sejah v %1 (%2) TTY %1 Prikaz uporabniškega imena Prijavljeni ste kot <b>%1</b> 