��          �      |      �     �  2        B     b       #   �      �     �  %   �       
   &     1     >     ]  �   }       ]   (     �  p   �  :        P  �  \  $   .  A   S  '   �  #   �     �  (         )     ;  ,   O     |     �     �     �  !   �  �   �     �	  i   �	      
  c   2
  9   �
  	   �
                                                                      
                	           Apply a look and feel package Command line tool to apply look and feel packages. Configure Look and Feel details Copyright 2017, Marco Martin Cursor Settings Changed Download New Look And Feel Packages EMAIL OF TRANSLATORSYour emails Get New Looks... List available Look and feel packages Look and feel tool Maintainer Marco Martin NAME OF TRANSLATORSYour names Reset the Plasma Desktop layout Select an overall theme for your workspace (including plasma theme, color scheme, mouse cursor, window and desktop switcher, splash screen, lock screen etc.) Show Preview This module lets you configure the look of the whole workspace with some ready to go presets. Use Desktop Layout from theme Warning: your Plasma Desktop layout will be lost and reset to the default layout provided by the selected theme. You have to restart KDE for cursor changes to take effect. packagename Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-27 05:46+0100
PO-Revision-Date: 2018-01-11 20:46+0100
Last-Translator: Andrej Mernik <andrejm@ubuntu.si>
Language-Team: Slovenian <lugos-slo@lugos.si>
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
X-Generator: Lokalize 2.0
 Uveljavi paket za Izgled in občutek Orodje ukazne vrstice za uveljavitev paketov Izgleda in občutka. Nastavi podrobnosti izgleda in občutka Avtorske pravice 2017, Marco Martin Spremenjene nastavitve kazalke Prejmi nove pakete za Izgled in občutek andrejm@ubuntu.si Dobi nov izgled ... Izpiši seznam paketov za Izgled in občutek Orodje za Izgled in občutek Vzdrževalec Marco Martin Andrej Mernik Ponastavi razpored namizja Plasma Izberite splošno temo za vašo delovno površino (vključno s temo Plasme, barvno shemo, kazalko miške, preklopnikom oken in namizja, pozdravnim oknom, zaklepom zaslona, itd.) Pokaži predogled Ta modul vam omogoči nastaviti izgled celotne delovne površine s pomočjo nekaterih predlog nastavitev. Uporabi razpored namizja iz teme Opozorilo: vaš razpored namizja Plasme bo izgubljen in ponastavljen na privzetega za izbrano temo. Za uveljavitev sprememb kazalke morate znova zagnati KDE. imepaketa 