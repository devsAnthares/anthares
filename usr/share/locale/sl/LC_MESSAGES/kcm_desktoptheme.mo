��          �      �       H     I     a      m     �     �     �  
   �     �  &   �          .     L  1   b  �  �     }     �  )   �     �     �     �  
          ,   *     W  "   v     �  -   �         	                                    
                    Configure Desktop Theme David Rosca EMAIL OF TRANSLATORSYour emails Get New Themes... Install from File... NAME OF TRANSLATORSYour names Open Theme Remove Theme Theme Files (*.zip *.tar.gz *.tar.bz2) Theme installation failed. Theme installed successfully. Theme removal failed. This module lets you configure the desktop theme. Project-Id-Version: kcm_desktopthemedetails
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-22 05:21+0100
PO-Revision-Date: 2018-01-11 20:43+0100
Last-Translator: Andrej Mernik <andrejm@ubuntu.si>
Language-Team: Slovenian <lugos-slo@lugos.si>
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
 Nastavi namizno temo David Rosca andrej.vernekar@moj.net,andrejm@ubuntu.si Dobi nove teme ... Namesti iz datoteke ... Andrej Vernekar,Andrej Mernik Odpri temo Odstrani temo Datoteke s temami (*.zip *.tar.gz *.tar.bz2) Namestitev teme je spodletela. Tema je bila uspešno nameščena. Odstranitev teme je spodletela. Ta modul vam omogoča nastaviti namizno temo. 