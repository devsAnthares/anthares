��    =        S   �      8  ;   9     u     �     �     �  6   �  A   
     L     U  $   Y  
   ~     �  #   �     �     �     �     �     �  7        >     [     w  '   �     �     �  $   �  ,   �          3     C     K     ]     y     �     �     �     �     �  �   �  9   �	  "   �	     �	     �	     
     *
     <
     R
     c
  %   u
     �
     �
     �
     �
     �
  
   �
  7        :     T     l     t  �  �  >   j     �     �     �     �               3     <  '   @     h     n  +   �     �  !   �     �     �       4        E     ]     x  0   ~     �     �  %   �  2   �     )     <     L     U  $   j     �     �     �     �     �     �  �   �  <   �  %   �     �  )   �     #     6     M     h     ~  (   �     �     �     �     �       
   
          5     9     =     C     !          3      0      (                 1      "          =   .   6                                         	      :          5       ,   #   &   7       ;   '             -   
             8                 *           %           <   4   9                )   /   $          2             +       

Choose the previous strip to go to the last cached strip. &Create Comic Book Archive... &Save Comic As... &Strip Number: *.cbz|Comic Book Archive (Zip) @option:check Context menu of comic image&Actual Size @option:check Context menu of comic imageStore current &Position Advanced All An error happened for identifier %1. Appearance Archiving comic failed Automatically update comic plugins: Cache Check for new comic strips: Comic Comic cache: Configure... Could not create the archive at the specified location. Create %1 Comic Book Archive Creating Comic Book Archive Destination: Display error when getting comic failed Download Comics Error Handling Failed adding a file to the archive. Failed creating the file with identifier %1. From beginning to ... From end to ... General Get New Comics... Getting comic strip failed: Go to Strip Information Jump to &current Strip Jump to &first Strip Jump to Strip ... Manual range Maybe there is no Internet connection.
Maybe the comic plugin is broken.
Another reason might be that there is no comic for this day/number/string, so choosing a different one might work. Middle-click on the comic to show it at its original size No zip file is existing, aborting. Range: Show arrows only on mouse over Show comic URL Show comic author Show comic identifier Show comic title Strip identifier: The range of comic strips to archive. Update Visit the comic website Visit the shop &website an abbreviation for Number# %1 days dd.MM.yyyy here strip means comic strip&Next Tab with a new Strip in a range: from toFrom: in a range: from toTo: minutes strips per comic Project-Id-Version: plasma_applet_comic
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-07-18 03:20+0200
PO-Revision-Date: 2015-04-03 12:07+0200
Last-Translator: Andrej Mernik <andrejm@ubuntu.si>
Language-Team: Slovenian <lugos-slo@lugos.si>
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
 

Izberite predhodno sliko za ogled zadnje predpomnjene slike. &Ustvari arhiv stripov ... &Shrani strip kot ... Številka &slike: *.cbz|Arhiv stripov (Zip) Dej&anska velikost Shrani trenutni &položaj Napredno Vse Za določilnik %1 je prišlo do napake. Videz Arhiviranje stripa ni uspelo Samodejno posodobi vstave vstavke s stripi: Predpomnilnik Preveri za novimi slikami stripa: Strip Predpomnilnik stripov: Nastavi ... Arhiva na navedenem mestu ni bilo mogoče ustvariti. Ustvari arhiv stripa %1 Ustvarjanje arhiva stripov Cilj: Prikaži napako, če pridobivanje stripa ne uspe Prejmi stripe Obravnavanje napak Dodajanje datoteke v arhiv ni uspelo. Ustvarjanje datoteke z določilnikom %1 ni uspelo. Od začetka do ... Od konca do ... Splošno Dobi nove stripe ... Pridobivanje slike stripa ni uspelo: Pojdi na sliko Podrobnosti Skoči na &trenutno sliko Skoči na &prvo sliko Skoči na sliko ... Ročni obseg Morda ni internetne povezave.
Morda je vstavek za strip pokvarjen.
Ali pa za današnji dan ali to številko/niz ni stripa. Poskusite izbrati drugo. Srednji klik na strip ga pokaže v njegovi izvirni velikosti Datoteka zip ne obstaja, preklicujem. Obseg: Prikaži puščice samo ob prehodu miške Pokaži URL stripa Pokaži avtorja stripa Pokaži določilnik stripa Pokaži naslov stripa Določilnik slike: Obseg slik stripa, ki naj se arhivirajo. Posodobi Obišči spletišče stripa Obišči &spletišče trgovine št. %1 dni dd.MM.yyyy &Naslednji zavihek z novo sliko Od: Do: minut slik na strip 