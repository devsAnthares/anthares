��    B      ,  Y   <      �  i   �  m        y  b   �     �     	  6        O  7   _     �     �  	   �     �  (   �  )   �  )   (     R     o  Y   u     �     �  �   �      y	  .   �	     �	  
   �	     �	     �	     
     
     !
     5
     B
     J
     c
     r
     w
     �
     �
     �
     �
     �
  	   �
  
   �
     �
     �
  	     
     
   *     5     B  	   `     j     o  
   �  �   �     (  8   8  �   q     �  8   	  ,   B  ,   o  %   �     �  �  �  Y   �  �        �  a   �          4  9   F     �  6   �     �     �     �  $   �  -     *   <  *   g     �     �  P   �     
       u   -     �  >   �     �     
          /     =     V     e     w  
   �  #   �     �     �     �     �     �          
       
   !     ,     8     F     `     o          �  &   �  
   �     �     �     �  �   	     �  :   �  r   �     a  6   p  '   �  '   �      �  !        $   !   %          ;   ,          B                 8   #                 7   9       +              )   ?   -   <         6                                    *   2       >                1       @       .   5   3   :   A         &      /                4              0      "             =   (   '         	   
           @info User changed Phonon backendTo apply the backend change you will have to log out and back in again. A list of Phonon Backends found on your system.  The order here determines the order Phonon will use them in. Apply Device List To... Apply the currently shown device preference list to the following other audio playback categories: Audio Hardware Setup Audio Playback Audio Playback Device Preference for the '%1' Category Audio Recording Audio Recording Device Preference for the '%1' Category Backend Colin Guthrie Connector Copyright 2006 Matthias Kretz Default Audio Playback Device Preference Default Audio Recording Device Preference Default Video Recording Device Preference Default/Unspecified Category Defer Defines the default ordering of devices which can be overridden by individual categories. Device Configuration Device Preference Devices found on your system, suitable for the selected category.  Choose the device that you wish to be used by the applications. EMAIL OF TRANSLATORSYour emails Failed to set the selected audio output device Front Center Front Left Front Left of Center Front Right Front Right of Center Hardware Independent Devices Input Levels Invalid KDE Audio Hardware Setup Matthias Kretz Mono NAME OF TRANSLATORSYour names Phonon Configuration Module Playback (%1) Prefer Profile Rear Center Rear Left Rear Right Recording (%1) Show advanced devices Side Left Side Right Sound Card Sound Device Speaker Placement and Testing Subwoofer Test Test the selected device Testing %1 The order determines the preference of the devices. If for some reason the first device cannot be used Phonon will try to use the second, and so on. Unknown Channel Use the currently shown device list for more categories. Various categories of media use cases.  For each category, you may choose what device you prefer to be used by the Phonon applications. Video Recording Video Recording Device Preference for the '%1' Category  Your backend may not support audio recording Your backend may not support video recording no preference for the selected device prefer the selected device Project-Id-Version: kcm_phonon
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2013-09-18 19:06+0200
Last-Translator: Andrej Mernik <andrejm@ubuntu.si>
Language-Team: Slovenian <lugos-slo@lugos.si>
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
X-Generator: Lokalize 1.5
 Da bodo spremembe zaledja stopile v veljavo, se boste morali odjaviti in znova prijaviti. Seznam najdenih zaledij za Phonon, ki so na vašem sistemu. Tukaj prikazan seznam določa, v kakšnem vrstnem redu jih bo Phonon uporabljal. Uveljavi seznam za ... Uveljavi trenutno prikazan seznam prednosti naprav še za naslednje kategorije predvajanja zvoka: Nastavitev zvočnih naprav Predvajanje zvoka Prednost naprav za predvajanje zvoka za kategorijo »%1« Snemanje zvoka Prednost naprav za snemanje zvoka za kategorijo »%1« Zaledje Colin Guthrie Povezovalnik Avtorske pravice 2006 Matthias Kretz Privzeta prednost naprav za predvajanje zvoka Privzeta prednost naprav za snemanje zvoka Privzeta prednost naprav za snemanje videa Privzeta/nedoločena kategorija Nižje Določa privzeti vrstni red naprav, ki ga posamezne kategorije lahko prepišejo. Nastavitev naprave Prednost naprav Naprave najdene na vašem sistemu, ki so primerne za izbrano kategorijo. Izberite napravo, ki jo želite uporabljati. email4marko@gmail.com Izbrane naprave za predvajanje zvoka ni bilo mogoče nastaviti Spredaj v sredini Spredaj levo Spredaj levo od sredine Spredaj desno Spredaj desno od sredine Strojna oprema Neodvisne naprave Vhodne vrednosti Neveljavno KDE-jeva nastavitev zvočnih naprav Matthias Kretz Mono Marko Burjek Nastavitveni modul Phonon Predvajanje (%1) Višje Profil Zadaj v sredini Zadaj levo Zadaj desno Snemanje (%1) Prikaži napredne naprave Ob strani levo Ob strani desno Zvočna kartica Zvočna naprava Postavitev in preizkušanje zvočnikov Nizkotonec Preizkus Preizkusi izbrano napravo Preizkušanje – %1 Vrstni red določa prednost naprav. Če iz kakršnegakoli razloga ne more biti uporabljena prva naprava, bo Phonon poskusil uporabiti drugo, in tako naprej. Neznan kanal Uporabi trenutno prikazan seznam naprav za več kategorij. Različne kategorije uporab predstavnosti. Za vsako kategorijo lahko izberete, katero napravo želite uporabljati. Snemanje videa Prednost naprav za snemanje videa za kategorijo »%1« Zaledje morda ne podpira snemanja zvoka Zaledje morda ne podpira snemanja videa znižaj prednost izbrani napravi povečaj prednost izbrani napravi 