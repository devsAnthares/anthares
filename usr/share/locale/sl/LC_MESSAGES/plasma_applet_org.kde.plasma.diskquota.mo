��    
      l      �       �   
   �      �        .   0     _     t     �  (   �  8   �  �       �  &   �     #  M   ?  #   �     �     �  &   �     �        
                             	        Disk Quota No quota restrictions found. Please install 'quota' Quota tool not found.

Please install 'quota'. Running quota failed e.g.: 12 GiB of 20 GiB%1 of %2 e.g.: 8 GiB free%1 free example: Quota: 83% usedQuota: %1% used usage of quota, e.g.: '/home/bla: 38% used'%1: %2% used Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2016-11-20 12:05+0100
Last-Translator: Andrej Mernik <andrejm@ubuntu.si>
Language-Team: Slovenian <lugos-slo@lugos.si>
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
X-Generator: Lokalize 1.5
 Količinska omejitev diska Ni bilo najdenih količinskih omejitev Namestite program »quota« Orodje za količinske omejitve ni bilo najdeno.

Namestite program »quota«. Zaganjanje programa quota ni uspelo %1 od %2 %1 neporabljeno Količinska omejitev: %1 % uporabljeno %1: %2 % uporabljeno 