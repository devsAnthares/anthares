��    F      L  a   |         
     
     
        "     1     5     I     X     ^  	   m     w  	        �     �     �  
   �     �     �  3   �  	   �     �               $     9     @     G     V     f     m  
        �  2   �  ?   �                    )     2     ?     N     S     a     r     �     �     �     �     �     �  	   �     �     �     �     �  b   �  �   [	     �	     �	  	   
     
     (
  
   8
  	   C
     M
     ]
     e
     k
     }
  �  �
  
   `     k     y     �     �     �     �     �     �     �     �     �             	     
   &  	   1     ;     A     C     K     ]     s     x     �  	   �     �     �     �     �     �     �  ;     Y   @     �     �  	   �     �     �     �     �     �     �                    )     7  
   =     H     T     a     j     }     �  ]   �  �   �     �     �     �     �     �     �     �     �                    2     +       E   C   4       &       D           
   <      *   9           !   F      /   #                A       @   =                     $   ;                  B             :       '           0         1   2                                -   (       6   ?         >              "   .   %   ,              )       8       3   5   	                  7    Activities Add Action Add Spacer Add Widgets... Alt Alternative Widgets Always Visible Apply Apply Settings Apply now Author: Auto Hide Back-Button Bottom Cancel Categories Center Close Concatenation sign for shortcuts, e.g. Ctrl+Shift+ Configure Configure activity Create activity... Ctrl Currently being used Delete Email: Forward-Button Get new widgets Height Horizontal-Scroll Input Here Keyboard shortcuts Layout cannot be changed whilst widgets are locked Layout changes must be applied before other changes can be made Layout: Left Left-Button License: Lock Widgets Maximize Panel Meta Middle-Button More Settings... Mouse Actions OK Panel Alignment Remove Panel Right Right-Button Screen Edge Search... Shift Stop activity Stopped activities: Switch The settings of the current module have changed. Do you want to apply the changes or discard them? This shortcut will activate the applet: it will give the keyboard focus to it, and if the applet has a popup (such as the start menu), the popup will be open. Top Undo uninstall Uninstall Uninstall widget Vertical-Scroll Visibility Wallpaper Wallpaper Type: Widgets Width Windows Can Cover Windows Go Below Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-09 03:10+0200
PO-Revision-Date: 2018-01-11 21:14+0100
Last-Translator: Andrej Mernik <andrejm@ubuntu.si>
Language-Team: Slovenian <lugos-slo@lugos.si>
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
X-Generator: Lokalize 2.0
 Dejavnosti Dodaj dejanje Dodaj praznino Dodaj gradnike ... Alt Drugotni gradniki Vedno viden Uveljavi Uveljavi nastavitve Uveljavi sedaj Avtor: Samodejno skrij Gumb za premik nazaj Spodaj Prekliči Kategorije V sredini Zapri + Nastavi Nastavi dejavnost Ustvari dejavnost ... Ctrl Je trenutno v uporabi Izbriši E-pošta: Gumb za premik naprej Dobi nove gradnike Višina Vodoravno drsenje Vnesite sem Tipkovne bližnjice Razporeda ni mogoče spremeniti, če so gradniki zaklenjeni Dokler ne bodo uveljavljene spremembe razporeditev ne bo mogoče izvajati drugih sprememb Razporeditev: Levo Levi gumb Licenca: Zakleni gradnike Razpni pult Meta Srednji gumb Več nastavitev ... Dejanja miške V redu Poravnava pulta Odstrani pult Desno Desni gumb Rob zaslona Poišči ... Dvigalka Zaustavi dejavnost Zaustavljene dejavnosti: Preklopi Nastavitve trenutnega modula so se spremenile. Ali želite spremembe uveljaviti ali zavreči? Ta bližnjica bo omogočila aplet: žarišče tipkovnice bo postavljeno na aplet in če ima ta pojavno okno (npr. začetni meni), se bo to okno odprlo. Zgoraj Razveljavi odstranitev Odstrani Odstrani gradnik Navpično drsenje Vidnost Slika ozadja Vrsta slike ozadja: Gradniki Širina Okna ga lahko prekrijejo Okna gredo pod njega 