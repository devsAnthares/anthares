��          �      ,      �     �     �     �     �     �     �     �     �  .   �     .     L     \     m     �     �  H   �  �  �     �     �  	   �     �     
          %  	   8  7   B  E   z  $   �     �     �  
          v   2        	                                                                  
    &Configure Printers... Active jobs only All jobs Completed jobs only Configure printer General No active jobs No jobs No printers have been configured or discovered One active job %1 active jobs One job %1 jobs Open print queue Print queue is empty Printers Search for a printer... There is one print job in the queue There are %1 print jobs in the queue Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2015-02-21 09:37+0000
PO-Revision-Date: 2015-04-04 10:59+0200
Last-Translator: Andrej Mernik <andrejm@ubuntu.si>
Language-Team: Slovenian <lugos-slo@lugos.si>
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
X-Generator: Lokalize 1.5
 Nasta&vi tiskalnike ... Samo dejavni posli Vsi posli Samo zaključeni posli Nastavi tiskalnik Splošno Ni dejavnih poslov Ni poslov Nastavljenega ali odkritega ni bilo nobenega tiskalnika %1 dejavnih poslov %1 dejaven posel %1 dejavna posla %1 dejavni posli %1 poslov %1 posel %1 posla %1 posli Odpri tiskalno vrsto Tiskalna vrsta je prazna Tiskalniki Najdi tiskalnik ... V vrsti je %1 tiskalnih poslov V vrsti je %1 tiskalni posel V vrsti sta %1 tiskalna posla V vrsti so %1 tiskalni posli 