��          4      L       `   #   a   �   �   �  L  "   �  �                       Standard Actions successfully saved The changes have been saved. Please note that:<ul><li>Applications need to be restarted to see the changes.</li>    <li>This change could introduce shortcut conflicts in some applications.</li></ul> Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2009-11-03 21:29+0100
Last-Translator: Axel Rousseau <axel@esperanto-jeunes.org>
Language-Team: Esperanto <kde-i18n-doc@kde.org>
Language: eo
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=2; plural=n != 1;
 Normalaj agoj suksese konserviĝis La ŝanĝoj konserviĝis. Bonvolu noti ke <ul><li>Aplikaĵoj bezonas restarti por montri ŝanĝojn</li><li>Tiuj ŝanĝoj eble konfliktiĝos kun klavkombinoj en aliaj aplikaĵoj.</li></ul> 