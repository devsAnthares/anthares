��          �            x  �   y  m   �  `   i     �     �     �     �           3     R     W     h  ?   u  Y   �  +     �  ;  |   �  \   f  o   �     3     <  	   R     \     z     �     �     �     �  8   �  Y     *   b        	                                                  
                     <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. Project-Id-Version: kcminput
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-01-02 22:58+0100
Last-Translator: Axel Rousseau <axel@esperanto-jeunes.org>
Language-Team: Esperanto <kde-i18n-doc@kde.org>
Language: eo
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=2; plural=n != 1;
 <qt>Ĉu vi vere volas forigi la <i>%1</i> kursoran etoson?<br /> Tio forigos ĉiujn dosierojn instalitajn de tiu etoso.</qt> <qt>Vi ne povas forigi la nun uzatan etoson.<br /> Vi devas unue ŝanĝi al alia etoso.</qt> Etoso kun la nomo %1 jam ekzistas en via piktogrametosa dosierujo. Ĉu vi volas anstataŭigi ĝin per ĉi tiun? Konfirmo Kursoragordo ŝanĝis Priskribo Ektiru aŭ tajpu etosan URLon pedrotpmx@wanadoo.fr Pierre-Marie Pédrot Nomo Anstataŭigi la etoson? Forigi Etoson La dosiero %1 ŝajne ne estas valida kursoretosa arkivo. Ne eblas elŝuti la kursoretosan arkivon. Bonvolu kontroli, ke la adreso %1 estas valida. Ne eblas trovi la kursoretosan arkivon %1. 