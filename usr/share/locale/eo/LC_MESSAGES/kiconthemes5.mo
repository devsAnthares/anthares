��          �      <      �  
   �     �  >   �            
        $     ,     4     ;  	   G     Q     _     f  2   u     �     �  �  �     _     k  E   t     �  
   �  
   �     �     �     �     �  
             "     '  7   ;     s     �                   
                                                                          	        &Browse... &Search: *.png *.xpm *.svg *.svgz|Icon Files (*.png *.xpm *.svg *.svgz) Actions Applications Categories Devices Emblems Emotes Icon Source Mimetypes O&ther icons: Places S&ystem icons: Search interactively for icon names (e.g. folder). Select Icon Status Project-Id-Version: kio4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:11+0100
PO-Revision-Date: 2008-05-14 13:25-0500
Last-Translator: Cindy McKee <cfmckee@gmail.com>
Language-Team: Esperanto <kde-i18n-doc@lists.kde.org>
Language: eo
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KAider 0.1
Plural-Forms: nplurals=2; plural=n != 1;
 &Foliumi... &Serĉi: *.png *.xpm *.svg *.svgz|piktogramdosieroj (*.png *.xpm *.svg *.svgz) Agoj Aplikaĵoj Kategorioj Aparatoj Emblemoj Miensimboloj Piktogramfonto MIME-tipoj Aliaj pik&togramoj: Ejoj S&istempiktogramoj: Interage serĉi nomojn de piktogramoj (ekz. dosierujo). Elekti piktogramon Stato 