��          �            x     y  
   �     �     �     �     �     �  	   �     �     �     �  (   �  '     "   D     g  �  p     #     ,     <     M  	   V  	   `  	   j     t     }     �     �  (   �  $   �  !   �  	               
                   	                                                Alphabetically By Desktop By Program Name Do Not Group Do Not Sort Filters General Grouping: Manually Maximum rows: On %1 Show only tasks from the current desktop Show only tasks from the current screen Show only tasks that are minimized Sorting: Project-Id-Version: plasma_applet_tasks
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2009-10-23 19:35+0300
Last-Translator: Cindy McKee <cfmckee@gmail.com>
Language-Team: Esperanto <kde-i18n-doc@lists.kde.org>
Language: eo
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KAider 0.1
Plural-Forms: nplurals=2; plural=n != 1;
 Alfabete Laŭ labortablo Laŭ programnomo Ne grupu Ne ordigu Filtriloj Ĝenerala Grupado: Permane Maksimumaj vicoj: Sur %1 Montri nur taskojn de la nuna labortablo Montri taskojn nur de la nuna ekrano Montri nur minimumigitajn taskojn Ordigado: 