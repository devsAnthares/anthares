��          �   %   �      `  &   a  �   �                    (     6     C     T      d  	   �  �   �  c   X     �     �     �     �               '     3     9  	   ?  C   I  j   �     �  �    *   �  q   �     O     V     b  
   v  	   �     �     �  C   �  	   �  �   �  X   �	     E
     [
  +   g
     �
     �
     �
     �
  	   �
  	   �
  
   �
  ?   �
  x   3     �                                                                         
                  	                                 (c) 2002 Karol Szwed, Daniel Molkentin <h1>Style</h1>This module allows you to modify the visual appearance of user interface elements, such as the widget style and effects. Button Checkbox Combobox Con&figure... Configure %1 Daniel Molkentin Description: %1 EMAIL OF TRANSLATORSYour emails Group Box Here you can choose from a list of predefined widget styles (e.g. the way buttons are drawn) which may or may not be combined with a theme (additional information like a marble texture or a gradient). If you enable this option, KDE Applications will show small icons alongside some important buttons. KDE Style Module Karol Szwed NAME OF TRANSLATORSYour names No description available. Preview Radio button Ralf Nolden Tab 1 Tab 2 Text Only There was an error loading the configuration dialog for this style. This area shows a preview of the currently selected style without having to apply it to the whole desktop. Unable to Load Dialog Project-Id-Version: kcmstyle
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-17 05:44+0100
PO-Revision-Date: 2007-12-18 10:07-0600
Last-Translator: Cindy McKee <cfmckee@gmail.com>
Language-Team: Esperanto <kde-i18n-eo@kde.org>
Language: eo
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=n != 1;
 (C) 2002 ĉe Karol Szwed, Daniel Molkentin <h1>Stilagordo</h1>Tiu modulo permesas ŝanĝi la aperon de la grafikaj elementoj, fenestraĵostiloj kaj efektoj. Butono Markobutono Redaktebla falmenuo &Agordi... Agordi %1 Daniel Molkentin Priskribo: %1 wolfram@steloj.de,Steffen.Pietsch@BerlinOnline.de,cfmckee@gmail.com Grupazono Tie ĉi vi povas elekti iun el listo de antaŭdifinitaj fenestraĵostiloj.Stiloj difinas ekz. kiel butonoj aspektas ktp. Stiloj povas esti kombinitaj kun etosoj, kiuj donas aldonajn aspekterojn, kiel marmoran teksturon aŭ kolortransirojn k.s. Se elektita, KDE-aplikaĵoj montras malgrandajn piktogramojn apud kelkaj gravaj butonoj. Agordmodulo por stilo Karol Szwed Wolfram Diestel,Steffen Pietsch,Cindy McKee Neniu priskribo troviĝis. Antaŭrigardo Radiobutono Ralf Nolden Langeto 1 Langeto 2 Nur teksto Eraro okazis dum ŝargo de la agorda dialogo por ĉi tiu stilo. Tiu areo montras antaŭrigardon de la elektita stilo. Tiel vi povas prijuĝi ĝin sen apliki ĝin al la tuta labortablo. Ne povis ŝargi dialogon 