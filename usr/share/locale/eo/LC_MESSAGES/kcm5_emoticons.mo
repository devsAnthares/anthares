��          �   %   �      0     1     I     V      ]     ~     �     �      �     �     �  	   �       )     5   ?     u     �     �     �     �  +   �  (     3   -  �   a  �  M     �       	     '   %     M     e     y     �     �  
   �     �     �  )   �  @        V     d     t     {      �  .   �  (   �  -   	  �   3	                                    
                                                    	                                       %1 theme already exists Add Emoticon Add... Could Not Install Emoticon Theme Create a new emoticon Delete emoticon Do you want to remove %1 too? EMAIL OF TRANSLATORSYour emails Edit Emoticon Edit... Emoticons Emoticons Manager Enter the name of the new emoticon theme: Install a theme archive file you already have locally NAME OF TRANSLATORSYour names New Emoticon Theme Remove Remove Theme Remove the selected emoticon Remove the selected emoticon from your disk Remove the selected theme from your disk This will remove the selected theme from your disk. You need to be connected to the Internet to use this action. A dialog will display a list of emoticon themes from the http://www.kde-look.org website. Clicking the Install button associated with a theme will install this theme locally. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-04 06:03+0100
PO-Revision-Date: 2009-11-08 19:46+0100
Last-Translator: Axel Rousseau <axel@esperanto-jeunes.org>
Language-Team: Esperanto <kde-i18n-doc@kde.org>
Language: eo
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=2; plural=n != 1;
  La etoso %1 jam ekzistas Aldoni Mienon Aldonu... Ne eblas instali la miensimbolan etoson Krei novan miensimbolon Forigi miensimbolon Ĉu vi vere volas forigi %1? axel@esperanto-jeunes.org Redakti Mienon Redaktu... Mienoj Miensimboloj administrilo Enigi nomon de la nova miensimbola etoso: Instali etos-arkivan dosieron kion vi jam havas en via komputilo Axel Rousseau Nova Mien-etoso Forigi Forigi la etoson Forigi la elektitan miensimbolon Forigi la elektitan miensimbolon el via disko. Forigu la elektitan etoson de via disko. Tio forigos la eletkitan etoson de via disko. Vi bezonas interretan konekton por fari tion. Dialogo montros liston de miensimbolaj etosoj el la retejo http://www.kde.org. Musklante sur la "instal-butonon" instalos loke la elektitan etoson. 