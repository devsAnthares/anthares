��    !      $  /   ,      �     �  /         0     9     M     _     u     �     �  	   �     �  	   �     �      �  *   �       	        $     D  
   Z     e     t     �     �  /   �     �     �  b     	   h  
   r  P   }     �  �  �     �  -   �     �     �     �               %     -     :     A     V     ^     f  	   x     �     �     �     �     �     �     �     �     �     	     	     2	  _   A	     �	  
   �	  S   �	     
     !       	                                                                                        
                                                           (c) 2009, Ben Cooksley <i>Contains 1 item</i> <i>Contains %1 items</i> About %1 About Active Module About Active View About System Settings Apply Settings Author Ben Cooksley Configure Configure your system Developer Dialog EMAIL OF TRANSLATORSYour emails General config for System SettingsGeneral Help Icon View Internal name for the view used Keyboard Shortcut: %1 Maintainer Mathias Soeken NAME OF TRANSLATORSYour names No views found Relaunch %1 Search through a list of control modulesSearch Show detailed tooltips System Settings The settings of the current module have changed.
Do you want to apply the changes or discard them? Tree View View Style Welcome to "System Settings", a central place to configure your computer system. Will Stephenson Project-Id-Version: systemsettings
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:08+0100
PO-Revision-Date: 2009-12-18 21:46+0100
Last-Translator: Axel Rousseau <axel@esperanto-jeunes.org>
Language-Team: Esperanto <kde-i18n-doc@kde.org>
Language: eo
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.0
 (c) 2009, Ben Cooksley <i>Enhavas 1 eron</i> <i>Enhavas %1 erojn</i> Pri %1 Pri aktiva modulo Pri aktiva rigardo Pri Sistema agordo Apliki agordon Aŭtoro Ben Cooksley Agordi Agordi vian sistemon Kreinto Dialogo cfmckee@gmail.com Ĝenerala Helpo Piktograma vido Interna nomo de la uzata vido Fulmoklavo: %1 Prizorganto Mathias Soeken Cindy McKee Neniu vido trovita Relanĉi %1 Serĉi Montri detalajn ŝpruchelpilojn Sistema agordo La agordoj de la aktuala modulo ŝanĝiĝis.
Ĉu vi volas apliki la ŝanĝojn aŭ forlasi ilin? Arboperspektivo Vida stilo Bonvenon en la "Sistemaj Agordoj", centra loko por agordi vian komputilan sistemon. Will Stephenson 