��    N      �  k   �      �     �     �     �     �  
   �  #   �     �            
   -     8      V     w  /   �     �     �     �  p   �     W     j     }     �     �     �     �     �  	   �  
   �     	     	     0	     @	     ]	  	   x	     �	     �	     �	     �	     �	     �	     
     5
     F
     U
     m
     �
  9   �
     �
     �
     �
     �
          *     @     _          �     �  :   �            #   6     Z     p     �     �     �     �     �          1     H     _     u    �  �   �  S   �  �  �     �     �  
   �     �     �  $   �     �     �                 U   ;     �  $   �     �     �     �  p   �     j     }     �     �  "   �     �     �               ,     8     N     _  !   u     �     �     �     �  ;   �  #   #  !   G  "   i      �     �     �     �     �       >        V     k     �     �     �     �      �                6     J  !   d  -   �     �     �     �               1     H     d  %   �  "   �     �     �     �       �   0  �     `   �                  ,          C      /   @           
   5   L   0       F   )       N          A             <   7   6       .                            +              1          #            8   J      '   3      (   9       E   $   I       B   %          :   4         M   	       K           2      "          &   *   ;   G   ?              D   !      =          -                  >          H    &All Desktops &Close &Fullscreen &Move &No Border Activate Window Demanding Attention Close Window Cristian Tibirna Daniel M. Duley Desktop %1 Disable configuration options EMAIL OF TRANSLATORSYour emails Hide Window Border Indicate that KWin has recently crashed n times KDE window manager KWin KWin helper utility KWin is unstable.
It seems to have crashed several times in a row.
You can select another window manager to run: Keep &Above Others Keep &Below Others Keep Window Above Others Keep Window Below Others Keep Window on All Desktops Kill Window Lower Window Luboš Luňák Ma&ximize Maintainer Make Window Fullscreen Matthias Ettrich Maximize Window Maximize Window Horizontally Maximize Window Vertically Mi&nimize Minimize Window Move Window NAME OF TRANSLATORSYour names Pack Grow Window Horizontally Pack Grow Window Vertically Pack Shrink Window Horizontally Pack Shrink Window Vertically Pack Window Down Pack Window Up Pack Window to the Left Pack Window to the Right Raise Window Replace already-running ICCCM2.0-compliant window manager Resize Window Setup Window Shortcut Shade Window Suspend Compositing Switch One Desktop Down Switch One Desktop Up Switch One Desktop to the Left Switch One Desktop to the Right Switch to Next Desktop Switch to Next Screen Switch to Previous Desktop This helper utility is not supposed to be called directly. Toggle Window Raise/Lower Walk Through Desktop List Walk Through Desktop List (Reverse) Walk Through Desktops Walk Through Desktops (Reverse) Walk Through Windows Walk Through Windows (Reverse) Window One Desktop Down Window One Desktop Up Window One Desktop to the Left Window One Desktop to the Right Window Operations Menu Window to Next Desktop Window to Next Screen Window to Previous Desktop You have selected to show a window in fullscreen mode.
If the application itself does not have an option to turn the fullscreen mode off you will not be able to disable it again using the mouse: use the window operations menu instead, activated using the %1 keyboard shortcut. You have selected to show a window without its border.
Without the border, you will not be able to enable the border again using the mouse: use the window operations menu instead, activated using the %1 keyboard shortcut. kwin: unable to claim manager selection, another wm running? (try using --replace)
 Project-Id-Version: kwin
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-15 03:16+0100
PO-Revision-Date: 2008-02-27 21:37+0100
Last-Translator: Pierre-Marie Pédrot <pedrotpmx@wanadoo.fr>
Language-Team: esperanto <kde-i18n-eo@kde.org>
Language: eo
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=n != 1;
 Ĉ&iuj labortabloj F&ermi &Tutekrano &Movi &Nenia bordo Aktivigi fenestron kiu petas atenton Fermi fenestron Cristian Tibirna Daniel M. Duley Labortablo %1 Malebligi agordajn elektojn wolfram@steloj.de,Steffen.Pietsch@BerlinOnline.de,matthias@peick.de,cfmckee@gmail.com Kaŝi fenestran bordon Indiki ke KWin jam kolapsis n fojojn KDEa fenestroadministrilo KWin KWin-helpilo KWin estas nestabila.
Ŝajne, ĝi kolapsis plurajn fojojn sinsekve.
Vi povas elekti alian fenestroadministrilon: Fiksi super &aliaj Fiksi su&b aliaj Fiksi fenestron super la aliaj Fiksi fenestron sub la aliaj Fiksi fenestron en ĉiu labortablo Mortigi fenestron Fonigi fenestron Luboš Luňák M&aksimumigi Prizorganto Tutekranigi fenestron Matthias Ettrich Maksimumigi fenestron Maksimumigi fenestron horizontale Maksimumigi fenestron vertikale Mi&nimumigi Minimumigi fenestron Movi fenestron Wolfram Diestel,Steffen Pietsch, Matthias Peick,Cindy McKee Horizontala maksimumigo de fenestro Vertikala maksimumigo de fenestro Horizontala minimumigo de fenestro Vertikala minimumigo de fenestro Malsuprenigi fenestron Suprenigi fenestron Maldekstrigi fenestron Dekstrigi fenestron Malfonigi fenestron Anstataŭigi rulantan ICCCM2.0-konforman fenestroadministrilon Regrandigi fenestron Agordi fenestran fulmoklavon Volvi fenestron Kunmetitaĵo paŭziĝas Iri al suba labortablo Iri al supra labortablo Iri al labortablo maldekstrapuda Iri al labortablo dekstrapuda Iri al sekva labortablo Iri al sekva ekrano Iri al antaŭa labortablo Tiu helpilo ne estu vokita rekte. Baskuli inter malfonigo/fonigo de la fenestro Iro tra la labortablolisto Reiro tra la labortablolisto Iro tra la labortabloj Reiro tra la labortabloj Iro tra la fenestroj Reiro tra la fenestroj Fenestro al suba labortablo Fenestro al supra labortablo Fenestro al labortablo maldekstrapuda Fenestro al labortablo dekstrapuda Fenestro-agmenuo Fenestro al sekva labortablo Fenestro al sekva ekrano Fenestro al antaŭa labortablo Vi elektis vidigi fenestron tutekrane.
Se la aplikaĵo ne havas eblon maltutekranigi, vi ne povas reveni al normala rigardo per la muzo. Uzu anstataŭe la fenestran menuon. Vi povas premi la klavkombinon: %1. Vi elektis vidigi senbordajn fenestrojn.
Senborde vi ne povas rebordigi per la muzo. Uzu anstataŭe la fenestran menuon. Vi povas premi la klavkombinon: %1. kwin: Ne eblas regi la administradon, ĉu alia fenestroadministrilo ruliĝas? (Provu --replace)
 