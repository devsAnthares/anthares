��    @        Y         �     �     �     �     �     �     �  	   �     �  	   �  
   �     �     �     �     �     �     �     �  
             &  	   >  
   H     S     Z     a     w     �     �      �     �     �  D   �  
     
   )     4     9     F     ]     b     g     v     {     �     �     �  
   �     �     �     �     �     �     �     �     �     �     �          
       #   +     O     S     _  �  g     
     
     
     
      
     )
  	   0
  	   :
  	   D
     N
     V
     _
     k
     o
     r
     w
     {
     �
     �
     �
  	   �
     �
     �
  
   �
     �
     �
          ,     3  	   ?     I  U   U  	   �     �  
   �     �  
   �     �     �     �                         !     0     8     @     P     X  
   `     k     �     �  	   �  	   �     �     �     �  /   �       
     	                     "       8      )   0   ,   %                        @      7          $   :       	             
   +   =   1                     ?                      !         #   >         <               *   .       2                     4                    &   /      3   (      9   -      ;             6   '   5         %  msec  px  ° &Color: &Height: &Opacity: &Radius: &Spacing: &Strength: &Width: (Un-)Minimize window -90 90 Advanced Alt Angle: Appearance Apply effect to &groups Apply effect to &panels Automatic Background Bottom Center Clear All Mouse Marks Clear Last Mouse Mark Clear Mouse Marks Custom Desktop name alignment:Disabled Dialogs: Disabled Draw with the mouse by holding Shift+Meta keys and moving the mouse. End effect Filter:
%1 Left Left button: Left mouse buttonLeft Less Meta Middle button: More Natural Opaque Out Pager Reflection Right Right button: Right mouse buttonRight Shift Shortcut Show Desktop Grid Size Sphere Tab 1 Tab 2 Text Text color: Toggle Invert Effect Toggle Thumbnail for Current Window Top Transparent Windows Project-Id-Version: kwin_effects
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-02 03:18+0100
PO-Revision-Date: 2008-01-10 09:34-0600
Last-Translator: Cindy McKee <cfmckee@gmail.com>
Language-Team: Esperanto <kde-i18n-eo@kde.org>
Language: eo
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=n != 1;
  %  msek  rm  ° &Koloro: &Alto: &Opakeco: &Radiuso: &Spacado: &Forto: &Larĝo: Horizontala -90 90 Plie Alt Angulo: Aspekto Apliki efekton al &grupoj Apliki efekton al &paneloj Aŭtomata Fono Malsupra Centrigita Viŝi ĉiujn musospurojn Viŝi la lastan musospuron Viŝi la musospurojn Propra Malŝaltita Dialogoj: Malŝaltita Vi povas skizi per la muso, nur premadu la Majusklan+Metan-klavojn kaj movu la muson. Vertikala Filtrilo:
%1 Maldekstra Maldekstra butono: Maldekstra Malpli Meta Meza butono: Pli Natura Netravidebla Elen Mesaĝricevilo Resendo Dekstra Dekstra butono: Dekstra Majuskl Klavosigno Videbligi la labortablan kradon Grando Sfero Langeto 1 Langeto 2 Teksto Teksta koloro: Baskuligi inversan efekton Baskuligi miniaturan rigardon por nuna fenestro Supra Travidebla Fenestroj 