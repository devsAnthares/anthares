��    :      �  O   �      �     �                    ,     3  �  N  �   )  -   �  )   �  -   	  +   ;	  r   g	  	   �	  	   �	     �	     
     
     
  
   $
     /
     ;
     C
     K
      b
     �
     �
     �
      �
     �
     �
  r   �
  5   ]     �     �     �  	   �     �     �     �  (   �                9     S     n     t  +   �  3   �     �     �     �     �  S     )   _     �  �   �  �  s     !     *     3     @     I     P  �  k  �     ,   �     �     �  	   �  ]   �     M     S     e     }     �     �     �  	   �  
   �     �     �     �     �          	  #        :     F  y   e  @   �           /     >  
   C     N     U     c  (   q     �  ,   �  /   �  .   	  	   8     B  1   Y  -   �     �  
   �     �     �  \   �  1   ?     q  �   ~               2                '   (          1      !            8                                       .               5   	   6   $      )       7   4          *                 0   
             :         ,                    +      %      "   -   /   #       9           &      3    &Amount: &Effect: &Second color: &Semi-transparent &Theme (c) 2000-2003 Geert Jansen <h1>Icons</h1>This module allows you to choose the icons for your desktop.<p>To choose an icon theme, click on its name and apply your choice by pressing the "Apply" button below. If you do not want to apply your choice you can press the "Reset" button to discard your changes.</p><p>By pressing the "Install Theme File..." button you can install your new icon theme by writing its location in the box or browsing to the location. Press the "OK" button to finish the installation.</p><p>The "Remove Theme" button will only be activated if you select a theme that you installed using this module. You are not able to remove globally installed themes here.</p><p>You can also specify effects that should be applied to the icons.</p> <qt>Are you sure you want to remove the <strong>%1</strong> icon theme?<br /><br />This will delete the files installed by this theme.</qt> <qt>Installing <strong>%1</strong> theme</qt> @label The icon rendered as activeActive @label The icon rendered as disabledDisabled @label The icon rendered by defaultDefault A problem occurred during the installation process; however, most of the themes in the archive have been installed Ad&vanced All Icons Antonio Larrosa Jimenez Co&lor: Colorize Confirmation Desaturate Description Desktop Dialogs Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Effect Parameters Gamma Geert Jansen Get new themes from the Internet Icons Icons Control Panel Module If you already have a theme archive locally, this button will unpack it and make it available for KDE applications Install a theme archive file you already have locally Main Toolbar NAME OF TRANSLATORSYour names Name No Effect Panel Preview Remove Theme Remove the selected theme from your disk Set Effect... Setup Active Icon Effect Setup Default Icon Effect Setup Disabled Icon Effect Size: Small Icons The file is not a valid icon theme archive. This will remove the selected theme from your disk. To Gray To Monochrome Toolbar Torsten Rahn Unable to download the icon theme archive;
please check that address %1 is correct. Unable to find the icon theme archive %1. Use of Icon You need to be connected to the Internet to use this action. A dialog will display a list of themes from the http://www.kde.org website. Clicking the Install button associated with a theme will install this theme locally. Project-Id-Version: kcmicons
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-08 06:27+0100
PO-Revision-Date: 2009-10-21 09:51+0100
Last-Translator: Axel Rousseau <axel@esperanto-jeunes.org>
Language-Team: esperanto <kde-i18n-eo@kde.org>
Language: eo
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=n != 1;
 &Kvanto: &Efekto: &Dua koloro: &Diafana &Etoso (c) 2000-2003 Geert Jansen <h1>Piktogramoj</h1> La modulo eblas al vi elekti piktogramojn por via labortablo. <p>Por elekti piktogrametoson, alklaku ĝian nomon kaj apliku ĝin per la malsupra butono. Se vi ne volas apliki vian elekton, vi povas premi la butonon "Rezigni" por forĵeti la ŝanĝojn.</p> <p>Premante la "Instali novajn etosojn" vi povas instali vian novan piktogrametoson per skribo ĝian lokon en la enigo aŭ uzu la serĉilon. Premu la butonon "Konfirmi" por finigi la instalon.</p> <p>"Forigi etoson" nur estas uzebla, se vi elektas etoson instalita de vi. Vi ne povas forigi sistemajn etosojn ĉi tie.</p> <p>Vi ankaŭ povas difini efektojn, kiuj estas aplikataj al la piktogramoj.</p> <qt>Ĉu vi estas certa, ke vi volas forigi la <strong>%1</strong> piktogrametoson?<br /><br />Tio forigos la instalitajn dosierojn de tiu etoso.</qt> <qt>Instalas <strong>%1</strong> etoson</qt> Aktiva Neaktiva Defaŭlta Problemo okazis dum la instalado. Sed la plejmulto de la etosoj en la arkivo estas instalitaj &Plie Ĉiuj piktogramoj Antonio Larrosa Jimenez Ko&loro: Kolorigi Konfirmo Malsaturigi Priskribo Labortablo Dialogoj Enmetu aŭ donu URL de etoso heiko@evermann.de Efektaj parametroj Gamo Geert Jansen Elŝuti novajn etosojn el Interreto Piktogramoj Panelmodulo por piktogramstiro Se vi jam havas en via komputilo la etos-arkivon, tiu butono malpakos ĝin kaj disponebligos ĝin por viaj KDE aplikaĵoj Instali etos-arkivan dosieron kion vi jam havas en via komputilo Ĉefa ilobreto Heiko Evermann Nomo Sen efekto Panelo Antaŭrigardo Forigi etoson Forigu la elektitan etoson de via disko. Agordi efekton... Agordi la efekton por la aktivaj piktogramoj Agordi la efekton por la defaŭltaj piktogramoj Agordi la efekton por la neaktivaj piktogramoj Grandeco: Malgrandaj piktogramoj La dosiero ne estas valida piktogrametosa arkivo. Tio forigos la eletkitan etoson de via disko. Grizigi Dukolorigi Ilobreto Torsten Rahn Ne eblas ricevi la piktogrametosan arkivon.
Bonvole kontrolu la ĝustecon de la adreso "%1". Ne eblas trovi la piktogrametosan arkivon por %1. Piktogramuzo Vi bezonas interret-konekton por uzi tiun agon. Dialog-mesaĝo montros liston de etoso el la retejo http://www.kde.org. Klikante sur la Instal-butono ligita al iu etoso instalos tiun etoson en via komputilo. 