��          �            h     i     m     o     �     �     �  M   �       	        #     <     K     X     x  �  �     D     H     J  !   b     �     �  S   �     �     
          1     ?     R     ^                   
                                      	                        ms % &Reactivation delay: &Switch desktop on edge: Activation &delay: Always Enabled Change desktop when the mouse cursor is pushed against the edge of the screen Lock Screen No Action Only When Moving Windows Other Settings Show Desktop Switch desktop on edgeDisabled Window Management Project-Id-Version: kcmkwinscreenedges
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-30 03:13+0100
PO-Revision-Date: 2009-10-22 10:38+0100
Last-Translator: Axel Rousseau <axel@esperanto-jeunes.org>
Language-Team: ESPERANTO <kde-i18n-eo@kde.org>
Language: eo
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-Language: Esperanto
Plural-Forms: nplurals=2; plural=n != 1;
  ms % Daŭro de &Re-aktivado: Ŝanĝi labortablon ĉe la rando: Daŭro de Aktivado: Ĉiam Ebligita Ŝanĝi labortablon kiam la musmontrilo estas premi kontraŭ la rando  de la ekrano Ŝlosi ekranon Neniu Agado Nur kiam moviĝas Fenestro Aliaj Agordoj Montri Labortablon Malebligita Fenestradministrilo 