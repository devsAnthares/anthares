��          �   %   �      P     Q     b     w     �     �     �  
   �     �     �     �     �     �  /        3     Q     p     v     �     �     �     �     �     �     �       �  %     �  /   �       ;   4     p  3   �     �     �  
   �     �     �  !     d   4  I   �  B   �  
   &  &   1  &   X       A   �  A   �  F   	  F   a	  7   �	  8   �	                            
                                                                                             	    %1 file %1 files %1 folder %1 folders %1 of %2 processed %1 of %2 processed at %3/s %1 processed %1 processed at %2/s Appearance Behavior Cancel Clear Configure... Finished Jobs List of running file transfers/jobs (kuiserver) Move them to a different list Move them to a different list. Pause Remove them Remove them. Resume Show all jobs in a list Show all jobs in a list. Show all jobs in a tree Show all jobs in a tree. Show separate windows Show separate windows. Project-Id-Version: kuiserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2011-11-12 17:55+0200
Last-Translator: Yasen Pramatarov <yasen@lindeas.com>
Language-Team: Bulgarian <dict@fsa-bg.org>
Language: bg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.2
 %1 файл %1 файла %1 директория %1 директории %1 от %2 обработени %1 от %2 обработени при %3 в секунда %1 обработени %1 обработени при %2 в секунда Изглед Поведение Отказ Изчистване Настройки... Приключили задачи Списък с работещите файлови изтегляния и задачи (kuiserver) Преместване на записа в различен списък Преместване на записа в друг списък. Пауза Премахване на записа Премахване на записа Продължение Показване на всички задачи в списък Показване на всички задачи в списък Показване на всички задачи дървовидно Показване на всички задачи дървовидно Показване на отделни прозорци Показване на отделни прозорци. 