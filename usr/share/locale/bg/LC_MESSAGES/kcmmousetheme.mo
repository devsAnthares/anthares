��          �      \      �     �  �   �  m   r  `   �     A     N     f     s           �     �  '   �     �               %  ?   2  Y   r  +   �  �  �     �  )  �  �   �  `   ~     �  H   �      A	     b	  *   s	     �	     �	  R   �	     
     1
     8
      X
  l   y
  �   �
  g   �                                                             	                               
       (c) 2003-2007 Fredrik Höglund <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Cursor Theme Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Fredrik Höglund Get new color schemes from the Internet NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. Project-Id-Version: kcminput
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2011-07-12 21:29+0300
Last-Translator: Yasen Pramatarov <yasen@lindeas.com>
Language-Team: Bulgarian <dict@fsa-bg.org>
Language: bg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=2; plural=n != 1;
 (c) 2003-2007 Fredrik Höglund <qt>Сигурни ли сте, че искате темата с показалци на мишката <i>%1</i> да бъде изтрита?<br />При тази операция ще бъдат изтрити всичките файлове инсталирани от тази тема.</qt> <qt>Не можете да изтриете текущо използваната тема.<br />Първо превключете на друга.</qt> Вече има тема с име "%1". Искате ли да бъде презаписана? Потвърждение Настройките на показалеца са променени Изглед на курсора Описание Местоположение на тема radnev@yahoo.com Fredrik Höglund Получаване на нови цветови схеми от Интернет Радостин Раднев Име Презапис на тема? Изтриване на тема Файлът "%1" не е валиден архив на тема с показалци на мишката. Не може да бъде изтеглена темата с показалци на мишката. Моля, проверете дали адресът "%1" е правилен. Не може да бъде намерена темата с показалци на мишката %1. 