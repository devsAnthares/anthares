��    T      �  q   \         #   !     E     ^     `  J   b     �     �     �     �     �                      	   (     2     O     l     {     �     �     �      �     	      #	     D	     J	  '   [	     �	  !   �	     �	     �	     �	  
   �	     �	  	   

     
     $
     ,
     4
     S
     a
     n
     v
     �
     �
     �
     �
     �
     �
     �
     �
          !     ?     G     Z     g  	   w     �     �     �  0   �  3   �          %     9     M     k     �     �     �     �     �          /     M     _     t     �     �     �  !   �  �  �  <   �     �     �     �  `   �     T     n  /   �     �     �     �  	             )     :     L  (   d     �  9   �  7   �  ;     -   U  2   �  0   �  '   �            K   ;  5   �  C   �       #     3   C     w     �     �     �     �     �  5   �     !     ?     ]     k     �  
   �     �  $   �  ;   �  )   )  "   S  .   v     �  P   �  
          "   <     _      |     �     �      �  C   �  A   9     {     }          �  
   �     �      �     �     �  
             #  
   4     ?     N     a     r     y     �                   :          %                  <   L   F   9   A   2      -       1       4       *      	       +              R   >       7      
   =       .      B          ,         G             #       5            "   )   I   T   @   P   &       H                 ?   $               !   N   S   6   K   (            O   3   Q          J   C   E      8           ;      /      '               0      M      D        &Enter a name for the color scheme: (c) 2007 Matthew Woehlke 0 1 A color scheme with that name already exists.
Do you want to overwrite it? Active Text Active Titlebar Active Titlebar Text Alternate Background Button Background Button Text Color: Colors Contrast Contrast: Current color schemeCurrent Default color schemeDefault Disabled color Disabled color effect amount Disabled color effect type Disabled contrast amount Disabled contrast type Disabled intensity effect amount Disabled intensity effect type EMAIL OF TRANSLATORSYour emails Error Focus Decoration Get new color schemes from the Internet Import Color Scheme Import a color scheme from a file Inactive Text Inactive Titlebar Inactive Titlebar Text Intensity: Jeremy Whiting Link Text Matthew Woehlke Maximum Minimum NAME OF TRANSLATORSYour names Negative Text Neutral Text New Row Normal Background Normal Text Options Positive Text Remove Scheme Remove the selected scheme Save Color Scheme Selection Background Selection Inactive Text Selection Text Shade sorted column &in lists Shading Tooltip Background Tooltip Text View Background View Text Visited Text Window Background Window Text You do not have permission to delete that scheme You do not have permission to overwrite that scheme color-kcm-preview! color-kcm-preview+ color-kcm-preview= color-kcm-previewNormal text color-kcm-previewPush Button color-kcm-previewSelected text color-kcm-previewWindow text color-kcm-previewlink color-kcm-previewvisited color-kcm-set-previewfocus color-kcm-set-previewlink color-kcm-set-previewvisited color-setsButton color-setsSelection color-setsTooltip color-setsWindow no disabled color effectNone no disabled contrastNone no disabled intensity effectNone Project-Id-Version: kcmcolors
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-08 06:27+0100
PO-Revision-Date: 2010-06-22 21:33+0300
Last-Translator: Yasen Pramatarov <yasen@lindeas.com>
Language-Team: Bulgarian <dict@fsa-bg.org>
Language: bg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=n != 1;
 &Въведете име на цветовата схема: (c) 2007 Matthew Woehlke 0 1 Вече има такава схема.
Искате ли да бъде презаписана? Активен текст Активно заглавие Текст на активно заглавие Алтернативен фон Фон на бутон Текст на бутон Цвят: Цветове Контраст Контраст: Текуща схема Стандартна схема на KDE Изключен цвят Сила на ефекта за изключен цвят Вид на ефекта за изключен цвят Наситеност на изключен контраст Вид на изключен контраст Сила на ефекта за изключено Вид на ефекта за изключено radnev@yahoo.com,zlatkopopov@fsa-bg.org Грешка Украса на фокуса Импортиране на цветова схема от Интернет Импортиране на цветова схема Импортиране на цветова схема от файл Неактивен текст Неактивно заглавие Текст на неактивно заглавие Интензитет: Jeremy Whiting Хипервръзка Matthew Woehlke Максимално Минимално Радостин Раднев,Златко Попов Негативен текст Неутрален текст Нов ред Нормален фон Нормален текст Опции Позитивен текст Премахване на схема Премахване на маркираната схема Запис на цветова схема Фон на маркираното Маркиран неактивен текст Маркиран текст Открояване на сортираната колона &в списъка Сянка Фон на подсказка Текст на подсказка Преглед на фона Преглед на текста Посетен текст Фон на прозорец Текст на прозорец Нямате права за изтриване на схемата Нямате права за презапис на схемата ! + = Нормален текст Бутон Избран текст Текст на прозорец Препратка посетена фокус препратка посетена Бутон Избрано Подсказка Прозорец Без Без Без 