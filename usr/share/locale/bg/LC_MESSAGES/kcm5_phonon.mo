��    ,      |  ;   �      �     �     �     �     �               1     F      X     y  
   �     �     �     �     �     �     �     �                    >     Z     h     p  	   |  
   �     �  	   �  
   �  
   �     �     �  	   �     �     �  
             .  ,   >  ,   k  %   �     �  �  �  7     -   �     �     �     	     	  0   3	  ;   d	     �	      �	     �	  4   �	     "
  6   >
     u
  )   �
     �
  >   �
                 .   9  #   h     �     �     �     �     �     �          8  #   R  J   v     �  
   �  A   �          :  .   V  I   �  K   �  L     E   h        &   %                            "             *   
                                $                          	           #                    ,   (                       '   !   )          +          Audio Hardware Setup Audio Playback Audio Recording Backend Colin Guthrie Copyright 2006 Matthias Kretz Device Configuration Device Preference EMAIL OF TRANSLATORSYour emails Front Center Front Left Front Left of Center Front Right Front Right of Center Hardware Independent Devices Input Levels KDE Audio Hardware Setup Matthias Kretz Mono NAME OF TRANSLATORSYour names Phonon Configuration Module Playback (%1) Profile Rear Center Rear Left Rear Right Recording (%1) Side Left Side Right Sound Card Sound Device Speaker Placement and Testing Subwoofer Test Test the selected device Testing %1 Unknown Channel Video Recording Your backend may not support audio recording Your backend may not support video recording no preference for the selected device prefer the selected device Project-Id-Version: kcm_phonon
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2011-11-12 21:26+0200
Last-Translator: Yasen Pramatarov <yasen@lindeas.com>
Language-Team: Bulgarian <dict@fsa-bg.org>
Language: bg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=2; plural=n != 1;
 Настройки на звуковия хардуер Възпроизвеждане на звук( Запис на звук Ядро Colin Guthrie Copyright 2006 Matthias Kretz Настройки на устройството Предпочитан ред на устройствата yasen@lindeas.com Отпред от центъра Отпред отляво Отпред отляво или от центъра Отпред отдясно Отпред отдясно или от центъра Хардуер Независими устройства Входни нива Настройки на звуковия хардуер в KDE Matthias Kretz Моно Ясен Праматаров Модул за настройка на Phonon Възпроизвеждане (%1) Профил Отзад от центъра Отзад отляво Отзад отдясно Запис (%1) Отстрани отляво Отстрани отдясно Звукова карта Звуково устройство Разположение на говорителя и изпробване Субуфер Проба Изпробване на избраното устройство Изпробване на %1 Непознат канал Възпроизвеждане на образ Ядрото може да не поддържа запис на звук Ядрото може да не поддържа запис на видео без предпочитане на избраното устройство предпочитане на избраното устройство 