��    L      |  e   �      p  '   q  f   �                (  7   9  9   q  y   �     %     7     >  M   Q     �     �     �      �     �     	     	  
   #	      .	  
   O	     Z	     g	  	   p	     z	     ~	     �	  	   �	     �	     �	     �	     �	     �	     �	     �	     �	     

     )
     .
     B
     T
     W
     r
     �
     �
     �
     �
  <   �
     �
             
   &     1     @     L     _     f     {  !   �  F   �  ?   �  0   =     n     s     �     �     �     �     �     �     �     �     �  	   �  �  �  O   �  &   �  !        ;  *   U  7   �  
   �  �  �     _     {     �  �   �  =   B     �  7   �  ]   �  U   %  -   {     �     �     �     �     �  *   �  +   &     R  <   V     �     �     �  '   �     �            3   -  
   a  4   l     �     �     �     �     �  L   �  4   C  "   x     �     �     �  �   �     Z  4   k     �  -   �  G   �     6     H     g  )   z  D   �  C   �  �   -  e      ^   f     �     �     �       /        E      d  $   �     �     �  &   �     �         &   1                   %   8   F      .         )   
   +               A   ?       I                 @          	       J   B   5   (   4      -   D      =          H                    2   9           !   >      ;      :   '      7                 E   /   $   6   K       ,   3          #   *                        0               <      C             G   L   "    %1 BPP, Depth: %2, Scanline padding: %3 %1 is one of the modules of the kinfocenter, cpu info, os info, etcNo information available about %1. %1 x %2 Pixels (%3 x %4 mm) %1 x %2 dpi (Default Screen) (c) 2008 Nicolas Ternisien
(c) 1998 - 2002 Helge Deller @title:column Column name for PCI informationInformation All the information modules return information about a certain aspect of your computer hardware or your operating system. Available Screens Bitmap Black %1, White %2 Could not find any programs with which to query your system's PCI information Current Input Event Mask DMA-Channel Default Colormap Default Number of Colormap Cells Depth of Root Window Depths (%1) Description Dimensions EMAIL OF TRANSLATORSYour emails Event = %1 Helge Deller I/O-Port I/O-Range IRQ Image Byte Order Information Interrupt LSBFirst Largest Cursor Location MSBFirst Manufacturer Maximum Request Size Model Motion Buffer Size NAME OF TRANSLATORSYour names Name Name of the Display Nicolas Ternisien No No I/O port devices found. No PCI devices found. Number of Colormaps Options Order PA-RISC Revision PCI subsystem could not be queried: %1 could not be executed Padding Pixmap Format #%1 Preallocated Pixels Resolution Root Window ID Screen # %1 Server Information Status Supported Extensions Supported Pixmap Formats System Information Control Module The PCI subsystem could not be queried, this may need root privileges. This list displays system information on the selected category. This system may not be completely supported yet. Unit Unknown Order %1 Used By Value Vendor Release Number Vendor String Version Number When mapped X-Server Yes minimum %1, maximum %2 unlimited Project-Id-Version: kcminfo
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-09 06:04+0100
PO-Revision-Date: 2010-12-05 15:30+0200
Last-Translator: Yasen Pramatarov <yasen@lindeas.com>
Language-Team: Bulgarian <dict@fsa-bg.org>
Language: bg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=2; plural=n != 1;
 %1 бита/пиксел, качество: %2 бита, запълване: %3 Няма информация за %1. %1 x %2 точки (%3 x %4 мм) %1 x %2 точки/инч (Екран по подразбиране) (c) 2008 Nicolas Ternisien
(c) 1998 - 2002 Helge Deller Данни Този модул предоставя информация за хардуера на вашия компютър и някои параметри на операционната система. В зависимост от архитектурата на компютъра и инсталираната операционна система някои модули може да не са достъпни. Налични екрани Побитова карта Черно %1, бяло %2 Не може да бъде намерена програма, която да предоставя информация за шината PCI. Текуща маска на събитията за вход Канал DMA Цветова карта по подразбиране Брой цветове по подразбиране в клетките на картата Качество на цветовете на родителския прозорец Качество на цветовете (%1) Описание Размери radnev@yahoo.com Събитие = %1 Helge Deller Входно-изходни портове Диапазон на вход-изхода IRQ Ред на байтовете в изображението Информация Прекъсване LSBFirst Максимален показалец Местоположение MSBFirst Производител Максимален размер на заявка Модел Размер на буфера за движение Радостин Раднев Име Име на екрана Nicolas Ternisien Не Не са намерени входно-изходни устройства. Не са намерени устройства PCI. Брой цветови карти Настройки Ред Версия на PA-RISC Няма информация за подсистемата PCI. Програмата "%1" не може да бъде изпълнена. Подложка Формат на пикселна карта № %1 Запазени пиксели Разделителна способност Идентификатор на родителския прозорец Екран № %1 Данни за сървъра Състояние Поддържани разширения Поддържани формати на пикселни карти Контролен модул с данни за системата Няма информация за подсистемата PCI. Програмата, която доставя информация, изисква права на системен администратор. Този списък показва информация за избраната категория. Тази система може все още да не се поддържа напълно. Единица Непознат ред %1 Използване Стойност Сериен номер от търговеца Име на търговеца Номер на версията Когато е изобразено Сървър X Да минимум %1, максимум %2 неограничено 