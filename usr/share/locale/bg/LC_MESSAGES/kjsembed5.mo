��    "      ,  /   <      �  Z   �  *   T          �     �     �     �     �  9        <     D      ^          �     �     �     �                2  !   E     g     �     �     �     �     �               4  (   J     s  >   �  �  �     i  B   q     �  $   �  $   �  9   	     Q	  ?   ^	     �	     
  E   7
  R   }
  <   �
  1     >   ?  I   ~  9   �  '     <   *  )   g  C   �  1   �        0   (  7   Y      �  -   �  .   �       1   )  3   [     �  G   �                         "                             	                          
                                                                   !           %1 is 'the slot asked for foo arguments', %2 is 'but there are only bar available'%1, %2. %1 is not a function and cannot be called. %1 is not an Object type '%1' is not a valid QLayout. '%1' is not a valid QWidget. Action takes 2 args. Alert Call to '%1' failed. Call to method '%1' failed, unable to get argument %2: %3 Confirm Could not construct value Could not create temporary file. Could not open file '%1' Could not open file '%1': %2 Could not read file '%1' Failed to create Action. Failed to create Layout. Failed to create Widget. Failed to load file '%1' File %1 not found. First argument must be a QObject. Incorrect number of arguments. Must supply a filename. Must supply a layout name. Must supply a valid parent. Must supply a widget name. No classname specified No classname specified. No such method '%1'. Not enough arguments. There was an error reading the file '%1' Wrong object type. but there is only %1 available but there are only %1 available Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2013-07-23 01:52+0300
Last-Translator: Yasen Pramatarov <yasen@lindeas.com>
Language-Team: Bulgarian <dict@ludost.net>
Language: bg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 %1, %2. %1 не е функция и не може да се извика. %1 не е тип Object "%1" не е валиден QLayout. "%1" не е валиден QWidget. Действието изисква 2 аргумента. Аларма Комуникацията с "%1" беше неуспешна. Комуникацията с метод "%1" беше неуспешна. Не беше намерен аргумент %2: %3 Потвърждение Стойността не може да бъде определена Създаването на временен файл беше неуспешно. Файлът "%1" не може да бъде отворен Грешка при отваряне на "%1":%2 Файлът "%1" не може да бъде прочетен Създаването на действие беше неуспешно. Грешка при създаване на изглед. Грешка при създаване. Файлът "%1" не може да бъде зареден Файлът "%1" не е намерен. Първият аргумент трябва да бъде QObject. Неправилен брой аргументи. Трябва да има име. Трябва да представите име. Трябва да има валиден родител. Трябва да има име. Не е посочено име на клас Не е посочено име на клас. Няма метод "%1". Няма достатъчно аргументи. Грешка при четене на файл "%1" Неправилен тип. но е налично само %1 но са налични само %1 