��    *      l  ;   �      �  A   �     �  /        2     ;     O     a     w     �     �  	   �     �  3   �  	   �     �      �  $      *   E     p  	   u  5        �     �  
   �     �          $  5   3  6   i     �  ,   �  /   �     	        O   0  Z   �  b   �  	   >  
   H  P   S     �  �  �  ]   _
     �
  P   �
     %  *   7  ,   b  6   �  .   �  
   �             3      Z   T     �     �     �  J   �     0  
   9      D  h   e  @   �  !        1     D     T  '   r  q   �  r           c   �       ;     #   O  �   s  �     �   �  #   f     �  �   �     T            )             &      $   	   %         '                                                !      
       *                     "                                                 (                 #    %1 is an external application and has been automatically launched (c) 2009, Ben Cooksley <i>Contains 1 item</i> <i>Contains %1 items</i> About %1 About Active Module About Active View About System Settings Apply Settings Author Ben Cooksley Configure Configure your system Determines whether detailed tooltips should be used Developer Dialog EMAIL OF TRANSLATORSYour emails Expand the first level automatically General config for System SettingsGeneral Help Icon View Internal module representation, internal module model Internal name for the view used Keyboard Shortcut: %1 Maintainer Mathias Soeken NAME OF TRANSLATORSYour names No views found Provides a categorized icons view of control modules. Provides a classic tree-based view of control modules. Relaunch %1 Reset all current changes to previous values Search through a list of control modulesSearch Show detailed tooltips System Settings System Settings was unable to find any views, and hence has nothing to display. System Settings was unable to find any views, and hence nothing is available to configure. The settings of the current module have changed.
Do you want to apply the changes or discard them? Tree View View Style Welcome to "System Settings", a central place to configure your computer system. Will Stephenson Project-Id-Version: systemsettings
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:08+0100
PO-Revision-Date: 2009-12-28 11:58+0200
Last-Translator: Yasen Pramatarov <yasen@lindeas.com>
Language-Team: Bulgarian <dict@fsa-bg.org>
Language: bg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: KBabel 1.11.4
 %1 е външна програма и беше автоматично презаредена (c) 2009, Ben Cooksley <i>Съдържа 1 елемент</i> <i>Съдържа %1 елемента</i> Относно %1 Относно активния модул Относно активния изглед Относно системните настройки Прилагане на настройките Автор Ben Cooksley Настройка Настройване на системата ви Определя дали да се използват подробни подсказки Разработчик Диалог yasen@lindeas.com Автоматично разширяване на първото ниво Общи Помощ Изглед като икони Представяне на вътрешни модули, модел на вътрешни модули Вътрешно име на използвания изглед Съкратен клавиш: %1 Поддръжка  Mathias Soeken Ясен Праматаров Не са открити изгледи Осигурява категоризиран изглед с икони на контролните модули Осигурява класически дървовиден изглед на контролните модули Презареждане на %1 Връщане на всички промени към предишните им стойности Търсене Показване на подробни подсказки Системни настройки Програмата за системни настройки не открива зададени изгледи и затова не показва нищо. Програмата за системни настройки не открива зададени изгледи и затова нищо не е налично за настройване. Настройките на текущия модул са променени.
Да се приложат ли или да се отхвърлят? Дървовиден преглед Стил на изгледа Добре дошли в "Системни настройки", централно място за настройване на компютърната ви система! Will Stephenson 