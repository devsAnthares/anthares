��    8      �  O   �      �     �     �     �     
          +     9     H     X     g     w     �     �     �     �     �     �     �     �     
          ,     @     Q     e     x     �     �     �     �     �     �     �                 6     W     w     �     �     �  #   �  "        0  $   P     u     �     �     �     �     
	     #	  &   <	  !   c	  !   �	  �  �	  &   {  
   �     �     �  
   �  
   �  &   �          #     4     I      e     �     �     �     �     �     �               4     N     e  "   t  .   �     �     �  
   �     �  
   �       +     4   B  #   w  -   �     �  C   �  #   -  0   Q  #   �  2   �  7   �  !     1   3  6   e  #   �     �  -   �  4     0   A  
   r     }     �     �     �     ,                                          #         1               %                                	      "                        6   7   3             0      !      &   '      /   $   +      8   -   *                      .                
   4   5           (   2       )        @labelAlbum Artist @labelArchive @labelArtist @labelAspect Ratio @labelAudio @labelAuthor @labelBitrate @labelChannels @labelComment @labelComposer @labelCopyright @labelCreation Date @labelDocument @labelDuration @labelFrame Rate @labelHeight @labelImage @labelKeywords @labelLanguage @labelLyricist @labelPage Count @labelPresentation @labelPublisher @labelRelease Year @labelSample Rate @labelSpreadsheet @labelSubject @labelText @labelTitle @labelVideo @labelWidth @label EXIFImage Date Time @label EXIFImage Make @label EXIFImage Model @label EXIFImage Orientation @label EXIFPhoto Aperture Value @label EXIFPhoto Exposure Bias @label EXIFPhoto Exposure Time @label EXIFPhoto F Number @label EXIFPhoto Flash @label EXIFPhoto Focal Length @label EXIFPhoto Focal Length 35mm @label EXIFPhoto ISO Speed Rating @label EXIFPhoto Metering Mode @label EXIFPhoto Original Date Time @label EXIFPhoto Saturation @label EXIFPhoto Sharpness @label EXIFPhoto White Balance @label EXIFPhoto X Dimension @label EXIFPhoto Y Dimension @label music albumAlbum @label music genreGenre @label music track numberTrack Number @label number of linesLine Count @label number of wordsWord Count Project-Id-Version: kfilemetadata
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:36+0200
PO-Revision-Date: 2014-05-17 22:19+0200
Last-Translator: Svetoslav Stefanov <svetlisashkov@yahoo.com>
Language-Team: BULGARIAN <kde-i18n-doc@kde.org>
Language: bg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 1.5.4
X-Poedit-SourceCharset: UTF-8
 Изпълнител на албума Архив Изпълнител Съотношение Аудио Автор Скорост на предаване Канали Коментар Композитор Авторско право Дата на създаване Документ Времетраене Кадрова честота Височина Изображение Ключови думи Език Текст на песента Брой страници Презентация Издател Година на издаване Честота на дискретизация Таблица Тема Текст Заглавие Видео Широчина Изображение - дата и час Изображение - създадено с(ъс) Изображение - модел Изображение - ориентация Снимка - апертура Снимка - компенсация на експозицията Снимка - експозиция Снимка - диафрагмено число Снимка - светкавица Снимка - фокусно разстояние Снимка - 35mm фокусно разстояние Снимка - скорост ISO Снимка - режим на измерване Снимка - оригинални дата и час Снимка - наситеност Снимка - острота Снимка - баланс на бялото Снимка - хоризонтален размер Снимка - вертикален размер Албум Жанр Номер на песен Брой редове Брой думи 