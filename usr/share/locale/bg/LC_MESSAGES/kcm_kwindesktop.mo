��             +         �     �     �  
   O  +   Z  
   �     �     �     �      �     �     �       	          *  *   K  H   v     �     �     �  )   �  ;     	   X  (   b     �     �     �     �     �          $  	   ?  �  I  	   �  �   �     �  L   �     !	     <	  7   X	  "   �	  \   �	  ;   
  =   L
     �
     �
  (   �
  V   �
  c   H     �  5   �     �  L     �   T     �  v   �  7   o  7   �  7   �  9     :   Q  ;   �  ;   �                                                                                                   	                                   
                msec <h1>Multiple Desktops</h1>In this module, you can configure how many virtual desktops you want and how these should be labeled. Animation: Assigned global Shortcut "%1" to Desktop %2 Desktop %1 Desktop %1: Desktop Effect Animation Desktop Names Desktop Switch On-Screen Display Desktop Switching Desktop navigation wraps around Desktops Duration: EMAIL OF TRANSLATORSYour emails Here you can enter the name for desktop %1 Here you can set how many virtual desktops you want on your KDE desktop. Layout NAME OF TRANSLATORSYour names No Animation No suitable Shortcut for Desktop %1 found Shortcut conflict: Could not set Shortcut %1 for Desktop %2 Shortcuts Show shortcuts for all possible desktops Switch One Desktop Down Switch One Desktop Up Switch One Desktop to the Left Switch One Desktop to the Right Switch to Desktop %1 Switch to Next Desktop Switch to Previous Desktop Switching Project-Id-Version: kcm_kwindesktop
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-06 05:35+0100
PO-Revision-Date: 2010-06-26 14:14+0300
Last-Translator: Yasen Pramatarov <yasen@lindeas.com>
Language-Team: Bulgarian <dict@fsa-bg.org>
Language: bg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=n != 1;
  мсек <h1>Много работни плотове</h1>В този модул можете да настроите броя и имената на виртуалните работни плотове. Анимация: Добавен е бърз клавиш "%1" за работен плот %2 Работен плот %1 Работен плот %1: Анимация на работните плотове Имена на плотовете Показване на екрана на превключвател на плотовете Превключване на работни плотове Кръгова навигация през плотовете Работни плотове Продължителност: yasen@lindeas.com,zlatkopopov@fsa-bg.org Тук можете да въведете името на работния плот %1 Тук можете да настроите броя на работните плотове в KDE. Изглед Ясен Праматаров,Златко Попов Без анимация Не е открит подходящ бърз клавиш за плот %1 Конфликт на клавиш — грешка при задаване на бърз клавиш %1 за работен плот %2 Бързи клавиши Показване на бързите клавиши за всички възможни работни плотове Превклъчвяне към плота отдолу Превклъчвяне към плота отгоре Превключване към плота отляво Превключване към плота отдясно Превключване към работен плот %1 Превключване към следващия плот Превключване към предишния плот Превключване 