��          T      �       �      �      �      �   .   �        H   +  �  t  (   ]     �  .   �  G   �  0     {   H                                        Active jobs only All jobs Completed jobs only No printers have been configured or discovered Print queue is empty There is one print job in the queue There are %1 print jobs in the queue Project-Id-Version: plasma_applet_org.kde.printmanager
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2015-02-21 09:37+0000
PO-Revision-Date: 2014-05-18 23:43+0200
Last-Translator: Svetoslav Stefanov <svetlisashkov@yahoo.com>
Language-Team: BULGARIAN <kde-i18n-doc@kde.org>
Language: bg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 1.5.4
X-Poedit-SourceCharset: UTF-8
 Само активните задачи Всички задачи Само приключилите задачи Не бяха настроени или открити принтери Опашката за печат е празна Има една задача в опашката за печат Има %1 задачи в опашката за печат 