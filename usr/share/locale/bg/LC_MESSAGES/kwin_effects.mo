��    �      d  �   �      �     �     �     �     �     �     �     �  	   �     �  	   �  
   �          
       5     %   G     m     �  
   �     �     �     �     �  	   �     �  
   �  	   �  
                  &     2     ?     K     X     _     h     m     t     �     �      �     �     �     �     �          !     (  "   8     [     x     �     �     �  
   �  
   �     �     �     �     �     �     
               $     1     D     Q     V     [     h     m     s     �     �     �     �     �     �  	   �  	   �     �     �  
   �     
          "     '  	   -     7     ?     G     N     R     X     ^     k  
   x  
   �     �     �     �     �     �     �     �     �     �     �     	          #     (     /     G     N     T     Z     _     k  
   w     �     �     �  	   �     �  	   �     �     �     �     �      �  
             $  �  )     �  	   �     �     �     �  
   �     	          9     H  
   [     f     y     }     �     �     �  *   �     �  +   �     *     C  	   G     Q  9   b     �     �     �     �     �     �               -     C     P     e     j  &   �  .   �  (   �       $        <  )   O  A   y  I   �               1     O     m  3   �     �     �  )   �                1  #   R  %   v  '   �     �     �  $   �      	  7   *  $   b     �     �     �     �     �  '   �     �          
     #  !   0  #   R  #   v  #   �  <   �  >   �  %   :  #   `  -   �     �     �     �     �     �       
   0     ;     J      Y     z  %   �     �     �     �  
          #   )  *   M     x     ~  %   �  <   �  1   �  "   )     L  
   Y  +   d  
   �     �     �  
   �  )   �     �        +   0      \      e      y      �      �   #   �   ,   �      
!     !!  8   8!     q!     }!  
   �!     <   X              B   ]       d   R         \   N       �   �   +           L   S   O         4   D   u   g       l   3   h      /       �   .                  0   $   s           -       8   K              E   _   Q                         
      G       >   j       (   %                  �   &      r   J   I   	   x   q   a   5   H   �   }      [   #   �                                 1   7       9   C   �   v   ;   i   !   ~   w           n   6           {      "   ^               b   p   @   Z   A      '   V       t   P   k             2          c                 U   F       Y                   f              o   �   M   ,      z   W   T   )          `          :      �   *   m              |   =   ?   �   e   y     %  msec  pixel  pixels  px  ° &Color: &Height: &Opacity: &Radius: &Spacing: &Strength: &Width: -90 90 @title:group actions when clicking on desktopDesktop @title:tab Advanced SettingsAdvanced @title:tab Basic SettingsBasic Activate window Activation Additional Options Advanced Alt Angle: Animation Animation duration: Appearance Automatic Background Background color: Bottom Bottom Left Bottom Right Bottom-Left Bottom-Right Center Centered Ctrl Custom Desktop Cube Desktop Cylinder Desktop Sphere Desktop name alignment:Disabled Dialogs: Disabled Display desktop name Display for moving windows Display for resizing windows Dra&g: Dropdown menus: Duration of flip animationDefault Duration of rotationDefault Duration of zoomDefault Enable Focus Tracking Far Faster Fill &gaps Filter:
%1 Flexible Grid Follow Focus Front color Glide Angle: Glide Effect: Hide In Inactive windows: Inside Graph Invert cursor keys Invert mouse Keep Left Left button: Less Light Maximum &width: Menus: Meta Middle button: More Mouse Pointer: Mouse Tracking: Move Down Move Left Move Mouse to Center Move Mouse to Focus Move Right Move Up Moving windows: Near Nicer No action Nowhere Opacity Opaque Out Pager Plane Popup menus: Proportional Rear color Reflection Reflections Regular Grid Right Right button: Rotation duration: Scale window Shift Shortcut Show &panels Show Desktop Grid Show desktop Show outline Size Sphere Strength of the effect: Strong Tab 1 Tab 2 Text Text alpha: Text color: Text font: Text position: Top Top Left Top Right Top-Left Top-Right Torn-off menus: Track mouse Translucency Transparent Visibility of the mouse-pointer. Wallpaper: Windows Zoom Project-Id-Version: kwin_effects
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-02 03:18+0100
PO-Revision-Date: 2011-07-12 20:30+0300
Last-Translator: Yasen Pramatarov <yasen@lindeas.com>
Language-Team: Bulgarian <dict@fsa-bg.org>
Language: bg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.2
  %  мсек  точка  точки  px  ° &Цвят: &Височина: &Непрозрачност: &Радиус: &Интервал: &Сила: &Широчина: -90 90 Работен плот Допълнителни Основни Активиране на прозорец Включване Допълнителни настройки Допълнителни Alt Ъгъл: Анимация Продължителност на анимацията: Външен вид Автоматично Фон Цвят на фона: Долу Долу вляво Долу вдясно Долу вляво Долу вдясно Център Центрирано Ctrl Потребителски Кубичен работен плот Цилиндричен работен плот Сферичен работен плот Изключено Диалогови прозорци: Изключено Показване име на плота Показване при премествани прозорци Показване при преоразмерявани прозорци Вла&чене: Падащи менюта: По подразбиране По подразбиране По подразбиране Включване следене на фокуса Далече По-бързо &Запълване на празнини Филтър:
%1 Гъвкава мрежа Следене на фокуса Цвят на задния план Ъгъл на приплъзване: Ефект на приплъзване: Скриване Навътре Неактивни прозорци: Вътре в графиката Обръщане на клавишите-стрелки Обръщане на мишката Запазване Ляво Ляв бутон: По-малко Слаб Максимална &широчина: Менюта: Meta Среден бутон: Повече Курсор на мишката: Следене на мишката: Преместване надолу Преместване наляво Преместване на мишката в центъра Преместване на мишката във фокуса Преместване надясно Преместване нагоре Преместващи се прозорци: Близко По-красиво Няма действие Никъде Непрозрачност Непрозрачност Навън Пейджър Равнина Изскачащи менюта: Пропорционално Цвят на предния план Отразяване Отразяване Обикновена мрежа Дясно Десен бутон: Време за завъртане: Мащабиране на прозорец Shift Бърз клавиш Показване на па&нели Показване мрежа на работния плот Показване на работния плот Показване на рамка Размер Сфера Интензивност на ефекта: Силен Страница 1 Страница 2 Текст Прозрачност на текста: Цвят на текста: Шрифт за текста: Разположение на текста: Горе Горе вляво Горе вдясно Горе вляво Горе вдясно Отделящи се менюта: Проследяване на мишката Прозрачност Прозрачност Видимост на курсора на мишката Тапет: Прозорци Мащаб 