��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  �  K     �  X      �  Y  �  
  �   �     �     �     �  Z        g  $   n  X   �  >   �                                          
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2006-12-03 08:33+0200
Last-Translator: Zlatko Popov <zlatkopopov@fsa-bg.org>
Language-Team: Bulgarian <dict@linux.zonebg.com>
Language: bg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=n != 1;
  сек П&родължителност на индикацията при стартиране: <h1>Уведомяване чрез системния панел</h1>
Това е друг начин да бъдете информирани за това, че дадена програма се стартира. Върху системния панел се появява бутон с пясъчен часовник в него. Възможно е да има програми, които не спазват изискванията и часовникът да продължи да се върти неопределено време. В такъв случай, се използва настройката "Продължителност на индикацията при стартиране". <h1>Зает курсор</h1>
Това е начин да бъдете информирани за това, че се стартира дадена програма. Показалецът на мишката ще се промени по начина, определен от вас от списъка. Възможно е да има програми, които не спазват изискванията и показалецът на мишката ще бъде във форма на зает курсор неопределено време. В такъв случай, се използва настройката "Продължителност на индикацията при стартиране". <h1>Обратна връзка</h1> От тук може да определите дали и как да бъдете уведомявани за стартирането на дадена програма. Мигащ курсор Анимиран курсор &Зает курсор Вкл&ючване на уведомяването чрез системния панел Без Пасивен зает курсор Про&дължителност на индикацията при стартиране: &Уведомяване чрез системния панел 