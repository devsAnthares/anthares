��          �            h  &   i  +   �     �  	   �     �     �     �     �     �  (        7  ,   N     {     �  �  �  ^   @  i   �     	  #     3   6  
   j     u     �  &   �  ^   �  (     X   G     �  2   �                                                                  	   
           Do you want to suspend to RAM (sleep)? Do you want to suspend to disk (hibernate)? General Hibernate Hibernate (suspend to disk) Leave Leave... Lock Lock the screen Logout, turn off or restart the computer Sleep (suspend to RAM) Start a parallel session as a different user Suspend Switch user Project-Id-Version: plasma_applet_lockout
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2011-07-09 12:36+0300
Last-Translator: Yasen Pramatarov <yasen@lindeas.com>
Language-Team: Bulgarian <dict@fsa-bg.org>
Language: bg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=2; plural=n != 1;
 Наистина ли искате да приспите компютъра в паметта? Наистина ли искате да приспите дълбоко компютъра в диска? Общи Дълбоко приспиване Дълбоко приспиване (в диска) Изход Изход... Заключване Заключване на екрана Излизане, изключване или рестартиране на компютъра Приспиване (в паметта) Зареждане на паралелна сесия на друг потребител Приспиване Превключване на потребител 