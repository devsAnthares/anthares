��          T      �       �      �      �   '   �   5     D   :       �  �     L  J   Q  U   �  m   �     `  G   i                                         MiB Enable low disk space warning Is the free space notification enabled. Minimum free space before user starts being notified. The settings dialog main page name, as in 'general settings'General Warn when free space is below: Project-Id-Version: freespacenotifier
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-06-26 23:39+0300
Last-Translator: Yasen Pramatarov <yasen@lindeas.com>
Language-Team: Bulgarian <dict@fsa-bg.org>
Language: bg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=n != 1;
  MiB Предупреждение при малко свободно място Дали е включено уведомяване за свободно място. Минимално свободно място преди уведомяване на потребителя. Общи Предупреждение при свободно място под: 