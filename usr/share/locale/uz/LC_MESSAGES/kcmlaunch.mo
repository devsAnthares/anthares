��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  �  K     �       0  #  /  T  o   �	     �	     
      
  +   -
     Y
     h
     {
     �
                                          
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2005-10-11 21:21+0200
Last-Translator: Mashrab Kuvatov <kmashrab@uni-bremen.de>
Language-Team: Uzbek <floss-uz-l10n@googlegroups.com>
Language: uz
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.10.2
Plural-Forms: nplurals=1; plural=0;
  soniya &Ishga tushirish uchun taymaut: <H1>Vazifalar panelida xabarnoma</H1>Bu usulda vazifalar panelida dastur ishga tushayotgani haqida xabar beruvchi harakatlanuvchi qum soati koʻrinadi. Baʼzi dasturlarda bunday imkoniyat yoʻq boʻlishi mumkin bu holda, xabarnoma "Ishga tushirish uchun taymaut"da koʻrsatilgan vaqtdan keyin yoʻqoladi. <H1>Band kursor</H1>Bu usulda sichqonchaning yonida dastur ishga tushayotgani haqida xabar beruvchi dasturning harakatlanuvchi nishonchasi koʻrinadi. Baʼzi dasturlarda bunday imkoniyat yoʻq boʻlishi mumkin bu holda, xabarnoma "Ishga tushirish uchun taymaut"da koʻrsatilgan vaqtdan keyin yoʻqoladi. <h1>Dastur ishga tushish xabarnomasi</h1>Bu yerda dastur ishga tushish haqida xabarnoma usulini moslash mumkin. Oʻchib-yonadigan kursor Sakraydigan kursor &Band kursor Vazifalar panelida &xabarnomani koʻrsatish Band kursorsiz Passiv band kursor Ishga tushirish uchun &taymaut: &Vazifalar panelida xabarnoma 