��          �      �       H     I  /   N     ~     �     �     �     �     �     �  "   �       6   0  *   g  �  �     A     F     ]     x  6   |  )   �     �     �     �  ,   �  &   (  C   O  -   �           	                                 
                      to  A non-proportional font (i.e. typewriter font). Ad&just All Fonts... BGR Click to change all fonts Configure Anti-Alias Settings Configure... E&xclude range: RGB Used by menu bars and popup menus. Used by the window titlebar. Used for normal text (e.g. button labels, list items). Used to display text beside toolbar icons. Project-Id-Version: kcmfonts
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-24 05:54+0100
PO-Revision-Date: 2006-12-17 12:09+0000
Last-Translator: Mashrab Kuvatov <kmashrab@uni-bremen.de>
Language-Team: Uzbek <floss-uz-l10n@googlegroups.com>
Language: uz
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=1; plural=0;
  to  Mutanosib emas shrift. Hamma sh&riftlarni moslash BGR Hamma shriftlarni oʻzgartirish uchun shu yerni bosing Shriftlarni tekislash moslamalari moslash Moslash Qoʻl&lanilmasin: RGB Menyu va kontekst menyusi uchun ishlatiladi. Oynaning sarlavhasi uchun ishlatiladi. Oddiy matn (m-n tugma yozuvi, roʻyxat bandlari) uchun ishlatiladi. Nishonchalar yonidagi matn uchun ishlatiladi. 