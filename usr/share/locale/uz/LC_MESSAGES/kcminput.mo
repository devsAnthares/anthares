��          �   %   �      @     A  
   G     R     g  /   p  '   �      �  �   �     �     �  9   �     (     1  E   >     �      �     �     �     �     �     �               ,  �  :     �     �     �       '     *   @  !   k  �   �     n     �  2   �     �     �  D   �  !   )	     K	     b	     o	     �	  
   �	     �	     �	     �	     �	                                                                
                                                   	            msec  pixel/sec &Acceleration delay: &General &Move pointer with keyboard (using the num pad) &Single-click to open files and folders (c) 1997 - 2005 Mouse developers <h1>Mouse</h1> This module allows you to choose various options for the way in which your pointing device works. Your pointing device may be a mouse, trackball, or some other hardware that performs a similar function. Acceleration &profile: Acceleration &time: Activates and opens a file or folder with a single click. Advanced Button Order Dou&ble-click to open files and folders (select icons on first click) Double click interval: EMAIL OF TRANSLATORSYour emails Icons Le&ft handed Ma&ximum speed: Mouse NAME OF TRANSLATORSYour names Pointer acceleration: R&epeat interval: Righ&t handed Project-Id-Version: kcminput
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-05 03:17+0100
PO-Revision-Date: 2005-10-19 21:17+0200
Last-Translator: Mashrab Kuvatov <kmashrab@uni-bremen.de>
Language-Team: Uzbek <floss-uz-l10n@googlegroups.com>
Language: uz
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.10.2
Plural-Forms: nplurals=1; plural=0;
  ms  piksel/sek &Tezlanish kechikishi: &Umumiy &Sichqonchani tugmatag bilan boshqarish &Bir marta bosish fayl va jildlarni ochadi (C) 1997-2005, Mouse tuzuvchilari <h1>Sichqoncha</h1> Bu modul yordamida koʻrsatgich uskunasining turli parametrlarini tanlashingiz mumkin. Koʻrsatgich uskunasi sichqoncha, trekbol yoki shunga oʻxshgan ammallarni bajaruvchi boshqa uskuna boʻlishi mumkin. Tezlanish &profili: Tezlanish &vaqti: Bir marta bosib fayl yoki jildni tanlash va opish. Qoʻshimcha Tugmalar tartibi &Ikki marta bosish fayl va jildlarni ochadi (bir marta - belgilaydi) Ikki marta bosish orasidagi vaqt: kmashrab@uni-bremen.de Nishonchalar Cha&p qoʻl uchun &Eng katta tezligi: Sichqoncha Mashrab Quvatov Kursorning tezlanishi: &Qaytarish vaqti: Oʻ&ng qoʻl uchun 