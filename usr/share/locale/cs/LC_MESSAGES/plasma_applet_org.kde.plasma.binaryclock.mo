��    	      d      �       �   
   �      �      �   	              "     C  "   ]  �  �     M     T     [     l     �  (   �  %   �  *   �                             	                 Appearance Colors: Display seconds Draw grid Show inactive LEDs: Use custom color for active LEDs Use custom color for grid Use custom color for inactive LEDs Project-Id-Version: plasma_applet_binaryclock
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-31 05:47+0100
PO-Revision-Date: 2017-08-10 15:48+0100
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
 Vzhled Barvy: Zobrazit sekundy Nakreslit mřížku Zobrazit neaktivní LED: Použít vlastní barvu pro aktivní LED Použít vlastní barvu pro mřížku Použít vlastní barvu pro neaktivní LED 