��    
      l      �       �   P   �   )   B  %   l  @   �     �  V   �     @     [     m  �       3  P   :  [   �  P   �      8  �   Y  )   �                      	               
                  %1 is '%1 packages to update' and %2 is 'of which %1 is security updates'%1, %2 1 package to update %1 packages to update 1 security update %1 security updates First part of '%1, %2'1 package to update %1 packages to update No packages to update Second part of '%1, %2'of which 1 is security update of which %1 are security updates Security updates available System up to date Updates available Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-15 03:15+0100
PO-Revision-Date: 2015-11-05 08:57+0100
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
 %1, %2 1 balíček k aktualizaci %1 balíčky k aktualizaci %1 balíčků k aktualizaci 1 bezpečnostní aktualizace %1 bezpečnostní aktualizace %1 bezpečnostních aktualizací 1 balíček k aktualizaci %1 balíčky k aktualizaci %1 balíčků k aktualizaci Žádný balíček k aktualizaci z nichž je 1 bezpečnostní aktualizace z nichž jsou %1 bezpečnostní aktualizace z nichž je %1 bezpečnostních aktualizací Jsou dostupné bezpečnostní aktualizace Systém je aktuální Jsou dostupné aktualizace 