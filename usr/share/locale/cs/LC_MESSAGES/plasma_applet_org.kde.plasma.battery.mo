��    "      ,  /   <      �  9   �     3     I     W     r  7   �  	   �     �     �     �     �          %     3     ;     O     V     m  #   z  %   �     �     �  �   �  P   �  o     5   ~     �     �     �     �  g   �  )   [  ?   �  �  �     �	     �	     �	  "   �	     
      
  	   1
     ;
  !   G
     i
     u
     �
     �
     �
     �
     �
  !   �
     �
     �
     �
           $  �   6  0   �  3   ,  +   `     �     �     �  	   �  e   �     #     '                                                                                       	   
                                           "                     !    %1 is remaining time, %2 is percentage%1 Remaining (%2%) %1% Battery Remaining %1%. Charging &Configure Power Saving... Battery and Brightness Battery is currently not present in the bayNot present Capacity: Charging Configure Power Saving... Discharging Display Brightness Enable Power Management Fully Charged General Keyboard Brightness Model: No Batteries Available Not Charging Placeholder is battery capacity%1% Placeholder is battery percentage%1% Power management is disabled Show percentage Some Application and n others are currently suppressing PM%2 and %1 other application are currently suppressing power management. %2 and %1 other applications are currently suppressing power management. Some Application is suppressing PM%1 is currently suppressing power management. Some Application is suppressing PM: Reason provided by the app%1 is currently suppressing power management: %2 The battery applet has enabled system-wide inhibition Time To Empty: Time To Full: Used for measurement100% Vendor: Your notebook is configured not to suspend when closing the lid while an external monitor is connected. battery percentage below battery icon%1% short symbol to signal there is no battery curently available- Project-Id-Version: plasma_applet_battery
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-03 03:15+0100
PO-Revision-Date: 2017-02-22 14:19+0100
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
X-Language: cs_CZ
X-Source-Language: en_US
 %1 zbývá (%2%) %1% baterie zbývá %1%. Nabíjí se N&astavte šetření napájení... Baterie a Jas Není přítomna Kapacita: Nabíjí se Nastavte šetření napájení... Vybíjí se Jas obrazovky Povolit správu napájení Plně nabitá Obecné Jas klávesnice Model: Nejsou dostupné žádné baterie Nenabíjí se %1% %1% Správce napájení je zakázán Ukazovat procenta %2 a %1 další aplikace právě potlačuje správu napájení. %2 a %1 další aplikace právě potlačují správu napájení. %2 a %1 dalších aplikací právě potlačují správu napájení. %1 v současnosti potlačuje správu napájení. %1 v současnosti potlačuje správu napájení: %2 Aplet baterie povolil pozastavení systému Čas do vybití: Čas do nabití: 100% Výrobce: Váš notebook je nastaven tak, aby neusínal při zavření víka a připojení externího monitoru. %1% - 