��          �      <      �     �     �     �  +   �  @        L     a     i     w     }     �  
   �     �     �     �     �     �  �  
     �     �     �  /   �  E   #      i     �     �     �     �     �     �     �     �     �          .     
         	                                                                                       <a href='%1'>%1</a> Close Copy Automatically: Don't show this dialog, copy automatically. Drop text or an image onto me to upload it to an online service. Error during upload. General History Size: Paste Please wait Please, try again. Sending... Share Shares for '%1' Successfully uploaded The URL was just shared Upload %1 to an online service Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-01 04:04+0100
PO-Revision-Date: 2017-02-24 11:42+0100
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
 <a href='%1'>%1</a> Zavřít Kopírovat automaticky: Nezobrazovat dialog ale kopírovat automaticky. Upusťte na mě text nebo obrázky pro odeslání na webovou službu. Nastala chyba při odesílání. Obecné Velikost historie: Vložit Čekejte prosím Prosím, zkuste znovu. Odesílám... Sdílet Sdílení pro '%1' Úspěšně odesláno URL byla právě sdílena Odeslat %1 do služby online 