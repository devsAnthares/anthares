��    w      �  �   �      
     
  	   &
     0
     =
     K
     Q
     X
     i
     o
     w
     
     �
     �
     �
     �
  	   �
     �
     �
  
   �
     �
     �
       
             !     (  	   7     A  
   G  
   R     ]     i     x     ~     �     �     �     �  ;   �  -   �  #        ;     T     l  
   �     �     �  
   �     �     �     �               7     K     P  	   k      u     �     �     �  
   �     �     �               .     E     V     f     v     �     �  
   �     �  )   �     �          #     2     A     R     X     `     s  '   �     �     �     �     �     �     
           .  C   <     �  s   �           %     :     Q     f     �     �      �     �  -   �  /        4  	   =  !   G     i      �     �     �     �     �     �  �  �     �     �     
          6  	   =     G     Y     e     m     z  
   �     �     �     �     �     �     �     	          /     D     K     b     p     y     �  	   �     �     �     �     �     �     �               &     .     >     N  !   [     }     �     �     �  
   �     �     �     �     �           :     Y     y     �     �     �     �     �  	   �                7     K      k     �     �     �     �  !   �          +     ;     X     j  %        �     �     �     �                     )  %   F  &   l     �     �  #   �     �     �               ;  W   V     �  W   �     !  '   ?  %   g  '   �     �     �      �  &   �     #  '   0  0   X     �  	   �     �     �     �     �                         o   5   l   W   	             1               R   6          J         >   u   d           V          M   T       X   r   v          G   .   Q   \       a              E   -       O               ^          b   7           +   Y   C      [       4                 H       k   B   c       ]   I   *   P   (   h   w   ;   N   3   
                @      "   S      _                   L   $      9   F      D          m   U   %       K       &                     t   Z   `   !       ,   0   g   f   p          i      2   e       n   /      q       ?      <   A   #   8          =   '   s   )               :   j           %1 &Handbook &About %1 &Actual Size &Add Bookmark &Back &Close &Configure %1... &Copy &Delete &Donate &Edit Bookmarks... &Find... &First Page &Fit to Page &Forward &Go To... &Go to Line... &Go to Page... &Last Page &Mail... &Move to Trash &New &Next Page &Open... &Paste &Previous Page &Print... &Quit &Redisplay &Rename... &Replace... &Report Bug... &Save &Save Settings &Spelling... &Undo &Up &Zoom... @action:button Goes to next tip, opposite to previous&Next @action:button Goes to previous tip&Previous @option:check&Show tips on startup @titleDid you know...?
 @title:windowConfigure @title:windowTip of the Day About &KDE C&lear Check spelling in document Clear List Close document Configure &Notifications... Configure S&hortcuts... Configure Tool&bars... Copy selection to clipboard Create new document Cu&t Cut selection to clipboard Dese&lect EMAIL OF TRANSLATORSYour emails Encodings menuAutodetect Encodings menuDefault F&ull Screen Mode Find &Next Find Pre&vious Fit to Page &Height Fit to Page &Width Go back in document Go forward in document Go to first page Go to last page Go to next page Go to previous page Go up NAME OF TRANSLATORSYour names No Entries Open &Recent Open a document which was recently opened Open an existing document Paste clipboard content Print Previe&w Print document Quit application Re&do Re&vert Redisplay document Redo last undone action Revert unsaved changes made to document Save &As... Save document Save document under a new name Select &All Select zoom level Send document by mail Show &Menubar Show &Toolbar Show Menubar<p>Shows the menubar again after it has been hidden</p> Show St&atusbar Show Statusbar<p>Shows the statusbar, which is the bar at the bottom of the window used for status information.</p> Show a print preview of document Show or hide menubar Show or hide statusbar Show or hide toolbar Switch Application &Language... Tip of the &Day Undo last action View document at its actual size What's &This? You are not allowed to save the configuration You will be asked to authenticate before saving Zoom &In Zoom &Out Zoom to fit page height in window Zoom to fit page in window Zoom to fit page width in window go back&Back go forward&Forward home page&Home show help&Help without name Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-16 03:10+0100
PO-Revision-Date: 2017-09-12 14:22+0100
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: en_US
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
X-Language: cs_CZ
X-Source-Language: en_US
 &Příručka aplikace '%1' O &aplikaci '%1' S&kutečná velikost &Přidat k záložkám &Zpět &Zavřít N&astavit '%1'... &Kopírovat &Smazat &Přispějte Upr&avit záložky... &Najít... P&rvní strana Přizpůsobit &stránce &Vpřed &Přejít na... Př&ejít na řádku... Př&ejít na stranu... Po&slední strana Od&eslat poštou... Pře&sunout do koše &Nový &Následující strana &Otevřít... V&ložit &Předchozí strana &Tisknout... U&končit Opětovně zob&razit &Přejmenovat... Na&hradit... Nahlásit &chybu... &Uložit &Uložit nastavení &Kontrola pravopisu... &Zpět Nahor&u &Zvětšení... &Následující &Předchozí Zobrazovat tipy při &spuštění Věděli jste...?
 Nastavit Tip dne O prostředí &KDE Vyči&stit Zkontrolovat pravopis dokumentu Smazat seznam Zavřít dokument Nastavit oznamová&ní... Nastavit klávesové zk&ratky... Nastavit panely nástro&jů... Kopírovat výběr do schránky Vytvořit nový dokument Vyjmou&t Vyjmout výběr do schránky Zr&ušit výběr vit@pelcak.org,mkyral@email.cz Automatická detekce Výchozí Celoobrazovkový reži&m Najít &následující Na&jít předchozí Přizpůsobit &výšce stránky &Přizpůsobit šířce stránky Zpátky v dokumentu Vpřed v dokumentu Přejít na první stranu Přejít na poslední stranu Přejít na následující stranu Přejít na předchozí stranu Přejít nahoru Vít Pelčák, Marián Kyral Žádné položky Ot&evřít nedávný Otevřít nedávno zavřený dokument Otevřít existující dokument Vložit obsah schránky Ná&hled před tiskem Vytisknout dokument Ukončit aplikaci Zno&vu V&rátit Obnovit zobrazení dokumentu Zopakovat naposled vrácenou činnost Vrátit neuložené změny v dokumentu Uložit j&ako... Uložit dokument Uložit dokument pod jiným názvem Vybr&at vše Vybrat úroveň přiblížení Odeslat dokument emailem Zobrazovat hlavní na&bídku Zobrazi&t panel nástrojů Zobrazovat hlavní nabídku<p>Znovu zobrazí hlavní nabídku, poté co byla skryta</p> Zobrazovat st&avový panel Zobrazovat stavový panel<p>Zobrazovat panel se stavovými informacemi vespod okna.</p> Zobrazit náhled před tiskem Zobrazovat nebo skrýt hlavní nabídku Zobrazovat nebo skrýt stavový panel Zobrazovat nebo skrýt panel nástrojů Přepnout jazyk &aplikace... Tip &dne Vrátit zpět poslední činnost Zobrazit dokument v reálné velikosti Co je &toto? Není vám dovoleno ukládat nastavení Před uložením budete požádáni o ověření &Přiblížit O&ddálit Přizpůsobit výšce stránky Přizpůsobit stránku oknu Přizpůsobit šířce stránky &Zpět &Vpřed &Domů Nápo&věda beze jména 