��          �      L      �  
   �     �  >   �                  
   -     8     @     H     O  	   [     e     s     z  2   �     �     �  �  �     �     �  9   �  	   �     �     �  	   �     �       	     
     	   #     -     >     E  2   Y     �     �                                   	                                                         
        &Browse... &Search: *.png *.xpm *.svg *.svgz|Icon Files (*.png *.xpm *.svg *.svgz) Actions All Applications Categories Devices Emblems Emotes Icon Source Mimetypes O&ther icons: Places S&ystem icons: Search interactively for icon names (e.g. folder). Select Icon Status Project-Id-Version: kio4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:11+0100
PO-Revision-Date: 2015-01-19 12:39+0100
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
 &Listovat... &Hledat: *.png *.xpm *.svg *.svgz|Ikony (*.png *.xpm *.svg *.svgz) Činnosti Vše Aplikace Kategorie Zařízení Emblémy Emotikony Zdroj ikon Mime typy Osta&tní ikony: Místa S&ystémové ikony: Hledat interaktivně názvy ikon (např. složka). Vybrat ikonu Stav 