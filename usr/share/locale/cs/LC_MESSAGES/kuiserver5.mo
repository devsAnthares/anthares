��          �   %   �      P     Q     b     w     �     �     �  
   �     �     �     �     �     �  /        3     Q     p     v     �     �     �     �     �     �     �       �  %     �          "     6     T     c     |  	   �     �     �     �     �  9   �      �  !     
   8     C     P     ^  "   f  #   �  "   �  #   �     �                                 
                                                                                             	    %1 file %1 files %1 folder %1 folders %1 of %2 processed %1 of %2 processed at %3/s %1 processed %1 processed at %2/s Appearance Behavior Cancel Clear Configure... Finished Jobs List of running file transfers/jobs (kuiserver) Move them to a different list Move them to a different list. Pause Remove them Remove them. Resume Show all jobs in a list Show all jobs in a list. Show all jobs in a tree Show all jobs in a tree. Show separate windows Show separate windows. Project-Id-Version: kuiserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2011-12-15 14:42+0100
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 1.2
 1 soubor %1 soubory %1 souborů 1 složka %1 složky %1 složek Zpracováno %1 z %2 Zpracováno %1 z %2 při %3/s Zpracováno %1 Zpracováno %1 při %2/s Vzhled Chování Zrušit Vymazat Nastavit... Ukončené úlohy Seznam spuštěných přenosů souborů/úloh (kuiserver) Přesunout je do jiného seznamu Přesunout je do jiného seznamu. Pozastavit Odstranit je Odstranit je. Obnovit Zobrazit všechny úlohy v seznamu Zobrazit všechny úlohy v seznamu. Zobrazit všechny úlohy ve stromu Zobrazit všechny úlohy ve stromu. Zobrazit oddělená okna Zobrazit oddělená okna. 