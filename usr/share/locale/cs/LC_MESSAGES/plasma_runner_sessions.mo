��          �   %   �      P  �  Q          )  6   9  -   p     �     �     �     �  (   �  d        x     �     �     �     �     �     �                7  "   X     {     �     �  �  �  �  �     '
     A
  7   U
  ;   �
     �
     �
     �
       *     i   C     �     �     �  
   �  	   �     �                     '     /     8     M  
   T                               	                                                                      
                        <p>You have chosen to open another desktop session.<br />The current session will be hidden and a new login screen will be displayed.<br />An F-key is assigned to each session; F%1 is usually assigned to the first session, F%2 to the second session and so on. You can switch between sessions by pressing Ctrl, Alt and the appropriate F-key at the same time. Additionally, the KDE Panel and Desktop menus have actions for switching between sessions.</p> Lists all sessions Lock the screen Locks the current sessions and starts the screen saver Logs out, exiting the current desktop session New Session Reboots the computer Restart the computer Shutdown the computer Starts a new session as a different user Switches to the active session for the user :q:, or lists all active sessions if :q: is not provided Turns off the computer User sessionssessions Warning - New Session lock screen commandlock log out log out commandLogout log out commandlogout new session restart computer commandreboot restart computer commandrestart shutdown computer commandshutdown switch user switch user commandswitch switch user commandswitch :q: Project-Id-Version: krunner_sessions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2014-10-22 11:12+0200
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 1.5
 <p>Zvolili jste otevření dalšího sezení.<br />Aktuální sezení bude skryto a bude zobrazena nová přihlašovací obrazovka.<br />Každému sezení je přiřazena F-klávesa, F%1 obvykle prvnímu sezení, F%2 druhému atd. Mezi sezeními se lze přepínat stisknutím klávesové kombinace Ctrl + Alt + F-klávesa. Mimo to obsahují panel a plocha odpovídající činnosti pro přepínaní mezi sezeními.</p> Vypíše všechna sezení Uzamknout obrazovku Uzamkne aktuální sezení a spustí spořič obrazovky Odhlásí se a ukončí aktuální sezení pracovní plochy Nové sezení Restartovat počítač Restartovat počítač Vypnout počítač Spustí nové sezení jako jiný uživatel Přepne aktivní sezení pro uživatele :q:, nebo vypíše seznam aktivních sezení, pokud nezadáte :q: Vypnout počítač sezení Varování - Nové sezení uzamčení odhlásit Odhlásit se odhlášení nové sezení reboot restart vypnutí přepnout uživatele switch switch :q: 