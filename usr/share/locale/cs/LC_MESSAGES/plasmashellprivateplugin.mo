��            )   �      �     �     �     �     �     �     �            !   $     F  !   [     }     �     �     �     �     �     �     �     �     �  2     @   >  	     4   �  8   �  $   �          *  �  .     �     �     �  %        .     M     T     k     r     �     �     �     �     �     �  
   �     	          5     N     k  B   �  =   �     	     	     	  '   	     B	     T	     
                                                             	                                                                       &Execute All Widgets Categories: Desktop Shell Scripting Console Download New Plasma Widgets Editor Executing script at %1 Filters Install Widget From Local File... Installation Failure Installing the package %1 failed. Load New Session Open Script File Output Running Runtime: %1ms Save Script File Screen lock enabled Screen saver timeout Select Plasmoid File Sets the minutes after which the screen is locked. Sets whether the screen will be locked after the specified time. Templates Toolbar Button to switch to KWin Scripting ModeKWin Toolbar Button to switch to Plasma Scripting ModePlasma Unable to load script file <b>%1</b> Uninstallable Use Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-23 03:23+0100
PO-Revision-Date: 2017-02-24 11:54+0100
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
 &Spustit Všechny widgety Kategorie:  Skriptovací konzole shellu na ploše Stáhnout nové Plasma widgety Editor Spouštím skript v %1 Filtry Instalovat widget ze souboru... Chyba instalace Instalace balíčku %1 selhala. Načíst Nové sezení Otevřít soubor se skriptem Výstup Běžící Doba běhu: %1ms Uložit soubor se skriptem Zámek obrazovky povolen Prodleva spořiče obrazovky Vyberte soubor plasmoidu Nastavuje počet minut, po jejichž uplynutí se zamkne obrazovka. Nastavuje, zda bude zámek obrazovky po daném čase povolen. Šablony KWin Plasma Nelze načíst soubor skriptu <b>%1</b> Odinstalovatelné Použít 