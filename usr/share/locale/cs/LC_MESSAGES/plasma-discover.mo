��    S      �  q   L                '     *     2  5   B  E   x  E   �  >        C     Z  7   l     �     �     �     �     	     ,	     :	     L	     S	     Z	     m	     �	     �	     �	     �	     �	  	   �	     �	     �	     
     
     (
     0
     9
     G
  	   O
     Y
  	   a
     k
     {
     �
      �
     �
  
   �
     �
     �
             
        '     /     7     >     Q     X     g     n  	   �  
   �     �     �     �     �     �     �     �     �          )     2     8     D  
   `     k     {     �     �     �     �     �  $   �  �       �     �     �     �  2   �  J     J   i  C   �     �  
     ;        [     t  %   �  $   �     �     �     �  	                  1     H     X     v     �     �  
   �  (   �     �     �     �     
               ,     :  
   M     X     g     w       ,   �  "   �     �  )   �          4     H     Q     f     r  	   ~     �     �     �     �     �  	   �  
   �     �  
   �     �       	   $      .     O     V     q  	   �     �     �     �     �     �     �     �     �               0  #   E        L   $          R   6      1   "         -   C   F   >   &          O      :                   @      P         B       .   /   9   I       	   E   ,   !   K           2      M   
      5      J   +              '   *   D       =      A       Q          ;       )   H   N                                       7       8   3          S          0                #                     ?   %          <   (   4       G    
Also available in %1 %1 %1 (%2) <b>%1</b> by %2 <em>%1 out of %2 people found this review useful</em> <em>Useful? <a href='true'><b>Yes</b></a>/<a href='false'>No</a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'><b>No</b></a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'>No</a></em> @infoFetching updates @infoFetching... @infoIt is unknown when the last check for updates was @infoLooking for updates @infoNo updates @infoNo updates are available @infoShould check for updates @infoThe system is up to date @infoUpdates @infoUpdating... Accept Addons Aleix Pol Gonzalez An application explorer Apply Changes Available backends:
 Available modes:
 Back Cancel Category: Checking for updates... Could not find category '%1' Couldn't open %1 Delete the origin Discard Discover Extensions... Help... Homepage: Install Installed Jonathan Thomas Launch License: List all the available backends. List all the available modes. Loading... Local package file to install Make default More Information... More... No Updates Proceed Rating: Remove Resources for '%1' Review Reviewing '%1' Search Search in '%1'... Search... Search: %1 Search: %1 + %2 Settings Short summary... Show reviews (%1)... Size: Sorry, nothing found... Source: Specify the new source for %1 Still looking... Summary: Tasks Tasks (%1%) Unable to find resource: %1 Update All Update Selected Update section nameUpdate (%1) Updates Version: unknown reviewer updates not selected updates selected © 2010-2016 Plasma Development Team Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:02+0100
PO-Revision-Date: 2017-11-15 16:56+0100
Last-Translator: Vit Pelcak <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
 
Také dostupné na %1 %1 %1 (%2) <b>%1</b> od %2 <em>%1 z %2 osob shledaly recenzi užitečnou</em> <em>Užitečné? <a href='true'><b>Ano</b></a>/<a href='false'>Ne</a></em> <em>Užitečné? <a href='true'>Ano</a>/<a href='false'><b>Ne</b></a></em> <em>Užitečné? <a href='true'>Ano</a>/<a href='false'>Ne</a></em> Získávají se aktualizace Stahuji... Není známo, kdy proběhla poslední kontrola aktualizací Vyhledávám aktualizace Žádné aktualizace Nejsou dostupné žádné aktualizace Měli byste zkontrolovat aktualizace Váš systém je aktuální Aktualizace Aktualizuji... Přijmout Doplňky Aleix Pol Gonzalez Prohlížeč aplikací Provést změny Dostupné podpůrné vrstvy:
 Dostupné režimy:
 Zpět Zrušit Kategorie: Kontroluje se dostupnost aktualizací... Nelze nalézt kategorii '%1' Nelze otevřít %1 Smazat původ Zahodit Discover Rozšíření... Nápověda... Domácí stránka: Instalovat Nainstalováno Jonathan Thomas Spustit Licence: Vypsat všechny dostupné podpůrné vrstvy. Vypsat všechny dostupné režimy. Probíhá načítání... Lokální soubor s balíčkem k instalaci Nastavit jako výchozí Více informací... Více... Žádné aktualizace Pokračovat Hodnocení: Odstranit Zdroje pro '%1' Kontrola Stahuji '%1' Hledat Hledat v '%1'... Hledat... Hledat: %1 Hledat: %1 + %2 Nastavení Krátké shrnutí... Zobrazit recenze (%1)... Velikost: Bohužel, nic nebylo nalezeno... Zdroj: Zadejte nový zdroj pro %1 Stále hledám... Shrnutí: Úkoly Úkoly (%1%) Nelze najít zdroj: %1 Aktualizovat vše Aktualizovat vybrané Aktualizovat (%1) Aktualizace Verze: neznámý kontrolor nevybrané aktualizace vybrané aktualizace © 2010-2016 Vývojový tým Plasma 