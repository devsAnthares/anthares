��    0      �  C         (  6   )  M   `  K   �     �  '        +     2     C  	   Z  A   d  >   �  9   �  9     9   Y  W   �  E   �     1     B      H     i  7   �      �     �     �  X        `     h     ~     �     �     �     �  
   �  
   �     �     	  E  4	     z
     �
     �
     �
     �
     �
     �
     �
  	          �    H   	  {   R  b   �     1  %   :     `     f     x     �     �     �     �     �  	   �  .   �               #     *     9  4   X  !   �     �     �  :   �  
   !     ,     C     \     p     v     �  
   �  
   �     �     �  [  �     '     D      c     �     �     �     �      �     �     �                     
      '                     *   "      ,   .             )                       &             	   +   #   0                                      !            %       $           /       -                   (            "Full screen repaints" can cause performance problems. "Only when cheap" only prevents tearing for full screen changes like a video. "Re-use screen content" causes severe performance problems on MESA drivers. Accurate Allow applications to block compositing Always Animation speed: Author: %1
License: %2 Automatic Category of Desktop Effects, used as section headerAccessibility Category of Desktop Effects, used as section headerAppearance Category of Desktop Effects, used as section headerCandy Category of Desktop Effects, used as section headerFocus Category of Desktop Effects, used as section headerTools Category of Desktop Effects, used as section headerVirtual Desktop Switching Animation Category of Desktop Effects, used as section headerWindow Management Configure filter Crisp EMAIL OF TRANSLATORSYour emails Enable compositor on startup Exclude Desktop Effects not supported by the Compositor Exclude internal Desktop Effects Full screen repaints Get New Effects... Hint: To find out or configure how to activate an effect, look at the effect's settings. Instant KWin development team Keep window thumbnails: NAME OF TRANSLATORSYour names Never Only for Shown Windows Only when cheap OpenGL 2.0 OpenGL 3.1 OpenGL Platform InterfaceEGL OpenGL Platform InterfaceGLX OpenGL compositing (the default) has crashed KWin in the past.
This was most likely due to a driver bug.
If you think that you have meanwhile upgraded to a stable driver,
you can reset this protection but be aware that this might result in an immediate crash!
Alternatively, you might want to use the XRender backend instead. Re-enable OpenGL detection Re-use screen content Rendering backend: Scale method: Search Smooth Smooth (slower) Tearing prevention ("vsync"): Very slow XRender Project-Id-Version: kcmkwincompositing
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2017-11-01 12:27+0100
Last-Translator: Vit Pelcak <vpelcak@suse.cz>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
X-Language: cs_CZ
X-Source-Language: en_US
 "Překreslení celé obrazovky" může způsobit výkonnostní problémy "Pouze pokud není náročné" zabrání trhání pouze po změny na celé obrazovce jako třeba při přehrávání videa. "Znovu využít obsah obrazovky" způsobuje na ovladačích MESA vážné výkonnostní problémy. Přesná Povolit aplikacím blokovat kompozici Vždy Rychlost animace: Autor: %1
Licence: %2 Automaticky Zpřístupnění Vzhled Pastva pro oči Zaměření Nástroje Animace při přepínání virtuálních ploch Správa oken Nastavte filtr Hrubá koty@seznam.cz Povolit kompozitor při startu Vyjmout efekty na ploše nepodporované kompozitorem Vyjmout interní efekty na ploše Překreslení celé obrazovky Získat nové efekty ... Rada: více informací o efektu najdete v jeho nastavení. Okamžitě Tým vývojářů KWin Zachovat miniatury oken: Klára Cihlářová Nikdy Pouze pro zobrazená okna Pouze pokud není náročné OpenGL 2.0 OpenGL 3.1 EGL GLX Kompozice OpenGL (výchozí) vedla v minulosti k pádu KWinu.
K tomu nejspíš došlo kvůli chybě v ovladači.
Pokud si myslíte, že jste mezitím aktualizovali na stabilní verzi ovladače,
můžete resetovat ochranu, ale mějte na paměti, že to může vést k okamžitému pádu.
Případně můžete na místo toho zkusit použít XRender. Znovu povolit detekci OpenGL Znovu využít obsah obrazovky Vykreslovací podpůrná vrstva: Metoda škálování: Hledat Jemná Jemná (pomalejší) Zabránění trhání ("VSync"): Velmi pomalu XRender 