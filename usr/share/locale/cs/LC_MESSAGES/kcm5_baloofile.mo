��          �      �       H     I     a  #   w      �      �     �  J   �     ;  &   Y     �     �  *   �     �  �  �     �     �  #         $     @     O  Y   o  ,   �  8   �     /     =  '   M     u               
              	                                    Also index file content Configure File Search Copyright 2007-2010 Sebastian Trüg Do not search in these locations EMAIL OF TRANSLATORSYour emails Enable File Search File Search helps you quickly locate all your files based on their content Folder %1 is already excluded Folder's parent %1 is already excluded NAME OF TRANSLATORSYour names Sebastian Trüg Select the folder which should be excluded Vishesh Handa Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2017-04-18 13:12+0100
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
X-Language: cs_CZ
X-Source-Language: en_US
 Taky indexovat obsah souboru Nastavit hledání souborů Copyright 2007-2010 Sebastian Trüg Nehledat v těchto místech vit@pelcak.org Povolit vyhledávání souborů Hledání souborů vám pomáhá rychle najít všechny vaše soubory podle jejich obsahu Složka %1 je již nastavená pro vyřazení Složka nadřazená %1 je již nastavená pro vyřazení Vít Pelčák Sebastian Trüg Zvolte složku jež má být vynechána Vishesh Handa 