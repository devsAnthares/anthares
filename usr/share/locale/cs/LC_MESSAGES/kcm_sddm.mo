��    ,      |  ;   �      �  (   �     �  �   �     �     �     �     �     �     �     �     �     �               %     1      J     k     s     �     �     �     �     �     �     �               /     4     I     Y     l     y     �     �      �  7   �                     1     6  �  <     �     �     �     	     )	     5	     ;	     X	     a	  	   r	     |	     �	     �	  	   �	     �	     �	     �	     �	     �	     
     
     5
     I
     e
     |
     �
     �
     �
     �
     �
     �
                 0     D     ]  !   f  A   �     �     �     �  	   �  
        !                 %   +   ,      "   #                               &                  	                           )                             *      '      $      (                                
    %1 is the name of a session%1 (Wayland) ... @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) @title:windowSelect Image Advanced Author Auto &Login Background: Clear Image Commands Could not decompress archive Cursor Theme: Customize theme Default Description Download New SDDM Themes EMAIL OF TRANSLATORSYour emails General Get New Theme Halt Command: Install From File Install a theme. Invalid theme package Load from file... Login screen using the SDDM Maximum UID: Minimum UID: NAME OF TRANSLATORSYour names Name No preview available Reboot Command: Relogin after quit Remove Theme SDDM KDE Config SDDM theme installer Session: The default cursor theme in SDDM The theme to install, must be an existing archive file. Theme Unable to install theme Uninstall a theme. User User: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2017-11-01 12:28+0100
Last-Translator: Vit Pelcak <vpelcak@suse.cz>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
 %1 (Wayland) ... (Dostupné velikosti: %1) Vybrat obrázek Pokročilé Autor Automatické přih&lášení Pozadí: Vymazat obrázek Příkazy Archiv nelze rozbalit. Motiv kurzorů: Upravit motiv Výchozí Popis Stáhnout nové motivy SDDM vit@pelcak.org Obecné Získat nový motiv Příkaz pro vypnutí: Instalovat ze souboru Nainstalovat motiv. Neplatný balíček motivu. Načíst ze souboru... Přihlašovací obrazovka SDDM Maximální UID: Minimální UID: Vít Pelčák Název Náhled není dostupný Příkaz pro restart: Znovu přihlásit po ukončení Odstranit motiv KDE nastavení SDDM Instalátor motivů SDDM Sezení: Výchozí motiv kurzorů pro SDDM Motiv pro instalací musí být existující archivovaný soubor. Motiv Motiv nelze nainstalovat Odinstalovat motiv. Uživatel Uživatel: 