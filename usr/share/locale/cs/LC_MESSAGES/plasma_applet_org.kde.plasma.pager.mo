��          �   %   �      0  *   1     \  .   s     �     �     �     �     �     �                 
   $     /     5     =     E     ]     t     �     �     �     �  �  �  K   �       @        _     o     �     �     �     �     �     �     �  
   �     �     �     
          3     P     k     �  	   �     �        
                             	                                                                                          %1 Minimized Window: %1 Minimized Windows: %1 Window: %1 Windows: ...and %1 other window ...and %1 other windows Activity name Activity number Add Virtual Desktop Configure Desktops... Desktop name Desktop number Display: Does nothing General Horizontal Icons Layout: No text Only the current screen Remove Virtual Desktop Selecting current desktop: Show Activity Manager... Shows desktop The pager layoutDefault Vertical Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-19 03:29+0200
PO-Revision-Date: 2016-11-03 16:43+0100
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
X-Language: cs_CZ
X-Source-Language: en_US
 %1 Minimalizované okno %1 Minimalizovaná okna: %1 Minimalizovaných oken: %1 okno: %1 okna: %1 oken: ...a %1 další okno ...a %1 další okna ...a %1 dalších oken Název aktivity Číslo aktivity Přidat virtuální plochu Nastavit plochy... Název plochy Číslo plochy Zobrazení: Nedělá nic Obecné Vodorovně Ikony Rozvržení: Žádný text Pouze aktuální obrazovku Odstranit virtuální plochu Výběr aktuální plochy: Zobrazit správce aktivit... Zobrazí plochu Výchozí Svisle 