��    "      ,  /   <      �     �          !     7     ;     D     H     O     [     o     x     �     �     �     �     �     �     �     �     �     �     �     �     �                    *     :     ?     K  
   \     g  �  m     j  	   l     v  
   �     �     �     �     �     �     �     �     �     �     
               /     7     ?     G     W     j     x     ~  	   �     �     �     �     �     �          !  
   @        	           !                                
                                  "                                                                         Abbreviation for secondss Application: Average clock: %1 MHz Bar Buffers: CPU CPU %1 CPU monitor CPU%1: %2% @ %3 Mhz CPU: %1% CPUs separately Cache Cached: Circular Colors Compact Bar General IOWait: Memory Memory monitor Memory: %1/%2 MiB Monitor type: Nice: Set colors manually Show: Swap Swap monitor Swap: %1/%2 MiB Sys: System load Update interval: Used swap: User: Project-Id-Version: plasma_applet_systemloadviewer
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-29 03:31+0200
PO-Revision-Date: 2017-02-24 16:52+0100
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
X-Language: cs_CZ
X-Source-Language: en_US
 s Aplikace: Průměrná frekvence: %1 MHz Sloupcový Zásobníky: CPU CPU %1 Monitor CPU CPU%1: %2% @ %3 MHz CPU: %1% CPU odděleně Mezipaměť V mezipaměti: Kruhový Barvy Kompaktní sloupcový Obecné IOWait: Paměť Monitor paměti Paměť: %1/%2 MiB Typ monitoru: Nice: Nastavit barvy ručně Zobrazit: Odkládací prostor Monitor odkládacího prostoru Využití swapu: %1%/%2 MiB Sys: Systémové zatížení Interval aktualizace: Použitý odkládací prostor: Uživatel: 