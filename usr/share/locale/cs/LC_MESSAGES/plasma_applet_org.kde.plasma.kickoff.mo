��            )   �      �     �  
   �  /   �  -   �             
   "     -     :     P     Y  ,   n  	   �     �     �     �     �     �  
   �     �     �               /     I     ]  1   r     �  �  �     �  
   �  	   �     �     �     �     �     �     �  
          4   0  
   e     p     ~     �     �     �     �     �     �     �               ;      R     s     �                                                                                   
                                	                       %1@%2 %2@%3 (%1) @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Favorites All Applications Appearance Applications Applications updated. Computer Edit Applications... Expand search to bookmarks, files and emails Favorites Hidden Tabs History Icon: Leave Menu Buttons Often Used On All Activities On The Current Activity Remove from Favorites Show In Favorites Show applications by name Sort alphabetically Switch tabs on hover Type is a verb here, not a nounType to search... Visible Tabs Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2017-09-11 13:22+0100
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
X-Language: cs_CZ
X-Source-Language: en_US
 %1@%2 %2@%3 (%1) Vybrat... Vymazat ikonu Přidat do oblíbených Všechny aplikace Vzhled Aplikace Aplikace byla aktualizována. Počítač Upravit aplikace... Rozšířit hledání na záložky, soubory a emaily Oblíbené Skryté karty Historie Ikona: Opustit Tlačítka nabídky Často používané Ve všech aktivitách V současné aktivitě Odstranit z oblíbených Zobrazit v oblíbených Zobrazit aplikace podle jména Seřadit podle abecedy Přepínat na karty pod kurzorem Text k vyhledání... Viditelné karty 