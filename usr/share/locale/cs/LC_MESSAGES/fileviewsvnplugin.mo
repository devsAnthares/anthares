��          �   %   �      p     q  +   �  .   �  6   �  *     #   D  &   h  /   �  2   �  :   �  0   -  3   ^  ;   �  K   �  -     $   H  '   m     �     �     �     �     �  #        1     O     c     |  �  �     =  +   D  *   p  0   �     �     �     	  ,   &	  )   S	  4   }	  +   �	  *   �	  0   	
  L   :
  %   �
  "   �
     �
     �
     �
  
     
     
        &     D     ]  
   d     o                                                    	   
                                                                    @action:buttonCommit @info:statusAdded files to SVN repository. @info:statusAdding files to SVN repository... @info:statusAdding of files to SVN repository failed. @info:statusCommit of SVN changes failed. @info:statusCommitted SVN changes. @info:statusCommitting SVN changes... @info:statusRemoved files from SVN repository. @info:statusRemoving files from SVN repository... @info:statusRemoving of files from SVN repository failed. @info:statusReverted files from SVN repository. @info:statusReverting files from SVN repository... @info:statusReverting of files from SVN repository failed. @info:statusSVN status update failed. Disabling Option "Show SVN Updates". @info:statusUpdate of SVN repository failed. @info:statusUpdated SVN repository. @info:statusUpdating SVN repository... @item:inmenuSVN Add @item:inmenuSVN Commit... @item:inmenuSVN Delete @item:inmenuSVN Revert @item:inmenuSVN Update @item:inmenuShow Local SVN Changes @item:inmenuShow SVN Updates @labelDescription: @title:windowSVN Commit Show updates Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2015-11-24 16:28+0100
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 Commit Soubory byly přidány do repozitáře SVN. Přidávám soubory do repozitáře SVN... Přidání souborů do repozitáře SVN selhalo. Selhalo zaslání změn v SVN. Změny v SVN byly odeslány. Zasílají se změny v SVN... Soubory byly odstraněny z repozitáře SVN. Odstraňuji soubory z repozitáře SVN... Odstraňování souborů z repozitáře SVN selhalo. Soubory byly navráceny z repozitáře SVN. Navrácení souborů z repozitáře SVN... Navrácení souborů z repozitáře SVN selhalo. Aktualizace stavu SVN selhala. Zakazuji možnost "Zobrazit aktualizace SVN". Aktualizace repozitáře SVN selhala. Repozitář SVN byl aktualizován. Aktualizuji repozitář SVN... SVN Add SVN Commit... SVN Delete SVN Revert SVN Update Zobrazit lokální změny SVN Zobrazit aktualizace SVN Popis: SVN Commit Zobrazit aktualizace 