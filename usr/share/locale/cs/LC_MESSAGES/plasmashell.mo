��    "      ,  /   <      �     �       	   &  0   0  .   a     �  7   �      �            -   0  $   ^     �     �  E   �  G   �  >   =     |     �     �     �     '     4     J     i  <   n  :   �  s   �  o   Z  .   �  ,   �  .   &  ,   U  �  �  #   a
     �
     �
     �
     �
     �
  /   �
             '   -     U  *   j     �     �     �     �     �     �     �       �        �     �  !   �     �     �     
      #      D     e     u     �     �                                                                       !                      	                                    "                         
    Activate Task Manager Entry %1 Activities... Add Panel Bluetooth was disabled, keep shortBluetooth Off Bluetooth was enabled, keep shortBluetooth On Configure Mouse Actions Plugin Do not restart plasma-shell automatically after a crash EMAIL OF TRANSLATORSYour emails Empty %1 Enable QML Javascript debugger Fatal error message titlePlasma Cannot Start Force loading the given shell plugin Hide Desktop NAME OF TRANSLATORSYour names OSD informing that some media app is muted, eg. Amarok Muted%1 Muted OSD informing that the microphone is muted, keep shortMicrophone Muted OSD informing that the system is muted, keep shortAudio Muted Plasma Plasma Failed To Start Plasma Shell Plasma is unable to start as it could not correctly use OpenGL 2.
 Please check that your graphic drivers are set up correctly. Show Desktop Stop Current Activity Unable to load script file: %1 file mobile internet was disabled, keep shortMobile Internet Off mobile internet was enabled, keep shortMobile Internet On on screen keyboard was disabled because physical keyboard was plugged in, keep shortOn-Screen Keyboard Deactivated on screen keyboard was enabled because physical keyboard got unplugged, keep shortOn-Screen Keyboard Activated touchpad was disabled, keep shortTouchpad Off touchpad was enabled, keep shortTouchpad On wireless lan was disabled, keep shortWifi Off wireless lan was enabled, keep shortWifi On Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-12 03:10+0100
PO-Revision-Date: 2017-02-24 11:48+0100
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
X-Language: cs_CZ
X-Source-Language: en_US
 Aktivovat záznam Správce úloh %1 Aktivity... Přidat panel Bluetooth vypnut Bluetooth zapnut Nastavit modul činností myši Nerestartovat plasma-shell po pádu automaticky vit@pelcak.org Prázdný %1 Povolit ladicí nástroj QML JavaScript Plasmu nelze spustit Vynutit načítání daného moduly shellu Skrýt pracovní plochu Vít Pelčák %1 ztlumený Mikrofon ztlumen Zvuk ztlumen Plasma Start Plasmy selhal Shell Plasmy Plasma se nemůže spustit protože nemůže správně používat OpenGL 2.
 Prosím, zkontrolujte, že máte správně nastaveny ovladače. Zobrazit plochu Zastavit současnou aktivitu Nelze načíst soubor skriptu: %1 soubor Mobilní internet vypnut Mobilní internet zapnut Vypnuta klávesnice na obrazovce Zapnuta klávesnice na obrazovce Touchpad vypnut Touchpad zapnut Wifi vypnuta Wifi zapnuta 