��          �            x  
   y  	   �     �     �  3   �     �     �                    %     *     F  B   Y     �  �  �     |     �     �     �  9   �     �     �             	   7     A     J     `     }     �     	                                                  
                        Appearance Browse... Choose an image Fifteen Puzzle Image Files (*.png *.jpg *.jpeg *.bmp *.svg *.svgz) Number color Path to custom image Piece color Show numerals Shuffle Size Solve by arranging in order Solved! Try again. The time since the puzzle started, in minutes and secondsTime: %1 Use custom image Project-Id-Version: plasma_applet_fifteenPuzzle
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-01-12 03:59+0100
PO-Revision-Date: 2014-11-12 15:57+0100
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 1.5
 Vzhled Procházet... Vyberte obrázek Hra Patnáct Soubory obrázků (*.png *.jpg *.jpeg *.bmp *.svg *.svgz) Barva čísla Cesta k vlastnímu obrázku Barva dílku Zobrazit číslování Zamíchat Velikost Vyřešit seřazením Vyřešeno! Zkuste to znovu. Čas: %1 Použít vlastní obrázek 