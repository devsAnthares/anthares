��    0      �  C         (     )     +     I     M     U     ^     p     }     �     �     �     �      �     �     �     �               ,     <     J     W     e     s  	        �     �     �     �     �     �     �     �     �     �     �               (     /     6     >     [     b     p  4   t     �  �  �     t     v     |     �     �     �     �     �     �     �     �     �     �     �     �     
	     '	     3	     A	     Q	     f	     y	     �	     �	     �	     �	     �	     �	     �	     �	     �	  	   �	  
   
     
     
     ,
     .
     6
     >
     G
     I
      R
     s
     u
     x
  	   |
     �
         	                    +                           ,   *            
             %                    /       -                                    $          &      (         )   '   #                       .   !             "   0    % %1 is value, %2 is unit%1 %2 %1: Battery Capacity Charge Percentage Charge state Charging Current Degree Celsius°C Details: %1 Discharging EMAIL OF TRANSLATORSYour emails Energy Energy Consumption Energy Consumption Statistics Environment Fully charged Kai Uwe Broulik Last 12 hours Last 2 hours Last 24 hours Last 48 hours Last 7 days Last hour Manufacturer Model NAME OF TRANSLATORSYour names No Not charging PID: %1 Path: %1 Rechargeable Refresh Serial Number Shorthand for WattsW System Temperature Vendor VoltV Voltage Wakeups per second: %1 (%2%) WattW Watt-hoursWh Yes current power draw from the battery in WConsumption literal percent sign% Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2017-02-24 09:41+0100
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
 % %1 %2 %1: Baterie Kapacita Procento nabití Stav nabití Nabíjí se Proud °C Podrobnosti: %1 Vybíjí se vit@pelcak.org Energie Spotřeba energie Statistika spotřeby energie Prostředí Plně nabitá Kai Uwe Broulik Posledních 12 hodin Poslední 2 hodiny Posledních 24 hodin Posledních 48 hodin Posledních 7 dní Poslední hodina Výrobce Model Vít Pelčák Ne Nenabíjí se PID: %1 Cesta: %1 Nabíjecí Obnovit Sériové číslo W Systém Teplota Prodejce V Napětí Probuzení za vteřinu: %1 (%2%) W Wh Ano Spotřeba % 