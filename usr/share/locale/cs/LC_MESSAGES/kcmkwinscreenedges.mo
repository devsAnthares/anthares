��             +         �     �     �     �  	   �     �               '     3     H     a     t     �  S   �  w   �     `  M   u      �     �     �  	             2     K  %   Z     �     �  #   �     �     �     �  �  	     �     �     �               (     ?  
   K     V     l     �     �     �  d   �  p   %	     �	  A   �	     �	     �	     
     
     0
     G
     X
  $   k
     �
  
   �
  (   �
     �
     �
  	                                               
                                                     	                                                ms % %1 - All Desktops %1 - Cube %1 - Current Application %1 - Current Desktop %1 - Cylinder %1 - Sphere &Reactivation delay: &Switch desktop on edge: Activation &delay: Activity Manager Always Enabled Amount of time required after triggering an action until the next trigger can occur Amount of time required for the mouse cursor to be pushed against the edge of the screen before the action is triggered Application Launcher Change desktop when the mouse cursor is pushed against the edge of the screen EMAIL OF TRANSLATORSYour emails Lock Screen NAME OF TRANSLATORSYour names No Action Only When Moving Windows Open krunnerRun Command Other Settings Quarter tiling triggered in the outer Show Desktop Switch desktop on edgeDisabled Toggle alternative window switching Toggle window switching Window Management of the screen Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-30 03:13+0100
PO-Revision-Date: 2017-04-18 13:14+0100
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
X-Language: cs_CZ
X-Source-Language: en_US
  ms % %1 - Všechny plochy %1 - Kostka %1 - Aktuální aplikace %1 - Aktuální plocha %1 - Válec %1 - Koule Prodleva &reaktivace: &Přepnout obrazovku na okraji: Pro&dleva aktivace: Správce aktivit Vždy povoleno Doba, která musí uběhnout od naposled vykonané činnosti po spuštění následující činnosti Doba, po jakou musí být ukazatel myši posunován proti okraji obrazovky aby se spustila přidělená činnost Spouštěč aplikací Změnit plochu pokud je ukazatel myši posunut k okraji obrazovky vit@pelcak.org Zamknout obrazovku Vít Pelčák Žádná činnost Pouze při pohybu oken Spustit příkaz Další nastavení Dlaždice aktivována ve vnějších Zobrazit pracovní plochu Zakázáno Zapnout alternativní přepínání oken Zapnout přepínání oken Správa oken obrazovky 