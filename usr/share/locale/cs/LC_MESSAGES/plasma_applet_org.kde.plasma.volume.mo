��    &      L  5   |      P     Q  8   S     �     �     �     �     �     �  3   �  >        N     i     y     �  m   �     �          "     2     7     ?  *   O      z     �     �  "   �     �     �          3     :     H     X     e     �  ;   �     �  �  �     �     �     �     �     �  	   �     �     	     	  	   &	     0	     M	     `	     l	     t	     z	     �	     �	     �	  
   �	     �	  <   �	  7   "
     Z
     t
     �
     �
     �
  !   �
  	   �
     �
               ,     4     ;     @        #   %                                                              "          $                      
                                           &      !                	        % Accessibility data on volume sliderAdjust volume for %1 Applications Audio Muted Audio Volume Behavior Capture Devices Capture Streams Checkable switch for (un-)muting sound output.Mute Checkable switch to change the current default output.Default Decrease Microphone Volume Decrease Volume Devices General Heading for a list of ports of a device (for example built-in laptop speakers or a plug for headphones)Ports Increase Microphone Volume Increase Volume Maximum volume: Mute Mute %1 Mute Microphone No applications playing or recording audio No output or input devices found Playback Devices Playback Streams Port is unavailable (unavailable) Port is unplugged (unplugged) Raise maximum volume Show additional options for %1 Volume Volume at %1% Volume feedback Volume step: label of device items%1 (%2) label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2017-10-06 13:43+0100
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
 % Upravit hlasitost pro %1 Aplikace Zvuk ztlumen Hlasitost zvuku Chování Zachytávací zařízení Zachytávat proudy Ztlumit Výchozí Snížit hlasitost mikrofonu Snížit hlasitost Zařízení Obecné Porty Zvýšit hlasitost mikrofonu Zvýšit hlasitost Maximální hlasitost: Ztlumit Ztišit %1 Ztlumit mikrofon Žádné aplikace přehrávající nebo nahrávající audio Nenalezena žádná vstupní ani výstupní zařízení Přehrávací zařízení Přehrávat proudy  (nedostupné)  (nepřipojen) Zvýšit maximální hlasitost Zobrazit další možnosti pro %1 Hlasitost Hlasitost na %1% Zpětná vazba hlasitosti Krok hlasitosti: %1 (%2) %1: %2 100% %1% 