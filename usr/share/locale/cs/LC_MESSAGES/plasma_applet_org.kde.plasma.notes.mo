��          �   %   �      0     1     E     X     l          �     �     �     �  
   �     �     �                                    "     0  	   <     F     L  �  S          6     Q     m     �     �     �     �                &     .     5     =     E  
   N  	   Y  	   c     m     |     �     �     �     	                                                                          
                                                A black sticky note A blue sticky note A green sticky note A pink sticky note A red sticky note A translucent sticky note A white sticky note A yellow sticky note An orange sticky note Appearance Black Blue Bold Green Italic Orange Pink Red Strikethrough Translucent Underline White Yellow Project-Id-Version: plasma_applet_notes
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-07-09 03:12+0200
PO-Revision-Date: 2017-02-24 11:41+0100
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
 Černá poznámka na plochu Modrá poznámka na plochu Zelená poznámka na plochu Růžová poznámka na plochu Červená poznámka na plochu Průhledná poznámka na plochu Bílá poznámka na plochu Žlutá poznámka na plochu Oranžová poznámka na plochu Vzhled Černá Modrá Tučné Zelená Kurzíva Oranžová Růžová Červená Přeškrtnuté Průhledné Podtržené Bílá Žlutá 