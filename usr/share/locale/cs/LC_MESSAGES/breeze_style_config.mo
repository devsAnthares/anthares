��            )   �      �     �  "   �     �  !   �  !        $  
   :     E     `     p  !   �     �  0   �     �  !        6     T     r     �     �     �     �     �  
   �  
   �  
   �  &        .     :  �  O     .  (   2     [  &   u  *   �     �     �     �     �       +   "  &   N  E   u  .   �  .   �  (   	  ,   B	     o	  /   �	     �	  /   �	  	   �	     
     
     #
  
   4
  3   ?
     s
     �
                                                                                                  	                       
                 ms &Keyboard accelerators visibility: &Top arrow button type: Always Hide Keyboard Accelerators Always Show Keyboard Accelerators Anima&tions duration: Animations Bottom arrow button t&ype: Breeze Settings Center tabbar tabs Drag windows from all empty areas Drag windows from titlebar only Drag windows from titlebar, menubar and toolbars Draw focus indicator in lists Draw frame around dockable panels Draw frame around page titles Draw frame around side panels Draw slider tick marks Draw toolbar item separators Enable animations Enable extended resize handles Frames General No Buttons One Button Scrollbars Show Keyboard Accelerators When Needed Two Buttons W&indows' drag mode: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:42+0200
PO-Revision-Date: 2017-09-12 14:23+0100
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
X-Language: cs_CZ
X-Source-Language: en_US
  ms &Viditelnost akcelerátorů klávesnice: &Typ horního tlačítka: Vždy skrýt akcelerátory klávesnice Vždy zobrazovat akcelerátory klávesnice Délka &animací: Animace T&yp tlačítka šipky dolů: Nastavení Breeze Vystředit karty Přesouvat okna ze všech prázdných míst Přesouvat okna pouze z panelu titulku Přesouvat okna z panelu titulku, panelu nabídky a panelu nástrojů Vykreslovat v seznamech indikátor zaměření Kreslit rámeček okolo ukotvitelných panelů Kreslit rámečky okolo názvů stránek Kreslit rámečky okolo postranních panelů Kreslit značky na posuvníku Kreslit oddělovače položek panelu nástrojů Povolit animace Povolit rozšířené úchytky změny velikosti Rámečky Obecné Žádná tlačítka Jedno tlačítko Posuvníky Zobrazovat akcelerátory klávesnice podle potřeby Dvě tlačítka Rež&im přesouvání oken: 