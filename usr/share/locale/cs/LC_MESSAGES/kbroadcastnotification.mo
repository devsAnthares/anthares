��    
      l      �       �      �   O   
     Z      r     �     �  H   �  !        7  �  T       U   !     w     �     �     �  C   �                                   	   
                  (c) 2016 Kai Uwe Broulik A tool that emits a notification for all users by sending it on the system DBus Broadcast Notifications EMAIL OF TRANSLATORSYour emails Icon for the notification NAME OF TRANSLATORSYour names Name of the application that should be associated with this notification The actual notification body text Timeout for the notification Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-11 05:52+0100
PO-Revision-Date: 2017-02-22 12:17+0100
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
 (c) 2016 Kai Uwe Broulik Nástroj, který posílá oznámení všem uživatelům posláním přes systém DBus Rozeslat oznámení vit@pelcak.org Ikona pro oznámení Vít Pelčák Název aplikace, jenž by měla být přidružena tomuto oznámení Text oznámení upozornění Časová prodleva pro oznámení 