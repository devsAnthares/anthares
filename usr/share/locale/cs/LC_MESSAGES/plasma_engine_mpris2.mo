��          t      �         C        U     i  3   �     �     �  F   �  5   *     `       �  �  5   l     �     �     �     �     �  >     :   R     �     �                              	                          
    Attempting to perform the action '%1' failed with the message '%2'. Media playback next Media playback previous Name for global shortcuts categoryMedia Controller Play/Pause media playback Stop media playback The argument '%1' for the action '%2' is missing or of the wrong type. The media player '%1' cannot perform the action '%2'. The operation '%1' is unknown. Unknown error. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-06 03:09+0200
PO-Revision-Date: 2012-07-24 12:41+0200
Last-Translator: Lukáš Tinkl <lukas@kde.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 1.4
X-Language: cs_CZ
X-Source-Language: en_US
 Pokus o provedení akce '%1' selhal se zprávou '%2'. Další média Předchozí média Ovládání médií Přehrát/pozastavit média Zastavit přehrávání média Argument '%1' pro akci '%2' chybí nebo je nesprávného typu. Přehrávač médií '%1' nemůže provést činnost '%2'. Operace '%1' není známa. Neznámá chyba. 