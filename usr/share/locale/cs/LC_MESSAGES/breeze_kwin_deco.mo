��    @        Y         �     �     �     �  !   �  "   �  &   �  ,     #   <  &   `  !   �  &   �  '   �  "   �  "     '   >     f  +   j     �  
   �     �     �     �     �     �     �     �          (  !   /     Q     q      v     �     �     �     �     �  !   �     	  	   	     %	     -	     M	     h	  &   {	     �	     �	     �	     �	     �	     �	     �	     �	     
  $   
     <
     M
     g
     y
     �
     �
     �
  >   �
  �       �     �      �  	   �                  
   ,  
   7     B     H     X     e     l     r       6   �     �     �     �     �  
   �     
  	   %     /     6     I     c  &   j     �     �  $   �     �     �               "     >     Y     _     p  "   �     �     �  ,   �  %     	   '     1     L     S  
   Z     e     m     �  &   �     �     �     �     �          &     3     Q     %       +      4      6         2   "       #   :   *      @                         <       !              	   >      ;   5                              )      
   $                  .   8      9           &                  0                      ?       1   '      7          (         /       -          ,   =   3         ms % &Matching window property:  @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Border @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large @item:inlistbox Button size:Large @item:inlistbox Button size:Small @item:inlistbox Button size:Very Large Add Add handle to resize windows with no border Anima&tions duration: Animations B&utton size: Border size: Center Center (Full Width) Class:  Color: Decoration Options Detect Window Properties Dialog Draw a circle around close button Draw window background gradient Edit Edit Exception - Breeze Settings Enable animations Enable/disable this exception Exception Type General Hide window title bar Information about Selected Window Left Move Down Move Up New Exception - Breeze Settings Question - Breeze Settings Regular Expression Regular Expression syntax is incorrect Regular expression &to match:  Remove Remove selected exception? Right Shadows Si&ze: Tiny Tit&le alignment: Title:  Use window class (whole application) Use window title Warning - Breeze Settings Window Class Name Window Identification Window Property Selection Window Title Window-Specific Overrides strength of the shadow (from transparent to opaque)S&trength: Project-Id-Version: kwin_clients
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-09 03:20+0100
PO-Revision-Date: 2016-07-21 15:50+0100
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
  ms % Odpovídající vlastnost ok&na: Obrovská Velká Bez okrajů Bez postranních okrajů Normální Nadměrná Malá Velmi obrovská Velmi velká Velká Malá Velmi velká Přidat Přidat úchytku pro změnu velikosti oken bez okrajů Délka &animací: Animace Velikost &tlačítka: Velikost okraje: Uprostřed Na střed (plná šířka) Třída:  Barva: Možnosti dekorace Detekovat vlastnosti okna Dialog Kreslit kruh kolem tlačítka zavřít Vykreslit přechod pozadí okna Upravit Upravit výjimku - Nastavení Breeze Povolit animace Povolit/zakázat tuto výjimku Typ výjimky Obecné Skrýt titulkový pruh okna Informace o vybraném oknu Vlevo Přesunout dolů Přesunout nahoru Nová výjimka - Nastavení Breeze Otázka - Nastavení Breeze Regulární výraz Syntaxe regulárního výrazu je nesprávná Re&gulární výraz pro porovnání:  Odstranit Odebrat zvolenou výjimku? Vpravo Stíny Veliko&st: Drobná Zarovnání titu&lku: Název:  Použít třídu okna (celá aplikace) Použít titulek okna Varování - Nastavení Breeze Název třídy okna Identifikace okna Výběr vlastnosti okna Titulek okna Výjimky pro specifická okna Sí&la: 