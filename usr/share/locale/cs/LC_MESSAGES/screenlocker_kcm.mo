��          �            h     i  
   �     �  .   �     �     �     �      �  *         9  ,   Z  ,   �  0   �     �  �  �  "   �     �     �  &     
   +     6     K  #   ]  #   �      �     �     �  8   �                           
   	                                                       &Lock screen on resume: Activation Error Failed to successfully test the screen locker. Immediately Keyboard shortcut: Lock Session Lock screen automatically after: Lock screen when waking up from suspension Re&quire password after locking: Spinbox suffix. Short for minutes min  mins Spinbox suffix. Short for seconds sec  secs The global keyboard shortcut to lock the screen. Wallpaper &Type: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:10+0100
PO-Revision-Date: 2017-11-13 14:23+0100
Last-Translator: Vit Pelcak <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
X-Language: cs_CZ
X-Source-Language: en_US
 Zam&knout obrazovku po probuzení: Aktivace Chyba Otestování zámku obrazovky selhalo. Okamžitě Klávesová zkratka: Uzamknout sezení Automaticky uzamknout obrazovku po: Uzamknout obrazovku při probuzení V&yžadovat heslo po uzamčení:  min  min  min  sek  sek  sek Globální klávesová zkratka pro zamykání obrazovky. &Typ tapety: 