��    _                   C   	  /   M  -   }     �     �     �     �      	     	     1	     >	     J	  
   S	     ^	     g	     p	     �	  	   �	     �	     �	     �	  ,   �	  	    
     

  
   )
     4
     L
     `
     u
     �
     �
     �
     �
     �
  	   �
     �
     �
     
               !     (  	   ;     E     ]  
   r     }     �  
   �     �     �     �  
   �     �     �               $     2     @     R     h     y     �     �     �     �  	   �     �     �     �               4     Q     j     �     �     �     �  	   �     �  ,   �          !     0     @     L     S     b     t     �  #   �     �  �  �     �  	   �     �     �     �     �  "        =     O     W     `  	   u  	     
   �     �     �     �  	   �     �     �     �  4   �  
   0  &   ;     b     v     �     �     �     �     �     	     (     0  
   8  	   C     M     ]  	   d     n     �     �     �     �     �     �     �          &     9     U     \  
   p     {     �     �     �     �     �     �               1  )   E     o          �  	   �     �     �      �     �     �  %     %   4  &   Z     �     �     �  	   �     �     �  0   �     0     6     G     U     j     r     �     �      �  -   �     �         Q         Y   5           %       H       J   !              \   *       @   V   4   [       A   (   
       U   B   ]   ^       $   _         '   >       D   K      -                  ?      ,   O   0       R   8   6   W   2   I   =   X   E   #       N   C       T   G   M          3   "         9             +          S          F           ;   :                    &   <   L          P         7          .       /       1                     )            	   Z       @action opens a software center with the applicationManage '%1'... @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Desktop Add to Favorites Add to Panel (Widget) Align search results to bottom All Applications App name (Generic name)%1 (%2) Applications Apps & Docs Behavior Categories Computer Contacts Description (Name) Description only Documents Edit Application... Edit Applications... End session Expand search to bookmarks, files and emails Favorites Flatten menu to a single level Forget All Forget All Applications Forget All Contacts Forget All Documents Forget Application Forget Contact Forget Document Forget Recent Documents General Generic name (App name)%1 (%2) Hibernate Hide %1 Hide Application Icon: Lock Lock screen Logout Name (Description) Name only Often Used Applications Often Used Documents Often used On All Activities On The Current Activity Open with: Pin to Task Manager Places Power / Session Properties Reboot Recent Applications Recent Contacts Recent Documents Recently Used Recently used Removable Storage Remove from Favorites Restart computer Run Command... Run a command or a search query Save Session Search Search results Search... Searching for '%1' Session Show Contact Information... Show In Favorites Show applications as: Show often used applications Show often used contacts Show often used documents Show recent applications Show recent contacts Show recent documents Show: Shut Down Sort alphabetically Start a parallel session as a different user Suspend Suspend to RAM Suspend to disk Switch User System System actions Turn off computer Type to search. Unhide Applications in '%1' Unhide Applications in this Submenu Widgets Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2017-09-11 13:22+0100
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
X-Language: cs_CZ
X-Source-Language: en_US
 Spravovat '%1'... Vybrat... Vymazat ikonu Přidat na plochu Přidat do oblíbených Přidat do panelu (Widget) Zarovnat výsledky hledání dolů Všechny aplikace %1 (%2) Aplikace Aplikace a dokumenty Chování Kategorie Počítač Kontakty Popis (název) Pouze popis Dokumenty Upravit aplikaci... Upravit aplikace... Ukončit sezení Rozšířit hledání na záložky, soubory a emaily Oblíbené Zploštit nabídku na jedinou úroveň Zapomenout všechny Zapomenout všechny aplikace Zapomenout všechny kontakty Zapomenout všechny dokumenty Zapomenout aplikaci Zapomenout kontakt Zapomenout dokument Zapomenout nedávné dokumenty Obecné %1 (%2) Hibernovat Skrýt %1 Skrýt aplikaci Ikona: Uzamknout Zamknout obrazovku Odhlásit se Název (popis) Pouze název Často používané aplikace Často používané dokumenty Často používané Ve všech aktivitách V současné aktivitě Otevřít pomocí: Připnout do Správce úloh Místa Napájení / relace Vlastnosti Restartovat Nedávné aplikace Nedávné kontakty Nedávné dokumenty Nedávno použité Nedávno použité Odpojitelná úložiště Odstranit z oblíbených Restartovat počítač Spustit příkaz... Spustit příkaz nebo vyhledávací dotaz Uložit sezení Hledat Výsledky hledání Hledat... Vyhledávám %1' Sezení Zobrazit informace o kontaktu... Zobrazit v oblíbených Zobrazit aplikace jako: Zobrazit často používané aplikace Zobrazit často používané kontakty Zobrazit často používané dokumenty Zobrazit nedávné aplikace Zobrazit nedávné kontakty Zobrazit nedávné dokumenty Zobrazit: Vypnout Seřadit podle abecedy Spustit souběžné sezení jako jiný uživatel Uspat Uspat do paměti Uspat na disk Přepnout uživatele Systém Činnosti systému Vypnout počítač Text k vyhledání. Zobrazit skryté aplikace v '%1' Zobrazit skryté aplikace v této podnabídce Widgety 