��    5      �  G   l      �     �     �     �     �     �     �  !   �  '        >     M  -   _     �     �     �     �     �     �     �     �           
     +  	   3     =     N     [  
   i     t     �     �  A   �                     %     2     @     L  <   Z  <   �     �  3   �       �        �     �  ;   �     	     	     *	  %   6	  b   \	  �  �	     �     �     �     �  $   �  '        ;     V     p     �  /   �     �     �  
   �     �     
          7     F     \     h     w     �     �     �     �     �     �     �  %     N   (  
   w  
   �     �     �     �     �     �     �     �       ,        ;  �   W          %  Z   -     �     �     �  .   �  �   �             ,      &      3           0   #                 *      /      5       -                                       '   (   !   )   2   4             "          %                         .   +                    1                   
   	         $    %1 Hz &Disable All Outputs &Reconfigure (c), 2012-2013 Daniel Vrátil 90° Clockwise 90° Counterclockwise @title:windowDisable All Outputs @title:windowUnsupported Configuration Active profile Advanced Settings Are you sure you want to disable all outputs? Auto Break Unified Outputs Button Checkbox Combobox Configuration for displays Daniel Vrátil Display Configuration Display: EMAIL OF TRANSLATORSYour emails Enabled Group Box Identify outputs KCM Test App Laptop Screen Maintainer NAME OF TRANSLATORSYour names No Primary Output No available resolutions No kscreen backend found. Please check your kscreen installation. Normal Orientation: Primary display: Radio button Refresh rate: Resolution: Scale Display Scale multiplier, show everything at 1 times normal scale1x Scale multiplier, show everything at 2 times normal scale2x Scale: Scaling changes will come into effect after restart Screen Scaling Sorry, your configuration could not be applied.

Common reasons are that the overall screen size is too big, or you enabled more displays than supported by your GPU. Tab 1 Tab 2 Tip: Hold Ctrl while dragging a display to disable snapping Unified Outputs Unify Outputs Upside Down Warning: There are no active outputs! Your system only supports up to %1 active screen Your system only supports up to %1 active screens Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-24 03:18+0200
PO-Revision-Date: 2017-09-07 12:28+0100
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
X-Language: cs_CZ
X-Source-Language: en_US
 %1 Hz &Zakázat všechny výstupy Z&novu nastavit (c), 2012-2013 Daniel Vrátil 90° ve směru hodinových ručiček 90° proti směru hodinových ručiček Zakázat všechny výstupy Nepodporované nastavení Aktivní profil Pokročilá nastavení Opravdu si přejete zakázat všechny výstupy? Automaticky Rozdělit unifikované výstupy Tlačítko Přepínač Rozbalovací seznam Nastavení pro obrazovky Daniel Vrátil Nastavení zobrazení Zobrazení: vit@pelcak.org Povoleno Skupinový box Identifikovat výstupy Testovací aplikace KCM Obrazovka notebooku Správce Vít Pelčák Není primární výstup Nejsou dostupná žádná rozlišení Nenalezen žádný kscreen modul. Prosím zkontrolujte svou instalaci kscreen. Normální Orientace: Primární obrazovka: Přepínací tlačítko Obnovovací frekvence: Rozlišení: Změnit měřítko obrazovky 1x 2x Měřítko: Změna měřítka bude provedena po restartu Změna měřítka obrazovky Litujeme, ale vaše konfiguraci nelze použít.

Obvyklé důvody jsou, že celková velikost obrazovky je příliš velká nebo jste povolili více obrazovek, než vaše grafická karta podporuje. Karta 1 Karta 2 Tip: přidržením klávesy Ctrl během přetahování obrazovky zakážete přitahování Sjednocené výstupy Sjednotit výstupy Vzhůru nohama Varování: Nejsou žádné aktivní výstupy. Váš system podporuje pouze až 1 aktivní obrazovku Váš system podporuje pouze až %1 aktivní obrazovky Váš system podporuje pouze až %1 aktivních obrazovek 