��    ^           �      �     �               !     '     .     :     K     Y     a  /   i  -   �     �     �  
   �  
   �     �     �     	     	     	     "	     /	     4	  
   <	     G	     S	     `	     o	  	   �	     �	  	   �	     �	     �	     �	     �	     �	  	   �	     �	     
     
  	   
     '
     -
     3
     8
     =
  	   F
     P
     ^
     e
     l
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
  
             *     <     K     _  )   m     �     �     �     �     �     �     �     �     �  
               	   "     ,     3     8     R     [     m     }  *   �  �  �     �     �     �  
   �     �     �     �     �            	   "     ,     :     C     K     Y     g     w     }     �     �     �     �  	   �  	   �     �     �     �          "     .     F     R     a     g     x     �     �     �     �  	   �     �     �     �     �     �     �            	   #  	   -     7     U     \     e     h     {     �  
   �     �     �     �     �     �     �     �     �            "   ;     ^  0   o     �  #   �     �     �     �  
   �  
     	             .     <     B     J     \     e     i     �     �     �     �     �         3   J   6           7   M          -   K               G       ,      I             N       /   @   P           	   8   +   <       (       =   A       5         ]   Q   )      1   C          E   H       4   W   :                &   %          2                  #      [   9   S   
              T       \   *   0      ?      Y   B   .   U       L   ;         X                '         ^      R   "   O       F   >           !                  V      Z   $   D           &Delete &Empty Trash Bin &Move to Trash &Open &Paste &Properties &Refresh Desktop &Refresh View &Reload &Rename @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Align Appearance: Arrange In Arrange in Arrangement: Back Cancel Columns Configure Desktop Custom title Date Default Descending Description Deselect All Desktop Layout Enter custom title here Features: File name pattern: File type File types: Filter Folder preview popups Folders First Folders first Full path Got it Hide Files Matching Huge Icon Size Icons Large Left List Location Location: Lock in place Locked Medium More Preview Options... Name None OK Panel button: Preview Plugins Preview thumbnails Remove Resize Restore from trashRestore Right Rotate Rows Search file type... Select All Select Folder Selection markers Show All Files Show Files Matching Show a place: Show files linked to the current activity Show the Desktop folder Show the desktop toolbox Size Small Small Medium Sort By Sort by Sorting: Specify a folder: Text lines Tiny Title: Tool tips Tweaks Type Type a path or a URL here Unsorted Use a custom icon Widget Handling Widgets unlocked whether to use icon or list viewView mode Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:11+0100
PO-Revision-Date: 2017-11-02 13:07+0100
Last-Translator: Vit Pelcak <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
X-Language: cs_CZ
X-Source-Language: en_US
 &Smazat V&yprázdnit koš Pře&sunout do koše &Otevřít V&ložit &Vlastnosti O&bnovit plochu Ob&novit pohled Znovu načís&t &Přejmenovat Vybrat... Vymazat ikonu Zarovnat Vzhled: Uspořádat v Uspořádat v Uspořádání: Zpět Zrušit Sloupce Nastavte pracovní plochu Vlastní titulek Datum Výchozí Sestupně Popis Zrušit výběr Rozvržení pracovní plochy Zde zadejte vlastní titulek Vlastnosti: Vzorek názvu souborů: Typ souboru Typy souborů: Filtr Náhledy složky Složky první Složky první Celá cesta Jasně Skrýt soubory odpovídající Obrovská Velikost ikon Ikony Velká Vlevo Seznam Místo Umístění: Uzamknout na místě Uzamčeno Střední Více možností náhledů... Název Žádný OK Tlačítko panelu: Moduly pro náhledy Náhled miniatur +Odstranit +Změnit velikost Obnovit Vpravo +Otočit Řádky Hledat typ souboru... Vybrat vše Vybrat složku Značky výběru Zobrazovat všechny soubory Zobrazovat soubory odpovídající Zobrazit místo: Zobrazit soubory spojené s aktuální aktivitou Zobrazit složku plochy Zobrazit nástrojovou lištu plochy Velikost Malá Menší střední Řadit dle Řadit dle Řazení: Zadejte složku: Řádky textu Malá Název: Nástrojové tipy Ladění Typ Zde zadejte cestu nebo URL Neseřazeno Použít vlastní ikonu Zacházení s widgetem Widgety odemčeny Režim zobrazení 