��    )      d  ;   �      �     �     �  )   �     �     	          8     N  #   n     �  
   �     �     �  %   �  +   	     5     J     ]  @   j     �     �     �     �     �                    )     /     O     U  .   ]     �  (   �  	   �  	   �  	   �  '   �  7     "   N  �  q     %	     5	     I	     i	     r	  
   z	  	   �	     �	     �	     �	     �	     �	     �	      
  ,   $
     Q
     p
     �
  J   �
     �
     �
     �
           *     K     S     Y     k     s     �  	   �  /   �  $   �  (   �     !     )     7     >     C     a                   #                                 $                      )                                               	       (   "             %           
                        &             '   !    &Do not remember @actionWalk through activities @actionWalk through activities (Reverse) @action:buttonApply @action:buttonCancel @action:buttonChange... @action:buttonCreate @title:windowActivity Settings @title:windowCreate a New Activity @title:windowDelete Activity Activities Activity information Activity switching Are you sure you want to delete '%1'? Blacklist all applications not on this list Clear recent history Create activity... Description: Error loading the QML files. Check your installation.
Missing %1 For a&ll applications Forget a day Forget everything Forget the last hour Forget the last two hours General Icon Keep history Name: O&nly for specific applications Other Privacy Private - do not track usage for this activity Remember opened documents: Shortcut for switching to this activity: Shortcuts Switching Wallpaper for in 'keep history for 5 months'for  unit of time. months to keep the history month  months unlimited number of monthsforever Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2017-02-24 09:41+0100
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
 &Nepamatovat si Procházet aktivity Procházet aktivity (pozpátku) Použít Zrušit Změnit... Vytvořit Nastavení aktivity Vytvořit novou aktivitu Smazat aktivitu Aktivity Informace o aktivitách Přepínání aktivit Opravdu si přejete smazat '%1'? zakázat všechny aplikace mimo tento seznam Vyprázdnit nedávnou historii Vytvořit aktivitu... Popis: Chyba při načítání souborů QML. Zkontrolujte si instalaci. Chybí %1 Pro všechny ap&likace Zapomenout den Zapomenout vše Zapomenout poslední hodinu Zapomenout poslední dvě hodiny Obecné Ikona Ponechat historii Název: Pouze pro ko&nkrétní aplikace Jiné Soukromí Soukromé - nesledovat použití této aktivity Zapamatovat si otevřené dokumenty: Zkratka pro přepnutí na tuto aktivitu: Zkratky Přepínání Tapeta pro   měsíc  měsíce  měsíců pořád 