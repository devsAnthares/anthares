��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  (  K     t  !   z  w  �  M  	  k   b
     �
     �
     �
  '        7     P  !   k     �                                          
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2017-09-28 17:58+0200
Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>
Language-Team: Serbian <kde-i18n-sr@kde.org>
Language: sr@latin
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
  sek. &Prekovreme odziva na pokretanju: <qt><h1>Obaveštenje u traci zadataka</h1>Možete odrediti i dodatni način za obaveštavanje o pokretanju, gde se u traci zadataka javlja dugme sa rotirajućim peščanikom, simbolično ukazujući na učitavanje programa. Neki programi mogu ne biti svesni ovakvog obaveštavanja, i tada će dugme nestati posle vremena datog pod <i>Prekovreme odziva na pokretanju:</i>.</qt> <qt><h1>Zauzet pokazivač</h1>KDE nudi zauzet pokazivač za obaveštavanje o pokretanju programa. Da biste ga uključili, izaberite jedan od pokazivača sa spiska. Neki programi mogu ne biti svesni ovakvog obaveštavanja, i tada će pokazivač prestati da trepće posle vremena datog pod <i>Prekovreme odziva na pokretanju:</i>.</qt> <h1>Odziv pri pokretanju</h1>Ovde podešavate odziv na osnovu koga uočavate da je program upravo pokrenut. Trepćući pokazivač Skakutajući pokazivač &Zauzet pokazivač Uključi obaveštenje u &traci zadataka Bez zauzetog pokazivača Pasivni zauzeti pokazivač Preko&vreme odziva na pokretanju: &Obaveštenje u traci zadataka 