��          �      �       H     I     a      m     �     �     �  
   �     �  &   �          .     L  1   b  P  �     �     �               3     J     X     g  '   s     �     �     �  /   �         	                                    
                    Configure Desktop Theme David Rosca EMAIL OF TRANSLATORSYour emails Get New Themes... Install from File... NAME OF TRANSLATORSYour names Open Theme Remove Theme Theme Files (*.zip *.tar.gz *.tar.bz2) Theme installation failed. Theme installed successfully. Theme removal failed. This module lets you configure the desktop theme. Project-Id-Version: kcm_desktoptheme
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-22 05:21+0100
PO-Revision-Date: 2017-10-30 23:08+0100
Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>
Language-Team: Serbian <kde-i18n-sr@kde.org>
Language: sr@latin
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Associated-UI-Catalogs: plasma
X-Environment: kde
 Podešavanje teme površi David Rosca caslav.ilic@gmx.net Dobavi nove teme... Instaliraj iz fajla... Časlav Ilić Otvaranje teme Ukloni temu fajlovi tema (*.zip *.tar.gz *.tar.bz2) Instaliranje teme propalo. Tema uspešno instalirana. Uklanjanje teme propalo. U ovom modulu možete da podesite temu površi. 