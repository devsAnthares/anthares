��          �      <      �     �     �     �     �     �  '   �  >        L     f     �     �     �  )   �  7   �  '        B     T  H  s     �     �     �     �     �     �  
             +  !   K     m     �  %   �     �     �     �     �                                       
                      	                                        Current user General Layout Lock Screen New Session Nobody logged in on that sessionUnused Show a dialog with options to logout/shutdown/restartLeave... Show both avatar and name Show full name (if available) Show login username Show only avatar Show only name Show technical information about sessions User logged in on console (X display number)on %1 (%2) User logged in on console numberTTY %1 User name display You are logged in as <b>%1</b> Project-Id-Version: plasma_applet_org.kde.plasma.userswitcher
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-11 03:18+0100
PO-Revision-Date: 2016-04-15 23:42+0200
Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>
Language-Team: Serbian <kde-i18n-sr@kde.org>
Language: sr@latin
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
 Tekući korisnik Opšte Raspored Zaključaj ekran Nova sesija nekorišćeno Napusti... Prikaži i avatar i ime Prikaži celo ime (ako je dato) Prikaži prijavno korisničko ime Prikaži samo avatar Prikaži samo ime Prikaži tehničke podatke o sesijama na %1 %2|/|na $[lok %1] %2 TTY %1 Prikaz korisničkog imena Prijavljeni ste kao <b>%1</b> 