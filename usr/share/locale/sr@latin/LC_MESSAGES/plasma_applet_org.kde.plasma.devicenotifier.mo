��          �      l      �  3   �  $     �   :     �  4   �     �  #        =  �   E  �   �  +   �     �     �     �  1     (   3     \  �   s  $   �  (     J  F  d   �     �     	     	  8   	  #   U	  +   y	     �	  �   �	  �   >
  +        ;     M     e       ,   �     �     �  +   �  .                                                      	   
                                            1 action for this device %1 actions for this device @info:status Free disk space%1 free Accessing is a less technical word for Mounting; translation should be short and mean 'Currently mounting this device'Accessing... All devices Click to access this device from other applications. Click to eject this disc. Click to safely remove this device. General It is currently <b>not safe</b> to remove this device: applications may be accessing it. Click the eject button to safely remove this device. It is currently <b>not safe</b> to remove this device: applications may be accessing other volumes on this device. Click the eject button on these other volumes to safely remove this device. It is currently safe to remove this device. Most Recent Device No Devices Available Non-removable devices only Open auto mounter kcmConfigure Removable Devices Open popup when new device is plugged in Removable devices only Removing is a less technical word for Unmounting; translation shoud be short and mean 'Currently unmounting this device'Removing... This device is currently accessible. This device is not currently accessible. Project-Id-Version: plasma_applet_org.kde.plasma.devicenotifier
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2016-07-10 19:45+0200
Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>
Language-Team: Serbian <kde-i18n-sr@kde.org>
Language: sr@latin
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
 %1 radnja za ovaj uređaj %1 radnje za ovaj uređaj %1 radnji za ovaj uređaj radnja za ovaj uređaj %1 slobodno Pristupam... sve uređaje Kliknite da pristupite ovom uređaju iz drugih programa. Kliknite za izbacivanje ovog diska. Kliknite da bezbedno uklonite ovaj uređaj. Opšte Trenutno <b>nije bezbedno</b> ukloniti ovaj uređaj, jer mu programi možda pristupaju. Kliknite na dugme za izbacivanje da ga bezbedno uklonite. Trenutno <b>nije bezbedno</b> ukloniti ovaj uređaj, jer programi možda pristupaju drugim skladištima na njemu. Kliknite na dugme za izbacivanje na tim drugim skladištima da bezbedno uklonite ovaj uređaj. Trenutno je bezbedno ukloniti ovaj uređaj. Poslednji uređaj Nema dostupnih uređaja samo neuklonjive uređaje Podesi uklonjive uređaje Prikaži iskakač kad se utakne novi uređaj samo uklonjive uređaje Uklanjam... Ovom uređaju se trenutno može pristupiti. Ovom uređaju se trenutno ne može pristupiti. 