��          �      <      �     �     �     �  +   �  @        L     a     i     w     }     �  
   �     �     �     �     �     �  F  
     Q     e     m  -   �  =   �     �               "  
   )     4  
   G     R     Y     n     �     �     
         	                                                                                       <a href='%1'>%1</a> Close Copy Automatically: Don't show this dialog, copy automatically. Drop text or an image onto me to upload it to an online service. Error during upload. General History Size: Paste Please wait Please, try again. Sending... Share Shares for '%1' Successfully uploaded The URL was just shared Upload %1 to an online service Project-Id-Version: plasma_applet_org.kde.plasma.quickshare
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-01 04:04+0100
PO-Revision-Date: 2015-05-01 17:25+0200
Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>
Language-Team: Serbian <kde-i18n-sr@kde.org>
Language: sr@latin
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
 <a href='%1'>%1</a> Zatvori Kopiraj automatski: Ne prikazuj ovaj dijalog, kopiraj automatski. Prevucite ovde tekst ili sliku za otpremanje servisu na vezi. Greška pri otpremanju. Opšte Veličina istorijata: Nalepi Sačekajte Pokušajte ponovo. Šaljem... Podeli Deljenja za „%1“ Uspešno otpremljeno URL je upravo podeljen Otpremi %1 servisu na vezi 