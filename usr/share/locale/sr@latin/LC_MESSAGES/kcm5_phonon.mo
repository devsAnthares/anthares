��    B      ,  Y   <      �  i   �  m        y  b   �     �     	  6        O  7   _     �     �  	   �     �  (   �  )   �  )   (     R     o  Y   u     �     �  �   �      y	  .   �	     �	  
   �	     �	     �	     
     
     !
     5
     B
     J
     c
     r
     w
     �
     �
     �
     �
     �
  	   �
  
   �
     �
     �
  	     
     
   *     5     B  	   `     j     o  
   �  �   �     (  8   8  �   q     �  8   	  ,   B  ,   o  %   �     �  *  �  M     i   V     �  b   �     A     ]  ;   m     �  :   �     �     �            2   '  1   Z  1   �  $   �  	   �  ]   �     K     a  w   t     �  7         8     J     W     n     |     �     �     �     �     �     �     �     �  P   �     N  	   ]     g     n          �     �     �     �     �     �     �  #   �          "     (     B  y   P     �  A   �  |        �  :   �  +   �  +     +   :     f     $   !   %          ;   ,          B                 8   #                 7   9       +              )   ?   -   <         6                                    *   2       >                1       @       .   5   3   :   A         &      /                4              0      "             =   (   '         	   
           @info User changed Phonon backendTo apply the backend change you will have to log out and back in again. A list of Phonon Backends found on your system.  The order here determines the order Phonon will use them in. Apply Device List To... Apply the currently shown device preference list to the following other audio playback categories: Audio Hardware Setup Audio Playback Audio Playback Device Preference for the '%1' Category Audio Recording Audio Recording Device Preference for the '%1' Category Backend Colin Guthrie Connector Copyright 2006 Matthias Kretz Default Audio Playback Device Preference Default Audio Recording Device Preference Default Video Recording Device Preference Default/Unspecified Category Defer Defines the default ordering of devices which can be overridden by individual categories. Device Configuration Device Preference Devices found on your system, suitable for the selected category.  Choose the device that you wish to be used by the applications. EMAIL OF TRANSLATORSYour emails Failed to set the selected audio output device Front Center Front Left Front Left of Center Front Right Front Right of Center Hardware Independent Devices Input Levels Invalid KDE Audio Hardware Setup Matthias Kretz Mono NAME OF TRANSLATORSYour names Phonon Configuration Module Playback (%1) Prefer Profile Rear Center Rear Left Rear Right Recording (%1) Show advanced devices Side Left Side Right Sound Card Sound Device Speaker Placement and Testing Subwoofer Test Test the selected device Testing %1 The order determines the preference of the devices. If for some reason the first device cannot be used Phonon will try to use the second, and so on. Unknown Channel Use the currently shown device list for more categories. Various categories of media use cases.  For each category, you may choose what device you prefer to be used by the Phonon applications. Video Recording Video Recording Device Preference for the '%1' Category  Your backend may not support audio recording Your backend may not support video recording no preference for the selected device prefer the selected device Project-Id-Version: kcm5_phonon
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2017-09-28 17:58+0200
Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>
Language-Team: Serbian <kde-i18n-sr@kde.org>
Language: sr@latin
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
 Da bi izmena pozadine stupila na snagu, moraćete da se odjavite i prijavite. Spisak pozadina znanih Phononu na sistemu. Phonon će pokušati da ih upotrebi ovde navedenim redosledom. Primeni spisak uređaja na... Primeni trenutno prikazan spisak poželjnih uređaja na sledeće druge kategorije audio puštanja: Podešavanje audio hardvera Audio puštanje Poželjni uređaj za audio puštanje za kategoriju „%1“ Audio snimanje Poželjni uređaj za audio snimanje za kategoriju „%1“ Pozadina Kolin Gatri Konektor © 2006, Matijas Krec Poželjni podrazumevani uređaj za audio puštanje Poželjni podrazumevani uređaj za audio snimanje Poželjni podrazumevani uređaj za video snimanje podrazumevana/neodređena kategorija Neželjen Određuje podrazumevani redosled uređaja, koji se može potisnuti po pojedinim kategorijama. Podešavanje uređaja Poželjni uređaji Uređaji nađeni na sistemu koji su podesni za izabranu kategoriju. Izaberite uređaj koji želite da koriste programi. caslav.ilic@gmx.net Neuspelo postavljanje izabranog uređaja za audio izlaz Prednji centralni Prednji levi Prednji levo od centra Prednji desni Prednji desno od centra Hardver Nezavisni uređaji Nivoi ulaza Loše Podešavanje audio hardvera Matijas Krec Mono Časlav Ilić Modul za podešavanje Phonona|/|$[svojstva dat 'Modulu za podešavanje Phonona'] Puštanje (%1) Poželjan Profil Zadnji centralni Zadnji levi Zadnji desni Snimanje (%1) Napredni uređaji Bočni levi Bočni desni Zvučna kartica Zvučni uređaj Razmeštaj zvučnika i isprobavanje Bas zvučnik Proba Isprobaj izabrani uređaj Isprobavam %1 Redosled određuje poželjnost uređaja. Ako iz nekog razlog Phonon ne može da upotrebi prvi, pokušaće sa drugim, itd. Nepoznat kanal Primenite trenutno prikazani spisak uređaja na više kategorija. Razne kategorije upotrebe medijuma. Za svaku od njih možete odabrati uređaj koji želite da koriste programi nad Phononom. Video snimanje Poželjni uređaj za video snimanje za kategoriju „%1“ Pozadina možda ne podržava audio snimanje Pozadina možda ne podržava video snimanje izabrani uređaj nema određenu poželjnost izabrani uređaj je poželjan 