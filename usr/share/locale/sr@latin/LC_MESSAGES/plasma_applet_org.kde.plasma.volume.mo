��    &      L  5   |      P     Q  8   S     �     �     �     �     �     �  3   �  >        N     i     y     �  m   �     �          "     2     7     ?  *   O      z     �     �  "   �     �     �          3     :     H     X     e     �  ;   �     �  B  �     5	     7	     L	     U	     b	  
   p	     {	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
     "
     4
  
   <
     G
  *   X
  /   �
     �
     �
     �
     �
     �
          "     *     6     D     S     [     b     g        #   %                                                              "          $                      
                                           &      !                	        % Accessibility data on volume sliderAdjust volume for %1 Applications Audio Muted Audio Volume Behavior Capture Devices Capture Streams Checkable switch for (un-)muting sound output.Mute Checkable switch to change the current default output.Default Decrease Microphone Volume Decrease Volume Devices General Heading for a list of ports of a device (for example built-in laptop speakers or a plug for headphones)Ports Increase Microphone Volume Increase Volume Maximum volume: Mute Mute %1 Mute Microphone No applications playing or recording audio No output or input devices found Playback Devices Playback Streams Port is unavailable (unavailable) Port is unplugged (unplugged) Raise maximum volume Show additional options for %1 Volume Volume at %1% Volume feedback Volume step: label of device items%1 (%2) label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: plasma_applet_org.kde.plasma.volume
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-26 05:50+0100
PO-Revision-Date: 2017-09-25 19:53+0200
Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>
Language-Team: Serbian <kde-i18n-sr@kde.org>
Language: sr@latin
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
 % Podesi jačinu za %1 Programi Zvuk utišan Jačina zvuka Ponašanje Uređaji za snimanje Tokovi snimanja Utišaj Podrazumevano Smanji jačinu mikrofona Smanji jačinu Uređaji Opšte Portovi Povećaj jačinu mikrofona Povećaj jačinu Najveća jačina: Utišaj Utišaj %1 Utišaj mikrofon Nijedan program ne pušta niti snima zvuk. Nije nađen nijedan izlazni ili ulazni uređaj. Uređaji za puštanje Tokovi puštanja  (nedostupan)  (izvučen) Podigni najveću jačinu Dodatne opcije za %1 Jačina Jačina %1% Odziv jačine Korak jačine: %1 (%2) %1: %2 100% %1% 