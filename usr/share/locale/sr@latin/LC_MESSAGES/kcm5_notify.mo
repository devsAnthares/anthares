��          �      �       0     1  �  H     �       &         ?     `     n     v     �     �  (   �  *  �       |  +     �     �  &   �     �               %     3     @  i   R         
                 	                                (c) 2002-2006 KDE Team <h1>System Notifications</h1>Plasma allows for a great deal of control over how you will be notified when certain events occur. There are several choices as to how you are notified:<ul><li>As the application was originally designed.</li><li>With a beep or other noise.</li><li>Via a popup dialog box with additional information.</li><li>By recording the event in a logfile without any additional visual or audible alert.</li></ul> Carsten Pfeiffer Charles Samuels Disable sounds for all of these events EMAIL OF TRANSLATORSYour emails Event source: KNotify NAME OF TRANSLATORSYour names Olivier Goffart Original implementation System Notification Control Panel Module Project-Id-Version: kcm5_notify
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-09 07:18+0200
PO-Revision-Date: 2017-08-21 22:34+0200
Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>
Language-Team: Serbian <kde-i18n-sr@kde.org>
Language: sr@latin
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
 © 2002-2006, tim KDE‑a <h1>Sistemska obaveštenja</h1><p>Plasma pruža široku kontrolu nad načinom obaveštavanja pri raznim događajima. Postoji nekoliko vidova obaveštavanja:</p><ul><li>kako je program sam odredio,</li><li>bip ili drugi zvučni signal,</li><li>iskačući dijalog sa dopunskim podacima,</li><li>beleženje događa u dnevnički fajl, bez vizuelnih ili zvučnih pokazatelja.</li></ul> Karsten Pfajfer Čarls Semjuels Isključi zvukove za sve ove događaje caslav.ilic@gmx.net Izvor događaja: K‑obaveštenja Časlav Ilić Olivje Gofar prvobitna izvedba Kontrolni modul za sistemska obaveštenja|/|$[svojstva dat 'Kontrolnom modulu za sistemska obaveštenja'] 