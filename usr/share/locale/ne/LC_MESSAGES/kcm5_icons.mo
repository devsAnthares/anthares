��    .      �  =   �      �     �     �               $     +  �   F  -   �  r      	   s  	   }     �     �     �     �  
   �     �     �      �               $     1     7     R     _     ~  	   �     �     �     �     �     �     �     �          
  +        B     J     X     `  S   m  )   �     �  �  �     �	     �	     �	  %   �	     
  B   '
  ?  j
  a   �  �        �     	  D   )     n  %   v     �  (   �     �  }   �  +   l  (   �     �     �     �  Z     .   _  3   �  	   �  "   �     �  !     =   $     b  g   �     �  z   j     �  "   �  |        �     �     �  %   �  �     {   �  J   j                             .          #   )           *      +   -              '       ,   !   &                        $                    
   	             "                           %      (                    &Amount: &Effect: &Second color: &Semi-transparent &Theme (c) 2000-2003 Geert Jansen <qt>Are you sure you want to remove the <strong>%1</strong> icon theme?<br /><br />This will delete the files installed by this theme.</qt> <qt>Installing <strong>%1</strong> theme</qt> A problem occurred during the installation process; however, most of the themes in the archive have been installed Ad&vanced All Icons Antonio Larrosa Jimenez Co&lor: Colorize Confirmation Desaturate Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Effect Parameters Gamma Geert Jansen Icons Icons Control Panel Module Main Toolbar NAME OF TRANSLATORSYour names Name No Effect Panel Preview Remove Theme Set Effect... Setup Active Icon Effect Setup Default Icon Effect Setup Disabled Icon Effect Size: Small Icons The file is not a valid icon theme archive. To Gray To Monochrome Toolbar Torsten Rahn Unable to download the icon theme archive;
please check that address %1 is correct. Unable to find the icon theme archive %1. Use of Icon Project-Id-Version: kcmicons
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-08 06:27+0100
PO-Revision-Date: 2007-11-04 18:53+0545
Last-Translator: Shyam Krishna Bal <shyamkrishna_bal@yahoo.com>
Language-Team: Nepali <info@mpp.org.np>
Language: ne
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n !=1

X-Generator: KBabel 1.11.4
 मात्रा: प्रभाव: दोस्रो रङ: अर्ध-पारदर्शी विषयवस्तु (सी) २०००-२००३ जर्ट जानसेन <qt>के तपाईँ <strong>%1</strong> प्रतिमा विषयवस्तु हटाउँन चाहनुहुन्छ? <br /><br />यो विषयवस्तुद्वारा स्थापना गरिएका फाइलहरू यसले मेट्नेछ ।</qt> <qt>विषयवस्तु <strong>%1</strong> स्थापना गर्दैछ</qt> सङ्ग्रहमा धेरैजसो विषयवस्तु स्थापन भएता पनि; स्थापना प्रक्रियामा एउटा समस्या देखापर्यो उन्नत सबै प्रतिमा एन्टोनियो लारोसा जिमेनेज रङ: रङ लगाउनुहोस् स्वीकृति अतितृप्त विहिन वर्णन विषयवस्तु यूआरएल तान्नुहोस् वा टाइप गर्नुहोस् nabin@mpp.org.np,shyamkrishna_bal@yahoo.com प्रभाव परिमिति गामा जर्ट जानसेन प्रतिमा प्रतिमा नियन्त्रण प्यानल मोड्युल मुख्य उपकरणपट्टी Nabin gautam, श्यामकृष्ण बल नाम प्रभाव विहिन प्यानल पूर्वावलोकन विषयवस्तु हटाउँनुहोस् सेट प्रभाव... सक्रिय प्रतिमा प्रभाव सेटअप गर्नुहोस् पूर्वनिर्धारित प्रतिमा प्रभाव सेटअप गर्नुहोस् अक्षम पारिएका प्रतिमा प्रभाव सेटअप गर्नुहोस् साइज: साना प्रतिमा फाइल एउटा वैध प्रतिमा विषयवस्तु सङ्ग्रह होइन । ग्रेमा मोनोक्रोम उपकरणपट्टी टोर्सटेन राहन प्रतिमा विषय वस्तु सङ्ग्रह डाउनलोड गर्न अक्षम;
कृपया %1 ठेगाना ठीक भएको जाँच गर्नुहोस् । प्रतिमा विषयवस्तु सङ्ग्रह %1 फेला पार्न अक्षम । प्रतिमाको प्रयोग गर्नुहोस् 