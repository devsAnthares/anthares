��          �   %   �      `  &   a  �   �                    (     6     C     T      d  	   �  �   �  c   X     �     �     �     �               '     3     9  	   ?  C   I  j   �     �  �    h   �  e  "  	   �	     �	     �	  4   �	  4   �	  1   2
     d
  !   x
     �
    �
  J  �  2        @  2   `  =   �  =   �       %   ,     R     f     z  �   �  �   H  @   -                                                                         
                  	                                 (c) 2002 Karol Szwed, Daniel Molkentin <h1>Style</h1>This module allows you to modify the visual appearance of user interface elements, such as the widget style and effects. Button Checkbox Combobox Con&figure... Configure %1 Daniel Molkentin Description: %1 EMAIL OF TRANSLATORSYour emails Group Box Here you can choose from a list of predefined widget styles (e.g. the way buttons are drawn) which may or may not be combined with a theme (additional information like a marble texture or a gradient). If you enable this option, KDE Applications will show small icons alongside some important buttons. KDE Style Module Karol Szwed NAME OF TRANSLATORSYour names No description available. Preview Radio button Ralf Nolden Tab 1 Tab 2 Text Only There was an error loading the configuration dialog for this style. This area shows a preview of the currently selected style without having to apply it to the whole desktop. Unable to Load Dialog Project-Id-Version: kcmstyle
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-17 05:44+0100
PO-Revision-Date: 2007-11-01 19:58+0545
Last-Translator: Shyam Krishna Bal <shyamkrishna_bal@yahoo.com>
Language-Team: Nepali <info@mpp.org.np>
Language: ne
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n !=1

X-Generator: KBabel 1.11.4
 (सी) २००२ कारोल सेज्ड, डानियल मोल्कानटिन <h1>शैली</h1>यो मोड्युलले तपाईँलाई प्रयोगकर्ता इन्टरफेस तत्वहरूको वास्तविक दृश्य परिमार्जन गर्न अनुमति दिन्छ, जस्तै विजेट शैली र प्रभाव । बटन जाँच बाकस कम्बोबाकस कन्फिगर गर्नुहोस्... %1 कन्फिगर गर्नुहोस् डानियल मोलकेन्टिन वर्णन: %1 nabin@mpp.org.np,shyam@mpp.org.np समूह बाकस यहाँ तपाईँले सूचीबाट पूर्व परिभाषित विजेट शैलीहरू रोज्न सक्नुहुन्छ (जस्तै बाटोका बटनहरू कोरिएका छन्) जुन विषयवस्तुद्वारा संयोजन गर्न सकिनेछ वा सकिनेछैन (थप जानकारी जस्तै कडा बनावट वा ग्रेडियन्ट ) । यदि तपाईँले यो विकल्प सक्षम पार्नु भयो भने, केडीई अनुप्रयोगहरूले केही महत्वपूर्ण बटनहरू वरिपरि साना प्रतिमाहरू देखाउँनेछ । केडीई शैली मोड्युल कारोल सेज्ड Nabin Gautam,श्यामकृष्ण बल कुनै वर्णन उपलब्ध छैन । पूर्वावलोकन गर्नुहोस् रेडियो बटन राल्फ नोल्डेन ट्याब १ ट्याब २ पाठ मात्र त्यहाँ यो शैलीका लागि कन्फिगरेसन संवाद लोड गर्दा एउटा त्रुटि थियो । पूरै डेस्कटपमा लागू नगरीकन यो क्षेत्रले हालै चयन गरिएको शैलीको पूर्वावलोकन देखाउँछ । संवाद लोड गर्न अक्षम भयो 