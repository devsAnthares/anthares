��          �            h  �   i  m   �  `   Y     �     �     �     �           #     B     G  ?   X  Y   �  +   �  �    J  �  *    W  ?     �	  H   �	     �	  }   	
  +   �
  3   �
  	   �
  Q   �
  �   C  �   �  u   �         	                                                  
                     <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails NAME OF TRANSLATORSYour names Name Overwrite Theme? The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. Project-Id-Version: kcminput
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2007-11-04 18:50+0545
Last-Translator: Shyam Krishna Bal <shyamkrishna_bal@yahoo.com>
Language-Team: Nepali <info@mpp.org.np>
Language: ne
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n !=1

X-Generator: KBabel 1.11.4
 <qt>के तपाईँ <strong>%1</strong> कर्सर विषयवस्तु हटाउन निश्चिन्त हुनुहुन्छ?<br /> यसले यस विषयवस्तुद्वारा स्थापना गरिएका सबै फाइल मेट्नेछ ।</qt> <qt>तपाईँ हाल प्रयोग गरिरहेको विषयवस्तु मेट्न सक्नुहुन्न ।<br />तपाईँले पहिले अर्को विषयवस्तु स्विच गर्नुपर्नेछ ।</qt> %1 नाम गरिएको विषयवस्तु तपाईँको प्रतिमा विषयवस्तु फोल्डरमा पहिले नै अवस्थित छ । के तपाईँ यसलाई योसँग प्रतिस्थापन गर्न चाहनुहुन्छ? स्वीकृति कर्सर सेटिङ परिवर्तन गरियो वर्णन विषयवस्तु यूआरएल तान्नुहोस् वा टाइप गर्नुहोस् nabin@mpp.org.np,shyamkrishna_bal@yahoo.com Nabin Gautam, श्यामकृष्ण बल नाम विषयवस्तु अधिलेखन गर्नुहुन्छ? %1 फाइल वैध कर्सर विषयवस्तु सङ्ग्रह हो जस्तो देखिदैन । कर्सर विषयवस्तु सङ्ग्रह डाउनलोड गर्न अक्षम; कृपया %1 ठेगाना ठीक भएको जाँच गर्नुहोस् । कर्सर विषयवस्तु सङ्ग्रह %1 फेला पार्न अक्षम । 