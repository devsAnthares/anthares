��          �      L      �  m   �     /  "   <  F   _  F   �  F   �  J   4  N     R   �     !  %   2  $   X  #   }  $   �  %   �  &   �  �        �  �  �     _     y  -   �  R   �  R     U   ^  S   �  T   	  P   ]	     �	     �	  	   �	     �	     �	     �	  
    
     
     %
                                
                          	                                          Close the encrypted container; partitions inside will disappear as they had been unpluggedLock the container Eject medium Finds devices whose name match :q: Lists all devices and allows them to be mounted, unmounted or ejected. Lists all devices which can be ejected, and allows them to be ejected. Lists all devices which can be mounted, and allows them to be mounted. Lists all devices which can be unmounted, and allows them to be unmounted. Lists all encrypted devices which can be locked, and allows them to be locked. Lists all encrypted devices which can be unlocked, and allows them to be unlocked. Mount the device Note this is a KRunner keyworddevice Note this is a KRunner keywordeject Note this is a KRunner keywordlock Note this is a KRunner keywordmount Note this is a KRunner keywordunlock Note this is a KRunner keywordunmount Unlock the encrypted container; will ask for a password; partitions inside will appear as they had been plugged inUnlock the container Unmount the device Project-Id-Version: plasma_runner_solid
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-11-06 14:03+0100
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.1
Plural-Forms: nplurals=2; plural=n != 1;
 De container vergrendelen Medium uitwerpen Zoekt naar apparaten die overeenkomen met :q: Alle apparaten laten zien en toestaan deze aan of af te koppelen of uit te werpen. Alle apparaten tonen die uitgeworpen kunnen worden en deze toestaan uit te werpen. Alle apparaten tonen die aangekoppeld kunnen worden en toestaan deze aan te koppelen. Alle apparaten tonen doe afgekoppeld kunnen worden en toestaan deze af te koppelen. Alle apparaten tonen die vergrendeld kunnen worden en toestaan deze te vergrendelen. Alle apparaten tonen die ontsloten kunnen worden en toestaan deze te ontsluiten. Het apparaat aankoppelen apparaat uitwerpen vergrendelen aankoppelen ontgrendelen afkoppelen De container ontgrendelen Het apparaat afkoppelen 