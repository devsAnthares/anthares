��          �      <      �     �  <   �  
   �     	          &     3     ?  
   G     R     c     q     }     �     �  
   �     �  �  �     b  N   �  	   �  	   �     �                "     +     B     [     q     }     �     �     �     �                                                  
   	                                                Add Launcher... Add launchers by Drag and Drop or by using the context menu. Appearance Arrangement Edit Launcher... Enable popup Enter title General Hide icons Maximum columns: Maximum rows: Quicklaunch Remove Launcher Show hidden icons Show launcher names Show title Title Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-02-26 04:09+0100
PO-Revision-Date: 2016-01-19 14:30+0100
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 Programmastarter toevoegen... Starters toevoegen door slepen en laten vallen met gebruik van het contextmenu Uiterlijk Plaatsing Programmastarter bewerken... Pop-up inschakelen Titel invoeren Algemeen Pictogrammen verbergen Maximum aantal kolommen: Maximum aantal rijen: Snelstarten Programmastarter verwijderen Verborgen pictogrammen tonen Startnamen tonen Titel tonen Titel 