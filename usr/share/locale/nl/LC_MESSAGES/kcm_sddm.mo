��    ,      |  ;   �      �  (   �     �  �   �     �     �     �     �     �     �     �     �     �               %     1      J     k     s     �     �     �     �     �     �     �               /     4     I     Y     l     y     �     �      �  7   �                     1     6  �  <     �     �     �     	     	     *	     1	     H	     U	  
   g	     r	     �	     �	  	   �	     �	     �	     �	     �	     �	     
     0
     H
     _
     t
  "   �
     �
     �
     �
     �
      �
      �
          >     P  $   f     �  !   �  @   �     �     �       	   1  
   ;     !                 %   +   ,      "   #                               &                  	                           )                             *      '      $      (                                
    %1 is the name of a session%1 (Wayland) ... @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) @title:windowSelect Image Advanced Author Auto &Login Background: Clear Image Commands Could not decompress archive Cursor Theme: Customize theme Default Description Download New SDDM Themes EMAIL OF TRANSLATORSYour emails General Get New Theme Halt Command: Install From File Install a theme. Invalid theme package Load from file... Login screen using the SDDM Maximum UID: Minimum UID: NAME OF TRANSLATORSYour names Name No preview available Reboot Command: Relogin after quit Remove Theme SDDM KDE Config SDDM theme installer Session: The default cursor theme in SDDM The theme to install, must be an existing archive file. Theme Unable to install theme Uninstall a theme. User User: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2017-10-17 09:57+0100
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 %1 (Wayland) ... (Beschikbare afmetingen: %1) Afbeelding selecteren Geavanceerd Auteur &Automatisch aanmelden Achtergrond: Afbeelding wissen Commando's Kon archief niet uitpakken Cursorthema: Thema aanpassen Standaard Beschrijving Nieuwe SDDM-thema's downloaden freekdekruijf@kde.nl Algemeen Nieuw thema ophalen Commando voor geheel stoppen: Uit bestand installeren Een thema installeren. Ongeldig themapakket Uit bestand laden... Aanmeldscherm met gebruik van SDDM Maximale UID: Minimale UID: Freek de Kruijf Naam Geen afdrukvoorbeeld beschikbaar Commando voor opnieuw opstarten: Opnieuw aanmelden na afsluiten Thema verwijderen SDDM KDE configuratie Installatieprogramma voor SDDM-thema Sessie: Het standaard cursorthema in SDDM Het te installeren thema, moet een bestaand archiefbestand zijn. Thema Kan thema niet installeren Een thema deïnstalleren. Gebruiker Gebruiker: 