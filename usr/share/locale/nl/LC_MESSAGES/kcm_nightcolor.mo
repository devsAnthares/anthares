��          �      �           	               4  	   I     S      c  "   �      �     �     �  	   �     �               )  
   9     D     S     a     g     {  �  �     "     %     -     M     b     n     �     �     �     �     �     �     �  
             %  
   5     @     U     a     h  
   w                              	                                     
                                      K (HH:MM) (In minutes - min. 1, max. 600) Activate Night Color Automatic Detect location EMAIL OF TRANSLATORSYour emails Error: Morning not before evening. Error: Transition time overlaps. Latitude Location Longitude NAME OF TRANSLATORSYour names Night Color Night Color Temperature:  Operation mode: Roman Gilg Sunrise begins Sunset begins Times Transition duration and ends Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-12 03:09+0100
PO-Revision-Date: 2017-12-12 22:43+0100
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
  K (HH:MM) (In minuten - min. 1, max. 600) Nachtkleur activeren Automatisch Locatie detecteren freekdekruijf@kde.nl Fout: morgen niet voor avond. Fout: transitietijd overlapt Breedtegraad Locatie Lengtegraad Freek de Kruijf Nachtkleur Nachtkleurtemperatuur:  Operatie-modus: Roman Gilg Begin van zonsopgang Zon komt op Tijden Duur transitie en eindigt 