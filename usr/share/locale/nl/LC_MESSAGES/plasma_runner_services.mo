��          D      l       �      �   6   �   a   �      /  �  ?     �  G         H     P                          Applications Finds applications whose name or description match :q: Jump list search result, %1 is action (eg. open new tab), %2 is application (eg. browser)%1 - %2 System Settings Project-Id-Version: plasma_runner_services
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-14 03:09+0100
PO-Revision-Date: 2016-05-01 16:41+0200
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 Toepassingen Zoekt toepassingen waarvan de naam of beschrijving overeen komt met :q: %1 - %2 Systeeminstellingen 