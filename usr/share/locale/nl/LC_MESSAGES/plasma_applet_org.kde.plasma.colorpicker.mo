��          |      �          %   !     G     U     c     u     �     �  
   �     �     �  $   �  �  �  -   �     �     �     �     �               *     9     H     [     
   	                                                   Automatically copy color to clipboard Clear History Color Options Copy to Clipboard Default color format: General Open Color Dialog Pick Color Pick a color Show history When pressing the keyboard shortcut: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-12 03:03+0200
PO-Revision-Date: 2016-12-15 10:04+0100
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Kleur automatisch kopiëren naar het klembord Geschiedenis wissen Kleuropties Naar klembord kopiëren Standaard kleurformaat: Algemeen Kleurendialoog openen Kies een kleur Kies een kleur Geschiedenis tonen Bij indrukken van de sneltoets: 