��          �      L      �     �     �     �     �          )     1     9     P     h     t  K   �     �     �     �     �            �        �     �     �     �          4     <     D     Z     z     �     �     �     �     �     �     �     �                                                  
   	                                               Change the barcode type Clear history Clipboard Contents Clipboard history is empty. Clipboard is empty Code 39 Code 93 Configure Clipboard... Creating barcode failed Data Matrix Edit contents Indicator that there are more urls in the clipboard than previews shown+%1 Invoke action QR Code Remove from history Return to Clipboard Search Show barcode Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-10 03:07+0200
PO-Revision-Date: 2015-02-17 15:42+0100
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 Wijzig het type barcode Geschiedenis wissen Klembordinhoud Klembordgeschiedenis is leeg. Klembordgeschiedenis is leeg Code 39 Code 93 Klembord instellen... Aanmaken van barcode is mislukt Gegevensmatrix Inhoud bewerken +%1 Actie aanroepen QR-code Uit geschiedenis verwijderen Terug naar klembord Zoeken Barcode tonen 