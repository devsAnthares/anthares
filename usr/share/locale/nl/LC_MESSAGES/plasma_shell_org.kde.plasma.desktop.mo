��    F      L  a   |         
     
     
        "     1     5     I     X     ^  	   m     w  	        �     �     �  
   �     �     �  3   �  	   �     �               $     9     @     G     V     f     m  
        �  2   �  ?   �                    )     2     ?     N     S     a     r     �     �     �     �     �     �  	   �     �     �     �     �  b   �  �   [	     �	     �	  	   
     
     (
  
   8
  	   C
     M
     ]
     e
     k
     }
  �  �
     ,     9     I     b     w     {     �  	   �     �     �     �     �     �     �  	                  $     ,  	   .     8     M     d     i     {     �     �     �     �     �     �     �  ?   �  b   /  	   �     �     �  	   �     �     �     �     �     �  
             !     2     E     L  
   ]  	   h     r     x     �     �  b   �  �        �     �     �     �          ,     :     P     l     t     |     �     +       E   C   4       &       D           
   <      *   9           !   F      /   #                A       @   =                     $   ;                  B             :       '           0         1   2                                -   (       6   ?         >              "   .   %   ,              )       8       3   5   	                  7    Activities Add Action Add Spacer Add Widgets... Alt Alternative Widgets Always Visible Apply Apply Settings Apply now Author: Auto Hide Back-Button Bottom Cancel Categories Center Close Concatenation sign for shortcuts, e.g. Ctrl+Shift+ Configure Configure activity Create activity... Ctrl Currently being used Delete Email: Forward-Button Get new widgets Height Horizontal-Scroll Input Here Keyboard shortcuts Layout cannot be changed whilst widgets are locked Layout changes must be applied before other changes can be made Layout: Left Left-Button License: Lock Widgets Maximize Panel Meta Middle-Button More Settings... Mouse Actions OK Panel Alignment Remove Panel Right Right-Button Screen Edge Search... Shift Stop activity Stopped activities: Switch The settings of the current module have changed. Do you want to apply the changes or discard them? This shortcut will activate the applet: it will give the keyboard focus to it, and if the applet has a popup (such as the start menu), the popup will be open. Top Undo uninstall Uninstall Uninstall widget Vertical-Scroll Visibility Wallpaper Wallpaper Type: Widgets Width Windows Can Cover Windows Go Below Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-09 03:10+0200
PO-Revision-Date: 2017-09-09 15:55+0100
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Activiteiten Actie toevoegen Paneelscheider toevoegen Widgets toevoegen... Alt Alternatieve widgets Altijd zichtbaar Toepassen Instellingen toepassen Nu toepassen Auteur: Automatisch verbergen Knop voor terug Onderaan Annuleren Categorieën Midden Sluiten + Instellen Activiteit instellen Activiteit aanmaken... Ctrl Wordt nu gebruikt Verwijderen E-mail: Knop voor voorwaarts Nieuwe widgets ophalen Hoogte Horizontaal schuiven Invoer hier Sneltoetsen Indeling kan niet gewijzigd worden als widgets vergrendeld zijn Wijzigingen aan indeling moeten worden toegepast alvorens andere wijzigingen gemaakt kunnen worden Indeling: Links Linker muisknop Licentie: Widgets vergrendelen Paneel maximaliseren Meta Middelste muisknop Meer instellingen... Muisacties OK Paneeluitlijning Paneel verwijderen Rechts Rechter muisknop Schermrand Zoeken... Shift Activiteit stoppen Gestopte activiteiten Wisselen De instellingen van het huidige module zijn gewijzigd. Wilt u de wijzigingen opslaan of verwerpen? Deze sneltoets zal de applet activeren: het verlegt de focus van het toetsenbord naar deze applet en als deze applet een pop-up heeft (zoals het startmenu), zal de pop-up geopend worden. Bovenaan Deïnstalleren ongedaan maken Deïnstalleren Widget voor deïnstalleren Verticaal schuiven Zichtbaarheid Achtergrondafbeelding Type achtergrondafbeelding: Widgets Breedte Vensters mogen bedekken Vensters naar onderen 