��    
      l      �       �      �            	               .  I   3     }  	   �  �  �     1     A     P     V      ]     ~  Q   �     �     �               
   	                           &Trigger word: Add Item Alias Alias: Character Runner Config Code Creates Characters from :q: if it is a hexadecimal code or defined alias. Delete Item Hex Code: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2010-08-11 21:40+0200
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.1
Plural-Forms: nplurals=2; plural=n != 1;
 &Trigger-woord: Item toevoegen Alias Alias: Configuratie van tekenuitvoerder Code Maakt tekens aan uit :q: als het een hexadecimale code is of gedefinieerde alias. Item verwijderen Hexcode 