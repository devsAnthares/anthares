��          �      <      �     �     �     �  7  �  �  #  +   �  ^              �     �  x   �  �   %     �               ;     U  �  r     	     7	     L	    c	    {
  1   �  g   �          3     I  �   R    �     �       '   )     Q     k                          
       	                                                                    &End current session &Restart computer &Turn off computer <h1>Session Manager</h1> You can configure the session manager here. This includes options such as whether or not the session exit (logout) should be confirmed, whether the session should be restored again when logging in and whether the computer should be automatically shut down after session exit by default. <ul>
<li><b>Restore previous session:</b> Will save all applications running on exit and restore them when they next start up</li>
<li><b>Restore manually saved session: </b> Allows the session to be saved at any time via "Save Session" in the K-Menu. This means the currently started applications will reappear when they next start up.</li>
<li><b>Start with an empty session:</b> Do not save anything. Will come up with an empty desktop on next start.</li>
</ul> Applications to be e&xcluded from sessions: Check this option if you want the session manager to display a logout confirmation dialog box. Conf&irm logout Default Leave Option General Here you can choose what should happen by default when you log out. This only has meaning, if you logged in through KDM. Here you can enter a colon or comma separated list of applications that should not be saved in sessions, and therefore will not be started when restoring a session. For example 'xterm:konsole' or 'xterm,konsole'. O&ffer shutdown options On Login Restore &manually saved session Restore &previous session Start with an empty &session Project-Id-Version: kcmsmserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2013-12-24 12:17+0100
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 Huidige sessie b&eëindigen Computer he&rstarten Computer ui&tschakelen <h1>Sessiebeheerder</h1>U kunt hier de sessiebeheerder instellen. Dit omvat het instellen van opties, zoals of het afmelden zal worden bevestigd, of de sessie zal worden opgeslagen bij het afmelden, en of de computer standaard zal worden uitgeschakeld als u de sessie beëindigt. <ul>
<li><b>Vorige sessie herstellen:</b>Dit onthoudt welke toepassingen actief zijn als u zich afmeldt en herstelt ze bij de volgende start.</li>
<li><b>Handmatig opgeslagen sessie herstellen: </b> hiermee kunt u op elk gewenst moment een sessie opslaan via de optie "Sessie opslaan" in het K-menu. De toepassingen die op dat moment actief zijn zullen worden hersteld bij de volgende start van KDE.</li>
<li><b>Starten met een lege sessie:</b> sla niets op. Bij de volgende start begint u met een schone lei.</li>
</ul> Toepassingen die uit&gesloten worden van sessies: Schakel deze optie in als u wilt dat de sessiebeheerder u om een bevestiging vraagt als u zich afmeldt. Afmelden be&vestigen Standaard afmeldoptie Algemeen Hier kunt u kiezen wat er standaard zal gebeuren als u zich afmeldt. Dit is alleen van toepassing als u zich hebt aangemeld via het grafische aanmeldscherm van KDM. Hier kunt u programma's opgeven die niet in sessies mogen worden opgeslagen, en dus niet worden gestart tijdens het herstellen van een sessie. Scheidt de toepassingen van elkaar met behulp van komma's of dubbele punten, zoals 'xterm,xconsole' of 'xterm:konsole'. Afsluit&opties aanbieden Bij het aanmelden Hand&matig opgeslagen sessie herstellen V&orige sessie herstellen Met een lege &sessie opstarten 