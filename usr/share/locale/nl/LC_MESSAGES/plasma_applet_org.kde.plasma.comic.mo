��    =        S   �      8  ;   9     u     �     �     �  6   �  A   
     L     U  $   Y  
   ~     �  #   �     �     �     �     �     �  7        >     [     w  '   �     �     �  $   �  ,   �          3     C     K     ]     y     �     �     �     �     �  �   �  9   �	  "   �	     �	     �	     
     *
     <
     R
     c
  %   u
     �
     �
     �
     �
     �
  
   �
  7        :     T     l     t  �  �  D   6  '   {     �     �  %   �     �          (     4  '   :  	   b  "   l  5   �     �  #   �     �     �       8   "  *   [  #   �     �  /   �     �     �  5     6   K     �     �     �     �      �     �  
     "     "   3     V     u  �   �  Q   d  (   �     �  *   �           2  !   P     r     �  )   �  	   �     �     �            
   #  ,   .     [     b     g     o     !          3      0      (                 1      "          =   .   6                                         	      :          5       ,   #   &   7       ;   '             -   
             8                 *           %           <   4   9                )   /   $          2             +       

Choose the previous strip to go to the last cached strip. &Create Comic Book Archive... &Save Comic As... &Strip Number: *.cbz|Comic Book Archive (Zip) @option:check Context menu of comic image&Actual Size @option:check Context menu of comic imageStore current &Position Advanced All An error happened for identifier %1. Appearance Archiving comic failed Automatically update comic plugins: Cache Check for new comic strips: Comic Comic cache: Configure... Could not create the archive at the specified location. Create %1 Comic Book Archive Creating Comic Book Archive Destination: Display error when getting comic failed Download Comics Error Handling Failed adding a file to the archive. Failed creating the file with identifier %1. From beginning to ... From end to ... General Get New Comics... Getting comic strip failed: Go to Strip Information Jump to &current Strip Jump to &first Strip Jump to Strip ... Manual range Maybe there is no Internet connection.
Maybe the comic plugin is broken.
Another reason might be that there is no comic for this day/number/string, so choosing a different one might work. Middle-click on the comic to show it at its original size No zip file is existing, aborting. Range: Show arrows only on mouse over Show comic URL Show comic author Show comic identifier Show comic title Strip identifier: The range of comic strips to archive. Update Visit the comic website Visit the shop &website an abbreviation for Number# %1 days dd.MM.yyyy here strip means comic strip&Next Tab with a new Strip in a range: from toFrom: in a range: from toTo: minutes strips per comic Project-Id-Version: plasma_applet_comic
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-07-18 03:20+0200
PO-Revision-Date: 2015-03-06 11:04+0100
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 

Kies de vorige strip om naar de laatste strip in de cache te gaan. Archief voor stripverhalen &aanmaken... Stripverhaal op&slaan als... &Stripverhaalnummer: *.cbz|Archief van stripverhalen (zip) &Werkelijke grootte Huidige &positie opslaan Geavanceerd Alles Er trad een fout op voor identifier %1. Uiterlijk Stripverhaal archiveren is mislukt Automatisch bijwerken van plug-ins van stripverhalen: Cache Op nieuwe stripverhalen controleren Stripverhaal Cache van stripverhalen: Instellen... Kon geen archief aanmaken op de gespecificeerde locatie. Een %1 archief voor stripverhalen aanmaken Archief voor stripverhalen aanmaken Bestemming: Fout tonen als ophalen van stripverhaal mislukt Stripverhalen downloaden Afhandelen van fouten Toevoegen van een bestand aan het archief is mislukt. Aanmaken van het bestand met identifier %1 is mislukt. Van begin tot ... Van einde tot ... Algemeen Nieuwe stripverhalen ophalen... Stripverhaal ophalen is mislukt: Ga naar stripverhaal Informatie Naar &huidig stripverhaal springen &Naar eerste stripverhaal springen Naar stripverhaal springen ... Handmatige reeks Misschien is er geen internetverbinding.
Misschien is de plug-in voor strips gebroken.
Een andere reden zou kunnen zijn dat er geen strip voor vandaag/nummer/tekenreeks is, dus zou het kiezen van een andere kunnen werken. Klikken met middenknop op het stripverhaal om de oorspronkelijke grootte te tonen Er bestaat geen zip-bestand, afgebroken. Reeks: Alleen pijlen tonen bij muis erover zweven URL-adres van stripverhaal tonen Auteur van stripverhaal tonen Identifier van stripverhaal tonen Stripverhaaltitel tonen Identifier van stripverhaal: De reeks van te archiveren stripverhalen. Bijwerken Bezoek de stripverhaalwebsite Bezoek de stripverhaal&website # %1 dagen dd.MM.jjjj &Volgende tabblad met een nieuw stripverhaal Vanaf: Tot: minuten strips per stripverhaal 