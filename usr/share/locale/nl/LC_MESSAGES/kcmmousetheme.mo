��          �   %   �      @     A  �   `  m   �  �   P  )   �  `        o     |     �     �     �      �     �     �  '        ,     >     ]     b     s  ?   �  Y   �  +     H   F  �  �     -  �   L  v   �     Y	     v	  Y   �	     �	     �	     
     
  $   ,
     Q
     f
     w
  *   �
     �
     �
     �
     �
     �
  4   
  W   ?  .   �  S   �                                                 	                                                               
              (c) 2003-2007 Fredrik Höglund <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) @item:inlistbox sizeResolution dependent A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Cursor Theme Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Fredrik Höglund Get new Theme Get new color schemes from the Internet Install from File NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. You have to restart the Plasma session for these changes to take effect. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2018-01-24 11:00+0100
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 (c) 2003-2007 Fredrik Höglund <qt>Weet u zeker dat u het aanwijzerstijlbestand <i>%1</i> wilt verwijderen?<br />Alle bestanden horend bij deze stijl zullen worden verwijderd.</qt> <qt>U kunt het thema dat u op dit moment gebruikt niet verwijderen.<br />Schakel eerst over naar een ander thema.</qt> (Beschikbare afmetingen: %1) Resolutie-afhankelijk  Een stijl met naam %1 bestaat al in uw pictogramstijlmap. Wilt u die vervangen door deze? Bevestiging Aanwijzerinstellingen aangepast Cursorthema Beschrijving Sleep of typ het adres van het thema freekdekruijf@kde.nl Fredrik Höglund Nieuw thema ophalen Nieuw kleurenschema's van internet ophalen Uit bestand installeren Freek de Kruijf Naam Thema overschrijven? Thema verwijderen Het bestand %1 is geen geldig aanwijzerstijlbestand. Het aanwijzerstijlarchief kon niet worden gedownload. Zorg dat het adres %1 correct is. Het aanwijzerstijlarchief %1 is niet gevonden. Om deze wijzigingen te bekrachtigen dient u de Plasma sessie opnieuw op te starten. 