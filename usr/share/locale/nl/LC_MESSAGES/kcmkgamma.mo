��          �      ,      �     �  $  �     �  	   �  	   �  F   �     %  
   ,     7  
   >     I  	   R     \     a  	   {     �  �  �     0  �  M     �  
   �     �  T   �     N	     U	     a	     h	     t	  
   �	     �	  !   �	  	   �	     �	            
                                    	                              &Select test picture: <h1>Monitor Gamma</h1> This is a tool for changing monitor gamma correction. Use the four sliders to define the gamma correction either as a single value, or separately for the red, green and blue components. You may need to correct the brightness and contrast settings of your monitor for good results. The test images help you to find proper settings.<br> You can save them system-wide to XF86Config (root access is required for that) or to your own KDE settings. On multi head systems you can correct the gamma values separately for all screens. Blue: CMY Scale Dark Gray Gamma correction is not supported by your graphics hardware or driver. Gamma: Gray Scale Green: Light Gray Mid Gray RGB Scale Red: Save settings system wide Screen %1 Sync screens Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-18 03:02+0200
PO-Revision-Date: 2015-07-31 11:50+0100
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Test afbeelding &selecteren: <h1>Monitor Gamma</h1> Dit is een hulpmiddel voor het wijzigen van de gammacorrectie van de monitor. Gebruik de vier schuifregelaars om de gammacorrectie ofwel als een enkele waarde of separaat voor de rode, groene en blauwe componenten te definiëren. Het is misschien nodig om de instellingen voor helderheid en contrast van uw monitor te corrigeren voor goede resultaten. De testafbeeldingen helpen u om de juiste instellingen te vinden.<br> U kunt ze systeembreed in XF86Config opslaan (hiervoor is toegang van root vereist) of tot uw eigen KDE instellingen. Op systemen met meer schermen kunt u de gammawaarden apart voor alle schermen corrigeren. Blauw: CMY-schaal Donker grijs Gamma-correctie wordt niet ondersteund door uw grafische hardware of stuurprogramma. Gamma: Grijsschaal Groen: Licht grijs Middel grijs RGB-schaal Rood: Instellingen systeembreed opslaan Scherm %1 Schermen synchroniseren 