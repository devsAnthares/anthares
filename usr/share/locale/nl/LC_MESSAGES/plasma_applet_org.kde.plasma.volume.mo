��    &      L  5   |      P     Q  8   S     �     �     �     �     �     �  3   �  >        N     i     y     �  m   �     �          "     2     7     ?  *   O      z     �     �  "   �     �     �          3     :     H     X     e     �  ;   �     �  �  �     �     �     �     �     �     �     �     �     	  	   
	     	     -	  	   =	     G	     P	     X	     q	     �	     �	  	   �	     �	  0   �	  %   �	     

     
     3
     G
     W
      q
     �
     �
     �
     �
     �
     �
     �
     �
        #   %                                                              "          $                      
                                           &      !                	        % Accessibility data on volume sliderAdjust volume for %1 Applications Audio Muted Audio Volume Behavior Capture Devices Capture Streams Checkable switch for (un-)muting sound output.Mute Checkable switch to change the current default output.Default Decrease Microphone Volume Decrease Volume Devices General Heading for a list of ports of a device (for example built-in laptop speakers or a plug for headphones)Ports Increase Microphone Volume Increase Volume Maximum volume: Mute Mute %1 Mute Microphone No applications playing or recording audio No output or input devices found Playback Devices Playback Streams Port is unavailable (unavailable) Port is unplugged (unplugged) Raise maximum volume Show additional options for %1 Volume Volume at %1% Volume feedback Volume step: label of device items%1 (%2) label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2017-09-20 10:34+0100
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 % Volume voor %1 aanpassen Toepassingen Geluid gedempt Geluidsvolume Gedrag Opnameapparaten Streams om op te nemen Dempen Standaard Microfoonvolume verlagen Volume verlagen Apparaten Algemeen Poorten Microfoonvolume verhogen Volume verhogen Maximum volume: Dempen %1 dempen Microfoon dempen Geen toepassingen die geluid afspelen of opnemen Geen in- of uitvoerapparaten gevonden Afspeelapparaten Streams om af te spelen  (niet beschikbaar)  (losgekoppeld) Maximum volume vergroten: Additionele opties voor %1 tonen Volume Volume op %1% Volume van feedback Volumestap: %1 (%2) %1: %2 100% %1% 