��            )   �      �     �     �     �     �     �     �     �     �  0        3     G     ]     c     o     w     �  
   �     �     �     �     �     �               (     A     I     a     m  �  s  
     %        B     S     k     x     �     �  @   �     �  ,   �     $     )     >     F  %   e     �     �     �     �  !   �      	  	   *     4     M     l     u     �     �                                            
                                                                                      	       %1 by %2 Add Custom Wallpaper Add Folder... Add Image... Background: Blur Centered Change every: Directory with the wallpaper to show slides from Download Wallpapers Get New Wallpapers... Hours Image Files Minutes Next Wallpaper Image Open Containing Folder Open Image Open Wallpaper Image Positioning: Recommended wallpaper file Remove wallpaper Restore wallpaper Scaled Scaled and Cropped Scaled, Keep Proportions Seconds Select Background Color Solid color Tiled Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:39+0100
PO-Revision-Date: 2017-11-18 11:46+0100
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 %1 door %2 Eigen achtergrondafbeelding toevoegen Map toevoegen... Afbeelding toevoegen... Achtergrond: Vervagen Gecentreerd Allen wijzigen: Map met de achtergrondafbeelding waar vanaf dia's getoond worden Achtergronden downloaden Nieuwe achtergrondafbeeldingen downloaden... Uren Afbeeldingsbestanden Minuten Volgende achtergrondafbeelding De map openen waarin het zich bevindt Afbeelding openen Achtergrondafbeelding openen Positionering: Aanbevolen achtergrondbestand Achtergrondafbeelding verwijderen Achtergrondafbeelding herstellen Geschaald Geschaald en bijgesneden Geschaald, verhouding behouden Seconden Achtergrond kleur kiezen Vlakke kleur In tegelvorm 