��          �            x     y  
   �  
   �     �  .   �     �     �     �        *   )      T  ,   u  ,   �  0   �        �    !   �  	   �  	   �     �  ,   �       
   $     /  #   C  /   g  %   �     �     �  3   �                             
                                            	           &Lock screen on resume: Activation Appearance Error Failed to successfully test the screen locker. Immediately Keyboard shortcut: Lock Session Lock screen automatically after: Lock screen when waking up from suspension Re&quire password after locking: Spinbox suffix. Short for minutes min  mins Spinbox suffix. Short for seconds sec  secs The global keyboard shortcut to lock the screen. Wallpaper &Type: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:10+0100
PO-Revision-Date: 2018-01-10 01:00+0100
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Vergrende&l scherm bij hervatten: Activatie Uiterlijk Fout Testen van de schermvergrendeling is mislukt Onmiddellijk Sneltoets: Sessie vergrendelen Scherm automatisch vergrendelen na: Scherm vergrendelen bij ontwaken uit slaapstand Wachtwoord &vereisen na vergrendelen:  min  minuten  sec  seconden De globale sneltoets om het scherm te vergrendelen. &Type achtergrondafbeelding: 