��    	      d      �       �      �      �      �   E   
     P     f     o     �  �  �     6     C     Y  ?   n     �     �     �     �        	                                      Disable Disable touchpad Enable touchpad No mouse was detected.
Are you sure you want to disable the touchpad? No touchpad was found Touchpad Touchpad is disabled Touchpad is enabled Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2014-01-14 09:58+0100
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 Uitschakelen Touchpad uitschakelen Touchpad inschakelen Er is geen muis gedetecteerd.
Wilt u het touchpad uitschakelen? Geen touchpad gevonden Touchpad Touchpad is uitgeschakeld Touchpad is ingeschakeld 