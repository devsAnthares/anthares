��          �      |      �     �     �     �                 ;  	   \     f     �  &   �     �     �     �  	          �   %  �   �  t   ?  7   �  +   �       �       �  
   �     �     �  $   �          '     >     N  2   e     �     �      �  	   �     �  �   �  �   �  �   	     �	  B   �	     �	                                      
   	                                                         min Act like Always Define a special behavior Don't use special settings EMAIL OF TRANSLATORSYour emails Hibernate NAME OF TRANSLATORSYour names Never shutdown the screen Never suspend or shutdown the computer PC running on AC power PC running on battery power PC running on low battery Shut down Suspend The Power Management Service appears not to be running.
This can be solved by starting or scheduling it inside "Startup and Shutdown" The activity service is not running.
It is necessary to have the activity manager running to configure activity-specific power management behavior. The activity service is running with bare functionalities.
Names and icons of the activities might not be available. This is meant to be: Act like activity %1Activity "%1" Use separate settings (advanced users only) after Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-09 03:03+0200
PO-Revision-Date: 2016-05-05 13:05+0200
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
  min Werk zoals Altijd Definieer speciaal gedrag Geen speciale instellingen gebruiken freekdekruijf@kde.nl Slaapstand naar schijf Freek de Kruijf Laat het scherm actief Onderbreek de computer nooit en sluit het niet af  PC werkt op netstroom PC actief op batterij PC actief op bijna lege batterij Afsluiten Onderbreken De service energiebeheer lijkt niet actief te zijn.
Dit kan worden opgelost door het op te starten of te plannen in "Opstarten en afsluiten" De service activiteiten is niet actief.
De activiteitenbeheerder moet actief zijn om gedrag per activiteit voor energiebeheer in te kunnen stellen. De service activiteiten is actief met basis functionaliteiten.
Namen en pictogrammen van de activiteiten kunnen niet beschikbaar zijn. Activiteit "%1" Gescheiden instellingen gebruiken (alleen gebruikers met ervaring) na 