��    %      D  5   l      @     A     E     G  	   Y     c     |     �     �     �     �     �     �            S   ,  w   �     �  M         [     |  ?   �     �  	   �     �     
     #  %   2     X     e  F   �  #   �     �  ]     R   f     �     �  �  �     �	     �	     �	  
   �	     �	     �	     �	     �	     �	      
     2
  '   J
     r
     �
  ]   �
  {   �
     v  N   �     �     �  I   �     I  
   Y  '   d     �     �  7   �     �     �  ]     +   i     �  ^   �  f        {     �               !      "          #   %                                                                            $                          	                
                            ms % %1 - All Desktops %1 - Cube %1 - Current Application %1 - Current Desktop %1 - Cylinder %1 - Sphere &Reactivation delay: &Switch desktop on edge: Activation &delay: Active Screen Corners and Edges Activity Manager Always Enabled Amount of time required after triggering an action until the next trigger can occur Amount of time required for the mouse cursor to be pushed against the edge of the screen before the action is triggered Application Launcher Change desktop when the mouse cursor is pushed against the edge of the screen EMAIL OF TRANSLATORSYour emails Lock Screen Maximize windows by dragging them to the top edge of the screen NAME OF TRANSLATORSYour names No Action Only When Moving Windows Open krunnerRun Command Other Settings Quarter tiling triggered in the outer Show Desktop Switch desktop on edgeDisabled Tile windows by dragging them to the left or right edges of the screen Toggle alternative window switching Toggle window switching Trigger an action by pushing the mouse cursor against the corresponding screen edge or corner Trigger an action by swiping from the screen edge towards the center of the screen Window Management of the screen Project-Id-Version: kcmkwinscreenedges
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-30 03:13+0100
PO-Revision-Date: 2017-12-30 16:14+0100
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
  ms % %1 - alle bureaubladen %1 - kubus %1 - huidige toepassing %1 - huidig bureaublad %1 - cilinder %1 - bol &Reactiveringsvertraging: &Verander bureaublad op de rand: Activerings&vertraging: Hoeken en randen van het actieve scherm Activiteitenbeheerder Altijd ingeschakeld Hoeveelheid tijd vereist nadat een actie is genomen totdat de volgende actie kan plaatsvinden Hoeveelheid tijd vereist voor de muiscursor om tegen de rand van het scherm gedrukt te zijn alvorens de actie wordt genomen Programmastarter Verander van bureaublad als de muis tegen de rand van het scherm wordt gedrukt freekdekruijf@kde.nl Scherm vergrendelen Vensters maximaliseren door ze naar de bovenrand van het scherm te slepen Freek de Kruijf Geen actie Alleen bij het verplaatsen van vensters Commando uitvoeren Overige instellingen Kwartgedeelten laten overlappen gestart in de buitenste Bureaublad tonen Uitgeschakeld Vensters achter elkaar zetten door ze naar de linker of rechter rand van het scherm te slepen Alternatief venster omschakelen omschakelen Venster omschakelen omschakelen Om een actie te laten plaatsvinden drukt u de muis tegen de overeenkomende schermrand of hoek. Laat een actie plaatsvinden door vanaf de rand van het scherm in de richting van het centrum te vegen. Vensterbeheer van het scherm 