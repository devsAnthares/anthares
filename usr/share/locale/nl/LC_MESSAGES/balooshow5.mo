��          �      \      �     �     �  
   �                :      N     o  
   }     �     �     �  	   �  (   �  \   	     f  2   t     �     �  �  �     a     o     �     �     �     �  %   �                    ,     J  
   a  ;   l  l   �       @   .     o     ~     	                                                                                 
             %1 Terms: %2 (c) 2012, Vishesh Handa Baloo Show Device id for the files EMAIL OF TRANSLATORSYour emails File Name Terms: %1 Inode number of the file to show Internal Info Maintainer NAME OF TRANSLATORSYour names No index information found Print internal info Terms: %1 The Baloo data Viewer - A debugging tool The Baloo index could not be opened. Please run "%1" to see if Baloo is enabled and working. The file urls The fileID is not equal to the actual Baloo fileID This is a bug Vishesh Handa Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-28 03:17+0100
PO-Revision-Date: 2018-01-29 11:25+0100
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 %1 termen: %2 (c) 2012, Vishesh Handa Baloo-tonen Apparaat-id voor de bestanden freekdekruijf@kde.nl Bestandsnaamtermen: %1 Inode-nummer van het te tonen bestand Interne info Onderhouder Freek de Kruijf Geen indexinformatie gevonden Interne info afdrukken Termen: %1 De Baloo viewer van gegevens - een hulpmiddel voor debuggen De Baloo-index kon niet geopend worden. Geef het commando "%1" om te zien of Baloo is ingeschakeld en werkt. De URL's van het bestand Het bestands-ID is niet gelijk aan het actuele Baloo bestands-ID Dit is een bug Vishesh Handa 