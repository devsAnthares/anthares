��    &      L  5   |      P     Q     l     y     �     �     �     �     �     �     �     �     �  &   �               #     ,     3     ?     M     U     ]     d     s     �     �     �     �     �     �     �     �     �     �  
   �            �       �     �  !   �     �     �     �     �               !     *     ;  &   A     h  	   v     �     �     �     �     �     �     �     �     �     �     �          !     (     -     :     Q     V     g     x  
   �     �                           	      #      $                                                    &                                          !          %                            "   
    Abbreviation for secondss Application: Average clock: %1 MHz Bar Buffers: CPU CPU %1 CPU monitor CPU%1: %2% @ %3 Mhz CPU: %1% CPUs separately Cache Cache Dirty, Writeback: %1 MiB, %2 MiB Cache monitor Cached: Circular Colors Compact Bar Dirty memory: General IOWait: Memory Memory monitor Memory: %1/%2 MiB Monitor type: Nice: Set colors manually Show: Swap Swap monitor Swap: %1/%2 MiB Sys: System load Update interval: Used swap: User: Writeback memory: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-29 03:31+0200
PO-Revision-Date: 2017-02-25 23:36+0100
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 s Toepassing: Gemiddelde klokfrequentie: %1 MHz Balk Buffers: CPU CPU %1 CPU-monitor CPU%1: %2% @ %3 Mhz CPU: %1% Gescheiden CPU's Cache Cache Dirty, Writeback: %1 MiB, %2 MiB Cache-monitor Gecached: Cirkelvormig Kleuren Compacte balk Dirty geheugen: Algemeen IOWait: Geheugen Geheugenmonitor Geheugen: %1/%2 MiB Type monitor: Nice: Handmatig kleuren instellen Tonen: Swap Swap-monitor Swapgebruik: %1/%2 MiB Sys: Systeembelasting Bijwerkinterval: Gebruikte swap: Gebruiker: Writeback geheugen: 