��          �      |      �     �  2        B     b       #   �      �     �  %   �       
   &     1     >     ]  �   }       ]   (     �  p   �  :        P  �  \  -   �  S   (  )   |     �     �  )   �       .   "  8   Q  #   �     �     �     �  .   �  �   	     �	  m   �	  3   ;
  �   o
  R   	  
   \                                                                      
                	           Apply a look and feel package Command line tool to apply look and feel packages. Configure Look and Feel details Copyright 2017, Marco Martin Cursor Settings Changed Download New Look And Feel Packages EMAIL OF TRANSLATORSYour emails Get New Looks... List available Look and feel packages Look and feel tool Maintainer Marco Martin NAME OF TRANSLATORSYour names Reset the Plasma Desktop layout Select an overall theme for your workspace (including plasma theme, color scheme, mouse cursor, window and desktop switcher, splash screen, lock screen etc.) Show Preview This module lets you configure the look of the whole workspace with some ready to go presets. Use Desktop Layout from theme Warning: your Plasma Desktop layout will be lost and reset to the default layout provided by the selected theme. You have to restart KDE for cursor changes to take effect. packagename Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-27 05:46+0100
PO-Revision-Date: 2017-12-20 11:44+0100
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Een pakket voor uiterlijk en gedrag toepassen Hulpmiddel op de opdrachtregel om pakketten voor uiterlijk en gedrag toe te passen. Details van uiterlijk en gedrag instellen Copyright 2017, Marco Martin Aanwijzerinstellingen aangepast Nieuwe Look-And-Feel pakketten downloaden freekdekruijf@kde.nl Nieuwe elementen voor het uiterlijk ophalen... Lijst met beschikbare pakketten voor uiterlijk en gedrag Hulpmiddel voor uiterlijk en gedrag Onderhouder Marco Martin Freek de Kruijf De indeling van het Plasma bureaublad resetten Selecteer een thema voor een overal uiterlijk voor uw werkruimte (inclusief plasmathema, muiscursor, omschakelaar van venster en bureaublad, opstartscherm, vergrendelingsscherm etc.) Voorbeeld tonen Deze module laat u het uiterlijk van de gehele werkruimte instellen met enige gereedstaande voorinstellingen. Indeling van het bureaublad uit het thema gebruiken Waarschuwing: uw Plasma indeling van het bureaublad zal verloren gaan en gereset worden naar de standaard indeling geleverd door het geselecteerde thema. Om de wijzigingen aan de cursor te bekrachtigen dient u KDE opnieuw op te starten. pakketnaam 