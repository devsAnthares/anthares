��          �   %   �      p     q  +   �  .   �  6   �  *     #   D  &   h  /   �  2   �  :   �  0   -  3   ^  ;   �  K   �  -     $   H  '   m     �     �     �     �     �  #        1     O     c     |  �  �  
   %  )   0  ,   Z  2   �  *   �     �     	  ,    	  .   M	  8   |	  /   �	  /   �	  5   
  K   K
  $   �
     �
     �
     �
               #     4     B     _     ~     �     �                                                    	   
                                                                    @action:buttonCommit @info:statusAdded files to SVN repository. @info:statusAdding files to SVN repository... @info:statusAdding of files to SVN repository failed. @info:statusCommit of SVN changes failed. @info:statusCommitted SVN changes. @info:statusCommitting SVN changes... @info:statusRemoved files from SVN repository. @info:statusRemoving files from SVN repository... @info:statusRemoving of files from SVN repository failed. @info:statusReverted files from SVN repository. @info:statusReverting files from SVN repository... @info:statusReverting of files from SVN repository failed. @info:statusSVN status update failed. Disabling Option "Show SVN Updates". @info:statusUpdate of SVN repository failed. @info:statusUpdated SVN repository. @info:statusUpdating SVN repository... @item:inmenuSVN Add @item:inmenuSVN Commit... @item:inmenuSVN Delete @item:inmenuSVN Revert @item:inmenuSVN Update @item:inmenuShow Local SVN Changes @item:inmenuShow SVN Updates @labelDescription: @title:windowSVN Commit Show updates Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2015-11-08 23:03+0100
Last-Translator: Freek de Kruijf <freek@opensuse.org>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 Vastleggen Toegevoegde bestanden aan SVN-repository. Bestanden toevoegen aan de SVN-repository... Bestanden toevoegen aan SVN-repository is mislukt. Vastleggen van SVN-wijzigingen is mislukt. Vastgelegde SVN-wijzigingen. SVN-wijzigingen vastleggen... Verwijderde bestanden uit de SVN-repository. Bestanden verwijderen uit de SVN-repository... Verwijderen van bestanden uit SVN-repository is mislukt. Teruggedraaide bestanden uit de SVN-repository. Bestanden terugdraaien uit de SVN-repository... Bestanden terugdraaien uit SVN-repository is mislukt. SVN status update is mislukt. Optie "Show SVN Updates" wordt uitgeschakeld. SVN-repository bijwerken is mislukt. Bijgewerkte SVN-repository SVN-repository bijwerken... SVN toevoegen SVN vastleggen... SVN verwijderen SVN terugdraaien SVN bijwerken Locale SVN-wijzigingen tonen Wat SVN heeft bijgewerkt tonen Beschrijving: SVN vastleggen Wat is bijgewerkt tonen 