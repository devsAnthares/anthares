��          \      �       �   F   �   .     1   ?  4   q  7   �  5   �  1     �  F  (   	     2     >     R  "   h     �     �                                       plasma_containmentactions_contextmenuConfigure Contextual Menu Plugin plasma_containmentactions_contextmenuLeave... plasma_containmentactions_contextmenuLock Screen plasma_containmentactions_contextmenuRun Command... plasma_containmentactions_contextmenuWallpaper Actions plasma_containmentactions_contextmenu[Other Actions] plasma_containmentactions_contextmenu[Separator] Project-Id-Version: plasma_containmentactions_contextmenu
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2014-06-14 21:24+0200
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 De plugin voor het contextmenu instellen Verlaten... Scherm vergrendelen Commando uitvoeren... Acties van achtergrondafbeeldingen [Andere acties] [Scheiding] 