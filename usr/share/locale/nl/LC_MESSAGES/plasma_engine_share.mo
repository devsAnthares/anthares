��    
      l      �       �   $   �   %     :   <     w  '   �  -   �     �       '     �  <     �  &   �  A      .   b  +   �  "   �     �     �  1            	                            
        Could not detect the file's mimetype Could not find all required functions Could not find the provider with the specified destination Error trying to execute script Invalid path for the requested provider It was not possible to read the selected file Service was not available Unknown Error You must specify a URL for this service Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-08-28 17:26+0200
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.1
Plural-Forms: nplurals=2; plural=n != 1;
 Kon het mime-type niet bepalen Kon niet alle vereiste functies vinden Kon de leverancier met de gespecificeerde bestemming niet bepalen Fout bij het proberen uitvoeren van het script Ongeldige pad voor de gevraagde leverancier Kon het gekozen bestand niet lezen Service was niet beschikbaar Onbekende fout U dient een URL voor deze service te specificeren 