��    
      l      �       �   
   �      �        .   0     _     t     �  (   �  8   �  �       �  %   �     �  9   �     ,  	   K     U     d     x        
                             	        Disk Quota No quota restrictions found. Please install 'quota' Quota tool not found.

Please install 'quota'. Running quota failed e.g.: 12 GiB of 20 GiB%1 of %2 e.g.: 8 GiB free%1 free example: Quota: 83% usedQuota: %1% used usage of quota, e.g.: '/home/bla: 38% used'%1: %2% used Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2016-11-20 14:09+0100
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 Schijfquota Geen beperkingen voor quota gevonden. Installeer 'quota' Hulpmiddel voor quota niet gevonden.

Installeer 'quota'. Uitvoeren van quota is mislukt %1 van %2 %1 beschikbaar Quota: %1% gebruikt %1: %2% gebruikt 