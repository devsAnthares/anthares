��    ?        Y         p  �  q  }  J  M  �        
   7     B     K     j     �  <   �     �     �  	   �     �  .     %   A     g     }     �     �     �     �     �     �     �     �  4        B  9   T     �     �     �  �   �  !   �  t   �     8     D     a     n     s  	   �     �  -   �     �  B   �  &     ?   B  '   �  )   �  7   �  .     5   ;  +   q     �     �     �  ,   �          -  9   :  V   t  C   �  �    �  �  �  �  �  W     �"     #     #     #  $   8#     ]#  H   f#     �#     �#     �#     �#  8   $  (   ?$     h$     $     �$     �$     �$     �$     �$     �$     �$  !   %  1   $%     V%  I   k%     �%     �%     �%    �%  +   '  �   .'     �'  $   �'     �'     (     (     9(     E(  /   Q(     �(  I   �(  .   �(  R   )  .   T)  -   �)  @   �)  '   �)  B   *  A   ]*  (   �*  +   �*     �*  -   +  !   4+     V+  ?   b+  T   �+     �+     !      	   2   ?   
   /                 +   (   *       1            )   >           8          9   $              7             .       '   6          %   <             3   ,          ;       =   &                              -           #      "          :      5      0                                   4               <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body style=" font-family:'hlv'; font-size:9pt; font-weight:400; font-style:normal;">
<p style="-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><br /></p></body></html> <b>Security notice:</b>
                             According to a security audit by Taylor Hornby (Defuse Security),
                             the current implementation of Encfs is vulnerable or potentially vulnerable
                             to multiple types of attacks.
                             For example, an attacker with read/write access
                             to encrypted data might lower the decryption complexity
                             for subsequently encrypted data without this being noticed by a legitimate user,
                             or might use timing analysis to deduce information.
                             <br /><br />
                             This means that you should not synchronize
                             the encrypted data to a cloud storage service,
                             or use it in other circumstances where the attacker
                             can frequently access the encrypted data.
                             <br /><br />
                             See <a href='http://defuse.ca/audits/encfs.htm'>defuse.ca/audits/encfs.htm</a> for more information. <b>Security notice:</b>
                             CryFS encrypts your files, so you can safely store them anywhere.
                             It works well together with cloud services like Dropbox, iCloud, OneDrive and others.
                             <br /><br />
                             Unlike some other file-system overlay solutions,
                             it does not expose the directory structure,
                             the number of files nor the file sizes
                             through the encrypted data format.
                             <br /><br />
                             One important thing to note is that,
                             while CryFS is considered safe,
                             there is no independent security audit
                             which confirms this. @title:windowCreate a New Vault Activities Backend: Can not create the mount point Can not open an unknown vault. Change Choose the encryption system you want to use for this vault: Choose the used cypher: Close the vault Configure Configure Vault... Configured backend can not be instantiated: %1 Configured backend does not exist: %1 Correct version found Create Create a New Vault... CryFS Device is already open Device is not open Dialog Do not show this notice again EncFS Encrypted data location Failed to create directories, check your permissions Failed to execute Failed to fetch the list of applications using this vault Failed to open: %1 Forcefully close  General If you limit this vault only to certain activities, it will be shown in the applet only when you are in those activities. Furthermore, when you switch to an activity it should not be available in, it will automatically be closed. Limit to the selected activities: Mind that there is no way to recover a forgotten password. If you forget the password, your data is as good as gone. Mount point Mount point is not specified Mount point: Next Open with File Manager Password: Plasma Vault Please enter the password to open this vault: Previous The mount point directory is not empty, refusing to open the vault The specified backend is not available The vault configuration can only be changed while it is closed. The vault is unknown, can not close it. The vault is unknown, can not destroy it. This device is already registered. Can not recreate it. This directory already contains encrypted data Unable to close the vault, an application is using it Unable to close the vault, it is used by %1 Unable to detect the version Unable to perform the operation Unknown device Unknown error, unable to create the backend. Use the default cipher Vaul&t name: Wrong version installed. The required version is %1.%2.%3 You need to select empty directories for the encrypted storage and for the mount point formatting the message for a command, as in encfs: not found%1: %2 Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-11 05:53+0100
PO-Revision-Date: 2017-12-10 23:19+0100
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body style=" font-family:'hlv'; font-size:9pt; font-weight:400; font-style:normal;">
<p style="-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><br /></p></body></html> <b>Beveiligingsopmerking:</b>
                             Volgens een beveiligingsaudit door Taylor Hornby (Defuse Security),
                             is de huidige implementatie van Encfs kwetsbaar of potentieel kwetsbaar
                             voor meerdere typen aanvallen.
                             Een aanvaller kan bijvoorbeeld met lees-/schrijftoegang
                             tot versleutelde gegevens de complexiteit van de ontsleuteling
                             voor volgende versleuteling van gegevens verlagen zonder dat dat wordt opgemerkt door een legitieme gebruiker,
                             of kan tijdanalyse gebruiken om informatie af te leiden.
                             <br /><br />
                             Dit betekent dat u de versleutelde gegevens
                             niet moet synchroniseren naar een cloud-opslagservice,
                             of het in andere omstandigheden gebruiken waar de aanvaller
                             frequent toegang heeft tot de versleutelde gegevens.
                             <br /><br />
                             See <a href='http://defuse.ca/audits/encfs.htm'>defuse.ca/audits/encfs.htm</a> voor meer informatie. <b>Beveiligingsopmerking:</b>
                             CryFS versleutelt uw bestanden, zodat u ze veilig overal kan opslaan.
                             Het werkt goed samen met cloud-services zoals Dropbox, iCloud, OneDrive en anderen.
                             <br /><br />
                             Anders dan met sommige andere overlay-oplossingen voor bestandssystemen,
                             maakt het de mappenstructuur niet zichtbaar,
                             het aantal bestanden noch de groottes van bestanden
                             via het versleutelde gegevensformaat.
                             <br /><br />
                             Een belangrijk iets om op te merken is dat,
                             terwijl CryFS als velig wordt beschouwd,
                             is er geen onafhankelijke veiligheidsaudit
                             die dit bevestigt. Een nieuwe kluis aanmaken Activiteiten Backend: Kan aankoppelpunt niet aanmaken Kan een onbekende kluis niet openen. Wijzigen Kies het systeem voor vercijfering dat u voor deze kluis wilt gebruiken: Kies de gebruikte vercijfering: De kluis sluiten Configureren De kluis configureren... Geconfigureerde backend kan niet geïnitieerd worden: %1 Geconfigureerde backend bestaat niet: %1 Juiste versie gevonden Aanmaken Een nieuw kluis aanmaken... CryFS Apparaat is al geopend Apparaat is niet open Dialoog Deze melding niet meer tonen EncFS Versleutelde locatie van gegevens Mappen aanmaken is mislukt, controleer uw rechten Uitvoeren is mislukt Ophalen van de lijst met toepassingen die deze kluis gebruiken is mislukt Openen is mislukt: %1 Afgedwongen sluiten  Algemeen Als u deze kluis beperkt tot alleen bepaalde activiteiten, zal het in de applet alleen getoond worden wanneer u zich in die activiteiten bevindt. Verder, wanneer u omschakelt naar een activiteit waarin het niet beschikbaar zou moeten zijn, dan zal het automatisch gesloten worden. Tot de geselecteerde activiteiten beperken: Bedenk dat er geen manier is om te herstellen van een vergeten wachtwoord. Als u het wachtwoord vergeet, dan zijn uw gegevens zo goed als verdwenen. Aankoppelpunt Aankoppelpunt is niet gespecificeerd Aankoppelpunt (mount point): Volgende Openen met bestandsbeheerder Wachtwoord: Plasmakluis Geef het wachtwoord op om deze kluis te openen: Vorige De map voor aankoppelen is niet leeg, openen van de kluis wordt geweigerd De gespecificeerde backend is niet beschikbaar De configuratie van de kluis kan alleen gewijzigd worden wanneer deze is gesloten. De kluis is niet bekend, kan het niet sluiten. De kluis is niet bekend, kan het vernietigen. Het apparaat is al geregistreerd. Kan het niet opnieuw aanmaken. Deze map bevat al versleutelde gegevens Niet in staat om de kluis te sluiten, een toepassing gebruikt deze Niet in staat om de kluis te sluiten, deze wordt gebruikt door %1 Niet in staat om de versie te detecteren Niet in staat om de bewerking uit te voeren Onbekend apparaat Onbekende fout, kan de backend niet aanmaken. Gebruik de standaard vercijfering &Kluisnaam: Verkeerde versie geïnstalleerd. De vereiste versie is %1.%2.%3 U moet een lege map selecteren voor de versleutelde opslag en voor het aankoppelpunt %1: %2 