��          �   %   �      @     A  !   Y      {     �      �     �     �  +        =     N     [  (   z     �  ,   �  7   �     !  7   :     r  ]   �  1   �  	   "     ,  "   ?  5   b  �  �     6  %   R  '   x  !   �     �  !   �  "   �  3        P     g     t  3   �  (   �  5   �  @   	     X	  ?   o	     �	  m   �	  5   7
     m
     |
  -   �
  =   �
                    
         	                                                                                                 Cannot start initramfs. Cannot start update-alternatives. Configure Plymouth Splash Screen Download New Splash Screens EMAIL OF TRANSLATORSYour emails Get New Boot Splash Screens... Initramfs failed to run. Initramfs returned with error condition %1. Install a theme. Marco Martin NAME OF TRANSLATORSYour names No theme specified in helper parameters. Plymouth theme installer Select a global splash screen for the system The theme to install, must be an existing archive file. Theme %1 does not exist. Theme corrupted: .plymouth file not found inside theme. Theme folder %1 does not exist. This module lets you configure the look of the whole workspace with some ready to go presets. Unable to authenticate/execute the action: %1, %2 Uninstall Uninstall a theme. update-alternatives failed to run. update-alternatives returned with error condition %1. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-28 03:05+0200
PO-Revision-Date: 2017-01-06 17:01+0100
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Kan initramfs niet starten. Kan update-alternatives niet starten. Opstartscherm van Plymouth configureren Nieuwe opstartschermen downloaden freekdekruijf@kde.nl Nieuwe opstartschermen ophalen... Uitvoeren van initramfs is mislukt Uitvoeren van initramfs kwam terug met foutcode %1. Een thema installeren. Marco Martin Freek de Kruijf Geen thema gespecificeerd in parameters van helper. Installatieprogramma voor Plymouth-thema Een globaal opstartscherm voor het systeem selecteren Het te installeren thema, moet een bestaand archiefbestand zijn. Thema %1 bestaat niet. Thema beschadigd: .plymouth bestand niet binnen thema gevonden. Themamap %1 bestaat niet. Deze module laat u het uiterlijk van de gehele werkruimte instellen met enige gereedstaande voorinstellingen. De actie %1, %2 kan niet goegekeurd/uitgevoerd worden Deïnstalleren Een thema deïnstalleren. uitvoeren van update-alternatives is mislukt. uitvoeren van update-alternatives kwam terug met foutcode %1. 