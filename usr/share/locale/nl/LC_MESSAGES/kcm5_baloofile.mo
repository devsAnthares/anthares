��          �            h     i     �  #   �      �      �     �  J        [  &   y     �  T   �       *   $     O  �  ]     �       #   6     Z     w     �  O   �     �  -        @  V   P     �  .   �     �                            	                                             
       Also index file content Configure File Search Copyright 2007-2010 Sebastian Trüg Do not search in these locations EMAIL OF TRANSLATORSYour emails Enable File Search File Search helps you quickly locate all your files based on their content Folder %1 is already excluded Folder's parent %1 is already excluded NAME OF TRANSLATORSYour names Not allowed to exclude root folder, please disable File Search if you do not want it Sebastian Trüg Select the folder which should be excluded Vishesh Handa Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2017-10-17 10:00+0100
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Ook de bestandsinhoud indexeren Bestanden zoeken instellen Copyright 2007-2010 Sebastian Trüg Niet in deze locaties zoeken freekdekruijf@kde.nl Bestanden zoeken inschakelen Bestanden zoeken helpt u snel al uw bestanden te vinden op basis van hun inhoud Map %1 is al uitgesloten De bovenliggende map van %1 is al uitgesloten Freek de Kruijf Hoofdmap uitsluiten is niet toegestaan, schakel Bestand zoeken uit als u dat niet wilt Sebastian Trüg Selecteer de map die uitgesloten moeten worden Vishesh Handa 