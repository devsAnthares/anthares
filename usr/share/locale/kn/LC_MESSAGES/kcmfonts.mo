��          �      <      �     �  L   �  /        3     H     L     f     s     �     �     �  "   �     �  6   �  *   %     P     ]  �  j     �    �  �   
  ]   �     �  y        �  E   �  S   �  �   5     �  �   �  T   p	  �   �	  �   �
      R      s                                              	                                                   
     to  <p>Some changes such as DPI will only affect newly started applications.</p> A non-proportional font (i.e. typewriter font). Ad&just All Fonts... BGR Click to change all fonts Configure... E&xclude range: Font Settings Changed Force fonts DPI: RGB Used by menu bars and popup menus. Used by the window titlebar. Used for normal text (e.g. button labels, list items). Used to display text beside toolbar icons. Vertical BGR Vertical RGB Project-Id-Version: kcmfonts
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-24 05:54+0100
PO-Revision-Date: 2007-09-14 12:03+0530
Last-Translator: Manu B <iammanu@gmail.com>
Language-Team: Kannada
Language: kn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=n != 1;
 ಗೆ <p>ಚುಕ್ಕಿಗಳು ಪ್ರತಿ ಅಂಗುಲಕ್ಕೆ ರೀತಿಯ ಬದಲಾವಣೆಗಳು ಕೇವಲ ಹೊಸದಾಗಿ ಪ್ರಾರಂಭವಾದ ಅನ್ವಯಗಳಲ್ಲಿ ಪರಿಣಾಮ ಬೀರುತ್ತವೆ.</p> ಅನುಗುಣವಿಲ್ಲದ ಅಕ್ಷರಶೈಲಿ (ಬೆರಳಚ್ಚು ಯಂತ್ರ ಅಕ್ಷರಶೈಲಿ). ಎಲ್ಲಾ ಅಕ್ಷರಶೈಲಿಗಳನ್ನು &ಸರಿಹೊಂದಿಸು ನೀ.ಹ.ಕೆ(BGR) ಎಲ್ಲಾ ಅಕ್ಷರಶೈಲಿಗಳನ್ನು ಬದಲಾಯಿಸಲು ಕ್ಲಿಕ್ ಮಾಡಿ ಸಂರಚಿಸು... &ಬಹಿಷ್ಕರಣ ವ್ಯಾಪ್ತಿ (ರೇಂಜ್): ಅಕ್ಷರಶೈಲಿ ಸಂಯೋಜನೆಗಳು ಬದಲಾಗಿವೆ ಪ್ರಬಲ ಅಕ್ಷರಶೈಲಿಗಳಲ್ಲಿ ಚುಕ್ಕಿಗಳು ಪ್ರತಿ ಅಂಗುಲಕ್ಕೆ: ಕೆ.ಹ.ನೀ(RGB) ಪರಿವಿಡಿ ಪಟ್ಟಿಗಳು ಮತ್ತು ಪುಟಿಕೆ (ಪಾಪಪ್) ಪರಿವಿಡಿಗಳಲ್ಲಿ ಬಳಸುವ. ಕಿಟಕಿ ಶೀಷಿ೯ಕೆಪಟ್ಟಿಯಲ್ಲಿ ಬಳಸುವ. ಸಾಧಾರಣ ಪಠ್ಯಕ್ಕೆ ಬಳಸಲಾದ(ಉದಾ. ಗುಂಡಿ ಗುರುತುಪಟ್ಟಿ (ಲೇಬಲ್), ಯಾದಿಯಲ್ಲಿರುವ (ಲಿಸ್ಟ) ವಸ್ತುಗಳು). ಸಲಕರಣಾಪಟ್ಟಿಯ ಚಿಹ್ನೆಯ ಬದಿಯಲ್ಲಿರುವ ಪಠ್ಯವನ್ನು ಪ್ರದಶಿ೯ಸಲು ಬಳಸಲಾದ. ಲಂಬ ನೀ.ಹ.ಕೆ(BGR) ಲಂಬ ಕೆ.ಹ.ನೀ(RGB) 