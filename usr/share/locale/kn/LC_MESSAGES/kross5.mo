��            )   �      �  &   �     �     �     �     �      �               .     6  ,   S  3   �     �     �     �     �     �  '        4     S     Y     o     �     �     �     �     �  &   �     �  �  �     �  i   �     #  +   9     e  <        �  {   �     Q  x   m  �   �  �   �	  �   �
  �   O  
   �     �     �  t     �   �       I   *  w   t     �  ~        �  ]   �       �        �                                       
                      	                                                                            @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2010-06-24 18:32+0530
Last-Translator: Shankar Prasad <svenkate@redhat.com>
Language-Team: kn_IN <kde-i18n-doc@kde.org>
Language: kn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;

X-Generator: Lokalize 1.0
 ಸಾಮಾನ್ಯ ಹೊಸ ವಿಧಿಗುಚ್ಛವನ್ನು (ಸ್ಕ್ರಿಪ್ಟ್) ಸೇರಿಸು. ಸೇರಿಸು... ರದ್ದುಗೊಳಿಸಬೇಕೆ? ಅಭಿಪ್ರಾಯ: umeshrs@gmail.com, siddharudh@gmail.com, svenkate@redhat.com ಸಂಪಾದಿಸು ಆರಿಸಲಾದ ವಿಧಿಗುಚ್ಛವನ್ನು (ಸ್ಕ್ರಿಪ್ಟ್) ಸಂಪಾದಿಸು. ಸಂಪಾದಿಸು... ಆರಿಸಲಾದ ವಿಧಿಗುಚ್ಛವನ್ನು (ಸ್ಕ್ರಿಪ್ಟ್) ಚಾಲಯಿಸು. "%1" ವಿವರಣಕಾರಕ್ಕೆ (ಇಂಟರ್ಪ್ರಿಟರ್) ವಿಧಿಗುಚ್ಛ (ಸ್ಕ್ರಿಪ್ಟ್) ಸೃಷ್ಟಿಸುವುದು ವಿಫಲವಾಯಿತು ವಿಧಿಗುಚ್ಛಕಡತ (ಸ್ಕ್ರಿಪ್ಟ್ ಫೈಲ್) "%1" ಕ್ಕೆ ಯಾವುದೇ ವಿವರಣಕಾರ (ಇಂಟರ್ಪ್ರಿಟರ್) ಅನ್ನು ಕಂಡುಹಿಡಿಯಲಾಗಲಿಲ್ಲ "%1" ವಿವರಣಕಾರವನ್ನು (ಇಂಟರ್ಪ್ರಿಟರ್) ಉತ್ಥಾಪಿಸುವುದು ವಿಫಲವಾಯಿತು ವಿಧಿಗುಚ್ಛಕಡತ (ಸ್ಕ್ರಿಪ್ಟ್ ಫೈಲ್) "%1" ಅನ್ನು ತೆರೆಯಲಾಗಲಿಲ್ಲ ಕಡತ: ಚಿಹ್ನೆ: ವಿವರಣಕಾರ: ರೂಬಿ ವಿವರಣಕಾರದ (ಇಂಟರ್ಪ್ರೆಟರ್) ಸುರಕ್ಷತಾಸ್ತರ ಉಮೇಶ್ ರುದ್ರಪಟ್ಟಣ, ಸಿದ್ಧಾರೂಢ ಪಿ ಟಿ, ಶಂಕರ ಪ್ರಸಾದ್ ಎಂ ವಿ ಹೆಸರು: "%1" ಹೆಸರಿನ ಯಾವುದೇ ಕಾರ್ಯವಿಲ್ಲ "%1" ಹೆಸರಿನ ಯಾವುದೇ ವಿವರಣಕಾರ (ಇಂಟರ್ಪ್ರಿಟರ್) ಇಲ್ಲ ತೆಗೆದುಹಾಕು ಆರಿಸಿದ ವಿಧಿಗುಚ್ಛವನ್ನು (ಸ್ಕ್ರಿಪ್ಟ್) ತೆಗೆದುಹಾಕು. ನಿರ್ವಹಿಸು ವಿಧಿಗುಚ್ಛ ಕಡತ " %1" ಅಸ್ತಿತ್ವದಲ್ಲಿಲ್ಲ. ನಿಲ್ಲಿಸು ಆರಿಸಲಾದ ವಿಧಿಗುಚ್ಛದ (ಸ್ಕ್ರಿಪ್ಟ್) ಚಾಲನೆಯನ್ನು ನಿಲ್ಲಿಸು. ಪಠ್ಯ: 