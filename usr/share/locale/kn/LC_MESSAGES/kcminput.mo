��    %      D  5   l      @     A     N     T  
   c     n     �  '   �      �     �     �  9         :     C  
   R     ]     i  W   v     �     �  E   �     0     G     \      m     �  �   �  }   *     �     �     �     �     �                     :  �  F     �  !   �  .   	  (   M	  !   v	     �	  �   �	  8   J
  2   �
     �
  �   �
     �  +   �  %   �  "   �        �   =  "   �  -        D  6   E  -   |  -   �     �     �  s    6  z     �     �     �  .   �       0   <  (   m  Y   �  "   �        $   
                      !                                           	            "                               #                 %                                            line  lines  msec  pixel  pixels  pixel/sec &Acceleration delay: &General &Single-click to open files and folders (c) 1997 - 2005 Mouse developers Acceleration &profile: Acceleration &time: Activates and opens a file or folder with a single click. Advanced Bernd Gehrmann Brad Hards Brad Hughes Button Order Change the direction of scrolling for the mouse wheel or the 4th and 5th mouse buttons. David Faure Dirk A. Mueller Dou&ble-click to open files and folders (select icons on first click) Double click interval: Drag start distance: Drag start time: EMAIL OF TRANSLATORSYour emails Icons If you click with the mouse (e.g. in a multi-line editor) and begin to move the mouse within the drag start time, a drag operation will be initiated. If you click with the mouse and begin to move the mouse at least the drag start distance, a drag operation will be initiated. Ma&ximum speed: Mouse NAME OF TRANSLATORSYour names Patrick Dowler Pointer acceleration: R&epeat interval: Ralf Nolden Re&verse scroll direction Rik Hemsley Project-Id-Version: kcminput
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-05 03:17+0100
PO-Revision-Date: 2008-01-11 11:48+0530
Last-Translator: Umesh Rudrapatna <umeshrs@gmail.com>
Language-Team: Kannada <kde-l10n-kn@kde.org>
Language: kn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;

X-Generator: KBabel 1.11.4
 ಸಾಲು  ಮಿಲಿಸೆಕೆಂಡ್ ಚುಕ್ಕಿ ಚುಕ್ಕಿಗಳು ಚುಕ್ಕಿ/ಸೆಕೆಂಡು ತ್ವ&ರಣ ವಿಳಂಬ: ಸಾ(&ಮ)ಮಾನ್ಯ ಕಡತಗಳನ್ನು ಮತ್ತು ಕಡತಕೋಶಗಳನ್ನು ತೆರೆಯಲು ಒಂದುಬಾರಿ(&ದ)-ಅದುಮಿ (c) 1997 - 2005 ಮೌಸ್ ಡೆವೆಲಪರ್ಸ ತ್ವರಣ (&ವ)ವೈಶಿಷ್ಟ್ಯ: ತ್ವರಣ &ಸಮಯ: ಕಡತ ಯಾ ಕಡತಕೋಶವನ್ನು ಒಂದೇ ಕ್ಲಿಕ್ಕಲ್ಲಿ ತೆರೆಯುತ್ತದೆ ಮತ್ತು ಸಕ್ರಿಯಗೊಳಿಸುತ್ತದೆ. ಪ್ರೌಢ ಬರ್ನ್ಡ ಗೆಹರ್ಮನ್ ಬ್ರಾಡ್ ಹಾಡ್ಸ೯ ಬ್ರಾಡ್ ಯೂಜ್ಸ ಗುಂಡಿ ಕ್ರಮ ಮೂಷಕ ಗಾಲಿಯ ಸುರುಳುವಿಕೆ ಯಾ 4ನೇ ಮತ್ತು 5ನೇ ಮೂಷಕ ಗುಂಡಿಗಳ ಧಿಕ್ಕನ್ನು ಬದಲಿಸಿ. ಡೇವಿಡ್ ಫಾವ್೯ ಡರ್ಕ ಎ. ಮ್ಯುಲ್ಲರ್ ಕಡತಗಳನ್ನು ಮತ್ತು ಕಡತಕೋಶಗಳನ್ನು ತೆರೆಯಲು ಇಬ್ಬಾರಿ(&ರ)-ಅದುಮಿ(ಮೊದಲ ಅದುಮುವಿಕೆಯಲ್ಲಿ ಚಿಹ್ನೆಗಳನ್ನು ಆರಿಸಿ) ಜೋಡಿ ಕ್ಲಿಕ್ ಮಧ್ಯಂತರ: ಎಳೆತ ಪ್ರಾರಂಭ ದೂರ: ಎಳೆತ ಪ್ರಾರಂಭ ಸಮಯ: iammanu@gmail.com ಚಿಹ್ನೆಗಳು ನೀವು ಮೂಷಕದಲ್ಲಿ ಕ್ಲಿಕ್ ಮಾಡಿ(ಉದಾ. ಬಹು-ಸಾಲು ಸಂಪಾದಕದಲ್ಲಿ) ಅದನ್ನು ಎಳೆತ ಪ್ರಾರಂಭ ಸಮಯದ ಒಳಗಾಗಿ ಸರಿಸಿದರೆ, ಎಳೆತ ಕಾರ್ಯಾಚರಣೆಯನ್ನು ಮೊದಲುಗೊಳಿಸಲಾಗುತ್ತದೆ. ನೀವು ಮೂಷಕದಲ್ಲಿ ಕ್ಲಿಕ್ ಮಾಡಿ ಅದನ್ನು ಎಳೆತ ಪ್ರಾರಂಭ ದೂರದಷ್ಟಾದರೂಗಿ ಸರಿಸಿದರೆ, ಎಳೆತ ಕಾರ್ಯಾಚರಣೆಯನ್ನು ಮೊದಲುಗೊಳಿಸಲಾಗುತ್ತದೆ. &ಗರಿಷ್ಠ ವೇಗ: ಮೂಷಕ ಮನು .ಬಿ ಪ್ಯಾಟ್ರಿಕ್ ಡೌಲರ್ ಸೂಚಿ ತ್ವರಣ: ಪು&ನರಾವರ್ತನೆ ಅವಧಿ: ರಾಲ್ಫ ನಾಲ್ಡೆನ್ ಸುರುಳು ಧಿಕ್ಕನ್ನು ತಿರುಮುರುಗಾಗಿಸು ರಿಕ್ ಹೆಮ್ಸಲೇ 