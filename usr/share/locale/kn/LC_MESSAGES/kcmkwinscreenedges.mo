��          �            h     i     m     �     �     �  S   �  w     M   �  	   �     �     �     	          6  �  H     �  E   �  X     <   t  4   �  �   �  �   �  �   {  %   *  J   P  +   �  7   �  0   �  (   0	                 
                             	                                   ms &Reactivation delay: &Switch desktop on edge: Activation &delay: Always Enabled Amount of time required after triggering an action until the next trigger can occur Amount of time required for the mouse cursor to be pushed against the edge of the screen before the action is triggered Change desktop when the mouse cursor is pushed against the edge of the screen No Action Only When Moving Windows Other Settings Show Desktop Switch desktop on edgeDisabled Window Management Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-30 03:13+0100
PO-Revision-Date: 2010-01-21 12:38+0530
Last-Translator: Shankar Prasad <svenkate@redhat.com>
Language-Team: kn-IN <>
Language: kn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=2; plural=n != 1;
  ms ಮರುಸಕ್ರಿಯಗೊಳಿಕೆಯ ವಿಳಂಬ(&R): ಗಣಕತೆರೆಯನ್ನು ಬದಿಯಿಂದ ಬದಲಾಯಿಸು(&S): ಸಕ್ರಿಯಗೊಳಿಕೆಯ ವಿಳಂಬ(&d): ಯಾವಾಗಲೂ ಶಕ್ತಗೊಳಿಸು ಕ್ರಿಯೆಯನ್ನು ಚಾಲನೆಗೊಳಿಸಿದ ನಂತರ ಮುಂದಿನ ಕ್ರಿಯೆಯನ್ನು ಪ್ರಚೋದಿಸಲು ಬೇಕಿರುವ ಸಮಯ ಕ್ರಿಯೆಯನ್ನು ಪ್ರಚೋದಿಸಲು, ಮೌಸ್ ಸೂಚಕವನ್ನು ತೆರೆಯ ಬದಿಯಲ್ಲಿ ಒತ್ತಿಹಿಡಿಯಬೇಕಾದ ಸಮಯ. ಮೌಸ್‌ನ ಸೂಚಕವನ್ನು ತೆರೆಯ ಅಂಚಿಗೆ ಕೊಂಡೊಯ್ದಾಗ ಗಣಕತೆರೆಯನ್ನು ಬದಲಾಯಿಸು ಏನನ್ನೂ ಮಾಡಬೇಡ ಕಿಟಕಿಗಳನ್ನು ಚಲಿಸಿದಾಗ ಮಾತ್ರ ಇತರೆ ಸಿದ್ಧತೆಗಳು ಗಣಕತೆರೆಯನ್ನು ತೋರಿಸು ನಿಷ್ಕ್ರಿಯಗೊಂಡಿದೆ ಕಿಟಕಿ ನಿರ್ವಹಣೆ 