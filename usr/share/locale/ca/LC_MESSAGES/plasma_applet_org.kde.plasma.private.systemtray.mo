��          �      l      �     �     �            
   #     .     :     I     Q     ]     e     v     }     �  #   �     �     �     �     �  
      �       �           (     >  
   J     U     p     ~     �     �     �     �     �     �     �     �     �               0                                                       
   	                                         (Automatic load) Always show all entries Application Status Auto Categories Close popup Communications Entries Extra Items General Hardware Control Hidden Keyboard Shortcut Miscellaneous Name of the system tray entryEntry Show hidden icons Shown Status & Notifications System Services Visibility Project-Id-Version: plasma_applet_org.kde.plasma.private.systemtray
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-03 03:15+0100
PO-Revision-Date: 2017-01-11 23:11+0100
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Generator: Lokalize 2.0
  (càrrega automàtica) Mostra sempre totes les entrades Estat de l'aplicació Automàtica Categories Tanca la finestra emergent Comunicacions Entrades Elements extres General Control del maquinari Oculta Drecera de teclat Miscel·lània Entrada Mostra les icones ocultes Mostra Estat i notificacions Serveis del sistema Visibilitat 