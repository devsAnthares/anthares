��          �      �           	          &     -     4  
   =     H     Q     Y     i  >   w     �     �     �     �  
   �     �     �     �            U   %  �  {     R     g          �     �  	   �     �     �     �     �  0   �               %     9     K     \     b     p  "   �     �  y   �                       	                                                
                                 %1 is running %1 not running &Reset &Start Advanced Appearance Command: Display Execute command Notifications Remaining time left: %1 second Remaining time left: %1 seconds Run command S&top Show notification Show seconds Show title Text: Timer Timer finished Timer is running Title: Use mouse wheel to change digits or choose from predefined timers in the context menu Project-Id-Version: plasma_applet_org.kde.plasma.timer
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2015-04-23 21:15+0200
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.4
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
 S'està executant %1 No s'està executant %1 &Inicialitza &Engega Avançat Aparença Ordre: Visualització Executa l'ordre Notificacions Temps restant: %1 segon Temps restant: %1 segons Executa una ordre A&tura Mostra notificació Mostra els segons Mostra el títol Text: Temporitzador El temporitzador ha finalitzat S'està executant el temporitzador Títol: Useu la rodeta del ratolí per canviar els dígits o escollir entre els temporitzadors predefinits en el menú contextual 