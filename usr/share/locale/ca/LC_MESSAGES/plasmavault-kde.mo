��    ?        Y         p  �  q  }  J  M  �        
   7     B     K     j     �  <   �     �     �  	   �     �  .     %   A     g     }     �     �     �     �     �     �     �     �  4        B  9   T     �     �     �  �   �  !   �  t   �     8     D     a     n     s  	   �     �  -   �     �  B   �  &     ?   B  '   �  )   �  7   �  .     5   ;  +   q     �     �     �  ,   �          -  9   :  V   t  C   �  �    �  �  �  �  �  u     �"  
   #     #  '   #  *   G#     r#  B   y#     �#     �#  	   �#     �#  <   	$  $   F$     k$     �$     �$     �$     �$     �$     �$  $   �$     %  "   %  A   @%     �%  E   �%     �%     �%     &  �   &  &   '  �   +'     �'  '   �'     �'     (     (     ,(     9(  >   J(     �(  I   �(  '   �(  F   )  +   K)  -   w)  F   �)  ,   �)  ;   *  /   U*  !   �*  #   �*     �*  0   �*     +     .+  I   @+  c   �+     �+     !      	   2   ?   
   /                 +   (   *       1            )   >           8          9   $              7             .       '   6          %   <             3   ,          ;       =   &                              -           #      "          :      5      0                                   4               <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body style=" font-family:'hlv'; font-size:9pt; font-weight:400; font-style:normal;">
<p style="-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><br /></p></body></html> <b>Security notice:</b>
                             According to a security audit by Taylor Hornby (Defuse Security),
                             the current implementation of Encfs is vulnerable or potentially vulnerable
                             to multiple types of attacks.
                             For example, an attacker with read/write access
                             to encrypted data might lower the decryption complexity
                             for subsequently encrypted data without this being noticed by a legitimate user,
                             or might use timing analysis to deduce information.
                             <br /><br />
                             This means that you should not synchronize
                             the encrypted data to a cloud storage service,
                             or use it in other circumstances where the attacker
                             can frequently access the encrypted data.
                             <br /><br />
                             See <a href='http://defuse.ca/audits/encfs.htm'>defuse.ca/audits/encfs.htm</a> for more information. <b>Security notice:</b>
                             CryFS encrypts your files, so you can safely store them anywhere.
                             It works well together with cloud services like Dropbox, iCloud, OneDrive and others.
                             <br /><br />
                             Unlike some other file-system overlay solutions,
                             it does not expose the directory structure,
                             the number of files nor the file sizes
                             through the encrypted data format.
                             <br /><br />
                             One important thing to note is that,
                             while CryFS is considered safe,
                             there is no independent security audit
                             which confirms this. @title:windowCreate a New Vault Activities Backend: Can not create the mount point Can not open an unknown vault. Change Choose the encryption system you want to use for this vault: Choose the used cypher: Close the vault Configure Configure Vault... Configured backend can not be instantiated: %1 Configured backend does not exist: %1 Correct version found Create Create a New Vault... CryFS Device is already open Device is not open Dialog Do not show this notice again EncFS Encrypted data location Failed to create directories, check your permissions Failed to execute Failed to fetch the list of applications using this vault Failed to open: %1 Forcefully close  General If you limit this vault only to certain activities, it will be shown in the applet only when you are in those activities. Furthermore, when you switch to an activity it should not be available in, it will automatically be closed. Limit to the selected activities: Mind that there is no way to recover a forgotten password. If you forget the password, your data is as good as gone. Mount point Mount point is not specified Mount point: Next Open with File Manager Password: Plasma Vault Please enter the password to open this vault: Previous The mount point directory is not empty, refusing to open the vault The specified backend is not available The vault configuration can only be changed while it is closed. The vault is unknown, can not close it. The vault is unknown, can not destroy it. This device is already registered. Can not recreate it. This directory already contains encrypted data Unable to close the vault, an application is using it Unable to close the vault, it is used by %1 Unable to detect the version Unable to perform the operation Unknown device Unknown error, unable to create the backend. Use the default cipher Vaul&t name: Wrong version installed. The required version is %1.%2.%3 You need to select empty directories for the encrypted storage and for the mount point formatting the message for a command, as in encfs: not found%1: %2 Project-Id-Version: plasmavault-kde
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-11 05:53+0100
PO-Revision-Date: 2017-12-16 17:40+0100
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
X-Accelerator-Marker: &
 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body style=" font-family:'hlv'; font-size:9pt; font-weight:400; font-style:normal;">
<p style="-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><br /></p></body></html> <b>Avís de seguretat:</b>
                             D'acord amb una auditoria de seguretat feta per en Taylor Hornby
                             (Defuse Security), la implementació actual d'Encfs és vulnerable o
                             potencialment vulnerable a múltiples tipus d'atacs. Per exemple, un
                             atacant amb accés de lectura/escriptura a les dades encriptades podria
                             reduir la complexitat del desencriptatge de les dades encriptades,
                             de manera que després puguin ser vistes per un usuari legitimat, o
                             podrà utilitzar l'anàlisi temporal per a deduir la informació.
                             <br /><br />
                             Això vol dir que no heu de sincronitzar dades encriptades amb un servei
                             d'emmagatzematge en el núvol, o utilitzar-lo en altres circumstàncies on
                             l'atacant pugui accedir amb freqüència a les dades encriptades.
                             <br /><br />
                             Per a més informació vegeu <a href='http://defuse.ca/audits/encfs.htm'>defuse.ca/audits/encfs.htm</a>. <b>Avís de seguretat:</b>
                             CryFS encripta els vostres fitxers, de manera que els podreu emmagatzemar de
                             forma segura en qualsevol lloc. Treballa bé juntament amb serveis en el núvol
                             com Dropbox, iCloud, OneDrive i altres.
                            <br /><br />
                             A diferència d'altres solucions de superposició del sistema de fitxers,
                             aquest no exposa l'estructura dels directoris, el nombre de fitxers ni les
                             mides dels fitxers mitjançant el format de les dades encriptades.
                             <br /><br />
                             Una cosa important a tenir en compte és que, si bé CryFS es considera segur,
                             no hi ha una auditoria de seguretat independent que ho confirmi. Crea una volta nova Activitats Dorsal: No s'ha pogut crear el punt de muntatge No s'ha pogut obrir una volta desconeguda. Canvia Trieu el sistema d'encriptatge que voleu usar per a aquesta volta: Trieu el xifrat que s'usarà: Tanca la volta Configura Configura el Volta... No s'ha pogut crear una instància del dorsal configurat: %1 El dorsal configurat no existeix: %1 S'ha trobat la versió correcta Crea Crea una volta nova... CryFS El dispositiu ja es troba obert El dispositiu no està obert Diàleg No mostris aquest avís un altre cop EncFS Ubicació de les dades encriptades Ha fallat en crear els directoris, comproveu els vostres permisos Ha fallat en executar Ha fallat en recuperar la llista d'aplicacions que usen aquesta volta Ha fallat en obrir: %1 Tanca forçadament General Si limiteu aquesta volta només a certes activitats, només es mostrarà a la miniaplicació quan esteu en alguna d'aquestes activitats. D'altra banda, quan canvieu a una activitat que no hauria d'estar disponible, es tancarà automàticament. Limita a les activitats seleccionades: Penseu que no hi haurà manera de recuperar una contrasenya oblidada. Si oblideu la contrasenya, podreu considerar perdudes les vostres dades. Punt de muntatge No s'ha especificat el punt de muntatge Punt de muntatge: Següent Obre amb el gestor de fitxers Contrasenya: Volta del Plasma Si us plau, introduïu la contrasenya per obrir aquesta volta: Anterior El directori del punt de muntatge no està buit, es refusa obrir la volta El dorsal especificat no és disponible La configuració de la volta només es pot canviar quan està tancada. La volta és desconeguda, no es pot tancar. La volta és desconeguda, no es pot destruir. Aquest dispositiu ja està registrat. No s'ha pogut tornar a crear-lo. Aquest directori ja conté dades encriptades No s'ha pogut tancar la volta, l'està usant una aplicació No s'ha pogut tancar la volta, l'està usant %1 No s'ha pogut detectar la versió No s'ha pogut realitzar l'operació Dispositiu desconegut Error desconegut, no s'ha pogut crear el dorsal. Usa la xifra predeterminada Nom de la vol&ta: La versió instal·lada és incorrecta. La versió requerida és %1.%2.%3 Cal que seleccioneu els directoris buits per a l'emmagatzematge encriptat i per al punt de muntatge %1: %2 