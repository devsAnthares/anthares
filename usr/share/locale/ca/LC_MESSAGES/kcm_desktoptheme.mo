��          �      �       H     I     a      m     �     �     �  
   �     �  &   �          .     L  1   b  �  �  #   Y     }     �     �     �     �     �     �  +   �  %   '  &   M  !   t  8   �         	                                    
                    Configure Desktop Theme David Rosca EMAIL OF TRANSLATORSYour emails Get New Themes... Install from File... NAME OF TRANSLATORSYour names Open Theme Remove Theme Theme Files (*.zip *.tar.gz *.tar.bz2) Theme installation failed. Theme installed successfully. Theme removal failed. This module lets you configure the desktop theme. Project-Id-Version: kcm_desktoptheme
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-22 05:21+0100
PO-Revision-Date: 2017-10-17 20:24+0100
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
 Configuració del tema d'escriptori David Rosca txemaq@gmail.com Obtén temes nous... Instal·la des d'un fitxer... Josep Ma. Ferrer Obre un tema Elimina el tema Fitxers de temes (*.zip *.tar.gz *.tar.bz2) Ha fallat la instal·lació del tema. El tema s'ha instal·lat correctament. Ha fallat l'eliminació del tema. Aquest mòdul permet configurar el tema de l'escriptori. 