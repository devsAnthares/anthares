��    
      l      �       �      �            	               .  I   3     }  	   �  �  �     e     |     �     �  )   �     �  O   �          /               
   	                           &Trigger word: Add Item Alias Alias: Character Runner Config Code Creates Characters from :q: if it is a hexadecimal code or defined alias. Delete Item Hex Code: Project-Id-Version: plasma_runner_CharacterRunner
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2010-07-30 18:11+0200
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
 Paraula d'ac&tivació: Afegeix un element Àlies Àlies: Configuració de l'executor de caràcters Codi Crea caràcters a partir de :q: si és un codi hexadecimal o un àlies definit. Elimina l'element Codi hexadecimal: 