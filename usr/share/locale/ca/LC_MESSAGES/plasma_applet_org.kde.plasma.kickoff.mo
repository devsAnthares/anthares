��            )   �      �     �  
   �  /   �  -   �          !  
   2     =     J     `  W   i     �  ,   �  	                  !     '     -  
   :     E     W     o     �     �     �     �  1   �       �       �  
   �                    4  	   J     T  "   `  	   �  |   �     
  H   #  
   l     w  	   �     �     �     �     �     �     �     �     	     	     6	  ,   M	     z	     �	                                                                                  
                                	                       %1@%2 %2@%3 (%1) @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Favorites All Applications Appearance Applications Applications updated. Computer Drag tabs between the boxes to show/hide them, or reorder the visible tabs by dragging. Edit Applications... Expand search to bookmarks, files and emails Favorites Hidden Tabs History Icon: Leave Menu Buttons Often Used On All Activities On The Current Activity Remove from Favorites Show In Favorites Show applications by name Sort alphabetically Switch tabs on hover Type is a verb here, not a nounType to search... Visible Tabs Project-Id-Version: plasma_applet_org.kde.plasma.kickoff
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2017-09-09 15:26+0100
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Generator: Lokalize 2.0
 %1@%2 %2@%3 (%1) Tria... Neteja la icona Afegeix a les preferides Totes les aplicacions Aparença Aplicacions S'han actualitzat les aplicacions. Ordinador Arrossegueu les pestanyes entre els quadres per mostrar-les/ocultar-les, o reordeneu les pestanyes visibles arrossegant-les. Edita les aplicacions... Amplia la cerca a les adreces d'interès, fitxers i correus electrònics Preferides Pestanyes ocultes Historial Icona: Surt Botons de menú Usats sovint A totes les activitats A l'activitat actual Elimina de les preferides Mostra als preferits Mostra les aplicacions per nom Ordena alfabèticament Commuta les pestanyes en passar-hi per sobre Teclegeu per cercar... Pestanyes visibles 