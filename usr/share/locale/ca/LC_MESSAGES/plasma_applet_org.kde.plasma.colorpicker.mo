��          |      �          %   !     G     U     c     u     �     �  
   �     �     �  $   �  �  �  /   �     �          !     8     U     ]     v     �     �  !   �     
   	                                                   Automatically copy color to clipboard Clear History Color Options Copy to Clipboard Default color format: General Open Color Dialog Pick Color Pick a color Show history When pressing the keyboard shortcut: Project-Id-Version: plasma_applet_org.kde.plasma.colorpicker
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-12 03:03+0200
PO-Revision-Date: 2018-01-02 22:27+0100
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Generator: Lokalize 2.0
 Copia automàticament el color al porta-retalls Neteja l'historial Opcions del color Copia al porta-retalls Format de color per defecte: General Obre el diàleg de color Selecciona un color Selecciona un color Mostra l'historial En prémer la drecera del teclat: 