��            )   �      �     �     �     �     �     �     �     �     �  0        3     G     ]     c     o     w     �  
   �     �     �     �     �     �               (     A     I     a     m  �  s  	   C  *   M     x     �     �  	   �     �     �  B   �           *     K     Q     b     i     �     �      �     �  %   �          #     A     I  "   ]     �     �     �     �                                            
                                                                                      	       %1 by %2 Add Custom Wallpaper Add Folder... Add Image... Background: Blur Centered Change every: Directory with the wallpaper to show slides from Download Wallpapers Get New Wallpapers... Hours Image Files Minutes Next Wallpaper Image Open Containing Folder Open Image Open Wallpaper Image Positioning: Recommended wallpaper file Remove wallpaper Restore wallpaper Scaled Scaled and Cropped Scaled, Keep Proportions Seconds Select Background Color Solid color Tiled Project-Id-Version: plasma_applet_org.kde.image
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:39+0100
PO-Revision-Date: 2017-11-18 10:46+0100
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
 %1 per %2 Afegeix un fons d'escriptori personalitzat Afegeix una carpeta... Afegeix una imatge... Fons: Difuminat Centrat Canvia cada: Directori amb el fons de pantalla mostrant les diapositives des de Baixa més fons d'escriptori Obtén fons d'escriptori nous... Hores Fitxers d'imatge Minuts Fons d'escriptori següent Obre la carpeta contenidora Obre una imatge Obre imatge de fons d'escriptori Posicionament: Fitxer de fons d'escriptori recomanat Elimina el fons d'escriptori Restaura el fons d'escriptori Escalat Escalat i escapçat Escalat, mantenint les proporcions Segons Selecció del color de fons Color sòlid Mosaic 