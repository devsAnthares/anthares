��            )         �     �  "   �     �  !   �  !        4  
   J     U     p     �  !   �     �  0   �  8        ?  !   ]          �     �     �     �                '  
   /  
   :  
   E  &   P     w     �  �  �     `  )   d  '   �  (   �  (   �       
   #  '   .     V  ,   l  -   �  5   �  E   �  Q   C	  +   �	  2   �	  5   �	  0   *
  :   [
  ,   �
     �
  /   �
     	               $     -  .   E  
   t  '                                                                                                    	                       
                 ms &Keyboard accelerators visibility: &Top arrow button type: Always Hide Keyboard Accelerators Always Show Keyboard Accelerators Anima&tions duration: Animations Bottom arrow button t&ype: Breeze Settings Center tabbar tabs Drag windows from all empty areas Drag windows from titlebar only Drag windows from titlebar, menubar and toolbars Draw a thin line to indicate focus in menus and menubars Draw focus indicator in lists Draw frame around dockable panels Draw frame around page titles Draw frame around side panels Draw slider tick marks Draw toolbar item separators Enable animations Enable extended resize handles Frames General No Buttons One Button Scrollbars Show Keyboard Accelerators When Needed Two Buttons W&indows' drag mode: Project-Id-Version: breeze_style_config
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:42+0200
PO-Revision-Date: 2017-10-17 20:22+0100
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Generator: Lokalize 2.0
  ms Visibilitat dels a&cceleradors de teclat: &Tipus de botó de la fletxa cap amunt: Oculta sempre els acceleradors de teclat Mostra sempre els acceleradors de teclat D&urada de les animacions: Animacions T&ipus de botó de la fletxa cap avall: Arranjament del Brisa Centra les pestanyes a la barra de pestanyes Arrossega les finestres per les àrees buides Arrossega les finestres només per la barra de títol Arrossega les finestres per les barres de títol, de menús i d'eines Dibuixa una línia fina per indicar el focus en els menús i les barres de menús Dibuixa l'indicador de focus en les llistes Dibuixa un marc al voltant dels plafons acoblables Dibuixa un marc al voltant dels títols de la pàgina Dibuixa un marc al voltant dels plafons laterals Dibuixa les marques de verificació als controls lliscants Dibuixa els separadors de les barres d'eines Activa les animacions Activa les nanses de redimensionament ampliades Marcs General Sense botons Un botó Barres de desplaçament Mostra els acceleradors de teclat quan calguin Dos botons Mode d'arrossegament de les f&inestres: 