��    &      L  5   |      P      Q     r     ~  -   �  #   �  #   �  ?     1   S     �     �     �  H   �  L   �     F  V   N  N   �     �  
                   (     >     W  2   _  
   �     �  )   �  8   �  #      .   D  -   s  D   �  6   �  0     A   N  =   �  9   �  �  	      �
       0     9   E          �     �     �     �     �     �     �       	   +     5     >     B     N     c     v     �     �     �     �     �  (   �  9        K  =   Z  >   �     �     �       
   
               '                  "                            
       $                      !                &                                      %      	           #                                %1 notification %1 notifications %1 of %2 %3 %1 running job %1 running jobs &Configure Event Notifications and Actions... 10 seconds ago, keep short10 s ago 30 seconds ago, keep short30 s ago A button tooltip; expands the item to show detailsShow Details A button tooltip; hides item detailsHide Details Clear Notifications Copy Copy Link Address Either just 1 dir or m of n dirs are being processed1 dir %2 of %1 dirs Either just 1 file or m of n files are being processed1 file %2 of %1 files History How much many bytes (or whether unit in the locale has been copied over total%1 of %2 Indicator that there are more urls in the notification than previews shown+%1 Information Job Failed Job Finished More Options... No new notifications. No notifications or jobs Open... Placeholder is job name, eg. 'Copying'%1 (Paused) Select All Show a history of notifications Show application and system notifications Speed and estimated time to completion%1 (%2 remaining) Track file transfers and other jobs Use custom position for the notification popup minutes ago, keep short%1 min ago %1 min ago notification was added n days ago, keep short%1 day ago %1 days ago notification was added yesterday, keep shortYesterday notification was just added, keep shortJust now placeholder is row description, such as Source or Destination%1: the job, which can be anything, failed to complete%1: Failed the job, which can be anything, has finished%1: Finished Project-Id-Version: plasma_applet_org.kde.plasma.notifications
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:07+0100
PO-Revision-Date: 2018-01-14 16:13+0100
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Generator: Lokalize 2.0
 %1 notificació %1 notificacions %1 de %2 %3 %1 treball en execució %1 treballs en execució &Configura les notificacions d'esdeveniments i accions... Fa 10 s Fa 30 s Mostra els detalls Oculta els detalls Neteja les notificacions Copia Copia l'adreça de l'enllaç 1 directori %2 de %1 directoris 1 fitxer %2 de %1 fitxers Historial %1 de %2 +%1 Informació El treball ha fallat Treball finalitzat Més opcions... Sense notificacions noves. Sense notificacions ni treballs Obre... %1 (pausada) Selecciona-ho tot Mostra un historial de les notificacions Mostra les notificacions de les aplicacions i del sistema %1 (queden %2) Seguiment de les transferències de fitxers i altres treballs Usa una posició personalitzada per a la notificació emergent Fa %1 minut Fa %1 minuts Fa %1 dia Fa %1 dies Ahir Ara mateix %1: %1: Ha fallat %1: Finalitzat 