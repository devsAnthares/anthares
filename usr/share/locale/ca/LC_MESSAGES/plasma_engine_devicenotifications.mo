��          |      �             !  *   <     g  @   �  �   �  B   Q  R   �  &   �  *     ,   9  4   f  �  �  #   q  >   �  '   �  D   �  �   A  '   �  /     4   7  +   l  /   �     �        	       
                                            Could not eject this disc. Could not mount this device as it is busy. Could not mount this device. One or more files on this device are open within an application. One or more files on this device are opened in application "%2". One or more files on this device are opened in following applications: %2. Remove is less technical for unmountCould not remove this device. Remove is less technical for unmountYou are not authorized to remove this device. This device can now be safely removed. You are not authorized to eject this disc. You are not authorized to mount this device. separator in list of apps blocking device unmount,  Project-Id-Version: plasma_engine_devicenotifications
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-02-05 17:11+0100
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Generator: Lokalize 1.5
 No s'ha pogut expulsar aquest disc. No s'ha pogut muntar aquest dispositiu atès que està ocupat. No s'ha pogut muntar aquest dispositiu. Un o més fitxers d'aquest dispositiu estan oberts a una aplicació. Un o més fitxers d'aquest dispositiu estan oberts a l'aplicació «%2». Un o més fitxers d'aquest dispositiu estan oberts a les següents aplicacions: %2. No s'ha pogut treure aquest dispositiu. No esteu autoritzat a treure aquest dispositiu. Aquest dispositiu ara es pot extreure amb seguretat. No esteu autoritzat a expulsar aquest disc. No esteu autoritzat a muntar aquest dispositiu. ,  