��    %      D  5   l      @     A     E     G  	   Y     c     |     �     �     �     �     �     �            S   ,  w   �     �  M         [     |  ?   �     �  	   �     �     
     #  %   2     X     e  F   �  #   �     �  ]     R   f     �     �  �  �     �	     �	     �	     �	     �	     �	     �	     
     
     -
     M
  )   f
     �
     �
  e   �
  u        �  V   �     �       I   (     r  
   �     �     �     �  1   �        
     W     +   w     �  n   �  ]   1     �     �               !      "          #   %                                                                            $                          	                
                            ms % %1 - All Desktops %1 - Cube %1 - Current Application %1 - Current Desktop %1 - Cylinder %1 - Sphere &Reactivation delay: &Switch desktop on edge: Activation &delay: Active Screen Corners and Edges Activity Manager Always Enabled Amount of time required after triggering an action until the next trigger can occur Amount of time required for the mouse cursor to be pushed against the edge of the screen before the action is triggered Application Launcher Change desktop when the mouse cursor is pushed against the edge of the screen EMAIL OF TRANSLATORSYour emails Lock Screen Maximize windows by dragging them to the top edge of the screen NAME OF TRANSLATORSYour names No Action Only When Moving Windows Open krunnerRun Command Other Settings Quarter tiling triggered in the outer Show Desktop Switch desktop on edgeDisabled Tile windows by dragging them to the left or right edges of the screen Toggle alternative window switching Toggle window switching Trigger an action by pushing the mouse cursor against the corresponding screen edge or corner Trigger an action by swiping from the screen edge towards the center of the screen Window Management of the screen Project-Id-Version: kcmkwinscreenedges
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-30 03:13+0100
PO-Revision-Date: 2017-12-30 17:20+0100
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
  ms % %1 - Tots els escriptoris %1 - Cub %1 - Aplicació actual %1 - Escriptori actual %1 - Cilindre %1 - Esfera Retard de la &reactivació: &Canvia d'escriptori a la vora: Retar&d de l'activació: Cantonades i vores actives de la pantalla Gestor d'activitats Sempre activat Interval de temps requerit després d'activar l'acció fins que pugui ocórrer la següent activació Interval de temps requerit quan s'empeny el cursor del ratolí contra la vora de la pantalla abans d'activar l'acció Llançador d'aplicacions Canvia l'escriptori quan s'empenyi el cursor del ratolí contra la vora de la pantalla txemaq@gmail.com Bloqueig de la pantalla Maximitza les finestres arrossegant-les a la vora superior de la pantalla Josep Ma. Ferrer Cap acció Només en moure finestres Executa una ordre Altres arranjaments La disposició en mosaic de quarts s'activa en el Mostra l'escriptori Desactivat Posa les finestres en mosaic arrossegant-les a les vora dreta o esquerra de la pantalla Commutar a un canvi de finestres alternatiu Commutar el canvi de finestres Activa una acció empenyent el cursor del ratolí contra la vora corresponent de la pantalla o de la cantonada Activar una acció lliscant el dit des de la vora de la pantalla cap al centre de la pantalla Gestió de les finestres exterior de la pantalla 