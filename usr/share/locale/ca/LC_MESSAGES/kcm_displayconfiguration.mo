��    5      �  G   l      �     �     �     �     �     �     �  !   �  '        >     M  -   _     �     �     �     �     �     �     �     �           
     +  	   3     =     N     [  
   i     t     �     �  A   �                     %     2     @     L  <   Z  <   �     �  3   �       �        �     �  ;   �     	     	     *	  %   6	  b   \	  �  �	     �     �     �     �     �     �          1     J     W  4   l  
   �  !   �     �     �     �  !   �          *  	   G     Q  
   b     m     ~     �     �  
   �     �     �  $     E   +     q     x     �     �     �     �     �     �     �     �  >   �     1  �   H  
   �  
   �  b        k       
   �  #   �  o   �             ,      &      3           0   #                 *      /      5       -                                       '   (   !   )   2   4             "          %                         .   +                    1                   
   	         $    %1 Hz &Disable All Outputs &Reconfigure (c), 2012-2013 Daniel Vrátil 90° Clockwise 90° Counterclockwise @title:windowDisable All Outputs @title:windowUnsupported Configuration Active profile Advanced Settings Are you sure you want to disable all outputs? Auto Break Unified Outputs Button Checkbox Combobox Configuration for displays Daniel Vrátil Display Configuration Display: EMAIL OF TRANSLATORSYour emails Enabled Group Box Identify outputs KCM Test App Laptop Screen Maintainer NAME OF TRANSLATORSYour names No Primary Output No available resolutions No kscreen backend found. Please check your kscreen installation. Normal Orientation: Primary display: Radio button Refresh rate: Resolution: Scale Display Scale multiplier, show everything at 1 times normal scale1x Scale multiplier, show everything at 2 times normal scale2x Scale: Scaling changes will come into effect after restart Screen Scaling Sorry, your configuration could not be applied.

Common reasons are that the overall screen size is too big, or you enabled more displays than supported by your GPU. Tab 1 Tab 2 Tip: Hold Ctrl while dragging a display to disable snapping Unified Outputs Unify Outputs Upside Down Warning: There are no active outputs! Your system only supports up to %1 active screen Your system only supports up to %1 active screens Project-Id-Version: kcm_displayconfiguration
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-24 03:18+0200
PO-Revision-Date: 2017-08-18 07:27+0100
Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Generator: Lokalize 2.0
 %1 Hz &Desactiva totes les sortides &Reconfigura (c), 2012-2013 Daniel Vrátil 90° en sentit horari 90° en sentit antihorari Desactiva totes les sortides Configuració no permesa Perfil actiu Arranjament avançat Esteu segur que voleu desactivar totes les sortides? Automàtic Interromp les sortides unificades Botó Casella de selecció Quadre combinat Configuració per a les pantalles Daniel Vrátil Configuració de la pantalla Pantalla: txemaq@gmail.com Habilitada Grup de caselles Identificació de sortides Aplicació de prova del KCM Pantalla de portàtil Mantenidor Josep Ma. Ferrer No hi ha cap sortida primària Les resolucions no estan disponibles No s'ha trobat el dorsal kscreen, comproveu la vostra instal·lació. Normal Orientació: Pantalla primària: Botó d'opció Freqüència de refresc: Resolució: Escala de la pantalla: 1x 2x Escala: Els canvis en l'escala entraran en vigor després de reiniciar Escalat de la pantalla No es pot aplicar aquesta configuració.

El motiu més normal és que la mida de la pantalla és massa gran, o que heu activat més pantalles que les que permet la GPU. Pestanya 1 Pestanya 2 Consell: Manteniu premuda Ctrl en arrossegar una pantalla per desactivar l'ajustament de finestres Sortides unificades Sortides unificades Cara avall Avís: No hi ha cap sortida activa! Aquest sistema només permet fins a %1 pantalla activa Aquest sistema només permet fins a %1 pantalles actives 