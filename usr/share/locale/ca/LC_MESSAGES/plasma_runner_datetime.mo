��    	      d      �       �      �      �   -        <  -   V  #   �  #   �     �  �  �     �     �  2   �       1        O     T     Y                                   	           Current time is %1 Displays the current date Displays the current date in a given timezone Displays the current time Displays the current time in a given timezone Note this is a KRunner keyworddate Note this is a KRunner keywordtime Today's date is %1 Project-Id-Version: plasma_runner_datetime
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2015-02-01 11:35+0100
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.4
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
 L'hora actual és %1 Mostra la data actual Mostra la data actual en la zona horària indicada Mostra l'hora actual Mostra l'hora actual en la zona horària indicada data hora La data d'avui és %1 