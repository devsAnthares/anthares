��    
      l      �       �   
   �      �        .   0     _     t     �  (   �  8   �  �       �  (   �     !  :   ;  "   v     �  	   �     �     �        
                             	        Disk Quota No quota restrictions found. Please install 'quota' Quota tool not found.

Please install 'quota'. Running quota failed e.g.: 12 GiB of 20 GiB%1 of %2 e.g.: 8 GiB free%1 free example: Quota: 83% usedQuota: %1% used usage of quota, e.g.: '/home/bla: 38% used'%1: %2% used Project-Id-Version: plasma_applet_org.kde.plasma.diskquota
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2016-11-20 01:21+0100
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Generator: Lokalize 1.5
 Quota de disc No s'ha trobat cap restricció de quota. Instal·leu el «quota». No s'ha trobat l'eina de quota.

Instal·leu el «quota». Ha fallat en executar el «quota» %1 de %2 %1 lliure Quota: %1% usat %1: %2% usat 