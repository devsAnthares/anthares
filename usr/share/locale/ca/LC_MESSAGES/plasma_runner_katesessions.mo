��          <      \       p   !   q   3   �      �   �  �   1   �  =   �                        Finds Kate sessions matching :q:. Lists all the Kate editor sessions in your account. Open Kate Session Project-Id-Version: plasma_runner_katesessions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-18 03:08+0100
PO-Revision-Date: 2009-05-09 18:03+0200
Last-Translator: Joan Maspons <joanmaspons@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 0.3
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
 Cerca sessions del Kate que coincideixin amb :q:. Llista totes les sessions de l'editor Kate del vostre compte. Obre una sessió del Kate 