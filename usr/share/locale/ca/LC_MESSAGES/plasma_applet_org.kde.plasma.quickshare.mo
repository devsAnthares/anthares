��          �      <      �     �     �     �  +   �  @        L     a     i     w     }     �  
   �     �     �     �     �     �  �  
     �     �        1     G   I  &   �     �     �     �     �     �       
        #     <     T     i     
         	                                                                                       <a href='%1'>%1</a> Close Copy Automatically: Don't show this dialog, copy automatically. Drop text or an image onto me to upload it to an online service. Error during upload. General History Size: Paste Please wait Please, try again. Sending... Share Shares for '%1' Successfully uploaded The URL was just shared Upload %1 to an online service Project-Id-Version: plasma_applet_org.kde.plasma.quickshare
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-01 04:04+0100
PO-Revision-Date: 2015-04-25 15:23+0200
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.4
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
 <a href='%1'>%1</a> Tanca Copia automàticament: No mostris aquest diàleg, copia automàticament. Deixeu text o una imatge sobre meu per pujar-los a un servei en línia. Hi ha hagut un error durant la pujada. General Mida de l'historial: Enganxa Si us plau, espereu Torneu-ho a provar. S'està enviant... Comparteix Comparticions per «%1» S'ha pujat correctament S'ha compartit l'URL Puja %1 a un servei en línia 