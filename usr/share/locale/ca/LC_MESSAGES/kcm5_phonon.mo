��    B      ,  Y   <      �  i   �  m        y  b   �     �     	  6        O  7   _     �     �  	   �     �  (   �  )   �  )   (     R     o  Y   u     �     �  �   �      y	  .   �	     �	  
   �	     �	     �	     
     
     !
     5
     B
     J
     c
     r
     w
     �
     �
     �
     �
     �
  	   �
  
   �
     �
     �
  	     
     
   *     5     B  	   `     j     o  
   �  �   �     (  8   8  �   q     �  8   	  ,   B  ,   o  %   �     �  �  �  _   �  �     $   �     �  "   1     T  N   j     �  K   �            	   -     7  @   U  =   �  >   �  '        ;  a   D     �     �  �   �     n  C        �     �     �     �       	   &     0     I  	   [  *   e     �     �     �  "   �     �  	   �     �     �                /      >     _     n     z     �     �     �     �     �     �  �        �  P   �  �        �  L   �  /     0   8  1   i  #   �     $   !   %          ;   ,          B                 8   #                 7   9       +              )   ?   -   <         6                                    *   2       >                1       @       .   5   3   :   A         &      /                4              0      "             =   (   '         	   
           @info User changed Phonon backendTo apply the backend change you will have to log out and back in again. A list of Phonon Backends found on your system.  The order here determines the order Phonon will use them in. Apply Device List To... Apply the currently shown device preference list to the following other audio playback categories: Audio Hardware Setup Audio Playback Audio Playback Device Preference for the '%1' Category Audio Recording Audio Recording Device Preference for the '%1' Category Backend Colin Guthrie Connector Copyright 2006 Matthias Kretz Default Audio Playback Device Preference Default Audio Recording Device Preference Default Video Recording Device Preference Default/Unspecified Category Defer Defines the default ordering of devices which can be overridden by individual categories. Device Configuration Device Preference Devices found on your system, suitable for the selected category.  Choose the device that you wish to be used by the applications. EMAIL OF TRANSLATORSYour emails Failed to set the selected audio output device Front Center Front Left Front Left of Center Front Right Front Right of Center Hardware Independent Devices Input Levels Invalid KDE Audio Hardware Setup Matthias Kretz Mono NAME OF TRANSLATORSYour names Phonon Configuration Module Playback (%1) Prefer Profile Rear Center Rear Left Rear Right Recording (%1) Show advanced devices Side Left Side Right Sound Card Sound Device Speaker Placement and Testing Subwoofer Test Test the selected device Testing %1 The order determines the preference of the devices. If for some reason the first device cannot be used Phonon will try to use the second, and so on. Unknown Channel Use the currently shown device list for more categories. Various categories of media use cases.  For each category, you may choose what device you prefer to be used by the Phonon applications. Video Recording Video Recording Device Preference for the '%1' Category  Your backend may not support audio recording Your backend may not support video recording no preference for the selected device prefer the selected device Project-Id-Version: kcm5_phonon
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-07-24 18:08+0100
Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
 Per aplicar el canvi de dorsal cal finalitzar la sessió i tornar a connectar-vos un altre cop. És una llista dels dorsals del Phonon que s'han trobat en el sistema. Aquí, l'ordre determina l'ordre en què els usarà el Phonon. Aplica la llista de dispositius a... Aplica la llista de preferències de dispositiu actualment mostrada a les altres següents categories de reproducció d'àudio: Arranjament del maquinari d'àudio Reproducció d'àudio Preferència del dispositiu de reproducció d'àudio per a la categoria «%1» Gravació d'àudio Preferència del dispositiu de gravació d'àudio per a la categoria «%1» Dorsal Colin Guthrie Connector Copyright 2006 Matthias Kretz Preferència del dispositiu de reproducció d'àudio per defecte Preferència del dispositiu de gravació d'àudio per defecte Preferència del dispositiu de gravació de vídeo per defecte Categoria per defecte/sense especificar Defereix Defineix l'ordre per defecte dels dispositius que es poden substituir per categories individuals. Configuració del dispositiu Preferències de dispositiu Dispositius trobats en el sistema, adequats per a la categoria seleccionada. Indiqueu el dispositiu que desitgeu utilitzar en les aplicacions. txemaq@gmail.com Ha fallat en establir el dispositiu seleccionat de sortida d'àudio Frontal centre Frontal esquerra Frontal esquerra del centre Frontal dreta Frontal dreta del centre Maquinari Dispositius independents Nivells d'entrada No vàlid Arranjament del maquinari d'àudio del KDE Matthias Kretz Mono Josep Ma. Ferrer Mòdul de configuració del Phonon Reproducció (%1) Prefereix Perfil Posterior central Posterior esquerra Posterior dret Gravació (%1) Mostra els dispositius avançats Banda esquerra Banda dreta Targeta de so Dispositius de so Situació de l'altaveu i proves Altaveu de greus Prova Prova el dispositiu seleccionat S'està provant %1 L'ordre determina les preferències dels dispositius. Si per alguna raó no es pot usar el primer dispositiu, el Phonon intentarà usar el segon, i així successivament. Canal desconegut Utilitza la llista de dispositius visualitzats actualment per a més categories. Diverses categories de casos d'ús de suports. Per a cada categoria podeu seleccionar el dispositiu que desitgeu que utilitzin les aplicacions Phonon. Gravació de vídeo Preferència del dispositiu de gravació de vídeo per a la categoria «%1» El dorsal potser no admet la gravació d'àudio El dorsal potser no admet la gravació de vídeo no hi ha preferències pel dispositiu seleccionat prefereix el dispositiu seleccionat 