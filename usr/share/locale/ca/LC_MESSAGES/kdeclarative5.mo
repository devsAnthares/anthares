��          �            h  ~   i  +   �           5     T     s     �     �  �   �  g   A  0   �  .   �     	  @     �  Z  �     3   �     �  (   �          0  	   4     >  �   P  {   �  1   r  )   �     �     �                                                   	             
                Click on the button, then enter the shortcut like you would in the program.
Example for Ctrl+A: hold the Ctrl key and press A. Conflict with Standard Application Shortcut EMAIL OF TRANSLATORSYour emails KPackage QML application shell NAME OF TRANSLATORSYour names No shortcut definedNone Reassign Reserved Shortcut The '%1' key combination is also used for the standard action "%2" that some applications use.
Do you really want to use it as a global shortcut as well? The F12 key is reserved on Windows, so cannot be used for a global shortcut.
Please choose another one. The key you just pressed is not supported by Qt. The unique name of the application (mandatory) Unsupported Key What the user inputs now will be taken as the new shortcutInput Project-Id-Version: kdeclarative5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-30 03:07+0100
PO-Revision-Date: 2016-06-07 20:30+0200
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
 Cliqueu en el botó, llavors introduïu la drecera com ho faríeu en el programa.
Exemple per Ctrl+A: premeu la tecla Ctrl mentre premeu la «A». Conflicte amb la drecera estàndard de l'aplicació sps@sastia.com Intèrpret d'aplicació QML del KPackage Sebastià Pla i Sanz Cap Reassigna Drecera reservada La combinació de tecla «%1» ja s'ha assignat a l'acció estàndard «%2» que usen algunes aplicacions.
Esteu segur que també voleu usar-la com a drecera global? La tecla F12 està reservada en el Windows, no es pot utilitzar com a drecera global.
Si us plau, seleccioneu-ne una altra. La tecla que heu premut no està admesa a les Qt. El nom únic de l'aplicació (obligatori) Tecla no admesa Entrada 