��    ?        Y         p     q  !   �  "   �  &   �  ,   �  #   &  &   J  !   q  &   �  '   �  "   �  #     "   )  '   L     t     �  +   �  2   �  
   �     �               ,     3     G  U   O  7   �     �     �     		     	      	     6	     T	     c	     k	  !   �	     �	  	   �	     �	     �	     �	     �	  &   
     /
     N
     U
     p
     v
     ~
     �
  4   �
  $   �
     �
               /     G     ]     w  &   �     �  �  �  )   �     �     �     �     �     �     �  
   �       	                  $  	   *     4     R  A   Z  X   �  
   �             /   $     T     [     u  o   ~  Z   �     I  ,   _     �     �  +   �  %   �     �     �  (     ,   /     \  	   e  	   o  (   y  "   �     �  2   �  )        5  !   =     _     e     l     �  R   �  -   �          +     K     h     �  &   �     �  2   �  *        $                               1          "   *   )          :             	         ;   !                          9   =   5   /   3                    (         #   4       8       -   6   
   7          %                                         ?       0   &      +          '         .       ,      >       <   2        &Matching window property:  @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Border @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large @item:inlistbox Button size:Large @item:inlistbox Button size:Normal @item:inlistbox Button size:Small @item:inlistbox Button size:Very Large Active Window Glow Add Add handle to resize windows with no border Allow resizing maximized windows from window edges Animations B&utton size: Border size: Button mouseover transition Center Center (Full Width) Class:  Configure fading between window shadow and glow when window's active state is changed Configure window buttons' mouseover highlight animation Decoration Options Detect Window Properties Dialog Edit Edit Exception - Oxygen Settings Enable/disable this exception Exception Type General Hide window title bar Information about Selected Window Left Move Down Move Up New Exception - Oxygen Settings Question - Oxygen Settings Regular Expression Regular Expression syntax is incorrect Regular expression &to match:  Remove Remove selected exception? Right Shadows Tit&le alignment: Title:  Use the same colors for title bar and window content Use window class (whole application) Use window title Warning - Oxygen Settings Window Class Name Window Drop-Down Shadow Window Identification Window Property Selection Window Title Window active state change transitions Window-Specific Overrides Project-Id-Version: oxygen_kdecoration
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-13 05:20+0100
PO-Revision-Date: 2017-12-13 21:21+0100
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Generator: Lokalize 2.0
 &Propietat de coincidència de finestres: Enorme Gran Sense vores Sense vores laterals Normal Fora de mida Minúscula Molt enorme Molt gran Gran Normal Petit Molt gran Lluentor a la finestra activa Afegeix Afegeix la nansa per canviar la mida de les finestres sense vores Permet el redimensionament de les finestres maximitzades des de les vores de la finestra Animacions Mida dels b&otons: Mida de la vora: Transició del pas del ratolí sobre els botons Centre Centre (amplada completa) Classe:  Configura l'esvaïment entre l'ombra i la lluminositat de la finestra quan canvia l'estat de la finestra activa Configura l'animació del ressaltat en passar el ratolí sobre els botons de les finestres Opcions de decoració Detecció de les propietats de les finestres Diàleg Edita Edita l'excepció - Arranjament de l'Oxygen Habilita/inhabilita aquesta excepció Tipus d'excepció General Oculta la barra de títol de la finestra Informació quant a la finestra seleccionada Esquerra Mou avall Mou amunt Excepció nova - Arranjament de l'Oxygen Pregunta - Arranjament de l'Oxygen Expressió regular La sintaxi de l'expressió regular no és correcta Expressió regular que h&a de coincidir:  Elimina Elimino l'excepció seleccionada? Dreta Ombres A&lineació del títol: Títol:  Usa els mateixos colors per a la barra de títol i per al contingut de la finestra Usa la classe de finestra (tota l'aplicació) Usa el títol de la finestra Avís - Arranjament de l'Oxygen Nom de la classe de finestra Ombra difusa de la finestra Identificació de la finestra Selecció de propietats de la finestra Títol de la finestra Transicions de canvi d'estat de la finestra activa Substitucions específiques de la finestra 