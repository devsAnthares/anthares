��    w      �  �   �      
     
  	   &
     0
     =
     K
     Q
     X
     i
     o
     w
     
     �
     �
     �
     �
  	   �
     �
     �
  
   �
     �
     �
       
             !     (  	   7     A  
   G  
   R     ]     i     x     ~     �     �     �     �  ;   �  -   �  #        ;     T     l  
   �     �     �  
   �     �     �     �               7     K     P  	   k      u     �     �     �  
   �     �     �               .     E     V     f     v     �     �  
   �     �  )   �     �          #     2     A     R     X     `     s  '   �     �     �     �     �     �     
           .  C   <     �  s   �           %     :     Q     f     �     �      �     �  -   �  /        4  	   =  !   G     i      �     �     �     �     �     �  �  �     �     �  
   �     �                    +  
   2  	   =      G  	   h     r     �  	   �  
   �     �     �     �  
   �     �                    '     0     B     P  
   V     a     o          �     �     �     �     �     �  	   �  	   �     �               '     7     E  +   M     y     �     �     �      �  #   �          1  %   :     `  D   o     �     �     �     �     �  !     !   &     H     a     |     �     �     �     �  9   �          +  *   8     c  &   }     �     �     �     �  
   �     �       9   3     m     |     �     �     �     �     �       ^   .     �  �   �  2   (  "   [      ~      �  #   �     �     �  +        9  &   J  .   q     �     �  7   �  *   �  7        L  	   T     ^     e  	   l     o   5   l   W   	             1               R   6          J         >   u   d           V          M   T       X   r   v          G   .   Q   \       a              E   -       O               ^          b   7           +   Y   C      [       4                 H       k   B   c       ]   I   *   P   (   h   w   ;   N   3   
                @      "   S      _                   L   $      9   F      D          m   U   %       K       &                     t   Z   `   !       ,   0   g   f   p          i      2   e       n   /      q       ?      <   A   #   8          =   '   s   )               :   j           %1 &Handbook &About %1 &Actual Size &Add Bookmark &Back &Close &Configure %1... &Copy &Delete &Donate &Edit Bookmarks... &Find... &First Page &Fit to Page &Forward &Go To... &Go to Line... &Go to Page... &Last Page &Mail... &Move to Trash &New &Next Page &Open... &Paste &Previous Page &Print... &Quit &Redisplay &Rename... &Replace... &Report Bug... &Save &Save Settings &Spelling... &Undo &Up &Zoom... @action:button Goes to next tip, opposite to previous&Next @action:button Goes to previous tip&Previous @option:check&Show tips on startup @titleDid you know...?
 @title:windowConfigure @title:windowTip of the Day About &KDE C&lear Check spelling in document Clear List Close document Configure &Notifications... Configure S&hortcuts... Configure Tool&bars... Copy selection to clipboard Create new document Cu&t Cut selection to clipboard Dese&lect EMAIL OF TRANSLATORSYour emails Encodings menuAutodetect Encodings menuDefault F&ull Screen Mode Find &Next Find Pre&vious Fit to Page &Height Fit to Page &Width Go back in document Go forward in document Go to first page Go to last page Go to next page Go to previous page Go up NAME OF TRANSLATORSYour names No Entries Open &Recent Open a document which was recently opened Open an existing document Paste clipboard content Print Previe&w Print document Quit application Re&do Re&vert Redisplay document Redo last undone action Revert unsaved changes made to document Save &As... Save document Save document under a new name Select &All Select zoom level Send document by mail Show &Menubar Show &Toolbar Show Menubar<p>Shows the menubar again after it has been hidden</p> Show St&atusbar Show Statusbar<p>Shows the statusbar, which is the bar at the bottom of the window used for status information.</p> Show a print preview of document Show or hide menubar Show or hide statusbar Show or hide toolbar Switch Application &Language... Tip of the &Day Undo last action View document at its actual size What's &This? You are not allowed to save the configuration You will be asked to authenticate before saving Zoom &In Zoom &Out Zoom to fit page height in window Zoom to fit page in window Zoom to fit page width in window go back&Back go forward&Forward home page&Home show help&Help without name Project-Id-Version: kconfigwidgets5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-16 03:10+0100
PO-Revision-Date: 2017-08-24 14:22+0100
Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
 &Manual del %1 Qu&ant al %1 Mida re&al &Afegeix una adreça d'interès &Enrere Tan&ca &Configura el %1... &Copia Su&primeix &Donatius &Edita les adreces d'interès... &Cerca... P&rimera pàgina &Ajusta a la pàgina En&davant &Vés a... &Vés a la línia... &Vés a la pàgina... D&arrera pàgina Corr&eu... &Mou a la paperera &Nou Pàgina següe&nt &Obre... &Enganxa &Pàgina anterior Im&primeix... &Surt &Redibuixa &Reanomena... &Substitueix... &Informa d'un error... &Desa De&sa l'arranjament &Ortografia... &Desfés &Amunt &Zoom... &Següent &Anterior Mo&stra consells en engegar Sabíeu que...?
 Configuració Consell del dia Quant al &KDE &Neteja Verificació de l'ortografia en el document Neteja la llista Tanca el document Configura les &notificacions... Configura les &dreceres... Configura les &barres d'eines... Copia la selecció al porta-retalls Crea un document nou Re&talla Retalla la selecció al porta-retalls Desse&lecciona sps@sastia.com,antonibella5@yahoo.com,aacid@kde.org,txemaq@gmail.com Autodetecta Omissió &Mode de pantalla completa Cerca &següent Cerca &anterior Ajusta a l'a&lçada de la pàgina Ajusta a l'a&mplada de la pàgina Va enrere en el document Va endavant en el document Va a la primera pàgina Va a l'última pàgina Va a la pàgina següent Va a la pàgina anterior Puja Sebastià Pla,Antoni Bella,Albert Astals,Josep Ma. Ferrer Sense entrades Obre &recent Obre un document que s'ha obert recentment Obre un document existent Enganxa el contingut del porta-retalls &Vista prèvia d'impressió Imprimeix el document Surt de l'aplicació Re&fés Re&verteix Redibuixa el document Refà l'última acció desfeta Reverteix els canvis sense desar efectuats en el document Desa &com a... Desa el document Desa el document amb un nom nou Selecciona-ho &tot Selecció del nivell de zoom Envia un document per correu Mostra la barra de &menús Mostra la barra d'&eines Mostra la barra de menús<p>Torna a mostrar la barra de menús després d'haver-se ocultat</p> &Mostra la barra d'estat Mostra la barra d'estat<p>Mostra la barra d'estat, que és la barra a sota de la finestra usada per a la informació d'estat.</p> Mostra una vista prèvia d'impressió del document Mostra o oculta la barra de menús Mostra o oculta la barra d'estat Mostra o oculta la barra d'eines Canvia l'i&dioma de l'aplicació... Consell del &dia Desfà l'última acció Visualitza el document en la seva mida real Què és &això? No se us permet desar la configuració Se us demanarà l'autenticació abans de desar &Amplia Red&ueix Zoom per ajustar l'alçada de la pàgina en la finestra Zoom per ajustar la pàgina en la finestra Zoom per ajustar l'amplada de la pàgina en la finestra &Enrere En&davant &Inici A&juda sense nom 