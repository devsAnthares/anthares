��    
      l      �       �   $   �   %     :   <     w  '   �  -   �     �       '     �  <  /     3   4  B   h  #   �  '   �  *   �     "     @  &   Q         	                            
        Could not detect the file's mimetype Could not find all required functions Could not find the provider with the specified destination Error trying to execute script Invalid path for the requested provider It was not possible to read the selected file Service was not available Unknown Error You must specify a URL for this service Project-Id-Version: plasma_engine_share
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-06-27 18:19+0200
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
 No s'ha pogut detectar el tipus MIME del fitxer No s'han pogut trobar totes les funcions requerides No s'ha pogut trobar el proveïdor amb la destinació especificada Error en intentar executar l'script Camí no vàlid pel proveïdor requerit No s'ha pogut llegir el fitxer seleccionat El servei no està disponible Error desconegut Heu d'indicar un URL per aquest servei 