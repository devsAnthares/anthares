��    &      L  5   |      P     Q     l     y     �     �     �     �     �     �     �     �     �  &   �               #     ,     3     ?     M     U     ]     d     s     �     �     �     �     �     �     �     �     �     �  
   �            �       �     �     
     '     -     C     G     N     `     t     }     �  0   �     �     �     �                    -     5     A     J     a     u  
   �     �     �     �  #   �      �     	     	     3	     N	     _	     g	                           	      #      $                                                    &                                          !          %                            "   
    Abbreviation for secondss Application: Average clock: %1 MHz Bar Buffers: CPU CPU %1 CPU monitor CPU%1: %2% @ %3 Mhz CPU: %1% CPUs separately Cache Cache Dirty, Writeback: %1 MiB, %2 MiB Cache monitor Cached: Circular Colors Compact Bar Dirty memory: General IOWait: Memory Memory monitor Memory: %1/%2 MiB Monitor type: Nice: Set colors manually Show: Swap Swap monitor Swap: %1/%2 MiB Sys: System load Update interval: Used swap: User: Writeback memory: Project-Id-Version: plasma_applet_org.kde.plasma.systemloadviewer
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-29 03:31+0200
PO-Revision-Date: 2018-02-03 19:09+0100
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Generator: Lokalize 2.0
 s Aplicació: Mitjana del rellotge: %1 MHz Barra Memòria intermèdia: CPU CPU %1 Monitor de la CPU CPU%1: %2% @ %3 Mhz CPU: %1% Les CPU separadament Memòria cau Memòria cau bruta, reescriptura: %1 MiB, %2 MiB Monitor de la memòria cau A la memòria cau: Circular Colors Barra compacta Memòria bruta: General Espera E/S: Memòria Monitor de la memòria Memòria: %1/%2 MiB Tipus de monitor: Prioritat: Estableix els colors manualment Mostra: Memòria d'intercanvi Monitor de la memòria d'intercanvi Memòria d'intercanvi: %1/%2 MiB Sistema: Càrrega del sistema Interval d'actualització: Intercanvi usat: Usuari: Memòria de reescriptura: 