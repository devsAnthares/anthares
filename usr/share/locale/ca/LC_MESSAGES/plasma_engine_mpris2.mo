��          t      �         C        U     i  3   �     �     �  F   �  5   *     `       �  �  F   W  #   �  #   �     �  *   �     )  L   I  G   �  #   �                                   	                          
    Attempting to perform the action '%1' failed with the message '%2'. Media playback next Media playback previous Name for global shortcuts categoryMedia Controller Play/Pause media playback Stop media playback The argument '%1' for the action '%2' is missing or of the wrong type. The media player '%1' cannot perform the action '%2'. The operation '%1' is unknown. Unknown error. Project-Id-Version: plasma_engine_mpris2
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-06 03:09+0200
PO-Revision-Date: 2015-02-21 17:50+0100
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.4
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
 Ha fallat en intentar executar l'acció «%1» amb el missatge «%2». Següent del reproductor de suports Anterior del reproductor de suports Controlador multimèdia Reprodueix/Pausa el reproductor de suports Atura el reproductor de suports L'argument «%1» per l'acció «%2» no hi és o és d'un tipus incorrecte. El reproductor de suports «%1» no ha pogut realitzar l'acció «%2». L'operació «%1» és desconeguda. Error desconegut. 