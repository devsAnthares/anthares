��          �      |      �     �     �     �                 ;  	   \     f     �  &   �     �     �     �  	          �   %  �   �  t   ?  7   �  +   �       �       �     �     �  !     %   (     N     _     g     x  &   �  *   �  3   �  9        R     X  �   `  �   �  �   �	     )
  5   :
     p
                                      
   	                                                         min Act like Always Define a special behavior Don't use special settings EMAIL OF TRANSLATORSYour emails Hibernate NAME OF TRANSLATORSYour names Never shutdown the screen Never suspend or shutdown the computer PC running on AC power PC running on battery power PC running on low battery Shut down Suspend The Power Management Service appears not to be running.
This can be solved by starting or scheduling it inside "Startup and Shutdown" The activity service is not running.
It is necessary to have the activity manager running to configure activity-specific power management behavior. The activity service is running with bare functionalities.
Names and icons of the activities might not be available. This is meant to be: Act like activity %1Activity "%1" Use separate settings (advanced users only) after Project-Id-Version: powerdevilactivitiesconfig
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-09 03:03+0200
PO-Revision-Date: 2017-12-17 23:30+0100
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
  min Funciona com Sempre Defineix un comportament especial No utilitzis cap arranjament especial txemaq@gmail.com Hiberna Josep Ma. Ferrer No aturis mai la pantalla No suspenguis o aturis mai l'ordinador El PC està funcionant amb alimentació CA El PC està funcionant amb alimentació per bateria El PC està funcionant amb alimentació per bateria baixa Atura Suspèn Sembla que el servei de gestió d'energia no s'està executant.
Això es pot solucionar engegant o planificant-lo des d'«Inici i aturada». El servei de l'activitat no s'està executant.
Cal tenir el gestor d'activitat executant-se per a configurar el comportament de gestió d'energia específic d'activitat. El servei de l'activitat s'està executant amb les funcionalitats mínimes.
Els noms i les icones de les activitats podrien no estar disponibles. Activitat «%1» Usa un arranjament separat (només usuaris avançats) després de 