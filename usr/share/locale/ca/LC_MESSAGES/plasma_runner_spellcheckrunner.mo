��          t      �                 '     6     R     Z     w  Q   �     �      �       �    #   �          '  	   E  $   O  +   t     �     �     �  	   �     
      	                                                 &Require trigger word &Trigger word: Checks the spelling of :q:. Correct Could not find a dictionary. Spell Check Settings Spelling checking runner syntax, first word is trigger word, e.g.  "spell".%1:q: Suggested words: %1 separator for a list of words,  spell Project-Id-Version: plasma_runner_spellcheckrunner
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2016-04-09 15:28+0200
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
 &Requereix una paraula d'activació Paraula d'ac&tivació: Comprova l'ortografia de :q:. Corregeix No s'ha pogut trobar cap diccionari. Arranjament de la verificació ortogràfica %1:q: Paraules suggerides: %1 ,  corrector 