��    `        �         (     )     1     B     Q     W     ^     j     {     �     �  /   �  -   �     �     �  
   		  
   	     	     ,	     1	     8	     @	     R	     _	     d	  
   l	     w	     �	     �	     �	  	   �	     �	  	   �	     �	     �	     �	     
     
  	   #
     -
     4
     H
  	   M
     W
     ]
     c
     h
     m
  	   v
     �
     �
     �
     �
     �
     �
     �
     �
  <   �
               /     6     =     X     ^     e     j  
   ~     �     �     �     �     �  )   �               5     :     @     M     U     ]     f  
   x     �     �  	   �     �     �     �     �     �     �     �  E   �  *   A  �  l  
   I     T     g     z     �     �     �     �  
   �  
   �     �     �     �  
   �     �               *  
   1     <     E     c     x     }  
   �     �     �     �  (   �     �     
     $     4     F  $   M     r     �     �  
   �     �     �     �     �     �     �        	     
             4     ;      C     d     h     n     v  H   �     �  !   �               $     -     3     8     >     [     m     �     �     �     �  4   �  !     (   :     c     h     o  
   ~  
   �     �     �     �  
   �     �     �     �     �      �          +     H     [  N   p     �     J       -      M       %   Q           ^              U       L   S   $      .       H       X          Y   8   *         <   &          '       ]   N       3                     `          #   V             /   T   G   9   @   >   =   6   2   O   C   F   ?      0      
      A   E       	      P              _                D          Z               1   R   ,   +                \           I   )   B   K      :   !   W   4          ;               (      5   "       7       [           &Delete &Empty Trash Bin &Move to Trash &Open &Paste &Properties &Refresh Desktop &Refresh View &Reload &Rename @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Align Appearance: Arrange In Arrange in Arrangement: Back Cancel Columns Configure Desktop Custom title Date Default Descending Description Deselect All Desktop Layout Enter custom title here Features: File name pattern: File type File types: Filter Folder preview popups Folders First Folders first Full path Got it Hide Files Matching Huge Icon Size Icons Large Left List Location Location: Lock in place Locked Medium More Preview Options... Name None OK Panel button: Press and hold widgets to move them and reveal their handles Preview Plugins Preview thumbnails Remove Resize Restore from trashRestore Right Rotate Rows Search file type... Select All Select Folder Selection markers Show All Files Show Files Matching Show a place: Show files linked to the current activity Show the Desktop folder Show the desktop toolbox Size Small Small Medium Sort By Sort by Sorting: Specify a folder: Text lines Tiny Title: Tool tips Tweaks Type Type a path or a URL here Unsorted Use a custom icon Widget Handling Widgets unlocked You can press and hold widgets to move them and reveal their handles. whether to use icon or list viewView mode Project-Id-Version: plasma_applet_org.kde.desktopcontainment
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:11+0100
PO-Revision-Date: 2017-11-18 10:46+0100
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Generator: Lokalize 2.0
 &Suprimeix &Buida la paperera &Mou a la paperera &Obre &Enganxa &Propietats &Refresca l'escriptori &Refresca la vista &Recarrega &Reanomena Tria... Neteja la icona Alinea Aparença: Organitza per Organitza per Organització: Enrere Cancel·la Columnes Configuració de l'escriptori Títol personalitzat Data Omissió Descendent Descripció Desselecciona-ho tot Disposició de l'escriptori Introduïu aquí el títol personalitzat Característiques: Patró del nom de fitxer: Tipus de fitxer Tipus de fitxers: Filtre Vista prèvia emergent de la carpeta Primer les carpetes Primer les carpetes Camí sencer Ho heu fet Oculta els fitxers coincidents Enorme Mida de les icones Icones Gran Esquerra Llista Ubicació Ubicació: Bloqueja en la posició Blocat Mitjana Més opcions de vista prèvia... Nom Sense D'acord Botó del plafó: Premeu i mantingueu els estris per moure'ls i descobrir les seves nanses Connectors de vista prèvia Miniatures per a la vista prèvia Elimina Redimensiona Restaura Dreta Gira Files Cerca per tipus de fitxer... Selecciona-ho tot Selecció de carpeta Marcadors de la selecció Mostra tots els fitxers Mostra els fitxers coincidents Mostra un lloc: Mostra els fitxers enllaçats amb l'activitat actual Mostra la carpeta de l'escriptori Mostra el quadre d'eines de l'escriptori Mida Petita Mitjana petita Ordena per Ordena per Ordenació: Indiqueu una carpeta: Línies de text Minúscula Títol: Consells d'eines Ajustaments Tipus Indiqueu aquí un camí o un URL Sense ordenar Usa una icona personalitzada Gestió de l'estri Estris desbloquejats Podeu prémer i mantenir els estris per moure'ls i descobrir les seves nanses. Mode de vista 