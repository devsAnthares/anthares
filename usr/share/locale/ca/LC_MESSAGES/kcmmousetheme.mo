��          �   %   �      @     A  �   `  m   �  �   P  )   �  `        o     |     �     �     �      �     �     �  '        ,     >     ]     b     s  ?   �  Y   �  +     H   F  �  �     Q  �   p  j   �     e	     }	  l   �	     
  #   
     5
     D
  '   P
     x
     �
     �
  ,   �
     �
     �
          	        C   0  l   t  3   �  Q                                                    	                                                               
              (c) 2003-2007 Fredrik Höglund <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) @item:inlistbox sizeResolution dependent A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Cursor Theme Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Fredrik Höglund Get new Theme Get new color schemes from the Internet Install from File NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. You have to restart the Plasma session for these changes to take effect. Project-Id-Version: kcmmousetheme
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-07-12 19:52+0200
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Generator: Lokalize 1.5
 (c) 2003-2007 Fredrik Höglund <qt>Esteu segur que voleu eliminar el tema de cursor <i>%1</i>?<br />Això esborrarà tots els fitxers instal·lats per aquest tema.</qt> <qt>No podeu suprimir el tema que esteu usant actualment.<br />Primer heu de canviar a un altre tema.</qt> (Mides disponibles: %1) Depenent de la resolució A la vostra carpeta per a temes d'icones ja existeix un tema anomenat %1. Desitgeu substituir-lo per aquest? Confirmació Ha canviat l'arranjament del cursor Tema de cursor Descripció Arrossegueu o introduïu l'URL del tema txemaq@gmail.com Fredrik Höglund Obtén temes nous Obtén esquemes de color nous des d'Internet Instal·la des d'un fitxer Josep Ma. Ferrer Nom Sobreescriure el tema? Elimina el tema El fitxer %1 no sembla que sigui un arxiu vàlid de tema de cursor. No s'ha pogut descarregar l'arxiu del tema de cursor; si us plau, comproveu que l'adreça %1 sigui correcta. No s'ha pogut trobar l'arxiu del tema de cursor %1. Cal tornar a engegar la sessió del Plasma perquè aquests canvis tinguin efecte. 