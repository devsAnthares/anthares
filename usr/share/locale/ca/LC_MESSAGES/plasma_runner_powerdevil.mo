��          �      L      �     �     �  �   �  T   }  )   �  (   �  0   %  $   V  &   {  &   �  %   �  ?   �  F   /     v     �     �     �     �  �  �  "   �      �  �   �  ^   �          )     1     M     S     [     b     h     �     �     �     �     �     	                                              
                    	                                  Dim screen by half Dim screen totally Lists screen brightness options or sets it to the brightness defined by :q:; e.g. screen brightness 50 would dim the screen to 50% maximum brightness Lists system suspend (e.g. sleep, hibernate) options and allows them to be activated Note this is a KRunner keyworddim screen Note this is a KRunner keywordhibernate Note this is a KRunner keywordscreen brightness Note this is a KRunner keywordsleep Note this is a KRunner keywordsuspend Note this is a KRunner keywordto disk Note this is a KRunner keywordto ram Note this is a KRunner keyword; %1 is a parameterdim screen %1 Note this is a KRunner keyword; %1 is a parameterscreen brightness %1 Set Brightness to %1 Suspend to Disk Suspend to RAM Suspends the system to RAM Suspends the system to disk Project-Id-Version: plasma_runner_powerdevil
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-26 03:33+0200
PO-Revision-Date: 2017-12-17 23:19+0100
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
 Enfosqueix la pantalla a la meitat Enfosqueix la pantalla totalment Llista les opcions de lluminositat de la pantalla o les defineix a la lluminositat definida per :q:; p. ex. lluminositat de pantalla 50 reduirà la pantalla al 50% de la lluminositat màxima Llista les opcions de suspensió del sistema (p. ex. dormir, hibernació) i les permet activar enfosquiment de pantalla hiberna lluminositat de la pantalla adorm suspèn a disc a RAM enfosquiment de la pantalla %1 lluminositat de la pantalla %1 Defineix la lluminositat a %1 Suspèn al disc Suspèn a la RAM Suspèn el sistema a la RAM Suspèn el sistema al disc 