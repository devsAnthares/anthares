��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  �  K     	  %     �  2  �  �  c   y
     �
     �
     �
  /        =     Q  %   f  #   �                                          
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2012-11-07 23:01+0100
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.4
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
  s &Expiració de la indicació d'inici: <H1>Notificació a la barra de tasques</H1>
Podeu habilitar un segon mètode de notificació d'inici que l'usa
la barra de tasques on apareix un botó amb un disc girant,
simbolitzant que l'aplicació que heu engegat s'està carregant.
Pot passar que algunes aplicacions no se n'assabentin d'aquesta
notificació d'inici. En aquest cas, el botó desapareix després del
temps donat a la secció «Expiració de la indicació d'inici» <h1>Cursor ocupat</h1>
KDE ofereix un cursor ocupat per a la notificació de l'inici d'aplicacions.
Per habilitar el cursor ocupat, trieu un tipus de confirmació visual
del quadre combinat.
Pot passar que algunes aplicacions no se n'assabentin d'aquesta
notificació d'inici. En aquest cas, el cursor atura la intermitència
després del temps donat a la secció «Expiració de la indicació d'inici» <h1>Confirmació d'engegada</h1> Aquí podeu configurar la confirmació d'engegada de l'aplicació. Cursor intermitent Cursor botant Cursor &ocupat Habilita la notificació a la barra de &tasques Sense cursor ocupat Cursor ocupat passiu &Expiració de la indicació d'inici: &Notificació a la barra de tasques 