��          �      �       0  (   1     Z      q     �     �     �     �     �     �  `     ^   u     �  �  �  ,   �  #   �     �     	           ?     ]  #   n     �  /   �  +   �     �        
              	                                    *.kwinscript|KWin scripts (*.kwinscript) Configure KWin scripts EMAIL OF TRANSLATORSYour emails Get New Scripts... Import KWin Script Import KWin script... KWin Scripts KWin script configuration NAME OF TRANSLATORSYour names Placeholder is error message returned from the install serviceCannot import selected script.
%1 Placeholder is name of the script that was importedThe script "%1" was successfully imported. Tamás Krutki Project-Id-Version: kcm-kwin-scripts
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-19 03:12+0100
PO-Revision-Date: 2017-12-17 20:54+0100
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
 *.kwinscript|scripts del KWin (*.kwinscript) Configuració dels scripts del KWin txemaq@gmail.com Obtén scripts nous... Importació de script del KWin Importa un script del KWin... Scripts del KWin Configuració dels scripts del KWin Josep Ma. Ferrer No s'ha pogut importar l'script seleccionat.
%1 L'script «%1» s'ha importat correctament. Tamás Krutki 