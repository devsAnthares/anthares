��    !      $  /   ,      �  .   �  1     9   J     �  -   �  &   �  )   �  .   #  &   R  )   y  .   �  &   �  )   �  2   #  5   V  =   �  #   �     �  !     '   /  "   W  0   z  '   �  *   �     �          7     R     j     �     �  &   �  �  �  *   �	  /   �	  1   
  #   4
  +   X
  $   �
  )   �
  -   �
  $     *   &  )   Q  #   {  )   �  -   �  2   �  4   *      _     �     �  -   �  +   �  1     &   E  ,   l     �     �     �     �     �     �       "   &                                                              	                                
                                    !                             @info:statusAdded files to Bazaar repository. @info:statusAdding files to Bazaar repository... @info:statusAdding of files to Bazaar repository failed. @info:statusBazaar Log closed. @info:statusCommit of Bazaar changes failed. @info:statusCommitted Bazaar changes. @info:statusCommitting Bazaar changes... @info:statusPull of Bazaar repository failed. @info:statusPulled Bazaar repository. @info:statusPulling Bazaar repository... @info:statusPush of Bazaar repository failed. @info:statusPushed Bazaar repository. @info:statusPushing Bazaar repository... @info:statusRemoved files from Bazaar repository. @info:statusRemoving files from Bazaar repository... @info:statusRemoving of files from Bazaar repository failed. @info:statusReview Changes failed. @info:statusReviewed Changes. @info:statusReviewing Changes... @info:statusRunning Bazaar Log failed. @info:statusRunning Bazaar Log... @info:statusUpdate of Bazaar repository failed. @info:statusUpdated Bazaar repository. @info:statusUpdating Bazaar repository... @item:inmenuBazaar Add... @item:inmenuBazaar Commit... @item:inmenuBazaar Delete @item:inmenuBazaar Log @item:inmenuBazaar Pull @item:inmenuBazaar Push @item:inmenuBazaar Update @item:inmenuShow Local Bazaar Changes Project-Id-Version: fileviewbazaarplugin
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2015-05-28 19:28+0200
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
 S'han afegit fitxers al repositori Bazaar. S'estan afegint fitxers al repositori Bazaar... Ha fallat en afegir fitxers al repositori Bazaar. S'ha tancat el registre del Bazaar. Ha fallat en publicar els canvis al Bazaar. S'han publicat els canvis al Bazaar. S'estan publicant els canvis al Bazaar... Ha fallat en recuperar del repositori Bazaar. S'ha recuperat el repositori Bazaar. S'està recuperant el repositori Bazaar... Ha fallat l'entrega al repositori Bazaar. S'ha entregat al repositori Bazaar. S'està entregant al repositori Bazaar... S'han eliminat fitxers del repositori Bazaar. S'estan eliminant fitxers del repositori Bazaar... Ha fallat en eliminar fitxers del repositori Bazaar. Ha fallat en revisar els canvis. S'han revisat els canvis. S'estan revisant els canvis... Ha fallat en executar el registre del Bazaar. S'està executant el registre del Bazaar... Ha fallat l'actualització del repositori Bazaar. S'ha actualitzat el repositori Bazaar. S'està actualitzant el repositori Bazaar... Afegeix al Bazaar... Publica al Bazaar... Suprimeix del Bazaar Registre del Bazaar Recupera el Bazaar Entrega al Bazaar Actualitza el Bazaar Mostra els canvis locals al Bazaar 