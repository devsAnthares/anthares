��          �      L      �  
   �     �  >   �                  
   -     8     @     H     O  	   [     e     s     z  2   �     �     �  �  �  
   �     �  C   �     �     �     �  
   �     
       
        *  
   =     H     X     ^  4   s     �     �                                   	                                                         
        &Browse... &Search: *.png *.xpm *.svg *.svgz|Icon Files (*.png *.xpm *.svg *.svgz) Actions All Applications Categories Devices Emblems Emotes Icon Source Mimetypes O&ther icons: Places S&ystem icons: Search interactively for icon names (e.g. folder). Select Icon Status Project-Id-Version: kiconthemes5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:11+0100
PO-Revision-Date: 2016-11-19 15:00+0100
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
 &Navega... &Cerca: *.png *.xpm *.svg *.svgz|Fitxers d'icona (*.png *.xpm *.svg *.svgz) Accions Tot Aplicacions Categories Dispositius Emblemes Emoticones Origen de la icona Tipus MIME Al&tres icones: Llocs Icones del &sistema: Cerca noms d'icona interactivament (p. ex. carpeta). Trieu una icona Estat 