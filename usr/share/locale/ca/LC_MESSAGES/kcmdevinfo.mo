��    ]           �      �  !   �       	   !     +  S   4  	   �     �     �     �     �     �     �     �     �     �     	  J   $	     o	     }	     �	      �	  	   �	  
   �	     �	     �	     �	     �	     
     
     
  L   
  	   l
  	   v
  
   �
  
   �
  
   �
     �
     �
     �
     �
     �
     �
     �
     �
     	     (  	   +     5     G     S     [     i     m     }     �     �  
   �  	   �     �  
   �     �     �     �     �     �     	          +     ?     \     r     x     |     �  H   �     �     �     �     �     �       
     &        A  "   T     w     �     �     �     �       	     �  (  6   �       	   4     >     G     b     k     ~     �     �     �     �     �     �  
   �     �  1        ?     T     `     m  	   ~     �     �     �     �     �     �            A     	   W  	   a  
   k  
   v  
   �     �     �  
   �     �     �  
   �     �     �     
               -     H     X     d     s     w  	   �     �     �     �  
   �     �     �     �     �     �           !     B     \     u     �     �     �     �     �     �  d   �     <  	   O     Y     e     u     �  
   �  
   �     �     �  
   �     �  
   �  
   �  
   �  
   �     �           B   "       (   W   #   $   %            [   @      L   M   '   6       >   T           <   9   G   &           A      .   8   R       7       J             ?   4                   H   -                      F          5   )   N   D       ,       K             3       U   =   
       ;       O   :           +           !   Z   /      V   Y              Q         E       1       X             ]       0      P         \   I          	          C       S   2       *    
Solid Based Device Viewer Module (c) 2010 David Hubner AMD 3DNow ATI IVEC Available space out of total partition size (percent used)%1 free of %2 (%3% used) Batteries Battery Type:  Bus:  Camera Cameras Charge Status:  Charging Collapse All Compact Flash Reader Default device tooltipA Device Device Information Device Listing Whats ThisShows all the devices that are currently listed. Device Viewer Devices Discharging EMAIL OF TRANSLATORSYour emails Encrypted Expand All File System File System Type:  Fully Charged Hard Disk Drive Hotpluggable? IDE IEEE1394 Info Panel Whats ThisShows information about the currently selected device. Intel MMX Intel SSE Intel SSE2 Intel SSE3 Intel SSE4 Keyboard Keyboard + Mouse Label:  Max Speed:  Memory Stick Reader Mounted At:  Mouse Multimedia Players NAME OF TRANSLATORSYour names No No Charge No data available Not Mounted Not Set Optical Drive PDA Partition Table Primary Processor %1 Processor Number:  Processors Product:  Raid Removable? SATA SCSI SD/MMC Reader Show All Devices Show Relevant Devices Smart Media Reader Storage Drives Supported Drivers:  Supported Instruction Sets:  Supported Protocols:  UDI:  UPS USB UUID:  Udi Whats ThisShows the current device's UDI (Unique Device Identifier) Unknown Drive Unused Vendor:  Volume Space: Volume Usage:  Yes kcmdevinfo name of something is not knownUnknown no device UDINone no instruction set extensionsNone platform storage busPlatform unknown battery typeUnknown unknown deviceUnknown unknown device typeUnknown unknown storage busUnknown unknown volume usageUnknown xD Reader Project-Id-Version: kcmdevinfo
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-23 03:11+0200
PO-Revision-Date: 2016-12-04 18:06+0100
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
 
Mòdul visualitzador de dispositius basat en el Solid (c) 2010 David Hubner AMD 3DNow ATI IVEC %1 lliure de %2 (%3% usat) Bateries Tipus de bateria:  Bus:  Càmera Càmeres Estat de la càrrega:  Està carregant Redueix-ho tot Lector de «Compact Flash» Dispositiu Informació del dispositiu Mostra tots els dispositius que hi ha actualment. Visor de dispositius Dispositius Descarregant txemaq@gmail.com Encriptat Expandeix-ho tot Sistema de fitxers Tipus de sistema de fitxers:  Totalment carregada Unitat de disc dur Connectable en calent? IDE IEEE1394 Mostra la informació quant al dispositiu seleccionat actualment. Intel MMX Intel SSE Intel SSE2 Intel SSE3 Intel SSE4 Teclat Teclat + ratolí Etiqueta:  Velocitat màxima:  Lector de «Memory Stick» Muntat a:  Ratolí Reproductors multimèdia Josep Ma. Ferrer No Sense càrrega No hi ha dades disponibles No està muntat No definida Unitat òptica PDA Taula de particions Primària Processador %1 Número de processador:  Processadors Producte:  RAID Extraïble? SATA SCSI Lector de «SD/MMC» Mostra tots els dispositius Mostra els dispositius apropiats Lector de «Smart Media» Unitats d'emmagatzematge Controladors acceptats:  Jocs d'instruccions admesos:  Protocols acceptats:  UDI:  UPS USB UUID:  Mostra l'UDI («Unique Device Identifier», Identificador únic de dispositiu) del dispositiu actual Unitat desconeguda Sense ús Fabricant:  Mida del volum: Ús del volum:  Sí kcmdevinfo Desconegut Cap Sense Plataforma Desconeguda Desconegut Desconegut Desconegut Desconegut Lector de «xD» 