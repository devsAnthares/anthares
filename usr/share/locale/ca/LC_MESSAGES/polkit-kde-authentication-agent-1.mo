��            )   �      �  ;   �  J   �          /  ~   7  A   �     �       )        G     X     i      q     �     �     �  
   �     �  
   �     �             "   <     _  	   y     �     �     �  �  �     �     �     �     �  �   �  Q   4     �     �  <   �     �      	     	     	     3	     G	     W	  
   e	     p	     �	     �	  "   �	  0   �	  =   �	  '   <
     d
     q
     �
     �
                                      
                                                                                            	        %1 is the full user name, %2 is the user login name%1 (%2) %1 is the name of a detail about the current action provided by polkit%1: (c) 2009 Red Hat, Inc. Action: An application is attempting to perform an action that requires privileges. Authentication is required to perform this action. Another client is already authenticating, please try again later. Application: Authentication Required Authentication failure, please try again. Click to edit %1 Click to open %1 Details EMAIL OF TRANSLATORSYour emails Former maintainer Jaroslav Reznik Lukáš Tinkl Maintainer NAME OF TRANSLATORSYour names P&assword: Password for %1: Password for root: Password or swipe finger for %1: Password or swipe finger for root: Password or swipe finger: Password: PolicyKit1 KDE Agent Select User Vendor: Project-Id-Version: polkit-kde-authentication-agent-1
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:22+0100
PO-Revision-Date: 2015-01-11 16:04+0100
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.4
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
 %1 (%2) %1: (c) 2009 Red Hat, Inc. Acció: Una aplicació està intentant de dur a terme una acció que requereix privilegis. Cal autentificar-se per dur a terme aquesta acció. Un altre client ja s'està autenticant. Si us plau, torneu-ho a provar més tard. Aplicació: Es requereix autenticació Ha fallat l'autenticació, si us plau, torneu-ho a intentar. Cliqueu per editar %1 Cliqueu per obrir %1 Detalls manutortosa@gmail.com Mantenidor anterior Jaroslav Reznik Lukáš Tinkl Mantenidor Manuel Tortosa Moreno Contr&asenya: Contrasenya per a %1: Contrasenya per a l'administrador: Poseu la contrasenya o llisqueu el dit per a %1: Poseu la contrasenya o llisqueu el dit per a l'administrador: Poseu la contrasenya o llisqueu el dit: Contrasenya: Agent PolicyKit1 del KDE Selecció d'usuari Proveïdor: 