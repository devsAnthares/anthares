��          �            x  
   y  	   �     �     �  3   �     �     �                    %     *     F  B   Y     �  �  �  	   �  
   �     �     �  8   �                 2     E     Y     a     f  !   �  	   �     �     	                                                  
                        Appearance Browse... Choose an image Fifteen Puzzle Image Files (*.png *.jpg *.jpeg *.bmp *.svg *.svgz) Number color Path to custom image Piece color Show numerals Shuffle Size Solve by arranging in order Solved! Try again. The time since the puzzle started, in minutes and secondsTime: %1 Use custom image Project-Id-Version: plasma_applet_org.kde.plasma.fifteenpuzzle
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-01-12 03:59+0100
PO-Revision-Date: 2017-01-13 17:45+0100
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
 Aparença Explora... Tria una imatge Trencaclosques quinze Fitxers d'imatge (*.png *.jpg *.jpeg *.bmp *.svg *.svgz) Nombre de colors Camí a la imatge personalitzada Color de les peces Mostra els números Barreja Mida Se soluciona posant-ho en ordre Solucionat! Torneu-ho a intentar. Temps: %1 Usa una imatge personalitzada 