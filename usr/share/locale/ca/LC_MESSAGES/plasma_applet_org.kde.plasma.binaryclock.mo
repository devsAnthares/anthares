��    	      d      �       �   
   �      �      �   	              "     C  "   ]  �  �  	   ]     g     o     �     �  -   �  /   �  /                                	                 Appearance Colors: Display seconds Draw grid Show inactive LEDs: Use custom color for active LEDs Use custom color for grid Use custom color for inactive LEDs Project-Id-Version: plasma_applet_org.kde.plasma.binaryclock
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-31 05:47+0100
PO-Revision-Date: 2017-05-03 19:31+0100
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
 Aparença Colors: Mostra els segons Dibuixa la quadrícula Mostra els LED inactius: Usa un color personalitzat per als LED actius Usa un color personalitzat per a la quadrícula Usa un color personalitzat per als LED inactius 