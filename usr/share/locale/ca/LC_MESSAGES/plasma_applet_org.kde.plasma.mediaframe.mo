��            )   �      �     �     �     �     �     �     �     �  
           )   (     R     V     \     c     v     �     �     �  3   �     �       <   #  5   `  8   �  8   �                    /  �  1                7     E     [     r     �     �     �  6   �     �     �     �  $        ,     I     i     |  3   �     �     �  <   �  :   .	  8   i	  8   �	     �	     �	     �	     
                                                  
                                                                               	        Add files... Add folder... Background frame Change picture every Choose a folder Choose files Configure plasmoid... Fill mode: General Left click image opens in external viewer Pad Paths Paths: Pause on mouseover Preserve aspect crop Preserve aspect fit Randomize items Stretch The image is duplicated horizontally and vertically The image is not transformed The image is scaled to fit The image is scaled uniformly to fill, cropping if necessary The image is scaled uniformly to fit without cropping The image is stretched horizontally and tiled vertically The image is stretched vertically and tiled horizontally Tile Tile horizontally Tile vertically s Project-Id-Version: plasma_applet_org.kde.plasma.mediaframe
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-16 03:31+0100
PO-Revision-Date: 2017-11-16 22:33+0100
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Generator: Lokalize 2.0
 Afegeix fitxers... Afegeix una carpeta... Marc del fons Canvia la imatge cada Selecciona una carpeta Selecció de fitxers Configura el plasmoide... Mode d'emplenat: General Un clic esquerre a la imatge l'obre en un visor extern Normal Camins Camins: Pausa en passar el ratolí per sobre Conserva l'aspecte de retall Conserva l'aspecte d'ajustament Elements aleatoris Estira La imatge es duplica horitzontalment i verticalment La imatge no es transforma La imatge s'escala per ajustar La imatge s'escala uniformement per omplir, retallant si cal La imatge s'escala uniformement per ajustar sense retallar La imatge s'estira horitzontalment i amb mosaic vertical La imatge s'estira verticalment i amb mosaic horitzontal Mosaic Mosaic horitzontal Mosaic vertical s 