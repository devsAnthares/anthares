��          �   %   �      @     A  �   `  m   �  �   P  )   �  `        o     |     �     �     �      �     �     �  '        ,     >     ]     b     s  ?   �  Y   �  +     H   F  �  �     Q  �   p  n   �     h	     �	  b   �	  
   �	     

     &
     3
  "   :
     ]
     p
     �
  +   �
     �
     �
     �
     �
       6     ]   M  /   �  9   �                                                 	                                                               
              (c) 2003-2007 Fredrik Höglund <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) @item:inlistbox sizeResolution dependent A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Cursor Theme Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Fredrik Höglund Get new Theme Get new color schemes from the Internet Install from File NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. You have to restart the Plasma session for these changes to take effect. Project-Id-Version: kcmmousetheme
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-08-21 10:27+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 (c) 2003-2007 Fredrik Höglund <qt>Naozaj chcete odstrániť kurzorovú tému <i>%1</i>?<br />Tým odstránite všetky súbory, ktoré táto téma nainštalovala.</qt> <qt>Nemôžete vymazať tému, ktorú práve používate.<br />Najprv sa musíte prepnúť na inú tému.</qt> (Dostupné veľkosti: %1) Závislé od rozlíšenia Téma s názvom %1 už existuje vo vašom priečinku kurzorových tém. Chcete ju nahradiť novou? Potvrdenie Nastavenie kurzora zmenené Cursor Theme Popis	 Ťahajte alebo napíšte URL témy wizzarsk@gmail.com Fredrik Höglund Získať novú tému Získať nové farebné schémy z Internetu Nainštalovať zo súboru Roman Paholík Názov Prepísať tému? Odstrániť tému Súbor %1 asi nie je platný archív kurzorovej témy. Nepodarilo sa stiahnuť archív kurzorovej témy. Prosím, overte, že je adresa %1 správna. Nepodarilo sa nájsť archív témy kurzora %1. Plasma musí byť reštartovaná, aby sa zmena prejavila. 