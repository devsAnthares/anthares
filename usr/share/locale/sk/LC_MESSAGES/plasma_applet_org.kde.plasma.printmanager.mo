��          �      ,      �     �     �     �     �     �     �     �     �  .   �     .     L     \     m     �     �  H   �  �  �     �     �     �     �          *     6     N  4   ]  <   �     �     �     	  
   &     1  g   F        	                                                                  
    &Configure Printers... Active jobs only All jobs Completed jobs only Configure printer General No active jobs No jobs No printers have been configured or discovered One active job %1 active jobs One job %1 jobs Open print queue Print queue is empty Printers Search for a printer... There is one print job in the queue There are %1 print jobs in the queue Project-Id-Version: plasma_applet_org.kde.printmanager
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2015-02-21 09:37+0000
PO-Revision-Date: 2015-02-21 19:43+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 Nastaviť tlačiarne... Iba aktívne úlohy Všetky úlohy Iba ukončené úlohy Nastaviť tlačiareň Všeobecné Žiadne aktívne úlohy Žiadna úloha Žiadne tlačiarne neboli nastavené alebo nájdené Jedna aktívne úloha %1 aktívne úlohy %1 aktívnych úloh Jedna úloha %1 úlohy %1 úloh Otvoriť tlačovú frontu Tlačová fronta je prázdna Tlačiarne Hľadať tlačiareň Je jedna tlačová úloha vo fronte Sú %1 tlačové úlohy vo fronte Je %1 tlačových úloh vo fronte 