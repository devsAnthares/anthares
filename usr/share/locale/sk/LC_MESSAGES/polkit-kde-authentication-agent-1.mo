��            )   �      �  ;   �  J   �          /  ~   7  A   �     �       )        G     X     i      q     �     �     �  
   �     �  
   �     �             "   <     _  	   y     �     �     �  �  �     �     �     �     �  }   �  G   +     s  !     ,   �     �     �     �     	     	     .	     >	     L	     U	     d	     l	     z	  #   �	  &   �	     �	     �	     �	     
  	   &
                                      
                                                                                            	        %1 is the full user name, %2 is the user login name%1 (%2) %1 is the name of a detail about the current action provided by polkit%1: (c) 2009 Red Hat, Inc. Action: An application is attempting to perform an action that requires privileges. Authentication is required to perform this action. Another client is already authenticating, please try again later. Application: Authentication Required Authentication failure, please try again. Click to edit %1 Click to open %1 Details EMAIL OF TRANSLATORSYour emails Former maintainer Jaroslav Reznik Lukáš Tinkl Maintainer NAME OF TRANSLATORSYour names P&assword: Password for %1: Password for root: Password or swipe finger for %1: Password or swipe finger for root: Password or swipe finger: Password: PolicyKit1 KDE Agent Select User Vendor: Project-Id-Version: polkit-kde-authentication-agent-1
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:22+0100
PO-Revision-Date: 2015-01-09 15:36+0100
Last-Translator: Roman Paholík <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 %1 (%2) %1: (c) 2009 Red Hat, Inc. Akcia: Aplikácia sa pokúša vykonať činnosť, ktorá vyžaduje práva. Na vykonanie tejto činnosti je potrebné overenie práv. Iný klient už vykonáva overovanie, prosím skúste to neskôr znovu. Aplikácia: Vyžaduje sa overenie totožnosti Overenie zlyhalo, prosím, skúste to znovu. Kliknite na úpravu %1 Kliknite na otvorenie %1 Detaily wizzardsk@gmail.com Predošlý správca Jaroslav Reznik Lukáš Tinkl Správca Roman Paholík H&eslo: Heslo pre %1: Heslo pre roota: Heslo alebo odtlačok prsta pre %1: Heslo alebo odtlačok prsta pre roota: Heslo alebo odtlačok prsta: Heslo: PolicyKit1 KDE Agent Vybrať používateľa Predajca: 