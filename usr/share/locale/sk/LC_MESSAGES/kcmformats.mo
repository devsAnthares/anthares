��          �      �           	          "     4  	   A     K  a   R     �  
   �  	   �     �     �                 3      T     u  	   �     �     �     �  7   �  �  �     �     �     �     �     �     �  c   �     \     w     }     �     �     �     �     �     �     �  	                    6   %                                                               
                              	           %1 (%2) %1 (long format) %1 (short format) %1 - %2 (%3) &Numbers: &Time: <h1>Formats</h1>You can configure the formats used for time, dates, money and other numbers here. Co&llation and Sorting: Currenc&y: Currency: De&tailed Settings Format Settings Changed Measurement &Units: Measurement Units: Measurement comboboxImperial UK Measurement comboboxImperial US Measurement comboboxMetric No change Numbers: Re&gion: Time: Your changes will take effect the next time you log in. Project-Id-Version: kcmlocale
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-11 03:20+0100
PO-Revision-Date: 2015-11-08 10:33+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 %1 (%2) %1 (dlhý formát) %1 (krátky formát) %1 - %2 (%3) Čí&slice: Čas: <h1>Formáty</h1>Tu môžete nastaviť formáty použité pre čas, dátum, peniaze a iné čísla. Porovnávanie a triedenie: Mena: Mena: Podrobné nastavenia Nastavenia formátu sa zmenili Systém jednotiek: Systém jednotiek: Imperiálny UK Imperiálny US Metrika Bez zmeny Čísla: Región: Čas: Vaše zmeny sa prejavia pri najbližšom prihlásení. 