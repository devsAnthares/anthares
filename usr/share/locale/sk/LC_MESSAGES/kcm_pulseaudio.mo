��    $      <  5   \      0     1     6  )   J     t  $   �  &   �  #   �  !   �  "   !     D     T     f     z     �  J   �     �  L        [     c     k      {     �  
   �     �     �     �     �     �  "   �       )   9  <   c     �  ;   �     �  �       �     �     �     �  &   	  %   /	  ,   U	  %   �	  '   �	     �	  
   �	     �	  
   �	  
   �	  k   
     q
  N   �
  	   �
     �
     �
     	          $     1     @     T     ]     j     o     ~  )   �  >   �     �     �          	                                                                         !       #             "                                        
           $                               100% @info:creditAuthor @info:creditCopyright 2015 Harald Sitter @info:creditHarald Sitter @labelNo Applications Playing Audio @labelNo Applications Recording Audio @labelNo Device Profiles Available @labelNo Input Devices Available @labelNo Output Devices Available @labelProfile: @titlePulseAudio @title:tabAdvanced @title:tabApplications @title:tabDevices Add virtual output device for simultaneous output on all local sound cards Advanced Output Configuration Automatically switch all running streams when a new output becomes available Capture Default Device Profiles EMAIL OF TRANSLATORSYour emails Inputs Mute audio NAME OF TRANSLATORSYour names Notification Sounds Outputs Playback Port Port is unavailable (unavailable) Port is unplugged (unplugged) Requires 'module-gconf' PulseAudio module This module allows to set up the Pulseaudio sound subsystem. label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: kcm_pulseaudio
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-03 03:00+0200
PO-Revision-Date: 2017-05-08 17:09+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 100% Autor Copyright 2015 Harald Sitter Harald Sitter Žiadne aplikácie neprehrávajú zvuk Žiadne aplikácie nenahrávajú zvuk Nie sú dostupné žiadne profily zariadenia Nie sú dostupné vstupné zariadenia Nie sú dostupné výstupné zariadenia Profil: PulseAudio Pokročilé Aplikácie Zariadenia Pridať virtuálne výstupné zariadenie pre súčasný výstup na všetkých miestnych zvukových kartách Pokročilé nastavenie výstupu Automaticky prepnúť všetky bežiace prúdy pri dostupnosti nového výstupu Zachytiť Predvolené Profily zariadenia wizzardsk@gmail.com Vstupy Stlmiť zvuk Roman Paholík Zvuky notifikácií Výstupy Prehrávanie Port  (nedostupné)  (nezapojené) Vyžaduje PulseAudio modul 'module-gconf' Tento modul umožní nastaviť zvukový subsystém Pulseaudio. %1: %2 100% %1% 