��    *      l  ;   �      �     �     �  )   �               /     H     ^  #   ~     �  
   �     �     �  %   �  +        E     Z     m  @   z     �     �     �     �               '     ,     9     ?     _     e  .   m     �  F   �  (   �  	   '  	   1  	   ;  '   E  7   m  "   �  �  �     �	     �	  #   �	     �	     �	  
   �	  	   �	     
     
     .
     @
     I
     `
     u
  8   �
     �
     �
     �
  J        Q     h     x     �     �     �     �     �     �     �       	     5   $  #   Z  T   ~  (   �     �                         7                   $                                 %                      *                                               	   #   )   "             &           
                        '             (   !    &Do not remember @actionWalk through activities @actionWalk through activities (Reverse) @action:buttonApply @action:buttonCancel @action:buttonChange... @action:buttonCreate @title:windowActivity Settings @title:windowCreate a New Activity @title:windowDelete Activity Activities Activity information Activity switching Are you sure you want to delete '%1'? Blacklist all applications not on this list Clear recent history Create activity... Description: Error loading the QML files. Check your installation.
Missing %1 For a&ll applications Forget a day Forget everything Forget the last hour Forget the last two hours General Icon Keep history Name: O&nly for specific applications Other Privacy Private - do not track usage for this activity Remember opened documents: Remember the current virtual desktop for each activity (needs restart) Shortcut for switching to this activity: Shortcuts Switching Wallpaper for in 'keep history for 5 months'for  unit of time. months to keep the history month  months unlimited number of monthsforever Project-Id-Version: kcm_activities
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-04-02 21:16+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 Nezapamätať si Prechádzať cez aktivity Prechádzať cez aktivity (spätne) Použiť Zrušiť Zmeniť... Vytvoriť Nastavenie aktivity Vytvoriť novú aktivitu Vymazať aktivitu Aktivity Informácia o aktivite Prepínanie aktivít Naozaj chcete vymazať '%1'? Vylúčiť všetky aplikácie, ktoré nie sú na zozname Vyčistiť nedávnu históriu Vytvoriť aktivitu... Popis: Chyba načítania QML súborov. Skontrolujte vašu inštaláciu.
Chýba %1 Pre všetky aplikácie Zabudnúť deň Zabudnúť všetko Zabudnúť poslednú hodinu Zabudnúť posledné dve hodiny Všeobecné Ikona Ponechať históriu Názov: Iba pre špecifické aplikácie Iné Súkromie Súkromné - nesledovať použitie pre túto aktivitu Zapamätať si otvorené dokumenty: Zapamätať si aktuálnu virtuálnu plochu pre každú aktivitu (potrebuje reštart) Skratka na prepínanie na túto aktivitu Skratky Prepínanie Tapeta pre   mesiac  mesiace  mesiacov navždy 