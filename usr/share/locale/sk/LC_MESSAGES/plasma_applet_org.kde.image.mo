��          �   %   �      @     A     J     _     h  0   v     �     �     �     �     �     �        
        "     7     D     _     p     �     �     �     �     �     �  �  �     �     �     �     �  (   �          "     ;     B     T     \     y     �     �     �     �     �                  !   5     W     _     t                                                       	                                                            
           %1 by %2 Add Custom Wallpaper Centered Change every: Directory with the wallpaper to show slides from Download Wallpapers Get New Wallpapers... Hours Image Files Minutes Next Wallpaper Image Open Containing Folder Open Image Open Wallpaper Image Positioning: Recommended wallpaper file Remove wallpaper Restore wallpaper Scaled Scaled and Cropped Scaled, Keep Proportions Seconds Select Background Color Tiled Project-Id-Version: plasma_applet_org.kde.image
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:39+0100
PO-Revision-Date: 2017-10-27 21:08+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
 %1 od %2 Pridať vlastnú tapetu Na stred Zmeniť každých: Adresár s tapetou na zobrazenie snímok Stiahnuť tapety Získať nové tapety... Hodiny Súbory obrázkov Minúty Nasledujúci obrázok tapety Otvoriť obsahujúci priečinok Otvoriť obrázok Otvoriť obrázok tapety Umiestnenie: Odporúčaný súbor tapety Odstrániť tapetu Obnoviť tapetu Zväčšený Zväčšený a orezaný Zväčšený, zachovať proporcie Sekundy Výber farby pozadia Vydláždiť 