��    $      <  5   \      0      1     R     ^  -   }  #   �  #   �  ?   �  1   3     e     y  H   ~  L   �       V     N   s     �  
   �     �     �     �       2     
   P     [  )   {  8   �  #   �  .     -   1  D   _  6   �  0   �  A     =   N  9   �  �  �  ,   �
  
   �
  9   �
  *     	   B  	   L     V     h     w     �  0   �  .   �  	   �                         +     =     X     w     �     �     �  ,   �     �  (     :   -  #   h  +   �     �     �     �     �     �                                                
       "                                       $                                     #      	           !                                %1 notification %1 notifications %1 of %2 %3 %1 running job %1 running jobs &Configure Event Notifications and Actions... 10 seconds ago, keep short10 s ago 30 seconds ago, keep short30 s ago A button tooltip; expands the item to show detailsShow Details A button tooltip; hides item detailsHide Details Clear Notifications Copy Either just 1 dir or m of n dirs are being processed1 dir %2 of %1 dirs Either just 1 file or m of n files are being processed1 file %2 of %1 files History How much many bytes (or whether unit in the locale has been copied over total%1 of %2 Indicator that there are more urls in the notification than previews shown+%1 Information Job Failed Job Finished No new notifications. No notifications or jobs Open... Placeholder is job name, eg. 'Copying'%1 (Paused) Select All Show a history of notifications Show application and system notifications Speed and estimated time to completion%1 (%2 remaining) Track file transfers and other jobs Use custom position for the notification popup minutes ago, keep short%1 min ago %1 min ago notification was added n days ago, keep short%1 day ago %1 days ago notification was added yesterday, keep shortYesterday notification was just added, keep shortJust now placeholder is row description, such as Source or Destination%1: the job, which can be anything, failed to complete%1: Failed the job, which can be anything, has finished%1: Finished Project-Id-Version: plasma_applet_org.kde.plasma.notifications
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:07+0100
PO-Revision-Date: 2017-08-17 22:51+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 %1 upozornenie %1 upozornenia %1 upozornení %1 z %2 %3 %1 bežiaca úloha %1 bežiace úlohy %1 bežiacich úloh Nastaviť pripomienky a akcie udalostí... pred 10 s pred 30 s Zobraziť detaily Skryť detaily Vyčistiť upozornenia Kopírovať 1 adresár %2 z %1 adresárov %2 z %1 adresárov 1 súbor  %2 z %1 súborov   %2 z %1 súborov  História %1 z %2 +%1 Informácia Úloha zlyhala Úloha ukončená Žiadne nové upozornenia. Žiadne upozornenia ani úlohy Otvoriť... %1 (Pozastavené) Vybrať všetko Zobraziť históriu upozornení Zobraziť upozornenia aplikácií a systému %1 (zostáva: %2) Sledovať presuny súborov a iné úlohy Použiť vlastnú polohu pre vyskakovacie okno upozornenia pred %1 min pred %1 min pred %1 min pred %1 dňom pred %1 dňami pred %1 dňami Včera Práve teraz %1: %1: Zlyhalo %1: Dokončené 