��          �      <      �     �     �     �  +   �  @        L     a     i     w     }     �  
   �     �     �     �     �     �  �  
     �     �       5     C   R     �     �     �     �     �     �       	             .     A     \     
         	                                                                                       <a href='%1'>%1</a> Close Copy Automatically: Don't show this dialog, copy automatically. Drop text or an image onto me to upload it to an online service. Error during upload. General History Size: Paste Please wait Please, try again. Sending... Share Shares for '%1' Successfully uploaded The URL was just shared Upload %1 to an online service Project-Id-Version: plasma_applet_org.kde.plasma.quickshare
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-01 04:04+0100
PO-Revision-Date: 2015-04-30 17:34+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 <a href='%1'>%1</a> Zavrieť Kopírovať automaticky: Nezobrazovať tento dialóg, kopírovať automaticky. Pustite sem text alebo obrázok na jeho nahranie na online službu. Chyba počas nahrávania. Všeobecné Veľkosť histórie: Vložiť Prosím, čakajte Prosím, skúste znova. Posielam... Zdieľať Zdieľania pre '%1' Úspešne nahraté URL bolo práve zdieľané Nahrať %1 na online službu 