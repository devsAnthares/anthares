��          <      \       p      q   *   �   /   �   �  �   (   �  @   �  G                      Cannot find '%1' using %2. Connection to %1 weather server timed out. Weather information retrieval for %1 timed out. Project-Id-Version: libplasmaweather
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-13 03:17+0100
PO-Revision-Date: 2010-07-24 12:32+0200
Last-Translator: Michal Sulek <misurel@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 Nie je možné nájsť  '%1' pomocou %2. Vypršal časový limit pri pripojení na server s počasím %1. Vypršal časový limit pri získavaní informácií o počasí pre %1. 