��    	      d      �       �   	   �      �   <   �   5   0     f     �  4   �     �  �  �     �     �     �     �  )   �               "                                          	    Ends at 5 General Show the number of the day (eg. 31) in the iconDay in month Show the week number (eg. 50) in the iconWeek number Show week numbers in Calendar Starts at 9 What information is shown in the calendar iconIcon: Working Day Project-Id-Version: plasma_applet_org.kde.plasma.calendar
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2016-06-20 20:30+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 Končí o 5 Všeobecné Deň v mesiaci Číslo týždňa Zobraziť čísla týždňov v kalendári Začína o 9 Ikona: Pracovný deň 