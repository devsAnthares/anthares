��    R      �  m   <      �     �            5     #   U  E   y  E   �  >        D  7   [     �     �     �     �     	     	     !	     (	     /	     B	     Z	     h	     }	     �	     �	     �	  !   �	  F   �	     
     9
     J
  <   \
     �
     �
  *   �
     �
     �
     �
  	   �
     �
                     >  
   \     g     �  
   �  F   �  :   �          "     )     <     C  8   R     �     �  	   �  
   �     �     �     �     �     �               6     G     P     o     u     �  
   �     �     �     �     �     �       $     �  <     �            A   ,  *   n  L   �  L   �  E   3     y  ;   �     �  '   �  !     *   -     X     f     x     �     �     �     �     �     �     �     �       *   "  B   M  !   �     �     �  5   �            )         J     Z     h     w     �     �  	   �  %   �  $   �     �  &        -     5  Z   K  :   �     �     �     �     	       ?   $     d     m     �     �     �  
   �     �     �     �          
     %     8      @     a     h     u     �     �     �     �     �     �     
  #   $            /   ;       C                 @           L   Q   1   (       9   $   A   H                      :   P   	              R                 5       *             4          #   E   +      M      =   !   O          J   "       7   6          N         .   G   >   B   ?            2   )      ,   K   I   F   -                      '   <          %   &   D       8               0   3   
              
Also available in %1 %1 (%2) <b>%1</b> by %2 <em>%1 out of %2 people found this review useful</em> <em>Tell us about this review!</em> <em>Useful? <a href='true'><b>Yes</b></a>/<a href='false'>No</a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'><b>No</b></a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'>No</a></em> @infoFetching updates @infoIt is unknown when the last check for updates was @infoNo updates @infoNo updates are available @infoShould check for updates @infoThe system is up to date @infoUpdates @infoUpdating... Accept Addons Aleix Pol Gonzalez An application explorer Apply Changes Available backends:
 Available modes:
 Back Cancel Checking for updates... Compact Mode (auto/compact/full). Could not close the application, there are tasks that need to be done. Could not find category '%1' Couldn't open %1 Delete the origin Directly open the specified application by its package name. Discard Discover Display a list of entries with a category. Extensions... Help... Install Installed Jonathan Thomas Launch License: List all the available backends. List all the available modes. Loading... Local package file to install More... No Updates Open Discover in a said mode. Modes correspond to the toolbar buttons. Open with a program that can deal with the given mimetype. Rating: Remove Resources for '%1' Review Reviewing '%1' Running as <em>root</em> is discouraged and unnecessary. Search Search in '%1'... Search... Search: %1 Search: %1 + %2 Settings Short summary... Show reviews (%1)... Sorry, nothing found... Source: Specify the new source for %1 Still looking... Summary: Supports appstream: url scheme Tasks Tasks (%1%) Unable to find resource: %1 Update All Update Selected Update section nameUpdate (%1) Updates unknown reviewer updates not selected updates selected © 2010-2016 Plasma Development Team Project-Id-Version: muon-discover
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:02+0100
PO-Revision-Date: 2017-10-27 20:52+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 
Tiež dostupné v %1 %1 (%2) <b>%1</b> od %2 <em>%1 z %2 ľudí považujú toto hodnotenie za užitočné</em> <em>Povedzte nám o tomto hodnotení!</em> <em>Užitočné? <a href='true'><b>Áno</b></a>/<a href='false'>Nie</a></em> <em>Užitočné? <a href='true'>Áno</a>/<a href='false'><b>Nie</b></a></em> <em>Užitočné? <a href='true'>Áno</a>/<a href='false'>Nie</a></em> Sťahujem aktualizácie Nie je známe, kedy sa naposledy kontrolovali aktualizácie Žiadne aktualizácie Žiadne aktualizácie nie sú dostupné Treba skontrolovať aktualizácie Softvér na tomto počítači je aktuálny Aktualizácie Aktualizuje sa... Akceptovať Doplnky Aleix Pol Gonzalez Prieskumník aplikácií Aplikovať zmeny Dostupné backendy:
 Dostupné režimy:
 Späť Zrušiť Kontrolujú sa aktualizácie... Kompaktný režim (auto/kompaktný/plný). Nemôžem zatvoriť aplikáciu, je treba ukončiť priveľa úloh. Nemôžem nájsť kategóriu '%1' Nemôžem otvoriť %1 Vymazať pôvod Priamo otvoriť danú aplikáciu názvom jej balíka. Zahodiť Objaviť Zobraziť zoznam položiek s kategóriou. Rozšírenia... &Pomocník... Nainštalovať Nainštalované Jonathan Thomas Spustiť Licencia: Vypísať všetky dostupné backendy. Vypísať všetky dostupné režimy. Načítava sa... Miestny súbor balíka na inštaláciu Viac... Žiadne aktualizácie Otvoriť Discover a danom režime. Režimy korešpondujú s tlačidlami panelu nástrojov. Otvoriť v programe, ktorý vie spracovať daný mime typ. Hodnotenie: Odstrániť Zdroje pre '%1' Revízia Posudzovanie '%1' Spustenie ako <em>root</em> nie je odporúčané ani potrebné. Hľadať Hľadať v '%1'... Hľadať... Hľadať: %1 Hľadať: %1 + %2 Nastavenia Krátky súhrn... Zobraziť revízie (%1)... Prepáčte, nič sa nenašlo... Zdroj: Zadajte nový zdroj pre %1 Stále hľadám... Súhrn: Podporuje appstream: url schéma Úlohy Úlohy (%1%) Nemôžem nájsť zdroj: %1 Aktualizovať všetko Vybraná aktualizácia Aktualizovať (%1) Aktualizácie neznámy revízor aktualizácie nevybraté aktualizácií vybratých © 2010-2016 Vývojový tím Plasma 