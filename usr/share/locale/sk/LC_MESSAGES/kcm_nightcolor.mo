��          �      �           	               4  	   I     S      c  "   �      �     �     �  	   �     �               )  
   9     D     S     a     g     {  �  �     G     J      R     s     �     �     �  "   �     �               #     6     E     S     k  
   �     �     �     �     �  	   �                              	                                     
                                      K (HH:MM) (In minutes - min. 1, max. 600) Activate Night Color Automatic Detect location EMAIL OF TRANSLATORSYour emails Error: Morning not before evening. Error: Transition time overlaps. Latitude Location Longitude NAME OF TRANSLATORSYour names Night Color Night Color Temperature:  Operation mode: Roman Gilg Sunrise begins Sunset begins Times Transition duration and ends Project-Id-Version: kcm_nightcolor
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-12 03:09+0100
PO-Revision-Date: 2017-12-14 21:54+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
  K (HH:MM) (v minútach - min. 1, max. 600) Aktivovať nočnú farbu Automatické Zistiť umiestnenie wizzardsk@gmail.com Chyba: Ráno nie je pred večerom. Chyba: Čas prechodu presahuje. Zemepisná šírka Umiestnenie Zemepisná dĺžka Roman Paholík Nočná farba Teplota nočnej farby:  Prevádzkový režim: Roman Gilg Slnko vychádza Slnko zapadá Časy: Trvanie prechodu a končí 