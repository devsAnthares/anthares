��          �      <      �     �  <   �  
   �     	          &     3     ?  
   G     R     c     q     }     �     �  
   �     �  �  �     �  M   �                    3     N     ]     i     v     �     �     �     �      �          "                                                  
   	                                                Add Launcher... Add launchers by Drag and Drop or by using the context menu. Appearance Arrangement Edit Launcher... Enable popup Enter title General Hide icons Maximum columns: Maximum rows: Quicklaunch Remove Launcher Show hidden icons Show launcher names Show title Title Project-Id-Version: plasma_applet_org.kde.plasma.quicklaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-02-26 04:09+0100
PO-Revision-Date: 2016-02-23 18:27+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 Pridať spúšťač... Spúšťače pridáte pretiahnutím myšou, alebo pomocou kontextového menu. Vzhľad Usporiadanie Upraviť spúšťač... Povoliť vyskakovacie okno Zadajte názov Všeobecné Skryť ikony Maximálny počet stĺpcov: Maximálny počet riadkov: Rýchle spustenie Odstrániť spúšťač Zobraziť skryté ikony Zobrazovať názvy spúšťačov Zobraziť titulok Názov 