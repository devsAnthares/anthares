��    >        S   �      H  	   I  	   S     ]     e     s     y          �     �     �     �     �     �     �  
        "  ?   .     n  >   w  	   �     �     �  S   �      A     b  �  z     	     
	     	  
   '	     2	     B	     R	  
   _	     j	  A   y	     �	     �	  
   �	     �	     �	     
     #
     ,
     ;
     G
     X
     h
     |
     �
     �
     �
     �
     �
     �
               (  T   ;     �  S   �  �  �  
   �     �     �     �  	   �     �     �     �     
     )     2     Q     p     �     �     �  ?   �       =   !  	   _     i     �  X   �  q         r  �  �          '     :     K     \     l     |     �     �  E   �     �               3  U   G     �     �     �     �     �     �          "     4     N     e     z     �     �     �     �     �  [        `  =   n            3          1   6       2   4      !   *         #   7                    (   9          &           :   5          +                    "                        '           -          >   =   /   	      )                ;   
      .      $   %       <   ,                    8      0            [Hidden] &Comment: &Delete &Description: &Edit &File &Name: &New Submenu... &Run as a different user &Sort &Sort all by Description &Sort all by Name &Sort selection by Description &Sort selection by Name &Username: &Work path: (C) 2000-2003, Waldo Bastian, Raffaele Sandrini, Matthias Elter Advanced All submenus of '%1' will be removed. Do you want to continue? Co&mmand: Could not write to %1 Current shortcut &key: Do you want to restore the system menu? Warning: This will remove all custom menus. EMAIL OF TRANSLATORSYour emails Enable &launch feedback Following the command, you can have several place holders which will be replaced with the actual values when the actual program is run:
%f - a single file name
%F - a list of files; use for applications that can open several local files at once
%u - a single URL
%U - a list of URLs
%d - the folder of the file to open
%D - a list of folders
%i - the icon
%m - the mini-icon
%c - the caption General General options Hidden entry Item name: KDE Menu Editor KDE menu editor Main Toolbar Maintainer Matthias Elter Menu changes could not be saved because of the following problem: Menu entry to pre-select Montel Laurent Move &Down Move &Up NAME OF TRANSLATORSYour names New &Item... New Item New S&eparator New Submenu Only show in KDE Original Author Previous Maintainer Raffaele Sandrini Restore to System Menu Run in term&inal Save Menu Changes? Show hidden entries Spell Checking Spell checking Options Sub menu to pre-select Submenu name: Terminal &options: Unable to contact khotkeys. Your changes are saved, but they could not be activated. Waldo Bastian You have made changes to the menu.
Do you want to save the changes or discard them? Project-Id-Version: kmenuedit
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2013-09-03 17:42+0200
Last-Translator: Roman Paholík <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
  [Skryté] &Komentár: &Odstrániť &Popis: &Upraviť &Súbor &Názov: &Nové podmenu... Spustiť ako &iný užívateľ Triediť Zoradiť všetko podľa popisu Zoradiť všetko podľa názvu Zoradiť výber podľa popisu Zoradiť výber podľa názvu &Užívateľské meno: Pracovná &cesta: (C) 2000-2003, Waldo Bastian, Raffaele Sandrini, Matthias Elter Pokročilé Všetky podmenu '%1' budú odstránené. Chcete pokračovať? &Príkaz: Nepodarilo sa zapísať do %1 Aktuálna klávesová &skratka: Chcete obnoviť systémové menu? Upozornenie: týmto odstránite všetky vlastné menu. hornicek@globtel.sk,visnovsky@nenya.ms.mff.cuni.cz,orpheus@hq.alert.sk,Richard.Fric@kdemail.net,misurel@gmail.com P&ovoliť odozvu pri spustení Za príkazom je možné zadať rôzne zastupiteľné znaky, ktoré sa pri spustení programu nahradia skutočnými hodnotami:
%f - názov súboru
%F - zoznam súborov, napr. pre aplikácie, ktoré dokážu otvoriť niekoľko lokálnych súborov naraz
%u - jedno URL
%U - zoznam URL
%d - priečinok súboru, ktorý sa má otvoriť
%D - zoznam priečinkov
%i - ikona
%m - mini ikona
%c - titulok Všeobecné Všeobecné voľby Skrytá položka Názov položky: KDE Editor menu KDE Editor menu Hlavný panel nástrojov Správca Matthias Elter Zmeny v menu sa nepodarilo uložiť, kvôli nasledujúcemu problému: Položka menu na predvolenie Montel Laurent Posunúť &nižšie Posunúť &vyššie Tomáš Horníček,Stanislav Višňovský,Pavol Cvengroš,Richard Frič,Michal Šulek Nová &položka... Nová položka Nový &oddeľovač Nové podmenu Zobrazovať len v KDE Originálny autor Predchádzajúci správca Raffaele Sandrini Obnoviť systémové menu Spustiť v &termináli Uložiť zmeny menu? Zobraziť skryté položky Kontrola pravopisu Možnosti kontroly pravopisu Podmenu na predvolenie Názov podmenu: &Nastavenia terminálu: Nepodarilo sa kontaktovať khotkeys. Vaše zmeny boli uložené, ale nedajú sa aktivovať. Waldo Bastian Urobili ste v menu zmeny.
Chcete ich uložiť alebo zahodiť? 