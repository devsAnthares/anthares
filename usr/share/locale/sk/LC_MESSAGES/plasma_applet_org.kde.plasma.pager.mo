��          �   %   �      0  *   1     \  .   s     �     �     �     �     �     �                 
   $     /     5     =     E     ]     t     �     �     �     �  �  �  L   �     �  7        Q     a     r     �     �     �     �     �     �     �     �     �               (     F     `     ~     �     �        
                             	                                                                                          %1 Minimized Window: %1 Minimized Windows: %1 Window: %1 Windows: ...and %1 other window ...and %1 other windows Activity name Activity number Add Virtual Desktop Configure Desktops... Desktop name Desktop number Display: Does nothing General Horizontal Icons Layout: No text Only the current screen Remove Virtual Desktop Selecting current desktop: Show Activity Manager... Shows desktop The pager layoutDefault Vertical Project-Id-Version: plasma_applet_org.kde.plasma.pager
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-19 03:29+0200
PO-Revision-Date: 2016-11-04 21:39+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 %1 minimalizované okno: %1 minimalizované okná: %1 minimalizované okien: %1 okno: %1 okná: %1 okien: ...a %1 iné okno ...a %1 iné okná ...a %1 iné okien Názov aktivity Číslo aktivity Pridať virtuálnu plochu Nastaviť plochy... Názov plochy Číslo plochy Zobrazenie: Nerobí nič Všeobecné Horizontálne Ikony Rozloženie: Žiadny text Iba aktuálna obrazovka Odstrániť virtuálnu plochu Výber aktuálnej plochy: Ukázať správcu aktivít... Zobrazí plochu Predvolené Vertikálne 