��    _                   C   	  /   M  -   }     �     �     �     �      	     	     1	     >	     J	  
   S	     ^	     g	     p	     �	  	   �	     �	     �	     �	  ,   �	  	    
     

  
   )
     4
     L
     `
     u
     �
     �
     �
     �
     �
  	   �
     �
     �
     
               !     (  	   ;     E     ]  
   r     }     �  
   �     �     �     �  
   �     �     �               $     2     @     R     h     y     �     �     �     �  	   �     �     �     �               4     Q     j     �     �     �     �  	   �     �  ,   �          !     0     @     L     S     b     t     �  #   �     �  �  �     �  
   �     �     �     �       '   !     I     \  
   d     o  
   �  
   �  
   �     �     �  	   �  	   �     �     �       3        G  '   S     {     �     �     �     �     �          "     @     L     T  	   `     j     |  	   �     �     �     �  
   �     �     �     �          )     @     R     o     v  
   �     �     �     �     �     �     �               8     Q  +   e     �     �     �     �     �     �  #   �          %  %   ?  #   e  $   �     �     �     �  
               0   0     a     h     v     �     �     �     �     �     �  $        &         Q         Y   5           %       H       J   !              \   *       @   V   4   [       A   (   
       U   B   ]   ^       $   _         '   >       D   K      -                  ?      ,   O   0       R   8   6   W   2   I   =   X   E   #       N   C       T   G   M          3   "         9             +          S          F           ;   :                    &   <   L          P         7          .       /       1                     )            	   Z       @action opens a software center with the applicationManage '%1'... @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Desktop Add to Favorites Add to Panel (Widget) Align search results to bottom All Applications App name (Generic name)%1 (%2) Applications Apps & Docs Behavior Categories Computer Contacts Description (Name) Description only Documents Edit Application... Edit Applications... End session Expand search to bookmarks, files and emails Favorites Flatten menu to a single level Forget All Forget All Applications Forget All Contacts Forget All Documents Forget Application Forget Contact Forget Document Forget Recent Documents General Generic name (App name)%1 (%2) Hibernate Hide %1 Hide Application Icon: Lock Lock screen Logout Name (Description) Name only Often Used Applications Often Used Documents Often used On All Activities On The Current Activity Open with: Pin to Task Manager Places Power / Session Properties Reboot Recent Applications Recent Contacts Recent Documents Recently Used Recently used Removable Storage Remove from Favorites Restart computer Run Command... Run a command or a search query Save Session Search Search results Search... Searching for '%1' Session Show Contact Information... Show In Favorites Show applications as: Show often used applications Show often used contacts Show often used documents Show recent applications Show recent contacts Show recent documents Show: Shut Down Sort alphabetically Start a parallel session as a different user Suspend Suspend to RAM Suspend to disk Switch User System System actions Turn off computer Type to search. Unhide Applications in '%1' Unhide Applications in this Submenu Widgets Project-Id-Version: plasma_applet_org.kde.plasma.kicker
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2017-10-27 21:00+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
 Spravovať '%1'... Vybrať... Vymazať ikonu Pridať na pracovnú plochu Pridať do obľúbených Pridať do panelu (widget) Zarovnať výsledky hľadania na spodok Všetky aplikácie %1 (%2) Aplikácie Aplikácie a dokumenty Správanie Kategórie Počítač Kontakty Popis (názov) Len popis Dokumenty Upraviť aplikáciu... Upraviť aplikácie... Ukončiť sedenie Rozbaliť hľadanie na záložky, súbory a e-maily Obľúbené Sploštiť menu na jednoduchú úroveň Zabudnúť všetko Zabudnúť všetky aplikácie Zabudnúť všetky kontakty Zabudnúť všetky dokumenty Zabudnúť aplikáciu Zabudnúť kontakt Zabudnúť dokument Zabudnúť nedávne dokumenty Všeobecné %1 (%2) Hibernovať Skryť %1 Skryť aplikáciu Ikona: Zamknúť Zamknúť obrazovku Odhlásenie Názov (popis) Iba názov Často použité aplikácie Často použité dokumenty Často použité Na všetkých aktivitách Na aktuálnej aktivite Otvoriť pomocou: Pripnúť k správcovi úloh Miesta Napájanie / Sedenie Vlastnosti Reštartovať Nedávne aplikácie Nedávne kontakty Nedávne dokumenty Nedávno použité Nedávno použité Vymeniteľné úložisko Odstrániť z obľúbených Reštartovať počítač Spustiť príkaz... Spustiť príkaz alebo vyhľadávací dotaz Uložiť sedenie Hľadať Výsledok hľadania Hľadať... Vyhľadáva sa '%1' Sedenie Zobraziť informácie o kontakte... Zobraziť v obľúbených Zobraziť aplikáciu ako: Zobraziť často použité aplikácie Zobraziť často použité kontakty Zobraziť často použité dokumenty Zobraziť nedávne aplikácie Zobraziť nedávne kontakty Ukázať nedávne dokumenty Zobraziť: Vypnúť Zoradiť podľa abecedy Spustiť súbežné sedenie ako iný užívateľ Uspať Uspať do RAM Uspať na disk Prepnúť užívateľa Systém Systémové akcie Vypnúť počítač Zadajte text na vyhľadanie. Odkryť aplikácie v '%1' Odkryť aplikácie v tejto podponuke Widgety 