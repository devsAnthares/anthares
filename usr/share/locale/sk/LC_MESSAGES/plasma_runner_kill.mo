��          �            h     i     r     �  	   �  *   �     �  "   �                 ;   +     g     z       �  �     K     \     q     �  $   �     �  +   �               ,  ?   9     y     �     �     	                 
                                                          &Sort by &Trigger word: &Use trigger word CPU usage It is not sure, that this will take effect Kill Applications Config Process ID: %1
Running as user: %2 Send SIGKILL Send SIGTERM Terminate %1 Terminate running applications whose names match the query. inverted CPU usage kill nothing Project-Id-Version: plasma_runner_kill
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2009-12-28 16:28+0100
Last-Translator: Michal Sulek <misurel@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 &Triediť podľa &Spúšťacie slovo: &Použiť spúšťacie slovo využitia CPU Nie je isté, že to bude mať efekt Nastavenie zabitia aplikácií ID procesu: %1
Beží pod užívateľom: %2 Poslať SIGKILL Poslať SIGTERM Ukončiť %1 Ukončí bežiace aplikácie zodpovedajúce hľadaným názvom. využitia CPU (obrátene) kill ničoho 