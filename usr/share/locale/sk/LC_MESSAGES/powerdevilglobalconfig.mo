��          �      l      �     �     �     �  *         +  >   ?  9   ~     �     �  
   �      �       	        (  !   :     \  $   {  	   �     �  �   �  �  8                    *     B  M   X  J   �     �     
     &     4  	   H     R     ^  ,   w     �  5   �     �     �  �   �                              
                                                  	                  % &Critical level: &Low level: <b>Battery Levels                     </b> A&t critical level: Battery will be considered critical when it reaches this level Battery will be considered low when it reaches this level Configure Notifications... Critical battery level Do nothing EMAIL OF TRANSLATORSYour emails Enabled Hibernate Low battery level Low level for peripheral devices: NAME OF TRANSLATORSYour names Pause media players when suspending: Shut down Suspend The Power Management Service appears not to be running.
This can be solved by starting or scheduling it inside "Startup and Shutdown" Project-Id-Version: powerdevilglobalconfig
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-11 03:05+0200
PO-Revision-Date: 2017-05-18 21:53+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 % Kritická úroveň: Nízka úroveň: <b>Úrovne batérie</b> Na kritickej úrovni: Nabitie batérie bude považované za kritické, keď dosiahne túto úroveň Nabitie batérie bude považované za nízke, keď dosiahne túto úroveň Nastaviť upozornenia... Kritická úroveň batérie Nerobiť nič wizzardsk@gmail.com Povolené Hibernovať Nízka úroveň batérie Nízka úroveň v periférnych zariadeniach: Roman Paholík Pozastaviť prehrávače médií počas pozastavenia: Vypnúť Uspať Zdá sa, že služba správy napájania nebeží.
Toto môžte vyriešiť jej spustením alebo naplánovnaím v "Spustenie a vypnutie" 