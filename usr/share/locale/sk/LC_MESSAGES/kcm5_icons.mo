��    ?        Y         p     q     z     �     �     �     �  �  �  �   �  -   -	  )   [	  -   �	  +   �	  r   �	  	   R
  	   \
     f
     ~
     �
     �
  
   �
     �
     �
     �
     �
      �
     �
                      2     S     Y  r   t  5   �          /     I     Z     g     �  	   �     �     �     �  (   �     �     �                5     ;  +   G  3   s     �     �     �     �  "   �  S   �  )   I     s  �     �  ]  
        %     -     <     M     T  �  o  �   �  .   |     �  
   �     �  [   �     (     5     C     [  	   c  
   m  	   x     �     �     �     �  %   �     �     �     �     �           0  #   6  p   Z  )   �     �          )     :     S     s  
   x     �     �     �  )   �     �  $   �  (     &   /  
   V     a  (   m  .   �     �     �     �     �  %   �  Q   #  &   u     �  �   �            0           6   ;          =                   #                        8   *   -   )       %   :         &                   '   
   7   3      "              2          $      9           	   ?   5   <          >      (   +       .       ,                                         !            1             /   4    &Amount: &Effect: &Second color: &Semi-transparent &Theme (c) 2000-2003 Geert Jansen <h1>Icons</h1>This module allows you to choose the icons for your desktop.<p>To choose an icon theme, click on its name and apply your choice by pressing the "Apply" button below. If you do not want to apply your choice you can press the "Reset" button to discard your changes.</p><p>By pressing the "Install Theme File..." button you can install your new icon theme by writing its location in the box or browsing to the location. Press the "OK" button to finish the installation.</p><p>The "Remove Theme" button will only be activated if you select a theme that you installed using this module. You are not able to remove globally installed themes here.</p><p>You can also specify effects that should be applied to the icons.</p> <qt>Are you sure you want to remove the <strong>%1</strong> icon theme?<br /><br />This will delete the files installed by this theme.</qt> <qt>Installing <strong>%1</strong> theme</qt> @label The icon rendered as activeActive @label The icon rendered as disabledDisabled @label The icon rendered by defaultDefault A problem occurred during the installation process; however, most of the themes in the archive have been installed Ad&vanced All Icons Antonio Larrosa Jimenez Co&lor: Colorize Confirmation Desaturate Description Desktop Dialogs Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Effect Parameters Gamma Geert Jansen Get New Themes... Get new themes from the Internet Icons Icons Control Panel Module If you already have a theme archive locally, this button will unpack it and make it available for KDE applications Install a theme archive file you already have locally Install from File Installing icon themes... Jonathan Riddell Main Toolbar NAME OF TRANSLATORSYour names Name No Effect Panel Preview Remove Theme Remove the selected theme from your disk Set Effect... Setup Active Icon Effect Setup Default Icon Effect Setup Disabled Icon Effect Size: Small Icons The file is not a valid icon theme archive. This will remove the selected theme from your disk. To Gray To Monochrome Toolbar Torsten Rahn Unable to create a temporary file. Unable to download the icon theme archive;
please check that address %1 is correct. Unable to find the icon theme archive %1. Use of Icon You need to be connected to the Internet to use this action. A dialog will display a list of themes from the http://www.kde.org website. Clicking the Install button associated with a theme will install this theme locally. Project-Id-Version: kcmicons
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-08 06:27+0100
PO-Revision-Date: 2017-10-27 20:53+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 &Úroveň: &Efekt: &Druhá farba: &Polopriesvitné &Téma (c) 2000-2003 Geert Jansen <h1>Ikony</h1>Tento modul umožňuje vybrať ikony pre vaše prostredie.<p>Tému ikon vyberiete kliknutím na jej meno a stlačením tlačidla "Použiť". Ak zmeny použiť nechcete, môžete stlačiť "Vrátiť pôvodné" a tým zahodiť všetky zmeny.</p><p>Stlačením "Nainštalovať novú tému" je možné pridať novú tému zadaním jej umiestnenia. Potom stlačte "OK" pre dokončenie inštalácie</p><p>Tlačidlo "Odstrániť tému" je aktívne iba ak označíte tému, ktorú ste nainštalovali pomocou tohto modulu. Nie je možné odstrániť globálne témy.</p><p>Môžete zadať aj efekty, ktoré sa majú na ikony použiť.</p> <qt>Naozaj chcete odstrániť tému ikon <strong>%1</strong>?<br /><br /> Tým odstránite aj súbory nainštalované touto témou.</qt> <qt>Inštalujem tému <strong>%1</strong></qt> Aktívna Zakázaná Štandardná Počas inštalácie nastal problém, avšak väčšina tém z archívu bola nainštalovaná &Pokročilé Všetky ikony Antonio Larrosa Jimenez &Farba: Zafarbiť Potvrdenie Odfarbiť Popis Plocha Dialógy Zadajte URL témy orpheus@hq.alert.sk,mirodav@gmail.com Parametre efektu Zjasniť Geert Jansen Získať nové témy... Získať nové témy z Internetu Ikony Modul ovládacieho centra pre ikony Ak už máte lokálny súbor s témou, toto tlačidlo ho rozbalí a umožní používať tému KDE aplikáciám. Nainštalovať tému z lokálneho súboru Nainštalovať zo súboru Inštalujem témy ikon... Jonathan Riddell Hlavný panel nástrojov Pavol Cvengroš,Miroslav Dávid Meno Bez efektu Panel Náhľad Odstrániť tému Odstráni vybranú tému z vášho disku. Nastaviť efekt... Nastavenie efektu pre aktívnu ikonu Nastavenie efektu pre štandardnú ikonu Nastavenie efektu pre zakázanú ikonu Veľkosť: Malé ikony Súbor nie je platný archív tém ikon. Toto odstráni vybranú tému z vášho disku. Zošednúť Čierno-biele Panel nástrojov Torsten Rahn Nemôžem vytvoriť dočasný súbor. Nepodarilo sa stiahnuť archív témy;
prosím overte, že adresa %1 je správna. Nepodarilo sa nájsť archív tém %1. Použitie ikony Pre použitie tejto akcie musíte byť pripojení k Internetu. Dialógové okno zobrazí zoznam tém z webstránky http://www.kde.org. Kliknutím na tlačidlo Nainštalovať sa vybraná téma nainštaluje. 