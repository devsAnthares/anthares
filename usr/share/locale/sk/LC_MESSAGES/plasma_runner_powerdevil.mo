��          �      L      �     �     �  �   �  T   }  )   �  (   �  0   %  $   V  &   {  &   �  %   �  ?   �  F   /     v     �     �     �     �  �  �     �     �  �   �  L   n     �  
   �     �     �     �     �     �               )     =     L     Z     o                                              
                    	                                  Dim screen by half Dim screen totally Lists screen brightness options or sets it to the brightness defined by :q:; e.g. screen brightness 50 would dim the screen to 50% maximum brightness Lists system suspend (e.g. sleep, hibernate) options and allows them to be activated Note this is a KRunner keyworddim screen Note this is a KRunner keywordhibernate Note this is a KRunner keywordscreen brightness Note this is a KRunner keywordsleep Note this is a KRunner keywordsuspend Note this is a KRunner keywordto disk Note this is a KRunner keywordto ram Note this is a KRunner keyword; %1 is a parameterdim screen %1 Note this is a KRunner keyword; %1 is a parameterscreen brightness %1 Set Brightness to %1 Suspend to Disk Suspend to RAM Suspends the system to RAM Suspends the system to disk Project-Id-Version: plasma_runner_powerdevil
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-26 03:33+0200
PO-Revision-Date: 2010-01-27 20:53+0100
Last-Translator: Michal Sulek <misurel@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 Stmavieť obrazovku na polovicu Stmavieť obrazovku úplne Zobrazí možnosti jasu obrazovky alebo nastaví jas definovaný :q:; napr. jas obrazovky 50 stmavie obrazovku na 50% maximálneho jasu Zobrazí možnosti uspania (napr. do RAM, na disk) a umožní ich aktivovať stmaviet obrazovku hibernovat jas obrazovky uspat uspat na disk do ram stmaviet obrazovku %1 jas obrazovky %1 Nastaviť jas na %1 Uspať na disk Uspať do RAM Uspí systém do RAM Uspí systém na disk 