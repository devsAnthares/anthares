��    B      ,  Y   <      �  i   �  m        y  b   �     �     	  6        O  7   _     �     �  	   �     �  (   �  )   �  )   (     R     o  Y   u     �     �  �   �      y	  .   �	     �	  
   �	     �	     �	     
     
     !
     5
     B
     J
     c
     r
     w
     �
     �
     �
     �
     �
  	   �
  
   �
     �
     �
  	     
     
   *     5     B  	   `     j     o  
   �  �   �     (  8   8  �   q     �  8   	  ,   B  ,   o  %   �     �  �  �  E   �  k   �      S  d   t     �     �  @        H  ;   Z     �     �     �     �  4   �  /     /   8  !   h     �  b   �     �       |   $  %   �  @   �               )     B     Q     j     s     �  	   �     �     �     �     �     �               +     2     C     Q     _      p     �     �     �     �  &   �  	   �  
             ,  �   :     �  G   �  �   7     �  ;   �  3     3   L  !   �     �     $   !   %          ;   ,          B                 8   #                 7   9       +              )   ?   -   <         6                                    *   2       >                1       @       .   5   3   :   A         &      /                4              0      "             =   (   '         	   
           @info User changed Phonon backendTo apply the backend change you will have to log out and back in again. A list of Phonon Backends found on your system.  The order here determines the order Phonon will use them in. Apply Device List To... Apply the currently shown device preference list to the following other audio playback categories: Audio Hardware Setup Audio Playback Audio Playback Device Preference for the '%1' Category Audio Recording Audio Recording Device Preference for the '%1' Category Backend Colin Guthrie Connector Copyright 2006 Matthias Kretz Default Audio Playback Device Preference Default Audio Recording Device Preference Default Video Recording Device Preference Default/Unspecified Category Defer Defines the default ordering of devices which can be overridden by individual categories. Device Configuration Device Preference Devices found on your system, suitable for the selected category.  Choose the device that you wish to be used by the applications. EMAIL OF TRANSLATORSYour emails Failed to set the selected audio output device Front Center Front Left Front Left of Center Front Right Front Right of Center Hardware Independent Devices Input Levels Invalid KDE Audio Hardware Setup Matthias Kretz Mono NAME OF TRANSLATORSYour names Phonon Configuration Module Playback (%1) Prefer Profile Rear Center Rear Left Rear Right Recording (%1) Show advanced devices Side Left Side Right Sound Card Sound Device Speaker Placement and Testing Subwoofer Test Test the selected device Testing %1 The order determines the preference of the devices. If for some reason the first device cannot be used Phonon will try to use the second, and so on. Unknown Channel Use the currently shown device list for more categories. Various categories of media use cases.  For each category, you may choose what device you prefer to be used by the Phonon applications. Video Recording Video Recording Device Preference for the '%1' Category  Your backend may not support audio recording Your backend may not support video recording no preference for the selected device prefer the selected device Project-Id-Version: kcm_phonon
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2012-03-21 21:26+0100
Last-Translator: Richard Frič <Richard.Fric@kdemail.net>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 Na použitie zmeny backendu sa musíte odhlásiť a zase prihlásiť. Zoznam nájdených backendov Phononu v systéme.  Toto poradie určuje poradie, v akom ich Phonon použije. Použiť zoznam zariadení na... Aplikovať aktuálne zobrazený zoznam poradia zariadení na nasledujúce kategórie výstupu zvuku: Nastavenie audio hardvéru Prehrávanie audia Preferencia zariadenia na prehrávanie zvuku pre kategóriu '%1' Nahrávanie audia Preferencia zariadenia na záznam zvuku pre kategóriu '%1' Backend Colin Guthrie Konektor Copyright 2006 Matthias Kretz Predvolené poradie zariadenia na prehrávanie zvuku Predvolené poradie zariadenia na záznam zvuku Predvolené poradie zariadenia na záznam videa Štandardná/nezadaná kategória Nepreferovať Definuje štandardné poradie zariadení, ktoré je možné zmeniť v jednotlivých kategóriách. Nastavenie zariadenia Poradie zariadení Zariadenia nájdené v systéme, vhodné pre vybranú kategódiu. Vyberte zariadenie, ktoré chcete použiť pre aplikácie. misurel@gmail.com,wizzardsk@gmail.com Zlyhalo nastavenie zvoleného zvukového výstupného zariadenia Predný stredový Predný ľavý Predný ľavý stredový Predný pravý Predný pravý stredový Hardvér Nezávislé zariadenia Vstupné úrovne Neplatné Nastavenie audio hardvéru KDE Matthias Kretz Mono Michal Šulek,Roman Paholík Ovládací modul Phonon Prehrávanie (%1) Preferovať Profil Zadný stredový Zadný ľavý Zadný pravý Nahrávanie (%1) Zobraziť pokročilé zariadenia Bočný ľavý Bočný pravý Zvuková karta Zvukové zariadenie Umiestnenie a testovanie reproduktorov Subwoofer Otestovať Testovať vybrané zariadenie Testovanie %1 Toto poradie určuje prioritu výstupných zariadení. Ak nemôže byť z akéhokoľvek dôvodu použité prvé zariadenie, Phonon sa pokúsi použiť druhé, atď. Neznámy kanál Aplikovať aktuálne zobrazený zoznam zariadení pre viac kategórií. Rôzne kategórie prípadov médií. Pre každú kategóriu môžete vybrať, ktoré zariadenie preferujete pre použitie v aplikáciách Phonon. Prehrávanie videa Preferencia zariadenia na záznam videa pre kategóriu '%1' Váš backend nemusí podporovať nahrávanie audia Váš backend nemusí podporovať nahrávanie videa nepreferovať vybrané zariadenie preferovať vybrané zariadenie 