��          �   %   �      p     q     ~     �     �     �     �     �     �  )   �               !     4     I     ]     m  3   u     �     �  <   �  5     8   T  8   �     �     �     �     �  �  �     �     �     �     	     $     7     G     ]  <   i  	   �     �     �     �     �       
   #  +   .     Z      z  9   �  A   �  :   	  :   R	  	   �	     �	     �	     �	                        
                                                	                                                   Add files... Add folder... Background frame Change picture every Choose a folder Choose files Configure plasmoid... General Left click image opens in external viewer Pad Paths Pause on mouseover Preserve aspect crop Preserve aspect fit Randomize items Stretch The image is duplicated horizontally and vertically The image is not transformed The image is scaled to fit The image is scaled uniformly to fill, cropping if necessary The image is scaled uniformly to fit without cropping The image is stretched horizontally and tiled vertically The image is stretched vertically and tiled horizontally Tile Tile horizontally Tile vertically s Project-Id-Version: plasma_applet_org.kde.plasma.mediacontroller
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-16 03:31+0100
PO-Revision-Date: 2016-04-09 13:41+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 Pridať súbory... Pridať priečinok... Snímka pozadia Zmeniť obrázok každých Vybrať priečinok Zvoľte súbory Nastaviť plazmoid... Všeobecné Kliknutie ľavým tlačidlom otvorí v externom prehliadači Podložka Cesty Pozastaviť pri prechode myši Zachovať orezanie pomeru Zachovať prispôsobenie pomeru Zamiešať položky Natiahnuť Obrázok je duplikovaný vodorovne a zvislo Obrázok nie je trasnformovaný Tento obrázok je prispôsobený Tento obrázok je zmenený uniformne, oreže sa, ak treba Tento obrázok je zmenený uniformne, aby sa zmestil bez orezania Obrázok je roztiahnutý vodorovne a dlaždicovaný zvislo Obrázok je roztiahnutý zvislo a dlaždicovaný vodorovne Dlaždice Dlaždice vodorovne Dlaždice zvislo s 