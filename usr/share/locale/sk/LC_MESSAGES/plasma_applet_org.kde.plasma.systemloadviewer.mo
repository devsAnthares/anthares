��    &      L  5   |      P     Q     l     y     �     �     �     �     �     �     �     �     �  &   �               #     ,     3     ?     M     U     ]     d     s     �     �     �     �     �     �     �     �     �     �  
   �            �       �     �     
     "     *     3     7     >     J     ^     g     t  *   �     �     �     �     �     �                     (     0     @     S     a     g  
   ~     �     �     �     �     �     �     	     /	     <	                           	      #      $                                                    &                                          !          %                            "   
    Abbreviation for secondss Application: Average clock: %1 MHz Bar Buffers: CPU CPU %1 CPU monitor CPU%1: %2% @ %3 Mhz CPU: %1% CPUs separately Cache Cache Dirty, Writeback: %1 MiB, %2 MiB Cache monitor Cached: Circular Colors Compact Bar Dirty memory: General IOWait: Memory Memory monitor Memory: %1/%2 MiB Monitor type: Nice: Set colors manually Show: Swap Swap monitor Swap: %1/%2 MiB Sys: System load Update interval: Used swap: User: Writeback memory: Project-Id-Version: plasma_applet_org.kde.plasma.systemloadviewer
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-29 03:31+0200
PO-Revision-Date: 2017-05-08 17:11+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 s Aplikácia: Priemerný takt: %1 MHz Stĺpec Buffery: CPU CPU %1 Monitor CPU CPU%1: %2% @ %3 Mhz CPU: %1% CPU oddelene Vyrovnávacia pamäť Špinavá cache, Writeback: %1 MiB, %2 MiB Monitor vyrovnávacej pamäte Vyrovnávacia: Kruhovo Farby Kompaktný panel Špinavá pamäť: Všeobecné IOWait: Pamäť Monitor pamäte Pamäť: %1/%2 MiB Typ monitora: Nice: Nastaviť farby ručne Zobraziť: Odkladací priestor Monitor odkladacieho priestoru Odkladací priestor: %1/%2 MiB Sys: Systémové zaťaženie Interval aktualizácie: Použitý odkladací priestor: Užívateľ: Writeback pamäť: 