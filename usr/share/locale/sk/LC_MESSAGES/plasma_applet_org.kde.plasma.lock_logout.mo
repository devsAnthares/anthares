��          �      L      �  &   �  +   �       @     	   ]     g     �     �     �     �  (   �     �     �  ,   �               +     7  �  ;       $   .     S     _     e     q     �     �  	   �     �  3   �     �     �  0        <     C     Z     q        
                          	                                                                  Do you want to suspend to RAM (sleep)? Do you want to suspend to disk (hibernate)? General Heading for list of actions (leave, lock, shutdown, ...)Actions Hibernate Hibernate (suspend to disk) Leave Leave... Lock Lock the screen Logout, turn off or restart the computer No Sleep (suspend to RAM) Start a parallel session as a different user Suspend Switch User Switch user Yes Project-Id-Version: plasma_applet_org.kde.plasma.lock_logout
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2015-07-13 13:45+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 Chcete uspať do RAM? Chcete uspať na disk (hibernovať)? Všeobecné Akcie Hibernovať Uspať (uspať na disk) Opustiť Opustiť... Zamknúť Zamknúť obrazovku Odhlásiť, vypnúť alebo reštartovať počítač Nie Uspať (uspať do RAM) Spustiť súbežné sedenie ako iný užívateľ Uspať Prepnúť užívateľa Prepnúť užívateľa Áno 