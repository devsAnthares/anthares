��            )         �     �  "   �     �  !   �  !        4  
   J     U     p     �  !   �     �  0   �  8        ?  !   ]          �     �     �     �                '  
   /  
   :  
   E  &   P     w     �  �  �     `  )   d  #   �  &   �  +   �       	     $   #     H      Z  /   {     �  5   �  M    	  )   N	  -   x	  %   �	  '   �	     �	  1   
     E
  1   X
     �
     �
     �
     �
  
   �
  3   �
     �
                                                                                                      	                       
                 ms &Keyboard accelerators visibility: &Top arrow button type: Always Hide Keyboard Accelerators Always Show Keyboard Accelerators Anima&tions duration: Animations Bottom arrow button t&ype: Breeze Settings Center tabbar tabs Drag windows from all empty areas Drag windows from titlebar only Drag windows from titlebar, menubar and toolbars Draw a thin line to indicate focus in menus and menubars Draw focus indicator in lists Draw frame around dockable panels Draw frame around page titles Draw frame around side panels Draw slider tick marks Draw toolbar item separators Enable animations Enable extended resize handles Frames General No Buttons One Button Scrollbars Show Keyboard Accelerators When Needed Two Buttons W&indows' drag mode: Project-Id-Version: breeze_style_config
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:42+0200
PO-Revision-Date: 2017-10-27 20:56+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
  ms Viditeľnosť akcelerátorov klávesnice: Typ horného tlačidla so šípkou: Vždy skryť akcelerátory klávesnice Vždy zobrazovať akcelerátory klávesnice Trvanie animácií: Animácie Typ spodného tlačidla so šípkou: Nastavenia Vánok Vycentrovať karty panelu kariet Presúvať okná zo všetkých prázdnych miest Presúvať okná iba z titulku Presúvať okná z titulku, menu a panelov nástrojov Nakresliť tenkú čiaru na indikáciu zamerania v ponukách a pruhoch ponúk Kresliť indikátor zamerania v zoznamoch Kresliť rámce okolo dokovateľných panelov Kresliť rámce okolo tituliek strany Kresliť rámce okolo bočných panelov Kresliť zarážky posuvníkov Kresliť oddeľovač položiek panelov nástrojov Povoliť animácie Povoliť rozšírené spracovanie zmeny veľkosti Rámce Všeobecné Žiadne tlačidlá Jedno tlačidlo Posuvníky Ukázať akcelerátory klávesnice, ak je potrebné Dve tlačidlá Režim ťahania okien: 