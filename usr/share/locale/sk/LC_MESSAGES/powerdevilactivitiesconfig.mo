��          �      |      �     �     �     �                 ;  	   \     f     �  &   �     �     �     �  	          �   %  �   �  t   ?  7   �  +   �       �       �     �       !   	  !   +     M     a     m     |  -   �     �     �  &   �           )  �   0  �   �  d   :	     �	  :   �	     �	                                      
   	                                                         min Act like Always Define a special behavior Don't use special settings EMAIL OF TRANSLATORSYour emails Hibernate NAME OF TRANSLATORSYour names Never shutdown the screen Never suspend or shutdown the computer PC running on AC power PC running on battery power PC running on low battery Shut down Suspend The Power Management Service appears not to be running.
This can be solved by starting or scheduling it inside "Startup and Shutdown" The activity service is not running.
It is necessary to have the activity manager running to configure activity-specific power management behavior. The activity service is running with bare functionalities.
Names and icons of the activities might not be available. This is meant to be: Act like activity %1Activity "%1" Use separate settings (advanced users only) after Project-Id-Version: powerdevilactivitiesconfig
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-09 03:03+0200
PO-Revision-Date: 2016-06-07 22:10+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
  min Správať sa ako Vždy Definovať špeciálne správanie Nepoužiť špeciálne nastavenia wizzardsk@gmail.com Hibernovať Roman Paholík Nikdy nevypnúť obrazovku Nikdy nezastaviť alebo nevypnúť počítač PC beží napájané zo siete PC beží na batériu PC beží pri nízkej úrovni batérie Vypnúť Uspať Zdá sa, že služba správy napájania nebeží.
Toto môžte vyriešiť jej spustením alebo naplánovnaím v "Spustenie a vypnutie" Služba aktivít nebeží.
Je potrebné, aby správca aktivít bežal na nastavenie správania správy napájania podľa aktivít. Služba aktivít beží s obmedzenou funkcionalitou.
Názvy a ikony aktivít nemusia byť dostupné. Aktivita "%1" Použiť oddelené nastavenia (pokročilí používatelia) po 