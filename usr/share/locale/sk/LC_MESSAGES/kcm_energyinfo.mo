��    7      �  I   �      �     �     �     �     �     �     �               $     -     5     H     T      `     �     �     �     �     �     �     �     �                     )     7  	   C  	   M     W     d     j     �     �     �     �     �     �     �     �     �     �  @   �     7     @     \     c     j     r     �     �     �  4   �     �  �  �     �	     �	     �	     �	     �	     �	     �	     
  
   
  	   
     (
     ,
  
   8
     C
     W
     _
     p
  
   �
     �
     �
     �
     �
     �
     �
                ,     ?     O     `     i     o     ~     �     �  	   �     �     �     �     �     �     �  E   �     #  $   3     X     d     f     o     �     �     �     �     �     &   0      7                  #                
          	   '   (   +                       6              3      1                   ,   2                  "       *          $      4   )               %      5                     !          /   .   -       % %1 is value, %2 is unit%1 %2 %1: Application Energy Consumption Battery Capacity Charge Percentage Charge state Charging Current Degree Celsius°C Details: %1 Discharging EMAIL OF TRANSLATORSYour emails Energy Energy Consumption Energy Consumption Statistics Environment Full design Fully charged Has power supply Kai Uwe Broulik Last 12 hours Last 2 hours Last 24 hours Last 48 hours Last 7 days Last full Last hour Manufacturer Model NAME OF TRANSLATORSYour names No Not charging PID: %1 Path: %1 Rechargeable Refresh Serial Number Shorthand for WattsW System Temperature This type of history is currently not available for this device. Timespan Timespan of data to display Vendor VoltV Voltage Wakeups per second: %1 (%2%) WattW Watt-hoursWh Yes current power draw from the battery in WConsumption literal percent sign% Project-Id-Version: kcm_energyinfo
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2016-06-20 20:31+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 % %1 %2 %1: Aplikácia pre spotrebu energie Batéria Kapacita Percento nabitia Stav nabitia Nabíja sa Aktuálne °C Detaily: %1 Vybíja sa wizzardsk@gmail.com Energia Spotreba energie Štatistiky spotreby energie Prostredie Plný dizajn Plne nabitá Má napájací zdroj Kai Uwe Broulik Posledných 12 hodín Posledné 2 hodiny Posledných 24 hodín Posledných 48 hodín Posledných 7 dní Posledné plné Posledná hodina Výrobca Model Roman Paholík Nie Nenabíja sa PID: %1 Cesta: %1 Dobíjateľná Obnoviť Sériové číslo W Systém Teplota Tento typ histórie momentálne nie je dostupný pre toto zariadenie. Časový rozsah Časový rozsah zobrazených údajov Dodávateľ V Napätie Prebudení za sekundu: %1 (%2%) W Wh Áno Spotreba % 