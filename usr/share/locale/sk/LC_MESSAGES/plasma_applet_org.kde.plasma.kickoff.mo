��            )   �      �     �  
   �  /   �  -   �          !  
   2     =     J     `  W   i     �  ,   �  	                  !     '     -  
   :     E     W     o     �     �     �     �  1   �       �       �  
   �  
                  6     I  
   Q     \  
   w  c   �     �  3   �     1     =  	   M     W     ^     g     y     �     �     �     �  $   �     	  #   .	     R	     j	                                                                                  
                                	                       %1@%2 %2@%3 (%1) @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Favorites All Applications Appearance Applications Applications updated. Computer Drag tabs between the boxes to show/hide them, or reorder the visible tabs by dragging. Edit Applications... Expand search to bookmarks, files and emails Favorites Hidden Tabs History Icon: Leave Menu Buttons Often Used On All Activities On The Current Activity Remove from Favorites Show In Favorites Show applications by name Sort alphabetically Switch tabs on hover Type is a verb here, not a nounType to search... Visible Tabs Project-Id-Version: plasma_applet_org.kde.plasma.kickoff
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2017-10-27 21:03+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
 %1@%2 %2@%3 (%1) Vybrať... Vymazať ikonu Pridať do obľúbených Všetky aplikácie Vzhľad Aplikácie Aplikácie aktualizované. Počítač Presuňte karty medzi schránkami na zobrazenie a skrytie alebo zmenu poradia viditeľných kariet. Upraviť aplikácie... Rozbaliť hľadanie na záložky, súbory a e-maily Obľúbené Skryté značky História Ikona: Opustiť Tlačidlá ponuky Často použité Na všetkých aktivitách Na aktuálnej aktivite Odstrániť z obľúbených Zobraziť v obľúbených Zobrazovať aplikácie podľa názvu Zoradiť podľa abecedy Prepnúť karty pri prechode myšou Píšte na hľadanie... Viditeľné značky 