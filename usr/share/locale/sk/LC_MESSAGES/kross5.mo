��    $      <  5   \      0     1  +   E     q  4   �  &   �     �     �                     5     :     P     X  ,   u  3   �     �     �               !  '   .     V     u     {     �     �     �     �     �     �  &   �       B        b  �  y     7     =     \     l     �     �  
   �  	   �  
   �  `   �     $	     -	     G	     S	  /   m	  5   �	  %   �	  &   �	      
     (
     /
  (   <
  M   e
     �
     �
     �
     �
     �
          !  	   A  (   K     t  2   z     �                                       
                              !                                  $      #                               	                           "             @info:creditAuthor @info:creditCopyright 2006 Sebastian Sauer @info:creditSebastian Sauer @info:shell command-line argumentThe script to run. @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: application descriptionCommand-line utility to run Kross scripts. application nameKross Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2014-04-02 21:20+0200
Last-Translator: Roman Paholík <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 Autor Copyright 2006 Sebastian Sauer Sebastian Sauer Skript na spustenie. Všeobecné Pridať nový skript. Pridať... Zrušiť? Komentár: visnovsky@kde.org,Richard.Fric@kdemail.net,miguel@portugal.sk,vatrtj@gmail.com,misurel@gmail.com Upraviť Upraviť vybraný skript. Upraviť... Spustiť vybraný skript. Zlyhalo vytvorenie skriptu pre interpreter "%1" Zlyhalo určenie interpretera pre súbor skriptu "%1" Zlyhalo načítanie interpretera "%1" Zlyhalo otvorenie súboru skriptu "%1" Súbor: Ikona: Interpreter: Úroveň zabezpečenia interpretera Ruby Stanislav Višňovský,Richard Frič,Michal Gašpar,Jakub Vatrt,Michal Šulek Názov: Neexistujúca funkcia "%1" Žiadny interpreter '%1' Odstrániť Odstrániť vybraný skript. Spustiť Súbor skriptu "%1" neexistuje. Zastaviť Pozastaviť vykonanie vybraného skript. Text: Konzolová aplikácia na spustenie Kross skriptov. Kross 