��          |      �          %   !     G     U     c     u     �     �  
   �     �     �  $   �  �  �  *   �     �     �          %     @     L     c     q       #   �     
   	                                                   Automatically copy color to clipboard Clear History Color Options Copy to Clipboard Default color format: General Open Color Dialog Pick Color Pick a color Show history When pressing the keyboard shortcut: Project-Id-Version: plasma_applet_kolourpicker
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-12 03:03+0200
PO-Revision-Date: 2016-12-19 22:11+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 Automaticky kopírovať farbu do schránky Vyčistiť históriu Voľby farieb Kopírovať do schránky Predvolený formát farby: Všeobecné Otvoriť dialóg farby Vybrať farbu Zvoľte farbu Zobraziť históriu Pri stlačení klávesovej skratky: 