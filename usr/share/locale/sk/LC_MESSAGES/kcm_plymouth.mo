��          �   %   �      @     A  !   Y      {     �      �     �     �  +        =     N     [  (   z     �  ,   �  7   �     !  7   :     r  ]   �  1   �  	   "     ,  "   ?  5   b  �  �     Y  '   w  %   �  "   �     �  $   �  !   !  '   C     k     �     �  *   �     �  0   �  A   	     V	  9   k	     �	  m   �	  ,   3
     `
     o
  +   �
  1   �
                    
         	                                                                                                 Cannot start initramfs. Cannot start update-alternatives. Configure Plymouth Splash Screen Download New Splash Screens EMAIL OF TRANSLATORSYour emails Get New Boot Splash Screens... Initramfs failed to run. Initramfs returned with error condition %1. Install a theme. Marco Martin NAME OF TRANSLATORSYour names No theme specified in helper parameters. Plymouth theme installer Select a global splash screen for the system The theme to install, must be an existing archive file. Theme %1 does not exist. Theme corrupted: .plymouth file not found inside theme. Theme folder %1 does not exist. This module lets you configure the look of the whole workspace with some ready to go presets. Unable to authenticate/execute the action: %1, %2 Uninstall Uninstall a theme. update-alternatives failed to run. update-alternatives returned with error condition %1. Project-Id-Version: kcm_plymouth
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-28 03:05+0200
PO-Revision-Date: 2017-02-15 19:38+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 Nemôžem spustiť initramfs. Nemôžem spustiť update-alternatives. Nastaviť úvodnú obrazovku Plymouth Stiahnuť nové úvodné obrazovky wizzardsk@gmail.com Získať nové úvodné obrazovky... Initramfs sa nepodarilo spustiť. Initramfs vrátil chybu v podmienke %1. Nainštalovať tému. Marco Martin Roman Paholík Neurčená téma v pomocných parametroch. Inštalátor tém Plymouth Vyberte globálnu úvodnú obrazovku pre systém Téma na nainštalovanie, musí byť existujúci súbor archívu. Téma %1 neexistuje. Téma poškodená: súbor .plymouth sa nenašiel v téme. Priečinok témy %1 neexistuje. Tento modul vám umožní nastaviť vzhľad celej pracovnej plochy s niekoľkými pripravenými predvoľbami. Nepodarilo sa overiť/spustiť akciu: %1, %2 Odinštalovať Odinštalovať tému. update-alternatives sa nepodarilo spustiť. update-alternatives vrátil chybu v podmienke %1. 