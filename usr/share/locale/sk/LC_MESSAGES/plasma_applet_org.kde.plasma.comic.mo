��    =        S   �      8  ;   9     u     �     �     �  6   �  A   
     L     U  $   Y  
   ~     �  #   �     �     �     �     �     �  7        >     [     w  '   �     �     �  $   �  ,   �          3     C     K     ]     y     �     �     �     �     �  �   �  9   �	  "   �	     �	     �	     
     *
     <
     R
     c
  %   u
     �
     �
     �
     �
     �
  
   �
  7        :     T     l     t  �  �  p   M  "   �     �     �  !        .     C     `     l  $   t     �     �  *   �     �  &   �     &     -     <  2   I  "   |  "   �     �  ,   �     �       %     1   @     r     �     �     �     �     �     �     �          1     I  �   X  T   
  '   _     �  *   �     �     �      �     
     #  +   :     f  %   t  &   �     �     �  
   �     �     �     �     �          !          3      0      (                 1      "          =   .   6                                         	      :          5       ,   #   &   7       ;   '             -   
             8                 *           %           <   4   9                )   /   $          2             +       

Choose the previous strip to go to the last cached strip. &Create Comic Book Archive... &Save Comic As... &Strip Number: *.cbz|Comic Book Archive (Zip) @option:check Context menu of comic image&Actual Size @option:check Context menu of comic imageStore current &Position Advanced All An error happened for identifier %1. Appearance Archiving comic failed Automatically update comic plugins: Cache Check for new comic strips: Comic Comic cache: Configure... Could not create the archive at the specified location. Create %1 Comic Book Archive Creating Comic Book Archive Destination: Display error when getting comic failed Download Comics Error Handling Failed adding a file to the archive. Failed creating the file with identifier %1. From beginning to ... From end to ... General Get New Comics... Getting comic strip failed: Go to Strip Information Jump to &current Strip Jump to &first Strip Jump to Strip ... Manual range Maybe there is no Internet connection.
Maybe the comic plugin is broken.
Another reason might be that there is no comic for this day/number/string, so choosing a different one might work. Middle-click on the comic to show it at its original size No zip file is existing, aborting. Range: Show arrows only on mouse over Show comic URL Show comic author Show comic identifier Show comic title Strip identifier: The range of comic strips to archive. Update Visit the comic website Visit the shop &website an abbreviation for Number# %1 days dd.MM.yyyy here strip means comic strip&Next Tab with a new Strip in a range: from toFrom: in a range: from toTo: minutes strips per comic Project-Id-Version: plasma_applet_comic
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-07-18 03:20+0200
PO-Revision-Date: 2015-03-06 20:02+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 

Vyberte predchádzajúci prúžok, ak chcete prejsť na posledný prúžok uložený vo vyrovnávacej pamäti. Vytvoriť archív knihy komiksu... &Uložiť komiks ako... Číslo &prúžku: *.cbz|Archív knihy komiksu (Zip) &Aktuálna veľkosť Uložiť aktuálnu &pozíciu Pokročilé Všetko Pre identifikátor %1 nastala chyba. Vzhľad Archivovanie komiksu zlyhalo Automaticky aktualizovať pluginy komiksu: Vyrovnávacia pamäť Skontrolovať nové komiksové stripy: Komiks Cache komiksu: Nastaviť... Nemôžem vytvoriť archív v zadanom umiestnení. Vytvoriť %1 archív knihy komiksu Vytváranie archívu knihy komiksu Cieľ: Zobraziť chybu ak zlyhá stiahnutie komiksu Stiahnuť komiksy Spracovanie chýb Zlyhalo pridanie súboru do archívu. Zlyhalo vytvorenie súboru s identifikátorom %1. Od začiatku do... Od konca do... Všeobecné Získať nové komiksy... Získanie komiksu zlyhalo: Prejsť na prúžok Informácie Skočiť na &aktuálny prúžok Skočiť na &prvý prúžok Skočiť na prúžok... Ručný rozsah Nie ste asi pripojený do Internetu.
Modul komiksov je asi poškodený.
Ďalší dôvod môže byť, že neexistuje komiks pre tento deň/číslo/reťazec, skúste vybrať iný. Kliknutie stredným tlačidlom myši na komiks zobrazí komiks v pôvodnej veľkosti Neexistuje žiadny zip súbor, ruším. Rozsah: Zobraziť šípky len pri prejdení myšou Zobraziť URL komiksu Zobraziť autora komiksu Zobraziť identifikátor komiksu Zobraziť názov komiksu Identifikátor stripu: Rozsah komiksových stripov na archiváciu. Aktualizovať Navštíviť webovú stránku komiksu Navštíviť &webovú stránku obchodu # %1 dní dd.MM.yyyy Nová karta s novým stripom Od: Do: minút stripov na komiks 