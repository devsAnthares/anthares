��          �      L      �     �     �     �     �          )     1     9     P     h     t  K   �     �     �     �     �            �        �          .      >     _     u     }     �  $   �     �     �     �     �     �     �          (     1                                                  
   	                                               Change the barcode type Clear history Clipboard Contents Clipboard history is empty. Clipboard is empty Code 39 Code 93 Configure Clipboard... Creating barcode failed Data Matrix Edit contents Indicator that there are more urls in the clipboard than previews shown+%1 Invoke action QR Code Remove from history Return to Clipboard Search Show barcode Project-Id-Version: plasma_applet_org.kde.plasma.clipboard
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-10 03:07+0200
PO-Revision-Date: 2015-02-17 21:58+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 Zmeniť typ čiarového kódu Vyčistiť históriu Obsah schránky História schránky je prázdna. Schránka je prázdna Code 39 Code 93 Nastaviť schránku... Vytvorenie čiarového kódu zlyhalo Dátová matica Upraviť obsah +%1 Vyvolať akciu QR kód Odstrániť z knižnice Návrat do schránky Hľadať Zobraziť čiarový kód 