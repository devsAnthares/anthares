��          �      <      �     �     �     �  7  �  �  #  +   �  ^              �     �  x   �  �   %     �               ;     U  �  r     /	     L	     f	  3  {	  �  �
  #   �  p   �           5     V  �   b  �   �  "   �     �  "   
     -     I                          
       	                                                                    &End current session &Restart computer &Turn off computer <h1>Session Manager</h1> You can configure the session manager here. This includes options such as whether or not the session exit (logout) should be confirmed, whether the session should be restored again when logging in and whether the computer should be automatically shut down after session exit by default. <ul>
<li><b>Restore previous session:</b> Will save all applications running on exit and restore them when they next start up</li>
<li><b>Restore manually saved session: </b> Allows the session to be saved at any time via "Save Session" in the K-Menu. This means the currently started applications will reappear when they next start up.</li>
<li><b>Start with an empty session:</b> Do not save anything. Will come up with an empty desktop on next start.</li>
</ul> Applications to be e&xcluded from sessions: Check this option if you want the session manager to display a logout confirmation dialog box. Conf&irm logout Default Leave Option General Here you can choose what should happen by default when you log out. This only has meaning, if you logged in through KDM. Here you can enter a colon or comma separated list of applications that should not be saved in sessions, and therefore will not be started when restoring a session. For example 'xterm:konsole' or 'xterm,konsole'. O&ffer shutdown options On Login Restore &manually saved session Restore &previous session Start with an empty &session Project-Id-Version: kcmsmserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2009-07-15 20:29+0200
Last-Translator: Michal Sulek <misurel@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 0.3
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 U&končiť aktuálne sedenie &Reštartovať počítač &Vypnúť počítač <h1>Správca sedenia</h1> Tento modul umožňuje konfigurovať správcu sedenia. Je možné nastaviť potvrdzovanie pri odhlásení a či sa má pri odhlásení uložiť sedenie a pri ďalšom prihlásení obnoviť. Navyše môžete určiť, či sa má po ukončení sedenia počítač automaticky vypnúť. <ul>
<li><b>Obnoviť predchádzajúce sedenie:</b> Uloží všetky bežiace aplikácie pri ukončení a obnoví ich pri ďalšom štarte</li>
<li><b>Obnoviť ručne uložené sedenie: </b> Umožňuje uložiť sedenie kedykoľvek pomocou voľby "Uložiť sedenie" v K-Menu. To znamená že aktuálne bežiace aplikácie budú obnovené pri ďalšom štarte.</li>
<li><b>Spustiť prázdne sedenie:</b> Nič neukladať. Spustí sa nové sedenie s prázdnou plochou.</li>
</ul> Aplikácie &vylúčené zo sedení: Povoľte túto možnosť ak chcete, aby správca sedenia zobrazoval dialógové okno pre potvrdenie odhlásenia. Potvrdiť &odhlásenie Štandardná možnosť opustenia Všeobecné Tu si môžete vybrať čo sa štandardne stane po odhlásení. Toto nastavenie má zmysel iba v prípade, že ste prihlásený pomocou KDM. Tu môžete zadať zoznam aplikácií (oddelených čiarkami alebo dvojbodkou), ktoré sa nemajú ukladať ako súčasť sedenia a preto nebudú znovu spustené pri obnove sedenia. Napríklad 'xterm:konsole' alebo 'xterm,konsole'. Po&núknuť možnosti pre vypnutie Pri prihlásení Obnoviť &ručne uložené sedenie Obnoviť &uložené sedenie Spustiť &prázdne sedenie 