��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  �  K       1     Y  B  [  �  V   �	     O
     a
     u
  $   �
     �
     �
  !   �
                                               
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2003-11-19 18:20+0100
Last-Translator: Stanislav Visnovsky <visnovsky@kde.org>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.3
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
  sek &Dĺžka upozornenia pri štarte (v sekundách):  <H1>Upozornenie v paneli úloh</H1>
Druhou metódou upozornenia na štart aplikácie je zobrazenie
tlačidla s rotujúcimi hodinami v paneli úloh pre znázornenie štartu.
Môže sa stať, že niektoré aplikácie nevedia o tomto upozorňovaní.
V tom prípade kurzor prestane blikať po čase zadanom v časti
'Dĺžka upozornenie pri štarte' <h1>Zaneprázdnený kurzor</h1>
KDE ponúka pre upozornenie na štart aplikácie zaneprázdnený kurzor.
Pre jeho povolenie použite jednu z volieb v rozbaľovacieho zoznamu.
Môže sa stať, že niektoré aplikácie nevedia o tomto upozorňovaní.
V tom prípade kurzor prestane blikať po čase zadanom v časti
'Dĺžka upozornenie pri štarte' <h1>Odozva na spúšťanie</h1>Tu je možné nastaviť reakciu pri štarte aplikácie. Blikajúci kurzor Poskakujúci kurzor Zaneprá&zdnený kurzor Povoliť upozornenie v paneli ú&loh Bez zaneprázdneného kurzoru Pasívny zaneprázdnený kurzor &Dĺžka upozornenia pri štarte: Upozor&nenie v paneli úloh 