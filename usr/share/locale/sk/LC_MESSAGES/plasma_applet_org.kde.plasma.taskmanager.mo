��    O      �  k         �     �     �     �     �     �     �     �  .   �  U   (     ~     �      �     �  /   �  /        5     A     J  
   V     a     q  $   �     �     �     �     �     �     �  	   	     	  
   	     )	     <	     O	     g	  	   m	     w	  !   �	     �	     �	  	   �	      �	     �	     �	     
     $
     5
     :
     G
     M
     _
     w
  (   �
     �
  =   �
            "   5     X     s  J   {  1   �  )   �  (   "  '   K  "   s  4   �     �     �     �     �     �          "  U   8  N   �  9   �  J     �  b     ?  	   O     Y     j     v     �  	   �     �     �     �     �     �     �  6     6   :     q  
   ~     �     �     �     �  $   �     
  
        #     *     H     T     m     {     �     �     �      �     �     �       .        <     X     s  *   �     �     �     �     �                     &     @  "   W  )   z     �     �     �     �     �  	               9   $  *   ^  2   �  +   �  $   �  <        J     h  
   n     y     �  	   �     �     �     �     �  #   �     %   N         @   A       >   +   "       ,       F   /   B   O           9      ;      $                      G          3          K         ?                        2   L   *      !      H   =                   <              '               :       -            .   I   J   6   C           M       1          #   0      4       7       (   	   
           8               D   5   )   &   E    &All Desktops &Close &Fullscreen &Move &New Desktop &Pin &Shade 1 = number of desktop, 2 = desktop name&%1 %2 Activities a window is currently on (apart from the current one)Also available on %1 Add To Current Activity All Activities Allow this program to be grouped Alphabetically Always arrange tasks in columns of as many rows Always arrange tasks in rows of as many columns Arrangement Behavior By Activity By Desktop By Program Name Close Window or Group Cycle through tasks with mouse wheel Do Not Group Do Not Sort Filters Forget Recent Documents General Grouping and Sorting Grouping: Highlight windows Icon size: Keep &Above Others Keep &Below Others Keep launchers separate Large Ma&ximize Manually Mark applications that play audio Maximum columns: Maximum rows: Mi&nimize Minimize/Restore Window or Group More Actions Move &To Current Desktop Move To &Activity Move To &Desktop Mute New Instance On %1 On All Activities On The Current Activity On middle-click: Only group when the task manager is full Open groups in popups Open or bring to the front window of media player appRestore Pause playbackPause Play next trackNext Track Play previous trackPrevious Track Quit media player appQuit Re&size Remove launcher button for application shown while it is not runningUnpin Show all user Places%1 more Place %1 more Places Show only tasks from the current activity Show only tasks from the current desktop Show only tasks from the current screen Show only tasks that are minimized Show progress and status information in task buttons Show tooltips Small Sorting: Start New Instance Start playbackPlay Stop playbackStop The click actionNone Toggle action for showing a launcher button while the application is not running&Pin When clicking it would toggle grouping windows of a specific appGroup/Ungroup Which activities a window is currently onAvailable on %1 Which virtual desktop a window is currently onAvailable on all activities Project-Id-Version: plasma_applet_org.kde.plasma.taskmanager
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2017-05-18 21:53+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 &Všetky plochy &Zavrieť &Celá obrazovka &Presunúť Nová plocha &Pin Za&baliť &%1 %2 Dostupné aj na %1 Pridať k aktuálnej aktivite Všetky aktivity Povoliť programu zoskupovanie Abecedne Vždy usporiadať úlohy v stĺpcoch s počtom riadkov Vždy usporiadať úlohy v riadkoch s počtom stĺpcov Usporiadanie Správanie Podľa aktivity Podľa pracovnej plochy Podľa názvu programu Zatvoriť okno alebo skupinu Cyklovať cez úlohy kolieskom myši Nezoskupovať Netriediť Filtre Zabudnúť nedávne dokumenty Všeobecné Zoskupovanie a triedenie Zoskupovanie: Zvýrazniť okná Veľkosť ikon: Držať na&d ostatnými Držať &pod ostatnými Ponechať spúšťače oddelené Veľké Ma&ximalizovať Ručne Označiť aplikácie, ktoré prehrávajú zvuk Maximálny počet stĺpcov: Maximálny počet riadkov: Mi&nimalizovať Minimalizovať/obnoviť okno alebo skupinu Viac akcií Presunúť na &aktuálnu plochu Presunúť na aktivitu Presunúť na &plochu Stlmiť Nová inštancia Na %1 Na všetkých aktivitách Na aktuálnej aktivite Pri kliknutí stredného tlačidla Zoskupiť iba, ak je správca úloh plný Otvoriť okná v popupoch Obnoviť Pozastaviť Nasledujúca skladba Predchádzajúca skladba Ukončiť Z&meniť veľkosť Odpin %1 ďalšie miesto %1 ďalšie miesta %1 ďalších miest Zobraziť iba úlohy z aktuálnej aktivity Zobraziť iba úlohy z aktuálnej pracovnej plochy Zobraziť iba úlohy z aktuálnej obrazovky Zobraziť iba minimalizované úlohy Zobraziť pokrok a stavovú informáciu v tlačidlách úloh Zobrazovať nástrojové tipy Malé Triedenie: Spustiť novú inštanciu Prehrať Zastaviť Žiadne &Pin Zoskupiť/Odzoskupiť Dostupné na %1 Dostupné na všetkých aktivitách 