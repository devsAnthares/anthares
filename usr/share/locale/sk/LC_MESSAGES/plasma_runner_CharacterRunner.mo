��    
      l      �       �      �            	               .  I   3     }  	   �  �  �     b     w     �     �     �     �  K   �     �  
                  
   	                           &Trigger word: Add Item Alias Alias: Character Runner Config Code Creates Characters from :q: if it is a hexadecimal code or defined alias. Delete Item Hex Code: Project-Id-Version: plasma_runner_CharacterRunner
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2011-01-13 20:10+0100
Last-Translator: Michal Sulek <misurel@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.1
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 &Spúšťacie slovo: Pridať položku Alias Alias: Nastavenie znaku Kód Vytvorí znaky z :q:, ak je to hexadecimálny kód alebo definovaný alias. Odstrániť položku Hexa kód: 