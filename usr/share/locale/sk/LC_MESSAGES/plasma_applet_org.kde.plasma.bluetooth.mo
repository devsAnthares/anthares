��    )      d  ;   �      �     �     �     �     �     �     �     �  	   �     �          %     :     G     _     v     ~  
   �  
   �     �     �     �     �     �     �     �     �               .  U   C  Z   �  O   �  D   D     �     �     �  	   �  	   �     �     �  �  �     �     �     �     �     �     �     	  	   	     !	     9	     Q	     f	     z	     �	  	   �	     �	  	   �	     �	  
   �	     �	     �	     
     
     "
     (
     ,
     F
     ]
     w
     �
     �
     �
  L   �
     @     P     \     n     ~     �     �                                             %                   	         (          &   !          '                          )   #          "                          $          
                            Adapter Add New Device Add New Device... Address Audio Audio device Available devices Bluetooth Bluetooth is Disabled Bluetooth is disabled Bluetooth is offline Browse Files Configure &Bluetooth... Configure Bluetooth... Connect Connected devices Connecting Disconnect Disconnecting Enable Bluetooth File transfer Input Input device Network No No Adapters Available No Devices Found No adapters available No connected devices Notification when the connection failed due to FailedConnection to the device failed Notification when the connection failed due to Failed:HostIsDownThe device is unreachable Notification when the connection failed due to NotReadyThe device is not ready Number of connected devices%1 connected device %1 connected devices Other device Paired Remote Name Send File Send file Trusted Yes Project-Id-Version: plasma_applet_org.kde.plasma.bluetooth
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:01+0100
PO-Revision-Date: 2015-04-30 17:32+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 Adaptér Pridať nové zariadenie Pridať nové zariadenie... Adresa Zvuk Zvukové zariadenie Dostupné zariadenia Bluetooth Bluetooth je zakázaný Bluetooth je zakázaný Bluetooh je vypnutý Prehliadať súbory Nastaviť Bluetooth... Nastaviť Bluetooth... Pripojiť Pripojené zariadenia Pripájam Odpojiť Odpája sa Povoliť Bluetooth Prenos súboru Vstup Vstupné zariadenie Sieť Nie Nie sú žiadne adaptéry Nenašli sa zariadenia Nie sú žiadne adaptéry Žiadne pripojené zariadenia Pripojenie k zariadeniu zlyhalo Zariadenie je nedosiahnuteľné Zariadenie nie je pripravené %1 pripojené zariadenie %1 pripojené zariadenia %1 pripojených zariadení Iné zariadenie Spárované Vzdialený názov Odoslať súbor Poslať súbor Dôveryhodné Áno 