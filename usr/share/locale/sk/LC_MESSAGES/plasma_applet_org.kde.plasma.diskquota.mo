��    
      l      �       �   
   �      �        .   0     _     t     �  (   �  8   �  �       �  &   �     !  ?   ?          �     �     �     �        
                             	        Disk Quota No quota restrictions found. Please install 'quota' Quota tool not found.

Please install 'quota'. Running quota failed e.g.: 12 GiB of 20 GiB%1 of %2 e.g.: 8 GiB free%1 free example: Quota: 83% usedQuota: %1% used usage of quota, e.g.: '/home/bla: 38% used'%1: %2% used Project-Id-Version: plasma_applet_org.kde.plasma.diskquota
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2016-11-20 19:59+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 Disková kvóta Nenašli sa žiadne obmedzenia kvóty. Prosím nainštalujte 'quota' Nástroj na kvótu nenájdený.

Prosím nainštalujte 'quota'. Spustenie quota zlyhalo %1 z %2 %1 voľných Quota: %1% použité %1: %2% použité 