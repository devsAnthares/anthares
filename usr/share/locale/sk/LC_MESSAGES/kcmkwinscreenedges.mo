��             +         �     �     �     �  	   �     �               '     3     H     a     t     �  S   �  w   �     `  M   u      �     �     �  	             2     K  %   Z     �     �  #   �     �     �     �  �  	     �     �     �  
   �     �       
   %  
   0     ;     V     s     �     �  :   �  U   �     ?	  A   W	     �	     �	     �	     �	     �	     �	     	
  ,   
     J
  
   [
  )   f
     �
     �
  	   �
                                            
                                                     	                                                ms % %1 - All Desktops %1 - Cube %1 - Current Application %1 - Current Desktop %1 - Cylinder %1 - Sphere &Reactivation delay: &Switch desktop on edge: Activation &delay: Activity Manager Always Enabled Amount of time required after triggering an action until the next trigger can occur Amount of time required for the mouse cursor to be pushed against the edge of the screen before the action is triggered Application Launcher Change desktop when the mouse cursor is pushed against the edge of the screen EMAIL OF TRANSLATORSYour emails Lock Screen NAME OF TRANSLATORSYour names No Action Only When Moving Windows Open krunnerRun Command Other Settings Quarter tiling triggered in the outer Show Desktop Switch desktop on edgeDisabled Toggle alternative window switching Toggle window switching Window Management of the screen Project-Id-Version: kcmkwinscreenedges
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-30 03:13+0100
PO-Revision-Date: 2017-05-08 17:17+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
  ms % %1 - Všetky plochy %1 - Kocka %1 - Aktuálna aplikácia %1 - Aktuálna plocha %1 - Valec %1 - Guľa Oneskorenie &reaktivácie: &Prepnúť plochu na okraji: Oneskorenie &aktivácie: Správca aktivít Vždy povolené Ako dlho je potrebné počkať na spustenie ďalšej akcie Ako dlho je potrebné držať kurzor myši za okrajom obrazovky pred spustením akcie Spúšťač aplikácií Zmeniť plochu, keď je kurzor myši posunutý za okraj obrazovky wizzardsk@gmail.com Zamknúť obrazovku Roman Paholík Žiadna akcia Len pri presúvaní okien Spustiť príkaz Ostatné nastavenia Štvrtinové obklady vyvolané na vonkajšom Zobraziť plochu Zakázané Prepnúť alternatívne prepínanie okien Prepnúť prepínanie okien Správa okien obrazovky 