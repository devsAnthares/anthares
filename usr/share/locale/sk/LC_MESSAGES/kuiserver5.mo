��          �   %   �      P     Q     b     w     �     �     �  
   �     �     �     �     �     �  /        3     Q     p     v     �     �     �     �     �     �     �       �  %      �  *        0     D     a     p     �  
   �     �  
   �     �     �  5   �        !   %     G     S     c     t  "   }  #   �  !   �  "   �     	     %                            
                                                                                             	    %1 file %1 files %1 folder %1 folders %1 of %2 processed %1 of %2 processed at %3/s %1 processed %1 processed at %2/s Appearance Behavior Cancel Clear Configure... Finished Jobs List of running file transfers/jobs (kuiserver) Move them to a different list Move them to a different list. Pause Remove them Remove them. Resume Show all jobs in a list Show all jobs in a list. Show all jobs in a tree Show all jobs in a tree. Show separate windows Show separate windows. Project-Id-Version: kuiserver5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2014-06-18 09:15+0200
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 %1 súbor %1 súbory %1 súborov %1 priečinok %1 priečinky %1 priečinkov Spracované %1 z %2 Spracované %1 z %2 pri %3/s Spracované %1 Spracované %1 pri %2/s Vzhľad Správanie Zrušiť Vyčistiť Nastaviť... Ukončené úlohy Zoznam bežiacich prenosov súborov/úloh (kuiserver) Presunúť ich do iného zoznamu Presunúť ich do iného zoznamu. Pozastaviť Odstrániť ich Odstrániť ich. Obnoviť Zobraziť všetky úlohy v zozname Zobraziť všetky úlohy v zozname. Zobraziť všetky úlohy v strome Zobraziť všetky úlohy v strome. Zobraziť samostatné okná Zobraziť samostatné okná. 