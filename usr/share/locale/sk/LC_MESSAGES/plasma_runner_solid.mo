��          �      L      �  m   �     /  "   <  F   _  F   �  F   �  J   4  N     R   �     !  %   2  $   X  #   }  $   �  %   �  &   �  �        �  �  �     s     �  1   �  Q   �  R     S   p  Q   �  b   	  b   y	     �	  
   �	     �	     
     
     
     
     &
     ;
                                
                          	                                          Close the encrypted container; partitions inside will disappear as they had been unpluggedLock the container Eject medium Finds devices whose name match :q: Lists all devices and allows them to be mounted, unmounted or ejected. Lists all devices which can be ejected, and allows them to be ejected. Lists all devices which can be mounted, and allows them to be mounted. Lists all devices which can be unmounted, and allows them to be unmounted. Lists all encrypted devices which can be locked, and allows them to be locked. Lists all encrypted devices which can be unlocked, and allows them to be unlocked. Mount the device Note this is a KRunner keyworddevice Note this is a KRunner keywordeject Note this is a KRunner keywordlock Note this is a KRunner keywordmount Note this is a KRunner keywordunlock Note this is a KRunner keywordunmount Unlock the encrypted container; will ask for a password; partitions inside will appear as they had been plugged inUnlock the container Unmount the device Project-Id-Version: plasma_runner_solid
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-12-15 21:17+0100
Last-Translator: Michal Sulek <misurel@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.1
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 Zamknúť kontajner Vysunúť médium Nájde zariadenia, ktorých názov zodpovedá :q: Zobrazí všetky zariadenia a umožní ich pripojenie, odpojenie alebo vysunutie. Zobrazí všetky zariadenia, ktoré je možné vysunúť a umožní ich vysunutie. Zobrazí všetky zariadenia, ktoré je možné pripojiť a umožní ich pripojenie. Zobrazí všetky zariadenia, ktoré je možné odpojiť a umožní ich odpojenie. Zobrazí všetky zašifrované zariadenia, ktoré je možné uzamknúť a umožní ich uzamknutie. Zobrazí všetky zašifrované zariadenia, ktoré je možné odomknúť a umožní ich odomknutie. Pripojiť zariadenie zariadenie vysunut zamknut pripojit odomknut odpojit Odomknúť kontajner Odpojiť zariadenie 