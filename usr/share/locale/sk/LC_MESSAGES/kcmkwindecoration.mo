��            )   �      �  !   �  "   �  '   �  ,     #   ;  &   _  !   �  &   �  '   �     �                 V   $  1   {     �  *   �     �        
     
   "     -     6     ;     D     T     [     a     g  �  p  	   6     @     H     W  	   l  	   v     �     �     �     �     �  
   �     �  Y   �  +   9     e  ;   {  $   �     �     �     	     	      	     %	     4	     L	     U	     ^	     d	                                                                          
                                                    	           @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Borders @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large Application menu Border si&ze: Buttons Close Close by double clicking:
 To open the menu, keep the button pressed until it appears. Close windows by double clicking &the menu button Context help Drag buttons between here and the titlebar Drop here to remove button Get New Decorations... Keep above Keep below Maximize Menu Minimize On all desktops Search Shade Theme Titlebar Project-Id-Version: kcmkwindecoration
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-04-14 02:49+0200
PO-Revision-Date: 2015-10-11 13:06+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 Obrovská Veľká Žiadne okraje Bez bočného okraja Normálna Nadmerná Drobná Veľmi obrovská Veľmi veľká Aplikačné menu Veľkosť okraja: Tlačidlá Zavrieť Zavrieť dvojklikom:
 Na otvorenie ponuky nechajte tlačidlo stlačené, kým sa objaví. Zavrieť okná dvojklikom na tlačidlo menu Kontextový pomocník Presuňte tlačidlá medzi týmto miesto a titulným pruhom Pustite tu na odstránenie tlačidla Získať nové dekorácie... Držať nad Držať pod Maximalizovať Menu Minimalizovať Na všetkých plochách Hľadať Zabaliť Téma Titulkový pruh 