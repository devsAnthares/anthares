��          �            x     y  
        �  
   �     �     �     �     �  ,   �  	             &     ,     2     L  �  a     \  
   b     m     }  
   �     �  
   �     �  +   �  
   �           	               .                            	      
                                             %1@%2 %2@%3 (%1) All Applications Appearance Applications Applications updated. Computer Edit Applications... Expand search to bookmarks, files and emails Favorites History Icon: Leave Show applications by name Switch tabs on hover Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-31 06:54+0200
PO-Revision-Date: 2014-08-28 18:54+0200
Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 %1@%2 %2@%3 (%1) Alle programmer Utseende Programmer Programmer oppdatert Datamaskin Rediger programmer … Utvid søk til bokmerker. filer og e-poster Favoritter Historie Ikon: Gå ut Vis programmer ved navn Bytt faner ved sveving 