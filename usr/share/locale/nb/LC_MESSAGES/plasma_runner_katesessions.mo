��          <      \       p   !   q   3   �      �   �  �   &   �  &         '                   Finds Kate sessions matching :q:. Lists all the Kate editor sessions in your account. Open Kate Session Project-Id-Version: KDE 4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-18 03:08+0100
PO-Revision-Date: 2009-05-28 10:02+0200
Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 0.3
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Finner Kate-økter som passer med :q:. Lister alle Kate-økter i kontoen din. Åpne Kate-økt 