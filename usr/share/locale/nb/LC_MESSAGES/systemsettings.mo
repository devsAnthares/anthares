��    +      t  ;   �      �  A   �     �  /        B     K     _     q     �     �     �  	   �     �  3   �  	   �             $   0  *   U     �  	   �  5   �     �     �  
   �               "     A  5   P  6   �     �  ,   �  /   �     &     =  O   M  Z   �  b   �  	   [  
   e  P   p     �    �  9   �
       :   )     d     j     �     �     �     �     �     �     �  3   �     ,     5     <      J     k     t     z  .   �  %   �     �     �     �     
          .  7   C  6   {     �  ;   �                !  T   5  Y   �  S   �  
   8     C  S   P     �            *             '      #   	   &         (                                                "      
       +       %                                             !                 )                 $    %1 is an external application and has been automatically launched (c) 2009, Ben Cooksley <i>Contains 1 item</i> <i>Contains %1 items</i> About %1 About Active Module About Active View About System Settings Apply Settings Author Ben Cooksley Configure Configure your system Determines whether detailed tooltips should be used Developer Dialog EMAIL OF TRANSLATORSYour emails Expand the first level automatically General config for System SettingsGeneral Help Icon View Internal module representation, internal module model Internal name for the view used Keyboard Shortcut: %1 Maintainer Marco Martin Mathias Soeken NAME OF TRANSLATORSYour names No views found Provides a categorized icons view of control modules. Provides a classic tree-based view of control modules. Relaunch %1 Reset all current changes to previous values Search through a list of control modulesSearch Show detailed tooltips System Settings System Settings was unable to find any views, and hence has nothing to display. System Settings was unable to find any views, and hence nothing is available to configure. The settings of the current module have changed.
Do you want to apply the changes or discard them? Tree View View Style Welcome to "System Settings", a central place to configure your computer system. Will Stephenson Project-Id-Version: systemsettings
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-05 03:26+0200
PO-Revision-Date: 2009-08-16 18:36+0200
Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 %1 er et eksternt program og er blitt stafrtet automatisk © 2009 Ben Cooksley <i>Inneholder 1 element</i> <i>Inneholder %1 elementer</i> Om %1 Om den aktive modulen Om den aktive visninga Om Systeminnstillinger Ta i bruk endringer Utvikler Ben Cooksley Sett opp Sett opp systemet ditt Velger hvordan detaljerte verkitøytips skal brukes Utvikler Dialog slx@nilsk.net Fold ut første nivå automatisk Generelt Hjelp Ikonvisning Intern modulrepresentasjon, intern modulmodell Internt navn for den brukte visningen Tastatursnarvei: %1 Vedlikeholder Marco Martin Mathias Soeken Nils Kristian Tomren Fant ingen visninger Gir en kategori-basert ikonvisning av styringsmodulene. Gir en klassisk tre-basert visning av styringsmoduler. Start %1 på nytt Tilbakestill alle nåværende endringer til forrige verdier Søk Vis detaljerte verktøytips Systeminnstillinger Systeminnstillinger klarte ikke å finne noen visning, så det er ingenting å vise. Systeminnstillinger klarte ikke å finne noen visning, så det er ingenting å sette opp. Innstillingene for gjeldende modul er endret.
Skal endringene brukes eller avvises? Trevisning Visningsstil Dette er «Systeminnstillinger», et sentralt sted for oppsett av datamaskinen din. Will Stephenson 