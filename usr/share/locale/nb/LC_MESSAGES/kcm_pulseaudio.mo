��          �   %   �      0     1  )   E     o  $   �  &   �  !   �  "   �          +     =     Q     i     q      y     �     �     �     �     �  <   �       ;   0     l  �  �  	   }     �     �     �     �  "   �  !        1  
   9     D  
   M     X     a     j     �     �     �  
   �     �  =   �     �     �                                         	                   
                                                                        @info:creditAuthor @info:creditCopyright 2015 Harald Sitter @info:creditHarald Sitter @labelNo Applications Playing Audio @labelNo Applications Recording Audio @labelNo Input Devices Available @labelNo Output Devices Available @labelProfile: @titlePulseAudio @title:tabAdvanced @title:tabApplications Capture Default EMAIL OF TRANSLATORSYour emails Inputs NAME OF TRANSLATORSYour names Outputs Playback Port This module allows to set up the Pulseaudio sound subsystem. label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-03 03:00+0200
PO-Revision-Date: 2015-08-27 17:01+0200
Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Forfatter Copyright 2015 Harald Sitter Harald Sitter Ingen programmer spiller lyd Ingen programmer tar opp lyd Ingen inndata-enheter tilgjengelig Ingen utgangsenheter tilgjengelig Profil: PulseAudio Avansert Programmer Fang inn Standard bjornst@skogkatt.homelinux.org Inndata Bjørn Steensrud Utdata Avspilling Port Denne modulen brukes til å sette opp lydsystemet Pulseaudio. %1: %2 100% %1% 