��    $      <  5   \      0     1  +   E     q  4   �  &   �     �     �                     5     :     P     X  ,   u  3   �     �     �               !  '   .     V     u     {     �     �     �     �     �     �  &   �       B        b    y  	   |     �     �     �     �     �     �     �  
   �  L   		     V	     ^	     u	     �	  -   �	  1   �	     �	  "   
     @
     E
     K
  !   S
  &   u
     �
     �
     �
     �
     �
     �
     �
       &        9  2   @     s                                       
                              !                                  $      #                               	                           "             @info:creditAuthor @info:creditCopyright 2006 Sebastian Sauer @info:creditSebastian Sauer @info:shell command-line argumentThe script to run. @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: application descriptionCommand-line utility to run Kross scripts. application nameKross Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2014-11-05 13:40+0100
Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Forfatter © 2006 Sebastian Sauer Sebastian Sauer Skriptet som skal kjøres. Generelt Legg til nytt skript. Legg til … Avbryt? Kommentar: knut.yrvin@gmail.com,fri_programvare@bojer.no,bjornst@skogkatt.homelinux.org Rediger Rediger valgte skript. Rediger … Kjør det valgte skriptet. Klarte ikke opprette skript for tolken «%1» Klarte ikke bestemme tolker for skriptfila «%1» Klarte ikke laste tolker «%1» Klarte ikke åpne skripfila «%1» Fil: Ikon: Tolker: Sikkerhetsnivå for Ruby-tolkeren Knut Yrvin,Axel Bojer,Bjørn Steensrud Navn: Ingen funksjon «%1» Ingen tolker «%1» Fjern Fjern valgte skript. Kjør Skriptfila «%1» finnes ikke. Stopp Stopp kjøring av det valgte skriptet. Tekst: Kommandolinjeprogram for å kjøre Kross-skripter. Kross 