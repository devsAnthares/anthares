��          T      �       �   >   �   #   �   '     3   D  .   x  "   �  �  �  F   �  +     %   8  1   ^  *   �     �                                        Delay till the lock user interface gets shown in milliseconds. Don't show any lock user interface. File descriptor for connecting to ksld. Greeter for the KDE Plasma Workspaces Screen locker Lock immediately, ignoring any grace time etc. Starts the greeter in testing mode Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-04-20 02:56+0200
PO-Revision-Date: 2015-06-13 22:11+0200
Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Forsinkelse i millisekunder til grensesnittet for brukerlåsing vises. Ikke vis noe grensesnitt for brukerlåsing. Filbeskriver for tilkobling til ksld. Hilser for skjermlåseren i KDE Plasma Arbeidsrom Lås straks, uten hensyn til ventetid osv. Starter hilseren i testmodus 