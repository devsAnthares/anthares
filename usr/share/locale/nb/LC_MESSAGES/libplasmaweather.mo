��          <      \       p      q   *   �   /   �   �  �   %   �  /     <   8                   Cannot find '%1' using %2. Connection to %1 weather server timed out. Weather information retrieval for %1 timed out. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-06-12 07:48+0000
PO-Revision-Date: 2010-06-10 15:22+0200
Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Kan ikke finne «%1» ved bruk av %2. Tilkobling til vær-tjener %1 fikk tidsavbrudd. Det ble tidsavbrudd under henting av værinformasjon for %1. 