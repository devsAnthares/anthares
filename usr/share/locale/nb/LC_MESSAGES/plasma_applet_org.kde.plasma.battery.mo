��          �   %   �      0  9   1     k     �     �  7   �  	   �     �     �               ,     :     N     U     l  #   y  %   �     �  5   �          %     3  )   ;  �  e     `     s  
   �     �     �  
   �     �     �     �     �     �               %  
   B     M     Q     U  @   m     �     �     �     �            
                                                               	                                                    %1 is remaining time, %2 is percentage%1 Remaining (%2%) %1% Battery Remaining %1%. Charging &Configure Power Saving... Battery is currently not present in the bayNot present Capacity: Charging Discharging Display Brightness Enable Power Management Fully Charged Keyboard Brightness Model: No Batteries Available Not Charging Placeholder is battery capacity%1% Placeholder is battery percentage%1% Power management is disabled The battery applet has enabled system-wide inhibition Time To Empty: Time To Full: Vendor: battery percentage below battery icon%1% Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-07-26 07:09+0200
PO-Revision-Date: 2014-09-22 19:25+0200
Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 %1 gjenstår (%2%) %1% batteri gjenstår %1%. Lader Tilpass &strømsparing … Ikke til stede Kapasitet: Lader Lader ut Vis lysstyrke Slå på strømstyring Ferdig ladet Tastaturlysstyrke Modell: Ingen batterier tilgjengelig Lader ikke %1% %1% Strømstyring slått av Batteri-miniprogrammet har slått på hindring for hele systemet Tid til full utlading: Tid til full lading; Leverandør: %1% 