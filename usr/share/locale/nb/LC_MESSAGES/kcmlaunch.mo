��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5    K     P      U  �  v  |  2	  _   �
          '     @  '   O     w     �      �     �                                          
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2008-10-16 20:04+0200
Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
  sek &Tidsgrense for oppstartsvarsel: <H1>Kunngjøring på oppgavelinja</H1> 
Du kan slå på en annen metode for oppstartskunngjøring, som kommer
på oppgavelinja, hvor en en knapp med et roterende timeglass dukker opp.
Dette symboliserer at programmet holder på å starte.
Det kan skje at noen programmer ikke er klar over denne 
oppstartskunngjøringen. I så fall vil pekeren slutte å blinke etter den tiden
som står oppført i seksjonen 'Tidsgrense for oppstartsindikator' <h1>Opptatt-peker</h1>
KDE tilbyr en opptatt-peker som oppstartskunngjørning for programmer.
For å slå på opptatt-peker, kryss av for ett av valgene i 
kombinasjonsboksen.
Det kan skje at noen programmer ikke er klar over denne 
oppstartskunngjøringen. I så fall vil pekeren slutte å blinke etter den tiden
som står oppført i seksjonen 'Tidsgrense for oppstartsindikator' <h1>Oppstartskunngjøring</h1> Her kan du velge hvordan det skal varsles at et program starter. Blinkende opptatt-peker Sprettende opptatt-peker Opp&tatt-peker Slå på &kunngjøring på oppgavelinja Ingen opptatt-peker Passiv opptatt-peker Tid&sgrense for oppstartsvarsel: &Kunngjøring på oppgavelinja 