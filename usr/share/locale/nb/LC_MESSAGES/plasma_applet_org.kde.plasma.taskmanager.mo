��    &      L  5   |      P     Q     _     l  U   s     �      �     �            
        (     8     E     Q     Y     a  	   v     �  
   �     �  	   �     �     �  	   �     �     �     �  )   �  (   $  '   M  "   u     �     �     �     �  9   �  J       M     [     l  	   }     �     �  '   �  
   �  
   �     �      	     	     #	     0	     <	     C	     L	     d	     p	     �	     �	  	   �	     �	     �	     �	     �	     �	     �	  )   �	  *   '
  &   R
  !   y
     �
     �
  
   �
     �
     �
  !   �
                                                  #         $                                                                      &   "   !   %                  	                  
    &All Desktops &New Desktop &Shade Activities a window is currently on (apart from the current one)Also available on %1 All Activities Allow this program to be grouped Alphabetically Behavior By Activity By Desktop By Program Name Do Not Group Do Not Sort Filters General Grouping and Sorting Grouping: Highlight windows Icon size: Large Ma&ximize Manually Maximum rows: Mi&nimize More Actions Move &To Current Desktop On %1 Show only tasks from the current activity Show only tasks from the current desktop Show only tasks from the current screen Show only tasks that are minimized Show tooltips Small Sorting: Start New Instance Which activities a window is currently onAvailable on %1 Which virtual desktop a window is currently onAvailable on all activities Project-Id-Version: plasma_applet_tasks
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-29 03:12+0200
PO-Revision-Date: 2013-11-04 23:00+0100
Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 &Alle skrivebord &Nytt skrivebord &Rull opp Også tilgjengelig på %1 Alle aktiviteter Tillat dette programmet å bli gruppert Alfabetisk Oppførsel Etter aktivitet Etter skrivebord Etter programnavn Ikke grupper Ikke sorter Filtre Generelt Gruppering og sortering Gruppering: Fremhev vinduer Ikonstørrelse: Stor Ma&ksimer Manuelt Høyeste antall rader: Mi&nimer Flere handlinger Flytt &til dette skrivebordet På %1 Vis bare oppgaver fra gjeldende aktivitet Vis bare oppgaver fra gjeldende skrivebord Vis bare oppgaver fra gjeldende skjerm Vis bare oppgaver som er minimert Vis verktøytips Liten Sortering: Start ny instans Tilgjengelig på %1 Tilgjengelig på alle aktiviteter 