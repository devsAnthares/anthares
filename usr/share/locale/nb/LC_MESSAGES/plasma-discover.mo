��    /      �  C                5   !  #   W  E   {  E   �  >     
   F     Q     X     k     �     �     �     �     �     �  <   �     1     9  *   B  
   m     x  	   �     �     �     �      �     �  
   �  :   �     /     7     >     Q     X  	   j     t     }     �     �     �     �     �  
   �     �     �  �  �     �	  5   �	  $   $
  D   I
  D   �
  =   �
                (     ;     O     _     w     �     �     �  ;   �     �       /     
   =  	   H  
   R     ]     m     s  )   {      �     �  ?   �  	             "  
   7     B  	   U     _     m     �     �     �     �     �     �     �     �                                      !          
          $   (            %                                ,          "   *               )                -          +                               #   /   	               '   &       .    %1 (%2) <em>%1 out of %2 people found this review useful</em> <em>Tell us about this review!</em> <em>Useful? <a href='true'><b>Yes</b></a>/<a href='false'>No</a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'><b>No</b></a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'>No</a></em> Add Source Addons Aleix Pol Gonzalez An application explorer Apply Changes Available backends:
 Available modes:
 Browse the origin's resources Delete the origin Description Directly open the specified application by its package name. Discard Discover Display a list of entries with a category. Homepage:  Install Installed Jonathan Thomas Launch License: List all the available backends. List all the available modes. Loading... Open with a program that can deal with the given mimetype. Rating: Remove Resources for '%1' Review Search in '%1'... Search... Settings Short summary... Size: %1 Source: Specify the new source for %1 Summary: Tasks Update All Updates Version: %1 Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-12 03:30+0200
PO-Revision-Date: 2015-06-13 21:56+0200
Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 %1 (%2) <em>%1 av %2 syntes denne anmeldelsen var nyttig</em> <em>Fortell oss om anmeldelsen!</em> <em>Nyttig?<a href='true'><b>Ja</b></a>/<a href='false'>Nei</a></em> <em>Nyttig?<a href='true'>Ja</a>/<a href='false'><b>Nei</b></a></em> <em>Nyttig?<a href='true'>Ja</a>/<a href='false'>Nei</a></em> Legg til kilde Tillegg Aleix Pol Gonzalez En programutforsker Bruk endringene Tilgjengelige motorer:
 Tilgjengelige moduser:
 Bla i ressursene i kilden Slett kilden Beskrivelse Åpne direkte det oppgitte programmet etter sitt pakkenavn. Forkast Oppdag Vis en liste over oppføringer med en kategori. Nettside:  Installer Installert Jonathan Thomas Start Lisens: List alle tilgjengelige bakgrunnsmotorer. List alle tilgjengelige moduser. Laster … Åpne med et program som kan håndtere den oppgitte mime-typen. Karakter: Fjern Ressurser for «%1» Gjennomgå Søk i «%1» … Søk … Innstillinger Kort sammendrag … Størrelse: %1 Kilde: Oppgi den nye kilden til %1| Sammendrag: Oppgaver Oppdater alle Oppdateringer Versjon: %1 