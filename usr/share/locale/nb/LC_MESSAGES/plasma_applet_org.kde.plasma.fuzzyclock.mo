��    �      t  �   �      �     �  	   �     �  
   �  	   �     �               ,     8     @     O     _     p          �     �     �     �     �     �     �     �               #     0     =     J     V     d     p     |     �     �     �     �  	   �     �     �     �     �                     0     >     L     \     m     {     �     �     �     �     �     �     �     �     �     �               0     A     T     e     v     �     �     �     �     �     �     �               !     0     ?     P     b     q     �     �     �     �     �     �     �     �     �                    *     9     I     V     c     q     }     �  
   �     �  
   �  
   �     �     �  
   �     �     �     
          /     @     Q     b     r     �     �     �     �     �     �     �     �     	          '     5     E     S     a     q     �     �     �    �  	   �     �  
   �     �     �     �               !     0  
   6     A     P     `     m     {     �     �     �     �     �     �     �     �     �     �       
        #     /     ;  
   H     S     _  
   l     w     �  	   �     �  	   �  	   �     �     �     �  
   �     �     �  	   �     �     �            
   !     ,     3  	   8     B  
   G     R     c     u     �     �     �     �     �     �     �     �     �               ,     :     I     V     d     r     �     �     �     �  
   �     �     �  	   �     �     �            
        '     3     ?  
   L     W     c  
   p     {     �  
   �     �  	   �  
   �  
   �     �  	   �  
   �     �  	   �  
                  *     ;     L     \     k     z     �     �     �     �     �     �     �               +     ;     K     ^     p     �     �     �  	   �     �         n             �   ]   S      %              
          �          1       I   J          P   F              "       )   Y       e      p                  0   ^   g   G   x   s           {      �       /       7   L   D      �   <   r   k       =       .   :   (   !   #          3   5   ;          Q   K   >               '   6      w       �   W       8      |   Z   B       -   i   �   j   [   $      X           M   �   V      +   2   9       �   �   \       u   f   R   h   `       ,   c   H          _       d       l   4               }   �             	       E   C      A   o       �   U   ?   v           O       *          &   N       m   T   z   b   q   @         ~          t   �       y             a               Accurate Afternoon Almost noon Appearance Bold text Early morning Eight o’clock Eleven o’clock End of week Evening Five o’clock Five past eight Five past eleven Five past five Five past four Five past nine Five past one Five past seven Five past six Five past ten Five past three Five past twelve Five past two Five to eight Five to eleven Five to five Five to four Five to nine Five to one Five to seven Five to six Five to ten Five to three Five to twelve Five to two Four o’clock Fuzzy Fuzzyness Half past eight Half past eleven Half past five Half past four Half past nine Half past one Half past seven Half past six Half past ten Half past three Half past twelve Half past two Italic text Late evening Middle of week Morning Night Nine o’clock Noon One o’clock Quarter past eight Quarter past eleven Quarter past five Quarter past four Quarter past nine Quarter past one Quarter past seven Quarter past six Quarter past ten Quarter past three Quarter past twelve Quarter past two Quarter to eight Quarter to eleven Quarter to five Quarter to four Quarter to nine Quarter to one Quarter to seven Quarter to six Quarter to ten Quarter to three Quarter to twelve Quarter to two Seven o’clock Six o’clock Start of week Ten o’clock Ten past eight Ten past eleven Ten past five Ten past four Ten past nine Ten past one Ten past seven Ten past six Ten past ten Ten past three Ten past twelve Ten past two Ten to eight Ten to eleven Ten to five Ten to four Ten to nine Ten to one Ten to seven Ten to six Ten to ten Ten to three Ten to twelve Ten to two Three o’clock Twelve o’clock Twenty past eight Twenty past eleven Twenty past five Twenty past four Twenty past nine Twenty past one Twenty past seven Twenty past six Twenty past ten Twenty past three Twenty past twelve Twenty past two Twenty to eight Twenty to eleven Twenty to five Twenty to four Twenty to nine Twenty to one Twenty to seven Twenty to six Twenty to ten Twenty to three Twenty to twelve Twenty to two Two o’clock Weekend! Project-Id-Version: plasma_applet_fuzzy_clock
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2014-10-02 11:03+0200
Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Nøyaktig Ettermiddag Snart tolv Utseende Halvfet tekst Tidlig om morgenen Klokka åtte Klokka elleve Slutten av uka Kveld Klokka fem Fem over åtte Fem over elleve Fem over fem Fem over fire Fem over ni Fem over ett Fem over sju Fem over seks Fem over ti Fem over tre Fem over tolv Fem over to Fem på åtte Fem på elleve Fem på fem Fem på fire Fem på ni Fem på ett Fem på sju Fem på seks Fem på ti Fem på tre Fem på tolv Fem på to Klokka fire Uklar Uskarphet Halv ni Halv tolv Halv seks Halv fem Halv ti Halv to Halv åtte Halv sju Halv elleve Halv fire Halv ett Halv tre Kursiv tekst Sent om kvelden Midt i uka Morgen Natt Klokka ni Tolv Klokka ett Kvart over åtte Kvart over elleve Kvart over fem Kvart over fire Kvart over ni Kvart over ett Kvart over sju Kvart over seks Kvart over ti Kvart over tre Kvart over tolv Kvart over to Kvart på åtte Kvart på elleve Kvart på fem Kvart på fire Kvart på ni Kvart på ett Kvart på sju Kvart på seks Kvart på ti Kvart på tre Kvart på tolv Kvart på to Klokka sju Klokka seks Tidlig i uka Klokka ti Ti over åtte Ti over elleve Ti over fem Ti over fire Ti over ni Ti over ett Ti over sju Ti over seks Ti over ti Ti over tre Ti over tolv Ti over to Ti på åtte Ti på elleve Ti på fem Ti på fire Ti på ni Ti på ett Ti på sju Ti på seks Ti på ti Ti på tre Ti på tolv Ti på to Klokka tre Klokka tolv Ti på halv ni Ti på halv tolv Ti på halv seks Ti på halv fem Ti på halv ti Ti på halv to Ti på halv åtte Ti på halv sju Ti på halv elleve Ti på halv fire Ti på halv ett Ti på halv tre Ti over halv åtte Ti over halv elleve Ti over halv fem Ti over halv  fire Ti over halv ni Ti over halv to Ti over halv åtte Ti over halv seks Ti over halv ti Ti over halv fire Ti over halv tolv Ti over halv to Klokka to Helg! 