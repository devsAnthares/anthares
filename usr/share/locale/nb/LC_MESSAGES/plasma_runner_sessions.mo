��          �   %   �      P  �  Q          )  6   9  -   p     �     �     �     �  (   �  d        x     �     �     �     �     �     �                7  "   X     {     �     �  
  �  �  �     a
     t
  0   �
  -   �
     �
     �
            %   ,  d   R     �     �     �     �     �     �     �                         $     0     7                               	                                                                      
                        <p>You have chosen to open another desktop session.<br />The current session will be hidden and a new login screen will be displayed.<br />An F-key is assigned to each session; F%1 is usually assigned to the first session, F%2 to the second session and so on. You can switch between sessions by pressing Ctrl, Alt and the appropriate F-key at the same time. Additionally, the KDE Panel and Desktop menus have actions for switching between sessions.</p> Lists all sessions Lock the screen Locks the current sessions and starts the screen saver Logs out, exiting the current desktop session New Session Reboots the computer Restart the computer Shutdown the computer Starts a new session as a different user Switches to the active session for the user :q:, or lists all active sessions if :q: is not provided Turns off the computer User sessionssessions Warning - New Session lock screen commandlock log out log out commandLogout log out commandlogout new session restart computer commandreboot restart computer commandrestart shutdown computer commandshutdown switch user switch user commandswitch switch user commandswitch :q: Project-Id-Version: krunner_sessions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2011-01-14 16:00+0100
Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.1
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 <p>Du har valgt å åpne en ny skrivebordsøkt.<br />Denne økta blir gjemt, og et nytt innloggingsbilde blir vist. <br />En f-tast er tilordnet hver økt. F%1 er oftest knyttet til den første økta, F%2 til den andre økta og så videre. Du kan bytte mellom øktene ved å trykke Ctrl, Alt og funksjonstasten som hører til. I KDE-panelet og på skrivebordsmenyen kan du også bytte mellom øktene.</p> Lister alle økter Lås skjermen Låser gjeldende økter og starter pauseskjermen Logger ut og avslutter denne skrivebordsøkta Ny økt Starter maskinen på nytt Start maskinen på nytt Slå av maskinen Starer en ny økt som en annen bruker Bytter til den aktive økta for bruker :q:, eller lister alle aktive økter hvis :q: ikke er oppgitt Slår av datamaskinen økter Advarsel – ny økt lock logg ut logout logout ny økt reboot restart shutdown bytt bruker switch bytt :q: 