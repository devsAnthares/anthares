��    
      l      �       �      �            	               .  I   3     }  	   �  �  �     �     �     �     �     �     �  V   �     0  	   >               
   	                           &Trigger word: Add Item Alias Alias: Character Runner Config Code Creates Characters from :q: if it is a hexadecimal code or defined alias. Delete Item Hex Code: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2010-09-01 09:02+0200
Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 U&tløserord: Legg til et element Alias Alias: Oppsett av Tegnkjører Kode Lager spesialtegn ut fra :q: hvis det er en heksadesimal kode eller et definert alias. Slett element Hekskode: 