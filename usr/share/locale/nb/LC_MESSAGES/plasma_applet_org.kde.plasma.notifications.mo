��    
      l      �       �       �                =     B  V   J     �     �     �  �  �     �     �  !   �          "     +     4     I  
   g               
      	                        %1 notification %1 notifications %1 of %2 %3 %1 running job %1 running jobs Copy History How much many bytes (or whether unit in the locale has been copied over total%1 of %2 No new notifications. No notifications or jobs Open... Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-26 03:23+0200
PO-Revision-Date: 2014-09-22 19:17+0200
Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 %1 varsling %1 varslinger %1 av %2 %3 %1 jobb kjører %1 jobber kjører Kopier Historie %1 av %2 Ingen nye varslinger Ingen varslinger eller jobber Åpne … 