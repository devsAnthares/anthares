��    !      $  /   ,      �     �  �   �     �     �     �     �     �     �     �     �     �     �      �               '     9     U     b     o     �     �     �     �     �     �     �     �      �          %     *  �  0     +     /     O  	   X     b  	   o  
   y  
   �  
   �     �     �     �     �     �     �     �          &     4     A     R  #   W     {  "   �  
   �     �  
   �     �     �     �     �     	                                                                    !            	      
                                                                         ... @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) Advanced Author Auto &Login Background: Clear Image Commands Cursor Theme: Customize theme Default Description EMAIL OF TRANSLATORSYour emails General Halt Command: Load from file... Login screen using the SDDM Maximum UID: Minimum UID: NAME OF TRANSLATORSYour names Name No preview available Reboot Command: Relogin after quit Remove Theme SDDM KDE Config Select image Session: The default cursor theme in SDDM Theme User User: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-07-12 06:58+0200
PO-Revision-Date: 2015-01-08 21:37+0100
Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 … (Tilgjengelige størrelser: %1) Avansert Forfatter Auto&logginn Bakgrunn: Tøm bilde Kommandoer Pekertema: Tilpass temaet Standard Beskrivelse bjornst@skogkatt.homelinux.org Generelt Stopp-kommando: Last fra fil … Loginn-skjerm som bruker SDDM Maksimum UID: Minimum UID: Bjørn Steensrud Navn Ingen forhåndsvisning tilgjengelig Omstart-kommando: Logg inn på nytt etter avslutning Fjern tema SDDM KDE-oppsett Velg bilde Økt: Standard pekertema i SDDM Tema Bruker Bruker: 