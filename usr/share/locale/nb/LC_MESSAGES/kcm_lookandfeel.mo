��    	      d      �       �      �                 :     G     f  ]   s  :   �  �             (     D     c     p     �  _   �  E   �                             	                 Configure Look and Feel details Cursor Settings Changed EMAIL OF TRANSLATORSYour emails Marco Martin NAME OF TRANSLATORSYour names Show Preview This module lets you configure the look of the whole workspace with some ready to go presets. You have to restart KDE for cursor changes to take effect. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-01-24 10:13+0100
PO-Revision-Date: 2015-04-30 21:55+0200
Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Sett opp  utseende og oppførsel Markørinnstillinger endret bjornst@skogkatt.homelinux.org Marco Martin Bjørn Steensrud Forhåndsvis Med denne modulen kan du sette opp utseendet for hele arbeidsflaten, med noe forhåndsdefinert. Du må starte KDE på nytt for at markørendringene skal tre i kraft. 