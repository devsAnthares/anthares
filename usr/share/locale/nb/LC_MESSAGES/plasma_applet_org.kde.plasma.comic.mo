��    1      �  C   ,      8  ;   9     u     �     �     �  6   �  A   
     L     U  $   Y  
   ~     �  #   �     �     �     �     �  7   �     1     N     j     w  $   �  ,   �     �     �     �          "     .     :     Q     f     x  �   �  "   A     d     k  %   }     �     �     �     �  
   �  7   	     =	     W	     o	  �  w	  D   w     �     �     �     �          -     G     P  '   U     }     �  .   �     �     �  
   �       1        M     d     z     �  ,   �  2   �     �     	          '     G     V     b     |     �     �  �   �     �     �     �  %   �     �               9     @     O     k     p     u     '   #      )   1                              "             *   	          ,           /      $       (   .                             0         &                           +   -   
                   %         !                        

Choose the previous strip to go to the last cached strip. &Create Comic Book Archive... &Save Comic As... &Strip Number: *.cbz|Comic Book Archive (Zip) @option:check Context menu of comic image&Actual Size @option:check Context menu of comic imageStore current &Position Advanced All An error happened for identifier %1. Appearance Archiving comic failed Automatically update comic plugins: Cache Check for new comic strips: Comic Comic cache: Could not create the archive at the specified location. Create %1 Comic Book Archive Creating Comic Book Archive Destination: Error Handling Failed adding a file to the archive. Failed creating the file with identifier %1. From beginning to ... From end to ... General Getting comic strip failed: Go to Strip Information Jump to &current Strip Jump to &first Strip Jump to Strip ... Manual range Maybe there is no Internet connection.
Maybe the comic plugin is broken.
Another reason might be that there is no comic for this day/number/string, so choosing a different one might work. No zip file is existing, aborting. Range: Strip identifier: The range of comic strips to archive. Update Visit the comic website Visit the shop &website an abbreviation for Number# %1 dd.MM.yyyy here strip means comic strip&Next Tab with a new Strip in a range: from toFrom: in a range: from toTo: minutes Project-Id-Version: KDE 4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-07-18 03:20+0200
PO-Revision-Date: 2011-05-21 15:16+0200
Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.1
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 

Velg forrige stripe for å gå til den sist mellomlagrede stripen. &Lag tegneseriearkiv … &Lagre serien som … &Serie nummer: *.cbz|tegneseriearkiv (Zip) &Virkelig størrelse Lagre gjeldende &posisjon Avansert Alle Det oppsto en feil for identifikator %1 Utseende Klarte ikke å arkivere serie Oppdater automatisk programtillegg for serier: Mellomlager Se etter nye tegneserier: Tegneserie Mellomlager for serie: Klarte ikke å opprette arkivet på oppgitt sted. Lag %1 tegneseriearkiv Lager tegneseriearkiv Mål: Feilhåndtering Klarte ikke å legge til en fil til arkivet. Klarte ikke å opprette fila med identifikator %1. Fra begynnelsen til … Fra slutten til … Generelt Det lyktes ikke å hente serie: Gå til serien Informasjon Hopp til &gjeldende serie Hopp til &første serie Hopp til serien … Manuelt område Kanskje det ikke er noen Internett-forbindelse.
Kanskje programtillegget for serier er ødelagt.
En annen grunn kan være at det ikke er noen serie for denne /dag/tall-strengen, så kanskje det virker med en annen streng. Fines ingen zip-fil, avbryter Område: Seriens identifikator: Tegneserieutgaver som skal arkiveres. Oppdater Gå til seriens nettsted Gå til butikkens &nettsted Nr. %1 dd.MM.åååå &Neste fane med en ny serie Fra: Til: minutter 