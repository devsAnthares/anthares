��          �      �           	  
             /     8  0   F     w     �     �     �     �     �  
   �     �     �     �                1     8     Q     Y    _     p     }     �  	   �     �  -   �     �     �       
   #     .     7     L     Y  
   n     y     �     �     �     �     �     �                                     
                                             	                      %1 by %2 Add Folder Background Color: Centered Change every: Directory with the wallpaper to show slides from Download Wallpapers Get New Wallpapers... Hours Image Files Minutes Next Wallpaper Image Open Image Open Wallpaper Image Open... Positioning: Recommended wallpaper file Remove wallpaper Scaled Scaled, Keep Proportions Seconds Tiled Project-Id-Version: plasma_wallpaper_image
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-16 07:10+0200
PO-Revision-Date: 2014-08-28 21:03+0200
Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 «%1» av %2 Legg til mappe Bakgrunnsfarge: Midtstilt Bytt bilde hver: Mappe med tapetet det skal vises lysbilder av Last ned bakgrunnsbilder Skaff nye bakgrunnsbilder … Timer Bildefiler Minutter Neste bakgrunnsbilde Åpne bildet Åpne bakgrunnsbilde Åpne … Plassering: Anbefalt bakgrunnsbilde Fjern tapet Skalert Skalert, behold proporsjoner Sekunder Flislagt 