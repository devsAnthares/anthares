��          |      �             !     5  @   ;     |     �     �     �     �  
   �     �     �  �  �     �     �  L   �     ?     H     \  	   d     n     }     �     �                      	                       
              <a href='%1'>%1</a> Close Drop text or an image onto me to upload it to an online service. General History Size: Paste Please wait Please, try again. Sending... Share Successfully uploaded Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-01 04:04+0100
PO-Revision-Date: 2014-10-04 10:15+0200
Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 <a href='%1'>%1</a> Lukk Slipp en tekst eller et bilde her for å laste det opp til en nett-tjeneste. Generelt Historiestørrelse: Lim inn Vent litt Forsøk igjen. Sender … Del Fullført opplasting 