��    >        S   �      H  H   I     �     �     �     �     �  '   �       "        ;     U     l  
   u     �     �     �     �     �     �     �     �  "        +     2  ?   P     �     �     �     �  &   �      �  %        5     O     i     �  >   �     �     �  '   	     8	     I	     \	     o	     �	     �	     �	     �	     �	     �	     �	      
     
     %
     7
     H
     [
     n
     �
     �
  3   �
  �  �
     �     �     
               "     1     @     P     \     e     |  	   �     �     �     �     �     �     �     �     �  #        %     *     ,     5     A     I     U     \     l     x     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �               	                         #     4                        "      &                                             :      /   #   $              3   =   ;              +   )   6   9          <      (   
   >         1   4                    2   %          8         0      *   5                 ,   	   !                 .      7      '   -    A weather station location and the weather service it comes from%1 (%2) Beaufort scale bft Celsius °C Degree, unit symbol° Details Fahrenheit °F Forecast period timeframe1 Day %1 Days Hectopascals hPa High & Low temperatureH: %1 L: %2 High temperatureHigh: %1 Inches of Mercury inHg Kelvin K Kilometers Kilometers per Hour km/h Kilopascals kPa Knots kt Low temperatureLow: %1 Meters per Second m/s Miles Miles per Hour mph Millibars mbar No weather stations found for '%1' Search Short for no data available- Shown when you have not set a weather providerPlease Configure Temperature: Units Weather Station Wind conditionCalm content of water in airHumidity: %1%2 distance, unitVisibility: %1 %2 ground temperature, unitDewpoint: %1 humidex, unitHumidex: %1 pressure tendencyfalling pressure tendencyrising pressure tendencysteady pressure tendency, rising/falling/steadyPressure Tendency: %1 pressure, unitPressure: %1 %2 temperature, unit%1%2 visibility from distanceVisibility: %1 wind directionE wind directionENE wind directionESE wind directionN wind directionNE wind directionNNE wind directionNNW wind directionNW wind directionS wind directionSE wind directionSSE wind directionSSW wind directionSW wind directionVR wind directionW wind directionWNW wind directionWSW wind direction, speed%1 %2 %3 wind speedCalm windchill, unitWindchill: %1 winds exceeding wind speed brieflyWind Gust: %1 %2 Project-Id-Version: KDE 4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2009-09-27 18:16+0200
Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 %1 (%2) Beaufort-skala bft Celsius °C ° Detaljer Fahrenheit °F 1 dag %1 dager Hektopascal hPa H: %1 L: %2 Maks: %1 Tommer kvikksølv inHg Kelvin K Kilometer Kilometer i timen km/h Kilopascal kPa Knop kt Min: %1 Meter per sekund m/s Miles Miles pr. time mph Millibar mbar Fant ingen værstasjoner for «%1» Søk - Sett opp Temperatur: Enheter Værstasjon Stille Fuktighet: %1%2 Sikt: %1 %2 Duggpunkt: %1 Fuktindeks: %1 fallende økende stabilt Trykktendens: %1 Trykk: %1 %2 %1%2 Sikt: %1 O ONO OSO N No NNO NNV NV S SO SSO SSV SV var. V VNV VSV %1 %2 %3 Stille Vindkjøling: %1 Vindkast: %1 %2 