��          �   %   �      p  !   q  "   �  '   �  ,   �  #     &   /  !   V  &   x  '   �     �     �     �     �  V   �     K  *   X     �     �  
   �  
   �     �     �     �     �     �     �     �                        0     E     L  
   X     c     p     |     �     �     �  g   �       %   !     G     e  	   �  
   �     �     �     �     �     �     �     �                                                                              
                	                             @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Borders @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large Application menu Border si&ze: Buttons Close Close by double clicking:
 To open the menu, keep the button pressed until it appears. Context help Drag buttons between here and the titlebar Drop here to remove button Get New Decorations... Keep above Keep below Maximize Menu Minimize On all desktops Search Shade Titlebar Project-Id-Version: kcmkwindecoration
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-04-14 02:49+0200
PO-Revision-Date: 2015-04-26 21:29+0200
Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Enorm Stor Ingen kantlinjer Ingen sidekantlinjer Normal Forstørret Bitteliten Veldig enorm Svært stor Programmeny Kantlin&jestørrelse: Knapper Lukk Lukk ved å dobbelttrykke.
 Hvis menyen skal vises igjen så hold knappen nede til den kommer til syne. Konteksthjelp Dra knapper mellom her og tittellinja Slipp her for å fjerne knapp Hent nye dekorasjoner … Hold over Hold under Maksimer Meny Minimer På alle skrivebord Søk Rull Tittellinje 