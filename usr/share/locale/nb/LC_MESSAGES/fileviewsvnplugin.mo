��          �   %   �      0     1  +   G  .   s  6   �  *   �  #     &   (  /   O  2     :   �  K   �  -   9  $   g  '   �     �     �     �     �  #        8     V     j     �  �  �     �     �  %   �  -   �  '        .     G     e  "   �  ,   �  K   �  "    	     C	     Z	     v	     �	  	   �	     �	     �	     �	     �	     �	     �	                                                    	   
                                                                        @action:buttonCommit @info:statusAdded files to SVN repository. @info:statusAdding files to SVN repository... @info:statusAdding of files to SVN repository failed. @info:statusCommit of SVN changes failed. @info:statusCommitted SVN changes. @info:statusCommitting SVN changes... @info:statusRemoved files from SVN repository. @info:statusRemoving files from SVN repository... @info:statusRemoving of files from SVN repository failed. @info:statusSVN status update failed. Disabling Option "Show SVN Updates". @info:statusUpdate of SVN repository failed. @info:statusUpdated SVN repository. @info:statusUpdating SVN repository... @item:inmenuSVN Add @item:inmenuSVN Commit... @item:inmenuSVN Delete @item:inmenuSVN Update @item:inmenuShow Local SVN Changes @item:inmenuShow SVN Updates @labelDescription: @title:windowSVN Commit Show updates Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2011-01-15 19:12+0100
Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.1
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Innlever La til filer til SVN-lageret. Legger til filer til SVN-lageret … Klarte ikke å legge til filer i SVN-lageret. Innlevering av SVN-endringer mislyktes. SVN-endringer innlevert. Innleverer SVN-endringer … Fjernet filer fra SVN-lageret. Fjerner filer fra SVN-lageret … Klarte ikke å fjerne filer fra SVN-lageret. SVN statusoppdatering mislyktes. Slår av valget «Vis SVN-oppdateringer». Klarte ikke oppdatere SVN-lageret. SVN-lageret oppdatert. Oppdaterer SVN-lageret … SVN Legg til SVN Innlever … SVN Slett SVN Oppdater Vis lokale SVN-endringer Vis SVN-oppdateringer Beskrivelse: SVN Innlever Vis oppdateringer 