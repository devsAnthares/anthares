��          �   %   �      P     Q  L   V  /   �     �     �     �          $     1     A     W     f  I   w     �  *   �     �            !   9  "   [     ~  6   �  *   �     �     
  �       �  �   �  �   �  {   �     
	  �   #	  x   �	  *   :
  6   e
  �   �
     9  �   >  �   �     �  �   �  O   }  1   �  1   �  C   1  �   u  �   �  �   �  �   u  F   \  F   �           	                          
                                                                                         to  <p>Some changes such as DPI will only affect newly started applications.</p> A non-proportional font (i.e. typewriter font). Ad&just All Fonts... BGR Click to change all fonts Configure Anti-Alias Settings Configure... E&xclude range: Font Settings Changed Font role%1:  Force fonts DPI: Hinting is a process used to enhance the quality of fonts at small sizes. RGB Smallest font that is still readable well. Use a&nti-aliasing: Use anti-aliasingDisabled Use anti-aliasingEnabled Use anti-aliasingSystem Settings Used by menu bars and popup menus. Used by the window titlebar. Used for normal text (e.g. button labels, list items). Used to display text beside toolbar icons. Vertical BGR Vertical RGB Project-Id-Version: kcmfonts
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-24 05:54+0100
PO-Revision-Date: 2010-02-01 01:02+0530
Last-Translator: seshagiri prabhu <seshagiriprabhu@gmail.com>
Language-Team: Malayalam <smc-discuss@googlegroups.com>
Language: ml
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 0.3
Plural-Forms: nplurals=2; plural=(n != 1);
 ടു <p>ഡിപിഐ പോലുള്ള മാറ്റങ്ങള്‍ പുതുതായി ആരംഭിക്കുന്ന പ്രയോഗങ്ങളില്‍ മാത്രമേ ലഭ്യമാകുള്ളൂ.</p> ഒരു ആനുപാതികമല്ലാത്ത അക്ഷരസഞ്ചയം (അതായതു  ടൈപ്പ്റൈറ്റര്‍ അക്ഷരസഞ്ചയം). എല്ലാ അക്ഷരസഞ്ചയങ്ങളും ആ&വശ്യാനുസൃതമാക്കുക... ബിജിആര്‍ എല്ലാ അക്ഷരസഞ്ചയങ്ങളും മാറ്റുന്നതിനായി ക്ലിക്ക് ചെയ്യുക ആന്റി-അലിയാസ് സജ്ജീകരണങ്ങള്‍ ക്രമീകരിക്കുക ക്രമീകരിക്കുക... ഒ&ഴിവാക്കേണ്ട പരിധി: അക്ഷരസഞ്ചയ ക്രമീകരണങ്ങളില്‍ മാറ്റം വരുത്തിയിരിക്കുന്നു %1:  അക്ഷരസഞ്ചയങ്ങളുടെ ഡിപിഐ നിര്‍ബന്ധമായാക്കേണ്ടതു്: വലിപ്പം കുറഞ്ഞ അക്ഷരങ്ങളുടെ ഗുണനിലവാരം കൂട്ടാനുള്ളൊരു പ്രവൃത്തിയാണു് ഹിന്റിങ്ങ്. ആര്‍ജിബി വായിക്കുവാന്‍ സാധ്യമാകുന്ന ഏറ്റവും ചെറിയ വലിപ്പമുള്ള അക്ഷരസഞ്ചയം. &ആന്റി-അലിയാസിങ് ഉപയോഗിക്കുക: പ്രവര്‍ത്തന രഹിതം പ്രവര്‍ത്തന സജ്ജം സിസ്റ്റം ക്രമീകരണങ്ങള്‍ മെനു ബാറുകളും പോപപ്പ് മെനുകളും ഉപയോഗിക്കുന്നു. ജാലകത്തിനുള്ള തലക്കെട്ട് പട്ടയില്‍ ഉപയോഗിക്കുന്നു. സാധാരണ വാചകത്തിനു് ഉപയോഗിക്കുന്നതു് (ഉദാ. ബട്ടണ്‍ ലേബലുകള്‍, പട്ടികയിലുള്ള വസ്തുക്കള്‍) ഉപകരണപട്ടയുടെ ചിഹ്നത്തിനടുത്തു് വാചകം പ്രദര്‍ശിപ്പിക്കുന്നതിനായി ഉപയോഗിക്കുന്നു. വെര്‍ട്ടിക്കല്‍ ബിജിആര്‍ വെര്‍ട്ടിക്കല്‍ ആര്‍ജിബി 