��          �            h     i     m     �     �     �  S   �  w     M   �  	   �     �     �     	          6  �  H     �  Y     S   _  E   �  d   �  �   ^  6    �   ;     		  Y   (	  =   �	  7   �	  ?   �	     8
                 
                             	                                   ms &Reactivation delay: &Switch desktop on edge: Activation &delay: Always Enabled Amount of time required after triggering an action until the next trigger can occur Amount of time required for the mouse cursor to be pushed against the edge of the screen before the action is triggered Change desktop when the mouse cursor is pushed against the edge of the screen No Action Only When Moving Windows Other Settings Show Desktop Switch desktop on edgeDisabled Window Management Project-Id-Version: kcmkwinscreenedge.po
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-30 03:13+0100
PO-Revision-Date: 2010-01-30 14:00+0530
Last-Translator: seshagiri prabhu <seshagiriprabhu@gmail.com>
Language-Team: malayalam <smc-discuss@googlegroups.com>
Language: ml
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 എംഎസ് വീണ്ടും സജീവമാക്കുന്നതിലെ താമസം &വക്കില്‍ വച്ചു് പണിയിടം മാറുക: സജീവമാക്കുന്നതിലെ &താമസം: എല്ലായ്പോഴും പ്രവര്‍ത്തനസജ്ജമാക്കി ഒരു പ്രവൃത്തിക്കു ശേഷം അടുത്ത പ്രവൃത്തി തുടങ്ങുവാനുള്ള സമയം മൌസ് ചൂണ്ടിയെ സ്ക്രീനിന്റെ വക്ക് ലക്ഷ്യമാക്കി അമര്‍ത്തിയതിനു് ശേഷം നടപടി തുടങ്ങുന്നതിനു് മുമ്പു് എത്ര സമയം വേണം. മൌസ് ചൂണ്ടി സ്ക്രീനിന്റെ വക്കു് ചേര്‍ത്തു് അമര്‍ത്തുമ്പോള്‍ പണിയിടം മാറുക നടപടിയില്ല ജാലകങ്ങള്‍ നീക്കുമ്പോള്‍ മാത്രം മറ്റു് സജ്ജീകരണങ്ങള്‍ പണിയിടം കാണിയ്ക്കുക പ്രവര്‍ത്തനരഹിതമാക്കി ജാലകപാലനം 