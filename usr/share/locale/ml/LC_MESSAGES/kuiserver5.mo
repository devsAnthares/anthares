��          �   %   �      0     1     B     W     j     �     �  
   �     �     �     �     �     �     �                ,     9     @     X     q     �     �     �  G  �  .        F  J   f  Y   �  =     M   I     �     �     �  *   �  I     f   N  f   �  U   	  /   r	  6   �	  4   �	  r   
  w   �
  l   �
  q   f  S   �  T   ,                             
                                                                                              	    %1 file %1 files %1 folder %1 folders %1 of %2 processed %1 of %2 processed at %3/s %1 processed %1 processed at %2/s Appearance Behavior Cancel Configure... Finished Jobs Move them to a different list Move them to a different list. Pause Remove them Remove them. Resume Show all jobs in a list Show all jobs in a list. Show all jobs in a tree Show all jobs in a tree. Show separate windows Show separate windows. Project-Id-Version: kuiserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2008-07-10 21:23+0530
Last-Translator: # ANI PETER|അനി പീറ്റര്‍ <peter.ani@gmail.com>
Language-Team: Swathanthra|സ്വതന്ത്ര Malayalam|മലയാളം Computing|കമ്പ്യൂട്ടിങ്ങ് <smc-discuss@googlegroups.com>
Language: ml
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: KBabel 1.11.4
 %1 ഫയല്‍ %1 ഫയലുകള്‍ %1 അറ %1 അറകള്‍ %2-ല്‍ %1 പ്രവര്‍ത്തനം കഴിഞ്ഞു %3/s-ല്‍ %2-ല്‍ %1 പ്രവര്‍ത്തനം കഴിഞ്ഞു %1 പ്രവര്‍ത്തനം കഴിഞ്ഞു  %2/s-ല്‍ %1 പ്രവര്‍ത്തനം കഴിഞ്ഞു കാഴ്ച വിശേഷത റദ്ദാക്കുക ക്രമീകരിക്കുക... പൂര്‍ത്തിയാക്കിയ ജോലികള്‍ മറ്റൊരു പട്ടികയിലേക്കു് അവയെ മാറ്റുക അവയെ മറ്റൊരു പട്ടികയിലേക്കു് നീക്കുക തല്‍ക്കാലത്തേക്കു് നിര്‍ത്തുക അവ നീക്കം ചെയ്യുക അവയെ നീക്കം ചെയ്യുക. വീണ്ടും ആരംഭിക്കുക ഒരു പട്ടികയിലുള്ള ജോലികളെല്ലാം കാണിക്കുക ഒരു പട്ടികയിലുള്ള എല്ലാ ജോലികളും കാണിക്കുക. ഒരു ട്രീയിലുള്ള ജോലികളെല്ലാം കാണിക്കുക ഒരു ട്രീയിലുള്ള എല്ലാ ജോലികളും കാണിക്കുക. വെവ്വേറെ ജാലകങ്ങള്‍ കാണിക്കുക വെവ്വേറെ ജാലകങ്ങള്‍ കാണിക്കുക. 