��          �      <      �  m   �          7     ?     ]     z  Y   �     �      �               ;     W     ^     t  %   y     �  �  �    v  V   �  !   �  g     P   u  !   �  �   �  L   �  :    	  :   [	  �   �	  G   +
  .   s
  h   �
       �   $  x   �                                                          
                                       	    A list of Phonon Backends found on your system.  The order here determines the order Phonon will use them in. Apply Device List To... Backend Copyright 2006 Matthias Kretz Default/Unspecified Category Defer Defines the default ordering of devices which can be overridden by individual categories. Device Preference EMAIL OF TRANSLATORSYour emails Matthias Kretz NAME OF TRANSLATORSYour names Phonon Configuration Module Prefer Show advanced devices Test no preference for the selected device prefer the selected device Project-Id-Version: kcm_phonon
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2008-12-17 00:46-0800
Last-Translator: Praveen Arimbrathodiyil <pravi.a@gmail.com>
Language-Team: Malayalam <smc-discuss@googlegroups.com>
Language: ml
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 0.3
Plural-Forms: nplurals=2; plural=(n != 1);
 നിങ്ങളുടെ സിസ്റ്റത്തില്‍ ലഭ്യമുള്ള ഫോണണ്‍ ബാക്കെന്‍ഡുകള്‍.  ഈ ക്രമത്തിലാണു് ഫോണണ്‍ അവ ഉപയോഗിക്കുന്നതു്. ഉപകരണ പട്ടിക സൂക്ഷിക്കേണ്ടതു്... ബാക്കെന്‍ഡ് പകര്‍പ്പവകാശം 2006 മത്തിയാസ് ക്രെറ്റ്സ് സഹജമായ/വ്യക്തമല്ലാത്ത വിഭാഗം വൈകിയ്ക്കുക ഉപകരണങ്ങളുടെ സഹജമായ ക്രമം വ്യക്തമാക്കുന്നു, ഇത് പ്രത്യേക വിഭാഗങ്ങള്‍ക്ക് അവഗണിക്കാം. ഉപകരണത്തിന്റെ മുന്‍ഗണനകള്‍ apeter@redhat.com, harivishnu@gmail.com, pravi.a@gmail.com മത്തിയാസ് ക്രെറ്റ്സ് അനി പീറ്റര്‍, ഹരി വിഷ്ണു, പ്രവീണ്‍ അരിമ്പ്രത്തൊടിയില്‍ ഫോണണ്‍ ക്രമീകരണ മൊഡ്യൂള്‍ മുന്‍ഗണന നല്‍കുക സങ്കീര്‍ണ്ണമായ ഉപകരണങ്ങള്‍ കാണിക്കുക പരീക്ഷണം തെരഞ്ഞെടുത്ത ഉപകരണത്തിനു് മുന്‍ഗണനകള്‍ ലഭ്യമല്ല തെരഞ്ഞെടുത്ത ഉപകരണത്തിനു് മുന്‍ഗണന നല്‍കുക 