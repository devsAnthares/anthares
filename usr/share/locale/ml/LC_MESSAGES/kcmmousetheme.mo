��          �            x  �   y  m   �  `   i     �     �     �     �           3     R     W     h  ?   u  Y   �  +     K  ;  �  �  a    j  {	     �
  Y        _  �   r       
   9     D  D   Q  %   �  �   �  
  p  �   {        	                                                  
                     <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. Project-Id-Version: kcminput
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2008-11-12 12:27+0530
Last-Translator: മണിലാല്‍ കെ.എം|Manilal K M <libregeek@gmail.com>
Language-Team:  Swathanthra|സ്വതന്ത്ര Malayalam|മലയാളം Computing|കമ്പ്യൂട്ടിങ്ങ് <smc-discuss@googlegroups.com>
Language: ml
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: KBabel 1.11.4
 <qt><i>%1</i> എന്ന കര്‍സര്‍ പ്രമേയം ഒഴിവാക്കണം എന്നു താങ്കള്‍ക്കു ഉറപ്പാണോ?<br />ഈ പ്രവര്‍ത്തനം %1 എന്ന പ്രമേയം ഇന്‍സ്റ്റാള്‍ ചെയ്ത ഫയലുകള്‍ എല്ലാം മായ്ക്കും.</qt> <qt>നിലവില്‍ ഉപയോഗിച്ചു കൊണ്ടിരിക്കുന്ന പ്രമേയം മായ്ക്കുവാന്‍ താങ്കള്‍ക്കു സാധിക്കില്ല.<br />ആദ്യം മറ്റൊരു തീമിലേക്കു മാറേണ്ടതാണു.</qt> താങ്കളുടെ ചിഹ്നം പ്രമേയം ഫോള്‍ഡറില്‍ %1 എന്ന പേരിലുള്ള ഒരു പ്രമേയം നിലവിലുണ്ട്. താങ്കള്‍ക്കു അതിനെ ഈ പ്രമേയം കൊണ്ടു റീപ്ലേസ് ചെയ്യണമോ? സ്ഥിരീകരണം കര്‍സറിന്റെ ക്രമീകരണങ്ങള്‍ മാറി വിവരണം പ്രമേയം യൂആര്‍‌എല്‍ ടൈപ്പ് ചെയ്യുകയോ ഡ്രാഗ് ചെയ്യുകയോ ചെയ്യുക. shijualexonline@gmail.com Shiju Alex പേര് പ്രമേയം മാറ്റിയെഴുതട്ടെ? പ്രമേയം കളയുക %1 എന്ന പ്രമാണം സാധുവായ ഒരു കര്‍സര്‍ പ്രമേയം ആണെന്നു തോന്നുന്നില്ല. കര്‍സര്‍ പ്രമേയം ഡൌണ്‍ലോഡ് ചെയ്യുവാന്‍ സാധിച്ചില്ല. %1 എന്ന വിലാസം ശരിയാണോ എന്നു പുനഃപരിശോധിക്കുക. %1 എന്ന കര്‍സര്‍ പ്രമേയം കണ്ടെത്താന്‍ കഴിഞ്ഞില്ല. 