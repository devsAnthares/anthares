��          D      l       �      �   
   	       *      K  K  �  �          6  �   P                          <h1>Multiple Desktops</h1>In this module, you can configure how many virtual desktops you want and how these should be labeled. Desktop %1 Desktop %1: Here you can enter the name for desktop %1 Project-Id-Version: kcm_kwindesktop
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-06 05:35+0100
PO-Revision-Date: 2008-07-10 02:43+0530
Last-Translator: ANI PETER|അനി പീറ്റര്‍ <peter.ani@gmail.com>
Language-Team: Swathanthra|സ്വതന്ത്ര Malayalam|മലയാളം Computing|കമ്പ്യൂട്ടിങ്ങ് <smc-discuss@googlegroups.com>
Language: ml
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=(n != 1);
 <h1>അനവധി പണിയിടങ്ങള്‍</h1>നിങ്ങള്‍ക്കാവശ്യമുള്ള വിര്‍ച്ച്വല്‍ പണിയിടങ്ങള്‍ ക്രമീകരിക്കുന്നതിനും അവ ലേബല്‍ ചെയ്യുന്നതിനും ഈ ഘടകം സഹായിക്കുന്നു. പണിയിടം %1 പണിയിടം %1: ഇവിടെ നിങ്ങള്‍ക്കു് പണിയിടം %1-നുള്ള പേരു് നല്‍കാം 