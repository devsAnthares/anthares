��            )   �      �     �     �     �     �     �     �     �     �     �     �                    #     ;     N     Z     n     t     z  
        �     �     �     �     �     �     �  �       �     �     �     �     �                %  
   4     ?     K     _     l  #   y     �     �     �     �     �     �  
   �               /     <     T     [     n                                                                    
      	                                                                &Delete &Empty Trash Bin &Move to Trash &Open &Paste &Properties &Refresh Desktop &Refresh View &Reload &Rename Custom title Default Deselect All Enter custom title here File name pattern: File types: Hide Files Matching Icons Large None Select All Show All Files Show Files Matching Show a place: Show the Desktop folder Small Specify a folder: Type a path or a URL here Project-Id-Version: plasma_applet_folderview
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:11+0100
PO-Revision-Date: 2013-04-18 20:47+0000
Last-Translator: Sveinn í Felli <sveinki@nett.is>
Language-Team: Icelandic <kde-isl@molar.is>
Language: is
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: Plural-Forms: nplurals=2; plural=n != 1;


X-Generator: Lokalize 1.2
 &Eyða &Tæma ruslakörfu &Setja í ruslið &Opna &Líma &Eiginleikar Endu&rteikna skjáborð Uppfæ&ra sýn Endu&rlesa Endu&rnefna Sérsniðinn titill Sjálfgefið Afvelja allt Sláðu hér inn sérsniðinn titil Mynstur skráheitis: Skráartegundir: Fela skrár sem samsvara Táknmyndir Stórar Ekkert Velja allt Sýna allar skrár Sýna skrár sem samsvara Sýna stað: Sýna skjáborðsmöppu Smáar Tilgreindu möppu: Settu inn slóð hér 