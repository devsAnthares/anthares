��            )   �      �  &   �     �     �     �     �      �               .     6  ,   S  3   �     �     �     �     �     �  '        4     S     Y     o     �     �     �     �     �  &   �     �  �  �     �     �     �     �     
  _        v     |  	   �     �  3   �  +   �  (        <     [  
   b     m     y  e   �     �     	     	  
   8	     C	     ]	      c	     �	  "   �	     �	                                       
                      	                                                                            @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2016-04-08 22:57+0000
Last-Translator: Sveinn í Felli <sv1@fellsnet.is>
Language-Team: Icelandic <translation-team-is@lists.sourceforge.net>
Language: is
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: Plural-Forms: nplurals=2; plural=n != 1;






 Almennt Bæta við nýrri skriftu. Bæta við... Hætta við? Athugasemd: ra@ra.is, logi@logi.org, pjetur@pjetur.net, leosson@frisurf.no, svanur@tern.is, sv1@fellsnet.is Sýsl Breyta valinni skriftu. Sýsla... Keyra valda skriftu. Gat ekki búið til skriftu fyrir þýðandann "%1" Gat ekki fundið túlk fyrir skriftuna "%1" Gat ekki hlaðið inn þýðandanum "%1" Gat ekki opnað skriftuna "%1" Skrá: Táknmynd: Þýðandi: Öryggisstig Ruby túlksins Richard Allen, Logi Ragnarsson, Pjetur G. Hjaltason, Arnar Leósson, Svanur Pálsson, Sveinn í Felli Heiti: Engin slík aðferð "%1" Enginn slíkur túlkur "%1" Fjarlægja Fjarlægja valda skriftu. Keyra Skriftuskráin "%1" fannst ekki. Stöðva Stöðva keyrslu valinnar skriftu. Texti: 