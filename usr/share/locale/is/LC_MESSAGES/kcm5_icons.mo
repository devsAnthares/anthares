��    :      �  O   �      �     �                    ,     3  �  N  �   )  -   �  )   �  -   	  +   ;	  r   g	  	   �	  	   �	     �	     
     
     
  
   $
     /
     ;
     C
     K
      b
     �
     �
     �
      �
     �
     �
  r   �
  5   ]     �     �     �  	   �     �     �     �  (   �                9     S     n     t  +   �  3   �     �     �     �     �  S     )   _     �  �   �  �  s     %     ,     :     I     Z     a  �  |  �   
  -   �     �     �     �  [   �     3     ?     Q     i     q     v     �     �  
   �     �     �     �     �     �     �     �            w   ?  >   �     �                 	   ,     6     >  +   O     {  $   �  *   �  &   �     	       .   %  6   T     �     �  
   �     �  W   �  $        <  �   O               2                '   (          1      !            8                                       .               5   	   6   $      )       7   4          *                 0   
             :         ,                    +      %      "   -   /   #       9           &      3    &Amount: &Effect: &Second color: &Semi-transparent &Theme (c) 2000-2003 Geert Jansen <h1>Icons</h1>This module allows you to choose the icons for your desktop.<p>To choose an icon theme, click on its name and apply your choice by pressing the "Apply" button below. If you do not want to apply your choice you can press the "Reset" button to discard your changes.</p><p>By pressing the "Install Theme File..." button you can install your new icon theme by writing its location in the box or browsing to the location. Press the "OK" button to finish the installation.</p><p>The "Remove Theme" button will only be activated if you select a theme that you installed using this module. You are not able to remove globally installed themes here.</p><p>You can also specify effects that should be applied to the icons.</p> <qt>Are you sure you want to remove the <strong>%1</strong> icon theme?<br /><br />This will delete the files installed by this theme.</qt> <qt>Installing <strong>%1</strong> theme</qt> @label The icon rendered as activeActive @label The icon rendered as disabledDisabled @label The icon rendered by defaultDefault A problem occurred during the installation process; however, most of the themes in the archive have been installed Ad&vanced All Icons Antonio Larrosa Jimenez Co&lor: Colorize Confirmation Desaturate Description Desktop Dialogs Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Effect Parameters Gamma Geert Jansen Get new themes from the Internet Icons Icons Control Panel Module If you already have a theme archive locally, this button will unpack it and make it available for KDE applications Install a theme archive file you already have locally Main Toolbar NAME OF TRANSLATORSYour names Name No Effect Panel Preview Remove Theme Remove the selected theme from your disk Set Effect... Setup Active Icon Effect Setup Default Icon Effect Setup Disabled Icon Effect Size: Small Icons The file is not a valid icon theme archive. This will remove the selected theme from your disk. To Gray To Monochrome Toolbar Torsten Rahn Unable to download the icon theme archive;
please check that address %1 is correct. Unable to find the icon theme archive %1. Use of Icon You need to be connected to the Internet to use this action. A dialog will display a list of themes from the http://www.kde.org website. Clicking the Install button associated with a theme will install this theme locally. Project-Id-Version: kcmicons
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-08 06:27+0100
PO-Revision-Date: 2012-09-07 06:48+0000
Last-Translator: Sveinn í Felli <sveinki@nett.is>
Language-Team: Icelandic <kde-isl@molar.is>
Language: is
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: Plural-Forms: nplurals=2; plural=n != 1;


 &Magn: &Framsetning: &Seinni litur: &Hálf-gegnsætt Þ&ema (c) 2000-2003 Geert Jansen <h1>Táknmyndir</h1>Þessi eining gerir þér kleift að velja táknmyndir fyrir skjáborðið þitt.<p>Til að velja þér táknmyndaþema smelltu heiti þemans og staðfestu valið með því að smella á hnappinn "Virkja" fyrir neðan. Ef þú vilt ekki staðfesta breytingar geturðu smellt á hnappinn "Endurstilla" Til að gleyma breytingum þínum.</p><p>Með því að smella á hnappinn "Setja nýtt þema inn..." geturðu sett inn nýtt  táknmyndaþema með því að skrá staðsetningu þemunar eða flakkað að staðsetningunni. Smelltu á hnappinn "Í lagi" til að staðfesta innsetningu á þemu .</p><p>Hnappurinn "Fjarlæga þema" mun einungis verða virkur ef þú velur eitthvað þema sem þú settir sjálfur inn með þessu forriti. Þú getur ekki hent út táknmyndaþemum sem aðrir eru að nota.</p><p>Þú getur einnig stillt framsetningu og sjónbrellur táknmyndanna hér.</p> <qt>Viltu örugglega fjarlægja táknmyndaþemað <strong>%1</strong>?
<br /><br />Þá verður skránum sem því tilheyra eytt.</qt> <qt>Set inn þemað: <strong>%1</strong></qt> Virk Óvirk Sjálfgefin Vandamál komu upp við uppsetningu; þó hafa flestar þemur í safninu verið settar upp. Í&tarlegra Allar táknmyndir Antonio Larrosa Jimenez &Litur: Lita Staðfesting Afmetta Lýsing Skjáborð Samskiptagluggar Slóð að þema: ra@ra.is Viðföng framsetninga Gamma Geert Jansen Sækja nýjar þemur á netið Táknmyndir Stjórneining fyrir táknmyndir Ef þú átt þemasafnskrá geymda á tölvunni, mun þessi hnappur afpakka henni og gera aðgengilega fyrir KDE forrit Setja upp þemasafnskrá sem þú ert þegar með á tölvunni Aðaltækjaslá Richard Allen Heiti Engin framsetning Spjaldið Forsýn Fjarlægja þema Fjarlægja valið þema af disknum þínum. Stilla framsetningu... Stilla virka framsetningu táknmynda Stilla sjálfgefna framsetningu táknmynda Stilla óvirka framsetningu táknmynda Stærð: Litlar táknmyndir Þessi skrá er ekki gilt táknmyndaþemusafn. Þetta mun fjarlægja valið þema af disknum þínum. Yfir í grátt Svart/hvítt Tækjaslá Torsten Rahn Get ekki sótt táknmyndaþemasafnið;
athugðu hvort slóðin %1 sé örugglega rétt. Finn ekki táknmyndaþemusafnið %1. Notkun táknmyndar Þú verður að vera tengd(ur) við internetið til að nota þessa aðgerð. Valmynd mun sýna lista yfir þemur frá http://www.kde.org vefsvæðinu. Með því að smella á innsetningarhnappinn verður þeman sett upp á tölvunni þinni. 