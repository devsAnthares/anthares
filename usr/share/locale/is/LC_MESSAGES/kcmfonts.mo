��    #      4  /   L           	      L     /   l     �     �     �     �     �     �     
           /  I   @     �  *   �     �     �     �  !     "   $     G  6   d  *   �     �     �     �     �     
          +     >     V     f  �  �     5
  9  ;
  O   u  /   �     �       *        B  	   Z     d  #   s     �     �  �   �     A  &   E     l     �     �     �  '   �     �  =   �  @   )     j     y  
   �     �     �     �  
   �     �     �     �                                                      	               
                                                                           "      !   #     to  <p>If you have a TFT or LCD screen you can further improve the quality of displayed fonts by selecting this option.<br />Sub-pixel rendering is also known as ClearType(tm).<br /> In order for sub-pixel rendering to work correctly you need to know how the sub-pixels of your display are aligned.</p> <p>On TFT or LCD displays a single pixel is actually composed of three sub-pixels, red, green and blue. Most displays have a linear ordering of RGB sub-pixel, some have BGR.<br /> This feature does not work with CRT monitors.</p> <p>Some changes such as DPI will only affect newly started applications.</p> A non-proportional font (i.e. typewriter font). Ad&just All Fonts... BGR Click to change all fonts Configure Anti-Alias Settings Configure... E&xclude range: Font Settings Changed Font role%1:  Force fonts DPI: Hinting is a process used to enhance the quality of fonts at small sizes. RGB Smallest font that is still readable well. Use a&nti-aliasing: Use anti-aliasingDisabled Use anti-aliasingEnabled Use anti-aliasingSystem Settings Used by menu bars and popup menus. Used by the window titlebar. Used for normal text (e.g. button labels, list items). Used to display text beside toolbar icons. Vertical BGR Vertical RGB font usageFixed width font usageGeneral font usageMenu font usageSmall font usageToolbar font usageWindow title no hintingNone no subpixel renderingNone Project-Id-Version: kcmfonts
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-24 05:54+0100
PO-Revision-Date: 2010-05-03 11:51+0000
Last-Translator: Sveinn í Felli <sveinki@nett.is>
Language-Team: Icelandic <kde-isl@molar.is>
Language: is
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: Plural-Forms: nplurals=2; plural=n != 1;



  til  <p>Ef þú ert með TFT eða LCD skjá geturðu bætt enn frekar gæði leturs með því að velja þennan möguleika.<br />Smádíla (sub-pixel) myndgerð er einnig þekkt sem ClearType(tm).<br /> Til að smádílamyndgerðin virki almennilega þarf að vita hvernig undirdílum er raðað á skjáinn.</p> <p>Á TFT og LCD skjám er hver og einn díll í rauninni samsettur úr þremur undirdílum; rauðum, grænum og bláum. Flestir framleiðendur nota línulega röðun RGB undirdíla, sumir nota BGR.<br /> Þessi möguleiki virkar ekki með CRT túbuskjám.</p> <p>Sumar breytingar eins og DPI taka aðeins gildi í nýopnuðum forritum.</p> Óhlutfallsbundið letur (t.d. ritvélarletur). S&tilla allar leturgerðir... BGR Smelltu til að breyta öllum leturgerðum Stillingar afstöllunar Stilla... Útilo&ka bil: Leturstillingum hefur verið breytt %1:  Þvinga letur PÁT: Aðlögun leturs er ferli sem er notað til að bæta útlit smágerðs leturs (laga staðsetningu miðað við dílagerð viðkomandi miðils). RGB Minnsta letrið sem samt er lesanlegt. &Nota afstöllun á letur: Slökkt Virkt Kerfisstillingar Notað í valstikum og gluggavalmyndum. Notað í titilrönd glugga. Notað fyrir venjulegan texta (t.d. hnappamiða, hlutalista). Notað til að sýna texta við hlið táknmynda á tækjastiku. Lóðrétt BGR Lóðrétt RGB Jafnbreitt Almennt Valmynd Smátt Tækjaslá Gluggatitill Engin Ekkert 