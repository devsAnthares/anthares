��    *      l  ;   �      �  A   �     �  /        2     ;     O     a     w     �     �  	   �     �  3   �  	   �     �      �  $      *   E     p  	   u  5        �     �  
   �     �          $  5   3  6   i     �  ,   �  /   �     	        O   0  Z   �  b   �  	   >  
   H  P   S     �  �  �  =   �
     �
  8   �
               *     8     L  	   ^     h     u     |  ?   �  	   �     �     �  %   �     %     -     4  0   C  (   t     �     �     �     �     �  5   
  J   @     �  +   �     �     �     �  Z   �  [   V  X   �  
          ]   '     �            )             &      $   	   %         '                                                !      
       *                     "                                                 (                 #    %1 is an external application and has been automatically launched (c) 2009, Ben Cooksley <i>Contains 1 item</i> <i>Contains %1 items</i> About %1 About Active Module About Active View About System Settings Apply Settings Author Ben Cooksley Configure Configure your system Determines whether detailed tooltips should be used Developer Dialog EMAIL OF TRANSLATORSYour emails Expand the first level automatically General config for System SettingsGeneral Help Icon View Internal module representation, internal module model Internal name for the view used Keyboard Shortcut: %1 Maintainer Mathias Soeken NAME OF TRANSLATORSYour names No views found Provides a categorized icons view of control modules. Provides a classic tree-based view of control modules. Relaunch %1 Reset all current changes to previous values Search through a list of control modulesSearch Show detailed tooltips System Settings System Settings was unable to find any views, and hence has nothing to display. System Settings was unable to find any views, and hence nothing is available to configure. The settings of the current module have changed.
Do you want to apply the changes or discard them? Tree View View Style Welcome to "System Settings", a central place to configure your computer system. Will Stephenson Project-Id-Version: systemsettings
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:08+0100
PO-Revision-Date: 2016-04-08 22:57+0000
Last-Translator: Sveinn í Felli <sv1@fellsnet.is>
Language-Team: Icelandic <translation-team-is@lists.sourceforge.net>
Language: is
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: Plural-Forms: nplurals=2; plural=n != 1;


X-Generator: Lokalize 1.5
 %1 er utanaðkomandi forrit og hefur verið ræst sjálfvirkt (c) 2009, Ben Cooksley <i>Inniheldur 1 atriði</i> <i>Inniheldur %1 atriði</i> Um %1 Um virka einingu Um virka sýn Um Kerfisstillingar Virkja stillingar Höfundur Ben Cooksley Stilla Stilla kerfið Skilgreinir hvort ítarlegar áhaldavísbendingar séu notaðar Forritari Samskiptagluggi ra@ra.is, sv1@fellsnet.is Fletta sjálfkrafa út fyrsta stiginu Almennt Hjálp Táknmyndasýn Framsetning innri eininga, hönnun innri eininga Innra heiti á sýninni sem er í notkun Flýtilyklasamsetning: %1 Umsjónarmaður Mathias Soeken Richard Allen, Sveinn í Felli Engar sýnir fundust Sýnir flokkaða táknmyndasýn með stjórneiningum. Gefur færi á klassískri greina-uppbyggðri framsetningu stjórneininga. Endurræsa %1 Endurstilla allar breytingar á fyrri gildi Leita Sýna ítarlegar smábendingar Kerfisstillingar Kerfisstillingaforritið fann engar sýnir, og þar með er ekkert sem hægt er að birta. Kerfisstillingaforritið fann engar sýnir, og þar með er ekkert sem hægt er að stilla. Það eru breytingar í einingunni sem nú er virk.
Viltu virkja þær eða henda þeim? Sýna tré Skoðunarstíll: Velkomin(n) í "Kerfisstillingar", miðlægan stað til að stilla virkni tölvunnar þinnar. Will Stephenson 