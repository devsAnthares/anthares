��    B      ,  Y   <      �  i   �  m        y  b   �     �     	  6        O  7   _     �     �  	   �     �  (   �  )   �  )   (     R     o  Y   u     �     �  �   �      y	  .   �	     �	  
   �	     �	     �	     
     
     !
     5
     B
     J
     c
     r
     w
     �
     �
     �
     �
     �
  	   �
  
   �
     �
     �
  	     
     
   *     5     B  	   `     j     o  
   �  �   �     (  8   8  �   q     �  8   	  ,   B  ,   o  %   �     �  �  �  \   �  z        �  X   �     �       4   %     Z  4   i     �     �     �  $   �  -   �  -     -   =  #   k     �  W   �     �       `        r  .   �     �     �      �     �          -     ;     N     ]     d     �     �     �     �     �     �     �     �               "     /     I     Y     h     t  #   �     �     �     �  	   �  �   �     ~  -   �  }   �     8  4   G  6   |  6   �      �          $   !   %          ;   ,          B                 8   #                 7   9       +              )   ?   -   <         6                                    *   2       >                1       @       .   5   3   :   A         &      /                4              0      "             =   (   '         	   
           @info User changed Phonon backendTo apply the backend change you will have to log out and back in again. A list of Phonon Backends found on your system.  The order here determines the order Phonon will use them in. Apply Device List To... Apply the currently shown device preference list to the following other audio playback categories: Audio Hardware Setup Audio Playback Audio Playback Device Preference for the '%1' Category Audio Recording Audio Recording Device Preference for the '%1' Category Backend Colin Guthrie Connector Copyright 2006 Matthias Kretz Default Audio Playback Device Preference Default Audio Recording Device Preference Default Video Recording Device Preference Default/Unspecified Category Defer Defines the default ordering of devices which can be overridden by individual categories. Device Configuration Device Preference Devices found on your system, suitable for the selected category.  Choose the device that you wish to be used by the applications. EMAIL OF TRANSLATORSYour emails Failed to set the selected audio output device Front Center Front Left Front Left of Center Front Right Front Right of Center Hardware Independent Devices Input Levels Invalid KDE Audio Hardware Setup Matthias Kretz Mono NAME OF TRANSLATORSYour names Phonon Configuration Module Playback (%1) Prefer Profile Rear Center Rear Left Rear Right Recording (%1) Show advanced devices Side Left Side Right Sound Card Sound Device Speaker Placement and Testing Subwoofer Test Test the selected device Testing %1 The order determines the preference of the devices. If for some reason the first device cannot be used Phonon will try to use the second, and so on. Unknown Channel Use the currently shown device list for more categories. Various categories of media use cases.  For each category, you may choose what device you prefer to be used by the Phonon applications. Video Recording Video Recording Device Preference for the '%1' Category  Your backend may not support audio recording Your backend may not support video recording no preference for the selected device prefer the selected device Project-Id-Version: kcm_phonon
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-04-08 22:57+0000
Last-Translator: Sveinn í Felli <sv1@fellsnet.is>
Language-Team: Icelandic <translation-team-is@lists.sourceforge.net>
Language: is
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: Plural-Forms: nplurals=2; plural=n != 1;






X-Generator: Lokalize 1.5
 Til að breyting á bakvinnslukerfi verði virk þarftu að skrá þig út og svo inn aftur. Listi með þeim Phonon bakendum sem fundist í vélinni þinni. Röðin hér sýnir í hvaða röð Phonon mun nota þá. Beita tækjalistanum á... Beita þessum lista yfir stillingar tækja á aðra eftirfarandi flokka hljóðspilunar: Uppsetning hljóðtækja Afspilun hljóðs Stillingar hljóðspilunartækis fyrir '%1' flokkinn Hljóðupptaka Stillingar hljóðupptökutækis fyrir '%1' flokkinn Bakendi Colin Guthrie Tenging Höfundarréttur 2006 Matthias Kretz Sjálfgefnar stillingar hljóðspilunartækis Sjálfgefnar stillingar hljóðupptökutækis Sjálfgefnar stillingar vídeóupptökutækis Sjálfgefinn/óskilgreindur flokkur Forðast Skilgreinir sjálfgefna röð tækja sem getur síðan verið breytt af hverjum flokki. Stillingar tækis Afköst tækis Hljóðtæki sem fundust á vélinni þinni.  Veldu það tæki sem þú vilt að forritin noti. ra@ra.is, sv1@fellsnet.is Mistókst að setja valið hljóðúttakstæki Framan fyrir miðju Framan vinstri Framan vinstra megin við miðju Framan hægri Framan hægra megin við miðju Vélbúnaður Sjálfstæð tæki Inntaksstyrkur Ógilt Uppsetning hljóðtækja í KDE Matthias Kretz Einóma Richard Allen, Sveinn í Felli Stillingaeining Phonon Afspilun (%1) Hygla (gefa forgang) Snið Aftan miðja Aftan vinstri Aftan hægri Upptaka (%1) Sýna ítarlegra um tæki Hliðar vinstri Hliðar hægri Hljóðkort Hljóðtæki Staðsetning og prófanir hátalara Bassabox Prófa Prófa valið tæki Prófa %1 Röðin stýrir hvernig þú vilt nota tækin. Ef það er einhverra hluta vegna ekki hægt að nota fyrsta tækið mun Phonon reyna að nota næsta og svo koll af kolli. Óþekkt rás Nota þennan tækjalista fyrir fleiri flokka: Ýmsar gerðir notkunar á margmiðlun. Fyrir hvern flokk getur þú valið hvaða hljóðtæki Phonon forrit ættu að nota. Vídeóupptaka Stillingar vídeóupptökutækis fyrir '%1' flokkinn Bakvinnslukerfið styður liklega ekki hljóðupptöku Bakvinnslukerfið styður liklega ekki vídeóupptöku ekkert valið fyrir þetta tæki hyggla völdu tæki 