��          �   %   �      P     Q     b     w     �     �     �  
   �     �     �     �     �     �  /        3     Q     p     v     �     �     �     �     �     �     �       �  %     �     �             	   '     1     B     I     Q     ]  	   e     o  0   �     �     �     �     �     �               6     P     k     �     �                            
                                                                                             	    %1 file %1 files %1 folder %1 folders %1 of %2 processed %1 of %2 processed at %3/s %1 processed %1 processed at %2/s Appearance Behavior Cancel Clear Configure... Finished Jobs List of running file transfers/jobs (kuiserver) Move them to a different list Move them to a different list. Pause Remove them Remove them. Resume Show all jobs in a list Show all jobs in a list. Show all jobs in a tree Show all jobs in a tree. Show separate windows Show separate windows. Project-Id-Version: kuiserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2011-09-19 19:22+0000
Last-Translator: Sveinn í Felli <sveinki@nett.is>
Language-Team: Icelandic <kde-isl@molar.is>
Language: is
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: Plural-Forms: nplurals=2; plural=n != 1;


X-Generator: KBabel 1.11.4
 %1 skrá %1 skrár %1 mappa %1 möppur %1 af %2 lokið %1 af %2 unnin á %3/s %1 lokið %1 unnin á %2/s Útlit Hegðun Hætta við Hreinsa Stilla... Verk sem er lokið Listi yfir verk/flutninga í keyrslu (kuiserver) Færa þau í annan lista Færa þau í annan lista. Bíða Fjarlægja þau Fjarlægja þau. Halda áfram Sýna öll verk í lista Sýna öll verk í lista. Sýna öll verk í greinum Sýna öll verk í greinum. Sýna aðskilda glugga Sýna aðskilda glugga. 