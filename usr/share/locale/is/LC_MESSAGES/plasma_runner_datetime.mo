��          \      �       �      �   -   �        -   +  #   Y  #   }     �  �  �     \  1   z     �  +   �  
   �     �                                            Displays the current date Displays the current date in a given timezone Displays the current time Displays the current time in a given timezone Note this is a KRunner keyworddate Note this is a KRunner keywordtime Today's date is %1 Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2013-04-19 21:49+0000
Last-Translator: Sveinn í Felli <sveinki@nett.is>
Language-Team: Icelandic <kde-isl@molar.is>
Language: is
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: Plural-Forms: nplurals=2; plural=n != 1;
 Sýnir núverandi dagsetningu Sýnir núverandi dagsetningu í gefnu tímabelti Sýnir núverandi tíma Sýnir núverandi tíma í gefnu tímabelti dagsetning tími Í dag er %1 