��          �      L      �  m   �     /  "   <  F   _  F   �  F   �  J   4  N     R   �     !  %   2  $   X  #   }  $   �  %   �  &   �  �        �  �  �     {     �  A   �  k   �  `   Z  b   �  j   	  n   �	  j   �	     c
     v
     }
     �
     �
     �
     �
     �
     �
                                
                          	                                          Close the encrypted container; partitions inside will disappear as they had been unpluggedLock the container Eject medium Finds devices whose name match :q: Lists all devices and allows them to be mounted, unmounted or ejected. Lists all devices which can be ejected, and allows them to be ejected. Lists all devices which can be mounted, and allows them to be mounted. Lists all devices which can be unmounted, and allows them to be unmounted. Lists all encrypted devices which can be locked, and allows them to be locked. Lists all encrypted devices which can be unlocked, and allows them to be unlocked. Mount the device Note this is a KRunner keyworddevice Note this is a KRunner keywordeject Note this is a KRunner keywordlock Note this is a KRunner keywordmount Note this is a KRunner keywordunlock Note this is a KRunner keywordunmount Unlock the encrypted container; will ask for a password; partitions inside will appear as they had been plugged inUnlock the container Unmount the device Project-Id-Version: plasma_runner_solid
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2009-12-24 06:59-0500
Last-Translator: Kevin Scannell <kscanne@gmail.com>
Language-Team: Irish <gaeilge-gnulinux@lists.sourceforge.net>
Language: ga
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=5; plural=n==1 ? 0 : n==2 ? 1 : n<7 ? 2 : n < 11 ? 3 : 4
 Cuir an coimeádán faoi ghlas Díchuir an meán Aimsigh gléasanna a bhfuil ainm orthu atá comhoiriúnach do :q: Taispeánann sé seo gach gléas agus ceadaíonn sé duit iad a fheistiú, a dhífheistiú, nó a dhíchur. Taispeánann sé seo gach gléas is féidir a dhíchur, agus ceadaíonn sé duit iad a dhíchur. Taispeánann sé seo gach gléas is féidir a fheistiú, agus ceadaíonn sé duit iad a fheistiú. Taispeánann sé seo gach gléas is féidir a dhífheistiú, agus ceadaíonn sé duit iad a dhífheistiú. Taispeánann sé seo gach gléas is féidir a chur faoi ghlas, agus ceadaíonn sé duit iad a chur faoi ghlas. Taispeánann sé seo gach gléas is féidir a dhíghlasáil, agus ceadaíonn sé duit iad a dhíghlasáil. Feistigh an gléas gléas díchuir glasáil feistigh díghlasáil dífheistigh Díghlasáil an coimeádán Dífheistigh an gléas 