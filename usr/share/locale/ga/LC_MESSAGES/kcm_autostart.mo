��          �      �           	                /     =     I  /   Q     �     �      �  
   �     �     �     �  P   �     P     \     o     w     ~     �  !   �  �  �     �     �     �     �     �     �  5   �     (     F     T     f     s     �     �  S   �     �     �               #  
   0     ;                           	                                                    
                         &Properties... &Remove Add Program... Add Script... Advanced... Command Copyright © 2006–2010 Autostart Manager team Create as symlink Desktop File EMAIL OF TRANSLATORSYour emails Maintainer Montel Laurent NAME OF TRANSLATORSYour names Name Only files with “.sh” extensions are allowed for setting up the environment. Script File Shell script path: Startup Status Stephen Leaf The program will be runEnabled The program won't be runDisabled Project-Id-Version: kdereview/kcm_autostart.po
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-09 07:18+0200
PO-Revision-Date: 2008-03-22 08:52-0500
Last-Translator: Kevin Scannell <kscanne@gmail.com>
Language-Team: Irish <gaeilge-gnulinux@lists.sourceforge.net>
Language: ga
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=5; plural=n==1 ? 0 : n==2 ? 1 : n<7 ? 2 : n < 11 ? 3 : 4
 &Airíonna... &Bain Cuir Ríomhchlár Leis... Cuir Script Leis... Casta... Ordú © 2006-2010 Foireann Bhainisteoir Uath-Thosaithe KDE Cruthaigh mar nasc siombalach Comhad Deisce kscanne@gmail.com Cothaitheoir Montel Laurent Kevin Scannell Ainm Ní cheadaítear ach comhaid leis an iarmhír '.sh' chun an timpeallacht a shocrú. Comhad Scripte Conair na scripte blaoisce: Tosú Stádas Stephen Leaf Cumasaithe Díchumasaithe 