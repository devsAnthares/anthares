��          �   %   �      `     a     f     |     �  /   �  ,   �  N   �  
   ?      J  	   k     u     �     �     �  
   �     �     �     �  &   �  %   %  f   K     �     �     �     �  C     �  R  
   !     ,     L     h  3   q  	   �  U   �            	   &     0     N     ]     y     �     �     �     �  .   �  +   	  S   :	     �	  (   �	  /   �	  &   �	  `   "
                                                                                       
                       	               min AC Adapter Plugged In Activity Manager After All pending suspend actions have been canceled. Brightness level, label for the sliderLevel Could not connect to battery interface.
Please check your system configuration Do nothing EMAIL OF TRANSLATORSYour emails Hibernate Lock screen NAME OF TRANSLATORSYour names On Profile Load On Profile Unload Run script Running on Battery Power Script Switch off after The power adaptor has been plugged in. The power adaptor has been unplugged. The profile "%1" has been selected, but it does not exist.
Please check your PowerDevil configuration. Turn off screen Unsupported suspend method When laptop lid closed When power button pressed Your battery level is critical, save your work as soon as possible. Project-Id-Version: kdebase/powerdevil.po
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-03 03:17+0100
PO-Revision-Date: 2009-01-20 10:11-0500
Last-Translator: Kevin Scannell <kscanne@gmail.com>
Language-Team: Irish <gaeilge-gnulinux@lists.sourceforge.net>
Language: ga
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=5; plural=n==1 ? 0 : n==2 ? 1 : n<7 ? 2 : n < 11 ? 3 : 4
  nóiméad Cuibheoir SA Plugáilte Isteach Bainisteoir Gníomhaíochta Tar éis Cealaíodh gach gníomh fionraíochta ar feitheamh. Leibhéal Níorbh fhéidir ceangal le comhéadan an chadhnra.
Seiceáil cumraíocht do chórais Ná déan faic kscanne@gmail.com Geimhrigh Cuir an scáileán faoi ghlas Kevin Scannell Ag am luchtú na próifíle Ag am dhíluchtú na próifíle Rith script Ar Chumhacht Chadhnra Script Múch an ríomhaire tar éis Tá an cuibheoir cumhachta plugáilte isteach. Bhí an cuibheoir cumhachta díphlugáilte. Roghnaíodh próifíl "%1", ach níl sé ann.
Seiceáil an chumraíocht PowerDevil. Múch an scáileán Modh fionraíochta nach dtacaítear leis Nuair a dhúntar clúdach an ríomhaire glúine Nuair a bhrúitear an cnaipe cumhachta Shroich do chadhnra leibhéal criticiúil: sábháil do chuid oibre chomh luath agus is féidir. 