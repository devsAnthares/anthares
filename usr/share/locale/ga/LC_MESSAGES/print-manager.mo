��    O      �  k         �  +   �     �  9     $   ;     `     x     �     �     �     �  "   �  )        =     E     N     [     i     w  	        �     �  
   �     �  	   �     �  	   �     �     �     �     �     	     	     	     ,	      4	     U	     d	  U   k	  U   �	     
     
     !
  ?   (
  	   h
     r
     z
     �
     �
     �
     �
     �
  	   �
     �
     �
     �
     �
                     '     0     <     C     R     Y     `     g     l     u     �     �     �  	   �  	   �     �     �     �     �  �  �  /   �      �  2   �          (     /  
   5     @     I     O  !   a  0   �  
   �     �     �     �     �        	          	        %     1     I  	   W     a  	   j     t  
   }     �     �     �  
   �     �     �     �     �  Y   �  _   Q     �  
   �  
   �  >   �            !        <     O     ^     c     i     r     ~     �  
   �     �     �  	   �     �     �                    (  	   -     7     >     D     R     b     j     �     �     �     �  	   �  	   �     �     I   7             G   :              @      +          ,   C   
      2           '          H                  -       &         J   A       "   0   /   L   (   N               $   )       #          !      3      ;       B   ?   =       8       K          >   M   D   6       .      5          9          	   1      O   *      4       E             F                <               %            '%1' has been added, using the '%2' driver. '%1' is ready for printing. @action:intoolbarShare printers connected to this system @label location of printerLocation: @label:listboxMembers: @label:spinboxPort: @label:textboxDescription: @label:textboxLocation: @label:textboxName: @label:textboxUsername: A printer connected to a USB port. A printer connected to the parallel port. Aborted Address: All Printers Allowed Users Authenticated Banners Bluetooth Cancel Canceled Classified Clean Print Heads Completed Confidential Configure Connection: Connections Created Current status Daniel Nicoletti Default Description: Driver: EMAIL OF TRANSLATORSYour emails Ending Banner: Failed HPLIP software driving a fax machine, or the fax function of a multi-function device. HPLIP software driving a printer, or the printer function of a multi-function device. HTTP Idle In use Local printer detected by the Hardware Abstraction Layer (HAL). Location: Members Missing printer driver Modify Printer NAME OF TRANSLATORSYour names Name Name: None Not found Pages Pending Policies Print Test Page Print settings Printer Printer Options Printers Printing... Queue: Remove Printer Resume Search Secret Size Standard Starting Banner: Status Status Message Stopped Test Page TextLabel USB Unclassified Unknown User Project-Id-Version: add-printer
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-12 03:00+0200
PO-Revision-Date: 2012-07-31 09:30-0500
Last-Translator: Kevin Scannell <kscanne@gmail.com>
Language-Team: Irish <gaeilge-gnulinux@lists.sourceforge.net>
Language: ga
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=5; plural=n==1 ? 0 : n==2 ? 1 : n<7 ? 2 : n < 11 ? 3 : 4
 Cuireadh '%1' leis, ag úsáid tiománaí '%2'. Tá '%1' réidh don phriontáil. Comhroinn printéir ceangailte leis an gcóras seo Suíomh: Baill: Port: Cur Síos: Suíomh: Ainm: Ainm Úsáideora: Printéir ceangailte le port USB. Printéir ceangailte leis an bport comhuaineach. Tobscortha Seoladh: Gach Printéir Úsáideoirí ceadaithe Fíordheimhnithe Meirgí Bluetooth Cealaigh Cealaithe Faoi Cheilt Glan Cnogaí Priontála Críochnaithe Faoi Rún Cumraigh Ceangail: Ceangail Cruthaithe Stádas reatha Daniel Nicoletti Réamhshocrú Cur Síos: Tiománaí: kscanne@gmail.com Meirge Deiridh: Teipthe Bogearraí HPLIP ag tiomáint gléis facs, nó feidhmiúlacht facs de ghléas ilfheidhme. Bogearraí HPLIP ag tiomáint printéara, nó feidhmiúlacht phriontála de ghléas ilfheidhme. HTTP Díomhaoin In úsáid Printéir logánta a bhraith HAL (Hardware Abstraction Layer). Suíomh: Baill Tiománaí printéara ar iarraidh Athraigh Printéir Kevin Scannell Ainm Ainm: Neamhní Gan aimsiú Leathanaigh Ar Feitheamh Polasaithe Priontáil Leathanach Tástála Socruithe priontála Printéir Roghanna Printéara Printéirí Á Phriontáil... Ciú: Bain Printéir Lean Cuardaigh Rúnda Méid Caighdeánach Meirge Tosaigh: Stádas Teachtaireacht Stádais Stoptha Leathanach Tástála Lipéad USB Gan aicme Anaithnid Úsáideoir 