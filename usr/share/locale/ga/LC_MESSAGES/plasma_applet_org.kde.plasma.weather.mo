��          �   %   �      P     Q  '   h  "   �     �  
   �     �     �     �  ?        T     Z     h  &   |      �  %   �     �  >        C     b  '   y  !   �     �     �       3      �  T     &  "   )     L     Y     m     z     �     �     �     �     �     �     �     �     �               (     4     9     F     [     n     w     �              
             	                                                                                                  Degree, unit symbol° Forecast period timeframe1 Day %1 Days High & Low temperatureH: %1 L: %2 High temperatureHigh: %1 Kilometers Low temperatureLow: %1 Miles Short for no data available- Shown when you have not set a weather providerPlease Configure Units Update every: Wind conditionCalm content of water in airHumidity: %1%2 distance, unitVisibility: %1 %2 ground temperature, unitDewpoint: %1 humidex, unitHumidex: %1 pressure tendency, rising/falling/steadyPressure Tendency: %1 pressure, unitPressure: %1 %2 temperature, unit%1%2 visibility from distanceVisibility: %1 weather warningsWarnings Issued: weather watchesWatches Issued: wind direction, speed%1 %2 %3 windchill, unitWindchill: %1 winds exceeding wind speed brieflyWind Gust: %1 %2 Project-Id-Version: plasma_applet_weather.po
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-28 05:50+0100
PO-Revision-Date: 2009-02-03 08:48-0500
Last-Translator: Kevin Scannell <kscanne@gmail.com>
Language-Team: Irish <gaeilge-gnulinux@lists.sourceforge.net>
Language: ga
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=5; plural=n==1 ? 0 : n==2 ? 1 : n<7 ? 2 : n < 11 ? 3 : 4
 ° %1 Lá %1 Lá %1 Lá %1 Lá %1 Lá A: %1 Í: %2 Teocht Is Airde: %1 Ciliméadair Teocht Is Ísle: %1 Mílte - Cumraigh, le do thoil Aonaid Nuashonraigh gach: Ciúin Bogthaise: %1%2 Léargas: %1 %2 Drúchtphointe: %1 Taisinnéacs: %1 Claonadh an Bhrú: %1 Brú: %1 %2 %1%2 Léargas: %1 Rabhaidh a Eisíodh: Fairí a Eisíodh: %1 %2 %3 Gaothfhuarú: %1 Séideán Gaoithe: %1 %2 