��          �      \      �     �  �   �  m   r  `   �     A     N     f     s           �     �  '   �     �               %  ?   2  Y   r  +   �  �  �     �  �   �  �     r     	   w  "   �     �  	   �  '   �  !   �     		  )   	  %   D	     j	     o	     �	  K   �	  [   �	  3   :
                                                             	                               
       (c) 2003-2007 Fredrik Höglund <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Cursor Theme Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Fredrik Höglund Get new color schemes from the Internet NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. Project-Id-Version: kcminput
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2004-12-03 14:52-0500
Last-Translator: Kevin Scannell <kscanne@gmail.com>
Language-Team: Irish <gaeilge-gnulinux@lists.sourceforge.net>
Language: ga
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.0beta1
Plural-Forms: nplurals=5; plural=n==1 ? 0 : n==2 ? 1 : n<7 ? 2 : n < 11 ? 3 : 4
 © 2003-2007 Fredrik Höglund <qt>An bhfuil tú cinnte gur mian leat téama cúrsóra <i>%1</i> a bhaint?<br />Scriosfar gach comhad suiteáilte ag an téama seo.</qt> <qt>Níl cead agat an téama atá in úsáid faoi láthair a scriosadh.<br />Ní mór duit an téama a athrú i dtosach báire.</qt> Tá téama darb ainm %1 i bhfillteáin do théamaí deilbhíní. An bhfuil fonn ort an ceann seo a chur ina ionad? Deimhniú Athraíodh Socruithe an Chúrsóra Téama Cúrsóra Cur Síos Tarraing nó Clóscríobh URL an Téama seoc@iolfree.ie,kscanne@gmail.com Fredrik Höglund Faigh scéimeanna nua datha ón Idirlíon Séamus Ó Ciardhuáin,Kevin Scannell Ainm Forscríobh an Téama? Bain Téama Ní cartlann bhailí téama cúrsóra é an comhad %1 de réir cosúlachta. Ní féidir cartlann téama cúrsóra a íosluchtú; bí cinnte go bhfuil seoladh %1 ceart. Ní féidir cartlann téama cúrsóra %1 a aimsiú. 