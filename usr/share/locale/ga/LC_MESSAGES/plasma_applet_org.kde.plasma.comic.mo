��          �   %   �      @     A     S  6   b  A   �     �     �  
   �     �               "     /     7     C     O     f     {     �     �     �     �     �     �  
   �  �    #   �          %     2     N     T     Y     ^     x     �     �     �     �     �  '   �  '   �     !     <      B     c  6   p  $   �     �  
   �                                       	                               
                                                        &Save Comic As... &Strip Number: @option:check Context menu of comic image&Actual Size @option:check Context menu of comic imageStore current &Position Advanced All Appearance Check for new comic strips: Comic Comic cache: Destination: General Go to Strip Information Jump to &current Strip Jump to &first Strip Jump to Strip ... Range: Strip identifier: Update Visit the comic website Visit the shop &website an abbreviation for Number# %1 dd.MM.yyyy Project-Id-Version: playground-base/plasma_applet_comic.po
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-07-18 03:20+0200
PO-Revision-Date: 2007-11-16 11:40-0500
Last-Translator: Kevin Scannell <kscanne@gmail.com>
Language-Team: Irish <gaeilge-gnulinux@lists.sourceforge.net>
Language: ga
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=5; plural=n==1 ? 0 : n==2 ? 1 : n<7 ? 2 : n < 11 ? 3 : 4
 &Sábháil an Stiallchartún Mar... &Uimhir Stiallchartúin: &Fíormhéid Stóráil an &tIonad reatha Casta Uile Cuma Lorg stiallchartúin nua: Stiallchartún Taisce stiallchartún: Sprioc: Ginearálta Téigh go Stiallchartún Eolas Léim go dtí an Stiallchartún &reatha Léim go dtí an &chéad Stiallchartún Léim go Stiallchartún... Raon: Aitheantóir an stiallchartúin: Nuashonraigh Déan cuairt ar shuíomh Gréasáin an stiallchartúin Déan cuairt ar an siopa &Gréasáin # %1 MM.dd.yyyy 