��          �      <      �  
   �     �  >   �            
        $     ,     4     ;  	   G     Q     _     f  2   u     �     �  �  �     �  
   �  G   �                  
   '     2     ;     G     Z     k  	   ~     �  E   �     �     �                   
                                                                          	        &Browse... &Search: *.png *.xpm *.svg *.svgz|Icon Files (*.png *.xpm *.svg *.svgz) Actions Applications Categories Devices Emblems Emotes Icon Source Mimetypes O&ther icons: Places S&ystem icons: Search interactively for icon names (e.g. folder). Select Icon Status Project-Id-Version: kdelibs/kio4.po
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:11+0100
PO-Revision-Date: 2004-12-03 14:52-0500
Last-Translator: Kevin Scannell <kscanne@gmail.com>
Language-Team: Irish <gaeilge-gnulinux@lists.sourceforge.net>
Language: ga
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 0.9.6
Plural-Forms: nplurals=5; plural=n==1 ? 0 : n==2 ? 1 : n<7 ? 2 : n < 11 ? 3 : 4
 &Brabhsáil... &Cuardach: *.png *.xpm *.svg *.svgz|Comhaid Deilbhíní (*.png *.xpm *.svg *.svgz) Gníomhartha Feidhmchláir Catagóirí Gléasanna Feathail Straoiseoga Foinse Deilbhíní Cineálacha MIME Deilbhíní &eile: Áiteanna D&eilbhíní an chórais: Cuardach idirghníomhach ar ainmneacha deilbhíní (m.sh. fillteán). Roghnaigh Deilbhín Stádas 