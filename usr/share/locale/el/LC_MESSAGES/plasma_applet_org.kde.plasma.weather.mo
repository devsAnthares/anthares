��    J      l  e   �      P     Q  ^   V  H   �     �               4     <  '   K     s  "   �     �     �     �  
   �     �            	        (     @     V     \     o     ~  "   �     �     �  	   �     �     �  ?   �     4	     A	     G	     U	     a	     q	     �	  ;   �	  &   �	      �	  %   
     ;
     U
     o
     �
  >   �
     �
     �
  '     !   >     `     �     �     �     �     �     �     �                #     5     H     [     m     ~     �     �     �     �  3   �  �  %     �     �     �     �          &     )     B     W     t     �     �  (   �     �     �  '   �     '     B     R     f  .   w  
   �     �     �     �  M         N     g     i     u     �  !   �     �     �     �     �  +     
   0     ;     Z     c     x  '   �  #   �     �     �     �          '     9     >  :   U  :   �     �     �     �     �     �     �     �     �     �     �     �                              #  
   ,     7     S     '   H   8                            >   5   B           .   6   !      +       -   4       2         "       C         %       9       0   7   @      )         
   G       :         F              E   *      /   =               &                                #      A         J   ,       1      (   $          ?   I       <      3      	      ;       D                   min %1 is the weather condition, %2 is the temperature,  both come from the weather provider%1 %2 A weather station location and the weather service it comes from%1 (%2) Beaufort scale bft Celsius °C Degree, unit symbol° Details Fahrenheit °F Forecast period timeframe1 Day %1 Days Hectopascals hPa High & Low temperatureH: %1 L: %2 High temperatureHigh: %1 Inches of Mercury inHg Kelvin K Kilometers Kilometers per Hour km/h Kilopascals kPa Knots kt Location: Low temperatureLow: %1 Meters per Second m/s Miles Miles per Hour mph Millibars mbar N/A No weather stations found for '%1' Notices Percent, measure unit% Pressure: Search Short for no data available- Shown when you have not set a weather providerPlease Configure Temperature: Units Update every: Visibility: Weather Station Wind conditionCalm Wind speed: certain weather condition (probability percentage)%1 (%2%) content of water in airHumidity: %1%2 distance, unitVisibility: %1 %2 ground temperature, unitDewpoint: %1 humidex, unitHumidex: %1 pressure tendencyfalling pressure tendencyrising pressure tendencysteady pressure tendency, rising/falling/steadyPressure Tendency: %1 pressure, unitPressure: %1 %2 temperature, unit%1%2 visibility from distanceVisibility: %1 weather warningsWarnings Issued: weather watchesWatches Issued: wind directionE wind directionENE wind directionESE wind directionN wind directionNE wind directionNNE wind directionNNW wind directionNW wind directionS wind directionSE wind directionSSE wind directionSSW wind directionSW wind directionW wind directionWNW wind directionWSW wind direction, speed%1 %2 %3 wind speedCalm windchill, unitWindchill: %1 winds exceeding wind speed brieflyWind Gust: %1 %2 Project-Id-Version: plasma_applet_weather
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-28 05:50+0100
PO-Revision-Date: 2016-08-31 20:04+0200
Last-Translator: Dimitris Kardarakos <dimkard@gmail.com>
Language-Team: Greek <kde-i18n-el@kde.org>
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
  λεπτά %1 %2 %1 (%2) Κλίμακα Μποφόρ bft Κελσίου °C ° Λεπτομέρειες Φαρενάιτ °F 1 ημέρα %1 ημέρες Εκτοπασκάλ (hPa) Υψ: %1 Χαμ: %2 Υψηλό: %1 Ίντσες υδραργύρου (inHg) Κέλβιν K Χιλιόμετρα Χιλιόμετρα ανά ώρα km/h Κιλοπασκάλ (kPa) Κόμβοι kt Τοποθεσία: Χαμηλό: %1 Μέτρα ανά δευτερόλεπτο m/s Μίλια Μίλια ανά ώρα mph Χιλιοστά μπαρ Μη διαθέσιμη Δε βρέθηκαν μετεωρολογικοί σταθμοί για '%1' Ειδοποιήσεις % Πίεση: Αναζήτηση - Παρακαλώ ρυθμίστε Θερμοκρασία: Μονάδες Ενημέρωση κάθε: Ορατότητα: Μετεωρολογικός σταθμός Ήρεμα Ταχύτητα ανέμου: %1 (%2%) Υγρασία: %1%2 Ορατότητα: %1 %2 Σημείο υγροποίησης: %1 Δείκτης υγρασίας: %1 φθίνουσα αύξουσα σταθερή Τάση πίεσης: %1 Πίεση: %1 %2 %1%2 Ορατότητα: %1 Εμφανιζόμενες προειδοποιήσεις: Εμφανιζόμενες παρακολουθήσεις: Α ΑΒΑ ΑΝΑ Β ΒΑ ΒΒΑ ΒΒΔ ΒΔ Ν ΝΑ ΝΝΑ ΝΝΔ ΝΔ Δ ΔΒΔ ΔΝΔ %1 %2 %3 Ήρεμα Ψύχος ανέμου: %1 Ριπή ανέμου: %1 %2 