��          4      L       `   $   a   /   �   �  �   R   }  a   �                    Finds Konsole sessions matching :q:. Lists all the Konsole sessions in your account. Project-Id-Version: plasma_runner_konsolesessions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2009-04-26 12:57+0300
Last-Translator: Toussis Manolis <manolis@koppermind.homelinux.org>
Language-Team: Greek <kde-i18n-el@kde.org>
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 0.3
Plural-Forms: nplurals=2; plural=n != 1;
 Εύρεση συνεδριών Konsole που ταιριάζουν με το :q:. Εμφάνιση όλων των συνεδριών Konsole του λογαριασμού σας. 