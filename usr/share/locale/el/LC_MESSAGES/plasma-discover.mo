��    M      �  g   �      �     �     �  5   �  #   �  E   �  E   A  >   �     �  7   �          &     E     d     �     �     �     �     �     �     �     �     �     	     	     	  !   5	  F   W	     �	     �	     �	  <   �	     
     #
  *   ,
     W
     e
     m
  	   u
     
     �
      �
     �
  
   �
     �
     �
  
     F     :   X     �     �     �     �     �  8   �       	     
         +     ;     D     U     m     �     �     �     �     �     �  
   �     �          -     5     F     [  $   l  �  �     =     E  [   Y  C   �  T   �  T   N  M   �  '   �  g        �  C   �  B   �  5   &     \     s     �     �     �  2   �     �  >     -   N     |     �  (   �  S   �  �     @   �  /   �  "        1     �     �  N   �          2     D     [     v     �  b   �  M   �     H  F   Z     �     �  �   �  �   �          3     D     `     o  b   �  !   �               5     Q  "   d  5   �  >   �  /   �     ,  /   >     n       5   �     �  )   �          *  '   A  2   i  -   �  ;   �         /   $   	       H      @   L      0             J   7       .         :       &       F       "       (         A   ,                     E              >       5                     G   M   3      -   '      *                1   8   6   D   !       4   K         ;   
   I      <   =             C   9              %   )           B   2              ?            #       +    %1 (%2) <b>%1</b> by %2 <em>%1 out of %2 people found this review useful</em> <em>Tell us about this review!</em> <em>Useful? <a href='true'><b>Yes</b></a>/<a href='false'>No</a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'><b>No</b></a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'>No</a></em> @infoFetching updates @infoIt is unknown when the last check for updates was @infoNo updates @infoNo updates are available @infoShould check for updates @infoThe system is up to date @infoUpdates @infoUpdating... Accept Addons Aleix Pol Gonzalez An application explorer Apply Changes Available backends:
 Available modes:
 Back Cancel Checking for updates... Compact Mode (auto/compact/full). Could not close the application, there are tasks that need to be done. Could not find category '%1' Couldn't open %1 Delete the origin Directly open the specified application by its package name. Discard Discover Display a list of entries with a category. Extensions... Help... Install Installed Jonathan Thomas Launch List all the available backends. List all the available modes. Loading... Local package file to install More... No Updates Open Discover in a said mode. Modes correspond to the toolbar buttons. Open with a program that can deal with the given mimetype. Rating: Remove Resources for '%1' Review Reviewing '%1' Running as <em>root</em> is discouraged and unnecessary. Search in '%1'... Search... Search: %1 Search: %1 + %2 Settings Short summary... Sorry, nothing found... Specify the new source for %1 Still looking... Summary: Supports appstream: url scheme Tasks Tasks (%1%) Unable to find resource: %1 Update All Update Selected Update section nameUpdate (%1) Updates unknown reviewer updates not selected updates selected © 2010-2016 Plasma Development Team Project-Id-Version: muon-discover
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:02+0100
PO-Revision-Date: 2017-06-12 16:28+0200
Last-Translator: Dimitris Kardarakos <dimkard@gmail.com>
Language-Team: Greek <kde-i18n-el@kde.org>
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 %1 (%2) <b>%1</b> από %2 <em>%1 στα %2 άτομα βρήκαν την κριτική αυτή χρήσιμη</em> <em>Πείτε μας για αυτήν την κριτική!</em> <em>Χρήσιμη; <a href='true'><b>Ναι</b></a>/<a href='false'>Όχι</a></em> <em>Χρήσιμη; <a href='true'>Ναι</a>/<a href='false'><b>Όχι</b></a></em> <em>Χρήσιμη; <a href='true'>Ναι</a>/<a href='false'>Όχι</a></em> Ανάκτηση ενημερώσεων Άγνωστο πότε έγινε ο τελευταίος έλεγχος για ενημερώσεις Καμία ενημέρωση Δεν υπάρχουν διαθέσιμες ενημερώσεις Πρέπει να γίνει έλεγχος ενημερώσεων Το σύστημα είναι ενημερωμένο Ενημερώσεις Ενημέρωση... Αποδοχή Πρόσθετα Aleix Pol Gonzalez Ένας εξερευνητής εφαρμογών Εφαρμογή αλλαγών Διαθέσιμα συστήματα υποστήριξης:
 Διαθέσιμες λειτουργίες:
 Πίσω Ακύρωση Έλεγχος ενημερώσεων... Λειτουργία συμπτυγμένης προβολής (auto/compact/full). Αδυναμία κλεισίματος της εφαρμογής, υπάρχουν εργασίες που πρέπει να γίνουν. Αδυναμία εύρεσης της κατηγορίας '%1' Αδυναμία ανοίγματος του %1 Διαγραφή της πηγής Απευθείας άνοιγμα της σχετικής εφαρμογής με το όνομα του πακέτου της. Απόρριψη Discover Εμφανίζει μια λίστα εγγραφών με κατηγορία. Επεκτάσεις... Βοήθεια... Εγκατάσταση Εγκατεστημένο Jonathan Thomas Εκκίνηση Εμφάνιση όλων των διαθέσιμων συστημάτων υποστήριξης. Εμφάνιση όλων των διαθέσιμων λειτουργιών. Φόρτωση... Τοπικό αρχείο πακέτου για εγκατάσταση Περισσότερα... Καμία ενημέρωση Άνοιγμα του Discover στη δηλωθείσα λειτουργία. Οι λειτουργίες αντιστοιχούν σε κουμπιά της γραμμής εργαλείων. Ανοίξτε το με ένα πρόγραμμα που μπορεί να χειριστεί το δοσμένο τύπο mime. Αξιολόγηση: Αφαίρεση Πόροι για το '%1' Κριτική Κριτική '%1' Η εκτέλεση ως <em>root</em> δε συνίσταται και δε χρειάζεται. Αναζήτηση στο '%1'... Αναζήτηση... Αναζήτηση: %1 Αναζήτηση: %1 + %2 Ρυθμίσεις Σύντομη περίληψη... Δυστυχώς, δε βρέθηκε τίποτα... Καθορισμός της νέας πηγής για το %1 Ακόμα γίνεται αναζήτηση... Περίληψη: Υποστήριξη appstream: μορφή url  Εργασίες Εργασίες (%1%) Αδυναμία εύρεσης του πόρου: %1 Ενημέρωση όλων Ενημέρωση επιλεγμένων Ενημέρωση (%1) Ενημερώσεις άγνωστος αναθεωρητής μη επιλεγμένες ενημερώσεις επιλεγμένες ενημερώσεις © 2010-2016 Η ομάδα ανάπτυξης του Plasma 