��    B      ,  Y   <      �  i   �  m        y  b   �     �     	  6        O  7   _     �     �  	   �     �  (   �  )   �  )   (     R     o  Y   u     �     �  �   �      y	  .   �	     �	  
   �	     �	     �	     
     
     !
     5
     B
     J
     c
     r
     w
     �
     �
     �
     �
     �
  	   �
  
   �
     �
     �
  	     
     
   *     5     B  	   `     j     o  
   �  �   �     (  8   8  �   q     �  8   	  ,   B  ,   o  %   �     �  �  �  �   �  �   )  6     �   V  $        5  �   U     �  �   �  %   {     �     �  =   �  d   �  \   a  `   �  E        e  �   n     @  #   `    �  5   �  d   �     >     Z  5   z     �  /   �  
   �  '        -     K  /   ]     �     �  :   �  -   �          6     ?     L     b     |     �  :   �     �     �          )  6   A  "   x     �  ;   �     �  I  �     B  �   ^    �     �  �     y   �  }      O   �   A   �      $   !   %          ;   ,          B                 8   #                 7   9       +              )   ?   -   <         6                                    *   2       >                1       @       .   5   3   :   A         &      /                4              0      "             =   (   '         	   
           @info User changed Phonon backendTo apply the backend change you will have to log out and back in again. A list of Phonon Backends found on your system.  The order here determines the order Phonon will use them in. Apply Device List To... Apply the currently shown device preference list to the following other audio playback categories: Audio Hardware Setup Audio Playback Audio Playback Device Preference for the '%1' Category Audio Recording Audio Recording Device Preference for the '%1' Category Backend Colin Guthrie Connector Copyright 2006 Matthias Kretz Default Audio Playback Device Preference Default Audio Recording Device Preference Default Video Recording Device Preference Default/Unspecified Category Defer Defines the default ordering of devices which can be overridden by individual categories. Device Configuration Device Preference Devices found on your system, suitable for the selected category.  Choose the device that you wish to be used by the applications. EMAIL OF TRANSLATORSYour emails Failed to set the selected audio output device Front Center Front Left Front Left of Center Front Right Front Right of Center Hardware Independent Devices Input Levels Invalid KDE Audio Hardware Setup Matthias Kretz Mono NAME OF TRANSLATORSYour names Phonon Configuration Module Playback (%1) Prefer Profile Rear Center Rear Left Rear Right Recording (%1) Show advanced devices Side Left Side Right Sound Card Sound Device Speaker Placement and Testing Subwoofer Test Test the selected device Testing %1 The order determines the preference of the devices. If for some reason the first device cannot be used Phonon will try to use the second, and so on. Unknown Channel Use the currently shown device list for more categories. Various categories of media use cases.  For each category, you may choose what device you prefer to be used by the Phonon applications. Video Recording Video Recording Device Preference for the '%1' Category  Your backend may not support audio recording Your backend may not support video recording no preference for the selected device prefer the selected device Project-Id-Version: kcm_phonon
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2012-06-24 12:31+0200
Last-Translator: Dimitrios Glentadakis <dglent@gmail.com>
Language-Team: Greek <kde-i18n-el@kde.org>
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.4
Plural-Forms: nplurals=2; plural=n != 1;
 Για να εφαρμοστεί η αλλαγή του συστήματος υποστήριξης θα πρέπει να αποσυνδεθείτε πρώτα. Μια λίστα των συστημάτων υποστήριξης Phonon που βρέθηκαν στο σύστημά σας. Η σειρά καθορίζει και την προτεραιότητα χρήσης τους από το Phonon. Εφαρμογή λίστας συσκευών σε... Εφαρμογή της εμφανιζόμενης λίστας προτίμησης συσκευών στις ακόλουθες κατηγορίες αναπαραγωγής ήχου: Ρύθμιση υλικού ήχου Αναπαραγωγή ήχου Προτίμηση προκαθορισμένης συσκευής αναπαραγωγής ήχου για την κατηγορία '%1' Εγγραφή ήχου Προτίμηση προκαθορισμένης συσκευής εγγραφής ήχου για την κατηγορία '%1' Σύστημα υποστήριξης Colin Guthrie Σύνδεση Πνευματικά δικαιώματα 2006 Matthias Kretz Προτίμηση προκαθορισμένης συσκευής αναπαραγωγής ήχου Προτίμηση προκαθορισμένης συσκευής εγγραφής ήχου Προτίμηση προκαθορισμένης συσκευής εγγραφής βίντεο Προκαθορισμένη/Μη ορισμένη κατηγορία Κάτω Ορίζει την προκαθορισμένη σειρά των συσκευών η οποία μπορεί να αλλάξει από τις ρυθμίσεις των διάφορων κατηγοριών. Ρύθμιση συσκευής Προτίμηση συσκευής Οι συσκευές που βρέθηκαν στο σύστημά σας, κατάλληλες για την επιλεγμένη κατηγορία. Επιλέξτε τη συσκευή που επιθυμείτε να χρησιμοποιείται από τις εφαρμογές. capoiosct@gmail.com, manolis@koppermind.homelinux.org Αποτυχία ορισμού της επιλεγμένης συσκευής εξόδου ήχου Μπροστά κέντρο Μπροστά αριστερά Μπροστά αριστερά του κέντρου Μπροστά δεξιά Μπροστά δεξιά του κέντρου Υλικό Ανεξάρτητες συσκευές Στάθμες εισόδου Μη έγκυρο Ρύθμιση υλικού ήχου του KDE Matthias Kretz Μονοφωνικό Γέραλης Αντώνης, Τούσης Μανώλης Άρθρωμα ρύθμισης του Phonon Αναπαραγωγή (%1) Πάνω Προφίλ Πίσω κέντρο Πίσω αριστερά Πίσω δεξιά Εγγραφή (%1) Εμφάνιση προχωρημένων συσκευών Πλάγια αριστερά Πλάγια δεξιά Κάρτα ήχου Συσκευή ήχου Τοποθέτηση και δοκιμή ηχείου  Υποβαθύφωνο (subwoofer) Έλεγχος Δοκιμή της επιλεγμένης συσκευής Έλεγχος %1 Η σειρά καθορίζει την προτίμηση των συσκευών. Αν για κάποιο λόγο η πρώτη συσκευή δεν μπορεί να χρησιμοποιηθεί, το Phonon θα προσπαθήσει να χρησιμοποιήσει τη δεύτερη και ούτω καθ' εξής. Άγνωστο κανάλι Χρήση της εμφανιζόμενης λίστας συσκευών για περισσότερες κατηγορίες. Διάφορες κατηγορίες περιπτώσεων χρήσης μέσων. Για κάθε κατηγορία μπορείτε να επιλέξετε ποια συσκευή προτιμάτε να χρησιμοποιείται από τις εφαρμογές Phonon. Εγγραφή βίντεο Προτίμηση προκαθορισμένης συσκευής εγγραφής βίντεο για την κατηγορία '%1' Το σύστημα υποστήριξης σας μπορεί να μην υποστηρίζει εγγραφή ήχου Το σύστημα υποστήριξης σας μπορεί να μην υποστηρίζει εγγραφή βίντεο καμία προτίμηση για την επιλεγμένη συσκευή προτίμηση της επιλεγμένης συσκευής 