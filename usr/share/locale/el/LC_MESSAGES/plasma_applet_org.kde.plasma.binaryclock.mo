��    	      d      �       �   
   �      �      �   	              "     C  "   ]  �  �     8     I  +   Y  #   �  )   �  U   �  K   )  [   u                             	                 Appearance Colors: Display seconds Draw grid Show inactive LEDs: Use custom color for active LEDs Use custom color for grid Use custom color for inactive LEDs Project-Id-Version: plasma_applet_binaryclock
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-31 05:47+0100
PO-Revision-Date: 2017-05-22 17:26+0200
Last-Translator: Dimitris Kardarakos <dimkard@gmail.com>
Language-Team: Greek <kde-i18n-el@kde.org>
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 Εμφάνιση Χρώματα: Εμφάνιση δευτερολέπτων Σχεδίαση πλέγματος Εμφάνιση μη ενεργών LED: Χρήση προσαρμοσμένου χρώματος για τα ενεργά LED Χρήση προσαρμοσμένου χρώματος πλέγματος Χρήση προσαρμοσμένου χρώματος για τα μη ενεργά LED: 