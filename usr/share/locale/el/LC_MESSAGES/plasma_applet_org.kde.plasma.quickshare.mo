��          �      <      �     �     �     �  +   �  @        L     a     i     w     }     �  
   �     �     �     �     �     �  �  
     �     �  $   �  {   �  �   o  .   �     &  "   3     V  %   k  -   �     �     �  ,   �  !     6   8  I   o     
         	                                                                                       <a href='%1'>%1</a> Close Copy Automatically: Don't show this dialog, copy automatically. Drop text or an image onto me to upload it to an online service. Error during upload. General History Size: Paste Please wait Please, try again. Sending... Share Shares for '%1' Successfully uploaded The URL was just shared Upload %1 to an online service Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-01 04:04+0100
PO-Revision-Date: 2015-11-24 14:16+0200
Last-Translator: Dimitris Kardarakos <dimkard@gmail.com>
Language-Team: Greek <kde-i18n-el@kde.org>
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 <a href='%1'>%1</a> Κλείσιμο Αυτόματη αντιγραφή: Να μην εμφανίζεται αυτός ο διάλογος, να γίνεται αντιγραφή αυτόματα. Απόθεση κειμένου ή εικόνας εδώ, για την αποστολή σε μια δικτυακή υπηρεσία. Σφάλμα κατά την αποστολή. Γενικά Μέγεθος ιστορικού: Επικόλληση Παρακαλώ περιμένετε Παρακαλώ δοκιμάστε ξανά. Αποστολή... Κοινή χρήση Κοινές χρήσεις για το '%1' Επιτυχής αποστολή Μόλις έγινε κοινή χρήση του URL Αποστολή του %1 σε μια δικτυακή υπηρεσία. 