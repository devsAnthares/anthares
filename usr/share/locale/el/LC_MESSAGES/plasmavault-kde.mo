��    7      �  I   �      �  �  �  }  �  M    
   V     a     �  <   �     �     �  	          .   !  %   P     v     �     �     �     �     �     �     �     �  4        ;  9   M     �     �  �   �  !   �  t   �          *     G     T     Y     p  B   y  &   �  ?   �  '   #  )   K  7   u  .   �  5   �  +        >     [     {  ,   �     �     �  9   �  V     C   l  �  �  �  O  V  (          �%  M   �%  V   �%  �   E&  e   �&  /   E'     u'  6   �'  {   �'  X   =(  )   �(     �(     �(  2   �(  2   )     A)  J   R)     �)  D   �)  k   �)  #   T*  �   x*  +   +     <+  �  I+  T   .-    �-  #   �.  H   �.  $   /     ,/  @   ;/     |/  �   �/  h   70  �   �0  U   61  U   �1  s   �1  h   V2  �   �2  |   C3  4   �3  :   �3     04  x   N4  D   �4  +   5  �   85  �   �5     �6               -       ,      6         1      /   '      (       3      .      #      )   
          +          *   %          5          4   2   	      7                                                       $                            0   !   &          "       <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body style=" font-family:'hlv'; font-size:9pt; font-weight:400; font-style:normal;">
<p style="-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><br /></p></body></html> <b>Security notice:</b>
                             According to a security audit by Taylor Hornby (Defuse Security),
                             the current implementation of Encfs is vulnerable or potentially vulnerable
                             to multiple types of attacks.
                             For example, an attacker with read/write access
                             to encrypted data might lower the decryption complexity
                             for subsequently encrypted data without this being noticed by a legitimate user,
                             or might use timing analysis to deduce information.
                             <br /><br />
                             This means that you should not synchronize
                             the encrypted data to a cloud storage service,
                             or use it in other circumstances where the attacker
                             can frequently access the encrypted data.
                             <br /><br />
                             See <a href='http://defuse.ca/audits/encfs.htm'>defuse.ca/audits/encfs.htm</a> for more information. <b>Security notice:</b>
                             CryFS encrypts your files, so you can safely store them anywhere.
                             It works well together with cloud services like Dropbox, iCloud, OneDrive and others.
                             <br /><br />
                             Unlike some other file-system overlay solutions,
                             it does not expose the directory structure,
                             the number of files nor the file sizes
                             through the encrypted data format.
                             <br /><br />
                             One important thing to note is that,
                             while CryFS is considered safe,
                             there is no independent security audit
                             which confirms this. Activities Can not create the mount point Can not open an unknown vault. Choose the encryption system you want to use for this vault: Choose the used cypher: Close the vault Configure Configure Vault... Configured backend can not be instantiated: %1 Configured backend does not exist: %1 Correct version found Create CryFS Device is already open Device is not open Dialog Do not show this notice again EncFS Encrypted data location Failed to create directories, check your permissions Failed to execute Failed to fetch the list of applications using this vault Forcefully close  General If you limit this vault only to certain activities, it will be shown in the applet only when you are in those activities. Furthermore, when you switch to an activity it should not be available in, it will automatically be closed. Limit to the selected activities: Mind that there is no way to recover a forgotten password. If you forget the password, your data is as good as gone. Mount point Mount point is not specified Mount point: Next Open with File Manager Previous The mount point directory is not empty, refusing to open the vault The specified backend is not available The vault configuration can only be changed while it is closed. The vault is unknown, can not close it. The vault is unknown, can not destroy it. This device is already registered. Can not recreate it. This directory already contains encrypted data Unable to close the vault, an application is using it Unable to close the vault, it is used by %1 Unable to detect the version Unable to perform the operation Unknown device Unknown error, unable to create the backend. Use the default cipher Vaul&t name: Wrong version installed. The required version is %1.%2.%3 You need to select empty directories for the encrypted storage and for the mount point formatting the message for a command, as in encfs: not found%1: %2 Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-11 05:53+0100
PO-Revision-Date: 2017-09-08 16:11+0200
Last-Translator: Dimitris Kardarakos <dimkard@gmail.com>
Language-Team: Greek <kde-i18n-el@kde.org>
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body style=" font-family:'hlv'; font-size:9pt; font-weight:400; font-style:normal;">
<p style="-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><br /></p></body></html> <b>Επισήμανση ασφαλείας:</b>
                             Σύμφωνα με τον έλεγχο ασφαλείας του Taylor Hornby (Defuse Security),
                            η τρέχουσα υλοποίηση του Encfs είναι ευάλωτη ή δυνητικά ευάλωτη
                             σε διαφόρους τύπους επιθέσεων.
                             Για παράδειγμα, ένας εισβολέας με δικαιώματα εγγραφής/ανάγνωσης
                             σε κρυπτογραφημένα δεδομένα μπορεί να χαμηλώσει την πολυπλοκότητα κρυπτογράφησης
                             στα κρυπτογραφημένα δεδομένα που θα ακολουθήσουν, χωρίς αυτό να το αντιληφθεί κάποιος νόμιμος χρήστης
                             ή μπορεί να χρησιμοποιήσει χρονική ανάλυση για να συμπεράνει πληροφορίες.
                             <br /><br />
                             Αυτό σημαίνει ότι δεν πρέπει να συγχρονίζετε
                             τα κρυπτογραφημένα δεδομένα σε υπηρεσίες cloud,
                             ή να τη χρησιμοποιείτε σε άλλες συνθήκες όπου ο εισβολέας
                             μπορεί να έχει συχνά πρόσβαση στα κρυπτογραφημένα δεδομένα.
                             <br /><br />
                             Δείτε στο <a href='http://defuse.ca/audits/encfs.htm'>defuse.ca/audits/encfs.htm</a> για περισσότερες πληροφορίες. <b>Επισήμανση ασφαλείας:</b>
                             Το CryFS κρυπτογραφεί τα αρχεία σας, έτσι μπορείτε να τα αποθηκεύετε με ασφάλεια οπουδήποτε.
                             Δουλεύει σωστά σε συνδυασμό με υπηρεσίες cloud όπως Dropbox, iCloud, OneDrive και άλλες.
                             <br /><br />
                             Αντίθετα με άλλες λύσεις επιπέδου συστήματος αρχείων,
                             δε γίνεται έκθεση της δομής των καταλόγων,
                             του αριθμού και του μεγέθους των αρχείων
                             από τον κρυπτογραφημένο τύπο δεδομένων.
                             <br /><br />
                             Αξίζει επίσης να σημειωθεί ότι 
                             ενώ το CryFS θεωρείται ασφαλές,
                             δεν υπάρχει ανεξάρτητη αρχή ελέγχου
                             που να πιστοποιεί κάτι τέτοιο. Δραστηριότητες Αδυναμία δημιουργίας σημείου προσάρτησης Αδυναμία ανοίγματος άγνωστου θησαυροφυλακίου. Επιλέξτε τη μέθοδο κρυπτογράφησης που θα χρησιμοποιηθεί σε αυτό το θησαυροφυλάκιο: Καθορίστε εδώ την κρυπτογράφηση που θα χρησιμοποιηθεί: Κλείσιμο θησαυροφυλακίου Διαμόρφωση Διαμόρφωση θησαυροφυλακίου... Το διαμορφωμένο σύστημα υποστήριξης δεν μπορεί να αρχικοποιηθεί: %1 Δεν υπάρχει διαμορφωμένο σύστημα υποστήριξης: %1 Βρέθηκε η σωστή έκδοση Δημιουργία CryFS Η συσκευή είναι ήδη ανοιχτή Η συσκευή δεν είναι ανοιχτή Διάλογος Να μην εμφανιστεί ξανά αυτή η επισήμανση EncFS Κρυπτογραφημένη τοποθεσία δεδομένων Αποτυχία δημιουργίας καταλόγων, ελέγξτε τα δικαιώματά σας Αποτυχία εκτέλεσης Αποτυχία ανάκτησης της λίστας εφαρμογών που χρησιμοποιούν αυτό το θησαυροφυλάκιο Εξαναγκασμένο κλείσιμο Γενικά Αν περιορίσετε το θησαυροφυλάκιο σε συγκεκριμένες δραστηριότητες, θα εμφανίζεται στη μικροεφαρμογή μόνο όταν βρίσκεστε σε αυτές τις δραστηριότητες. Κατ' επέκταση, όταν μεταβαίνετε σε κάποια δραστηριότητα όπου δε θα έπρεπε να είναι διαθέσιμο, θα κλείνει αυτόματα. Περιορισμός σε συγκεκριμένες δραστηριότητες: Έχετε υπόψιν σας ότι δεν υπάρχει τρόπος να επαναφέρετε κάποιον ξεχασμένο κωδικό πρόσβασης. Αν ξεχάσετε τον κωδικό σας, τα δεδομένα σας δεν υπάρχουν πια. Σημείο προσάρτησης Δεν έχει καθοριστεί σημείο προσάρτησης Σημείο προσάρτησης: Επόμενο Άνοιγμα με τον διαχειριστή αρχείων Προηγούμενο Ο κατάλογος σημείου προσάρτησης δεν είναι άδειος, άρνηση ανοίγματος του θησαυροφυλακίου Το συγκεκριμένο σύστημα υποστήριξης δεν είναι διαθέσιμο Η διαμόρφωση του θησαυροφυλακίου μπορεί να τροποποιηθεί μόνο όταν είναι κλειστό. Άγνωστο θησαυροφυλάκιο, αδυναμία κλεισίματος. Άγνωστο θησαυροφυλάκιο, αδυναμία καταστροφής. Η συσκευή έχει ήδη καταχωρηθεί. Δε μπορεί να δημιουργηθεί πάλι. Αυτός ο κατάλογος περιέχει ήδη κρυπτογραφημένα δεδομένα Αδυναμία κλεισίματος θησαυροφυλακίου, το χρησιμοποιεί κάποια εφαρμογή Αδυναμία κλεισίματος θησαυροφυλακίου, το χρησιμοποιεί η εφαρμογή %1 Αδυναμία ανίχνευσης έκδοσης Αδυναμία εκτέλεσης λειτουργίας Άγνωστη συσκευή Άγνωστο σφάλμα, αδυναμία δημιουργίας του συστήματος υποστήριξης. Χρήση προεπιλεγμένης κρυπτογράφησης Όνομα &θησαυροφυλακίου: Έχει εγκατασταθεί λανθασμένη έκδοση. Η απαιτούμενη έκδοση είναι η %1.%2.%3 Πρέπει να επιλέξετε άδειους καταλόγους για τον χώρο κρυπτογραφημένης αποθήκευσης και το σημείο προσάρτησης %1: %2 