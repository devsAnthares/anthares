��          �   %   �      P     Q     b     w     �     �     �  
   �     �     �     �     �     �  /        3     Q     p     v     �     �     �     �     �     �     �       �  %     �  #   �  )     4   <     q  *   �     �     �     �     �       +     c   F  6   �  7   �  
        $     >     Y  A   j  B   �  C   �  D   3	  4   x	  5   �	                            
                                                                                             	    %1 file %1 files %1 folder %1 folders %1 of %2 processed %1 of %2 processed at %3/s %1 processed %1 processed at %2/s Appearance Behavior Cancel Clear Configure... Finished Jobs List of running file transfers/jobs (kuiserver) Move them to a different list Move them to a different list. Pause Remove them Remove them. Resume Show all jobs in a list Show all jobs in a list. Show all jobs in a tree Show all jobs in a tree. Show separate windows Show separate windows. Project-Id-Version: kuiserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2011-09-17 08:22+0200
Last-Translator: Dimitrios Glentadakis <dglent@gmail.com>
Language-Team: Greek <kde-i18n-el@kde.org>
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.2
 %1 αρχείο %1 αρχεία %1 φάκελος %1 φάκελοι Επεξεργάστηκαν %1 από %2 Επεξεργάστηκαν %1 από %2 με %3/δ Επεξεργάστηκαν %1 Επεξεργάστηκαν %1 με %2/δ Εμφάνιση Συμπεριφορά Ακύρωση Καθαρισμός Ρύθμιση... Ολοκληρωμένες εργασίες Λίστα των τρεχόντων αρχείων μεταφορών/εργασιών (kuiserver) Μετακίνησή τους σε άλλη λίστα Μετακίνησή τους σε άλλη λίστα. Παύση Αφαίρεσή τους Αφαίρεσή τους. Συνέχιση Εμφάνιση όλων των εργασιών σε λίστα Εμφάνιση όλων των εργασιών σε λίστα. Εμφάνιση όλων των εργασιών σε δέντρο Εμφάνιση όλων των εργασιών σε δέντρο. Εμφάνιση χωριστών παραθύρων Εμφάνιση χωριστών παραθύρων. 