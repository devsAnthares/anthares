��    $      <  5   \      0     1     6  )   J     t  $   �  &   �  #   �  !   �  "   !     D     T     f     z     �  J   �     �  L        [     c     k      {     �  
   �     �     �     �     �     �  "   �       )   9  <   c     �  ;   �     �  �       �     �  <   �     	  >   	  U   R	  H   �	  M   �	  K   ?
     �
  
   �
  !   �
     �
     �
  �   �
  8   �  �   �     \     k     �     �     �     �  %   �          #     0     G     P     k  J   �  �   �     R     Y     ^     	                                                                         !       #             "                                        
           $                               100% @info:creditAuthor @info:creditCopyright 2015 Harald Sitter @info:creditHarald Sitter @labelNo Applications Playing Audio @labelNo Applications Recording Audio @labelNo Device Profiles Available @labelNo Input Devices Available @labelNo Output Devices Available @labelProfile: @titlePulseAudio @title:tabAdvanced @title:tabApplications @title:tabDevices Add virtual output device for simultaneous output on all local sound cards Advanced Output Configuration Automatically switch all running streams when a new output becomes available Capture Default Device Profiles EMAIL OF TRANSLATORSYour emails Inputs Mute audio NAME OF TRANSLATORSYour names Notification Sounds Outputs Playback Port Port is unavailable (unavailable) Port is unplugged (unplugged) Requires 'module-gconf' PulseAudio module This module allows to set up the Pulseaudio sound subsystem. label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-03 03:00+0200
PO-Revision-Date: 2017-05-22 17:25+0200
Last-Translator: Dimitris Kardarakos <dimkard@gmail.com>
Language-Team: Greek <kde-i18n-el@kde.org>
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 100% Συγγραφέας Πνευματικά δικαιώματα 2015 Harald Sitter Harald Sitter Καμιά εφαρμογή δεν αναπαράγει ήχο Καμιά εφαρμογή δεν πραγματοποιεί εγγραφή ήχου Δεν υπάρχουν προφίλ συσκευών διαθέσιμα Καμία συσκευή εισόδου δεν είναι διαθέσιμη Καμία συσκευή εξόδου δεν είναι διαθέσιμη Προφίλ: PulseAudio Για προχωρημένους Εφαρμογές Συσκευές Προσθήκη εικονικής συσκευής εξόδου για ταυτόχρονη έξοδο σε όλες τις τοπικές κάρτες ήχου Προχωρημένη διαμόρφωση εξόδου Αυτόματη εναλλαγή όλων των εκτελούμενων ροών όταν είναι διαθέσιμη μια νέα έξοδος Εγγραφή Προκαθορισμένο Προφίλ συσκευών dimkard@gmail.com Είσοδοι Σίγαση ήχου Δημήτρης Καρδαράκος Ήχοι ειδοποίησης Έξοδοι Αναπαραγωγή Θύρα  (μη διαθέσιμη) (αποσυνδέθηκε) Απαιτείται το άρθρωμα 'module-gconf' του PulseAudio  Αυτό το άρθρωμα σας επιτρέπει τη ρύθμιση του υποσυστήματος ήχου Pulseaudio %1: %2 100% %1% 