��    
      l      �       �   L   �   +   >  #   j     �  P   �  	   �  (     I   -  K   w  �  �  9   b     �  D   �  I   �  7   H     �  6   �  (   �  "   �                                      	   
    %1 is name of the newly connected displayA new display %1 has been detected Disables the newly connected screenDisable Failed to connect to KScreen daemon Failed to load root object Makes the newly conencted screen a clone of the primary oneClone Primary Output No Action Opens KScreen KCMAdvanced Configuration Places the newly connected screen left of the existing oneExtend to Left Places the newly connected screen right of the existing oneExtend to Right Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2015-02-10 14:43+0200
Last-Translator: Dimitris Kardarakos <dimkard@gmail.com>
Language-Team: Greek <kde-i18n-el@kde.org>
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 Μια νέα οθόνη %1 έχει εντοπιστεί Απενεργοποίηση Αποτυχία σύνδεσης με τον δαίμονα KScreen Αποτυχία φόρτωσης ριζικού αντικειμένου Κλωνοποίηση της κύριας εξόδου Καμία ενέργεια Διαμόρφωση για προχωρημένους Επέκταση στα αριστερά Επέκταση στα δεξιά 