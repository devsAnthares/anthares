��    #      4  /   L           	               $     1     :     J  3   Z  >   �     �     �     �        m        v     �     �     �     �  *   �      �          #  "   4     W     v     �     �     �     �     �     �  ;   �     4  �  J     �     �     �          &  !   =     _     y     �  0   �     �     �     	  
   	  0   	     J	     f	     �	  !   �	  o   �	  E   "
  )   h
  !   �
     �
     �
  ,   �
          $     <     \     u     }     �     �     #                          
                                    !                 "               	                                                          % Applications Audio Muted Audio Volume Behavior Capture Devices Capture Streams Checkable switch for (un-)muting sound output.Mute Checkable switch to change the current default output.Default Decrease Microphone Volume Decrease Volume Devices General Heading for a list of ports of a device (for example built-in laptop speakers or a plug for headphones)Ports Increase Microphone Volume Increase Volume Maximum volume: Mute Mute Microphone No applications playing or recording audio No output or input devices found Playback Devices Playback Streams Port is unavailable (unavailable) Port is unplugged (unplugged) Raise maximum volume Volume Volume at %1% Volume feedback Volume step: label of device items%1 (%2) label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2017-05-22 17:24+0200
Last-Translator: Dimitris Kardarakos <dimkard@gmail.com>
Language-Team: Greek <kde-i18n-el@kde.org>
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 % Εφαρμογές Χωρίς ήχο Ένταση ήχου Συμπεριφορά Συσκευές εγγραφής Ροές εγγραφής Σίγαση Προκαθορισμένη Μείωση έντασης μικροφώνου Μείωση έντασης Συσκευές Γενικά Θύρες Αύξηση έντασης μικροφώνου Αύξηση έντασης Μέγιστη ένταση: Σίγαση Σίγαση μικροφώνου Καμιά εφαρμογή δεν πραγματοποιεί εγγραφή ή αναπαραγωγή ήχου Δε βρέθηκαν συσκευές εισόδου ή εξόδου Συσκευές αναπαραγωγής Ροές αναπαραγωγής  (μη διαθέσιμη) (αποσυνδέθηκε) Αύξηση μέγιστης έντασης Ένταση Ένταση στο %1% Ανάδραση έντασης Βήμα έντασης: %1 (%2) %1: %2 100% %1% 