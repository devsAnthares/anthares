��          �            x     y  !   �     �     �     �  @   �     *  $   ?     d     k  "   �     �  %   �     �     �  �       �  )   �  8   �     !  =   .     l  
   {  Q   �     �     �  %        ,     9     =     T                        
                                    	                   Artist of the songby %1 Artist of the songby %1 (paused) Choose player automatically General No media playing Open player window or bring it to the front if already openOpen Pause playbackPause Pause playback when screen is locked Paused Play next trackNext Track Play previous trackPrevious Track Quit playerQuit Remaining time for song e.g -5:42-%1 Start playbackPlay Stop playbackStop Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-06 03:09+0200
PO-Revision-Date: 2017-06-12 16:32+0200
Last-Translator: Dimitris Kardarakos <dimkard@gmail.com>
Language-Team: Greek <kde-i18n-el@kde.org>
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 από τον/την %1 από τον/την %1 (σε παύση) Αυτόματη επιλογή αναπαραγωγέα Γενικά Δε γίνεται αναπαραγωγή πολυμέσων Άνοιγμα Παύση Παύση αναπαραγωγής όταν η οθόνη κλειδώνεται Σε παύση Επόμενο κομμάτι Προηγούμενο κομμάτι Έξοδος -%1 Αναπαραγωγή Διακοπή 