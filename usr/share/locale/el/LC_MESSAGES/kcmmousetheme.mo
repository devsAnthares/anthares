��          �   %   �      @     A  �   `  m   �  �   P  )   �  `        o     |     �     �     �      �     �     �  '        ,     >     ]     b     s  ?   �  Y   �  +     H   F  �  �     6  �   U  �   T	  %   
  0   E
  �   v
     4  3   K          �  K   �  6   �     +  #   <  P   `  *   �  ;   �  
     *   #     N  �   n  �   �  X   �  �   �                                                 	                                                               
              (c) 2003-2007 Fredrik Höglund <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) @item:inlistbox sizeResolution dependent A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Cursor Theme Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Fredrik Höglund Get new Theme Get new color schemes from the Internet Install from File NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. You have to restart the Plasma session for these changes to take effect. Project-Id-Version: kcminput
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-09-04 14:40+0200
Last-Translator: Dimitris Kardarakos <dimkard@gmail.com>
Language-Team: Greek <kde-i18n-el@kde.org>
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 (c) 2003-2007 Fredrik Höglund <qt>Επιθυμείτε σίγουρα την αφαίρεση του θέματος δρομέα <strong>%1</strong>;<br />Αυτό θα διαγράψει όλα τα αρχεία που εγκαταστάθηκαν από αυτό το θέμα.</qt> <qt>Δεν μπορείτε να διαγράψετε το θέμα που χρησιμοποιείτε.<br />Θα πρέπει πρώτα να κάνετε εναλλαγή σε άλλο θέμα.</qt> (Διαθέσιμα μεγέθη: %1) Σε αναλογία με την ανάλυση Ένα θέμα με όνομα %1 υπάρχει ήδη στο φάκελο θεμάτων εικονιδίων σας. Θέλετε να το αντικαταστήσετε με αυτό; Επιβεβαίωση Οι ρυθμίσεις δρομέα άλλαξαν Θέμα δρομέα Περιγραφή Σύρετε ή πληκτρολογήστε το URL του θέματος sngeorgaras@otenet.gr,manolis@koppermind.homelinux.org Fredrik Höglund Λήψη νέου θέματος... Λήψη νέων θεμάτων χρώματος από το διαδίκτυο Εγκατάσταση από αρχείο Σπύρος Γεωργαράς,Τούσης Μανώλης Όνομα Αντικατάσταση θέματος; Αφαίρεση θέματος Το αρχείο %1 δε φαίνεται να είναι μια έγκυρη αρχειοθήκη θέματος δρομέα. Αδύνατη η λήψη της αρχειοθήκης θέματος δρομέα. Παρακαλώ ελέγξτε ότι η διεύθυνση %1 είναι σωστή. Αδύνατο να βρεθεί η αρχειοθήκη θέματος δρομέα %1. Πρέπει να επανεκκινήσετε τη συνεδρία του Plasma για να εφαρμοστούν αυτές οι αλλαγές. 