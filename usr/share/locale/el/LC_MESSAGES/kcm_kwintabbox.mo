��    "      ,  /   <      �  
   �               ,     >     J  !   V     x     �     �     �     �     �  L   �     #     +     J     Y     u     z     �     �     �     �  	   �     �     �     �  �   �  F   �     �     �     �  �  �     �  5   �  >   �  %   -     S     p  %   �     �  +   �  !   �  4   	     G	  2   c	  �   �	     X
  H   g
     �
  r   �
     ?     L  8   i  /   �     �     �     �  :     "   H     k    �  �   �  8   L     �     �                    !                                            	                                                           "               
                    Activities All other activities All other desktops All other screens All windows Alternative An example Desktop NameDesktop 1 Content Current activity Current application Current desktop Current screen Filter windows by Focus policy settings limit the functionality of navigating through windows. Forward Get New Window Switcher Layout Hidden windows Include "Show Desktop" icon Main Minimization Only one window per application Recently used Reverse Screens Shortcuts Show selected window Sort order: Stacking order The currently selected window will be highlighted by fading out all other windows. This option requires desktop effects to be active. The effect to replace the list window when desktop effects are active. Virtual desktops Visible windows Visualization Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2012-06-24 08:58+0200
Last-Translator: Dimitrios Glentadakis <dglent@gmail.com>
Language-Team: Greek <kde-i18n-el@kde.org>
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.4
Plural-Forms: nplurals=2; plural=n != 1;
 Δραστηριότητες Όλες οι άλλες δραστηριότητες Όλες οι άλλες επιφάνειες εργασίας Όλες οι άλλες οθόνες Όλα τα παράθυρα Εναλλακτική Επιφάνεια εργασίας 1 Περιεχόμενο Τρέχουσα δραστηριότητα Τρέχουσα εφαρμογή Τρέχουσα επιφάνεια εργασίας Τρέχουσα οθόνη Φιλτράρισμα παραθύρων κατά Οι ρυθμίσεις της πολιτικής εστίασης περιορίζουν την λειτουργικότητα της πλοήγησης μεταξύ των παραθύρων. Μπροστά Λήψη νέας διάταξης εναλλαγής παραθύρων Κρυφά παράθυρα Συμπερίληψη του εικονιδίου "Εμφάνιση της επιφάνειας εργασίας" Βασική Ελαχιστοποίηση Μόνο ένα παράθυρο ανά εφαρμογή Χρησιμοποιημένα πρόσφατα Αντιστροφή Οθόνες Συντομεύσεις Εμφάνιση επιλεγμένου παραθύρου Σειρά ταξινόμησης: Σειρά στοίβαξης Το τρέχον επιλεγμένο παράθυρο θα τονιστεί με ομαλό σβήσιμο όλων των άλλων παραθύρων. Η επιλογή απαιτεί να είναι ενεργά τα εφέ επιφάνειας εργασίας. Το εφέ που θα αντικαθιστά την λίστα των παραθύρων όταν τα εφέ της επιφάνειας εργασίας είναι ενεργά. Εικονικές επιφάνειες εργασίας Ορατά παράθυρα Οπτικοποίηση 