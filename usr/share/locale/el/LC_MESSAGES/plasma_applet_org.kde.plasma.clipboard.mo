��          �      L      �     �     �     �     �          )     1     9     P     h     t  K   �     �     �     �     �            �     7   �  '   �  )     F   I  +   �     �     �  *   �  G     !   g  /   �     �     �     �  -   �  *        F  0   Y                                                  
   	                                               Change the barcode type Clear history Clipboard Contents Clipboard history is empty. Clipboard is empty Code 39 Code 93 Configure Clipboard... Creating barcode failed Data Matrix Edit contents Indicator that there are more urls in the clipboard than previews shown+%1 Invoke action QR Code Remove from history Return to Clipboard Search Show barcode Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-10 03:07+0200
PO-Revision-Date: 2015-03-05 15:28+0200
Last-Translator: Dimitris Kardarakos <dimkard@gmail.com>
Language-Team: Greek <kde-i18n-el@kde.org>
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 Αλλαγή τύπου γραμμωτού κώδικα Καθαρισμός ιστορικού Περιεχόμενα προχείρου Το ιστορικό του προχείρου είναι άδειο. Το πρόχειρο είναι άδειο Κωδικοποίηση 39 Κωδικοποίηση 93 Διαμόρφωση πρόχειρου... Αποτυχία δημιουργίας γραμμωτού κώδικα Πίνακας δεδομένων Επεξεργασία περιεχομένων +%1 Κλήση ενέργειας Κώδικας QR Αφαίρεση από το ιστορικό Επιστροφή στο πρόχειρο Αναζήτηση Εμφάνιση γραμμωτού κώδικα 