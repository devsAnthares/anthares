��          �   %   �      p     q     ~     �     �     �     �     �     �  )   �               !     4     I     ]     m  3   u     �     �  <   �  5     8   T  8   �     �     �     �     �  �  �  "   �  "   �     �  $   �          0  =   N     �  l   �            3   (  :   \  @   �  #   �     �  H   	  5   Z	  G   �	  {   �	  t   T
  c   �
  c   -     �  #   �  %   �     �                        
                                                	                                                   Add files... Add folder... Background frame Change picture every Choose a folder Choose files Configure plasmoid... General Left click image opens in external viewer Pad Paths Pause on mouseover Preserve aspect crop Preserve aspect fit Randomize items Stretch The image is duplicated horizontally and vertically The image is not transformed The image is scaled to fit The image is scaled uniformly to fill, cropping if necessary The image is scaled uniformly to fit without cropping The image is stretched horizontally and tiled vertically The image is stretched vertically and tiled horizontally Tile Tile horizontally Tile vertically s Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-16 03:31+0100
PO-Revision-Date: 2017-02-09 21:51+0200
Last-Translator: Petros Vidalis <pvidalis@gmail.com>
Language-Team: Greek <kde-i18n-doc@kde.org>
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Προσθήκη αρχείων... Προσθήκη φακέλου... Πλαίσιο φόντου Αλλαγή εικόνας κάθε Επιλογή φακέλου Επιλογή αρχείων Διαμόρφωση γραφικού συστατικού... Γενικά Με αριστερό κλικ στην εικόνα άνοιγμα σε εξωτερικό προβολέα Γέμισμα Διαδρομές Παύση στο πέρασμα ποντικιού Διατήρηση διαστάσεων κοψίματος Διατήρηση ταιριάσματος διαστάσεων Τυχαία αντικείμενα Επιμήκυνση Διπλότυπο εικόνας οριζόντια και κάθετα Η εικόνα δε μετασχηματίζεται Κλιμάκωση της εικόνας για να ταιριάζει Ομοιόμορφη κλιμάκωση της εικόνας για γέμισμα, κόψιμο αν χρειάζεται Ομοιόμορφη κλιμάκωση της εικόνας για να ταιριάζει χωρίς κόψιμο Οριζόντια επιμήκυνση εικόνας και παράθεση κατακόρυφα Κατακόρυφη επιμήκυνση εικόνας και παράθεση οριζόντια Παράθεση Οριζόντια παράθεση Κατακόρυφη παράθεση δ 