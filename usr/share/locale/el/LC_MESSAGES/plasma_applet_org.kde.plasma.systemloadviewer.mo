��    #      4  /   L           	     $     1     G     K     T     X     _     k          �     �     �     �     �     �     �     �     �     �     �     �                    0     6     ;     H     X     ]     i  
   z     �  �  �     D     G  ,   Y     �     �     �  	   �  !   �     �     �          "  <   @  '   }     �     �     �     �     �  
   �  '        )  (   ?     h  8   y     �     �     �     �     �  !   �  &    	     G	     ]	        	          "                                 
                                  #          !                                                              Abbreviation for secondss Application: Average clock: %1 MHz Bar Buffers: CPU CPU %1 CPU monitor CPU%1: %2% @ %3 Mhz CPU: %1% CPUs separately Cache Cache monitor Cached: Circular Colors Compact Bar General IOWait: Memory Memory monitor Memory: %1/%2 MiB Monitor type: Nice: Set colors manually Show: Swap Swap monitor Swap: %1/%2 MiB Sys: System load Update interval: Used swap: User: Project-Id-Version: plasma_applet_systemstatus
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-29 03:31+0200
PO-Revision-Date: 2017-03-14 12:24+0200
Last-Translator: Dimitris Kardarakos <dimkard@gmail.com>
Language-Team: Greek <kde-i18n-el@kde.org>
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 δ Εφαρμογή: Μέσος όρος ρολογιού: %1 MHz Ράβδος Ενδιάμεση μνήμη: ΚΜΕ ΚΜΕ %1 Παρακολούθηση ΚΜΕ ΚΜΕ%1: %2% @ %3 Mhz ΚΜΕ: %1% ΚΜΕ ξεχωριστά Προσωρινή μνήμη Παρακολούθηση προσωρινής μνήμης Στην προσωρινή μνήμη: Κυκλικά Χρώματα Συμπαγής ράβδος Γενικά IOWait: Μνήμη Παρακολούθηση μνήμης Μνήμη: %1/%2 MiB Τύπος παρακολούθησης: Χρήση nice: Χειροκίνητος ορισμός χρωμάτων Εμφάνιση: Swap Παρακολούθηση Swap Swap: %1/%2 MiB Sys: Φορτίο συστήματος Διάστημα ενημέρωσης: Swap σε χρήση: Χρήστης: 