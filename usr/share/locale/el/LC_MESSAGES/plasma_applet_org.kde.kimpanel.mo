��          �      �       0     1     9  
   S     ^     u     �     �     �     �     �     �     �  �  �     y  +   �     �  6   �  6   �  F   1     x  /   �  +   �     �  F   �     @     	   
                                                  (Empty) @title:windowSelect Font Appearance Configure Input Method Custom Font: Exit Input Method Hide %1 Reload Config Select Font Show Use Default Font Vertical List Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-07-18 03:21+0200
PO-Revision-Date: 2017-03-14 12:16+0200
Last-Translator: Dimitris Kardarakos <dimkard@gmail.com>
Language-Team: Greek <kde-i18n-el@kde.org>
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 (Άδειο) Επιλογή γραμματοσειράς Εμφάνιση Διαμόρφωση μεθόδου εισαγωγής Προσαρμοσμένη γραμματοσειρά: Έξοδος από μέθοδο εισαγωγής δεδομένων Απόκρυψη %1 Επαναφόρτωση διαμόρφωσης Επιλογή γραμματοσειράς Εμφάνιση Χρήση προκαθορισμένης γραμματοσειράς Κατακόρυφη λίστα 