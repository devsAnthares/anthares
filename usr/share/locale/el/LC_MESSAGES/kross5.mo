��    $      <  5   \      0     1  +   E     q  4   �  &   �     �     �                     5     :     P     X  ,   u  3   �     �     �               !  '   .     V     u     {     �     �     �     �     �     �  &   �       B        b  �  y       >   4     s  .   �     �  4   �     �     	     	  F   &	     m	  F   �	     �	  @   �	  d   &
  o   �
  H   �
  Q   D     �     �     �  H   �  Y        y  -   �  7   �     �  @   �     =  >   N     �  U   �     �  l        s                                       
                              !                                  $      #                               	                           "             @info:creditAuthor @info:creditCopyright 2006 Sebastian Sauer @info:creditSebastian Sauer @info:shell command-line argumentThe script to run. @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: application descriptionCommand-line utility to run Kross scripts. application nameKross Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2014-04-19 14:53+0200
Last-Translator: Dimitrios Glentadakis <dglent@free.fr>
Language-Team: Greek <kde-i18n-el@kde.org>
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 Συγγραφέας Πνευματικά δικαιώματα 2006 Sebastian Sauer Sebastian Sauer Το σενάριο προς εκτέλεση. Γενικά Προσθήκη ενός νέου σεναρίου. Προσθήκη... Ακύρωση? Σχόλιο: sng@hellug.gr, manolis@koppermind.homelinux.org, p_vidalis@hotmail.com Επεξεργασία Επεξεργασία του επιλεγμένου σεναρίου. Επεξεργασία... Εκτέλεση του επιλεγμένου σεναρίου. Αποτυχία δημιουργίας σεναρίου για το μεταγλωττιστή "%1" Αποτυχία καθορισμού μεταγλωττιστή για το αρχείο σεναρίου "%1" Αποτυχία φόρτωσης του μεταγλωττιστή "%1" Αποτυχία ανοίγματος του αρχείου σεναρίου "%1" Αρχείο: Εικονίδιο: Μεταγλωττιστής: Επίπεδο ασφαλείας του μεταγλωττιστή Ruby Σπύρος Γεωργαράς, Τούσης Μανώλης, Πέτρος Βιδάλης Όνομα: Δεν υπάρχει συνάρτηση "%1" Δεν υπάρχει μεταγλωττιστής "%1" Αφαίρεση Αφαίρεση του επιλεγμένου σεναρίου. Εκτέλεση Το αρχείο σεναρίου "%1" δεν υπάρχει. Σταμάτημα Σταμάτημα εκτέλεσης του επιλεγμένου σεναρίου. Κείμενο: Εργαλείο γραμμής εντολών για την εκτέλεση σεναρίων του Kross. Kross 