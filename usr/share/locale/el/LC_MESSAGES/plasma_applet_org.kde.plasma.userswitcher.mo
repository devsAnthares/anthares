��          �      <      �     �     �     �     �     �  '   �  >        L     f     �     �     �  )   �  7   �  '        B     T  �  s          .     ;     J     h     �     �  >   �  U   �  M   B  (   �  *   �  f   �     K     Z  .   a  ,   �                                       
                      	                                        Current user General Layout Lock Screen New Session Nobody logged in on that sessionUnused Show a dialog with options to logout/shutdown/restartLeave... Show both avatar and name Show full name (if available) Show login username Show only avatar Show only name Show technical information about sessions User logged in on console (X display number)on %1 (%2) User logged in on console numberTTY %1 User name display You are logged in as <b>%1</b> Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-11 03:18+0100
PO-Revision-Date: 2016-09-09 16:59+0200
Last-Translator: Dimitris Kardarakos <dimkard@gmail.com>
Language-Team: Greek <kde-i18n-el@kde.org>
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Τρέχων χρήστης Γενικά Διάταξη Κλείδωμα οθόνης Νέα συνεδρία Αχρησιμοποίητη Έξοδος... Εμφάνιση και εικόνας και ονόματος Εμφάνιση πλήρους ονόματος (αν είναι διαθέσιμο) Εμφάνιση αναγνωριστικού πρόσβασης χρήστη Εμφάνιση μόνο εικόνας Εμφάνιση μόνο ονόματος Εμφάνιση τεχνικών πληροφοριών σχετικά με τις συνεδρίες στο %1 (%2) TTY %1 Εμφάνιση ονόματος χρήστη Έχετε συνδεθεί σαν <b>%1</b> 