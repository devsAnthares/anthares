��          <      \       p   !   q   3   �      �   �  �   O   �  ^   �  &   F                   Finds Kate sessions matching :q:. Lists all the Kate editor sessions in your account. Open Kate Session Project-Id-Version: krunner_katesessions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-18 03:08+0100
PO-Revision-Date: 2009-04-27 11:33+0300
Last-Translator: Toussis Manolis <manolis@koppermind.homelinux.org>
Language-Team: Greek <kde-i18n-el@kde.org>
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 0.3
 Εύρεση συνεδριών Kate που ταιριάζουν με το :q:. Εμφάνιση όλων των συνεδριών Kate του λογαριασμού σας. Άνοιγμα συνεδρίας Kate 