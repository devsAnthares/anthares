��          �   %   �      p     q  +   �  .   �  6   �  *     #   D  &   h  /   �  2   �  :   �  0   -  3   ^  ;   �  K   �  -     $   H  '   m     �     �     �     �     �  #        1     O     c     |  �  �     %  D   4  B   y  R   �  4   	  /   D	  $   t	  I   �	  G   �	  W   +
  M   �
  I   �
  Y     �   u  G     0   `  0   �     �     �     �            2   0  +   c     �     �  '   �                                                    	   
                                                                    @action:buttonCommit @info:statusAdded files to SVN repository. @info:statusAdding files to SVN repository... @info:statusAdding of files to SVN repository failed. @info:statusCommit of SVN changes failed. @info:statusCommitted SVN changes. @info:statusCommitting SVN changes... @info:statusRemoved files from SVN repository. @info:statusRemoving files from SVN repository... @info:statusRemoving of files from SVN repository failed. @info:statusReverted files from SVN repository. @info:statusReverting files from SVN repository... @info:statusReverting of files from SVN repository failed. @info:statusSVN status update failed. Disabling Option "Show SVN Updates". @info:statusUpdate of SVN repository failed. @info:statusUpdated SVN repository. @info:statusUpdating SVN repository... @item:inmenuSVN Add @item:inmenuSVN Commit... @item:inmenuSVN Delete @item:inmenuSVN Revert @item:inmenuSVN Update @item:inmenuShow Local SVN Changes @item:inmenuShow SVN Updates @labelDescription: @title:windowSVN Commit Show updates Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2016-11-02 15:48+0200
Last-Translator: Petros Vidalis <pvidalis@gmail.com>
Language-Team: Greek <kde-i18n-doc@kde.org>
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Υποβολή Προστέθηκαν αρχεία στο αποθετήριο SVN. Προσθήκη αρχείων στο αποθετήριο SVN... Η προσθήκη αρχείων στο αποθετήριο SVN απέτυχε. Η υποβολή αλλαγών SVN απέτυχε. Υποβλήθηκαν οι αλλαγές SVN. Υποβολή αλλαγών SVN... Αφαιρέθηκαν αρχεία από το αποθετήριο SVN. Αφαίρεση αρχείων από το αποθετήριο SVN... Η αφαίρεση αρχείων από το αποθετήριο SVN απέτυχε. Επαναφέρθηκαν αρχεία από το αποθετήριο SVN. Επαναφορά αρχείων από το αποθετήριο SVN... Η επαναφορά αρχείων από το αποθετήριο SVN απέτυχε. Η ενημέρωση κατάστασης SVN απέτυχε. Απενεργοποίηση της επιλογής "Εμφάνιση ενημερώσεων SVN". Η ενημέρωση του αποθετηρίου SVN απέτυχε. Ενημερωμένο αποθετήριο SVN. Ενημέρωση αποθετηρίου SVN... Προσθήκη SVN Υποβολή SVN... Διαγραφή SVN Επαναφορά SVN Ενημέρωση SVN Εμφάνιση τοπικών αλλαγών SVN Εμφάνιση ενημερώσεων SVN Περιγραφή: Υποβολή SVN Εμφάνιση ενημερώσεων 