��          �   %   �      0     1     E     X     l          �     �     �     �  
   �     �     �                                    "     0  	   <     F     L  �  S  5     3   ;  9   o  1   �  9   �  A     5   W  9   �  =   �       
        !     *     7     F     S     f     m     |     �     �  
   �     �     	                                                                          
                                                A black sticky note A blue sticky note A green sticky note A pink sticky note A red sticky note A translucent sticky note A white sticky note A yellow sticky note An orange sticky note Appearance Black Blue Bold Green Italic Orange Pink Red Strikethrough Translucent Underline White Yellow Project-Id-Version: plasma_applet_notes
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-07-09 03:12+0200
PO-Revision-Date: 2015-11-17 12:18+0200
Last-Translator: Dimitris Kardarakos <dimkard@gmail.com>
Language-Team: Greek <kde-i18n-el@kde.org>
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 Μια μαύρη κολλημένη σημείωση Μια μπλε κολλημένη σημείωση Μια πράσινη κολλημένη σημείωση Μια ροζ κολλημένη σημείωση Μια κόκκινη κολλημένη σημείωση Μια ημιδιαφανής κολλημένη σημείωση Μια λευκή κολλημένη σημείωση Μια κίτρινη κολλημένη σημείωση Μια πορτοκαλί κολλημένη σημείωση Εμφάνιση Μαύρο Μπλε Έντονα Πράσινο Πλάγια Πορτοκαλί Ροζ Κόκκινο Επιγράμμιση Ημιδιαφανές Υπογράμμιση Λευκό Κίτρινο 