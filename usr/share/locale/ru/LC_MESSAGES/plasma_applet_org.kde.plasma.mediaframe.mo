��          �      <      �     �     �     �     �     �               %  )   -     W     ]     p  5   �     �     �     �     �  �  �     �     �  .     2   I     |     �  "   �     �  p   �     O  U   X     �  �   �     �  .   �  *   �                
                                         	                                                 Add files... Add folder... Background frame Change picture every Choose a folder Choose files Configure plasmoid... General Left click image opens in external viewer Paths Pause on mouseover Randomize items The image is scaled uniformly to fit without cropping Tile Tile horizontally Tile vertically s Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-16 03:31+0100
PO-Revision-Date: 2017-11-15 22:01+0300
Last-Translator: Alexander Potashev <aspotashev@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 2.0
 Добавить файлы... Добавить папку... Показывать рамку виджета Сменять изображение каждые Выбор папки Выбор файлов Настроить виджет... Основное Открывать в программе просмотра по нажатию левой кнопки мыши Пути Приостанавливать слайд-шоу при наведении мыши Случайный выбор Изображение масштабируется с сохранением пропорций до максимального размера, способного уместиться в рамке. Черепицей Черепицей по горизонтали Черепицей по вертикали  с 