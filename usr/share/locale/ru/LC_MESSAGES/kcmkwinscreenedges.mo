��    %      D  5   l      @     A     E     G  	   Y     c     |     �     �     �     �     �     �            S   ,  w   �     �  M         [     |  ?   �     �  	   �     �     
     #  %   2     X     e  F   �  #   �     �  ]     R   f     �     �  ;  �     
     
  '   
     E
  *   S
  -   ~
     �
     �
  A   �
  d     j   {  /   �          6  �   C  w   �  ,   L  �   y     �  %     n   :  !   �     �  3   �  !        @  :   Q  (   �     �  �   �  D   L  0   �  �   �  �   �  !   X     z               !      "          #   %                                                                            $                          	                
                            ms % %1 - All Desktops %1 - Cube %1 - Current Application %1 - Current Desktop %1 - Cylinder %1 - Sphere &Reactivation delay: &Switch desktop on edge: Activation &delay: Active Screen Corners and Edges Activity Manager Always Enabled Amount of time required after triggering an action until the next trigger can occur Amount of time required for the mouse cursor to be pushed against the edge of the screen before the action is triggered Application Launcher Change desktop when the mouse cursor is pushed against the edge of the screen EMAIL OF TRANSLATORSYour emails Lock Screen Maximize windows by dragging them to the top edge of the screen NAME OF TRANSLATORSYour names No Action Only When Moving Windows Open krunnerRun Command Other Settings Quarter tiling triggered in the outer Show Desktop Switch desktop on edgeDisabled Tile windows by dragging them to the left or right edges of the screen Toggle alternative window switching Toggle window switching Trigger an action by pushing the mouse cursor against the corresponding screen edge or corner Trigger an action by swiping from the screen edge towards the center of the screen Window Management of the screen Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-30 03:13+0100
PO-Revision-Date: 2018-01-31 05:45+0300
Last-Translator: Alexander Potashev <aspotashev@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
  мс % %1 — Все рабочие столы %1 — Куб %1 — Текущее приложение %1 — Текущий рабочий стол %1 — Цилиндр %1 — Сфера &Задержка перед повторной реакцией: &Смена рабочего стола при движении мыши за край экрана: &Задержка перед реакцией на подведение мыши к краю экрана: Действия для краёв экрана Диспетчер комнат Всегда Задержка между выполнением действий при подведении курсора мыши к краю экрана Задержка между подведением к краю экрана и выполнением действия. Меню запуска приложений Переход на другой рабочий стол при подведении курсора мыши к краю экрана aspotashev@gmail.com Заблокировать экран Растягивать окно при перемещении его к верхнему краю экрана Александр Поташев Ничего не делать Только при перемещении окна Выполнить команду Задержки Укладывать в четверть в крайних Показать рабочий стол Отключена Растягивать окно наполовину при перемещении его к боковому краю экрана Вызвать альтернативное переключение Вызвать переключение окон Для выбора действия, выполняемого при подведении мыши к какому-либо краю или углу экрана, нажмите в соответствующем месте мини-экрана. Для выполнения действия проведите по экрану от соответствующего края к центру экрана. Управление окнами экрана 