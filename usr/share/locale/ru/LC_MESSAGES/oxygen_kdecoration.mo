��    ?        Y         p     q  !   �  "   �  &   �  ,   �  #   &  &   J  !   q  &   �  '   �  "   �  #     "   )  '   L     t     �  +   �  2   �  
   �     �               ,     3     G  U   O  7   �     �     �     		     	      	     6	     T	     c	     k	  !   �	     �	  	   �	     �	     �	     �	     �	  &   
     /
     N
     U
     p
     v
     ~
     �
  4   �
  $   �
     �
               /     G     ]     w  &   �     �  H  �  6        E     V     e  "   w     �     �     �     �     �     �               0  .   J     y  a   �  V   �     C     T     c  O   |     �  ,   �       �     p   �  '   9  .   a     �     �  O   �  C        S     o  5   �  /   �  
   �     �  
   �  G     *   M  '   x  M   �  B   �     1  7   @     x     �  -   �     �  \   �  8   .      g  :   �  &   �  (   �  !     +   5     a  [   }  ?   �     $                               1          "   *   )          :             	         ;   !                          9   =   5   /   3                    (         #   4       8       -   6   
   7          %                                         ?       0   &      +          '         .       ,      >       <   2        &Matching window property:  @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Border @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large @item:inlistbox Button size:Large @item:inlistbox Button size:Normal @item:inlistbox Button size:Small @item:inlistbox Button size:Very Large Active Window Glow Add Add handle to resize windows with no border Allow resizing maximized windows from window edges Animations B&utton size: Border size: Button mouseover transition Center Center (Full Width) Class:  Configure fading between window shadow and glow when window's active state is changed Configure window buttons' mouseover highlight animation Decoration Options Detect Window Properties Dialog Edit Edit Exception - Oxygen Settings Enable/disable this exception Exception Type General Hide window title bar Information about Selected Window Left Move Down Move Up New Exception - Oxygen Settings Question - Oxygen Settings Regular Expression Regular Expression syntax is incorrect Regular expression &to match:  Remove Remove selected exception? Right Shadows Tit&le alignment: Title:  Use the same colors for title bar and window content Use window class (whole application) Use window title Warning - Oxygen Settings Window Class Name Window Drop-Down Shadow Window Identification Window Property Selection Window Title Window active state change transitions Window-Specific Overrides Project-Id-Version: kstyle_config
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-13 05:20+0100
PO-Revision-Date: 2018-02-01 09:50+0300
Last-Translator: Alexander Potashev <aspotashev@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Совпадает со &свойством окна:  Огромные Широкие Без рамки Без боковых границ Обычные Самые широкие Тонкие Гигантские Очень широкие Большие Обычные Маленькие Очень большие Подсветка активного окна Добавить Добавить уголок для изменения размера окон без рамки Разрешить изменение размеров распахнутых окон Анимация &Кнопки: Границы окна: Переходы кнопок при наведении курсора мыши По центру По центру (на всю ширину) Класс:  Параметры постепенного изменения между тенью и светом при изменении состояния активности окна Параметры анимации при наведении курсора мыши на кнопки окна Параметры оформления Определить свойства окна Диалоговое окно Изменить Редактирование исключения — Настройка Oxygen Включение или отключение исключения Тип исключения Основное Не показывать заголовок окна Сведения о выбранном окне Слева Вниз Вверх Добавление исключения — Настройка Oxygen Вопрос — Настройка Oxygen Регулярное выражение Неверный синтаксис регулярного выражения Совпадает с &регулярным выражением:  Удалить Удалить выбранное исключение? Справа Тени В&ыравнивание заголовка: Заголовок:  Одинаковые цвета для заголовка и содержимого окна По классу окна (всё приложение) По заголовку окна Предупреждение — Настройка Oxygen Название класса окна Окно отбрасывает тень По идентификатору Выбор окон по свойствам Заголовок окна Переходы при изменении состояния активности окна Переназначения для отдельных окон 