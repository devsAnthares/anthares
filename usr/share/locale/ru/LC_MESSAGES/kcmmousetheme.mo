��          �   %   �      @     A  �   `  m   �  �   P  )   �  `        o     |     �     �     �      �     �     �  '        ,     >     ]     b     s  ?   �  Y   �  +     H   F  C  �     �  �   �  �   �	  '   �
  3   �
  �   �
     �  2   �            I   -  <   w     �  )   �  Q   �  '   A  /   i     �     �     �  `   �  �   4  G   �  ~   �                                                 	                                                               
              (c) 2003-2007 Fredrik Höglund <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) @item:inlistbox sizeResolution dependent A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Cursor Theme Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Fredrik Höglund Get new Theme Get new color schemes from the Internet Install from File NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. You have to restart the Plasma session for these changes to take effect. Project-Id-Version: kcminput
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-09-29 02:16+0300
Last-Translator: Alexander Potashev <aspotashev@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 © Fredrik Höglund, 2003-2007 <qt>Вы действительно хотите удалить тему курсоров<i>%1</i>? <br />Все файлы, установленные этой темой, будут удалены.</qt> <qt>Вы не можете удалить эту тему, потому что она сейчас используется.<br />Сначала необходимо переключиться на другую тему.</qt> (Доступные размеры: %1) В зависимости от разрешения Тема с именем %1 уже существует в папке тем значков. Вы действительно хотите заменить её новой темой? Подтверждение Параметры курсора изменены Тема курсоров Описание Перетащите или введите ссылку (URL) к теме dyp@perchine.com, Hermann.Zheboldov@shq.ru, leon@asplinux.ru Fredrik Höglund Загрузить новые темы... Загрузить новые цветовые схемы из Интернета Установить из файла... Denis Pershin, Hermann Zheboldov, Leonid Kanter Имя Заменить тему? Удалить тему Похоже, что файл %1 не является архивом темы курсоров. Не удаётся загрузить архив темы курсоров, Убедитесь, что адрес %1 верен. Не удаётся найти архив темы курсоров %1. Чтобы изменения вступили в силу, необходимо перезапустить сеанс Plasma. 