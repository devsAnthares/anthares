��    F      L  a   |         
     
     
        "     1     5     I     X     ^  	   m     w  	        �     �     �  
   �     �     �  3   �  	   �     �               $     9     @     G     V     f     m  
        �  2   �  ?   �                    )     2     ?     N     S     a     r     �     �     �     �     �     �  	   �     �     �     �     �  b   �  �   [	     �	     �	  	   
     
     (
  
   8
  	   C
     M
     ]
     e
     k
     }
  ;  �
     �  !   �  '   �  "   #     F     J  !   j     �  )   �     �     �  +   �       
   1     <     I     \     n     }  !     !   �      �     �  %   �               0  0   N       /   �     �  !   �  r   �  �   o       
   &     1     I  )   \     �     �     �     �     �     �  %        *     F     S     m  
   �     �  #   �  *   �     �  u   �  j  j     �  !   �            +   /     [     n     w     �     �  4   �  ,   �     +       E   C   4       &       D           
   <      *   9           !   F      /   #                A       @   =                     $   ;                  B             :       '           0         1   2                                -   (       6   ?         >              "   .   %   ,              )       8       3   5   	                  7    Activities Add Action Add Spacer Add Widgets... Alt Alternative Widgets Always Visible Apply Apply Settings Apply now Author: Auto Hide Back-Button Bottom Cancel Categories Center Close Concatenation sign for shortcuts, e.g. Ctrl+Shift+ Configure Configure activity Create activity... Ctrl Currently being used Delete Email: Forward-Button Get new widgets Height Horizontal-Scroll Input Here Keyboard shortcuts Layout cannot be changed whilst widgets are locked Layout changes must be applied before other changes can be made Layout: Left Left-Button License: Lock Widgets Maximize Panel Meta Middle-Button More Settings... Mouse Actions OK Panel Alignment Remove Panel Right Right-Button Screen Edge Search... Shift Stop activity Stopped activities: Switch The settings of the current module have changed. Do you want to apply the changes or discard them? This shortcut will activate the applet: it will give the keyboard focus to it, and if the applet has a popup (such as the start menu), the popup will be open. Top Undo uninstall Uninstall Uninstall widget Vertical-Scroll Visibility Wallpaper Wallpaper Type: Widgets Width Windows Can Cover Windows Go Below Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-09 03:10+0200
PO-Revision-Date: 2017-10-05 03:09+0300
Last-Translator: Alexander Potashev <aspotashev@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Комнаты Добавить действие Добавить разделитель Добавить виджеты... Alt Варианты виджета Показывать всегда Применить Сохранение параметров Применить Автор:  Автоматически скрывать Кнопка «назад» Снизу Отмена Категории По центру Закрыть + Настроить комнату Настроить комнату Создать комнату... Ctrl Сейчас используется Удалить Эл. почта: Кнопка «вперёд» Пополнить список виджетов Высота Горизонтальная прокрутка Нажимайте здесь Комбинация клавиш Невозможно изменить тип комнаты, когда виджеты заблокированы. Изменение типа комнаты необходимо применить перед редактированием других параметров. Тип комнаты: Слева Левая кнопка Лицензия:  Заблокировать виджеты Растянуть панель Meta Средняя кнопка Дополнительно... Действия мыши ОК Выравнивание панели Удалить панель Справа Правая кнопка Край экрана Поиск Shift Остановить комнату Остановленные комнаты: Заменить В текущем модуле имеются несохранённые изменения. Применить их? Эта комбинация клавиш активирует виджет: фокус ввода с клавиатуры будет переведён на него. Если у виджета есть всплывающее окно (например, как у кнопки меню запуска приложений), то оно будет открыто. Сверху Отменить удаление Удалить Удалить виджет Вертикальная прокрутка Видимость Обои Тип обоев: Виджеты Ширина Допускать перекрытие окнами Панель перекрывает окна 