��    7      �  I   �      �     �     �     �     �     �     �               $     -     5     H     T      `     �     �     �     �     �     �     �     �                     )     7  	   C  	   M     W     d     j     �     �     �     �     �     �     �     �     �     �  @   �     7     @     \     c     j     r     �     �     �  4   �     �  �  �     �	     �	     �	  >   �	     <
     K
     Z
     v
     �
     �
     �
     �
     �
     �
  
     "     7   >  
   v  !   �  #   �  +   �     �           $     B      a     �  *   �     �     �        !        /     6  5   P     �     �     �     �     �     �     �  �   	  #   �  W   �     
             2   5     h     m     v  "   {     �     &   0      7                  #                
          	   '   (   +                       6              3      1                   ,   2                  "       *          $      4   )               %      5                     !          /   .   -       % %1 is value, %2 is unit%1 %2 %1: Application Energy Consumption Battery Capacity Charge Percentage Charge state Charging Current Degree Celsius°C Details: %1 Discharging EMAIL OF TRANSLATORSYour emails Energy Energy Consumption Energy Consumption Statistics Environment Full design Fully charged Has power supply Kai Uwe Broulik Last 12 hours Last 2 hours Last 24 hours Last 48 hours Last 7 days Last full Last hour Manufacturer Model NAME OF TRANSLATORSYour names No Not charging PID: %1 Path: %1 Rechargeable Refresh Serial Number Shorthand for WattsW System Temperature This type of history is currently not available for this device. Timespan Timespan of data to display Vendor VoltV Voltage Wakeups per second: %1 (%2%) WattW Watt-hoursWh Yes current power draw from the battery in WConsumption literal percent sign% Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2016-08-13 21:28+0300
Last-Translator: Alexander Potashev <aspotashev@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 2.0
 % %1 %2 %1: Потребление энергии приложениями Батарея Ёмкость Процент заряда Состояние заряда Заряжается Текущий заряд °C Подробности: %1 Разряжается aspotashev@gmail.com Заряд Энергопотребление Статистика энергопотребления Среда Расчётная ёмкость Полностью заряжена Имеет источник питания: Kai Uwe Broulik Последние 12 часов Последние 2 часа Последние 24 часа Последние 48 часов Последние 7 дней Последний полный заряд Последний час Производитель Модель Александр Поташев Нет Не заряжается Идентификатор процесса (PID): %1 Путь: %1 Перезаряжаемая Обновить Серийный номер  Вт Система Температура Эта хронологическая информация сейчас недоступна для этого устройства. Промежуток времени Показывать данные для этого промежутка времени Поставщик В Напряжение Пробуждений в секунду: %1 (%2%) Вт Вт·ч Да Энергопотребление % 