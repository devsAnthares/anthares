��          �      �           	          &     -     4  
   =     H     Q     Y     i  >   w     �     �     �     �  
   �     �     �     �            U   %  N  {     �     �     �               4     J     Z  !   p     �  �   �  #   s     �  C   �  #   �  6        L     X     e     �     �  �   �                       	                                                
                                 %1 is running %1 not running &Reset &Start Advanced Appearance Command: Display Execute command Notifications Remaining time left: %1 second Remaining time left: %1 seconds Run command S&top Show notification Show seconds Show title Text: Timer Timer finished Timer is running Title: Use mouse wheel to change digits or choose from predefined timers in the context menu Project-Id-Version: plasma_applet_timer
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2015-08-20 18:33+0300
Last-Translator: Alexander Potashev <aspotashev@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 %1 запущен %1 не запущен Сб&росить За&пустить Дополнительно Внешний вид Команда: Вид виджета Выполнять команду Уведомления Оставшееся время: %1 секунда Оставшееся время: %1 секунды Оставшееся время: %1 секунд Оставшееся время: %1 секунда Выполнение команды О&становить Показывать уведомление о завершении Показывать секунды Показывать заголовок виджета Текст: Таймер Отсчёт завершён Таймер запущен Заголовок: Меняйте часы, минуты и секунды прокруткой колеса мыши или выберите одно из заранее заданных времён в контекстном меню. 