��          �      l      �     �     �            
   #     .     :     I     Q     ]     e     v     }     �  #   �     �     �     �     �  
      S    4   _  9   �  '   �     �  +     .   =  
   l     w  /   �     �  /   �     �  !        9     F  ,   U  !   �  ,   �     �     �                                                       
   	                                         (Automatic load) Always show all entries Application Status Auto Categories Close popup Communications Entries Extra Items General Hardware Control Hidden Keyboard Shortcut Miscellaneous Name of the system tray entryEntry Show hidden icons Shown Status & Notifications System Services Visibility Project-Id-Version: plasma_applet_systemtray
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-03 03:15+0100
PO-Revision-Date: 2017-02-02 18:54+0300
Last-Translator: Alexander Potashev <aspotashev@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
  (загружается автоматически) Всегда показывать все элементы Состояния приложений Автоматически Показываемые категории Закрыть всплывающее окно Связь Элементы Индивидуальная настройка Основное Управление оборудованием Всегда скрывать Комбинация клавиш Прочее Элемент Показать скрытые значки Всегда показывать Состояние и уведомления Системные службы Видимость 