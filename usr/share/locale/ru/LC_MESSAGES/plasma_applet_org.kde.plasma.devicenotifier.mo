��          �      l      �  3   �  $     �   :     �  4   �     �  #        =  �   E  �   �  +   �     �     �     �  1     (   3     \  �   s  $   �  (     W  F  �   �     �	     �	  ,   �	  N   �	     4
  2   L
     
  L  �
  �  �  d   �  @     ,   P  6   }  5   �  c   �  ,   N     {  Y   �  [   �                                                   	   
                                            1 action for this device %1 actions for this device @info:status Free disk space%1 free Accessing is a less technical word for Mounting; translation should be short and mean 'Currently mounting this device'Accessing... All devices Click to access this device from other applications. Click to eject this disc. Click to safely remove this device. General It is currently <b>not safe</b> to remove this device: applications may be accessing it. Click the eject button to safely remove this device. It is currently <b>not safe</b> to remove this device: applications may be accessing other volumes on this device. Click the eject button on these other volumes to safely remove this device. It is currently safe to remove this device. Most Recent Device No Devices Available Non-removable devices only Open auto mounter kcmConfigure Removable Devices Open popup when new device is plugged in Removable devices only Removing is a less technical word for Unmounting; translation shoud be short and mean 'Currently unmounting this device'Removing... This device is currently accessible. This device is not currently accessible. Project-Id-Version: plasma_applet_devicenotifier
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2016-09-29 02:45+0300
Last-Translator: Alexander Potashev <aspotashev@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 %1 действие для этого устройства %1 действия для этого устройства %1 действий для этого устройства %1 действие для этого устройства Свободно %1 Подключение... Все носители информации Сделать носитель доступным для приложений Извлечь диск Безопасно извлечь носитель Основное В данный момент <b>небезопасно</b> извлекать это устройство, потому что некоторые приложения могут работать с ним. Сначала нажмите кнопку безопасного извлечения для этого устройства. В данный момент <b>небезопасно</b> извлекать это устройство, потому что некоторые приложения могут работать с другими логическими дисками на этом устройстве. Сначала нажмите кнопки безопасного извлечения для других логических дисков этого устройства. Теперь можно извлечь носитель без риска потери данных. Последнее подключённое устройство Нет доступных устройств Только стационарные носители Настроить внешние носители... Открывать всплывающее окно при подключении устройств Только съёмные носители Отключение... Устройство подключено и доступно для приложений Устройство отключено и недоступно для приложений 