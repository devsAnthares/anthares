��    "      ,  /   <      �  
   �               ,     >     J  !   V     x     �     �     �     �     �  L   �     #     +     J     Y     u     z     �     �     �     �  	   �     �     �     �  �   �  F   �     �     �     �  ;  �     7     H  +   g     �     �     �     �  *   �  "   $	  .   G	  /   v	      �	  $   �	  x   �	     e
  6   s
  (   �
  H   �
          -  A   @  O   �     �     �  !   �  0        @  /   V  �   �  �   `     �  $        @                    !                                            	                                                           "               
                    Activities All other activities All other desktops All other screens All windows Alternative An example Desktop NameDesktop 1 Content Current activity Current application Current desktop Current screen Filter windows by Focus policy settings limit the functionality of navigating through windows. Forward Get New Window Switcher Layout Hidden windows Include "Show Desktop" icon Main Minimization Only one window per application Recently used Reverse Screens Shortcuts Show selected window Sort order: Stacking order The currently selected window will be highlighted by fading out all other windows. This option requires desktop effects to be active. The effect to replace the list window when desktop effects are active. Virtual desktops Visible windows Visualization Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2012-10-12 23:32+0400
Last-Translator: Alexander Potashev <aspotashev@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.4
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Комнатам Из других комнат С других рабочих столов С других экранов Все окна Альтернативные Рабочий стол 1 Содержание списка окон Из текущей комнаты Окна текущего приложения С текущего рабочего стола С текущего экрана Фильтровать окна по Параметры смены фокуса не позволяют настроить переключение окон. Вперёд: Загрузить новые визуализации Только свёрнутые окна Включать пункт «Показать рабочий стол» Основные Видимости По одному окну на каждое приложение По времени последнего переключения на окно Назад: Экранам Комбинации клавиш Показывать выбранное окно Сортировка: По порядку наложения окон Выбранное окно будет выделено затенением всех остальных окон. Для работы должны быть включены эффекты рабочего стола. Визуализация переключения окон, используемая при включённых эффектах рабочего стола. Рабочим столам Только видимые окна Визуализация 