��    
      l      �       �   $   �   %     :   <     w  '   �  -   �     �       '     ;  <  ?   x  K   �  k     Y   p  R   �  B     !   `  #   �  O   �         	                            
        Could not detect the file's mimetype Could not find all required functions Could not find the provider with the specified destination Error trying to execute script Invalid path for the requested provider It was not possible to read the selected file Service was not available Unknown Error You must specify a URL for this service Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-10-14 05:11+0400
Last-Translator: Alexander Potashev <aspotashev@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.1
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Не удалось определить тип MIME файла Не удалось найти все необходимые функции Не удалось найти провайдера с указанным местом назначения Произошла ошибка при попытке выполнить сценарий Недопустимый путь для требуемого провайдера Не удалось прочитать выбранный файл Сервис недоступен Неизвестная ошибка Вы должны определить URL для данного сервиса 