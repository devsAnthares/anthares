��    !      $  /   ,      �  .   �  1     9   J     �  -   �  &   �  )   �  .   #  &   R  )   y  .   �  &   �  )   �  2   #  5   V  =   �  #   �     �  !     '   /  "   W  0   z  '   �  *   �     �          7     R     j     �     �  &   �  8  �  ;   
  A   S
  M   �
  !   �
  I     5   O  -   �  ?   �  +   �  3     C   S  /   �  5   �  9   �  ?   7  S   w  @   �  ,     (   9  J   b  @   �  ?   �  -   .  1   \     �  %   �     �     �     �          1  =   J                                                              	                                
                                    !                             @info:statusAdded files to Bazaar repository. @info:statusAdding files to Bazaar repository... @info:statusAdding of files to Bazaar repository failed. @info:statusBazaar Log closed. @info:statusCommit of Bazaar changes failed. @info:statusCommitted Bazaar changes. @info:statusCommitting Bazaar changes... @info:statusPull of Bazaar repository failed. @info:statusPulled Bazaar repository. @info:statusPulling Bazaar repository... @info:statusPush of Bazaar repository failed. @info:statusPushed Bazaar repository. @info:statusPushing Bazaar repository... @info:statusRemoved files from Bazaar repository. @info:statusRemoving files from Bazaar repository... @info:statusRemoving of files from Bazaar repository failed. @info:statusReview Changes failed. @info:statusReviewed Changes. @info:statusReviewing Changes... @info:statusRunning Bazaar Log failed. @info:statusRunning Bazaar Log... @info:statusUpdate of Bazaar repository failed. @info:statusUpdated Bazaar repository. @info:statusUpdating Bazaar repository... @item:inmenuBazaar Add... @item:inmenuBazaar Commit... @item:inmenuBazaar Delete @item:inmenuBazaar Log @item:inmenuBazaar Pull @item:inmenuBazaar Push @item:inmenuBazaar Update @item:inmenuShow Local Bazaar Changes Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2013-06-13 09:32+0400
Last-Translator: Alexander Lakhin <exclusion@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 1.5
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Файлы добавлены в хранилище Bazaar. Добавление файлов в хранилище Bazaar... Добавить файлы в хранилище Bazaar не удалось. Журнал Bazaar закрыт. Зафиксировать изменения Bazaar не удалось. Изменения Bazaar зафиксированы. Фиксация изменений Bazaar... Вытянуть изменения Bazaar не удалось. Изменения Bazaar вытянуты. Вытягивание изменений Bazaar... Вытолкнуть изменения Bazaar не удалось. Изменения Bazaar вытолкнуты. Выталкивание изменений Bazaar... Файлы удалены из хранилища Bazaar. Удаление файлов из хранилища Bazaar... Удалить файлы из хранилища Bazaar не получилось. Пересмотреть изменения не удалось. Изменения пересмотрены. Пересмотр изменений... Считать журнал изменений Bazaar не удалось. Считывание журнала изменений Bazaar... Обновить хранилище Bazaar не удалось. Хранилище Bazaar обновлено. Обновление хранилища Bazaar... Bazaar: добавить... Bazaar: зафиксировать... Bazaar: удалить Журнал Bazaar Bazaar: вытянуть Bazaar: вытолкнуть Bazaar: обновить Показать локальные изменения Bazaar 