��          �      L      �  m   �     /  "   <  F   _  F   �  F   �  J   4  N     R   �     !  %   2  $   X  #   }  $   �  %   �  &   �  �        �  2  �  -   �       ]   /  �   �  �   *	  �   �	  �   �
  �   )  �   �  )   �     �     �          )     >     [  /   n  '   �                                
                          	                                          Close the encrypted container; partitions inside will disappear as they had been unpluggedLock the container Eject medium Finds devices whose name match :q: Lists all devices and allows them to be mounted, unmounted or ejected. Lists all devices which can be ejected, and allows them to be ejected. Lists all devices which can be mounted, and allows them to be mounted. Lists all devices which can be unmounted, and allows them to be unmounted. Lists all encrypted devices which can be locked, and allows them to be locked. Lists all encrypted devices which can be unlocked, and allows them to be unlocked. Mount the device Note this is a KRunner keyworddevice Note this is a KRunner keywordeject Note this is a KRunner keywordlock Note this is a KRunner keywordmount Note this is a KRunner keywordunlock Note this is a KRunner keywordunmount Unlock the encrypted container; will ask for a password; partitions inside will appear as they had been plugged inUnlock the container Unmount the device Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2011-01-12 23:14+0300
Last-Translator: Andrey Cherepanov <skull@kde.ru>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Заблокировать контейнер Извлечь носитель Поиск устройств, названия которых соответствуют :q: Показать список всех устройств, в котором их можно подключать, отключать и извлекать. Показать список всех устройств, которые могут быть извлечены, с возможностью их извлечения. Показать список всех устройств, которые могут быть подключены, с возможностью их подключения. Показать список всех устройств, которые могут быть отключены, с возможностью их отключения. Показать список всех шифруемых устройств, которые могут быть заблокированы, с возможностью их заблокировать. Показать список всех шифруемых устройств, которые могут быть разблокированы, с возможностью их разблокировать Подключить устройство устройство извлечь заблокировать подключить разблокировать отключить Разблокировать контейнер Отключить устройство 