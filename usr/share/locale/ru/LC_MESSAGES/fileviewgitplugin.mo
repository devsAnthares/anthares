��    2      �  C   <      H     I     a     w     �     �  A   �  9   �  .   5  F   d  :   �  D   �  $   +  W   P  7   �  3   �  (     4   =  .   r  C   �  3   �  k        �     �     �     �  #   �     	     5	     N	  !   n	     �	  "   �	     �	     �	     �	     
     -
     C
     _
     t
  (   �
  +   �
  5   �
  3     7   P  1   �  1   �     �     �  8       @  C   `     �  :   �  %   �       B   .  .   q  h   �  P   	  f   Z  >   �  �      R   �  1   �  '     O   F  C   �  9   �  4     
   I     T     `          �     �     �     �  h     &   v     �     �     �     �     �  
     *     :   8     s     �  L   �  ;   �  =   +  B   i  <   �  Y   �  D   C     �     �     )             0         -                #              2      ,                    %      .   /   
                        1   	          &   *       '                                                "   +             $   (      !       @action:buttonCheckout @action:buttonCommit @action:buttonCreate Tag @action:buttonPull @action:buttonPush @action:button Add Signed-Off line to the message widgetSign off @info:tooltipA branch with the name '%1' already exists. @info:tooltipA tag named '%1' already exists. @info:tooltipAdd Signed-off-by line at the end of the commit message. @info:tooltipBranch names may not contain any whitespace. @info:tooltipCreate a new branch based on a selected branch or tag. @info:tooltipDiscard local changes. @info:tooltipProceed even if the remote branch is not an ancestor of the local branch. @info:tooltipTag names may not contain any whitespace. @info:tooltipThere are no tags in this repository. @info:tooltipThere is nothing to amend. @info:tooltipYou must enter a commit message first. @info:tooltipYou must enter a tag name first. @info:tooltipYou must enter a valid name for the new branch first. @info:tooltipYou must select a valid branch first. @item:intext Prepended to the current branch name to get the default name for a newly created branchbranch @label:listboxBranch: @label:listboxLocal Branch: @label:listboxRemote Branch: @label:listboxRemote branch: @label:listbox a git remoteRemote: @label:textboxTag Message: @label:textboxTag Name: @option:checkAmend last commit @option:checkCreate New Branch:  @option:checkForce @option:radio Git CheckoutBranch: @option:radio Git CheckoutTag: @title:groupAttach to @title:groupBranch Base @title:groupBranches @title:groupCheckout @title:groupCommit message @title:groupOptions @title:groupTag Information @title:group The remote hostDestination @title:group The source to pull fromSource @title:window<application>Git</application> Checkout @title:window<application>Git</application> Commit @title:window<application>Git</application> Create Tag @title:window<application>Git</application> Pull @title:window<application>Git</application> Push Dialog height Dialog width Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2013-06-13 09:32+0400
Last-Translator: Alexander Lakhin <exclusion@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Извлечение (checkout) Зафиксировать изменения в хранилище Создать метку Загрузка новых версий с сервера Отправить изменения Подпись Ветка с названием '%1' уже существует. Метка «%1» уже существует. Добавить подпись в конце комментария фиксации изменений Названия веток не могут содержать пробелов. Создать новую ветку на основе выбранной ветки или метки Игнорировать локальные изменения Выполнить, даже если удаленная ветка не является родительской для локальной ветки. Название метки не должно содержать пробелов. В этом хранилище нет меток. Нечего редактировать Сначала нужно ввести комментарий фиксации. Сначала нужно ввести название метки. Сначала нужно ввести имя ветки. Сначала нужно выбрать ветку. ветка Ветка: Локальная ветка: Удаленная ветка: Удаленная ветка: Удаленная: Сообщение метки: Название метки: Редактировать комментарий последней фиксации изменений Создать новую ветку:  Принудительно Ветка: Метка: Прикрепить к Базовая ветка Ветки Переключиться на ветку Комментарий фиксации изменений Параметры Свойства метки Удалённый сервер для занесения изменений Источник получения новых версий <application>Git</application> извлечение (chekout) <application>Git</application> фиксация изменений <application>Git</application> присвоить метку <application>Git</application> загрузка новых версий с сервера <application>Git</application> отправить изменения Длина диалога Ширина диалога 