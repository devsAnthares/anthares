��          �      |      �     �  2        B     b       #   �      �     �  %   �       
   &     1     >     ]  �   }       ]   (     �  p   �  :        P  �  \  S   Y  �   �  M   1       5   �  F   �  &   	  *   9	  \   d	  M   �	     
     ,
  E   9
  }   
  +  �
  /   )  �   Y  V   (  /    �   �     C                                                                      
                	           Apply a look and feel package Command line tool to apply look and feel packages. Configure Look and Feel details Copyright 2017, Marco Martin Cursor Settings Changed Download New Look And Feel Packages EMAIL OF TRANSLATORSYour emails Get New Looks... List available Look and feel packages Look and feel tool Maintainer Marco Martin NAME OF TRANSLATORSYour names Reset the Plasma Desktop layout Select an overall theme for your workspace (including plasma theme, color scheme, mouse cursor, window and desktop switcher, splash screen, lock screen etc.) Show Preview This module lets you configure the look of the whole workspace with some ready to go presets. Use Desktop Layout from theme Warning: your Plasma Desktop layout will be lost and reset to the default layout provided by the selected theme. You have to restart KDE for cursor changes to take effect. packagename Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-27 05:46+0100
PO-Revision-Date: 2018-02-01 11:37+0300
Last-Translator: Alexander Potashev <aspotashev@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 2.0
 Использовать указанный пакет оформления Plasma. Инструмент командной строки для изменения внешнего вида рабочей среды. Настройка внешнего вида рабочей среды Plasma © Marco Martin, 2017 Изменение темы курсоров мыши Загрузка новых пакетов оформлений Plasma aspotashev@gmail.com,KekcuHa@gmail.com Загрузить оформления... Показать список доступных пакетов оформлений Plasma. Настройка внешнего вида рабочей среды Plasma Сопровождающий Marco Martin Александр Поташев,Александр Яворский Восстановить стандартное расположение виджетов рабочего стола Plasma. Выберите общий вид рабочей среды, в том числе — тему рабочего стола, цветовую схему, курсоры мыши, переключатели окон и рабочих столов, заставку и экран блокировки. Предварительный просмотр Этот модуль позволяет выбрать общий вид рабочей среды, используя заранее продуманные разработчиками параметры. Применить расположение виджетов из оформления Предупреждение: настроенное вами расположение виджетов и другие параметры рабочего стола будут потеряны и заменены на стандартные из выбранного пакета оформления. Чтобы изменение темы курсоров мыши вступило в силу, необходимо перезапустить KDE. имя_пакета 