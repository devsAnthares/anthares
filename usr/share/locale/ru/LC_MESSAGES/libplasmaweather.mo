��          <      \       p      q   *   �   /   �   ;  �   <   #  d   `  Z   �                   Cannot find '%1' using %2. Connection to %1 weather server timed out. Weather information retrieval for %1 timed out. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-13 03:17+0100
PO-Revision-Date: 2011-01-31 15:19+0300
Last-Translator: Alexander Potashev <aspotashev@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 1.2
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Не удалось найти «%1» используя %2. Истекло время ожидания соединения с сервером погоды %1. Истекло время ожидания информации о погоде для %1. 