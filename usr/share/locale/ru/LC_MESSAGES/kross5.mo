��    $      <  5   \      0     1  +   E     q  4   �  &   �     �     �                     5     :     P     X  ,   u  3   �     �     �               !  '   .     V     u     {     �     �     �     �     �     �  &   �       B        b  C  y  
   �     �     �  '   �     	  '   (	     P	     d	     v	  Q   �	     �	  1   �	     
  3   3
  Z   g
  `   �
  H   #  A   l  	   �     �     �  I   �  �   ,     �  /   �  8        =  /   L     |  1   �     �  N   �     %  B   1     t                                       
                              !                                  $      #                               	                           "             @info:creditAuthor @info:creditCopyright 2006 Sebastian Sauer @info:creditSebastian Sauer @info:shell command-line argumentThe script to run. @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: application descriptionCommand-line utility to run Kross scripts. application nameKross Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2014-06-30 00:00+0400
Last-Translator: Alexander Potashev <aspotashev@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Автор © Sebastian Sauer, 2006 Sebastian Sauer Сценарий для запуска. Главное Создать новый скрипт. Добавить... Отменить? Комментарий: mok@kde.ru,shaforostoff@kde.ru,skull@kde.ru,leon@asplinux.ru,darkstar@altlinux.ru Правка Изменить выбранный скрипт. Изменить... Запустить выбранный скрипт. Не удалось создать скрипт для интерпретатора «%1» Не удалось определить интерпретатор для скрипта «%1» Не удалось загрузить интерпретатор «%1» Не удалось открыть файл скрипта «%1» Файл: Значок: Интерпретатор: Уровень безопасности интерпретатора Ruby Григорий Мохин,Николай Шафоростов,Андрей Черепанов,Леонид Кантер,Альберт Валиев Название: Функция «%1» не существует Неизвестный интерпретатор «%1» Удалить Удалить выбранный скрипт. Запустить Файл скрипта «%1» не найден. Остановить Остановить выполнение выбранного скрипта. Текст: Программа для запуска сценариев Kross. Kross 