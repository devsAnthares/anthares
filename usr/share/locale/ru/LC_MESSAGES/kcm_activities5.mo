��    *      l  ;   �      �     �     �  )   �               /     H     ^  #   ~     �  
   �     �     �  %   �  +        E     Z     m  @   z     �     �     �     �               '     ,     9     ?     _     e  .   m     �  F   �  (   �  	   '  	   1  	   ;  '   E  7   m  "   �  �  �     �	  )   �	  '   

     2
     E
     R
     f
  !   u
  *   �
     �
     �
  "   �
  %     %   :  ]   `  2   �      �       �   $  %   �     �     �  (     1   7     i     z     �     �  B   �     �  $     l   :  9   �  �   �  ]   n  !   �     �            ;   #  
   _                   $                                 %                      *                                               	   #   )   "             &           
                        '             (   !    &Do not remember @actionWalk through activities @actionWalk through activities (Reverse) @action:buttonApply @action:buttonCancel @action:buttonChange... @action:buttonCreate @title:windowActivity Settings @title:windowCreate a New Activity @title:windowDelete Activity Activities Activity information Activity switching Are you sure you want to delete '%1'? Blacklist all applications not on this list Clear recent history Create activity... Description: Error loading the QML files. Check your installation.
Missing %1 For a&ll applications Forget a day Forget everything Forget the last hour Forget the last two hours General Icon Keep history Name: O&nly for specific applications Other Privacy Private - do not track usage for this activity Remember opened documents: Remember the current virtual desktop for each activity (needs restart) Shortcut for switching to this activity: Shortcuts Switching Wallpaper for in 'keep history for 5 months'for  unit of time. months to keep the history month  months unlimited number of monthsforever Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-08-12 00:19+0300
Last-Translator: Alexander Potashev <aspotashev@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 2.0
 &Не запоминать На одну комнату вперёд На одну комнату назад Применить Отмена Изменить... Создать Настройка комнаты Создание новой комнаты Удаление комнаты Комнаты Сведения о комнате Переключение комнат Удалить комнату «%1»? Заблокировать все приложения, кроме перечисленных Очистить последнюю историю Создать комнату... Описание: Ошибка загрузки файла QML. Проверьте правильность установки.
Отсутствует файл «%1». Для &всех приложений Стереть день Стереть всё Стереть последний час Стереть последние два часа Основное Значок Хранить историю Название: Толь&ко для определённых приложений Дополнительно Конфиденциальность Конфиденциальная (не запоминать статистику использования) Запоминать открытые документы: Запоминать текущий рабочий стол для каждой комнаты (потребуется перезапуск) Комбинация клавиш для переключения на эту комнату: Комбинации клавиш Переключение Обои в течение   месяца  месяцев  месяцев  месяца вечно 