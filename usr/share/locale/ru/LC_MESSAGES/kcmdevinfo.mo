��    ]           �      �  !   �       	   !     +  S   4  	   �     �     �     �     �     �     �     �     �     �     	  J   $	     o	     }	     �	      �	  	   �	  
   �	     �	     �	     �	     �	     
     
     
  L   
  	   l
  	   v
  
   �
  
   �
  
   �
     �
     �
     �
     �
     �
     �
     �
     �
     	     (  	   +     5     G     S     [     i     m     }     �     �  
   �  	   �     �  
   �     �     �     �     �     �     	          +     ?     \     r     x     |     �  H   �     �     �     �     �     �       
     &        A  "   T     w     �     �     �     �       	     ;  (  9   d  /   �  	   �     �  :   �          +  
   C     N     c      x     �     �  E   �       *   !      L     m     �     �  &   �  8   �          5  !   U  #   w     �  2   �     �  	   �  e   �  	   Z  	   d  
   n  
   y  
   �     �      �     �  )   �  C   �  #   @     d  7   m  9   �     �     �     �          (  !   D     f     m     �     �  !   �     �     �                (     -  >   2  3   q  I   �  5   �  2   %  /   X  8   �  1   �     �     �             d     +   p     �     �  2   �  )   
     4  
   9     D     Y     `     g     z     �     �     �     �  :   �           B   "       (   W   #   $   %            [   @      L   M   '   6       >   T           <   9   G   &           A      .   8   R       7       J             ?   4                   H   -                      F          5   )   N   D       ,       K             3       U   =   
       ;       O   :           +           !   Z   /      V   Y              Q         E       1       X             ]       0      P         \   I          	          C       S   2       *    
Solid Based Device Viewer Module (c) 2010 David Hubner AMD 3DNow ATI IVEC Available space out of total partition size (percent used)%1 free of %2 (%3% used) Batteries Battery Type:  Bus:  Camera Cameras Charge Status:  Charging Collapse All Compact Flash Reader Default device tooltipA Device Device Information Device Listing Whats ThisShows all the devices that are currently listed. Device Viewer Devices Discharging EMAIL OF TRANSLATORSYour emails Encrypted Expand All File System File System Type:  Fully Charged Hard Disk Drive Hotpluggable? IDE IEEE1394 Info Panel Whats ThisShows information about the currently selected device. Intel MMX Intel SSE Intel SSE2 Intel SSE3 Intel SSE4 Keyboard Keyboard + Mouse Label:  Max Speed:  Memory Stick Reader Mounted At:  Mouse Multimedia Players NAME OF TRANSLATORSYour names No No Charge No data available Not Mounted Not Set Optical Drive PDA Partition Table Primary Processor %1 Processor Number:  Processors Product:  Raid Removable? SATA SCSI SD/MMC Reader Show All Devices Show Relevant Devices Smart Media Reader Storage Drives Supported Drivers:  Supported Instruction Sets:  Supported Protocols:  UDI:  UPS USB UUID:  Udi Whats ThisShows the current device's UDI (Unique Device Identifier) Unknown Drive Unused Vendor:  Volume Space: Volume Usage:  Yes kcmdevinfo name of something is not knownUnknown no device UDINone no instruction set extensionsNone platform storage busPlatform unknown battery typeUnknown unknown deviceUnknown unknown device typeUnknown unknown storage busUnknown unknown volume usageUnknown xD Reader Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-23 03:11+0200
PO-Revision-Date: 2016-03-25 02:30+0300
Last-Translator: Alexander Potashev <aspotashev@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 
Модуль просмотра устройств Solid © Дэвид Хабнер (David Hubner), 2010 AMD 3DNow ATI IVEC свободно %1 из %2 (использовано %3%) Батареи Тип батареи:  Шина:  Фотокамера Фотокамеры Состояние заряда: Заряжается Свернуть все Устройство чтения карт памяти Compact Flash Устройство Сведения об устройстве Список устройств. Список устройств Устройства Разряжается aspotashev@gmail.com,yur.arh@gmail.com Зашифрованный логический диск Развернуть все Файловая система Файловая система:  Полностью заряжена Жёсткий диск Подключается в любое время? IDE IEEE 1394 Эта панель показывает сведения о выбранном устройстве. Intel MMX Intel SSE Intel SSE2 Intel SSE3 Intel SSE4 Клавиатура Клавиатура и мышь Метка:  Максимальная частота:  Устройство чтения карт памяти MemoryStick Точка подключения:  Мышь Мультимедийные проигрыватели Александр Поташев,Юрий Ефремов Нет Без заряда Нет данных Не подключено Не установлена Оптический привод КПК Таблица разделов Основная Процессор %1 Номер процессора:  Процессоры Устройство:  RAID Сменный носитель? SATA SCSI Устройство чтения карт памяти SD/MMC Показать все типы устройств Показать присутствующие типы устройств Устройство чтения смарт-карт Устройства хранения данных Поддерживаемые драйверы:  Поддерживаемые наборы команд:  Поддерживаемые протоколы:  UDI:  ИБП USB UUID:  UDI (Unique Device Identifier, уникальный идентификатор устройства) Неизвестное устройство Не используется Производитель:  Используемое пространство: Способ использования:  Да kcmdevinfo неизвестно Нет Нет Платформа Неизвестно неизвестно неизвестно неизвестно Неизвестно Устройство чтения карт памяти xD 