��    &      L  5   |      P      Q     r     ~  -   �  #   �  #   �  ?     1   S     �     �     �  H   �  L   �     F  V   N  N   �     �  
                   (     >     W  2   _  
   �     �  )   �  8   �  #      .   D  -   s  D   �  6   �  0     A   N  =   �  9   �  ;  	  g   D     �  �   �  -   n     �     �  '   �  #   �  4   
     ?  .   T  O   �  Q   �     %  
   2     =     A  5   R  !   �  2   �  )   �  /        7  !   I     k  8   �  \   �       r   2  D   �  S   �  Y   >  
   �     �     �     �     �                  "                            
       $                      !                &                                      %      	           #                                %1 notification %1 notifications %1 of %2 %3 %1 running job %1 running jobs &Configure Event Notifications and Actions... 10 seconds ago, keep short10 s ago 30 seconds ago, keep short30 s ago A button tooltip; expands the item to show detailsShow Details A button tooltip; hides item detailsHide Details Clear Notifications Copy Copy Link Address Either just 1 dir or m of n dirs are being processed1 dir %2 of %1 dirs Either just 1 file or m of n files are being processed1 file %2 of %1 files History How much many bytes (or whether unit in the locale has been copied over total%1 of %2 Indicator that there are more urls in the notification than previews shown+%1 Information Job Failed Job Finished More Options... No new notifications. No notifications or jobs Open... Placeholder is job name, eg. 'Copying'%1 (Paused) Select All Show a history of notifications Show application and system notifications Speed and estimated time to completion%1 (%2 remaining) Track file transfers and other jobs Use custom position for the notification popup minutes ago, keep short%1 min ago %1 min ago notification was added n days ago, keep short%1 day ago %1 days ago notification was added yesterday, keep shortYesterday notification was just added, keep shortJust now placeholder is row description, such as Source or Destination%1: the job, which can be anything, failed to complete%1: Failed the job, which can be anything, has finished%1: Finished Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:07+0100
PO-Revision-Date: 2018-02-01 10:44+0300
Last-Translator: Alexander Potashev <aspotashev@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 %1 уведомление %1 уведомления %1 уведомлений %1 уведомление %1 из %2 %3 %1 выполняющееся задание %1 выполняющихся задания %1 выполняющихся заданий %1 выполняющееся задание Настроить &уведомления... 10 с назад 30 с назад Показать подробности Скрыть подробности Очистить список уведомлений Копировать Скопировать адрес ссылки %2 из %1 папки %2 из %1 папок %2 из %1 папок %1 папка %2 из %1 файла %2 из %1 файлов %2 из %1 файлов %1 файл Журнал %1 из %2 +%1 Сведения Не удалось выполнить задание Задание завершено Дополнительные параметры... Нет новых уведомлений. Нет уведомлений и заданий Открыть... (приостановлено) %1 Выделить всё Показывать журнал уведомлений Показывать уведомления от приложений и от системы %1 (осталось %2) Показывать ход выполнения операций с файлами и других заданий Выбрать место для показа уведомлений %1 мин назад %1 мин назад %1 мин назад %1 мин назад %1 день назад %1 дня назад %1 дней назад %1 день назад Вчера Только что %1: %1: ошибка %1: завершено 