��          �      |      �     �     �     �                 ;  	   \     f     �  &   �     �     �     �  	          �   %  �   �  t   ?  7   �  +   �       �         /   #     S  >   `  5   �     �     �       +     q   >  1   �  7   �  c   	  %   ~	     �	    �	  �   �
  �   �     �  ~   �  
   "                                      
   	                                                         min Act like Always Define a special behavior Don't use special settings EMAIL OF TRANSLATORSYour emails Hibernate NAME OF TRANSLATORSYour names Never shutdown the screen Never suspend or shutdown the computer PC running on AC power PC running on battery power PC running on low battery Shut down Suspend The Power Management Service appears not to be running.
This can be solved by starting or scheduling it inside "Startup and Shutdown" The activity service is not running.
It is necessary to have the activity manager running to configure activity-specific power management behavior. The activity service is running with bare functionalities.
Names and icons of the activities might not be available. This is meant to be: Act like activity %1Activity "%1" Use separate settings (advanced users only) after Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-09 03:03+0200
PO-Revision-Date: 2017-11-15 21:51+0300
Last-Translator: Alexander Potashev <aspotashev@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
  мин Действовать по аналогии с Всегда Определить специальное поведение Не применять особых настроек juliette.tux@gmail.com Спящий режим Julia Dronova Никогда не гасить экран Никогда не выключать компьютер и не переводить в ждущий режим Компьютер работает от сети Компьютер работает от батареи Компьютер работает от батареи с низким уровнем заряда Выключить компьютер Ждущий режим Похоже, что служба управления питанием не запущена.
Это можно исправить, запустив службу или запланировав её запуск в настройках «Запуск и завершение». Служба комнат не запущена.
Для индивидуальных настроек поведения менеджера управления питанием эта служба должна быть запущена. Служба активных действий запущена с минимальным функционалом.
Названия и иконки комнат могут быть недоступны. Комната «%1» Использовать отдельные настройки (только для опытных пользователей) после 