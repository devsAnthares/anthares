��            )   �      �  ;   �  J   �          /  ~   7  A   �     �       )        G     X     i      q     �     �     �  
   �     �  
   �     �             "   <     _  	   y     �     �     �  ;  �     �     �     �       �     �   �     �	  /   �	  \   �	  .   )
  ,   X
     �
  7   �
  +   �
                  a   ;     �     �     �  K   �  M   *  A   x     �     �  )   �                                           
                                                                                            	        %1 is the full user name, %2 is the user login name%1 (%2) %1 is the name of a detail about the current action provided by polkit%1: (c) 2009 Red Hat, Inc. Action: An application is attempting to perform an action that requires privileges. Authentication is required to perform this action. Another client is already authenticating, please try again later. Application: Authentication Required Authentication failure, please try again. Click to edit %1 Click to open %1 Details EMAIL OF TRANSLATORSYour emails Former maintainer Jaroslav Reznik Lukáš Tinkl Maintainer NAME OF TRANSLATORSYour names P&assword: Password for %1: Password for root: Password or swipe finger for %1: Password or swipe finger for root: Password or swipe finger: Password: PolicyKit1 KDE Agent Select User Vendor: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:22+0100
PO-Revision-Date: 2015-04-22 02:58+0300
Last-Translator: Alexander Potashev <aspotashev@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 %1 (%2) %1: © Red Hat, Inc., 2009 Действие: Приложение пытается выполнить действие, которое требует дополнительных привилегий. Для этого требуется аутентификация. Аутентификация уже выполняется другим процессом, повторите попытку позже. Приложение: Требуется аутентификация Сбой при проверке подлинности, попробуйте ещё раз. Нажмите, чтобы изменить %1 Нажмите, чтобы открыть %1 Подробности alex239@gmail.com,shaforostoff@kde.ru,yur.arh@gmail.com Прежний сопровождающий Jaroslav Reznik Lukáš Tinkl Сопровождающий Александр Мелентьев,Николай Шафоростов,Юрий Ефремов П&ароль: Пароль для %1: Пароль для root: Введите пароль или приложите палец для %1: Введите пароль или приложите палец для root: Введите пароль или приложите палец: Пароль: Агент PolicyKit1 от KDE Выберите пользователя Поставщик: 