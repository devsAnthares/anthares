��          �   %   �      0     1  +   G  .   s  6   �  *   �  #     &   (  /   O  2     :   �  K   �  -   9  $   g  '   �     �     �     �     �  #        8     V     j     �  ?  �     �  <   �  B   (  N   k  I   �  5   	  -   :	  :   h	  @   �	  N   �	  z   3
  P   �
  >   �
  7   >     v  7   �     �     �  6   �  /   /     _  *   q  %   �                                                    	   
                                                                        @action:buttonCommit @info:statusAdded files to SVN repository. @info:statusAdding files to SVN repository... @info:statusAdding of files to SVN repository failed. @info:statusCommit of SVN changes failed. @info:statusCommitted SVN changes. @info:statusCommitting SVN changes... @info:statusRemoved files from SVN repository. @info:statusRemoving files from SVN repository... @info:statusRemoving of files from SVN repository failed. @info:statusSVN status update failed. Disabling Option "Show SVN Updates". @info:statusUpdate of SVN repository failed. @info:statusUpdated SVN repository. @info:statusUpdating SVN repository... @item:inmenuSVN Add @item:inmenuSVN Commit... @item:inmenuSVN Delete @item:inmenuSVN Update @item:inmenuShow Local SVN Changes @item:inmenuShow SVN Updates @labelDescription: @title:windowSVN Commit Show updates Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2012-09-24 13:06+0400
Last-Translator: Julia Dronova <juliette.tux@gmail.com>
Language-Team: Русский <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Зафиксировать Файлы добавлены в SVN-репозиторий. Добавление файлов в SVN-репозиторий... Не удалось добавить файлы в SVN-репозиторий. Не удалось зафиксировать изменения в SVN. Изменения зафиксированы в SVN. Фиксация изменений в SVN... Файлы удалены из SVN-репозитория. Удаление файлов из SVN-репозитория... Не удалось удалить файлы из SVN-репозитория. Сбой обновления статуса SVN. Отключаю опцию "Показать SVN обновления". Не удалось обновить файлы из SVN-репозитория. Файлы обновлены из SVN-репозитория. Обновление из SVN-репозитория... Добавить в SVN Зафиксировать изменения в SVN... Удалить из SVN Обновить из SVN Показать локальные изменения Показать обновления из SVN. Описание: Фиксация изменений в SVN Показать обновления 