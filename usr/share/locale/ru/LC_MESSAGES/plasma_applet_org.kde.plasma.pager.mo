��          �   %   �      0  *   1     \  .   s     �     �     �     �     �     �                 
   $     /     5     =     E     ]     t     �     �     �     �  N  �     '  3   �  s   �     O     o  ?   �  /   �  ,   �  &   &     M     c     �     �  !   �     �     �  &   �  =   %	  A   c	  3   �	  (   �	     
     
        
                             	                                                                                          %1 Minimized Window: %1 Minimized Windows: %1 Window: %1 Windows: ...and %1 other window ...and %1 other windows Activity name Activity number Add Virtual Desktop Configure Desktops... Desktop name Desktop number Display: Does nothing General Horizontal Icons Layout: No text Only the current screen Remove Virtual Desktop Selecting current desktop: Show Activity Manager... Shows desktop The pager layoutDefault Vertical Project-Id-Version: plasma_applet_pager
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-19 03:29+0200
PO-Revision-Date: 2017-01-22 03:10+0300
Last-Translator: Alexander Potashev <aspotashev@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 %1 свёрнутое окно: %1 свёрнутых окна: %1 свёрнутых окон: %1 свёрнутое окно: %1 окно: %1 окна: %1 окон: %1 окно: и %1 другое окно. и %1 других окна. и %1 других окон. и %1 другое окно. Название комнаты Номер комнаты Добавить виртуальный рабочий стол Настроить рабочие столы... Название рабочего стола Номер рабочего стола Показывать: Не делать ничего Основное Горизонтальная Значки приложений Ориентация: Без надписи Только текущий экран Удалить виртуальный рабочий стол При выборе текущего рабочего стола: Показать диспетчер комнат... Показать рабочий стол По умолчанию Вертикальная 