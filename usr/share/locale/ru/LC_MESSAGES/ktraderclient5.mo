��    	      d      �       �   6   �   3     8   L     �      �     �     �  "   �  H    o   K  @   �  e   �     b     n     �  9   �  >   �                         	                     A command-line tool for querying the KDE trader system A constraint expressed in the trader query language A servicetype, like KParts/ReadOnlyPart or KMyApp/Plugin David Faure EMAIL OF TRANSLATORSYour emails KTraderClient NAME OF TRANSLATORSYour names Output only paths to desktop files Project-Id-Version: ktraderclient
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-01 02:51+0200
PO-Revision-Date: 2016-10-24 00:41+0300
Last-Translator: Alexander Potashev <aspotashev@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Консольная программа запроса системы обработки типов MIME в KDE Выражение на языке запросов службы Обрабатывающая служба, например, KParts/ReadOnlyPart или KMyApp/Plugin David Faure leon@asplinux.ru,skull@kde.ru KTraderClient Леонид Кантер,Андрей Черепанов Вывести только пути к файлам .desktop. 