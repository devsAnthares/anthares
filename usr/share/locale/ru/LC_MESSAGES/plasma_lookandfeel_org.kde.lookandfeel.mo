��    ,      |  ;   �      �     �     �  .   �  5        7     >     N     T  	   a     k     �     �     �  1   �     �     �                  '   0     X     [     d  '   k     �     �     �     �  5   �     �          
             $   ,  A   Q  �   �     y     �  C   �  '   �     �       �  "     
  
   #
  6   .
  +   e
     �
  #   �
     �
  :   �
       >     %   ^  '   �  +   �  �   �  
   �  0   �  3        B  0   `     �     �     �     �  �   �  !   �     �     �     �      "   +     N     f  !   }  /   �     �     �  
   �     �  0     *   I     t     �     �             #                    '                +   $       &      )                	                                    !   (      "                %                                    
   *       ,        %1% Back Button to change keyboard layoutSwitch layout Button to show/hide virtual keyboardVirtual Keyboard Cancel Caps Lock is on Close Close Search Configure Configure Search Plugins Desktop Session: %1 Different User Keyboard Layout: %1 Logging out in 1 second Logging out in %1 seconds Login Login Failed Login as different user Logout No media playing Nobody logged in on that sessionUnused OK Password Reboot Reboot in 1 second Reboot in %1 seconds Recent Queries Remove Restart Shutdown Shutting down in 1 second Shutting down in %1 seconds Start New Session Suspend Switch Switch Session Switch User Textfield placeholder textSearch... Textfield placeholder text, query specific KRunnerSearch '%1'... This is the first text the user sees while starting in the splash screen, should be translated as something short, is a form that can be seen on a product. Plasma is the project name so shouldn't be translated.Plasma made by KDE Unlock Unlocking failed User logged in on console (X display number)on TTY %1 (Display %2) User logged in on console numberTTY %1 Username Username (location)%1 (%2) Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-09 03:21+0100
PO-Revision-Date: 2017-10-16 18:41+0300
Last-Translator: Alexander Potashev <aspotashev@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 2.0
 %1% Назад Сменить раскладку клавиатуры Виртуальная клавиатура Отмена Включён режим Caps Lock Закрыть Закрыть строку поиска и запуска Настроить Настроить строку поиска и запуска Графический сеанс: %1 Выбрать пользователя Раскладка клавиатуры: %1 Завершение сеанса через %1 секунду Завершение сеанса через %1 секунды Завершение сеанса через %1 секунд Завершение сеанса через %1 секунду Войти Не удалось войти в систему Вход с выбором пользователя Завершить сеанс Ничего не воспроизводится Не используется ОК Пароль Перезагрузить Перезагрузка через %1 секунду Перезагрузка через %1 секунды Перезагрузка через %1 секунд Перезагрузка через %1 секунду Последние запросы Удалить Перезагрузить Выключить Выключение компьютера через %1 секунду Выключение компьютера через %1 секунды Выключение компьютера через %1 секунд Выключение компьютера через %1 секунду Начать новый сеанс Ждущий режим Переключить Переключить сеанс Переключить пользователя Поиск... Поиск... (%1) KDE Plasma Разблокировать Не удалось разблокировать на терминале %1 (экран %2) Терминал %1 Имя пользователя %1 (%2) 