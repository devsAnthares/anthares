��          �   %   �      P     Q     b     w     �     �     �  
   �     �     �     �     �     �  /        3     Q     p     v     �     �     �     �     �     �     �       D  %  5   j  7   �     �  ;   �     4  3   L     �     �     �     �     �  %   �  \     0   d  /   �  
   �     �     �     �  E   	  F   I	  Q   �	  R   �	  5   5
  6   k
                            
                                                                                             	    %1 file %1 files %1 folder %1 folders %1 of %2 processed %1 of %2 processed at %3/s %1 processed %1 processed at %2/s Appearance Behavior Cancel Clear Configure... Finished Jobs List of running file transfers/jobs (kuiserver) Move them to a different list Move them to a different list. Pause Remove them Remove them. Resume Show all jobs in a list Show all jobs in a list. Show all jobs in a tree Show all jobs in a tree. Show separate windows Show separate windows. Project-Id-Version: kuiserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-10-11 02:06+0400
Last-Translator: Alexander Potashev <aspotashev@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 1.1
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 %1 файл %1 файла %1 файлов %1 файл %1 папка %1 папки %1 папок %1 папка Обработано %1 из %2 Обработано %1 из %2 на скорости %3/с Обработано %1 Обработано %1 на скорости %2/с Внешний вид Поведение Отменить Очистить Настроить... Выполненные задания Список выполняющихся заданий и операций с файлами Перенести в другой список. Перенести в другой список Пауза Удалить Удалить Продолжить Показывать все задания в одном списке Показывать все задания в одном списке. Показывать все задания в иерархическом виде Показывать все задания в иерархическом виде. Показывать в отдельных окнах Показывать в отдельных окнах. 