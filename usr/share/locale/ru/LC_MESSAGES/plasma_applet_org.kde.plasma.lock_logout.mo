��          �      L      �  &   �  +   �       @     	   ]     g     �     �     �     �  (   �     �     �  ,   �               +     7  ;  ;  c   w  }   �     Y     j     {  M   �     �  "        $  %   ?  f   e     �  >   �  h        {  /   �  /   �     �        
                          	                                                                  Do you want to suspend to RAM (sleep)? Do you want to suspend to disk (hibernate)? General Heading for list of actions (leave, lock, shutdown, ...)Actions Hibernate Hibernate (suspend to disk) Leave Leave... Lock Lock the screen Logout, turn off or restart the computer No Sleep (suspend to RAM) Start a parallel session as a different user Suspend Switch User Switch user Yes Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2015-08-20 17:49+0300
Last-Translator: Alexander Potashev <aspotashev@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Перейти в спящий режим (с сохранением состояния в ОЗУ)? Выключить компьютер, предварительно сохранив его состояние на диск? Основное Действия Спящий режим Выключить с сохранением состояния на диск Завершить работу Завершить работу... Заблокировать Заблокировать экран Завершить сеанс, выключить или перезагрузить компьютер Нет Приостановить с сохранением в ОЗУ Начать параллельный сеанс от имени другого пользователя Ждущий режим Переключить пользователя Переключить пользователя Да 