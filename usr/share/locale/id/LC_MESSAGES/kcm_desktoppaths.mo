��            )   �      �  �   �  	   G     Q     a     w       	   �     �  	   �     �  %   �     �     �                    %     .  X   =  S   �  �   �  I   �  F   �  E   >  H   �  B   �  :     7   K  �  �  �   (	     �	     

     "
     8
     @
     N
     V
     d
     l
     z
     �
     �
  
   �
     �
     �
     �
     �
  T   �
  Q     �   e  K     H   e  H   �  J   �  I   B     �     �                                                        	                                                                 
                 <h1>Paths</h1>
This module allows you to choose where in the filesystem the files on your desktop should be stored.
Use the "Whats This?" (Shift+F1) to get help on specific options. Autostart Autostart path: Confirmation Required Desktop Desktop path: Documents Documents path: Downloads Downloads path: Move files from old to new placeMove Move the directoryMove Movies Movies path: Music Music path: Pictures Pictures path: The path for '%1' has been changed.
Do you want the files to be moved from '%2' to '%3'? The path for '%1' has been changed.
Do you want to move the directory '%2' to '%3'? This folder contains all the files which you see on your desktop. You can change the location of this folder if you want to, and the contents will move automatically to the new location as well. This folder will be used by default to load or save documents from or to. This folder will be used by default to load or save movies from or to. This folder will be used by default to load or save music from or to. This folder will be used by default to load or save pictures from or to. This folder will be used by default to save your downloaded items. Use the new directory but do not move anythingDo not Move Use the new directory but do not move filesDo not Move Project-Id-Version: kcm_desktoppaths
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-09 07:18+0200
PO-Revision-Date: 2017-12-27 21:53+0700
Last-Translator: Wantoyo <wantoyek@gmail.com>
Language-Team: Indonesian <kde-i18n-doc@kde.org>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 <h1>Alur</h1>
Modul ini memungkinkan anda untuk memilih tempat di dalam sistem berkas untuk menyimpan berkas di desktop anda.
Gunakan "Apakah Ini?" (Shift+F1) untuk mendapatkan bantuan tentang opsi tertentu. Jalankan Otomatis Alur jalankan otomatis: Konfirmasi Diperlukan Desktop Alur desktop: Dokumen Alur dokumen: Unduhan Alur unduhan: Pindah Pindah Film Alur film: Musik Alur musik: Gambar Alur gambar: Alur untuk '%1' telah diubah.
Anda ingin berkas untuk dipindahkan dari '%2' ke '%3'? Alur untuk '%1' telah diubah.
Anda ingin memindahkan direktori dari '%2' ke '%3'? Folder ini berisi semua berkas yang anda lihat di desktop anda. Anda dapat mengubah lokasi folder ini jika anda mau, dan isinya akan secara otomatis berpindah ke lokasi yang baru. Folder ini akan digunakan sebagai baku untuk memuat atau menyimpan dokumen. Folder ini akan digunakan sebagai baku untuk memuat atau menyimpan film. Folder ini akan digunakan sebagai baku untuk memuat atau menyimpan musik Folder ini akan digunakan sebagai baku untuk memuat atau menyimpan gambar. Folder ini akan digunakan sebagai baku untuk menyimpan item unduhan anda. Jangan Pindah Jangan Pindah 