��          �   %   �      0  U   1     �     �  
   �     �     �     �     �     �     �  	   �                     .  )   4  (   ^  '   �  "   �     �     �  9   �  J   #  �  n     <     P     _     q     �     �     �     �     �     �     �     �                  -   $  +   R  )   ~  '   �     �     �     �     �                                                                            
                           	                        Activities a window is currently on (apart from the current one)Also available on %1 Alphabetically By Activity By Desktop By Program Name Do Not Group Do Not Sort Filters General Grouping and Sorting Grouping: Highlight windows Manually Maximum rows: On %1 Show only tasks from the current activity Show only tasks from the current desktop Show only tasks from the current screen Show only tasks that are minimized Show tooltips Sorting: Which activities a window is currently onAvailable on %1 Which virtual desktop a window is currently onAvailable on all activities Project-Id-Version: plasma_applet_tasks
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2014-06-20 20:39+0700
Last-Translator: Andhika Padmawan <andhika.padmawan@gmail.com>
Language-Team: Indonesian <translation-team-id@lists.sourceforge.net>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 Juga tersedia di %1 Secara Alfabet Menurut Aktivitas Menurut Desktop Menurut Nama Program Jangan Kelompokkan Jangan Urutkan Filter Umum Pengelompokkan dan Pengurutan Pengelompokkan: Sorot jendela Secara Manual Baris maksimum: Di %1 Hanya tampilkan tugas dari aktivitas saat ini Hanya tampilkan tugas dari desktop saat ini Hanya tampilkan tugas dari layar saat ini Hanya tampilkan tugas yang diminimalkan Tampilkan tip alat Pengurutan: Tersedia di %1 Tersedia di semua aktivitas 