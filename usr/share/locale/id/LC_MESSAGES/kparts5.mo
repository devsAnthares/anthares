��          �   %   �      `     a  :   i     �     �  =   �          *     G     [     t     �  6   �     �     �  #   �           >     F     T     d     �     �  ;   �  V   �  G   %     m  �  v       ?   %     e  
   u  2   �     �     �     �     �     �     �      �             #   .     R     m     v     �     �     �     �  8   �  `   �  Q   Z	     �	                                                  
                                                                   	       &Search <qt>Do you want to search the Internet for <b>%1</b>?</qt> @action:inmenuOpen &with %1 @infoOpen '%1'? @info:whatsthisThis is the file name suggested by the server @label File nameName: %1 @label Type of fileType: %1 @label:button&Open @label:button&Open with @label:button&Open with %1 @label:button&Open with... @label:checkboxRemember action for files of this type Accept Close Document Do you really want to execute '%1'? EMAIL OF TRANSLATORSYour emails Execute Execute File? Internet Search NAME OF TRANSLATORSYour names Reject Save As The Download Manager (%1) could not be found in your $PATH  The document "%1" has been modified.
Do you want to save your changes or discard them? Try to reinstall it  

The integration with Konqueror will be disabled. Untitled Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2014-02-04 20:08+0700
Last-Translator: Andhika Padmawan <andhika.padmawan@gmail.com>
Language-Team: Indonesian <translation-team-id@lists.sourceforge.net>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 &Cari <qt>Apakah anda ingin mencari di Internet untuk <b>%1</b>?</qt> Buka &dengan %1 Buka '%1'? Ini adalah nama berkas yang disarankan oleh server Nama: %1 Tipe: %1 &Buka &Buka dengan &Buka dengan %1 &Buka dengan... Ingat aksi untuk berkas tipe ini Terima Tutup Dokumen Anda yakin ingin mengeksekusi '%1'? andhika.padmawan@gmail.com Eksekusi Eksekusi Berkas? Pencarian Internet Andhika Padmawan Tolak Simpan Sebagai Manajer Unduh (%1) tak dapat ditemukan dalam $PATH anda  Dokumen "%1" telah dimodifikasi.
Apakah anda ingin menyimpan perubahan anda atau mengabaikannya? Coba untuk menginstal ulang itu  

Integrasi dengan Konqueror akan dinonaktifkan. Tak ada judul 