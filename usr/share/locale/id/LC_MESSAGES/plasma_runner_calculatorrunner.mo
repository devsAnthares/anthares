��          4      L       `   m   a   R   �   �  "  f   �  F   <                    Calculates the value of :q: when :q: is made up of numbers and mathematical symbols such as +, -, /, * and ^. The exchange rates could not be updated. The following error has been reported: %1 Project-Id-Version: plasma_runner_calculatorrunner
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-28 05:09+0200
PO-Revision-Date: 2018-02-01 21:18+0700
Last-Translator: Wantoyo <wantoyek@gmail.com>
Language-Team: Indonesian <kde-i18n-doc@kde.org>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 Mengalkulasi nilai dari :q: jika :q: dibuat dari nomor dan simbol matematika seperti +, -, /, * dan ^. Nilai tukar tidak dapat diperbarui. Galat berikut telah dilaporkan: %1 