��    $      <  5   \      0     1  +   E     q  4   �  &   �     �     �                     5     :     P     X  ,   u  3   �     �     �               !  '   .     V     u     {     �     �     �     �     �     �  &   �       B        b  �  y     "     *     I     Y     q     v  	   �     �  	   �     �     �     �  
   �     �  )   	  3   -	     a	     ~	     �	     �	     �	      �	     �	     �	     �	  %   
     6
     <
     R
     [
     x
     }
     �
  6   �
     �
                                       
                              !                                  $      #                               	                           "             @info:creditAuthor @info:creditCopyright 2006 Sebastian Sauer @info:creditSebastian Sauer @info:shell command-line argumentThe script to run. @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: application descriptionCommand-line utility to run Kross scripts. application nameKross Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2014-06-23 21:25+0700
Last-Translator: Andhika Padmawan <andhika.padmawan@gmail.com>
Language-Team: Indonesian <translation-team-id@lists.sourceforge.net>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 Penulis Hak Cipta 2006 Sebastian Sauer Sebastian Sauer Skrip untuk dijalankan. Umum Tambah skrip baru. Tambah... Batal? Komentar: andhika.padmawan@gmail.com Sunting Sunting skrip terpilih. Sunting... Eksekusi skrip terpilih. Gagal membuat skrip untuk penerjemah "%1" Gagal menentukan penerjemah untuk berkas skrip "%1" Gagal memuat penerjemah "%1" Gagal membuka berkas skrip "%1" Berkas: Ikon: Penerjemah: Tingkat keamanan penerjemah Ruby Andhika Padmawan Nama: Tak ada fungsi seperti itu "%1" Tidak ada penerjemah seperti itu "%1" Hapus Hapus skrip terpilih. Jalankan Berkas skrip "%1" tidak ada. Stop Stop eksekusi skrip terpilih. Teks: Utilitas baris perintah untuk menjalankan skrip Kross. Kross 