��    B      ,  Y   <      �  i   �  m        y  b   �     �     	  6        O  7   _     �     �  	   �     �  (   �  )   �  )   (     R     o  Y   u     �     �  �   �      y	  .   �	     �	  
   �	     �	     �	     
     
     !
     5
     B
     J
     c
     r
     w
     �
     �
     �
     �
     �
  	   �
  
   �
     �
     �
  	     
     
   *     5     B  	   `     j     o  
   �  �   �     (  8   8  �   q     �  8   	  ,   B  ,   o  %   �     �  �  �  L   |  ~   �     H  k   g     �     �  3        ;  3   K          �     �     �  $   �  $   �  $        2     O  S   U     �     �  �   �  >   P  .   �     �  
   �     �     �     �          !     4  	   B  #   L     p       %   �     �     �     �     �     �     �     �                ;     H     V     b  !   p  	   �     �     �     �  �   �     ^  F   t  �   �     D  3   T  ;   �  ;   �  &         '     $   !   %          ;   ,          B                 8   #                 7   9       +              )   ?   -   <         6                                    *   2       >                1       @       .   5   3   :   A         &      /                4              0      "             =   (   '         	   
           @info User changed Phonon backendTo apply the backend change you will have to log out and back in again. A list of Phonon Backends found on your system.  The order here determines the order Phonon will use them in. Apply Device List To... Apply the currently shown device preference list to the following other audio playback categories: Audio Hardware Setup Audio Playback Audio Playback Device Preference for the '%1' Category Audio Recording Audio Recording Device Preference for the '%1' Category Backend Colin Guthrie Connector Copyright 2006 Matthias Kretz Default Audio Playback Device Preference Default Audio Recording Device Preference Default Video Recording Device Preference Default/Unspecified Category Defer Defines the default ordering of devices which can be overridden by individual categories. Device Configuration Device Preference Devices found on your system, suitable for the selected category.  Choose the device that you wish to be used by the applications. EMAIL OF TRANSLATORSYour emails Failed to set the selected audio output device Front Center Front Left Front Left of Center Front Right Front Right of Center Hardware Independent Devices Input Levels Invalid KDE Audio Hardware Setup Matthias Kretz Mono NAME OF TRANSLATORSYour names Phonon Configuration Module Playback (%1) Prefer Profile Rear Center Rear Left Rear Right Recording (%1) Show advanced devices Side Left Side Right Sound Card Sound Device Speaker Placement and Testing Subwoofer Test Test the selected device Testing %1 The order determines the preference of the devices. If for some reason the first device cannot be used Phonon will try to use the second, and so on. Unknown Channel Use the currently shown device list for more categories. Various categories of media use cases.  For each category, you may choose what device you prefer to be used by the Phonon applications. Video Recording Video Recording Device Preference for the '%1' Category  Your backend may not support audio recording Your backend may not support video recording no preference for the selected device prefer the selected device Project-Id-Version: kcm_phonon
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2018-01-24 07:23+0700
Last-Translator: Wantoyo <wantoyek@gmail.com>
Language-Team: Indonesian <kde-i18n-doc@kde.org>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 Untuk menerapkan perubahan ujung belakang anda harus keluar lalu masuk lagi. Senarai Ujung Belakang Phonon yang ditemukan di sistem anda. Urutan di sini menentukan urutan yang akan digunakan oleh Phonon. Terapkan Senarai Peranti Ke... Terapkan senarai setelan peranti yang ditampilkan saat ini ke kategori mainbalik audio lainnya berikut ini: Persiapan Perangkat Keras Audio Mainkan Balik Audio Setelan Peranti Mainbalik Audio untuk Kategori '%1' Perekaman Audio Setelan Peranti Perekaman Audio untuk Kategori '%1' Ujung Belakang Colin Guthrie Konektor Copyright 2006 Matthias Kretz Setelan Peranti Mainbalik Audio Baku Setelan Peranti Perekaman Audio Baku Setelan Peranti Perekaman Video Baku Kategori Baku/Tak Ditentukan Tunda Tentukan pengurutan peranti baku yang bisa di tentukan oleh kategori masing-masing. Konfigurasi Peranti Setelan Peranti Peranti yang ditemukan di sistem anda, cocok untuk kategori terpilih. Pilih peranti yang anda ingin untuk digunakan oleh aplikasi. wejick@gmail.com,andhika.padmawan@gmail.com,wantoyek@gmail.com Gagal mengatur peranti keluaran audio terpilih Tengah Depan Kiri Depan Kiri atau Tengah Depan Kanan Depan Kanan atau Tengah Depan Perangkat Keras Peranti Independen Level Masukan Tidak Sah Persiapan Perangkat Keras Audio KDE Matthias Kretz Mono Gian Giovani,Andhika Padmawan,Wantoyo Modul Setelan Phonon Mainkan balik (%1) Suka Profil Tengah Belakang Kiri Belakang Kanan Belakang Perekaman (%1) Tampilkan peranti tingkat lanjut Kiri Samping Kanan Samping Kartu Suara Peranti Suara Penempatan dan Uji Pengeras Suara Subwoofer Coba Tes peranti terpilih Uji %1 Urutan menentukan setelan dari peranti. Jika karena beberapa alasan peranti pertama tak dapat digunakan Phonon akan coba menggunakan yang kedua, dan seterusnya. Channel Tak Diketahui Gunakan senarai peranti yang ditampilkan saat ini untuk kategori lain. Beragam kategori tujuan penggunaan media. Untuk tiap kategori, anda dapat memilih peranti apa yang ingin digunakan oleh aplikasi Phonon. Perekaman Video Setelan Peranti Perekaman Video untuk Kategori '%1' Ujung belakang anda mungkin tidak mendukung perekaman audio Ujung belakang anda mungkin tidak mendukung perekaman video tak ada setelan untuk peranti terpilih pilih peranti terpilih 