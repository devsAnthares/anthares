��            )   �      �  "   �     �  !   �  !   �        
   6     A     \     l  !        �  0   �  8   �     +  !   I     k     �     �     �     �     �            
     
   &  
   1  &   <     c     o  �  �  !   K     m  '   �  %   �     �     �     �            $   .  $   S  9   x  H   �      �  (   	  '   E	  $   m	     �	     �	     �	  '   �	     
     
     
     #
     /
  .   <
  
   k
     v
                                                       
                                                                  	                &Keyboard accelerators visibility: &Top arrow button type: Always Hide Keyboard Accelerators Always Show Keyboard Accelerators Anima&tions duration: Animations Bottom arrow button t&ype: Breeze Settings Center tabbar tabs Drag windows from all empty areas Drag windows from titlebar only Drag windows from titlebar, menubar and toolbars Draw a thin line to indicate focus in menus and menubars Draw focus indicator in lists Draw frame around dockable panels Draw frame around page titles Draw frame around side panels Draw slider tick marks Draw toolbar item separators Enable animations Enable extended resize handles Frames General No Buttons One Button Scrollbars Show Keyboard Accelerators When Needed Two Buttons W&indows' drag mode: Project-Id-Version: KDE Frameworks 5 Applications
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:42+0200
PO-Revision-Date: 2017-12-11 21:56+0700
Last-Translator: Wantoyo <wantoyek@gmail.com>
Language-Team: Indonesian <translation-team-id@lists.sourceforge.net>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 &Ketampakan akselerator keyboard: &Tipe tombol panah atas: Selalu Sembunyikan Akselerator Keyboard Selalu Tampilkan Akselerator Keyboard Durasi ani&masi: Animasi Tipe tombol panah ba&wah: Setelan Breeze Pusat tab-tab bilah-tab Tarik jendela dari semua area kosong Tarik jendela hanya dari bilah judul Seret jendela dari bilah judul, bilah menu dan bilah-alat Gambar sebuah garis tipis untuk menunjukkan fokus di menu dan bilah menu Gambar indikator fokus di daftar Gambar bingkai di sekitar panel dockable Gambar bingkai di sekitar judul halaman Gambar bingkai di sekitar sisi panel Gambar tanda titik penggeser Bukaan bilah-alat pemisah item Aktifkan animasi Aktifkan penanganan ubah ukuran bentang Bingkai Umum Tak ada Tombol Satu Tombol Bilah gulung Tampilkan Akselerator Keyboard Jika Dibutuhkan Dua Tombol M&ode tarik jendela: 