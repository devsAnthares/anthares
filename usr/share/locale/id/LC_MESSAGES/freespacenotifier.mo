��          �      �       0     1  V   6  /   �     �  '   �       5     6   H  +     D   �     �  �     �  �     �     �     �  +   �  /   �     %  9   8     r  ,   �     �  *   �  L   �                  	                   
                      MiB Allows the user to configure the warning notification being shownConfigure Warning... Allows the user to hide this notifier itemHide Enable low disk space warning Is the free space notification enabled. Low Disk Space Minimum free space before user starts being notified. Opens a file manager like dolphinOpen File Manager... Remaining space in your Home folder: %1 MiB The settings dialog main page name, as in 'general settings'General Warn when free space is below: Warns the user that the system is running low on space on his home folder, indicating the percentage and absolute MiB size remainingYour Home folder is running out of disk space, you have %1 MiB remaining (%2%) Project-Id-Version: freespacenotifier
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2017-12-19 07:30+0700
Last-Translator: Wantoyo <wantoyek@gmail.com>
Language-Team: Indonesian <translation-team-id@lists.sourceforge.net>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
  MiB Konfigurasi Peringatan... Sembunyikan Aktifkan peringatan kekurangan ruang kosong Adalah notifikasi ruang kosong yang diaktifkan. Ruang Cakram Habis Ruang kosong minimum sebelum pengguna akan diperingatkan. Buka Manajer Berkas... Ruang tersisa di folder Beranda anda: %1 MiB Umum Peringatkan jika ruang kosong kurang dari: Folder Beranda anda kehabisan ruang cakram, anda mempunyai sisa %1 MiB (%2%) 