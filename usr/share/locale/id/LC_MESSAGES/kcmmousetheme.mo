��          �      l      �     �  �      m   �  �   �  `   �     �     �     
          #      :     [  '   l     �     �     �     �  ?   �  Y     +   p  �  �     9  �   X  t   �     Q  j   g  
   �     �     �  	    	     
	     $	     ?	  '   P	     x	     �	     �	  
   �	  <   �	  J   �	  )   -
                                    	                        
                                      (c) 2003-2007 Fredrik Höglund <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Cursor Theme Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Fredrik Höglund Get new color schemes from the Internet NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. Project-Id-Version: kcminput
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2017-12-12 21:32+0700
Last-Translator: Wantoyo <wantoyek@gmail.com>
Language-Team: Indonesian <kde-i18n-doc@kde.org>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 (c) 2003-2007 Fredrik Höglund <qt>Apakah anda yakin ingin menghapus tema kursor <i>%1</i>?<br />Ini akan menghapus semua berkas yang dipasang oleh tema ini.</qt> <qt>Anda tak dapat menghapus tema yang saat ini anda gunakan.<br />Anda harus mengganti ke tema lainnya dahulu.</qt> (Ukuran tersedia: %1) Sebuah tema bernama %1 telah ada di folder tema ikon anda. Apakah anda ingin menggantinya dengan yang ini? Konfirmasi Setelan Kursor Berubah Tema Kursor Deskripsi Tarik atau Ketik URL Tema andhika.padmawan@gmail.com Fredrik Höglund Dapatkan skema warna baru dari Internet Andhika Padmawan Nama Timpa Tema? Hapus Tema Berkas %1 tidak tampak seperti arsip tema kursor yang valid. Tak dapat mengunduh arsip tema kursor; silakan cek apakah alamat %1 benar. Tak dapat menemukan arsip tema kursor %1. 