��          �      <      �     �     �     �  7  �  �  #  +   �  ^              �     �  x   �  �   %     �               ;     U  �  r     	     (	     A	  -  S	  �  �
  *   y  a   �               *  �   /  �   �     �  
   �  '   �     �     �                          
       	                                                                    &End current session &Restart computer &Turn off computer <h1>Session Manager</h1> You can configure the session manager here. This includes options such as whether or not the session exit (logout) should be confirmed, whether the session should be restored again when logging in and whether the computer should be automatically shut down after session exit by default. <ul>
<li><b>Restore previous session:</b> Will save all applications running on exit and restore them when they next start up</li>
<li><b>Restore manually saved session: </b> Allows the session to be saved at any time via "Save Session" in the K-Menu. This means the currently started applications will reappear when they next start up.</li>
<li><b>Start with an empty session:</b> Do not save anything. Will come up with an empty desktop on next start.</li>
</ul> Applications to be e&xcluded from sessions: Check this option if you want the session manager to display a logout confirmation dialog box. Conf&irm logout Default Leave Option General Here you can choose what should happen by default when you log out. This only has meaning, if you logged in through KDM. Here you can enter a colon or comma separated list of applications that should not be saved in sessions, and therefore will not be started when restoring a session. For example 'xterm:konsole' or 'xterm,konsole'. O&ffer shutdown options On Login Restore &manually saved session Restore &previous session Start with an empty &session Project-Id-Version: kcmsmserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2017-12-11 20:48+0700
Last-Translator: Wantoyo <wantoyek@gmail.com>
Language-Team: Indonesian <kde-i18n-doc@kde.org>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 &Akhiri sesi saat ini &Jalankan ulang komputer &Matikan komputer <h1>Manajer Sesi</h1> Anda dapat mengkonfigurasi pengelola sesi di sini. Ini termasuk opsi seperti apakah perlu atau tidak keluar (log keluar) sesi harus dikonfirmasi, apakah sesi harus dikembalikan lagi ketika log masuk dan apakah komputer harus secara otomatis dimatikan setelah keluar sesi bakunya. <ul>
<li><b>Kembalikan sesi sebelumnya:</b> Akan menyimpan semua aplikasi yang berjalan saat keluar dan mengembalikannya saat hidupkan berikutnya</li>
<li><b>Kembalikan sesi disimpan secara manual: </b> Memungkinkan sesi untuk disimpan kapanpun via "Simpan Sesi" di K-Menu. Ini berarti aplikasi yang dijalankan saat ini akan muncul kembali saat hidupkan berikutnya.</li>
<li><b>Jalankan dengan sesi kosong:</b> Jangan simpan apapun. Akan muncul dengan desktop kosong saat hidupkan berikutnya.</li>
</ul> Aplikasi yang tidak &disertakan dari sesi: Centang opsi ini jika anda ingin pengelola sesi untuk menampilkan kotak dialog konfirmasi keluar. Konf&irmasi keluar Opsi Keluar Baku Umum Di sini anda dapat memilih baku apa yang akan terjadi ketika anda keluar. Ini hanya berarti satu hal, jika anda masuk melalui KDM. Di sini anda dapat memasukkan senarai aplikasi dipisahkan oleh titik dua atau koma yang tidak disimpan dalam sesi, sehingga tidak akan dijalankan ketika mengembalikan sesi. Misalnya 'xterm:konsole' atau 'xterm,konsole'. Ta&warkan opsi matikan Saat Masuk Kembalikan sesi disimpan secara &manual Kembalikan &sesi sebelumnya Jalankan dengan &sesi kosong 