��    .      �  =   �      �  (   �          1     Q     ]     j  *   q  *   �     �     �  �   �  	   �     �     �                7  <   D      �     �     �     �  "   �     �     �       r   &  ;   �  
   �     �     �          "     3     B     I     Y  #   p  )   �     �  !   �  &   �  $   #	  _   H	     �	  �  �	  %   Z     �  !   �  	   �  
   �  	   �     �  #   �          (  �   :  
   2     =     D     R     `     |  <   �     �     �  
   �     �  "        9     >     ]  j   l  /   �  	             "     8     H     ^     q     w     �  !   �  &   �     �       #   .  #   R  e   v     �                                      -                     '   *   &   $   !                                                ,             +   
       	   %   (       )                           "             .   #    (c) 2009, 2014 Solid Device Actions team (c) 2009, Ben Cooksley Action icon, click to change it Action name Action name: Add... All of the contained properties must match Any of the contained properties must match Ben Cooksley Cannot be deleted Command that will trigger the action
This can include any or all of the following case insensitive expands:

%f: The mountpoint for the device - Storage Access devices only
%d: Path to the device node - Block devices only
%i: The identifier of the device Command:  Contains Content Conjunction Content Disjunction Device Interface Match Device type: Devices must match the following parameters for this action: EMAIL OF TRANSLATORSYour emails Edit Parameter Edit... Editing Action '%1' Enter the name for your new action Equals Error Parsing Device Conditions Invalid action It appears that the action name, command, icon or condition are not valid.
Therefore, changes will not be applied. It appears that the predicate for this action is not valid. Maintainer NAME OF TRANSLATORSYour names No Action Selected Parameter type: Port to Plasma 5 Property Match Remove Reset Parameter Save Parameter Changes Solid Action Desktop File Generator Solid Device Actions Control Panel Module Solid Device Actions Editor The device must be of the type %1 The device property %1 must contain %2 The device property %1 must equal %2 Tool to automatically generate Desktop Files from Solid DeviceInterface classes for translation Value name: Project-Id-Version: kcm_solid_actions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-02-08 04:00+0100
PO-Revision-Date: 2018-01-24 07:23+0700
Last-Translator: Wantoyo <wantoyek@gmail.com>
Language-Team: Indonesian <kde-i18n-doc@kde.org>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 (c) 2009, 2014 tim Aksi Peranti Solid (c) 2009, Ben Cooksley Ikon aksi, klik untuk mengubahnya Nama aksi Nama aksi: Tambah... Semua isi properti harus cocok Salah satu isi properti harus cocok Ben Cooksley Tak dapat dihapus Perintah yang akan memicu aksi
Ini dapat termasuk satu atau semua perluasan tidak sensitif huruf berikut:

%f: Titik kait untuk peranti - hanya peranti Akses Penyimpanan
%d: Alur untuk node peranti - hanya peranti Blok
%i: Pengidentifikasi peranti Perintah:  Berisi Konjungsi Isi Disjungsi Isi Kecocokan Antarmuka Peranti Tipe peranti: Peranti harus cocok dengan parameter berikut untuk aksi ini: andhika.padmawan@gmail.com Sunting Parameter Sunting... Aksi Penyuntingan '%1' Masukkan nama untuk aksi baru anda Sama Galat Mengurai Kondisi Peranti Aksi tidak sah Kelihatannya nama, perintah, ikon, atau kondisi aksi tidak sah.
Sehingga, perubahan tidak akan diterapkan. Kelihatannya predikat untuk aksi ini tidak sah. Pengelola Andhika Padmawan Tak Ada Aksi Terpilih Tipe parameter: Pangkalan ke Plasma 5 Kecocokan Properti Hapus Atur Ulang Paramater Simpan Perubahan Parameter Pembuat Berkas Desktop Aksi Solid Modul Panel Kendali Aksi Peranti Solid Penyunting Aksi Peranti Solid Peranti harus merupakan tipe %1 Properti peranti %1 harus berisi %2 Properti peranti %1 harus sesuai %2 Alat untuk secara otomatis membuat Berkas Desktop dari kelas DeviceInterface Solid untuk penerjemahan Nama nilai: 