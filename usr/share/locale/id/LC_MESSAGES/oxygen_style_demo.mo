��    �        �   �	      8     9     L     [     i  	        �     �     �     �     �     �     �  
   �     �     �     �  
   �          
       7   $     \     d     q          �  
   �     �  
   �  	   �     �     �  
   �     �  ,     /   /     _     e  
   l     w     �  
   �     �     �  
   �     �     �     �     �     �     �  
   �     
               *     9     A     Y     ]     e     s     z     �     �     �     �     �     �     �     �     �     �     �     �     �     �               %     -     2     A     O     b     n     {     �     �     �     �     �  B   �     *     >  #   U     y  /   �  =   �  #     *   +  .   V     �     �     �     �     �     �     �  
   �     �     �            	   &     0     A  
   P     [     h  
   z  
   �  	   �     �     �  
   �  &   �  0   �  )   %  +   O  "   {  ,   �  %   �  '   �          /     G     _     x     }     �     �     �     �     �     �     �     �  	   �     �     �     �     
       
        #  �  ,     �     �     �     �  	              &  
   4     ?     K     R     X  	   _  	   i     s     z     �     �     �  
   �  8   �  
   �               !     /     A     N     \     l     z     �     �     �     �     �     �     �     �  
   �     �           	       
   ,     7     F     Z     `  
   n     y  
        �     �     �     �     �     �     �  
   �     �                              "     '     3     ;     M     V     c  	   p     z     �     �     �     �  
   �     �     �     �     �  
                  '     5  
   M     X     q  H   �     �     �  %         +  2   L  B     $   �  -   �  3        I  	   h     r     �     �     �     �  
   �  
   �     �     �     �  
   �                 $      0      =      N      Z      i      v      �      �   '   �   3   �   0   !  ,   7!  $   d!  0   �!  -   �!  )   �!     "     )"      I"     j"     �"     �"     �"     �"  
   �"     �"     �"     �"     �"  	   �"  
   �"  
   �"     �"     #     !#     '#     -#     9#     �   d          N   ,   �   /          3   �   S   A      ?       B   :   T       f   ]               �   �   Q   �   I              2   �       �   a   .   x   p                   9          &   }   m       M   �       t   �   8   �   [   W   6   R   X       U      o       '      K          v   z   i   |              c                 e   +          ~   �   Z       O           5   w   G       b   >   s            q   =   �       (   �   "   �          �   @   F   0   r   %      `              y   �   _           C       �   $      L             �       E   �   *   	       )   g       4      
       n          l             �   \   �   �   #                   P   �   ;   �   �       ^   <      �       �   H   !   D   k           7   �   u   {       -              V          J   Y      1   h   j   �    &Layout direction: &Tab position: &Third Choice Animate Progress Bars Benchmark Bottom Bottom to Top Bottom-left Bottom-right Buttons Cascade Center Checkboxes Description Dialog Document mode Down Arrow East Editable combobox Editors Emulates user interaction with widgets for benchmarking Enabled Example text F&irst Choice First Column First Description First Item First Label First Page First Row First Subitem First Subitem Description First item Flat Flat frame. No frame is actually drawn.Flat Flat group box. No frame is actually drawnFlat Frame Frames Grab mouse GroupBox Hide tabbar Horizontal Huge (48x48) Icon si&ze: Icons Only Input Widgets Invert Progress Bars Large Large (32x32) Layout Left  Left Arrow Left to Right Lists MDI Windows Medium (22x22) Modules Multi-line text editor: New New Row No Tick Marks Normal North Off On Open Options Oxygen Demo Partial Password editor: Preview Pushbuttons Radiobuttons Raised Right Right Arrow Right to Left Right to left layout Run Simulation S&unken Save Seco&nd Choice Second Column Second Description Second Item Second Label Second Page Second Subitem Second Subitem Description Second item Select Next Window Select Previous Window Select below the modules for which you want to run the simulation: Show Corner Buttons Show Tab Close Buttons Shows the appearance of MDI windows Shows the appearance of buttons Shows the appearance of lists, trees and tables Shows the appearance of sliders, progress bars and scrollbars Shows the appearance of tab widgets Shows the appearance of text input widgets Shows the appearance of various framed widgets Single line text editor: Sliders Sliders tick marks position Small Small (16x16) South Spinbox: Tab Widget Tab Widgets Tabs Te&xt position: Text Alongside Icons Text Only Text Under Icons Text and icon: Text only: Third Column Third Description Third Item Third Page Third Row Third Subitem Third Subitem Description Third item This is a normal, text and icon button This is a normal, text and icon button with menu This is a normal, text and icon combo box This is a normal, text and icon tool button This is a normal, text only button This is a normal, text only button with menu This is a normal, text only combo box This is a normal, text only tool button This is a sample text Tick Marks Above Slider Tick Marks Below Slider Tick Marks On Both Sides Tile Title Title: Toggle authentication Toolbox Toolbuttons Tools Top Top to Bottom Top-left Top-right Up Arrow Use flat buttons Use flat widgets Vertical West Wrap words password Project-Id-Version: kstyle_config
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-16 03:34+0200
PO-Revision-Date: 2017-12-24 09:35+0700
Last-Translator: Wantoyo <wantoyek@gmail.com>
Language-Team: Indonesian <kde-i18n-doc@kde.org>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 &Arah tata letak: &Posisi tab: &Pilihan Ketiga Animasikan Bilah Progres Benchmark Bawah Bawah ke Atas Kiri-bawah Kanan-bawah Tombol Jatuh Tengah Kotak Cek Deskripsi Dialog Mode dokumen Panah Bawah Timur Kotak kombo dapat disunting Penyunting Emulasi interaksi pengguna dengan widget untuk benchmark Diaktifkan Teks contoh P&ilihan Pertama Kolom Pertama Deskripsi Pertama Item Pertama Label Pertama Halaman Pertama Baris Pertama Subitem Pertama Deskripsi Subitem Pertama Item pertama Datar Datar Datar Bingkai Bingkai Ambil mouse Kotak Grup Sembunyikan bilah tab Mendatar Sangat Besar (48x48) Ukuran Ik&on: Hanya Ikon Widget Masukan Balik Bilah Progres Besar Besar (32x32) Tata Letak Kiri  Panah Kiri Kiri ke Kanan Senarai MDI Windows Sedang (22x22) Modul Penyunting teks banyak baris: Baru Baris Baru Tak Ada Tanda Centang Normal Utara Mati Hidup Buka Opsi Demo Oxygen Parsial Penyunting sandi: Pratilik Tombol Tekan Tombol Radio Dinaikkan Kanan Panah Kanan Kanan ke Kiri Tata letak kanan ke kiri Jalankan Simulasi T&enggelam Simpan Piliha&n Kedua Kolom Kedua Deskripsi Suara Item Kedua Label Kedua Halaman Kedua Subitem Kedua Deskripsi Subitem Kedua Item kedua Pilih Jendela Berikutnya Pilih Jendela Sebelumnya Pilih modul di bawah yang ingin anda gunakan untuk menjalankan simulasi: Tampilkan Pojok Tombol Tampilkan Tombol Tutup Tab Menampilkan tampilan dari MDI windows Menampilkan tampilan dari tombol Menampilkan tampilan dari senarai, pohon dan tabel Menampilkan tampilan dari penggeser, bilah proses dan bilah gulung Menampilkan tampilan dari widget tab Menampilkan tampilan dari teks widget masukan Menampilkan tampilan dari beragam widget berbingkai Penyunting teks baris tunggal: Penggeser Penggeser posisi tanda centang Kecil Kecil (16x16) Selatan Kotak putar: Widget Tab Widget Tab Tab Posisi te&ks: Teks Di Samping Ikon Hanya Teks Teks Di Bawah Ikon Teks dan ikon: Hanya teks: Kolom Ketiga Deskripsi Ketiga Item Ketiga Halaman Ketiga Baris Ketiga Subitem Ketiga Deskripsi Subitem Ketiga Item ketiga Ini adalah tombol normal, teks dan ikon Ini adalah tombol dengan menu normal, teks dan ikon Ini adalah kotak kombinasi normal, teks dan ikon Ini adalah tombol alat normal, teks dan ikon Ini adalah tombol normal, hanya teks Ini adalah tombol dengan menu normal, hanya teks Ini adalah kotak kombinasi normal, hanya teks Ini adalah tombol alat normal, hanya teks Ini adalah teks contoh Tanda Centang Di Atas Penggeser Tanda Centang Di Bawah Penggeser Tanda Centang Di Kedua Sisi Ubin Judul Judul: Ubah otentikasi Bilah alat Tombol alat Alat Atas Atas ke Bawah Kiri-atas Kanan-atas Panah Atas Gunakan tombol datar Gunakan widget datar Tegak Barat Gulung kata sandi 