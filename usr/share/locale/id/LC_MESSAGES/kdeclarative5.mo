��          �            h  ~   i  +   �           5     T     s     �     �  �   �  g   A  0   �  .   �     	  @     �  Z  �   �  (   ~  -   �     �     �     
          !  �   2  l   �  3   A      u     �     �                                                   	             
                Click on the button, then enter the shortcut like you would in the program.
Example for Ctrl+A: hold the Ctrl key and press A. Conflict with Standard Application Shortcut EMAIL OF TRANSLATORSYour emails KPackage QML application shell NAME OF TRANSLATORSYour names No shortcut definedNone Reassign Reserved Shortcut The '%1' key combination is also used for the standard action "%2" that some applications use.
Do you really want to use it as a global shortcut as well? The F12 key is reserved on Windows, so cannot be used for a global shortcut.
Please choose another one. The key you just pressed is not supported by Qt. The unique name of the application (mandatory) Unsupported Key What the user inputs now will be taken as the new shortcutInput Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-30 03:07+0100
PO-Revision-Date: 2018-01-26 18:53+0700
Last-Translator: Wantoyo <wantoyek@gmail.com>
Language-Team: Indonesian <kde-i18n-doc@kde.org>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 Klik pada tombol, lalu masukkan pintasan seperti yang anda inginkan di program.
Misalnya untuk Ctrl+A: tahan tombol Ctrl lalu tekan A. Konflik dengan Pintasan Aplikasi Standar andhika.padmawan@gmail.com,wantoyek@gmail.com Shell aplikasi KPackage QML Andhika Padmawan,Wantoyo Tak Ada Tugaskan Ulang Pintasan Dipesan Kombinasi tombol '%1' digunakan pula untuk aksi standar "%2" yang digunakan oleh beberapa aplikasi.
Anda yakin ingin menggunakannya sebagai pintasan global juga? Tombol F12 dipesan di Windows, sehingga tidak dapat digunakan untuk pintasan global.
Silakan pilih yang lain Tombol yang baru anda tekan tidak didukung oleh Qt. Nama unik dari aplikasi (mandat) Tombol Tak Didukung Masukan 