��    *      l  ;   �      �  A   �     �  /        2     ;     O     a     w     �     �  	   �     �  3   �  	   �     �      �  $      *   E     p  	   u  5        �     �  
   �     �          $  5   3  6   i     �  ,   �  /   �     	        O   0  Z   �  b   �  	   >  
   H  P   S     �  �  �  A   l
     �
     �
  
   �
     �
     �
          (     9     A     N     Z  5   r  
   �     �     �  %   �     �             1     +   H     t  	   �     �     �     �  ?   �  B        W  7   l     �     �     �  V   �  g   -  _   �     �       _        r            )             &      $   	   %         '                                                !      
       *                     "                                                 (                 #    %1 is an external application and has been automatically launched (c) 2009, Ben Cooksley <i>Contains 1 item</i> <i>Contains %1 items</i> About %1 About Active Module About Active View About System Settings Apply Settings Author Ben Cooksley Configure Configure your system Determines whether detailed tooltips should be used Developer Dialog EMAIL OF TRANSLATORSYour emails Expand the first level automatically General config for System SettingsGeneral Help Icon View Internal module representation, internal module model Internal name for the view used Keyboard Shortcut: %1 Maintainer Mathias Soeken NAME OF TRANSLATORSYour names No views found Provides a categorized icons view of control modules. Provides a classic tree-based view of control modules. Relaunch %1 Reset all current changes to previous values Search through a list of control modulesSearch Show detailed tooltips System Settings System Settings was unable to find any views, and hence has nothing to display. System Settings was unable to find any views, and hence nothing is available to configure. The settings of the current module have changed.
Do you want to apply the changes or discard them? Tree View View Style Welcome to "System Settings", a central place to configure your computer system. Will Stephenson Project-Id-Version: systemsettings
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:08+0100
PO-Revision-Date: 2017-12-19 07:30+0700
Last-Translator: Wantoyo <wantoyek@gmail.com>
Language-Team: Indonesian <translation-team-id@lists.sourceforge.net>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 %1 adalah aplikasi eksternal dan telah dijalankan secara otomatis (c) 2009, Ben Cooksley <i>Berisi %1 item</i> Tentang %1 Tentang Modul Aktif Tentang Tampilan Aktif Tentang Setelan Sistem Terapkan Setelan Penulis Ben Cooksley Konfigurasi Konfigurasi sistem anda Menentukan apakah tip alat terperinci harus digunakan Pengembang Dialog andhika.padmawan@gmail.com Perluas level pertama secara otomatis Umum Bantuan Tampilan Ikon Representasi modul internal, model modul internal Nama internal untuk tampilan yang digunakan Pintasan Keyboard: %1 Pengelola Mathias Soeken Andhika Padmawan Tak ada tampilan yang ditemukan Memberikan tampilan ikon yang dikategorikan dari modul kendali. Memberikan tampilan berbasis pohon yang klasik dari modul kendali. Menjalankan Ulang %1 Atur ulang semua perubahan saat ini ke nilai sebelumnya Cari Tampilkan tip alat terperinci Setelan Sistem Setelan Sistem tak dapat menemukan tampilan apapun, sehingga tak ada yang ditampilkan. Setelan Sistem tak dapat menemukan tampilan apapun, sehingga tak ada yang tersedia untuk dikonfigurasi. Setelan dari modul saat ini telah berubah.
Anda ingin menerapkan perubahan atau mengabaikannya? Tampilan Pohon Gaya Tampilan Selamat datang ke "Setelan Sistem", tempat terpusat untuk mengkonfigurasi sistem komputer anda. Will Stephenson 