��          �      <      �  &   �  +   �       	             2     8     A     F  (   V          �  ,   �     �     �     �     �  �  �  #   �  *   �     �  	   �     �  
             *     1  -   >     l     r  +   �     �     �     �     �        	      
                                                                                       Do you want to suspend to RAM (sleep)? Do you want to suspend to disk (hibernate)? General Hibernate Hibernate (suspend to disk) Leave Leave... Lock Lock the screen Logout, turn off or restart the computer No Sleep (suspend to RAM) Start a parallel session as a different user Suspend Switch User Switch user Yes Project-Id-Version: plasma_applet_lockout
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2018-02-03 06:03+0700
Last-Translator: Wantoyo <wantoyek@gmail.com>
Language-Team: Indonesian <kde-i18n-doc@kde.org>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 Anda ingin suspensi ke RAM (tidur)? Anda ingin suspensi ke cakram (hibernasi)? Umum Hibernasi Hibernasi (suspensi ke cakram) Tinggalkan Tinggalkan... Gembok Gembok layar Log keluar, matikan atau start ulang komputer Tidak Tidur (suspensi ke RAM) Jalankan sesi paralel sebagai pengguna lain Suspensi Ganti Pengguna Ganti pengguna Ya 