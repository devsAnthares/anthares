��          �      <      �  V  �  X       a  -   y     �     �     �     �     �     �          2     :     L     `  !   q  !   �  �  �  \  R  c  �     
  
   -
     8
     F
     a
     p
  #   �
     �
     �
     �
     �
     �
          .     6               
                	                                                                      <qt>Specifies how Jovie should speak the event when received.  If you select "Speak custom text", enter the text in the box.  You may use the following substitution strings in the text:<dl><dt>%e</dt><dd>Name of the event</dd><dt>%a</dt><dd>Application that sent the event</dd><dt>%m</dt><dd>The message sent by the application</dd></dl></qt> <qt>Specifies how knotify should speak the event when received.  If you select "Speak custom text", enter the text in the box.  You may use the following substitution strings in the text:<dl><dt>%e</dt><dd>Name of the event</dd><dt>%a</dt><dd>Application that sent the event</dd><dt>%m</dt><dd>The message sent by the application</dd></dl></qt> Configure Notifications Description of the notified eventDescription Log to a file Mark &taskbar entry Play a &sound Run &command Select the command to run Select the sound to play Show a message in a &popup Sp&eech Speak Custom Text Speak Event Message Speak Event Name State of the notified eventState Title of the notified eventTitle Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2018-01-28 16:27+0700
Last-Translator: Wantoyo <wantoyek@gmail.com>
Language-Team: Indonesian <kde-i18n-doc@kde.org>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 <qt>Tentukan cara Jovie harus bicara ketika peristiwa diterima. Jika anda memilih "Bicarakan teks suai", masukkan teks di dalam kotak. Anda dapat menggunakan tali substitusi berikut di dalam teks:<dl><dt>%e</dt><dd>Nama peristiwa</dd><dt>%a</dt><dd>Aplikasi yang mengirim peristiwa</dd><dt>%m</dt><dd>Pesan yang dikirim oleh aplikasi</dd></dl></qt> <qt>Tentukan bagaimana knotify harus bicara ketika peristiwa diterima. Jika kamu memilih "Bicarakan teks suai", masukkan teks di dalam kotak. Kamu dapat menggunakan tali substitusi berikut di dalam teks:<dl><dt>%e</dt><dd>Nama peristiwa</dd><dt>%a</dt><dd>Aplikasi yang mengirim peristiwa</dd><dt>%m</dt><dd>Pesan yang dikirim oleh aplikasi</dd></dl></qt> Konfigurasi Pemberitahuan Keterangan Log ke berkas Tandai entri batang &tugas Mainkan &suara Jalankan &perintah Pilih perintah yang akan dijalankan Pilih suara yang akan dimainkan Tampilkan pesan dalam &sembul Pi&dato Katakan Teks Suai Katakan Pesan Peristiwa Katakan Nama Peristiwa Keadaan Judul 