��          �      l      �     �     �     �  0        2     F     \     b     n     v  
   �     �     �     �     �     �     �     �            �  %  
   �  
   �  
   �  5   �     3     C     ^     b     p     v     �     �     �  &   �     �  
   �                2  	   8                            
                                                            	          %1 by %2 Centered Change every: Directory with the wallpaper to show slides from Download Wallpapers Get New Wallpapers... Hours Image Files Minutes Next Wallpaper Image Open Image Open Wallpaper Image Positioning: Recommended wallpaper file Remove wallpaper Scaled Scaled and Cropped Scaled, Keep Proportions Seconds Tiled Project-Id-Version: plasma_wallpaper_image
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:39+0100
PO-Revision-Date: 2014-11-22 11:28+0700
Last-Translator: Andhika Padmawan <andhika.padmawan@gmail.com>
Language-Team: Indonesian <translation-team-id@lists.sourceforge.net>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 %1 oleh %2 Dipusatkan Ubah tiap: Direktori dengan wallpaper untuk menampilkan salindia Unduh Wallpaper Dapatkan Wallpaper Baru... Jam Berkas Gambar Menit Gambar Wallpaper Berikutnya Buka Gambar Buka Gambar Wallpaper Letak: Berkas wallpaper yang direkomendasikan Dapatkan wallpaper Diskalakan Diskalakan dan Dipotong Diskalakan, Jaga Proporsi Detik Diubinkan 