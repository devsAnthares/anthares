��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  �  K     �  !   �  �    z  �  Z   A
     �
     �
     �
      �
     �
     �
  !        4                                          
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2018-02-08 19:42+0700
Last-Translator: Wantoyo <wantoyek@gmail.com>
Language-Team: Indonesian <kde-i18n-doc@kde.org>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
  det &Waktu habis indikasi permulaian: <H1>Notifikasi Bilah Tugas</H1>
Anda dapat mengaktifkan metode kedua notifikasi permulaian yang
digunakan oleh bilah alat tempat tombol dengan jam gelas berotasi
muncul, menyimbolkan bahwa aplikasi yang telah anda dijalankan sedang
dimuat.
Namun dapat terjadi, beberapa aplikasi tidak tahu akan adanya notifikasi
hidupkan ini. Dalam hal ini, tombol akan menghilang setelah waktu
yang diberikan di bagian 'Waktu habis indikasi permulaian' <h1>Kursor Sibuk</h1>
KDE menawarkan kursor sibuk untuk notifikasi permulaian aplikasi.
Untuk mengaktifkan kursor sibuk, pilih satu dari umpan balik visual
dari kotak kombo.
Namun dapat terjadi, beberapa aplikasi tidak tahu akan adanya notifikasi
hidupkan ini. Dalam hal ini, kursor akan berhenti berkedip setelah
waktu yang diberikan di bagian 'Waktu habis indikasi permulaian' <h1>Tanggapan Luncur</h1> Kamu bisa mengkonfigurasi tanggapan peluncuran aplikasi di sini. Kursor Berkedip Kursor Berpantul Kursor S&ibuk Aktifkan notifikasi bilah &tugas Tiada Kursor Sibuk Kursor Sibuk Pasif Waktu habis indikasi perm&ulaian: &Notifikasi Bilah Tugas 