��    .      �  =   �      �     �  ;   �  9   2  (   l  ;   �  9   �  8     $   D     i          �  /   �      �     �  ,        0     @  N   P  
   �      �  	   �  �   �     _     k  e   �     �             
   (     3     G     `     g  &   x  %   �  f   �  8   ,	  ;   e	     �	     �	     �	     �	  �   �	  "   �
  C   �
  �  �
     �     �     �  	   �     �     �          *     0     G     Y  4   a     �     �     �     �     �  N         O     f  	   �  �   �            r   /     �     �     �     �     �          )     /     ?     _  c   {  7   �  3        K     Y     v     �  �   �  #   /  A   S                             #          $      
   "   %   	                              !   .             &            -          +          *                                 ,   '                (   )                min @action:inmenu Global shortcutDecrease Keyboard Brightness @action:inmenu Global shortcutDecrease Screen Brightness @action:inmenu Global shortcutHibernate @action:inmenu Global shortcutIncrease Keyboard Brightness @action:inmenu Global shortcutIncrease Screen Brightness @action:inmenu Global shortcutToggle Keyboard Backlight @label:slider Brightness levelLevel AC Adapter Plugged In Activity Manager After All pending suspend actions have been canceled. Battery Critical (%1% Remaining) Battery Low (%1% Remaining) Brightness level, label for the sliderLevel Can't open file Charge Complete Could not connect to battery interface.
Please check your system configuration Do nothing EMAIL OF TRANSLATORSYour emails Hibernate KDE Power Management System could not be initialized. The backend reported the following error: %1
Please check your system configuration Lock screen NAME OF TRANSLATORSYour names No valid Power Management backend plugins are available. A new installation might solve this problem. On Profile Load On Profile Unload Prompt log out dialog Run script Running on AC power Running on Battery Power Script Switch off after The power adaptor has been plugged in. The power adaptor has been unplugged. The profile "%1" has been selected, but it does not exist.
Please check your PowerDevil configuration. This activity's policies prevent screen power management This activity's policies prevent the system from suspending Turn off screen Unsupported suspend method When laptop lid closed When power button pressed Your battery is low. If you need to continue using your computer, either plug in your computer, or shut it down and then change the battery. Your battery is now fully charged. Your battery level is critical, save your work as soon as possible. Project-Id-Version: powerdevil
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-03 03:17+0100
PO-Revision-Date: 2018-02-03 06:03+0700
Last-Translator: Wantoyo <wantoyek@gmail.com>
Language-Team: Indonesian <kde-i18n-doc@kde.org>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
  men Turunkan Kecerahan Keyboard Turunkan Kecerahan Layar Hibernasi Tingkatkan Kecerahan Keyboard Tingkatkan Kecerahan Layar Ubah Warna Belakang Keyboard Level Adaptor AC Ditancapkan Manajer Aktivitas Setelah Aksi suspensi yang menunggu apapun telah dibatalkan. Baterai Kritis (%1% Tersisa) Baterai Habis (%1% Tersisa) Level Tak dapat membuka berkas Pengisian Selesai Tak dapat menyambung ke antarmuka baterai.
Silakan cek konfigurasi sistem anda Jangan lakukan apa-apa andhika.padmawan@gmail.com Hibernasi Sistem Pengelolaan Daya KDE tak dapat diinisialisasi. Ujung belakang melaporkan galat berikut: %1
Silakan cek konfigurasi sistem anda Gembok layar Andhika Padmawan Tak ada plugin ujung belakang Pengelolaan Daya yang tersedia. Instalasi baru mungkin dapat memecahkan masalah ini. Saat Memuat Profil Saat Bongkar Muat Profil Tanyakan dialog Keluar Jalankan Skrip Berjalan dengan daya AC Berjalan Dengan Daya Baterai Skrip Matikan setelah Adaptor daya telah ditancapkan. Adaptor daya telah dicabut. Profil "%1" telah dipilih, tapi profil tersebut tidak ada.
Silakan cek konfigurasi PowerDevil anda. Kebijakan aktivitas ini mencegah pengelolaan daya layar Kebijakan aktivitas ini mencegah sistem tersuspensi Matikan layar Metode suspensi tak didukung Jika tutup laptop ditutup Jika tombol daya ditekan Baterai anda habis. Jika anda ingin melanjutkan penggunaan komputer anda, colokkan ke komputer anda, atau matikan lalu ganti baterai. Baterai anda saat ini penuh terisi. Level baterai anda kritis, simpan pekerjaan anda secepat mungkin. 