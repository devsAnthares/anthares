��          D      l       �   6   �   3   �   8   �      -  �  ;  7   �  7   !  <   Y     �                          A command-line tool for querying the KDE trader system A constraint expressed in the trader query language A servicetype, like KParts/ReadOnlyPart or KMyApp/Plugin KTraderClient Project-Id-Version: ktraderclient
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-01 02:51+0200
PO-Revision-Date: 2010-06-10 00:50-0400
Last-Translator: Andhika Padmawan <andhika.padmawan@gmail.com>
Language-Team: Indonesian <translation-team-id@lists.sourceforge.net>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 Alat baris perintah untuk menanyakan sistem penukar KDE Batasan yang diekspresikan di bahasa pertanyaan penukar Tipe layanan, seperti KParts/ReadOnlyPart atau KMyApp/Plugin KTraderClient 