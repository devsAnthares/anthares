��          �      \      �     �     �     �     �              	   <     F     e  &        �     �     �  �   �  �   y  t     7   �  +   �     �  �  �     �     �     �     �     �     �  	   �            *   (     S     l     �  �   �  �   <  q   �     ^	  4   m	     �	                                        
   	                                                         min Act like Always Define a special behavior Don't use special settings EMAIL OF TRANSLATORSYour emails Hibernate NAME OF TRANSLATORSYour names Never shutdown the screen Never suspend or shutdown the computer PC running on AC power PC running on battery power PC running on low battery The Power Management Service appears not to be running.
This can be solved by starting or scheduling it inside "Startup and Shutdown" The activity service is not running.
It is necessary to have the activity manager running to configure activity-specific power management behavior. The activity service is running with bare functionalities.
Names and icons of the activities might not be available. This is meant to be: Act like activity %1Activity "%1" Use separate settings (advanced users only) after Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-09 03:03+0200
PO-Revision-Date: 2017-12-27 22:08+0700
Last-Translator: Wantoyo <wantoyek@gmail.com>
Language-Team: Indonesian <translation-team-id@lists.sourceforge.net>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
  men Tindakan Selalu Batasi sebuah perilaku khusus Jangan gunakan setelan khusus wantoyo@yahoo.com Hibernasi Wantoyo Tidak pernah mematikan layar Tidak pernah suspend atau matikan komputer PC berjalan pada daya AC PC berjalan pada daya baterai PC berjalan pada baterai lemah Layanan Pengelolaan Daya tampaknya tidak akan berjalan.
Hal ini dapat diselesaikan dengan memulai atau penjadwalan di dalam "Hidupkan dan Matikan" Layanan aktivitas tidak berjalan.
 Hal ini diperlukan untuk memiliki pengelola aktivitas yang berjalan untuk mengkonfigurasi perilaku aktivitas pengelolaan daya yang spesifik. Layanan aktivitas berjalan dengan memperlihatkan fungsionalitas.
 Nama dan ikon aktivitas mungkin tidak tersedia. Aktivitas "%1" Gunakan setelan terpisah (hanya penggunaan lanjutan) setelah 