��    `        �         (     )     1     B     Q     W     ^     j     {     �     �  /   �  -   �     �     �  
   		  
   	     	     ,	     1	     8	     @	     R	     _	     d	  
   l	     w	     �	     �	     �	  	   �	     �	  	   �	     �	     �	     �	     
     
  	   #
     -
     4
     H
  	   M
     W
     ]
     c
     h
     m
  	   v
     �
     �
     �
     �
     �
     �
     �
     �
  <   �
               /     6     =     X     ^     e     j  
   ~     �     �     �     �     �  )   �               5     :     @     M     U     ]     f  
   x     �     �  	   �     �     �     �     �     �     �     �  E   �  *   A  �  l                9     R     X  	   `     j     |     �  
   �     �     �     �     �     �     �     �     �                 
   !     ,     4     9  	   ?     I     X     j     �     �  	   �  
   �     �     �     �     �  
   �     �                -     9     >     D     I     P     W     _  	   p     z     �     �     �     �     �  D   �                %     +     7     @     F     M     S     e     q     ~     �     �     �  7   �          )     F     M  
   S     ^     o  
   �     �  
   �     �     �     �     �     �  %   �     �               )  N   B     �     J       -      M       %   Q           ^              U       L   S   $      .       H       X          Y   8   *         <   &          '       ]   N       3                     `          #   V             /   T   G   9   @   >   =   6   2   O   C   F   ?      0      
      A   E       	      P              _                D          Z               1   R   ,   +                \           I   )   B   K      :   !   W   4          ;               (      5   "       7       [           &Delete &Empty Trash Bin &Move to Trash &Open &Paste &Properties &Refresh Desktop &Refresh View &Reload &Rename @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Align Appearance: Arrange In Arrange in Arrangement: Back Cancel Columns Configure Desktop Custom title Date Default Descending Description Deselect All Desktop Layout Enter custom title here Features: File name pattern: File type File types: Filter Folder preview popups Folders First Folders first Full path Got it Hide Files Matching Huge Icon Size Icons Large Left List Location Location: Lock in place Locked Medium More Preview Options... Name None OK Panel button: Press and hold widgets to move them and reveal their handles Preview Plugins Preview thumbnails Remove Resize Restore from trashRestore Right Rotate Rows Search file type... Select All Select Folder Selection markers Show All Files Show Files Matching Show a place: Show files linked to the current activity Show the Desktop folder Show the desktop toolbox Size Small Small Medium Sort By Sort by Sorting: Specify a folder: Text lines Tiny Title: Tool tips Tweaks Type Type a path or a URL here Unsorted Use a custom icon Widget Handling Widgets unlocked You can press and hold widgets to move them and reveal their handles. whether to use icon or list viewView mode Project-Id-Version: plasma_applet_folderview
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:11+0100
PO-Revision-Date: 2018-02-10 08:15+0700
Last-Translator: Wantoyo <wantoyek@gmail.com>
Language-Team: Indonesian <kde-i18n-doc@kde.org>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 &Hapus &Kosongkan Tempat Sampah &Pindah ke Tempat Sampah &Buka &Tempel &Properti &Segarkan Desktop &Segarkan Tampilan &Muat Ulang &Ubah Nama Pilih... Bersihkan Ikon Sejajar Penampilan: Penempatan Di Penempatan di Penempatan: Mundur Batal Kolom Konfigurasi Desktop Judul suai Tanggal Baku Turun Deskripsi Nonpilih Semua Tataletak Desktop Masukkan judul suai di sini Fitur: Pola nama file: Tipe file Tipe file: Filter Sembulan pratinjau folder Folder Dahulu Folder pertama Alur penuh Dapatkan Sembunyikan File Yang Cocok Sangat Besar Ukuran Ikon Ikon Besar Kiri Daftar Lokasi Lokasi: Gembok di tempat Tergembok Medium Opsi Pratinjau Selebihnya... Nama Nihil Oke Tombol panel: Tekan dan tahan widget untuk memindahnya dan menampakkan pegangannya Plugin Pratinjau Thumbnail pratinjau Hapus Ubah Ukuran Pulihkan Kanan Rotasi Baris Cari tipe file... Pilih Semua Pilih Folder Pemilihan penanda Tampilkan Semua File Tampilkan File Yang Cocok Tampilkan sebuah tempat: Tampilkan file yang ditautkan dengan aktivitas saat ini Tampilkan folder Desktop Tampilkan kotak alat desktop Ukuran Kecil Agak Kecil Urut Berdasarkan Pilah berdasarkan Pemilahan: Tentukan sebuah folder: Baris teks Mungil Judul: Tip alat Uprekan Tipe Ketikkan sebuah alur atau URL di sini Tak Dipilah Gunakan ikon kustom Pegangan Widget Widgets dibuka gemboknya Kamu bisa tekan dan tahan widget untuk memindahnya dan menampakkan pegangannya Mode tampilan 