��    N      �  k   �      �  "   �     �     �  8   �  9   -  ,   g      �  2   �  4   �  4     8   R  ?   �  0   �  #   �  /    	  0   P	  #   �	  2   �	  /   �	     
     
     #
     4
     :
     H
  
   `
     k
     t
     �
     �
     �
     �
     �
  }   �
     I     N     U      a     �     �     �     �     �     �     �  	   �     �     �     �          '     A     G     U     t     �     �     �     �     �     �     �     �  	   �  	   �  G   �           ,     ;     C     X     `     p     �     �     �     �  �  �  "        �     �  :   �  ;     +   >      j  6   �  8   �  9   �  4   5  =   j  /   �  #   �  -   �  5   *  #   `  0   �  /   �     �     �     �                 
   4     ?     H     U  
   ]     h     x     �  �   �     *     /     7     F     a     s     z          �     �     �  	   �     �     �     �     �          +     1     @     Q     b     k     s     |     �     �     �     �     �     �  Q   �  
                  %     ;     C     S     i     �     �     �         /   6          A   -   >      B                  5             L              )      %       K         4             3   +             G   ;      $   1   I      &          '   D   0           	   #   !       C      *   <          ,           2   H          @      
       .   N                         F          E             =                 J   8      9   7   :   ?   M           "   (    (c) 2001 Matthias Hoelzer-Kluepfel <b>Manufacturer:</b>  <b>Serial #:</b>  <tr><td><i>Attached Devicenodes</i></td><td>%1</td></tr> <tr><td><i>Bandwidth</i></td><td>%1 of %2 (%3%)</td></tr> <tr><td><i>Channels</i></td><td>%1</td></tr> <tr><td><i>Class</i></td>%1</tr> <tr><td><i>Intr. requests</i></td><td>%1</td></tr> <tr><td><i>Isochr. requests</i></td><td>%1</td></tr> <tr><td><i>Max. Packet Size</i></td><td>%1</td></tr> <tr><td><i>Power Consumption</i></td><td>%1 mA</td></tr> <tr><td><i>Power Consumption</i></td><td>self powered</td></tr> <tr><td><i>Product ID</i></td><td>0x%1</td></tr> <tr><td><i>Protocol</i></td>%1</tr> <tr><td><i>Revision</i></td><td>%1.%2</td></tr> <tr><td><i>Speed</i></td><td>%1 Mbit/s</td></tr> <tr><td><i>Subclass</i></td>%1</tr> <tr><td><i>USB Version</i></td><td>%1.%2</td></tr> <tr><td><i>Vendor ID</i></td><td>0x%1</td></tr> AT-commands ATM Networking Abstract (modem) Audio Bidirectional Boot Interface Subclass Bulk (Zip) CAPI 2.0 CAPI Control CDC PUF Communications Control Device Control/Bulk Control/Bulk/Interrupt Could not open one or more USB controller. Make sure, you have read access to all USB controllers that should be listed here. Data Device Direct Line EMAIL OF TRANSLATORSYour emails Ethernet Networking Floppy HDLC Host Based Driver Hub Human Interface Devices I.430 ISDN BRI Interface Keyboard Leo Savernik Live Monitoring of USB Bus Mass Storage Matthias Hoelzer-Kluepfel Mouse Multi-Channel NAME OF TRANSLATORSYour names No Subclass Non Streaming None Printer Q.921 Q.921M Q.921TM Q.932 EuroISDN SCSI Streaming Telephone This module allows you to see the devices attached to your USB bus(es). Transparent Unidirectional Unknown V.120 V.24 rate ISDN V.42bis Vendor Specific Vendor Specific Class Vendor Specific Protocol Vendor Specific Subclass Vendor specific kcmusb Project-Id-Version: kcmusb
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2017-12-27 21:56+0700
Last-Translator: Wantoyo <wantoyek@gmail.com>
Language-Team: Indonesian <translation-team-id@lists.sourceforge.net>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 (c) 2001 Matthias Hoelzer-Kluepfel <b>Manufaktur:</b> <b>Serial #:</b>  <tr><td><i>Node Peranti Tertancap</i></td><td>%1</td></tr> <tr><td><i>Bandwidth</i></td><td>%1 dari %2 (%3%)</td></tr> <tr><td><i>Channel</i></td><td>%1</td></tr> <tr><td><i>Kelas</i></td>%1</tr> <tr><td><i>Permintaan intrusi</i></td><td>%1</td></tr> <tr><td><i>Permintaan isokronus</i></td><td>%1</td></tr> <tr><td><i>Ukuran Paket Maksimum</i></td><td>%1</td></tr> <tr><td><i>Konsumsi Daya</i></td><td>%1 mA</td></tr> <tr><td><i>Konsumsi Daya</i></td><td>tenaga sendiri</td></tr> <tr><td><i>ID Produk</i></td><td>0x%1</td></tr> <tr><td><i>Protokol</i></td>%1</tr> <tr><td><i>Revisi</i></td><td>%1.%2</td></tr> <tr><td><i>Kecepatan</i></td><td>%1 Mbita/d</td></tr> <tr><td><i>Subkelas</i></td>%1</tr> <tr><td><i>Versi USB</i></td><td>%1.%2</td></tr> <tr><td><i>ID Vendor</i></td><td>0x%1</td></tr> Perintah AT Jaringan ATM Abstrak (modem) Audio Dwiarah Subkelas Antarmuka Boot Inti (Zip) CAPI 2.0 Kendali CAPI CDC PUF Komunikasi Peranti Kendali Kendali/Inti Kendali/Bulk/Interupsi Tak dapat membuka satu atau lebih pengontrol USB. Pastikan, anda memiliki akses baca ke semua pengontrol USB yang harus disenaraikan di sini. Data Peranti Jalur Langsung andhika.padmawan@gmail.com Jaringan Ethernet Floppy HDLC Penggerak Berbasis Host Hub Peranti Antarmuka Manusia I.430 ISDN BRI Antarmuka Keyboard Leo Savernik Memonitor Langsung Bus USB Penyimpanan Massa Matthias Hoelzer-Kluepfel Mouse Banyak Channel Andhika Padmawan Tak Ada Subkelas Non Arus Tak Ada Pencetak Q.921 Q.921M Q.921TM Q.932 EuroISDN SCSI Arus Telepon Modul ini memungkinkan anda untuk melihat peranti yang tertancap ke bus USB anda. Transparan Uniarah Tak Diketahui V.120 V.24 rasio ISDN V.42bis Spesifik Vendor Kelas Spesifik Vendor Protokol Spesifik Vendor Subkelas Spesifik Vendor Spesifik vendor kcmusb 