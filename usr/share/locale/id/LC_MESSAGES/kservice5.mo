��            )   �      �     �  /   �     �     �  _   	  6   i  `   �  N     n   P  E   �  W     @   ]     �     �      �  !   �  %     )   =      g  c   �  -   �  D        _     ~  !   �  B   �  @   �     ?  �  ]     �	  "   
     %
     1
  I   ?
     �
  O   �
  6   �
  [   '  %   �  6   �      �       !   !  $   C  $   h  &   �  )   �  -   �  e     )   r  E   �     �  &   �      "  @   C  *   �     �                        
                                                                                  	                                @info:creditAuthor @info:creditCopyright 1999-2014 KDE Developers @info:creditDavid Faure @info:creditWaldo Bastian @info:shell command-line optionCheck file timestamps (deprecated, no longer having any effect) @info:shell command-line optionCreate global database @info:shell command-line optionDisable checking files (deprecated, no longer having any effect) @info:shell command-line optionDisable incremental update, re-read everything @info:shell command-line optionDo not signal applications to update (deprecated, no longer having any effect) @info:shell command-line optionPerform menu generation test run only @info:shell command-line optionSwitch QStandardPaths to test mode, for unit tests only @info:shell command-line optionTrack menu id for debug purposes Could not launch Browser Could not launch Mail Client Could not launch Terminal Client Could not launch the browser:

%1 Could not launch the mail client:

%1 Could not launch the terminal client:

%1 EMAIL OF TRANSLATORSYour emails Error launching %1. Either KLauncher is not running anymore, or it failed to start the application. Function must be called from the main thread. KLauncher could not be reached via D-Bus. Error when calling %1:
%2
 NAME OF TRANSLATORSYour names No service implementing %1 The provided service is not valid The service '%1' provides no library or the Library key is missing application descriptionRebuilds the system configuration cache. application nameKBuildSycoca Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-02-07 08:40+0100
PO-Revision-Date: 2018-02-01 21:15+0700
Last-Translator: Wantoyo <wantoyek@gmail.com>
Language-Team: Indonesian <kde-i18n-doc@kde.org>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 Penulis Hak Cipta 1999-2014 Pengembang KDE David Faure Waldo Bastian Periksa file timestamp (tercela, tidak terlalu memiliki pengaruh apa pun) Buat basis data global Nonaktifkan pemeriksaan file (tercela, tidak terlalu memiliki pengaruh apa pun) Nonaktifkan pembaruan inkremental, baca ulang semuanya Jangan nyalakan aplikasi untuk pembaruan (tercela, tidak terlalu memiliki pengaruh apa pun) Hanya lakukan uji coba pembuatan menu Ganti QStandardPaths ke mode tes, hanya untuk tes unit Lacak id menu untuk tujuan debug Tak dapat menjalankan Penelusur Tak dapat menjalankan Klien Surat Tak dapat menjalankan Klien Terminal Tak dapat menjalankan penelusur:

%1 Tak dapat menjalankan klien surat:

%1 Tak dapat menjalankan klien terminal:

%1 andhika.padmawan@gmail.com,wantoyek@gmail.com Galat menjalankan %1. Entah KLauncher tidak berjalan lagi, atau KLauncher gagal menjalankan aplikasi. Fungsi harus dipanggil dari benang utama. KLauncher tak dapat dicapai via D-Bus. Galat ketika memanggil %1:
%2
 Andhika Padmawan,Wantoyo Tak ada layanan mengimplementasikan %1 Layanan yang diberikan tidak sah Layanan '%1' tidak menyediakan pustaka atau kunci Pustaka hilang Bangun ulang singgahan konfigurasi sistem. KBuildSycoca 