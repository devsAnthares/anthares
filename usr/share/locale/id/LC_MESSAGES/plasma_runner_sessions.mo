��          �   %   �      P  �  Q          )  6   9  -   p     �     �     �     �  (   �  d        x     �     �     �     �     �     �                7  "   X     {     �     �  �  �  �  l     :
     O
  8   \
  $   �
  	   �
     �
     �
     �
  3     h   <     �     �     �     �     �     �     �  	   �  
   �                    *  	   0                               	                                                                      
                        <p>You have chosen to open another desktop session.<br />The current session will be hidden and a new login screen will be displayed.<br />An F-key is assigned to each session; F%1 is usually assigned to the first session, F%2 to the second session and so on. You can switch between sessions by pressing Ctrl, Alt and the appropriate F-key at the same time. Additionally, the KDE Panel and Desktop menus have actions for switching between sessions.</p> Lists all sessions Lock the screen Locks the current sessions and starts the screen saver Logs out, exiting the current desktop session New Session Reboots the computer Restart the computer Shutdown the computer Starts a new session as a different user Switches to the active session for the user :q:, or lists all active sessions if :q: is not provided Turns off the computer User sessionssessions Warning - New Session lock screen commandlock log out log out commandLogout log out commandlogout new session restart computer commandreboot restart computer commandrestart shutdown computer commandshutdown switch user switch user commandswitch switch user commandswitch :q: Project-Id-Version: plasma_runner_sessions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2018-02-03 06:03+0700
Last-Translator: Wantoyo <wantoyek@gmail.com>
Language-Team: Indonesian <kde-i18n-doc@kde.org>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 <p>Anda telah memilih untuk membuka sesi desktop lainnya.<br />Sesi saat ini akan disembunyikan dan layar masuk baru akan ditampilkan.<br />Sebuah tombol F ditugaskan untuk tiap sesi; F%1 biasanya ditugaskan untuk sesi pertama, F%2 untuk sesi kedua dan seterusnya. Anda dapat berganti di antara sesi dengan menekan Ctrl, Alt dan tombol F yang tepat secara bersamaan. Sebagai tambahan, Panel KDE dan menu Desktop mempunyai aksi untuk berganti di antara sesi.</p> Tampilkan semua sesi Gembok layar Menggembok sesi saat ini dan menjalankan penyimpan layar Keluar, keluar sesi desktop saat ini Sesi Baru Menjalankan ulang komputer Jalankan ulang komputer Matikan komputer Menjalankan sesi baru sebagai pengguna yang berbeda Mengganti ke sesi aktif untuk pengguna :q:, atau menampilkan semua sesi aktif jika :q: tidak menyediakan Mematikan komputer sesi Peringatan - Sesi Baru kunci keluar Keluar keluar sesi baru boot ulang jalankan ulang matikan ganti pengguna ganti ganti :q: 