��          �   %   �      P     Q     b     w     �     �     �  
   �     �     �     �     �     �  /        3     Q     p     v     �     �     �     �     �     �     �       �  %  	   �  	   �     �                +     B     K     T     Z     `     o  >   }  !   �  (   �                    %  #   ,  $   P  !   u  "   �     �     �                            
                                                                                             	    %1 file %1 files %1 folder %1 folders %1 of %2 processed %1 of %2 processed at %3/s %1 processed %1 processed at %2/s Appearance Behavior Cancel Clear Configure... Finished Jobs List of running file transfers/jobs (kuiserver) Move them to a different list Move them to a different list. Pause Remove them Remove them. Resume Show all jobs in a list Show all jobs in a list. Show all jobs in a tree Show all jobs in a tree. Show separate windows Show separate windows. Project-Id-Version: kuiserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2017-12-19 07:30+0700
Last-Translator: Wantoyo <wantoyek@gmail.com>
Language-Team: Indonesian <translation-team-id@lists.sourceforge.net>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 %1 berkas %1 folder %1 dari %2 diproses %1 dari %2 diproses dalam %3/d %1 diproses %1 diproses dalam %2/d Tampilan Perilaku Batal Hapus Konfigurasi... Tugas Selesai Senarai transfer/tugas berkas yang sedang berjalan (kuiserver) Pindahkan ke senarai yang berbeda Pindahkan tugas ke senarai yang berbeda. Jeda Hapus tugas Hapus tugas. Lanjut Tampilkan semua tugas dalam senarai Tampilkan semua tugas dalam senarai. Tampilkan semua tugas dalam pohon Tampilkan semua tugas dalam pohon. Tampilkan jendela terpisah Tampilkan jendela terpisah. 