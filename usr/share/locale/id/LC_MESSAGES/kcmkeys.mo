��          �   %   �      @     A     H     Z  �   i  �   %  
   �     �       2   !     T     Y     j     o     �     �     �     �     �  /   �     "     B  ?   \  =   �  S   �  �  .     �     �     �  �     �   �     �	     �	     �	  ,   �	     
     
     +
     0
     D
     X
     g
     t
     �
  *   �
     �
     �
  A     3   M  ^   �                                                            	                            
                                      &Path: &Standard scheme: All Components Are you sure you want to remove the registered shortcuts for component '%1'? The component and shortcuts will reregister themselves with their default settings when they are next started. Component '%1' is currently active. Only global shortcuts currently not active will be removed from the list.
All global shortcuts will reregister themselves with their defaults when they are next started. Components Current Component Export Scheme... Failed to contact the KDE global shortcuts daemon
 File Import Scheme... Load Load Shortcut Scheme Message: %1
Error: %2 Remove component Reset to defaults Select Shortcut Scheme Select a shortcut scheme file Select one of the standard KDE shortcut schemes Select the Components to Export Set All Shortcuts to None This file (%1) does not exist. You can only select local files. You are about to reset all shortcuts to their default values. Your current changes will be lost if you load another scheme before saving this one Project-Id-Version: kcmkeys
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-03 04:12+0100
PO-Revision-Date: 2017-12-27 21:53+0700
Last-Translator: Wantoyo <wantoyek@gmail.com>
Language-Team: Indonesian <translation-team-id@lists.sourceforge.net>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 &Alur: &Skema standar: Semua Komponen Apakah anda yakin ingin menghapus pintasan terdaftar untuk komponen '%1'? Komponen dan pintasan akan mendaftarkan ulang dirinya sendiri dengan setelan baku masing-masing ketika dijalankan ulang berikutnya. Komponen '%1' saat ini aktif. Hanya pintasan global yang saat ini tidak aktif yang akan dihapus dari senarai.
Semua pintasan global akan mendaftarkan ulang dirinya sendiri dengan baku masing-masing ketika dijalankan ulang berikutnya. Komponen Komponen Saat Ini Ekspor Skema... Gagal menghubungi jurik pintasan global KDE
 Berkas Impor Skema... Muat Muat Skema Pintasan Pesan: %1
Galat: %2 Hapus komponen Ubah ke baku Pilih Skema Pintasan Pilih berkas skema pintasan Pilih satu dari skema pintasan KDE standar Pilih Komponen Untuk Diekspor Atur Semua Pintasan ke Nihil Berkas ini (%1) tidak ada. Anda hanya dapat memilih berkas lokal. Anda akan mengubah semua pintasan ke nilai bakunya. Pengubahan anda saat ini akan hilang jika anda memuat skema lainnya sebelum menyimpan yang ini 