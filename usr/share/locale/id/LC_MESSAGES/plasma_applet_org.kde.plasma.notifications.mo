��          �   %   �      0      1     R     ^  -   }  #   �  #   �     �  V   �     O     [     h     ~     �  
   �  )   �  8   �  #     -   1  D   _  6   �  0   �  A     9   N  �  �     8     F     T  -   k  	   �  	   �     �  
   �  	   �     �     �     �            (   %     N  '   ^     �     �     �  	   �     �     �                                                                                                                  
      	       %1 notification %1 notifications %1 of %2 %3 %1 running job %1 running jobs &Configure Event Notifications and Actions... 10 seconds ago, keep short10 s ago 30 seconds ago, keep short30 s ago Copy How much many bytes (or whether unit in the locale has been copied over total%1 of %2 Information Job Finished No new notifications. No notifications or jobs Open... Select All Show application and system notifications Speed and estimated time to completion%1 (%2 remaining) Track file transfers and other jobs minutes ago, keep short%1 min ago %1 min ago notification was added n days ago, keep short%1 day ago %1 days ago notification was added yesterday, keep shortYesterday notification was just added, keep shortJust now placeholder is row description, such as Source or Destination%1: the job, which can be anything, has finished%1: Finished Project-Id-Version: plasma_applet_notifications
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:07+0100
PO-Revision-Date: 2017-12-20 06:49+0700
Last-Translator: Wantoyo <wantoyek@gmail.com>
Language-Team: Indonesian <kde-i18n-doc@kde.org>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 %1 notifikasi %1 dari %2 %3 %1 tugas yang berjalan &Konfigurasi Notifikasi dan Aksi Peristiwa... 10 d lalu 30 d lalu Salin %1 dari %2 Informasi Pekerjaan Selesai Tak ada notifikasi baru. Tak ada notifikasi atau tugas Buka... Pilih Semua Tampilkan notifikasi aplikasi dan sistem %1 (%2 tersisa) Lacak transfer berkas dan tugas lainnya %1 men lalu %1 hari lalu Kemarin Baru saja %1: %1: Selesai 