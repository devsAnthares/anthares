��          �   %   �      0     1  L   6  /   �     �     �     �     �               !     7     F     W  *   [     �     �     �  "   �     �  6     *   F     q     ~  �  �     }  `   �  -   �          .  #   2     V     m     y  !   �     �     �     �     �     �     �     �            ?   7  ;   w  
   �  
   �           	                            
                                                                                         to  <p>Some changes such as DPI will only affect newly started applications.</p> A non-proportional font (i.e. typewriter font). Ad&just All Fonts... BGR Click to change all fonts Configure Anti-Alias Settings Configure... E&xclude range: Font Settings Changed Font role%1:  Force fonts DPI: RGB Smallest font that is still readable well. Use a&nti-aliasing: Use anti-aliasingDisabled Use anti-aliasingEnabled Used by menu bars and popup menus. Used by the window titlebar. Used for normal text (e.g. button labels, list items). Used to display text beside toolbar icons. Vertical BGR Vertical RGB Project-Id-Version: kcmfonts
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-11 03:37+0200
PO-Revision-Date: 2007-12-24 01:28+0100
Last-Translator: Børre Gaup <boerre@skolelinux.no>
Language-Team: Northern Sami <i18n-sme@lister.ping.uio.no>
Language: se
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
  gitta <p>Muhton rievdadusat, nugo DPI, guoská dušše prográmmaide mat álggahuvvojit ođđasit.</p> Dássegovddat fonta (dego čállinmašiinnas) &Heivet buot fonttaid … BGR Coahkkal molsundihtii buot fonttaid Fontasođbenheivehusat Heivet … &Lihcu dán gaskka: Fontaheivehusat leat rievdaduvvon %1:  Bágge fonta-DPI: RGB Unnimus logahahtti fonta. Geavat &fontasođbema Eret Alde Geavahuvvo fáluin. Geavahuvvo lásenamahusholggas Geavahuvvo dábálaš tekstii (omd boalut, mearkkat, govvožat) Geavahuvvo čájehit teavstta reaidoholgagovažiid balddas. Ceakko BGR Ceakko RGB 