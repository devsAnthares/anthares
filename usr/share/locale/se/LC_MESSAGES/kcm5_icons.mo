��    3      �  G   L      h     i     r     {     �     �     �  �   �  -   J  )   x  -   �  +   �  r   �  	   o  	   y     �     �     �     �     �  
   �     �     �      �          (     .     ;     A     \     m     z     �  	   �     �     �     �     �     �     �               %  +   1     ]     e     s     {  S   �  )   �     	  �  	       
             &     8     A  �   ]  4   �     0     <  
   A  c   L     �     �     �     �     �     �          
  
         !     B     W     l     r          �     �     �     �     �     �     �     �     �       %   &  #   L  +   p  
   �     �  $   �     �     �     �     
  R     $   j     �     +         ,                $              (       "   #                       !            )       -                   /                             0      '      *           
      	                 3                  2          %   &   1   .          &Amount: &Effect: &Second color: &Semi-transparent &Theme (c) 2000-2003 Geert Jansen <qt>Are you sure you want to remove the <strong>%1</strong> icon theme?<br /><br />This will delete the files installed by this theme.</qt> <qt>Installing <strong>%1</strong> theme</qt> @label The icon rendered as activeActive @label The icon rendered as disabledDisabled @label The icon rendered by defaultDefault A problem occurred during the installation process; however, most of the themes in the archive have been installed Ad&vanced All Icons Animate icons Antonio Larrosa Jimenez Co&lor: Colorize Confirmation Desaturate Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Effect Parameters Gamma Geert Jansen Icons Icons Control Panel Module Jonathan Riddell Main Toolbar NAME OF TRANSLATORSYour names Name No Effect Panel Preview Remove Theme Set Effect... Setup Active Icon Effect Setup Default Icon Effect Setup Disabled Icon Effect Size: Small Icons The file is not a valid icon theme archive. To Gray To Monochrome Toolbar Torsten Rahn Unable to download the icon theme archive;
please check that address %1 is correct. Unable to find the icon theme archive %1. Use of Icon Project-Id-Version: kcmicons
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-02-08 04:00+0100
PO-Revision-Date: 2007-12-24 01:29+0100
Last-Translator: Børre Gaup <boerre@skolelinux.no>
Language-Team: Northern Sami <i18n-sme@lister.ping.uio.no>
Language: se
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 &Dássi: &Effeakta: &Nuppi ivdni: &Beallenjuođvvas &Fáddá © 2000–2003 Geert Jansen <qt>Áiggut go duođas váldit <strong>%1</strong> govašfáttá eret?<br /><br />Dat máksá ahte sihkut fiillaid maid dát fáddá lea sajáiduhttán.</qt> <qt>Sajáiduhttimin <strong>%1</strong> fáttá</qt> Aktiivalaš Eret Standárda Šattai váttisvuođaid sajáiduhtedettiin. Aŋke eanáš fáddát vuorkás sajáiduhttojuvvojedje. E&renoamáš Buot govažat Animere govažiid Antonio Larrosa Jimenez &Ivdni: Ivdne Nannen Gilggodahte Válddahus Gease dahje čále fáddá-URL:a boerre@skolelinux.no Effeaktaparamehterat Gamma Geert Jansen Govažat Govažiid stivrenmoduvla Jonathan Riddell Váldoreaidoholga Børre Gaup Namma Ii effeakta Panela Ovdačájeheapmi Váldde fáttá eret Vállje effeavtta … Heivet aktiivalašgovažiid effeavtta Heivet standárdgovažiid effeavtta Heivet deaktiverejuvvon govažiid effeavtta Sturrodat: Smávva govažat Fiila gustohis govašfáddávuorká. Ránesguvlui Čáhppes-vielgadin Neavvoholga Torsten Rahn Ii sáhttán viežžat govašfáddávuorká.
Dárkkis ahte čujuhus %1 lea riekta. Ii gávdnan govašfáddávuorká %1. Govašgeavaheapmi 