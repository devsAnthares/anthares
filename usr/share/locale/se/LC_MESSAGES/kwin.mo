��    O      �  k         �     �     �     �     �  
   �     �     �     �            
   (     3      Q     r  /   �     �     �     �  p   �     R     e     x     �     �     �     �     �  	   �  
   �     	     	     +	     ;	     X	  	   s	     }	     �	     �	     �	     �	     �	     
     
     *
     C
  9   P
     �
     �
     �
     �
     �
     �
          (     ?     U  :   p  	   �     �     �  #   �          #     C     X     w     �     �     �     �     �          (    C  �   X  S   6     �     �  �  �     �     �     �     �     �     �     �     �     �            !   )     K     `  4   s     �     �     �  s   �     H     e  "   �     �  #   �     �     �       
             *     B     S     c     {  	   �     �     �     �  "   �  "   �          $     >     Y     u  >   �     �     �     �          $     A     ^  "   |     �     �  L   �     "     1     J  .   j  &   �  5   �     �  $     .   0  /   _  .   �  /   �     �  "        '     >  	  \  �   f  t   L     �     �                  +          B      .                 D   K   /       E           M          @             :   5   4       -                            *       N      0           $            6   I   	   (   2      )   7       ;   %   H       A   &          8   O         L   
       J   ?       1      #          '       9   F   >              C   "   3   <          ,   !             =          G    &All Desktops &Close &Fullscreen &Move &No Border &Resize &Shade Close Window Cristian Tibirna Daniel M. Duley Desktop %1 Disable configuration options EMAIL OF TRANSLATORSYour emails Hide Window Border Indicate that KWin has recently crashed n times KDE window manager KWin KWin helper utility KWin is unstable.
It seems to have crashed several times in a row.
You can select another window manager to run: Keep &Above Others Keep &Below Others Keep Window Above Others Keep Window Below Others Keep Window on All Desktops Kill Window Lower Window Luboš Luňák Ma&ximize Maintainer Make Window Fullscreen Matthias Ettrich Maximize Window Maximize Window Horizontally Maximize Window Vertically Mi&nimize Minimize Window Move Window NAME OF TRANSLATORSYour names Pack Grow Window Horizontally Pack Grow Window Vertically Pack Window Down Pack Window Up Pack Window to the Left Pack Window to the Right Raise Window Replace already-running ICCCM2.0-compliant window manager Resize Window Setup Window Shortcut Shade Window Switch One Desktop Down Switch One Desktop Up Switch One Desktop to the Left Switch One Desktop to the Right Switch to Next Desktop Switch to Next Screen Switch to Previous Desktop This helper utility is not supposed to be called directly. Timestamp Toggle Window Raise/Lower Walk Through Desktop List Walk Through Desktop List (Reverse) Walk Through Desktops Walk Through Desktops (Reverse) Walk Through Windows Walk Through Windows (Reverse) Window One Desktop Down Window One Desktop Up Window One Desktop to the Left Window One Desktop to the Right Window Operations Menu Window to Next Desktop Window to Next Screen Window to Previous Desktop You have selected to show a window in fullscreen mode.
If the application itself does not have an option to turn the fullscreen mode off you will not be able to disable it again using the mouse: use the window operations menu instead, activated using the %1 keyboard shortcut. You have selected to show a window without its border.
Without the border, you will not be able to enable the border again using the mouse: use the window operations menu instead, activated using the %1 keyboard shortcut. kwin: unable to claim manager selection, another wm running? (try using --replace)
 name time Project-Id-Version: kwin
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-02 03:25+0200
PO-Revision-Date: 2007-12-20 11:35+0100
Last-Translator: Børre Gaup <boerre@skolelinux.no>
Language-Team: Northern Sami <i18n-sme@lister.ping.uio.no>
Language: se
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 &Buot čállinbevddiide &Gidde &Deavasšearbma &Sirdde &Rávddaid haga &Rievdat sturrodaga &Rulle bajás Gidde láse Cristian Tibirna Daniel M. Duley Cállinbeavdi %1 Ale geavat heivehusmolssaeavttuid boerre@skolelinux.no Čiega láserámma Muittut ahte KWin dál easka lea riekčanan máŋgii KDE lásegieđahalli KWin KWin-veahkereaidu KWin ii leat starga.
Lea riekčanan moddii maŋŋálága.
Sáhtát válljet vuodjit muhttin eará lásegieđahalli: Čá&jet earáid bajábealde Čájet &earáid vuolábealde Doalat láse earáid bajábeallái Čájet láse earáid duohkken Čájet láse buot čállinbevddiin Gotte láse Vuolit láse Luboš Luňák Ma&ksimere Máŧasdoalli Deavdde olleš šearpma Matthias Ettrich Maksimere láse Maksimere láse láskut Maksimere láse ceaggut Mi&nimere Minimere láse Sirdde láse Børre Gaup Skále láse lassáneaddji láskut Skále láse lassáneaddji ceaggut Skále láse vulosguvlui Skále láse bajásguvlui Skále láse gurutbeallái Skále láse olgešbeallái Lokte láse Buhtte ICCCM2.0-heivvolaš lásegieđahalli mii juo lea jođus Rievdat láse sturrodaga Heivet láselávkestagaid Máhcu láse oktii Lonut vuolil čállinbeavdái Lonut bajil čállinbeavdái Lonut gurut čállinbeavdái Lonut olgeš čállinbeavdái Lonut čuovvovaš čállinbeavdái Molsso boahtte šerbmii Lonut ovddit čállinbeavdái Ii leat jurddašuvvon ahte gálggat gohččodit dán veahkereaiddu njuolggo. Áigesteamppal Lokte dahje vuolit láse Bláđđe čállinbeavdelisttus Bláđđe čállinbeavdelisttus (maŋosguvlui) Mana čállinbeavddis čállinbeavdái Mana čállinbeavddis čállinbeavdái (maŋosguvlui) Sirde láses lássii Sirdde láses lássii (maŋosguvlui) Sádde láse ovtta čállinbeavddi vulosguvlui Sádde láse ovtta čállinbeavddi bajásguvlui Sádde láse ovtta čállinbeavddi gurutguvlui Sádde láse ovtta čállinbeavddi olgešguvlui Lásiid doaimmafállu Láse čuovvovaš čállinbeavdái Láse boahtte šerbmii Láse ovddit čállinbeavdái Leat válljen čájehit láse dievasšearbmamodusas.
Jus ieš prográmmas ii leat molssaeaktu mainna guođát dán modusa, de it sáhte guođđit dán modusa sáhpániin, ferte baicce geavahit lásedoaimmaid fálu, mii aktiverejuvvo go deaddilat %1 njuolggobálgá. Don leat válljen ahte láse galgá leat rávddaid haga.
Go rávdalinnjá lea cihkon, de it sáhtte čajehit dan fas geavahettiin sáhpána. Fertet baicce geavahit lásedoaimmafálu, mii čájehuvvo go geavahat jođánisboalu %1. kwin: Ii sáhttán gieđahallat lásiid. Leago eará lásegieđahalli jođus? (Geahččal --replace molssaeavttuin)
 namma áigi 