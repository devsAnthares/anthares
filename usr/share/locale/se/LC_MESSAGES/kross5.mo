��            )   �      �  &   �     �     �     �     �      �               .     6  ,   S  3   �     �     �     �     �     �  '        4     S     Y     o     �     �     �     �     �  &   �     �  �  �     �     �  
             &     3     H     Q     q       2   �  9   �  %   	  (   /     X     _     g     n     �     �     �     �     �  #   �     	     
	     *	     1	     O	                                       
                      	                                                                            @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2012-05-30 23:19+0200
Last-Translator: Børre Gaup <boerre@skolelinux.no>
Language-Team: Northern Sami <i18n-sme@lister.ping.uio.no>
Language: se
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.4
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Oppalaš Lasit ođđa skripta. Lasit … Gaskkalduhte? Kommentára: boerre@skolelinux.no Doaimmat Doaimmat válljejuvvon skripta. Doaimmat … Vuoje válljejuvvon skripta. Filtii skripta ráhkadeames «%1»-dulkka várás. Ii sáhttán meroštit dulkka skriptafiila «%1» várás Ii sáhttán viežžat dulkka «%1»: Ii sáhttán rahpat skriptafiilla «%1» Fiila: Govaš: Dulka: Ruby-dulkka sihkkarvuođadássi Børre Gaup Namma: Doaibma «%1» ii gávdno Dulka «%1» ii gávdno Váldde eret Váldde eret válljejuvvon skripta. Vuoje Skriptafiila «%1» ii gávdno. Bisset Bisset válljejuvvon skripta. Teaksta: 