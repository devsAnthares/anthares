��    #      4  /   L           	       
              2  #   D  J   h     �     �     �  H   �           :  M   H     �     �  �   �     =     T     s  1   |  !   �  .   �  0   �      0  :   Q  #   �     �     �  N   �  �        �     �     �  �  �     �	  	   �	     �	     
     
  !   .
  O   P
  	   �
     �
     �
  F   �
            8   .     g     m  �   �  '        F  
   R  F   ]  3   �  C   �  E     2   b  P   �  5   �          4  V   :  �   �     :     R     l                            	                                          
                          #                   !      "                                     msec &Bell &Duration: &Keyboard Filters &Use visible bell (c) 2000, Matthias Hoelzer-Kluepfel All screen colors will be inverted for the amount of time specified below. AltGraph Audible Bell Author Click here to choose the color used for the "flash screen" visible bell. EMAIL OF TRANSLATORSYour emails F&lash screen Here you can customize the duration of the "visible bell" effect being shown. Hyper I&nvert screen If the option "Use customized bell" is enabled, you can choose a sound file here. Click "Browse..." to choose a sound file using the file dialog. KDE Accessibility Tool NAME OF TRANSLATORSYour names Press %1 Press %1 while CapsLock and ScrollLock are active Press %1 while CapsLock is active Press %1 while NumLock and CapsLock are active Press %1 while NumLock and ScrollLock are active Press %1 while NumLock is active Press %1 while NumLock, CapsLock and ScrollLock are active Press %1 while ScrollLock is active Sound &to play: Super The screen will turn to a custom color for the amount of time specified below. This option will turn on the "visible bell", i.e. a visible notification shown every time that normally just a bell would occur. This is especially useful for deaf people. Us&e customized bell Use &system bell Visible Bell Project-Id-Version: kcmaccess
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2005-10-23 23:34+0200
Last-Translator: Børre Gaup <boerre@skolelinux.no>
Language-Team: Northern Sami <i18n-sme@lister.ping.uio.no>
Language: se
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
  ms &Signála &Guhkkodat: &Boallobeavdesillit &Geavat oaidnus signála © 2000 Matthias Hoelzer-Kluepfel Buot šearbmaivnnit inverterejuvvo nu guhkká go mearriduvvo dás vuolábealde. Alt Graph Jietnasignála Čálli Coahkkal dása válljet ivnni mii geavahuvvo oaidnus signála várás. boerre@skolelinux.no &Livkke šearpma Dás mearridat man guhkká oaidnus signála čájehuvvo. Hyper &Jorgalahte ivnniid Jos «Vállje iežat signála» lea merkejuvvon, de sáhtát válljet jietnafiilla dáppe. Deaddil «Bláđđe» boalu válljet jietnafiilla fiilalásežis. KDE neavvu álkkibut geavaheami várás Børre Gaup Deaddil %1 %1 deaddejuvvo seammásgo Caps Lock ja Scroll Lock leat aktiivalačča %1 deaddejuvvo seammásgo Caps Lock lea aktiivalaš %1 deaddujuvvo seammásgo Num Lock ja Caps Lock leat aktiivalačča %1 deaddejuvvo seammásgo Num Lock ja Scroll Lock leat aktiivalačča %1 deaddejuvvo seammásgo Num Lock lea aktiivalaš %1 deaddejuvvo seammášgo Num Lock, Caps Lock ja ScrollLock leat aktiivalačča %1 deaddujuvvo seammásgo Scroll Lock lea aktiivalaš Jietna &maid čuojahit: Super Šearbmá molsu mearriduvvon ivdnái nu guhkká go lea mearriduvvon dás vuolábealde. Dát molssaeaktu bidja «oaidnus signála» doaibmat, dát máksá ahte oaidnus signála boahtá vuogádatsignála sadjái. Dát lea hui ávkkálaš bealjehis olbmuide. Geavat &iežat signála Geavat &vuogádatsignála Oaidnus signála 