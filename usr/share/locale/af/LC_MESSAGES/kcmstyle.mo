��          �      �        &   	  �   0     �     �     �     �     �     �      �  �     c   �     I     Z     y     �     �     �     �  	   �  C   �  j        m  �  �  &   2  �   Y     �     �  	     
             (  -   7  �   e  f   O	     �	     �	     �	      
     	
  	   
  	   !
  
   +
  G   6
  |   ~
     �
               
                                                                      	                   (c) 2002 Karol Szwed, Daniel Molkentin <h1>Style</h1>This module allows you to modify the visual appearance of user interface elements, such as the widget style and effects. Button Checkbox Combobox Con&figure... Configure %1 Description: %1 EMAIL OF TRANSLATORSYour emails Here you can choose from a list of predefined widget styles (e.g. the way buttons are drawn) which may or may not be combined with a theme (additional information like a marble texture or a gradient). If you enable this option, KDE Applications will show small icons alongside some important buttons. KDE Style Module NAME OF TRANSLATORSYour names No description available. Preview Radio button Tab 1 Tab 2 Text Only There was an error loading the configuration dialog for this style. This area shows a preview of the currently selected style without having to apply it to the whole desktop. Unable to Load Dialog Project-Id-Version: kcmstyle stable
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-17 05:44+0100
PO-Revision-Date: 2005-06-30 11:29+0200
Last-Translator: JUANITA FRANZ <JUANITA.FRANZ@VR-WEB.DE>
Language-Team: AFRIKAANS <translate-discuss-af@lists.sourceforge.net>
Language: af
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
 (c) 2002 Karol Szwed, Daniel Molkentin <h1>Styl</h1>Hierdie module laat jou toe om te verander die visuele voorkoms van gebruiker koppelvlak elemente, soos die gui-element Styl en effekte. Knoppie Keuseblokkie Komboboks Stel op... Konfigureer %1 Beskrywing: %1 frix@expertron.co.za, juanita.franz@vr-web.de Hier jy kan kies van 'n lys van vooraf gedefinieerde gui-element style (e.g. die weg knoppies word geteken) wat dalk mag of dalk mag nie wees gekombineer met 'n tema (aditionele informasie hou van 'n marmer tekstuur of 'n gradiënt). As jy aktiveer hierdie opsie, Kde Programme sal vertoon klein ikoone langs sommige belangrik knoppies. Kde Styl Module Frikkie Thirion, Juanita Franz Nee beskrywing beskikbaar. Voorskou Radio knoppie Oortjie 1 Oortjie 2 Teks Slegs Daar was 'n fout by laai van die konfigurasie dialoog vir hierdie styl. Hierdie area vertoon 'n voorskou van die huidiglik gekose styl sonder om te moet na wend aan dit na die volledige werkskerm. Nie moontlik om Dialoog te laai 