��    )      d  ;   �      �     �     �     �     �     �     �  -   �  r     	   �  	   �     �     �     �  
   �     �     �      �          "     (     .     I     h  	   m     w     }     �     �     �     �     �     �     �  +         ,     4     B  S   J  )   �     �  �  �     �     �     �     �     �     �  .   �  y   	  
   	     �	     �	     �	     �	  
   �	  
   �	     �	     �	     �	     
     
     
     0
     >
  
   C
     N
     U
     ^
     l
     z
     �
      �
     �
     �
  .   �
          %     3  X   <  1   �     �                         %   (          '                                                                             #                              "          )   &      	          $           !          
    &Amount: &Effect: &Second color: &Semi-transparent &Theme (c) 2000-2003 Geert Jansen <qt>Installing <strong>%1</strong> theme</qt> A problem occurred during the installation process; however, most of the themes in the archive have been installed Ad&vanced All Icons Co&lor: Colorize Confirmation Desaturate Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Effect Parameters Gamma Icons Icons Control Panel Module NAME OF TRANSLATORSYour names Name No Effect Panel Preview Remove Theme Set Effect... Setup Active Icon Effect Setup Default Icon Effect Setup Disabled Icon Effect Size: Small Icons The file is not a valid icon theme archive. To Gray To Monochrome Toolbar Unable to download the icon theme archive;
please check that address %1 is correct. Unable to find the icon theme archive %1. Use of Icon Project-Id-Version: kcmicons stable
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-08 06:27+0100
PO-Revision-Date: 2005-06-29 11:28+0200
Last-Translator: JUANITA FRANZ <JUANITA.FRANZ@VR-WEB.DE>
Language-Team: AFRIKAANS <translate-discuss-af@lists.sourceforge.net>
Language: af
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
 Hoeveelheid: Effek: Tweede kleur: Semi-deurskynend Tema (c) 2000-2003 Geert Jansen <qt>Installering <strong>%1</strong> tema</qt> 'n probleem het voorgekom gedurende die installasie proses; Die meeste van die temas in die argief is reeds geïnstalleer Gevorderde Alle Ikoone Kleur: Verkleur Bevestiging Desatureer Beskrywing Trek of Tipe Tema Url juanita.franz@vr-web.de Effek Parameters Gamma Ikoone Ikoone Beheer Paneel Module Juanita Franz Naam Geen Effek Paneel Voorskou Verwyder Tema Stel Effek... Opstelling Aktief Ikoon Effek Opstelling Verstek Ikoon Effek Opstelling Gestremde Ikoon Effek Grootte: Klein Ikoone Die lêer is nie 'n geldige ikoon tema argief. Na Grys Na Monochrome Nutsbalk Nie moontlik na aflaai die ikoon tema argief;
Asseblief bevestig dat adres %1 is korrek. Nie moontlik om te vind die ikoon tema argief %1! Gebruik van Ikoon 