��          �   %   �      @     A  �   `  m   �  �   P  )   �  `        o     |     �     �     �      �     �     �  '        ,     >     ]     b     s  ?   �  Y   �  +     H   F  �  �     6  �   U  r   �     W	     v	  T   �	  
   �	      �	     
  
   !
     ,
     J
     Z
     k
  +   x
     �
     �
     �
  !   �
     �
  /   �
  S   '  #   {  Y   �                                                 	                                                               
              (c) 2003-2007 Fredrik Höglund <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) @item:inlistbox sizeResolution dependent A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Cursor Theme Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Fredrik Höglund Get new Theme Get new color schemes from the Internet Install from File NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. You have to restart the Plasma session for these changes to take effect. Project-Id-Version: kcminput
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2017-04-13 14:18+0100
Last-Translator: Volkan Gezer <volkangezer@gmail.com>
Language-Team: Turkish <kde-l10n-tr@kde.org>
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 (c) 2003-2007 Fredrik Höglund <qt><i>%1</i> imleç temasını kaldırmak istediğinizden emin misiniz?<br />Bu işlem bu tema ile yüklenen tüm dosyaları silecektir.</qt> <qt>Şu anda kullanımda olan temayı silemezsiniz.<br />Önce başka bir temayı kullanıma almayı deneyin.</qt> (Kullanılabilir boyutlar: %1) Çözünürlüğe bağlı %1 isminde bir tema, simge temaları dizininde var. Üzerine yazmak istiyor musunuz? Doğrulama İmleç Ayarları Değiştirildi İmleç Teması Açıklama Tema Adresini Taşı veya Yaz adil@kde.org.tr Fredrik Höglund Yeni Tema Al Internet üzerinden yeni renk şemaları al Dosyadan Kur Adil Yıldız İsim Temanın Üzerine Yazılsın mı? Temayı Kaldır %1 dosya geçerli bir imleç tema dosya değil. İmleç tema arşivi indirilemedi; lütfen %1 adresinin doğruluğunu kontrol edin. %1 imleç tema arşivi bulunamadı. Bu değişikliklerin etkili olması için Plasma oturumunu yeniden başlatmanız gerekir. 