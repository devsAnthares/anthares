��          �      �       H     I  
   a     l  .   r     �     �      �  *   �        ,   '  ,   T  0   �     �  �  �     e     �     �  )   �     �     �  )   �  !     &   %     L     T  .   \     �     	                                              
              &Lock screen on resume: Activation Error Failed to successfully test the screen locker. Immediately Lock Session Lock screen automatically after: Lock screen when waking up from suspension Re&quire password after locking: Spinbox suffix. Short for minutes min  mins Spinbox suffix. Short for seconds sec  secs The global keyboard shortcut to lock the screen. Wallpaper &Type: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:10+0100
PO-Revision-Date: 2017-03-15 10:42+0100
Last-Translator: Volkan Gezer <volkangezer@gmail.com>
Language-Team: Turkish <kde-l10n-tr@kde.org>
Language: tr_TR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 &Devam ederken ekranı kilitle: Etkinleştirme Hata Ekran kilidini sınama başarısız oldu. Hemen Ekranı Kilitle Ekranı şu süre sonra otomatik kilitle: Uykudan uyanınca ekranı kilitle Kilitlendikten sonra &parola gerektir:  dk  dk  sn  sn Ekranı kilitlemek için evrensel kısayollar. Duvar Kağıdı &Türü: 