��          �      <      �     �     �     �  +   �  @        L     a     i     w     }     �  
   �     �     �     �     �     �  �  
     �     �     �  0   �  P        X     s     y     �     �     �     �     �     �     �       '   '     
         	                                                                                       <a href='%1'>%1</a> Close Copy Automatically: Don't show this dialog, copy automatically. Drop text or an image onto me to upload it to an online service. Error during upload. General History Size: Paste Please wait Please, try again. Sending... Share Shares for '%1' Successfully uploaded The URL was just shared Upload %1 to an online service Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-01 04:04+0100
PO-Revision-Date: 2015-05-19 12:46+0000
Last-Translator: Necdet <necdetyucel@gmail.com>
Language-Team: Turkish <kde-l10n-tr@kde.org>
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 1.5
 <a href='%1'>%1</a> Kapat Otomatik olarak kopyala: Bu pencereyi gösterme, otomatik olarak kopyala. Bir çevrimiçi hizmete yüklemek için üzerime bir resim veya metin bırakın. Yükleme sırasında hata. Genel Geçmiş Boyutu: Yapıştır Lütfen bekleyin Lütfen, yeniden deneyin. Gönderiyor... Paylaş '%1' için paylaşımlar Başarıyla yüklendi Bu adres az önce paylaşıldı %1 bir çevirim içi servise yüklensin 