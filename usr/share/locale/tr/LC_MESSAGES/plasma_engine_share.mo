��    
      l      �       �   $   �   %     :   <     w  '   �  -   �     �       '     �  <  #   �  &     -   ?  3   m  )   �     �     �       /            	                            
        Could not detect the file's mimetype Could not find all required functions Could not find the provider with the specified destination Error trying to execute script Invalid path for the requested provider It was not possible to read the selected file Service was not available Unknown Error You must specify a URL for this service Project-Id-Version: plasma_engine_share
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2014-10-21 09:49+0000
Last-Translator: Necdet <necdetyucel@gmail.com>
Language-Team: American English <kde-l10n-tr@kde.org>
Language: en_US
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 1.5
 Dosyanın mime türü belirlenemedi Gerekli işlevlerin tümü bulunamadı Belirtilen hedefteki sağlayıcı bulunamadı Betiği yürütmeye çalışırken bir hata oluştu İstenen sağlayıcı için geçersiz yol Seçilen dosya okunamadı Hizmet kullanılabilir değildi Bilinmeyen Hata Bu hizmet için bir adres (URL) belirtmelisiniz 