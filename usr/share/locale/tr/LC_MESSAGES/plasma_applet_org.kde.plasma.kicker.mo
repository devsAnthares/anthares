��    _                   C   	  /   M  -   }     �     �     �     �      	     	     1	     >	     J	  
   S	     ^	     g	     p	     �	  	   �	     �	     �	     �	  ,   �	  	    
     

  
   )
     4
     L
     `
     u
     �
     �
     �
     �
     �
  	   �
     �
     �
     
               !     (  	   ;     E     ]  
   r     }     �  
   �     �     �     �  
   �     �     �               $     2     @     R     h     y     �     �     �     �  	   �     �     �     �               4     Q     j     �     �     �     �  	   �     �  ,   �          !     0     @     L     S     b     t     �  #   �     �  �  �     �     �     �     �     �     �  &   �     $     5     =     I  
   `     k  
   w     �     �     �     �     �     �     �  9        <  (   P     y     �     �     �     �     �     �     �          $     ,     <     E     W     ^     f     v     |     �     �     �     �     �     �          (     E     L     [     g     w     �     �     �     �     �          3     P  -   f     �     �     �     �     �     �     �     �       )   +  %   U  %   {  %   �     �  !   �     �            5   '  
   ]     h     {     �     �     �     �     �  $   �  -     	   9         Q         Y   5           %       H       J   !              \   *       @   V   4   [       A   (   
       U   B   ]   ^       $   _         '   >       D   K      -                  ?      ,   O   0       R   8   6   W   2   I   =   X   E   #       N   C       T   G   M          3   "         9             +          S          F           ;   :                    &   <   L          P         7          .       /       1                     )            	   Z       @action opens a software center with the applicationManage '%1'... @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Desktop Add to Favorites Add to Panel (Widget) Align search results to bottom All Applications App name (Generic name)%1 (%2) Applications Apps & Docs Behavior Categories Computer Contacts Description (Name) Description only Documents Edit Application... Edit Applications... End session Expand search to bookmarks, files and emails Favorites Flatten menu to a single level Forget All Forget All Applications Forget All Contacts Forget All Documents Forget Application Forget Contact Forget Document Forget Recent Documents General Generic name (App name)%1 (%2) Hibernate Hide %1 Hide Application Icon: Lock Lock screen Logout Name (Description) Name only Often Used Applications Often Used Documents Often used On All Activities On The Current Activity Open with: Pin to Task Manager Places Power / Session Properties Reboot Recent Applications Recent Contacts Recent Documents Recently Used Recently used Removable Storage Remove from Favorites Restart computer Run Command... Run a command or a search query Save Session Search Search results Search... Searching for '%1' Session Show Contact Information... Show In Favorites Show applications as: Show often used applications Show often used contacts Show often used documents Show recent applications Show recent contacts Show recent documents Show: Shut Down Sort alphabetically Start a parallel session as a different user Suspend Suspend to RAM Suspend to disk Switch User System System actions Turn off computer Type to search. Unhide Applications in '%1' Unhide Applications in this Submenu Widgets Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2017-10-04 12:15+0000
Last-Translator: Kaan <kaanozdincer@gmail.com>
Language-Team: Turkish <kde-l10n-tr@kde.org>
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-POOTLE-MTIME: 1413102230.000000
 '%1' uygulamasını yönet... Seç... Simgeyi temizle Masaüstüne Ekle Sık Kullanılanlara Ekle Panele Ekle (Gereç) Arama sonuçlarını aşağıya hizala Tüm Uygulamalar %1 (%2) Uygulamalar Uygulamalar & Belgeler Davranış Kategoriler Bilgisayar Bağlantılar Açıklama (İsim) Yalnızca tanım Belgeler Uygulamayı Düzenle... Uygulamaları Düzenle... Oturumu sonlandır Aramayı yer imlerine, dosyalara ve e-postalara genişlet Sık Kullanılanlar Tek bir seviyede düzleştirilmiş menü Tümünü Unut Tüm Uygulamaları Unut Tüm Bağlantıları Unut Tüm Belgeleri Unut Uygulamayı Unut Bağlantıyı Unut Belgeyi Unut Son Kullanılan Belgeleri Unut Genel %1 (%2) Hazırda Beklet %1 Gizle Uygulamayı Gizle Simge: Kilitle Ekranı kilitle Çık İsim (Açıklama) Sadece isim Sıkça Kullanılan Uygulamalar Sıkça Kullanılan Belgeler Sıkça kullanılan Tüm Etkinliklerde Sadece Geçerli Etkinlikte Birlikte aç: Görev Yöneticisine Sabitle Yerler Güç / Oturum Özellikler Yeniden başlat Son Kullanılan Uygulamalar Son Bağlantılar Son Kullanılan Belgeler Yakın Zamanda Kullanılanlar Yakın zamanda kullanılanlar Çıkarılabilir Depolama Sık Kullanılanlardan Kaldır Bilgisayarı yeniden başlat Komut Çalıştır... Bir komut çalıştır veya arama sorgusu yap Oturumu Kaydet Ara Arama sonuçları Ara... '%1' aranıyor Oturum Bağlantı Bilgisini Göster... Sık Kullanılanlarda Göster Uygulamaları göster: Sıkça kullanılan uygulamaları göster Sıkça kullanılan kişileri göster Sıkça kullanılan belgeleri göster Son kullanılan uygulamaları göster Son kişileri göster Son kullanılan belgeleri göster Göster: Kapat Alfabetik olarak sırala Farklı bir kullanıcı olarak paralel oturum başlat Askıya Al RAM ile askıya al Disk ile askıya al Kullanıcı Değiştir Sistem Sistem eylemleri Bilgisayarı kapat Aramak için yazın. '%1' Uygulamalarını Görünür Yap Bu alt menüdeki Uygulamaları görünür yap Gereçler 