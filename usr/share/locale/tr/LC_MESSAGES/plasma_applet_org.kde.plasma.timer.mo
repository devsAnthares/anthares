��          �      �           	          &     -     4  
   =     H     Q     Y     i  >   w     �     �     �     �  
   �     �     �     �            U   %  �  {     d     u  
   �     �  
   �  
   �     �  
   �     �     �  -   �          $     ,     >     O     c     j     x     �  	   �  �   �                       	                                                
                                 %1 is running %1 not running &Reset &Start Advanced Appearance Command: Display Execute command Notifications Remaining time left: %1 second Remaining time left: %1 seconds Run command S&top Show notification Show seconds Show title Text: Timer Timer finished Timer is running Title: Use mouse wheel to change digits or choose from predefined timers in the context menu Project-Id-Version: kdeplasma-addons-kde4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2015-05-23 12:19+0000
Last-Translator: Necdet <necdetyucel@gmail.com>
Language-Team: Turkish (http://www.transifex.com/projects/p/kdeplasma-addons-k-tr/language/tr/)
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-POOTLE-MTIME: 1413102232.000000
 %1 çalışıyor %1 çalışmıyor &Sıfırla &Başlat Gelişmiş Görünüm Komut: Görünüm Komutu çalıştır Bildirimler Kalan zaman: %1 saniye Kalan zaman: %1 saniye Komut çalıştır D&urdur Bildirimi göster Saniyeyi göster Başlığı göster Metin: Zamanlayıcı Zamanlayıcı sonlandı Zamanlayıcı çalışıyor Başlık: Basamakları değiştirmek veya içerik menüsünden önceden tanımlanmış zamanlayıcıları seçmek için farenin tekerleğini kullan 