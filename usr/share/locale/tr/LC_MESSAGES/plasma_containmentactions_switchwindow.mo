��          T      �       �   "   �      �   *   �   3   '  E   [  .   �  �  �  *   �  $   �  4   �       -   &  
   T                                        Display a submenu for each desktop Display all windows in one list Display only the current desktop's windows plasma_containmentactions_switchwindowAll Desktops plasma_containmentactions_switchwindowConfigure Switch Window Plugin plasma_containmentactions_switchwindowWindows Project-Id-Version: plasma_containmentactions_switchwindow
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-30 03:13+0200
PO-Revision-Date: 2014-10-21 09:49+0000
Last-Translator: Necdet <necdetyucel@gmail.com>
Language-Team: Turkish <kde-l10n-tr@kde.org>
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 1.5
 Her masaüstü için bir alt menü göster Tüm pencereleri bir listede göster Sadece geçerli masaüstünün pencerelerini göster Tüm Masaüstleri Pencere Değiştirme Eklentisini Yapılandır Pencereler 