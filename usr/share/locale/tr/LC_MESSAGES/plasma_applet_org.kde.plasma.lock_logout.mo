��          �      L      �  &   �  +   �       @     	   ]     g     �     �     �     �  (   �     �     �  ,   �               +     7  �  ;  B   �  :   4     o     u  
   ~  #   �     �     �     �     �  .   �          
  4   *     _     f     }     �        
                          	                                                                  Do you want to suspend to RAM (sleep)? Do you want to suspend to disk (hibernate)? General Heading for list of actions (leave, lock, shutdown, ...)Actions Hibernate Hibernate (suspend to disk) Leave Leave... Lock Lock the screen Logout, turn off or restart the computer No Sleep (suspend to RAM) Start a parallel session as a different user Suspend Switch User Switch user Yes Project-Id-Version: plasma_applet_lockout
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2015-07-10 18:50+0000
Last-Translator: Necdet <necdetyucel@gmail.com>
Language-Team: Turkish <kde-l10n-tr@kde.org>
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-POOTLE-MTIME: 1413102242.000000
 Bellek kullanarak bekletmek (uyku kipine geçmek) istiyor musunuz? Disk kullanarak bekletmek (askıya almak) istiyor musunuz? Genel Eylemler Askıya Al Askıya Al (disk kullanarak beklet) Çık Çık... Kilitle Ekranı kilitle Çık, bilgisayarı kapat veya yeniden başlat Hayır Uyku (bellek kullanarak beklet) Başka bir kullanıcı olarak paralel oturum başlat Beklet Kullanıcı Değiştir Kullanıcı değiştir Evet 