��          �      l      �  3   �  $     �   :     �  4   �     �  #        =  �   E  �   �  +   �     �     �     �  1     (   3     \  �   s  $   �  (     �  F  0   #     T     \     k  ;   z  !   �  >   �     	  �   	  �   �	  #   �
     �
     �
  )   �
  )   	  (   3  !   \     ~      �  &   �                                                   	   
                                            1 action for this device %1 actions for this device @info:status Free disk space%1 free Accessing is a less technical word for Mounting; translation should be short and mean 'Currently mounting this device'Accessing... All devices Click to access this device from other applications. Click to eject this disc. Click to safely remove this device. General It is currently <b>not safe</b> to remove this device: applications may be accessing it. Click the eject button to safely remove this device. It is currently <b>not safe</b> to remove this device: applications may be accessing other volumes on this device. Click the eject button on these other volumes to safely remove this device. It is currently safe to remove this device. Most Recent Device No Devices Available Non-removable devices only Open auto mounter kcmConfigure Removable Devices Open popup when new device is plugged in Removable devices only Removing is a less technical word for Unmounting; translation shoud be short and mean 'Currently unmounting this device'Removing... This device is currently accessible. This device is not currently accessible. Project-Id-Version: plasma_applet_devicenotifier
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2017-04-24 16:54+0100
Last-Translator: Volkan Gezer <volkangezer@gmail.com>
Language-Team: Turkish <kde-l10n-tr@kde.org>
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-POOTLE-MTIME: 1413130721.000000
 Bu aygıt için 1 eylem Bu aygıt için %1 eylem %1 boş Erişiliyor... Tüm aygıtlar Bu aygıta diğer uygulamalardan erişmek için tıklayın. Diski çıkarmak için tıklayın Bu aygıtı güvenli bir şekilde kaldırmak için tıklayın. Genel Bu aygıtın kaldırılması <b>güvenli değil</b>: uygulamalar diski kullanıyor olabilir. Bu aygıtı güvenli kaldırmak için çıkar düğmesine basın. Bu aygıtın kaldırılması <b>güvenli değil</b>: uygulamalar aygıttaki  diğer bölümleri kullanıyor olabilir. Bu aygıtı güvenli kaldırmak için diğer bölümlerdeki çıkarma düğmesine basın. Aygıtın kaldırılması güvenli. Son Takılan Aygıt Kullanılabilen Aygıt Yok Sadece çıkarılabilir olmayan aygıtlar Sadece Çıkarılabilir Olmayan Aygıtlar Yeni aygıt takıldığında pencere aç Sadece çıkarılabilir aygıtlar Kaldırılıyor... Bu aygıta şimdi erişilebilir. Bu aygıt şimdi erişilebilir değil. 