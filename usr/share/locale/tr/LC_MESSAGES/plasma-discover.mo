��    R      �  m   <      �     �            5     #   U  E   y  E   �  >        D  7   [     �     �     �     �     	     	     !	     (	     /	     B	     Z	     h	     }	     �	     �	     �	  !   �	  F   �	     
     9
     J
  <   \
     �
     �
  *   �
     �
     �
     �
  	   �
     �
                     >  
   \     g     �  
   �  F   �  :   �          "     )     <     C  8   R     �     �  	   �  
   �     �     �     �     �     �               6     G     P     o     u     �  
   �     �     �     �     �     �       $     �  <  (   �            E   *  G   p  P   �  P   	  I   Z     �  2   �     �               3     B     Q     c  
   l     w     �     �     �     �     �     �  "   �  &      ;   G     �     �     �  6   �     �     �  .        1  
   ?     J     N     U     e     r  +   z  %   �     �     �     �       V     =   o     �     �     �     �     �  @   �     9     =     U     \     d     q     y     �  '   �     �     �     �       %     	   .     8     H     _     s     �     �     �     �     �  (   �            /   ;       C                 @           L   Q   1   (       9   $   A   H                      :   P   	              R                 5       *             4          #   E   +      M      =   !   O          J   "       7   6          N         .   G   >   B   ?            2   )      ,   K   I   F   -                      '   <          %   &   D       8               0   3   
              
Also available in %1 %1 (%2) <b>%1</b> by %2 <em>%1 out of %2 people found this review useful</em> <em>Tell us about this review!</em> <em>Useful? <a href='true'><b>Yes</b></a>/<a href='false'>No</a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'><b>No</b></a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'>No</a></em> @infoFetching updates @infoIt is unknown when the last check for updates was @infoNo updates @infoNo updates are available @infoShould check for updates @infoThe system is up to date @infoUpdates @infoUpdating... Accept Addons Aleix Pol Gonzalez An application explorer Apply Changes Available backends:
 Available modes:
 Back Cancel Checking for updates... Compact Mode (auto/compact/full). Could not close the application, there are tasks that need to be done. Could not find category '%1' Couldn't open %1 Delete the origin Directly open the specified application by its package name. Discard Discover Display a list of entries with a category. Extensions... Help... Install Installed Jonathan Thomas Launch License: List all the available backends. List all the available modes. Loading... Local package file to install More... No Updates Open Discover in a said mode. Modes correspond to the toolbar buttons. Open with a program that can deal with the given mimetype. Rating: Remove Resources for '%1' Review Reviewing '%1' Running as <em>root</em> is discouraged and unnecessary. Search Search in '%1'... Search... Search: %1 Search: %1 + %2 Settings Short summary... Show reviews (%1)... Sorry, nothing found... Source: Specify the new source for %1 Still looking... Summary: Supports appstream: url scheme Tasks Tasks (%1%) Unable to find resource: %1 Update All Update Selected Update section nameUpdate (%1) Updates unknown reviewer updates not selected updates selected © 2010-2016 Plasma Development Team Project-Id-Version: extragear-sysadmin-kde4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:02+0100
PO-Revision-Date: 2017-10-04 12:36+0000
Last-Translator: Kaan <kaanozdincer@gmail.com>
Language-Team: Turkish <kde-l10n-tr@kde.org>
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 
Ayrıca %1 üzerinde de kullanılabilir %1 (%2) <b>%1</b>, %2 <em>%2 kişinin %1 tanesi bu gözden geçirmeyi faydalı bulmuş</em> <em>Bu gözden geçirme hakkındaki düşüncelerinizi paylaşın!</em> <em>Faydalı mı? <a href='true'><b>Evet</b></a>/<a href='false'>Hayır</a></em> <em>Faydalı mı? <a href='true'>Evet</a>/<a href='false'><b>Hayır</b></a></em> <em>Faydalı mı? <a href='true'>Evet</a>/<a href='false'>Hayır</a></em> Güncellemeler alınıyor Son güncellemenin denetlenme zamanı belli değil Güncelleme yok Güncelleştirme yok Güncelleme kontrol edilmeli Sistem güncel Güncellemeler Güncelleniyor... Kabul Et Eklentiler Aleix Pol Gonzalez Bir uygulama keşfedici Değişiklikleri Uygula Kullanılabilir arka uçlar:
 Kullanılabilir kipler:
 Geri İptal Güncellemeler kontrol ediliyor... Sıkışık Kip (otomatik/sıkı/tam). Uygulama kapatılamadı, yapılması gereken görevler var. '%1' kategorisi bulunamadı %1 açılamadı Kökeni sil Belirtilen uygulamayı doğrudan paketin adı ile aç. Vazgeç Keşfet Bir kategoriye sahip girdi listelerini göster Uzantılar... Yardım... Kur Kurulu Jonathan Thomas Çalıştır Lisans: Tüm kullanılabilir arka uçları listele. Tüm kullanılabilir kipleri listele. Yükleniyor... Kurulacak yerel paket dosyası Daha fazla... Güncelleme Yok Keşfet'i söylenilen bir kipte aç. Kipler araç çubuğu düğmelerini değiştirir. Belirtilen mime türü ile işlem yapabilen bir programla aç Beğeni: Kaldır '%1' için kaynaklar Gözden Geçir '%1' Değerlendirmesi <em>root</em> olarak çalışmak tavsiye edilmez ve gereksizdir. Ara '%1' içerisinde ara... Ara... Ara: %1 Ara: %1 + %2 Ayarlar Kısa özet... Yorumları göster (%1)... Üzgünüm, hiçbir şey bulunamadı... Kaynak: %1 için yeni kaynak belirtin Hala arıyor... Özet: appstream: url şeması destekleniyor Görevler Görevler (%%1) Kaynak bulunamadı: %1 Tümünü Güncelle Seçilenleri Güncelle Güncelle (%1) Güncellemeler bilinmeyen değerlendirici güncelleme seçilmedi güncelleme seçildi © 2010-2016 Plasma Geliştirme Takımı 