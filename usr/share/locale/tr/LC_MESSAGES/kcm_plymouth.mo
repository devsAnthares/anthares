��          �   %   �      @     A  !   Y      {     �      �     �     �  +        =     N     [  (   z     �  ,   �  7   �     !  7   :     r  ]   �  1   �  	   "     ,  "   ?  5   b  �  �     !  &   >  +   e  $   �     �  &   �  *   �  %     	   E     O     \  4   l     �  0   �  9   �     '	  8   A	      z	  u   �	  6   
     H
     P
  4   `
  /   �
                    
         	                                                                                                 Cannot start initramfs. Cannot start update-alternatives. Configure Plymouth Splash Screen Download New Splash Screens EMAIL OF TRANSLATORSYour emails Get New Boot Splash Screens... Initramfs failed to run. Initramfs returned with error condition %1. Install a theme. Marco Martin NAME OF TRANSLATORSYour names No theme specified in helper parameters. Plymouth theme installer Select a global splash screen for the system The theme to install, must be an existing archive file. Theme %1 does not exist. Theme corrupted: .plymouth file not found inside theme. Theme folder %1 does not exist. This module lets you configure the look of the whole workspace with some ready to go presets. Unable to authenticate/execute the action: %1, %2 Uninstall Uninstall a theme. update-alternatives failed to run. update-alternatives returned with error condition %1. Project-Id-Version: kcm_plymouth
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-28 03:05+0200
PO-Revision-Date: 2017-10-04 13:43+0000
Last-Translator: Kaan <kaanozdincer@gmail.com>
Language-Team: Turkish <kde-i18n-doc@kde.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Initramfs başlatılamıyor. update-alternatives başlatılamıyor. Plymouth Açılış Ekranını Yapılandır Yeni Açılış Ekranlarını İndir kaanozdincer@gmail.com Yeni Açılış Ekranlarını Getir... Initramfs çalışması başarısız oldu. Initramfs %1 hata durumu döndürdü. Tema kur. Marco Martin Kaan Özdinçer Yardımcı parametrelerinde bir tema belirtilmemiş. Plymouth tema yükleyicisi Sistem için genel bir açılış ekranı seçin Yüklenecek tema, varlona arşiv dosyasında olmalıdır. %1 teması mevcut değil. Tema bozuk: .plymouth dosyası tema içinde bulunamadı. %1 tema klasörü mevcut değil. Bu modül, bazı hazır ön ayarlarlarını, tüm çalışma alanının görünümünü yapılandırmanızı sağlar. Bu eylem doğrulanamadı/çalıştırılamadı: %1, %2 Kaldır Temayı kaldı. update-alternatives çalışması başarısız oldu. update-alternatives %1 hata durumu döndürdü. 