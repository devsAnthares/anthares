��            )         �     �  "   �     �  !   �  !        4  
   J     U     p     �  !   �     �  0   �  8        ?  !   ]          �     �     �     �                '  
   /  
   :  
   E  &   P     w     �  �  �     0  (   4     ]  /   y  1   �     �     �     �       !   ,  '   N  2   v  X   �  F   	      I	  :   j	  .   �	  *   �	  '   �	  '   '
     O
  B   k
     �
     �
     �
     �
     �
  5   �
     %     2                                                                                                 	                       
                 ms &Keyboard accelerators visibility: &Top arrow button type: Always Hide Keyboard Accelerators Always Show Keyboard Accelerators Anima&tions duration: Animations Bottom arrow button t&ype: Breeze Settings Center tabbar tabs Drag windows from all empty areas Drag windows from titlebar only Drag windows from titlebar, menubar and toolbars Draw a thin line to indicate focus in menus and menubars Draw focus indicator in lists Draw frame around dockable panels Draw frame around page titles Draw frame around side panels Draw slider tick marks Draw toolbar item separators Enable animations Enable extended resize handles Frames General No Buttons One Button Scrollbars Show Keyboard Accelerators When Needed Two Buttons W&indows' drag mode: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:42+0200
PO-Revision-Date: 2017-10-23 11:10+0000
Last-Translator: Kaan <kaanozdincer@gmail.com>
Language-Team: Turkish <kde-l10n-tr@kde.org>
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
  ms &Klavye hızlandırma görünürlüğü: &Yukarı ok düğme türü: Her Zaman Klavye Hızlandırıcılarını Gizle Her Zaman Klavye Hızlandırıcılarını Göster Animasyon &süreleri: Animasyonlar Aşağ&ı ok düğme türü: Esinti Ayarları Sekme çubuğu sekmelerini ortala Pencereleri tüm boş alanlardan taşı Pencereleri sadece başlık çubuklarından taşı Pencereleri başlık çubuklarından, menü çubuğundan ve araç çubuklarından taşı Menü ve menü çubuklarında odağı belirtmek için ince çizgi çiz Listelerde odak belirtecini çiz Yapıştırılabilir panellerin çevresinde çerçeve çiz Sayfa başlıkları çevresinde çerçeve çiz Kenar panelleri çevresinde çerçeve çiz Kaydırma onay işaretleyicilerini çiz Araç çubuğu öge ayraçlarını çiz Animasyonları etkinleştir Genişletilmiş yeniden boyutlandırma tutucularını etkinleştir Çerçeveler Genel Düğme Yok Tek Düğme Kaydırma çubukları Gerektiğinde Klavye Hızlandırıcılarını Göster İki Düğme &Pencerelerin sürükleme kipi: 