��    7      �  I   �      �  �  �  }  �  M    
   V     a     �  <   �     �     �  	          .   !  %   P     v     �     �     �     �     �     �     �     �  4        ;  9   M     �     �  �   �  !   �  t   �          *     G     T     Y     p  B   y  &   �  ?   �  '   #  )   K  7   u  .   �  5   �  +        >     [     {  ,   �     �     �  9   �  V     C   l  �  �  �  <  �    c        d!  #   p!     �!  B   �!     �!     "     $"     1"  2   I"  /   |"     �"     �"     �"     �"     �"     �"     #     "#     (#  A   =#     #  ?   �#     �#     �#  �   �#     �$     �$     `%      r%     �%     �%     �%     �%  A   �%  *   &  A   =&  "   &     �&  /   �&  '   �&  3   '  4   K'     �'     �'     �'  ,   �'     �'     (  2   "(  S   U(     �(               -       ,      6         1      /   '      (       3      .      #      )   
          +          *   %          5          4   2   	      7                                                       $                            0   !   &          "       <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body style=" font-family:'hlv'; font-size:9pt; font-weight:400; font-style:normal;">
<p style="-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><br /></p></body></html> <b>Security notice:</b>
                             According to a security audit by Taylor Hornby (Defuse Security),
                             the current implementation of Encfs is vulnerable or potentially vulnerable
                             to multiple types of attacks.
                             For example, an attacker with read/write access
                             to encrypted data might lower the decryption complexity
                             for subsequently encrypted data without this being noticed by a legitimate user,
                             or might use timing analysis to deduce information.
                             <br /><br />
                             This means that you should not synchronize
                             the encrypted data to a cloud storage service,
                             or use it in other circumstances where the attacker
                             can frequently access the encrypted data.
                             <br /><br />
                             See <a href='http://defuse.ca/audits/encfs.htm'>defuse.ca/audits/encfs.htm</a> for more information. <b>Security notice:</b>
                             CryFS encrypts your files, so you can safely store them anywhere.
                             It works well together with cloud services like Dropbox, iCloud, OneDrive and others.
                             <br /><br />
                             Unlike some other file-system overlay solutions,
                             it does not expose the directory structure,
                             the number of files nor the file sizes
                             through the encrypted data format.
                             <br /><br />
                             One important thing to note is that,
                             while CryFS is considered safe,
                             there is no independent security audit
                             which confirms this. Activities Can not create the mount point Can not open an unknown vault. Choose the encryption system you want to use for this vault: Choose the used cypher: Close the vault Configure Configure Vault... Configured backend can not be instantiated: %1 Configured backend does not exist: %1 Correct version found Create CryFS Device is already open Device is not open Dialog Do not show this notice again EncFS Encrypted data location Failed to create directories, check your permissions Failed to execute Failed to fetch the list of applications using this vault Forcefully close  General If you limit this vault only to certain activities, it will be shown in the applet only when you are in those activities. Furthermore, when you switch to an activity it should not be available in, it will automatically be closed. Limit to the selected activities: Mind that there is no way to recover a forgotten password. If you forget the password, your data is as good as gone. Mount point Mount point is not specified Mount point: Next Open with File Manager Previous The mount point directory is not empty, refusing to open the vault The specified backend is not available The vault configuration can only be changed while it is closed. The vault is unknown, can not close it. The vault is unknown, can not destroy it. This device is already registered. Can not recreate it. This directory already contains encrypted data Unable to close the vault, an application is using it Unable to close the vault, it is used by %1 Unable to detect the version Unable to perform the operation Unknown device Unknown error, unable to create the backend. Use the default cipher Vaul&t name: Wrong version installed. The required version is %1.%2.%3 You need to select empty directories for the encrypted storage and for the mount point formatting the message for a command, as in encfs: not found%1: %2 Project-Id-Version: plasmavault-kde
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-11 05:53+0100
PO-Revision-Date: 2017-10-04 14:27+0000
Last-Translator: Kaan <kaanozdincer@gmail.com>
Language-Team: Turkish <kde-i18n-doc@kde.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body style=" font-family:'hlv'; font-size:9pt; font-weight:400; font-style:normal;">
<p style="-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><br /></p></body></html> <b>Güvenlik uyarısı:</b>
                             Taylor Hornby tarafından yapılan bir güvenlik denetimine göre (Defuse Security),
                             Encfs'in mevcut uygulaması, birden çok saldırı türüne karşı savunmasız,
                             veya potansiyel olarak savunmasızdır.
                             Örneğin, okuma / yazma erişimi olan bir saldırgan
                             şifreli verilere şifre çözme karmaşıklığını düşürebilir
                             meşru bir kullanıcı tarafından fark edilmeden sonradan şifrelenmiş veriler için
                             veya bilgi çıkarmak için zamanlama analizini kullanabilir.
                             <br /><br />
                             Bu, senkronize edilmemeniz gerektiği anlamına gelir
                             şifrelenmiş verileri bir bulut depolama hizmetine,
                             veya başka durumlarda saldırganın kullanabileceği
                             sıklıkla şifreli verilere erişebilir. \ n                             <br /><br />
                             Daha fazla bilgi için <a href='http://defuse.ca/audits/encfs.htm'>defuse.ca/audits/encfs.htm</a> konusuna bakın. <b>Güvenlik uyarısı:</b>
                             CryFS, dosyalarınızı şifreler, böylece onları güvenle depolayabilirsiniz.
                             Dropbox, iCloud, OneDrive ve diğerleri gibi bulut hizmetlerinde iyi çalışır.
                             <br /><br />
                             Bazı diğer dosya sistemi yerleşim çözümlerinin aksine,
                              dizin yapısını göstermez,
                              dosya sayısı veya dosya boyutları
                              şifreli veri formatı üzerinden.
                             <br /><br />
                             Unutulmaması gereken önemli bir nokta da
                              CryFS güvenli sayılırken
                              bağımsız bir güvenlik denetimi yok
                              bunu doğrulamaktadır. Etkinlikler Bağlama noktası oluşturulamıyor Bilinmeyen vault açılamıyor. Bu vault için kullanmak istediğiniz şifreleme sistemini seçin: Kullanılan şifrelemeyi seç: Vault'u kapat Yapılandır Vault'u Yapılandır... Yapılandırımış arka uç temsil edilemiyor: %1 Yapılandırılmış arka uç mevcut değil: %1 Doğru sürüm bulundu Oluştur CryFS Aygıt zaten açık Aygıt açık değil Pencere Bu duyuruyu bir daha gösterme EncFS Şifreli veri konumu Dizinleri oluşturma başarısız oldu, izinlerinizi kontrol edin Çalıştırılamadı Vault kullanan uygulamaların listesi getirilirken hata oluştu Kapatmayı zorla  Genel Bu vault yalnızca belirli etkinliklerle sınırlarsanız, uygulama yalnızca bu etkinliklerdeyken gösterilir. Dahası, içinde olmaması gereken bir etkinliğe geçtiğinizde otomatik olarak kapatılacaktır. Seçili etkinlikleri sınırla: Unuttuğunuz bir parolayı kurtarmanın bir yolu olmadığını unutmayın. Parolayı unutursan, verileriniz gitmiş sayılır. Bağlama noktası Bağlama noktası belirtilmemiş Bağlama noktası: Sonraki Dosya Yöneticisi ile Aç Önceki Bağlama noktası dizini boş değil, vault'u açmak reddediliyor Belirtilen arka uç kullanılabilir değil Vault yapılandırması sadece kapalıyken değiştirilebilirdir. Vault bilinmiyor, kapatılamıyor. Vault bilinmiyor, silinemez. Aygıt zaten kayıtlı. Yeniden oluşturulamaz. Dizin hala şifrelenmiş veri içeriyor Vault kapatılamıyor, bir uygulama onu kullanıyor Vault kapatılamıyor, %1 tarafından kullanılıyor Sürüm algılanamıyor İşlem gerçekleştirilemedi Bilinmeyen aygıt Bilinmeyen hata, arka uç oluşturulamıyor. Varsayılan şifrelemeyi kullan Vaul&t adı: Yanlış sürüm kurulu. Gerekli sürüm, %1.%2.%3 Şifrelenmiş depolama ve bağlama noktası için boş dizinler seçmeniz gerekiyor %1: %2 