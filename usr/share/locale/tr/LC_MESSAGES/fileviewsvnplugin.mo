��          �   %   �      p     q  +   �  .   �  6   �  *     #   D  &   h  /   �  2   �  :   �  0   -  3   ^  ;   �  K   �  -     $   H  '   m     �     �     �     �     �  #        1     O     c     |  �  �     ;     C  "   b  !   �  $   �  !   �  %   �      	  $   5	  #   Z	  (   ~	  ,   �	  +   �	  i    
     j
     �
     �
     �
     �
     �
     �
     �
  $   �
          9     E     Q                                                    	   
                                                                    @action:buttonCommit @info:statusAdded files to SVN repository. @info:statusAdding files to SVN repository... @info:statusAdding of files to SVN repository failed. @info:statusCommit of SVN changes failed. @info:statusCommitted SVN changes. @info:statusCommitting SVN changes... @info:statusRemoved files from SVN repository. @info:statusRemoving files from SVN repository... @info:statusRemoving of files from SVN repository failed. @info:statusReverted files from SVN repository. @info:statusReverting files from SVN repository... @info:statusReverting of files from SVN repository failed. @info:statusSVN status update failed. Disabling Option "Show SVN Updates". @info:statusUpdate of SVN repository failed. @info:statusUpdated SVN repository. @info:statusUpdating SVN repository... @item:inmenuSVN Add @item:inmenuSVN Commit... @item:inmenuSVN Delete @item:inmenuSVN Revert @item:inmenuSVN Update @item:inmenuShow Local SVN Changes @item:inmenuShow SVN Updates @labelDescription: @title:windowSVN Commit Show updates Project-Id-Version: kdesdk-kde4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2017-06-04 11:38+0000
Last-Translator: Mete <metebilgin48@gmail.com>
Language-Team: Turkish (http://www.transifex.com/projects/p/kdesdk-k-tr/language/tr/)
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Gönder Dosyalar SVN deposuna eklendi. Dosyalar SVN deposuna ekleniyor... Dosyalar SVN deposuna eklenemedi. SVN değişiklikleri gönderilemedi. SVN değişiklikleri gönderildi. SVN değişiklikleri gönderiliyor... Dosyalar SVN deposundan silindi. Dosyalar SVN deposundan siliniyor... Dosyalar SVN deposundan silinemedi. Dosyalar SVN deposundan geriye alındı. Dosyalar SVN deposundan geriye alınıyor... Dosyalar SVN deposundan geriye alınamadı. SVN durum güncellemesi başarısız oldu. "SVN Güncellemelerini Göster" seçeneği pasifleştiriliyor. SVN deposu güncellenemedi. SVN deposu güncellendi. SVN deposu güncelleniyor... SVN Ekle SVN Gönder... SVN Sil SVN Geri Al SVN Güncelle Yerel SVN Değişikliklerini Göster SVN Güncellemelerini Göster Açıklama: SVN Gönder Güncellemeleri göster 