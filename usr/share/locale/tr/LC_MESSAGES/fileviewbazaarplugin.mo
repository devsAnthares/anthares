��    !      $  /   ,      �  .   �  1     9   J     �  -   �  &   �  )   �  .   #  &   R  )   y  .   �  &   �  )   �  2   #  5   V  =   �  #   �     �  !     '   /  "   W  0   z  '   �  *   �     �          7     R     j     �     �  &   �  �  �  !   �	  %   �	  $   �	  #   
  '   (
  $   P
  (   u
  ;   �
  8   �
  <     :   P  7   �  ;   �  #   �  '   #  &   K  &   r  #   �  '   �  -   �  .        B     a     }     �     �  
   �     �  *   �  +        7  '   H                                                              	                                
                                    !                             @info:statusAdded files to Bazaar repository. @info:statusAdding files to Bazaar repository... @info:statusAdding of files to Bazaar repository failed. @info:statusBazaar Log closed. @info:statusCommit of Bazaar changes failed. @info:statusCommitted Bazaar changes. @info:statusCommitting Bazaar changes... @info:statusPull of Bazaar repository failed. @info:statusPulled Bazaar repository. @info:statusPulling Bazaar repository... @info:statusPush of Bazaar repository failed. @info:statusPushed Bazaar repository. @info:statusPushing Bazaar repository... @info:statusRemoved files from Bazaar repository. @info:statusRemoving files from Bazaar repository... @info:statusRemoving of files from Bazaar repository failed. @info:statusReview Changes failed. @info:statusReviewed Changes. @info:statusReviewing Changes... @info:statusRunning Bazaar Log failed. @info:statusRunning Bazaar Log... @info:statusUpdate of Bazaar repository failed. @info:statusUpdated Bazaar repository. @info:statusUpdating Bazaar repository... @item:inmenuBazaar Add... @item:inmenuBazaar Commit... @item:inmenuBazaar Delete @item:inmenuBazaar Log @item:inmenuBazaar Pull @item:inmenuBazaar Push @item:inmenuBazaar Update @item:inmenuShow Local Bazaar Changes Project-Id-Version: kdesdk-kde4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2014-06-08 14:01+0000
Last-Translator: Volkan Gezer <volkangezer@gmail.com>
Language-Team: Turkish (http://www.transifex.com/projects/p/kdesdk-k-tr/language/tr/)
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Dosyalar Bazaar deposuna eklendi. Dosyalar Bazaar deposuna ekleniyor... Dosyalar Bazaar deposuna eklenemedi. Bazaar Günlük Kaydı kapatıldı. Bazaar değişiklikleri gönderilemedi. Bazaar değişiklikleri gönderildi. Bazaar değişiklikleri gönderiliyor... Bazaar deposuna uzak depodaki değişiklikler indirilemedi. Bazaar deposuna uzak depodaki değişiklikler indirildi. Bazaar deposuna uzak depodaki değişiklikler indiriliyor... Bazaar deposuna yerel depodaki değişiklikler eklenemedi. Bazaar deposuna yerel depodaki değişiklikler eklendi. Bazaar deposuna yerel depodaki değişiklikler ekleniyor... Dosyalar Bazaar deposundan silindi. Dosyalar Bazaar deposundan siliniyor... Dosyalar Bazaar deposundan silinemedi. Değişiklikler gözden geçirilemedi. Değişiklikler Gözden Geçirildi. Değişiklikler Gözden Geçiriliyor... Bazaar Günlük Kaydı Çalıştırılamadı. Bazaar Günlük Kaydı Çalıştırılıyor... Bazaar deposu güncellenemedi. Bazaar deposu güncellendi. Bazaar deposu güncelleniyor... Bazaar Ekle... Bazaar Gönder... Bazaar Sil Bazaar Günlük Kaydı Bazaar Deposundaki Değişiklikleri İndir Bazaar Deposuna Yerel Değişiklikleri Ekle Bazaar Güncelle Yerel Bazaar Değişikliklerini Göster 