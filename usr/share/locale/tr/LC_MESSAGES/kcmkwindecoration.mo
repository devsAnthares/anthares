��            )   �      �  !   �  "   �  '   �  ,     #   ;  &   _  !   �  &   �  '   �     �                 V   $  1   {     �  *   �     �        
     
   "     -     6     ;     D     T     [     a     g  �  p           (     0     >     P     W     h     n     �     �     �  
   �     �  ]   �  6   #     Z  @   m  +   �     �  
   �  	   �     	     	  	   	      	     5	     9	     @	     E	                                                                          
                                                    	           @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Borders @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large Application menu Border si&ze: Buttons Close Close by double clicking:
 To open the menu, keep the button pressed until it appears. Close windows by double clicking &the menu button Context help Drag buttons between here and the titlebar Drop here to remove button Get New Decorations... Keep above Keep below Maximize Menu Minimize On all desktops Search Shade Theme Titlebar Project-Id-Version: kcmkwindecoration
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-04-14 02:49+0200
PO-Revision-Date: 2017-04-13 14:20+0100
Last-Translator: Volkan Gezer <volkangezer@gmail.com>
Language-Team: Turkish <kde-l10n-tr@kde.org>
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 Kocaman Büyük Kenarlık Yok Yan Kenarlık Yok Normal Aşırı Büyük Minik İnanılmaz Büyük Çok Büyük Uygulama menüsü Kenarlık &boyutu: Düğmeler Kapat Çift tıklayarak kapatın:
 Menüyü açmak için görünene kadar düğmeye basılı tutun. Menü düğmesine çift &tıklayarak pencereleri kapat İçerik yardımı Düğmeleri burası ile başlık çubuğu arasında sürükleyin Düğmeyi kaldırmak için buraya bırakın Yeni Dekorasyonlar Al... Üstte tut Altta tut Büyüt Menü Küçült Tüm masaüstlerinde Ara Gölge Tema Başlık çubuğu 