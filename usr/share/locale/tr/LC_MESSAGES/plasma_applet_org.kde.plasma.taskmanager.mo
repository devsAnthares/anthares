��    O      �  k         �     �     �     �     �     �     �     �  .   �  U   (     ~     �      �     �  /   �  /        5     A     J  
   V     a     q  $   �     �     �     �     �     �     �  	   	     	  
   	     )	     <	     O	     g	  	   m	     w	  !   �	     �	     �	  	   �	      �	     �	     �	     
     $
     5
     :
     G
     M
     _
     w
  (   �
     �
  =   �
            "   5     X     s  J   {  1   �  )   �  (   "  '   K  "   s  4   �     �     �     �     �     �          "  U   8  N   �  9   �  J     �  b          '  
   .     9     A     R  	   [     e  '   l     �     �  ,   �     �  9   �  9   6  
   p  
   {     �     �     �     �  7   �       	     	   )     3     K     Q     l     {     �     �     �     �     �     �       "        +     E  
   _  .   j     �     �     �     �  
   �     �               (     <  *   Z     �     �     �     �     �     �     �     �  #     1   '  2   Y  ,   �  +   �  9   �          6  
   ?     J     ^     d     k     t     }     �  "   �     %   N         @   A       >   +   "       ,       F   /   B   O           9      ;      $                      G          3          K         ?                        2   L   *      !      H   =                   <              '               :       -            .   I   J   6   C           M       1          #   0      4       7       (   	   
           8               D   5   )   &   E    &All Desktops &Close &Fullscreen &Move &New Desktop &Pin &Shade 1 = number of desktop, 2 = desktop name&%1 %2 Activities a window is currently on (apart from the current one)Also available on %1 Add To Current Activity All Activities Allow this program to be grouped Alphabetically Always arrange tasks in columns of as many rows Always arrange tasks in rows of as many columns Arrangement Behavior By Activity By Desktop By Program Name Close Window or Group Cycle through tasks with mouse wheel Do Not Group Do Not Sort Filters Forget Recent Documents General Grouping and Sorting Grouping: Highlight windows Icon size: Keep &Above Others Keep &Below Others Keep launchers separate Large Ma&ximize Manually Mark applications that play audio Maximum columns: Maximum rows: Mi&nimize Minimize/Restore Window or Group More Actions Move &To Current Desktop Move To &Activity Move To &Desktop Mute New Instance On %1 On All Activities On The Current Activity On middle-click: Only group when the task manager is full Open groups in popups Open or bring to the front window of media player appRestore Pause playbackPause Play next trackNext Track Play previous trackPrevious Track Quit media player appQuit Re&size Remove launcher button for application shown while it is not runningUnpin Show all user Places%1 more Place %1 more Places Show only tasks from the current activity Show only tasks from the current desktop Show only tasks from the current screen Show only tasks that are minimized Show progress and status information in task buttons Show tooltips Small Sorting: Start New Instance Start playbackPlay Stop playbackStop The click actionNone Toggle action for showing a launcher button while the application is not running&Pin When clicking it would toggle grouping windows of a specific appGroup/Ungroup Which activities a window is currently onAvailable on %1 Which virtual desktop a window is currently onAvailable on all activities Project-Id-Version: plasma_applet_tasks
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2017-06-09 17:30+0100
Last-Translator: Volkan Gezer <volkangezer@gmail.com>
Language-Team: Turkish <kde-l10n-tr@kde.org>
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 &Tüm Masaüstleri &Kapat Tam &Ekran &Taşı &Yeni Masaüstü &Sabitle &Gölgele &%1 %2 Ayrıca %1 üzerinde de kullanılabilir Geçerli Etkinliğe Ekle Tüm Etkinlikler Bu programın gruplandırılmasına izin ver Alfabetik Olarak Sütunlardaki görevleri her zaman satırlar kadar ayarla Satırlardaki görevleri her zaman sütunlar kadar ayarla Düzenleme Davranış Etkinliğe Göre Masaüstüne Göre Uygulama Adına Göre Pencere veya Grubu Kapat Görevler üzerinden farenin tekerleği ile geçiş yap Gruplandırma Sıralama Filtreler Geçerli Belgeleri Unut Genel Gruplandırma ve Sıralama Gruplandırma: Pencereleri vurgula Simge boyutu: &Diğerlerinin Üzerinde Tut Diğerlerinin &Altında Tut Başlatıcıları ayrı tut Büyük &Büyüt Elle Ses çalan uygulamaları işaretle En fazla sütun sayısı: En fazla satır sayısı: K&üçült Pencere veya Grubu Küçült/Eski Haline Getir Daha Fazla Eylem &Geçerli Masaüstüne Taşı &Etkinliğe Taşı Masaüstüne T&aşı Sesi Kapat Yeni Süreç %1 Üzerinde Tüm Etkinliklerde Geçerli Etkinlikte Orta tuşa tıklandığında: Sadece görev çubuğu doluysa gruplandır Grupları pencerelerde aç Geri Yükle Duraklat Sonraki Parça Önceki Parça Çık &Yeniden Boyutlandır Sabitlemeyi kaldır %1 daha fazla Yer %1 daha fazla Yer Sadece geçerli etkinliğe ait görevleri göster Sadece geçerli masaüstündeki görevleri göster Sadece geçerli ekrandaki görevleri göster Sadece küçültülmüş görevleri göster Görev düğmelerinde ilerleme ve durum bilgisini göster İpuçlarını göster Küçük Sıralama: Yeni Örnek Başlat Oynat Durdur Hiçbiri &Sabitle Gruplandır/Grubu Çöz %1 üzerinde kullanılabilir Tüm etkinliklerde kullanılabilir 