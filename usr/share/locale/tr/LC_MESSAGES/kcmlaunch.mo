��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  �  K     �  (   �  �    ~  �  k   :
     �
     �
     �
  -   �
            )   2     \                                          
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2014-10-28 09:17+0000
Last-Translator: Necdet <necdetyucel@gmail.com>
Language-Team: Turkish <kde-l10n-tr@kde.org>
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 1.0
  sn &Başlangıç gösterici zaman aşımı: <H1>Görev Çubuğu Bildirimi</H1>
Başlattığınız bir uygulamanın yüklendiğini gösterecek, görev çubuğunda
dönen bir kum saati olarak görünen başlangıç bildirimi için ikinci bir yöntemi
etkinleştirebilirsiniz. Bazı uygulamaların bu
başlangıç bildiriminden haberleri olmayabilir. Bu durumda düğme
'Başlangıç göstergesi zaman aşımı' kısmında verilen
zaman kadar sonra kaybolacaktır <h1>Meşgul İmleç</h1>
Bir uygulama çalıştığı zaman KDE meşgul bir imleç gösterebilir.
Meşgul imleci etkinleştirmek için 'Meşgul imleci etkinleştir'
düğmesine tıklayın.
Bazı uygulamalar bu başlatma bildiriminden haberdar olmayabilirler
Bu durumda imleç 'Başlangıç göstergesi zaman aşımı' bölümünde
belirtilen zamandan sonra yanıp sönmeyi bırakır <h1>Geri Bildirimi Çalıştır</h1>Buradan uygulama çalıştırma geri bildirimlerini ayarlayabilirsiniz. Titreyen İmleç Hareketli İmleç &Meşgul İmleç &Görev çubuğu bildirimlerini etkinleştir  Meşgul İmleci yok Pasif Meşgul İmleci &Başlangıç gösterici zaman aşımı:  &Görev Çubuğu Bildirimi 