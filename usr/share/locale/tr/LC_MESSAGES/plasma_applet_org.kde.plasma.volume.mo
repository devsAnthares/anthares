��    &      L  5   |      P     Q  8   S     �     �     �     �     �     �  3   �  >        N     i     y     �  m   �     �          "     2     7     ?  *   O      z     �     �  "   �     �     �          3     :     H     X     e     �  ;   �     �  �  �     �     �     �     �     �  
   �     �     �     �     	     	  
   '	  	   2	     <	     B	     X	     o	     {	     �	  	   �	     �	  5   �	  3   �	     
     +
     ?
     Y
     l
      �
     �
     �
     �
     �
     �
     �
     �
     �
        #   %                                                              "          $                      
                                           &      !                	        % Accessibility data on volume sliderAdjust volume for %1 Applications Audio Muted Audio Volume Behavior Capture Devices Capture Streams Checkable switch for (un-)muting sound output.Mute Checkable switch to change the current default output.Default Decrease Microphone Volume Decrease Volume Devices General Heading for a list of ports of a device (for example built-in laptop speakers or a plug for headphones)Ports Increase Microphone Volume Increase Volume Maximum volume: Mute Mute %1 Mute Microphone No applications playing or recording audio No output or input devices found Playback Devices Playback Streams Port is unavailable (unavailable) Port is unplugged (unplugged) Raise maximum volume Show additional options for %1 Volume Volume at %1% Volume feedback Volume step: label of device items%1 (%2) label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2017-10-04 12:13+0000
Last-Translator: Kaan <kaanozdincer@gmail.com>
Language-Team: Turkish <kde-l10n-tr@kde.org>
Language: tr_TR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 % %1 için sesi ayarla Uygulamalar Ses Kapalı Ses Düzeyi Davranış Yakalama Aygıtları Yakalama Akışları Sessiz Öntanımlı Mikrofon Sesini Azalt Sesi Azalt Aygıtlar Genel Bağlantı Noktaları Mikrofon Sesini Artır Sesi Artır Maksimum seviye: Sessiz Sessiz %1 Mikrofonu Kapat Ses çalan veya ses kaydı yapan hiçbir uygulama yok Hiçbir çıkış ya da giriş aygıtı bulunamadı Oynatma Aygıtları Oynatma Akışları  (kullanılabilir değil)  (takılı değil) Sesi maksimuma yükselt %1 için ek seçenekleri göster Ses Seviyesi Ses %%1 Ses seviyesi geribildirimi Ses basamağı: %1 (%2) %1: %2 100% %%1 