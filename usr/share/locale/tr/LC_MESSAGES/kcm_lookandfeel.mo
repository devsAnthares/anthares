��          �      �       H     I     i  #   �      �     �     �     �  �        �  ]   �       p   *  :   �  �  �  1   u      �  '   �     �               ,  �   9     �  �   �  !   y  ~   �  Y                        
          	                                  Configure Look and Feel details Cursor Settings Changed Download New Look And Feel Packages EMAIL OF TRANSLATORSYour emails Get New Looks... Marco Martin NAME OF TRANSLATORSYour names Select an overall theme for your workspace (including plasma theme, color scheme, mouse cursor, window and desktop switcher, splash screen, lock screen etc.) Show Preview This module lets you configure the look of the whole workspace with some ready to go presets. Use Desktop Layout from theme Warning: your Plasma Desktop layout will be lost and reset to the default layout provided by the selected theme. You have to restart KDE for cursor changes to take effect. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-27 05:46+0100
PO-Revision-Date: 2017-06-09 14:23+0100
Last-Translator: Volkan Gezer <volkangezer@gmail.com>
Language-Team: Turkish <kde-l10n-tr@kde.org>
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 Görünüm ve Doku ayrıntılarını yapılandır İmleç Ayarları Değiştirildi Yeni Bak ve Hisset Paketlerini İndirin volkangezer@gmail.com Yeni Görünümler Al... Marco Martin Volkan Gezer Çalışma alanınız için genel bir tema seçin (Plasma teması, renk şeması, fare imleci, pencere ve masaüstü değiştirici, açılış ekranı, kilit ekranı vb.) Önizlemeyi Göster Bu modül tüm çalışma alanının görünümünü önceden hazırlanmış olanları kullanıp yapılandırmanıza izin verir. Masaüstü Düzeni temadan kullan Uyarı: Plasma Masaüstü düzeniniz kaybolacak ve seçilen tema tarafından sağlanan varsayılan düzene sıfırlanacaktır. İmleç değişikliklerinin etkili olabilmesi için KDE'yi yeniden başlatmanız gerekli. 