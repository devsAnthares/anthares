��    1      �  C   ,      8     9     =     B  .   Q  5   �     �     �     �     �  	   �     �               &  1   :     l     r          �  
   �     �  '   �     �     �     �            '        @     O     V     ^  5   g     �     �     �     �     �  $   �  A   �  �   @     &     -  C   >  '   �     �     �     �  �  �     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
          '     ?  L   R     �     �  )   �  	   �     �     �               #     *     F     U  X   e     �     �     �     �  8   �     /     C  
   J     U     g     ~     �     �  
   �  !   �     �     �     �                         #   /       -                      $                +         (   1          .   !                        %   0      &       *       	         ,                               
         '                    )            "       %1% Back Battery at %1% Button to change keyboard layoutSwitch layout Button to show/hide virtual keyboardVirtual Keyboard Cancel Caps Lock is on Close Close Search Configure Configure Search Plugins Desktop Session: %1 Different User Keyboard Layout: %1 Logging out in 1 second Logging out in %1 seconds Login Login Failed Login as different user Logout Next track No media playing Nobody logged in on that sessionUnused OK Password Play or Pause media Previous track Reboot Reboot in 1 second Reboot in %1 seconds Recent Queries Remove Restart Shutdown Shutting down in 1 second Shutting down in %1 seconds Start New Session Suspend Switch Switch Session Switch User Textfield placeholder textSearch... Textfield placeholder text, query specific KRunnerSearch '%1'... This is the first text the user sees while starting in the splash screen, should be translated as something short, is a form that can be seen on a product. Plasma is the project name so shouldn't be translated.Plasma made by KDE Unlock Unlocking failed User logged in on console (X display number)on TTY %1 (Display %2) User logged in on console numberTTY %1 Username Username (location)%1 (%2) in category recent queries Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-09 03:21+0100
PO-Revision-Date: 2017-10-23 11:19+0000
Last-Translator: İşbaran <isbaran@gmail.com>
Language-Team: Turkish <kde-l10n-tr@kde.org>
Language: tr_TR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 %%1 Geri Pil %1 Yerleşim değiştir Sanal Klavye İptal Caps Lock açık Kapat Aramayı Kapat Yapılandır Arama Eklentisini Yapılandır Masaüstü Oturumu: %1 Farklı Bir Kullanıcı Klavye Düzeni: %1 1 saniye içinde oturum kapatılıyor %1 saniye içinde oturum kapatılıyor Giriş Giriş Başarısız Farklı bir kullanıcı olarak giriş yap Çıkış Sonraki parça Çalan ortam yok Kullanılmayan Tamam Parola Ortamı Çal ya da Duraklat Önceki parça Yeniden Başlat 1 saniye içerisinde yeniden başlatılacak %1 saniye içerisinde yeniden başlatılacak Son Sorgular Kaldır Yeniden Başlat Bilgisayarı Kapat 1 saniye içinde kapanıyor %1 saniye içinde kapanıyor Yeni Oturum Başlat Beklet Değiştir Oturum Değiştir Kullanıcı Değiştir Ara... Ara '%1'... KDE tarafından yapılan Plasma Kilidi Aç Kilidi açma işlemi başarısız TTY %1 (Ekran %2) TTY %1 Kullanıcı adı %1 (%2) kategorideki son sorgular 