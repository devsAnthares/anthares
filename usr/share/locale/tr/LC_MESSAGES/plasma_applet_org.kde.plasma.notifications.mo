��    $      <  5   \      0      1     R     ^  -   }  #   �  #   �  ?   �  1   3     e     y  H   ~  L   �       V     N   s     �  
   �     �     �     �       2     
   P     [  )   {  8   �  #   �  .     -   1  D   _  6   �  0   �  A     =   N  9   �  �  �     �
     �
  )   �
  1   �
  
   �
  
             *     ?     T     \     p     �     �     �     �     �     �     �     �     �     �       !   "  )   D     n  )   }  8   �     �     �                     $     :                                                
       "                                       $                                     #      	           !                                %1 notification %1 notifications %1 of %2 %3 %1 running job %1 running jobs &Configure Event Notifications and Actions... 10 seconds ago, keep short10 s ago 30 seconds ago, keep short30 s ago A button tooltip; expands the item to show detailsShow Details A button tooltip; hides item detailsHide Details Clear Notifications Copy Either just 1 dir or m of n dirs are being processed1 dir %2 of %1 dirs Either just 1 file or m of n files are being processed1 file %2 of %1 files History How much many bytes (or whether unit in the locale has been copied over total%1 of %2 Indicator that there are more urls in the notification than previews shown+%1 Information Job Failed Job Finished No new notifications. No notifications or jobs Open... Placeholder is job name, eg. 'Copying'%1 (Paused) Select All Show a history of notifications Show application and system notifications Speed and estimated time to completion%1 (%2 remaining) Track file transfers and other jobs Use custom position for the notification popup minutes ago, keep short%1 min ago %1 min ago notification was added n days ago, keep short%1 day ago %1 days ago notification was added yesterday, keep shortYesterday notification was just added, keep shortJust now placeholder is row description, such as Source or Destination%1: the job, which can be anything, failed to complete%1: Failed the job, which can be anything, has finished%1: Finished Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:07+0100
PO-Revision-Date: 2017-10-04 12:06+0000
Last-Translator: Kaan <kaanozdincer@gmail.com>
Language-Team: Turkish <kde-l10n-tr@kde.org>
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-POOTLE-MTIME: 1413102238.000000
 %1 bildirim %1 bildirim %1/%2 %3 %1 çalışan görev %1 çalışan görev &Olay Bildirimlerini ve Eylemleri Yapılandır... 10 s önce 30 s önce Ayrıntıları Göster Ayrıntıları Gizle Bildirimleri Temizle Kopyala 1 dizin %2/%1 dizin 1 dosya %2/%1 dosya Geçmiş %1/%2 +%1 Bilgi Görev başarısız oldu İş Tamamlandı Yeni bildirim yok. Hiç bildirim veya görev yok Aç... %1 (Duraklatıldı) Tümünü Seç Bildirimlerin geçmişini göster Uygulama ve sistem bildirimlerini göster %1 (%2 kaldı) Dosya aktarımlarını ve görevleri izle Bildirim açılan penceresi için özel bir konum kullan %1 dk önce %1 dk önce %1 gün önce %1 gün önce Dün Şimdi %1: %1: Başarısız oldu %1: [Tamamlandı] 