��          �      �       0     1  �  H     �       &         ?     `     n     v     �     �  (   �  �  �     �  �  �     \     m  2   }  '   �     �     �     �            (   1         
                 	                                (c) 2002-2006 KDE Team <h1>System Notifications</h1>Plasma allows for a great deal of control over how you will be notified when certain events occur. There are several choices as to how you are notified:<ul><li>As the application was originally designed.</li><li>With a beep or other noise.</li><li>Via a popup dialog box with additional information.</li><li>By recording the event in a logfile without any additional visual or audible alert.</li></ul> Carsten Pfeiffer Charles Samuels Disable sounds for all of these events EMAIL OF TRANSLATORSYour emails Event source: KNotify NAME OF TRANSLATORSYour names Olivier Goffart Original implementation System Notification Control Panel Module Project-Id-Version: kcmnotify
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-09 07:18+0200
PO-Revision-Date: 2017-10-04 12:58+0000
Last-Translator: Kaan <kaanozdincer@gmail.com>
Language-Team: Turkish <kde-l10n-tr@kde.org>
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 (c) 2002-2006 KDE Team <h1>Sistem Bildirimleri</h1> Plasma, belirli olaylar gerçekleştiğinde size nasıl bildirimde bulunulacağını büyük ölçüde kontrol etmenize imkan tanır. <ul><li>Uygulamanın orijinal tasarımına uygun şekilde.</li><li>Bip sesiyle ya da başka bir sesle.</li><li>Ek bilgilerin yer aldığı bir iletişim kutusuyla.</li><li>Olayın, başka bir görsel ya da işitsel alarm olmaksızın bir kayıt dosyasına kaydedilmesiyle.</li></ul> Carsten Pfeiffer Charles Samuels Tüm bu olaylar için sesleri devre dışı bırak adil@kde.org.tr, kaanozdincer@gmail.com Olay kaynağı: KNotify Adil Yıldız, Kaan Özdinçer Olivier Goffart Orjinal yürütme Sistem Bildirimi Kontrol Paneli Modülü 