��          �      <      �     �     �     �     �     �  '   �  >        L     f     �     �     �  )   �  7   �  '        B     T  �  s          '     -     4     D     P  	   `     j     �     �     �     �  *   �               !  "   ?                                       
                      	                                        Current user General Layout Lock Screen New Session Nobody logged in on that sessionUnused Show a dialog with options to logout/shutdown/restartLeave... Show both avatar and name Show full name (if available) Show login username Show only avatar Show only name Show technical information about sessions User logged in on console (X display number)on %1 (%2) User logged in on console numberTTY %1 User name display You are logged in as <b>%1</b> Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-11 03:18+0100
PO-Revision-Date: 2017-04-24 17:15+0100
Last-Translator: Volkan Gezer <volkangezer@gmail.com>
Language-Team: Turkish <kde-l10n-tr@kde.org>
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 Geçerli kullanıcı Genel Düzen Ekranı Kilitle Yeni Oturum Kullanılmıyor Ayrıl... İsim ve resmi göster Tam adı göster (mevcutsa) Oturum adını göster Sadece resmi göster Sadece adı göster Oturumlar hakkında teknik bilgiyi göster %1 üzerinde (%2) TTY %1 Kullanıcı adı görünümü <b>%1</b> olarak oturum açtınız 