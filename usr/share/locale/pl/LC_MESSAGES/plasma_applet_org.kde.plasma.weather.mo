��    O      �  k         �     �  ^   �  H     
   f     q     �     �     �     �  '   �     �  "   �          4     K  
   T     _     x     �  	   �     �     �     �     �     �     �  "   �     	      	  	   8	     B	  !   I	     k	  !   �	  ?   �	     �	     �	     �	     
     
     (
     <
  ;   H
  &   �
      �
  %   �
     �
          &     ?  >   X     �     �  '   �  +   �  !   !     C     c     t     �     �     �     �     �     �     �               +     >     P     b     s     �     �     �     �  3   �  �                                2     @     C     O     _     v     �     �     �  	   �     �     �     �     �     �  
             /     3     G     W  )   [     �     �     �     �      �     �  *   �     �        	             (     6  	   F     P     d     m     �     �     �     �     �     �     �     �     �     �     
          &     9  
   >  
   I     T     W     ^     g     p     w     z     �     �     �     �     �  
   �  
   �     �  	   �     �     �     /       ?       $         K   M   !   )      @                                 2   E       '               1          6              O   <      H   J      8       7      >       =   0   *          .      ,   :      N   D   G   C   3   	   #   +   4                F          %   A                   5       &   ;              9   
           -                      (   "   L      B   I             min %1 is the weather condition, %2 is the temperature,  both come from the weather provider%1 %2 A weather station location and the weather service it comes from%1 (%2) Appearance Beaufort scale bft Celsius °C Degree, unit symbol° Details Fahrenheit °F Forecast period timeframe1 Day %1 Days Hectopascals hPa High & Low temperatureH: %1 L: %2 High temperatureHigh: %1 Inches of Mercury inHg Kelvin K Kilometers Kilometers per Hour km/h Kilopascals kPa Knots kt Location: Low temperatureLow: %1 Meters per Second m/s Miles Miles per Hour mph Millibars mbar N/A No weather stations found for '%1' Notices Percent, measure unit% Pressure: Search Select weather services providers Short for no data available- Show temperature in compact mode: Shown when you have not set a weather providerPlease Configure Temperature: Units Update every: Visibility: Weather Station Wind conditionCalm Wind speed: certain weather condition (probability percentage)%1 (%2%) content of water in airHumidity: %1%2 distance, unitVisibility: %1 %2 ground temperature, unitDewpoint: %1 humidex, unitHumidex: %1 pressure tendencyfalling pressure tendencyrising pressure tendencysteady pressure tendency, rising/falling/steadyPressure Tendency: %1 pressure, unitPressure: %1 %2 temperature, unit%1%2 visibility from distanceVisibility: %1 weather services provider name (id)%1 (%2) weather warningsWarnings Issued: weather watchesWatches Issued: wind directionE wind directionENE wind directionESE wind directionN wind directionNE wind directionNNE wind directionNNW wind directionNW wind directionS wind directionSE wind directionSSE wind directionSSW wind directionSW wind directionVR wind directionW wind directionWNW wind directionWSW wind direction, speed%1 %2 %3 wind speedCalm windchill, unitWindchill: %1 winds exceeding wind speed brieflyWind Gust: %1 %2 Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-28 05:50+0100
PO-Revision-Date: 2018-01-28 06:49+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Lokalize 2.0
  min %1 %2 %1 (%2) Wygląd Skali Beauforta bft Celsjusza °C ° Szczegóły Fahrenheita °F 1 dzień %1 dni %1 dni Hektopaskali hPa N: %1 W: %2 Wysokie: %1 Barometr inHg Kelvina K Kilometrów Kilometrów na Godzinę km/h Kilopaskali kPa Węzłów kt Miejscowość: Niskie: %1 Metrów na Sekundę m/s Mil Mil na Godzinę mph Milibarów mbar N/D Nie odnaleziono stacji pogodowej dla '%1' Uwagi % Ciśnienie: Znajdź Wybierz dostawców usług pogody - Pokaż termperaturę w trybie kompaktowym: Wymaga ustawień Temperatura: Jednostki Uaktualniaj co:  Widoczność: Stacja pogodowa Spokojnie Prędkość wiatru: %1 (%2%) Wilgotność: %1%2 Widoczność: %1 %2 Punkt rosy: %1 Wilgotność: %1 spada rośnie stałe Tendencja ciśnienia: %1 Ciśnienie: %1 %2 %1%2 Widoczność: %1 %1 (%2) Wydane ostrzeżenia Wydane obserwacje: Wsch WschPnWsch WschPdWsch Pn PnWsch PnPnWsch PnPnZach PnZach Pd PdWsch PdPdWsch PdPdZach PdZach nie można określić Zach ZachPnZach ZachPdZach %1 %2 %3 Spokojnie Temperatura odczuwalna: %1 Porywy wiatru: %1 %2 