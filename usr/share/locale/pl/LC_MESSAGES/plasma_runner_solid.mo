��          �      L      �  m   �     /  "   <  F   _  F   �  F   �  J   4  N     R   �     !  %   2  $   X  #   }  $   �  %   �  &   �  �        �  �  �     �     �  2   �  T   �  ]   S  T   �  R   	  j   Y	  j   �	     /
     D
     P
     W
     `
     i
     r
     z
     �
                                
                          	                                          Close the encrypted container; partitions inside will disappear as they had been unpluggedLock the container Eject medium Finds devices whose name match :q: Lists all devices and allows them to be mounted, unmounted or ejected. Lists all devices which can be ejected, and allows them to be ejected. Lists all devices which can be mounted, and allows them to be mounted. Lists all devices which can be unmounted, and allows them to be unmounted. Lists all encrypted devices which can be locked, and allows them to be locked. Lists all encrypted devices which can be unlocked, and allows them to be unlocked. Mount the device Note this is a KRunner keyworddevice Note this is a KRunner keywordeject Note this is a KRunner keywordlock Note this is a KRunner keywordmount Note this is a KRunner keywordunlock Note this is a KRunner keywordunmount Unlock the encrypted container; will ask for a password; partitions inside will appear as they had been plugged inUnlock the container Unmount the device Project-Id-Version: plasma_runner_solid
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-03-19 07:16+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Zablokuj kontener Wysuń nośnik Znajduje urządzenia, których nazwa pasuje do :q: Pokazuje wszystkie urządzenia i pozwala na ich podpięcie, odpięcie i wysunięcie. Pokazuje wszystkie urządzenia, które można wysunąć i umożliwia wysunięcie urządzenia. Pokazuje wszystkie urządzenia, które można podpiąć i umożliwia ich podpięcie. Pokazuje wszystkie urządzenia, które można odpiąć i umożliwia ich odpięcie. Pokazuje wszystkie zaszyfrowane urządzenia, które mogą być zablokowane, i umożliwia ich zablokowanie. Pokazuje wszystkie zaszyfrowane urządzenia, które mogą być odblokowane, i umożliwia ich odblokowanie. Podepnij urządzenie urządzenie wysuń zablokuj podepnij odblokuj odepnij Odblokuj kontener Odepnij urządzenie 