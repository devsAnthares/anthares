��          �   %   �      P     Q     b     w     �     �     �  
   �     �     �     �     �     �  /        3     Q     p     v     �     �     �     �     �     �     �       �  %       $   *     O  )   d     �  $   �     �  
   �     �  	   �     �     �  7        >     W  	   t     ~  	   �     �     �      �  "   �  !   �          4                            
                                                                                             	    %1 file %1 files %1 folder %1 folders %1 of %2 processed %1 of %2 processed at %3/s %1 processed %1 processed at %2/s Appearance Behavior Cancel Clear Configure... Finished Jobs List of running file transfers/jobs (kuiserver) Move them to a different list Move them to a different list. Pause Remove them Remove them. Resume Show all jobs in a list Show all jobs in a list. Show all jobs in a tree Show all jobs in a tree. Show separate windows Show separate windows. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2014-06-23 20:18+0200
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Lokalize 1.5
 %1 plik %1 pliki %1 plików %1 katalog %1 katalogi %1 katalogów %1 z %2 przetworzone %1 z %2 przetworzone z prędkością %3/s %1 przetworzony %1 przetworzony z prędkością %2/s Wygląd Zachowanie Anuluj Wyczyść Ustawienia... Ukończone prace Spis uruchomionych przesyłań plików/prac (kuiserver) Przesuń je na inny spis Przemieść je na inny spis. Wstrzymaj Usuń je Usuń je. Wznów Pokaż wszystkie prace w spisie Pokaż wszystkie prace w spisie. Pokaż wszystkie zadania w drzewie Pokaż wszystkie prace w drzewie. Pokaż oddzielne okna Pokaż oddzielne okna. 