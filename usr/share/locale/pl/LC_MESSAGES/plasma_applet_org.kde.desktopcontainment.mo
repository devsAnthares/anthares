��    `        �         (     )     1     B     Q     W     ^     j     {     �     �  /   �  -   �     �     �  
   		  
   	     	     ,	     1	     8	     @	     R	     _	     d	  
   l	     w	     �	     �	     �	  	   �	     �	  	   �	     �	     �	     �	     
     
  	   #
     -
     4
     H
  	   M
     W
     ]
     c
     h
     m
  	   v
     �
     �
     �
     �
     �
     �
     �
     �
  <   �
               /     6     =     X     ^     e     j  
   ~     �     �     �     �     �  )   �               5     :     @     M     U     ]     f  
   x     �     �  	   �     �     �     �     �     �     �     �  E   �  *   A  �  l     U     \     m     �     �     �     �     �     �     �  
   �     �  	                        -     =     D  	   K     U     h     w  	   |  	   �     �     �     �     �     �     �     �                     7     I     [     l     u     �     �     �     �     �     �     �     �     �     �     �  !   �          #     (     +  ^   <     �     �     �     �  
   �  	   �     �  	   �     �          )     9     J     a     x  1   �     �  &   �     �                    '     9     H     W     f     l     t     �     �  !   �     �     �     �     �  o        �     J       -      M       %   Q           ^              U       L   S   $      .       H       X          Y   8   *         <   &          '       ]   N       3                     `          #   V             /   T   G   9   @   >   =   6   2   O   C   F   ?      0      
      A   E       	      P              _                D          Z               1   R   ,   +                \           I   )   B   K      :   !   W   4          ;               (      5   "       7       [           &Delete &Empty Trash Bin &Move to Trash &Open &Paste &Properties &Refresh Desktop &Refresh View &Reload &Rename @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Align Appearance: Arrange In Arrange in Arrangement: Back Cancel Columns Configure Desktop Custom title Date Default Descending Description Deselect All Desktop Layout Enter custom title here Features: File name pattern: File type File types: Filter Folder preview popups Folders First Folders first Full path Got it Hide Files Matching Huge Icon Size Icons Large Left List Location Location: Lock in place Locked Medium More Preview Options... Name None OK Panel button: Press and hold widgets to move them and reveal their handles Preview Plugins Preview thumbnails Remove Resize Restore from trashRestore Right Rotate Rows Search file type... Select All Select Folder Selection markers Show All Files Show Files Matching Show a place: Show files linked to the current activity Show the Desktop folder Show the desktop toolbox Size Small Small Medium Sort By Sort by Sorting: Specify a folder: Text lines Tiny Title: Tool tips Tweaks Type Type a path or a URL here Unsorted Use a custom icon Widget Handling Widgets unlocked You can press and hold widgets to move them and reveal their handles. whether to use icon or list viewView mode Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:11+0100
PO-Revision-Date: 2017-11-25 07:21+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Lokalize 2.0
 &Usuń &Opróżnij kosz Przenieś do &kosza &Otwórz &Wklej &Właściwości &Odśwież pulpit &Odśwież widok Wczytaj &ponownie &Zmień nazwę Wybierz... Wyczyść ikonę Wyrównaj Wygląd Rozmieść w Rozmieść w Rozmieszczenie: Wstecz Anuluj Kolumnach Ustawienia pulpitu Własny tytuł Daty Domyślne Malejąco Opis Odznacz wszystko Obszar pulpitu Tutaj podaj własny tytuł Możliwości Wzorzec nazwy pliku: Rodzaje pliku Rodzaje plików: Filtr Okna wysuwne podglądu katalogu Najpierw katalogi Najpierw katalogi Pełna ścieżka Rozumiem Ukryj pasujące pliki Wielki Rozmiar ikon Ikony Duże Do lewej Lista Położenie Położenie: Zablokuj na miejscu Zablokowane Średni Więcej możliwości podglądu... Nazwy Brak OK Przycisk panelu: Naciśnięcie i przytrzymanie odkrywa uchwyty i umożliwia przesunięcie elementów interfejsu Wtyczki podglądów Miniatury podglądu Usuń Zmień rozmiar Przywróć Do prawej Obróć Wierszach Wyszukaj rodzaj pliku... Zaznacz wszystko Wybierz katalog Znaczniki wyboru Pokaż wszystkie pliki Pokaż pasujące pliki Pokaż miejsce: Pokaż pliki dowiązane do bieżącego działania Pokaż katalog pulpitu Pokaż skrzynkę narzędziową pulpitu Rozmiaru Małe Średni mały Uszereguj według Uszereguj według Uszereguj wg.: Podaj katalog: Wiersze tekstu Mały Tytuł: Podpowiedzi Dodatki Rodzaju Wpisz tu ścieżkę lub adres URL Nie szereguj Użyj własnej ikony Obsługa elementów interfejsu Elementy interfejsu odblokowane Po naciśnięciu i przytrzymaniu elementu interfejsu można go przesunąć. Odkrywają się też jego uchwyty. Tryb widoku 