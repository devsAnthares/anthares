��            )   �      �  !   �  "   �  '   �  ,     #   ;  &   _  !   �  &   �  '   �     �                 V   $  1   {     �  *   �     �        
     
   "     -     6     ;     D     T     [     a     g  �  p     l     s     y     �     �     �     �     �     �     �     �  	   �       �   	  =   �     �  7   �  $   	     9	     Q	     d	     v	     �	     �	     �	     �	     �	     �	     �	                                                                          
                                                    	           @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Borders @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large Application menu Border si&ze: Buttons Close Close by double clicking:
 To open the menu, keep the button pressed until it appears. Close windows by double clicking &the menu button Context help Drag buttons between here and the titlebar Drop here to remove button Get New Decorations... Keep above Keep below Maximize Menu Minimize On all desktops Search Shade Theme Titlebar Project-Id-Version: kcmkwindecoration
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-04-14 02:49+0200
PO-Revision-Date: 2017-12-09 07:13+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
>
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Wielki Duży Bez obramowań Bez obramowania z boku Zwykły Olbrzymi Mały Bardzo wielki Bardzo duży Menu programów Ro&zmiar obramowania: Przyciski Zamknij Zamykanie po dwukrotnym kliknięciu:
 Aby otworzyć menu, przytrzymaj przycisk naciśniętym na menu do chwili jego pokazania się. Zamykaj okna po dwukro&tnym kliknięciu na ich przycisku menu Pomoc podręczna Wybierz żądany przycisk i upuść go na pasku tytułu Upuść tutaj, aby usunąć przycisk Pobierz nowy wygląd... Zawsze na wierzchu Zawsze na spodzie Maksymalizuj Menu Minimalizuj Na wszystkich pulpitach Znajdź Zwiń Wygląd Pasek tytułu 