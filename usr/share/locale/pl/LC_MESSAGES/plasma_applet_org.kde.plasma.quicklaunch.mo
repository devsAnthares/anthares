��          �      <      �     �  <   �  
   �     	          &     3     ?  
   G     R     c     q     }     �     �  
   �     �  �  �     �  [   �     )     1      @     a     v     �     �     �     �     �     �     �  (        .     <                                                  
   	                                                Add Launcher... Add launchers by Drag and Drop or by using the context menu. Appearance Arrangement Edit Launcher... Enable popup Enter title General Hide icons Maximum columns: Maximum rows: Quicklaunch Remove Launcher Show hidden icons Show launcher names Show title Title Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-02-26 04:09+0100
PO-Revision-Date: 2016-01-23 06:52+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Lokalize 2.0
 Dodaj program uruchamiający... Dodaj programy uruchamiające za pomocą "przeciągnij i upuść" lub z menu kontekstowego. Wygląd Rozmieszczenie Edytuj program uruchamiający... Włącz okno wysuwne Podaj tytuł Ogólne Ukryj ikony Minimalnie kolumn: Maksymalnie wierszy: Szybkie uruchamianie Usuń program uruchamiający Pokaż ukryte ikony Pokaż nazwy programów uruchamiających Pokaż tytuł Tytuł 