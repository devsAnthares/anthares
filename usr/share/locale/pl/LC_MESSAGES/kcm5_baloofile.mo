��          �            h     i     �  #   �      �      �     �  J        [  &   y     �  T   �       *   $     O  �  ]  #   F     j  )   �     �     �     �  b   
     m  -   �     �  ^   �     /  -   ?     m                            	                                             
       Also index file content Configure File Search Copyright 2007-2010 Sebastian Trüg Do not search in these locations EMAIL OF TRANSLATORSYour emails Enable File Search File Search helps you quickly locate all your files based on their content Folder %1 is already excluded Folder's parent %1 is already excluded NAME OF TRANSLATORSYour names Not allowed to exclude root folder, please disable File Search if you do not want it Sebastian Trüg Select the folder which should be excluded Vishesh Handa Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2017-11-25 07:14+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Lokalize 2.0
 Indeksuj także zawartość plików Ustawienia wyszukiwania plików Prawa autorskie 2007-2010 Sebastian Trüg Nie szukaj w tych miejscach lukasz.wojnilowicz@gmail.com Włącz wyszukiwanie plików Wyszukiwanie plików umożliwia szybkie ustalenie położenia plików na podstawie ich zawartości Katalog %1 już jest wykluczony Katalog nadrzędny do %1 już jest wykluczony Łukasz Wojniłowicz Nie można wykluczyć głównego katalogu, wyłącz wyszukiwanie plików, jeśli go nie chcesz Sebastian Trüg Wybierz katalog, który ma zostać wykluczony Vishesh Handa 