��    
      l      �       �   
   �      �        .   0     _     t     �  (   �  8   �  �       �  %        1  A   J      �     �  
   �     �     �        
                             	        Disk Quota No quota restrictions found. Please install 'quota' Quota tool not found.

Please install 'quota'. Running quota failed e.g.: 12 GiB of 20 GiB%1 of %2 e.g.: 8 GiB free%1 free example: Quota: 83% usedQuota: %1% used usage of quota, e.g.: '/home/bla: 38% used'%1: %2% used Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2016-11-20 09:35+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Lokalize 2.0
 Przydział dyskowy Nie znaleziono wartości przydziału. Wgraj narzędzie 'quota' Nie znaleziono narzędzia przydziału.

Wgraj narzędzie 'quota'. Nie udało się uruchomić quota %1 z %2 %1 wolnych Przydział: %1% wykorzystano %1: %2% wykorzystanych 