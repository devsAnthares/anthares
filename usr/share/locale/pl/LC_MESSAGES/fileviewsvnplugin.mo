��          �   %   �      p     q  +   �  .   �  6   �  *     #   D  &   h  /   �  2   �  :   �  0   -  3   ^  ;   �  K   �  -     $   H  '   m     �     �     �     �     �  #        1     O     c     |  �  �     r  !   y  (   �  /   �     �     	     )	  #   A	  &   e	  -   �	  "   �	  )   �	  0   
  O   8
  '   �
     �
      �
  	   �
     �
  	             "     4     N     g     m     |                                                    	   
                                                                    @action:buttonCommit @info:statusAdded files to SVN repository. @info:statusAdding files to SVN repository... @info:statusAdding of files to SVN repository failed. @info:statusCommit of SVN changes failed. @info:statusCommitted SVN changes. @info:statusCommitting SVN changes... @info:statusRemoved files from SVN repository. @info:statusRemoving files from SVN repository... @info:statusRemoving of files from SVN repository failed. @info:statusReverted files from SVN repository. @info:statusReverting files from SVN repository... @info:statusReverting of files from SVN repository failed. @info:statusSVN status update failed. Disabling Option "Show SVN Updates". @info:statusUpdate of SVN repository failed. @info:statusUpdated SVN repository. @info:statusUpdating SVN repository... @item:inmenuSVN Add @item:inmenuSVN Commit... @item:inmenuSVN Delete @item:inmenuSVN Revert @item:inmenuSVN Update @item:inmenuShow Local SVN Changes @item:inmenuShow SVN Updates @labelDescription: @title:windowSVN Commit Show updates Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2015-11-11 09:21+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Wdroż Dodano pliki do repozytorium SVN. Dodawanie plików do repozytorium SVN... Nieudane dodawanie plików do repozytorium SVN. Nieudane wdrażanie zmian SVN. Wdrażanie zmian SVN. Wdrażanie zmian SVN... Usunięto pliki z repozytorium SVN. Usuwanie plików z repozytorium SVN... Nieudane usuwanie plików z repozytorium SVN. Wycofano pliki z repozytorium SVN. Wycofywanie plików z repozytorium SVN... Nieudane wycofywanie plików z repozytorium SVN. Nieudana aktualizacja stanu SVN. Wyłączanie opcji "Pokaż uaktualnienia SVN". Nieudana aktualizacja repozytorium SVN. Repozytorium SVN uaktualnione. Aktualizacja repozytorium SVN... Dodaj SVN Wdrożenie SVN... Usuń SVN Wycofaj SVN Uaktualnienie SVN Pokaż lokalne zmiany SVN Pokaż uaktualnienia SVN Opis: Wdrożenie SVN Pokaż aktualizacje 