��          �   %   �      @     A  !   Y      {     �      �     �     �  +        =     N     [  (   z     �  ,   �  7   �     !  7   :     r  ]   �  1   �  	   "     ,  "   ?  5   b  �  �      �  *   �  &   �     �           /  $   P  )   u     �     �     �  ,   �     �  ,   	  :   F	     �	  K   �	  !   �	  j   
  5   s
     �
     �
  .   �
  3   �
                    
         	                                                                                                 Cannot start initramfs. Cannot start update-alternatives. Configure Plymouth Splash Screen Download New Splash Screens EMAIL OF TRANSLATORSYour emails Get New Boot Splash Screens... Initramfs failed to run. Initramfs returned with error condition %1. Install a theme. Marco Martin NAME OF TRANSLATORSYour names No theme specified in helper parameters. Plymouth theme installer Select a global splash screen for the system The theme to install, must be an existing archive file. Theme %1 does not exist. Theme corrupted: .plymouth file not found inside theme. Theme folder %1 does not exist. This module lets you configure the look of the whole workspace with some ready to go presets. Unable to authenticate/execute the action: %1, %2 Uninstall Uninstall a theme. update-alternatives failed to run. update-alternatives returned with error condition %1. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-28 03:05+0200
PO-Revision-Date: 2017-05-21 07:16+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Lokalize 2.0
 Nie można uruchomić initramfs. Nie można uruchomić update-alternatives. Ustawienia ekranu powitalnego Plymouth Pobierz nowe ekrany powitalne lukasz.wojnilowicz@gmail.com Pobierz nowe ekrany powitalne... Nie udało się wykonanie initramfs. Initramfs zakończyło z kodem błedu %1. Wgraj wygląd. Marco Martin Łukasz Wojniłowicz Nie podano wyglądu w parametrach pomocnika. Wgrywanie wyglądu Plymouth Wybierz globalny ekran powitalny dla systemu Wygląd do wgrania to musi być istniejący plik archiwum. Wygląd %1 nie istnieje. Archiwum wyglądądu jest uszkodzone: nie znaleziono w nim pliku .plymouth. Katalog wyglądu %1 nie istnieje. Ten moduł umożliwia ustawienia wyglądu dla całej przestrzeni roboczej, dzięki kilku gotowym zestawom. Nie można uwierzytelnić/wykonać działania: %1, %2 Usuń Usuń wygląd. Nie udało się wykonanie update-alternatives. update-alternatives zakończyło z kodem błedu %1. 