��          �   %   �      0  *   1     \  .   s     �     �     �     �     �     �                 
   $     /     5     =     E     ]     t     �     �     �     �  �  �  M   �     "  8   >     w     �     �     �     �     �     �     �                               *     @     W  0   u     �  	   �     �        
                             	                                                                                          %1 Minimized Window: %1 Minimized Windows: %1 Window: %1 Windows: ...and %1 other window ...and %1 other windows Activity name Activity number Add Virtual Desktop Configure Desktops... Desktop name Desktop number Display: Does nothing General Horizontal Icons Layout: No text Only the current screen Remove Virtual Desktop Selecting current desktop: Show Activity Manager... Shows desktop The pager layoutDefault Vertical Project-Id-Version: plasma_applet_pager
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-19 03:29+0200
PO-Revision-Date: 2016-10-29 08:54+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 %1 zminimalizowane okno: %1 zminimalizowane okno: %1 zminimalizowanych okien: %1 okno: %1 okna: %1 okien: ...i %1 inne okno ...i %1 inne okna ...i %1 innych okien Nazwa aktywności Numer aktywności Dodaj wirtualny pulpit Ustawienia pulpitów... Nazwa pulpitu Numer pulpitu Wyświetlaj: Nic nie robi Ogólne Poziomy Ikony Układ: Brak tekstu Tylko bieżący ekran Usuń wirtualny pulpit Wybranie bieżącego pulpitu: Pokaż program do zarządzania aktywnościami... Pokazuje pulpit Domyślny Pionowy 