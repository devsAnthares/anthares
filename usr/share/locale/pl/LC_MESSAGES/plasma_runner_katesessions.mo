��          <      \       p   !   q   3   �      �   �  �   %   �  8   �     *                   Finds Kate sessions matching :q:. Lists all the Kate editor sessions in your account. Open Kate Session Project-Id-Version: plasma_runner_katesessions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-18 03:08+0100
PO-Revision-Date: 2010-01-25 13:54+0100
Last-Translator: Maciej Wikło <maciej.wiklo@wp.pl>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Znajduje sesje Kate pasujące do :q:. Wyświetla wszystkie sesje edytora Kate na Twoim koncie. Otwórz sesję Kate 