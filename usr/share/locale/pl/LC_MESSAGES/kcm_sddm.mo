��    ,      |  ;   �      �  (   �     �  �   �     �     �     �     �     �     �     �     �     �               %     1      J     k     s     �     �     �     �     �     �     �               /     4     I     Y     l     y     �     �      �  7   �                     1     6  �  <     %	     2	     6	     O	     ]	     j	     p	     �	     �	  	   �	     �	     �	     �	  	   �	     �	     �	     
     .
     6
     K
     b
     p
     
     �
      �
     �
     �
     �
            !   #     E     b     p     �     �  $   �  :   �               *     9     E     !                 %   +   ,      "   #                               &                  	                           )                             *      '      $      (                                
    %1 is the name of a session%1 (Wayland) ... @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) @title:windowSelect Image Advanced Author Auto &Login Background: Clear Image Commands Could not decompress archive Cursor Theme: Customize theme Default Description Download New SDDM Themes EMAIL OF TRANSLATORSYour emails General Get New Theme Halt Command: Install From File Install a theme. Invalid theme package Load from file... Login screen using the SDDM Maximum UID: Minimum UID: NAME OF TRANSLATORSYour names Name No preview available Reboot Command: Relogin after quit Remove Theme SDDM KDE Config SDDM theme installer Session: The default cursor theme in SDDM The theme to install, must be an existing archive file. Theme Unable to install theme Uninstall a theme. User User: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2017-12-09 07:11+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Lokalize 2.0
 %1 (Wayland) ... (Dostępne rozmiary: %1) Wybierz obraz Zaawansowane Autor &Loguj bez pytania Tło: Wyczyść obraz Polecenia Nie można wypakować archiwum Zestaw wskaźników: Dostosuj wygląd Domyślny Opis Pobierz nowy wygląd SDDM lukasz.wojnilowicz@gmail.com Ogólne Pobierz nowy wygląd Polecenie zatrzymania: Wgraj z pliku Wgraj wygląd. Nieprawidłowy pakiet wyglądu Wczytaj z pliku... Ekran logowania używający SDDM Najwyższy UID: Najniższy UID: Łukasz Wojniłowicz Nazwa Podgląd niedostępny Polecenie ponownego uruchomienia: Zaloguj ponownie po wyjściu Usuń wygląd Ustawienia SDDM dla KDE Wgrywanie wyglądu SDDM Sesja: Domyślny zestaw wskaźników w SDDM Wygląd do wgrania to musi być istniejący plik archiwum. Wygląd Nie można wgrać wyglądu Usuń wygląd. Użytkownik Użytkownik: 