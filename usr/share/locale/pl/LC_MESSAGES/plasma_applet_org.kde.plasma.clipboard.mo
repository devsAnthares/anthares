��          �      L      �     �     �     �     �          )     1     9     P     h     t  K   �     �     �     �     �            �        	     &     :     N     k     ~     �     �  !   �     �     �     �     �     �               (     0                                                  
   	                                               Change the barcode type Clear history Clipboard Contents Clipboard history is empty. Clipboard is empty Code 39 Code 93 Configure Clipboard... Creating barcode failed Data Matrix Edit contents Indicator that there are more urls in the clipboard than previews shown+%1 Invoke action QR Code Remove from history Return to Clipboard Search Show barcode Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-10 03:07+0200
PO-Revision-Date: 2015-02-21 08:20+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Lokalize 1.5
 Zmień rodzaj kodu paskowego Wyczyść historię Zawartość schowka Historia schowka jest pusta. Schowek jest pusty Kod 39 Kod 93 Ustawienia schowka... Nieudane tworzenie kodu paskowego Macierz danych Edytuj zawartość +%1 Wywołaj działanie Kod QR Usuń z historii Wróć do schowka Znajdź Pokaż kod kreskowy 