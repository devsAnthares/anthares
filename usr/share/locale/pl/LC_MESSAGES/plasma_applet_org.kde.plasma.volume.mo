��    &      L  5   |      P     Q  8   S     �     �     �     �     �     �  3   �  >        N     i     y     �  m   �     �          "     2     7     ?  *   O      z     �     �  "   �     �     �          3     :     H     X     e     �  ;   �     �  �  �     �     �     �     	     	  
   -	     8	     Q	     k	  
   r	     }	     �	     �	     �	     �	     �	     �	     
     
  	   #
     -
  :   =
  7   x
     �
     �
     �
     �
  !        %     C     P     d     �     �     �     �     �        #   %                                                              "          $                      
                                           &      !                	        % Accessibility data on volume sliderAdjust volume for %1 Applications Audio Muted Audio Volume Behavior Capture Devices Capture Streams Checkable switch for (un-)muting sound output.Mute Checkable switch to change the current default output.Default Decrease Microphone Volume Decrease Volume Devices General Heading for a list of ports of a device (for example built-in laptop speakers or a plug for headphones)Ports Increase Microphone Volume Increase Volume Maximum volume: Mute Mute %1 Mute Microphone No applications playing or recording audio No output or input devices found Playback Devices Playback Streams Port is unavailable (unavailable) Port is unplugged (unplugged) Raise maximum volume Show additional options for %1 Volume Volume at %1% Volume feedback Volume step: label of device items%1 (%2) label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2017-10-01 11:05+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Lokalize 2.0
 % Dostosuj głośność dla %1 Programy Dźwięk wyciszony Głośność dźwięku Zachowanie Urządzenia nagrywające Przechwytywane strumienie Wycisz Domyślnie Zmniejsz głośność mikrofonu Zmniejsz głośność Urządzenia Ogólne Porty Zwiększ głośność mikrofonu Zwiększ głośność Maksymalna głośność: Wycisz Wycisz %1 Wycisz mikrofon Brak aplikacji odtwarzających lub nagrywających dźwięk Nie znaleziono urządzeń wyjściowych lub wejściowych Urządzenia odtwarzające Odtwarzane strumienie  (niedostępny)  (niepodłączony) Zwiększ maksymalną głośność Pokaż dodatkowe opcje dla %1 Głośność Głośność na %1% Informacja zwrotna głośności Krok głośności: %1 (%2) %1: %2 100% %1% 