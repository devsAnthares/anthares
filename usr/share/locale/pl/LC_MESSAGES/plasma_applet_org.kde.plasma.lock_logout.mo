��          �      L      �  &   �  +   �       @     	   ]     g     �     �     �     �  (   �     �     �  ,   �               +     7  �  ;  #   $  +   H     t  
   |     �     �     �  	   �     �     �  5   �     	       %   )     O     V     n     �        
                          	                                                                  Do you want to suspend to RAM (sleep)? Do you want to suspend to disk (hibernate)? General Heading for list of actions (leave, lock, shutdown, ...)Actions Hibernate Hibernate (suspend to disk) Leave Leave... Lock Lock the screen Logout, turn off or restart the computer No Sleep (suspend to RAM) Start a parallel session as a different user Suspend Switch User Switch user Yes Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2015-07-11 07:13+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Lokalize 2.0
 Czy uśpić system do pamięci RAM? Czy zahibernować system (uśpić na dysk)? Ogólne Działania Hibernuj Hibernuj (zapisz na dysk) Wyjdź Wyjdź... Zablokuj Zablokuj ekran Wylogowuje, wyłącza lub uruchamia ponownie komputer Nie Uśpij (zapisz do pamięci) Rozpocznij sesję innego użytkownika Uśpij Przełącz użytkownika Przełącz użytkownika Tak 