��             +         �     �  $   �     �     �  
             $     8     H     O     c     {     �  &   �      �  	   �  �   �  �   �  �   #  �   �     I     N     ]     p     �     �     �     �     �  S   �  G   L  �  �     �	  $   �	     �	     �	     �	     �	     �	     
     
     %
     @
     Y
  
   p
  *   {
     �
  
   �
  �   �
  �   Z  �   
  �   �     O     T     c  
   y     �  !   �     �     �     �  r   �  P   o                                                                                                          	                               
       &Folder: &Microsoft® Windows® network drive &Name: &Port: &Protocol: &Recent connection: &Secure shell (ssh) &Use encryption &User: &WebFolder (webdav) (c) 2004 George Staikos Add Network Folder C&onnect Cr&eate an icon for this remote folder EMAIL OF TRANSLATORSYour emails Encoding: Enter a name for this <i>File Transfer Protocol connection</i> as well as a server address and folder path to use and press the <b>Save & Connect</b> button. Enter a name for this <i>Microsoft Windows network drive</i> as well as a server address and folder path to use and press the <b>Save & Connect</b> button. Enter a name for this <i>Secure shell connection</i> as well as a server address, port and folder path to use and press the <b>Save & Connect</b> button. Enter a name for this <i>WebFolder</i> as well as a server address, port and folder path to use and press the <b>Save & Connect</b> button. FT&P George Staikos KDE Network Wizard NAME OF TRANSLATORSYour names Network Folder Information Network Folder Wizard Primary author and maintainer Save && C&onnect Se&rver: Select the type of network folder you wish to connect to and press the Next button. Unable to connect to server.  Please check your settings and try again. Project-Id-Version: knetattach
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-10 05:48+0100
PO-Revision-Date: 2017-12-09 07:39+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 &Folder: Dysk sieciowy &Microsoft® Windows® &Nazwa: &Port: &Protokół: &Ostatnie połączenie: Bezpieczna powłoka (&ssh) Użyj &szyfrowania &Użytkownik: Folder sieci &Web (WebDAV) (c) 2004 George Staikos  Dodaj katalog sieciowy &Podłącz S&twórz ikonę dla tego zdalnego katalogu adam.kubas@wp.pl Kodowanie: Podaj nazwę dla tego <i>połączenia FTP</i>, a także adres serwera i ścieżkę katalogu, a następnie naciśnij przycisk <b>Zapisz i podłącz</b>. Podaj nazwę dla tego <i>dysku sieciowego Microsoft Windows</i>, a także adres serwera, port i ścieżkę katalogu, a następnie naciśnij przycisk <b>Zapisz i podłącz</b>. Podaj nazwę dla tego <i>połączenia przez SSH</i>, a także adres serwera, port i ścieżkę katalogu, a następnie naciśnij przycisk <b>Zapisz i podłącz</b>. Podaj nazwę dla tego <i>katalogu sieciowego</i>, a także adres serwera, port i ścieżkę katalogu, a następnie naciśnij przycisk <b>Zapisz i podłącz</b>. FT&P George Staikos Pomocnik KDE od sieci Adam Kubas Informacja o katalogu sieciowym Pomocnik od katalogów sieciowych Główny autor i opiekun Zapisz i &podłącz &Serwer: Wybierz rodzaj katalogu sieciowego, z którym chcesz się połączyć, a następnie naciśnij na przycisk "Dalej". Nie można połączyć się z serwerem. Sprawdź ustawienia i spróbuj ponownie. 