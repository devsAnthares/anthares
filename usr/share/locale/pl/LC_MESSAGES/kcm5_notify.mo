��          �      �       0     1  �  H     �       &         ?     `     n     v     �     �  (   �  �  �     �  x  �     l     }  $   �     �     �     �     �     �       /            
                 	                                (c) 2002-2006 KDE Team <h1>System Notifications</h1>Plasma allows for a great deal of control over how you will be notified when certain events occur. There are several choices as to how you are notified:<ul><li>As the application was originally designed.</li><li>With a beep or other noise.</li><li>Via a popup dialog box with additional information.</li><li>By recording the event in a logfile without any additional visual or audible alert.</li></ul> Carsten Pfeiffer Charles Samuels Disable sounds for all of these events EMAIL OF TRANSLATORSYour emails Event source: KNotify NAME OF TRANSLATORSYour names Olivier Goffart Original implementation System Notification Control Panel Module Project-Id-Version: kcmnotify
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-09 07:18+0200
PO-Revision-Date: 2017-09-02 19:52+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 (c) 2002-2006 Zespół KDE <h1>System powiadamiania</h1> Plazma pozwala określić sposób powiadamiania o różnych zdarzeniach. Masz kilka możliwości do wyboru:<ul><li>Sposób domyślny programu.</li><li>Sygnał dźwiękowy.</li><li>Okienko dialogowe zawierające dodatkowe informacje</li>.<li>Zapisanie zdarzenia w pliku dziennika bez dodatkowych ostrzeżeń wizualnych czy dźwiękowych.</li></ul> Carsten Pfeiffer Charles Samuels Wyłącz dźwięki dla tych zdarzeń mrudolf@kdewebdev.org Źródło zdarzenia: KPowiadomienia Michał Rudolf Olivier Goffart Pierwotna wersja Moduł panelu sterowania systemem powiadamiania 