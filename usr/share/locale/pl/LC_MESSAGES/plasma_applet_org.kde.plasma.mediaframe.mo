��            )   �      �     �     �     �     �     �     �     �  
           )   (     R     V     \     c     v     �     �     �  3   �     �       <   #  5   `  8   �  8   �                    /  �  1          )     :     F     X     h     v     �     �  6   �     �  	   �  
   �          !     ?     ]     w  (   �     �  -   �  ^   �  X   X	  7   �	  7   �	     !
     )
     <
     M
                                                  
                                                                               	        Add files... Add folder... Background frame Change picture every Choose a folder Choose files Configure plasmoid... Fill mode: General Left click image opens in external viewer Pad Paths Paths: Pause on mouseover Preserve aspect crop Preserve aspect fit Randomize items Stretch The image is duplicated horizontally and vertically The image is not transformed The image is scaled to fit The image is scaled uniformly to fill, cropping if necessary The image is scaled uniformly to fit without cropping The image is stretched horizontally and tiled vertically The image is stretched vertically and tiled horizontally Tile Tile horizontally Tile vertically s Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-16 03:31+0100
PO-Revision-Date: 2017-11-25 07:23+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Lokalize 2.0
 Dodaj pliki... Dodaj katalog... Ramka w tle Zmieniaj obraz co Wybierz katalog Wybierz pliki Ustawienia plazmoidu... Tryb wypełniania: Ogólne Kliknięcie lewym otwiera w zewnętrznej przeglądarce Wypełnienie Ścieżki Ścieżki: Wstrzymaj po najechaniu myszą Zachowaj kształt przycięcia Zachowaj kształt dopasowania Elementy wybierane losowo Rozciągnij Obraz jest powielany w poziomie i pionie Obraz nie jest przekształcany Obraz jest skalowany tak, aby się zmieścił Obraz jest skalowany z jednakowo tak, aby wypełnić; gdy potrzeba dokonywane jest przycięcie Obraz jest skalowany z zachowaniem kształtu, aby zmieścić się  się bez przycinania Obraz jest rozciągany w poziomie, a w pionie powielany Obraz jest rozciągany w pionie, a w poziomie powielany Kafelki Kafelki w poziomie Kafelki w pionie s 