��    7      �  I   �      �     �     �     �     �     �     �               $     -     5     H     T      `     �     �     �     �     �     �     �     �                     )     7  	   C  	   M     W     d     j     �     �     �     �     �     �     �     �     �     �  @   �     7     @     \     c     j     r     �     �     �  4   �     �  �  �     �	     �	     �	     �	     
     
     
     -
     :
     G
     M
     Q
     a
     s
     �
     �
     �
  	   �
     �
     �
     �
               *     =     Q     d     s     �  	   �     �     �     �     �     �     �     �  	                  !     (  D   4     y  *   �     �     �  	   �     �     �     �     �     �          &   0      7                  #                
          	   '   (   +                       6              3      1                   ,   2                  "       *          $      4   )               %      5                     !          /   .   -       % %1 is value, %2 is unit%1 %2 %1: Application Energy Consumption Battery Capacity Charge Percentage Charge state Charging Current Degree Celsius°C Details: %1 Discharging EMAIL OF TRANSLATORSYour emails Energy Energy Consumption Energy Consumption Statistics Environment Full design Fully charged Has power supply Kai Uwe Broulik Last 12 hours Last 2 hours Last 24 hours Last 48 hours Last 7 days Last full Last hour Manufacturer Model NAME OF TRANSLATORSYour names No Not charging PID: %1 Path: %1 Rechargeable Refresh Serial Number Shorthand for WattsW System Temperature This type of history is currently not available for this device. Timespan Timespan of data to display Vendor VoltV Voltage Wakeups per second: %1 (%2%) WattW Watt-hoursWh Yes current power draw from the battery in WConsumption literal percent sign% Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2016-06-18 07:40+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Lokalize 2.0
 % %1 %2 %1: Pobór energii przez aplikacje Bateria Pojemność Procent ładunku Stan baterii Ładuje się Prąd °C Szczegóły: %1 Rozładowuje się lukasz.wojnilowicz@gmail.com Energia Pobór energii Statystka poboru energii Otoczenie Pojemność nowej W pełni naładowana Jest zasilana prądem Kai Uwe Broulik Ostatnie 12 godzin Ostatnie 2 godziny Ostatnie 24 godziny Ostatnie 48 godzin Ostatnie 7 dni Ostatnia pojemność Ostatnia godzina Wytwórca Model Łukasz Wojniłowicz Nie Nie jest ładowana PID: %1 Ścieżka: %1 Do wielokrotnego ładowania Odśwież Numer seryjny W System Temperatura Ten rodzaj historii nie jest obecnie dostępny dla tego urządzenia. Przedział czasowy Przedział czasowy danych do wyświetlenia Dostawca V Napięcie Wybudzeń na sekundę: %1 (%2%) W Wh Tak Pobór energii % 