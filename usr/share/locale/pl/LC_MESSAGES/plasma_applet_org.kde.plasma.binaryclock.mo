��    	      d      �       �   
   �      �      �   	              "     C  "   ]    �     �     �     �     �     �  %   �     �  (                                	                 Appearance Colors: Display seconds Draw grid Show inactive LEDs: Use custom color for active LEDs Use custom color for grid Use custom color for inactive LEDs Project-Id-Version: plasma_applet_binaryclock2
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-31 05:47+0100
PO-Revision-Date: 2017-05-21 07:22+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Wygląd Barwy: Wyświetl sekundy Narysuj siatkę Pokaż nieaktywne LEDy: Użyj własnych barw aktywnych LEDów Użyj własnej barwy dla siatki Użyj własnych barw nieaktywnych LEDów 