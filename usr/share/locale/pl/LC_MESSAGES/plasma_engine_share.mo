��    
      l      �       �   $   �   %     :   <     w  '   �  -   �     �       '     �  <  )     2   G  =   z  2   �  0   �  -        J     d  /   t         	                            
        Could not detect the file's mimetype Could not find all required functions Could not find the provider with the specified destination Error trying to execute script Invalid path for the requested provider It was not possible to read the selected file Service was not available Unknown Error You must specify a URL for this service Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2011-02-20 15:10+0100
Last-Translator: Marta Rybczyńska <kde-i18n@rybczynska.net>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Nie można wykryć typu MIME danego pliku Nie można znaleźć wszystkich wymaganych funkcji Nie można znaleźć dostawcy z określonym adresem docelowym Wystąpił błąd podczas próby wykonania skryptu Nieprawidłowa ścieżka dla żądanego dostawcy Odczytanie wybranego pliku nie było możliwe Serwis nie był dostępny Nieznany błąd Należy określić adres URL dla danego serwisu 