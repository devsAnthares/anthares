��    -      �  =   �      �  Z   �  *   <     g     �     �     �     �     �  >   �  B   .     q  9   �     �     �      �               9  ;   R  -   �  )   �     �     �          6     O  -   h     �  !   �     �     �               9     T     k     �     �  >   �  (   �     	  >   )	  &   h	  &   �	  �  �	     O  0   W     �  !   �      �     �  !   �       W   %  I   }      �  C   �  
   ,     7  (   V       #   �     �  F   �  .   *  *   Y  !   �  &   �     �     �       *   '     R  '   r     �     �     �  !   �          ,     C     [  (   m  W   �  0   �       T   5  #   �  #   �            	          '   
                      "                          (          -   %       *                     +                        $               !      &                 #   ,          )              %1 is 'the slot asked for foo arguments', %2 is 'but there are only bar available'%1, %2. %1 is not a function and cannot be called. %1 is not an Object type '%1' is not a valid QLayout. '%1' is not a valid QWidget. Action takes 2 args. ActionGroup takes 2 args. Alert Bad event handler: Object %1 Identifier %2 Method %3 Type: %4. Bad slot handler: Object %1 Identifier %2 Method %3 Signature: %4. Call to '%1' failed. Call to method '%1' failed, unable to get argument %2: %3 Confirm Could not construct value Could not create temporary file. Could not open file '%1' Could not open file '%1': %2 Could not read file '%1' Error encountered while processing include '%1' line %2: %3 Exception calling '%1' function from %2:%3:%4 Exception calling '%1' slot from %2:%3:%4 Failed to create Action. Failed to create ActionGroup. Failed to create Layout. Failed to create Widget. Failed to load file '%1' Failure to cast to %1 value from Type %2 (%3) File %1 not found. First argument must be a QObject. Incorrect number of arguments. Must supply a filename. Must supply a layout name. Must supply a valid parent. Must supply a widget name. No classname specified No classname specified. No such method '%1'. Not enough arguments. The slot asked for %1 argument The slot asked for %1 arguments There was an error reading the file '%1' Wrong object type. but there is only %1 available but there are only %1 available include only takes 1 argument, not %1. library only takes 1 argument, not %1. Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2014-03-23 06:53+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 %1, %2. %1 nie jest funkcją i nie może być wywołane. %1 nie jest obiektem '%1' nie jest poprawnym układem. '%1' nie jest poprawnym QWidget. Action wymaga 2 parametrów. ActionGroup wymaga 2 parametrów. Alarm Nieprawidłowa funkcja obsługi zdarzenia: Obiekt %1 Identyfikator %2 Metoda %3 Typ %4. Błędna obsługa slota: Obiekt %1 Identyfikator %2 Metoda %3 Podpis: %4. Wywołanie '%1' nie udało się. Wywołanie metody '%1' nieudane, nie można pobrać parametru %2:%3 Potwierdź Nie można utworzyć wartości Nie można utworzyć pliku tymczasowego. Nie można otworzyć pliku '%1' Nie można otworzyć pliku '%1': %2 Nie można odczytać pliku '%1' Błąd podczas przetwarzania pliku nagłówkowego '%1 w wierszu %2: %3 Wyjątek wywołujący funkcję '%1' z %2:%3:%4 Wyjątek wywołujący slot '%1' z %2:%3:%4 Nie udało się utworzyć Action. Nie udało się utworzyć ActionGroup. Nie można utworzyć Layout. Nie można utworzyć widgetu. Nie można wczytać pliku '%1' Błąd rzutowania na typ %1 z typu %2 (%3) Plik %1 nie został znaleziony. Pierwszym parametrem musi być QObject. Niepoprawna liczba parametrów. Trzeba podać nazwę pliku. Trzeba podać układ. Trzeba podać poprawnego rodzica. Trzeba podać nazwę widgeta. Nie podano nazwy klasy Nie podano nazwy klasy. Brak metody '%1'. Brak wystarczającej liczby parametrów. Pozycja wymaga %1 parametru Pozycja wymaga %1 parametrów Pozycja wymaga %1 parametrów Podczas wczytywania pliku '%1' wystąpił błąd Błędny typ obiektu. ale dostępny jest tylko %1 ale dostępne są tylko %1 ale dostępnych jest tylko %1 include wymaga 1 parametru, nie %1. library wymaga 1 parametru, nie %1. 