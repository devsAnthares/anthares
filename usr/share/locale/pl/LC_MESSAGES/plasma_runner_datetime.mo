��    	      d      �       �      �      �   -        <  -   V  #   �  #   �     �  �  �     �     �  4   �     .  2   H     {     �     �                                   	           Current time is %1 Displays the current date Displays the current date in a given timezone Displays the current time Displays the current time in a given timezone Note this is a KRunner keyworddate Note this is a KRunner keywordtime Today's date is %1 Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2015-01-31 07:05+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Bieżący czas to %1 Wyświetla bieżącą datę Wyświetla bieżącą datę w danej strefie czasowej Wyświetla bieżący czas Wyświetla bieżący czas w danej strefie czasowej data czas Dzisiejsza data to %1 