��    %      D  5   l      @     A     E     G  	   Y     c     |     �     �     �     �     �     �            S   ,  w   �     �  M         [     |  ?   �     �  	   �     �     
     #  %   2     X     e  F   �  #   �     �  ]     R   f     �     �  �  �     �	     �	     �	     �	     �	     
  
   *
  	   5
      ?
  %   `
     �
  (   �
     �
     �
  M   �
  g   E     �  6   �     �       C   %     i     ~     �     �     �  C   �            D   +  ,   p     �  e   �  P   #     t     �               !      "          #   %                                                                            $                          	                
                            ms % %1 - All Desktops %1 - Cube %1 - Current Application %1 - Current Desktop %1 - Cylinder %1 - Sphere &Reactivation delay: &Switch desktop on edge: Activation &delay: Active Screen Corners and Edges Activity Manager Always Enabled Amount of time required after triggering an action until the next trigger can occur Amount of time required for the mouse cursor to be pushed against the edge of the screen before the action is triggered Application Launcher Change desktop when the mouse cursor is pushed against the edge of the screen EMAIL OF TRANSLATORSYour emails Lock Screen Maximize windows by dragging them to the top edge of the screen NAME OF TRANSLATORSYour names No Action Only When Moving Windows Open krunnerRun Command Other Settings Quarter tiling triggered in the outer Show Desktop Switch desktop on edgeDisabled Tile windows by dragging them to the left or right edges of the screen Toggle alternative window switching Toggle window switching Trigger an action by pushing the mouse cursor against the corresponding screen edge or corner Trigger an action by swiping from the screen edge towards the center of the screen Window Management of the screen Project-Id-Version: kcmkwinscreenedges
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-30 03:13+0100
PO-Revision-Date: 2018-01-06 07:01+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
  ms % %1 - Wszystkie pulpity %1 - Kostka %1 - Bieżący program %1 - Bieżący pulpit %1 - Walec %1 - Kula Opóźnienie &nowego wywołania: Przełączanie pulpitu na &krawędzi: Opóźnienie &wywołania: Narożniki i krawędzie aktywnego ekranu Zarządzanie aktywnościami Zawsze włączone Czasu potrzebny na wywołanie kolejnego działania po wywołaniu poprzedniego Ilość czasu potrzebna na wyzwolenie działania przy napieraniu wskaźnikiem myszy na krawędź ekranu Aktywator programów Zmień pulpit, gdy kursor najedzie na krawędź ekranu lukasz.wojnilowicz@gmail.com Zablokuj ekran Okno zajmie cały ekran po przeciągnięciu go do górnej krawędzi Łukasz Wojniłowicz Brak działania Tylko przy przesuwaniu okien Wykonaj polecenie Inne ustawienia Okno zajmie ćwierć ekranu po przeciągnięciu go do zewnętrznych Pokaż pulpit Wyłączone Okno zajmie pół ekranu po przeciągnięciu go do bocznej krawędzi Przełącz alternatywne przełączanie okien Przełącz przełączanie okien Wyzwala działanie, przy napieraniu wskaźnikiem myszy na odpowiednią krawędź lub narożnik ekranu Wyzwala działanie, przy przesuwaniu z krawędzi ekranu w kierunku jego środka. Zarządzanie oknami ekranu 