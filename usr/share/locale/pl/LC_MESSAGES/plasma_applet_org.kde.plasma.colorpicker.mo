��          |      �          %   !     G     U     c     u     �     �  
   �     �     �  $   �    �  #   �       
   *     5     H     `     h     �     �     �  &   �     
   	                                                   Automatically copy color to clipboard Clear History Color Options Copy to Clipboard Default color format: General Open Color Dialog Pick Color Pick a color Show history When pressing the keyboard shortcut: Project-Id-Version: plasma_applet_kolourpicker
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-12 03:03+0200
PO-Revision-Date: 2016-12-24 12:03+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Samoczynnie kopiuj barwy do schowka Wyczyść historię Opcje barw Skopiuj do schowka Domyślny format barwy: Ogólne Otwórz okno dialogowe barw Wybierz barwę Wybierz barwę Pokaż historię Po naciśnięciu skrótu klawiszowego: 