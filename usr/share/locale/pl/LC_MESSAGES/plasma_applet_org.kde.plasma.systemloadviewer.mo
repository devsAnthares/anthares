��    &      L  5   |      P     Q     l     y     �     �     �     �     �     �     �     �     �  &   �               #     ,     3     ?     M     U     ]     d     s     �     �     �     �     �     �     �     �     �     �  
   �            �              +     	   :     D     L     U     a     s     �     �     �  1   �     �     	          &     -     >     O     W     _     h     y     �     �     �     �     �     �     �     	     	     "	     7	     R	     _	                           	      #      $                                                    &                                          !          %                            "   
    Abbreviation for secondss Application: Average clock: %1 MHz Bar Buffers: CPU CPU %1 CPU monitor CPU%1: %2% @ %3 Mhz CPU: %1% CPUs separately Cache Cache Dirty, Writeback: %1 MiB, %2 MiB Cache monitor Cached: Circular Colors Compact Bar Dirty memory: General IOWait: Memory Memory monitor Memory: %1/%2 MiB Monitor type: Nice: Set colors manually Show: Swap Swap monitor Swap: %1/%2 MiB Sys: System load Update interval: Used swap: User: Writeback memory: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-29 03:31+0200
PO-Revision-Date: 2017-02-25 06:50+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Lokalize 2.0
 s Program: Przeciętna częstotliwość zegara: %1 MHz Słupkowy Bufory: Procesor Procesor %1 Monitor procesora CPU%1: %2% @ %3 Mhz Procesor: %1% Procesory osobno Pamięć podręczna Pamięć podręczna brudna, Zapis: %1 MiB, %2 MiB Monitor pamięci podręcznej Pamięć podręczna: Kołowy Kolory Kompaktowy pasek Brudna pamięć: Ogólne IOWait: Pamięć Monitor pamięci Pamięć: %1/%2 MiB Rodzaj monitora: Nice: Ustaw kolory ręcznie Pokaż: Pamięć wymiany Monitor pamięci wymiany Pamięć wymiany: %1/%2 MiB Sys: Obciążenie systemu Okres uaktualnienia: Używana pamięć wymiany: Użytkownik: Pamięć do zapisu: 