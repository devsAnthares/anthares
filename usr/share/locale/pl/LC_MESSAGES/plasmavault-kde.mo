��    ?        Y         p  �  q  }  J  M  �        
   7     B     K     j     �  <   �     �     �  	   �     �  .     %   A     g     }     �     �     �     �     �     �     �     �  4        B  9   T     �     �     �  �   �  !   �  t   �     8     D     a     n     s  	   �     �  -   �     �  B   �  &     ?   B  '   �  )   �  7   �  .     5   ;  +   q     �     �     �  ,   �          -  9   :  V   t  C   �  �    �  �  �  �  �  �     F#     X#     d#  &   l#  &   �#     �#  9   �#     �#     $  
    $     +$  ,   ?$  !   l$     �$     �$     �$     �$     �$     �$     	%  3   %     L%     R%  @   r%     �%  ;   �%     &     &&     :&  �   B&  #   ?'  p   c'     �'     �'     (  	   (     (     >(     F(  %   R(  	   x(  C   �(     �(  =   �(  ,   )  -   L)  F   z)  &   �)  /   �)  2   *     K*     j*     �*  .   �*     �*     �*  /   �*  O   $+     t+     !      	   2   ?   
   /                 +   (   *       1            )   >           8          9   $              7             .       '   6          %   <             3   ,          ;       =   &                              -           #      "          :      5      0                                   4               <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body style=" font-family:'hlv'; font-size:9pt; font-weight:400; font-style:normal;">
<p style="-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><br /></p></body></html> <b>Security notice:</b>
                             According to a security audit by Taylor Hornby (Defuse Security),
                             the current implementation of Encfs is vulnerable or potentially vulnerable
                             to multiple types of attacks.
                             For example, an attacker with read/write access
                             to encrypted data might lower the decryption complexity
                             for subsequently encrypted data without this being noticed by a legitimate user,
                             or might use timing analysis to deduce information.
                             <br /><br />
                             This means that you should not synchronize
                             the encrypted data to a cloud storage service,
                             or use it in other circumstances where the attacker
                             can frequently access the encrypted data.
                             <br /><br />
                             See <a href='http://defuse.ca/audits/encfs.htm'>defuse.ca/audits/encfs.htm</a> for more information. <b>Security notice:</b>
                             CryFS encrypts your files, so you can safely store them anywhere.
                             It works well together with cloud services like Dropbox, iCloud, OneDrive and others.
                             <br /><br />
                             Unlike some other file-system overlay solutions,
                             it does not expose the directory structure,
                             the number of files nor the file sizes
                             through the encrypted data format.
                             <br /><br />
                             One important thing to note is that,
                             while CryFS is considered safe,
                             there is no independent security audit
                             which confirms this. @title:windowCreate a New Vault Activities Backend: Can not create the mount point Can not open an unknown vault. Change Choose the encryption system you want to use for this vault: Choose the used cypher: Close the vault Configure Configure Vault... Configured backend can not be instantiated: %1 Configured backend does not exist: %1 Correct version found Create Create a New Vault... CryFS Device is already open Device is not open Dialog Do not show this notice again EncFS Encrypted data location Failed to create directories, check your permissions Failed to execute Failed to fetch the list of applications using this vault Failed to open: %1 Forcefully close  General If you limit this vault only to certain activities, it will be shown in the applet only when you are in those activities. Furthermore, when you switch to an activity it should not be available in, it will automatically be closed. Limit to the selected activities: Mind that there is no way to recover a forgotten password. If you forget the password, your data is as good as gone. Mount point Mount point is not specified Mount point: Next Open with File Manager Password: Plasma Vault Please enter the password to open this vault: Previous The mount point directory is not empty, refusing to open the vault The specified backend is not available The vault configuration can only be changed while it is closed. The vault is unknown, can not close it. The vault is unknown, can not destroy it. This device is already registered. Can not recreate it. This directory already contains encrypted data Unable to close the vault, an application is using it Unable to close the vault, it is used by %1 Unable to detect the version Unable to perform the operation Unknown device Unknown error, unable to create the backend. Use the default cipher Vaul&t name: Wrong version installed. The required version is %1.%2.%3 You need to select empty directories for the encrypted storage and for the mount point formatting the message for a command, as in encfs: not found%1: %2 Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-11 05:53+0100
PO-Revision-Date: 2017-12-25 08:00+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Lokalize 2.0
 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body style=" font-family:'hlv'; font-size:9pt; font-weight:400; font-style:normal;">
<p style="-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><br /></p></body></html> <b>Uwaga odnośnie bezpieczeństwa:</b>
                             Zgodnie z audytem bezpieczeństwa przeprowadzonym przez Taylor Hornby (Rozbrajanie Zabezpieczenia),
                             obecna implementacja Encfs jest podatna lub potencjanie podatna
                             na wiele rodzajów ataków.
                             Na przykład, napastnik z uprawnieniami odczytu/zapisu
                             zaszyfrowanych danych może obniżyć złożoność szyfrowania
                             dla kolejnych zaszyfrowanych danych bez powiadomienia o tym prawowitego użytkownika,
                             lub może skorzystać z analizy czasów, aby wywnioskować informacje.
                             <br /><br />
                             Oznacza to, że nie powinieneś synchronizować
                             szyfrowanych danych z usługą przechowywania w chmurze,
                             lub narażać się w innych przypadkach, w których napastnik
                             może często odczytywać szyfrowane dane.
                             <br /><br />
                             Zajrzyj na <a href='http://defuse.ca/audits/encfs.htm'>defuse.ca/audits/encfs.htm</a> po więcej informacji. <b>Uwaga odnośnie bezpieczeństwa:</b>
                             CryFS szyfruje twoje pliki, tak abyś mógł je bezpieczenie przechowywać w każdym miejscu.
                             Dobrze działa z usługami w chmurze takimi jakDropbox, iCloud, OneDrive i innymi.
                             <br /><br />
                             W odróżnieniu do innych nakładkowych systemów plików,
                             nie przedstawia struktury katalogów,
                             liczby czy rozmiaru plików
                             przez format zaszyfrowanych danych.
                             <br /><br />
                             Należy miećc na uwadze,
                             że mimo iż CryFS jest uważany za bezpieczny,
                             to nie istnieje żaden niezależny audyt bezpieczeństwa,
                             który to potwierdza. Utwórz nowy sejf Aktywności Silnik: Nie można utworzyć punktu podpięcia Nie można otworzyć nieznanego sejfu. Zmiana Wybierz system szyfrowania wykorzystywany dla tego sejfu: Wybierz używany szyfr: Zamknij sejf Ustawienia Ustawienia sejfu... Nie można wywołać ustawionego silnika: %1 Ustawiony silnik nie istnieje: %1 Znaleziono poprawną wersję Utwórz Utwórz nowy sejf... CryFS Urządzenie jest już otwarte Urządzenie nie jest otwarte Okno dialogowe Nie wyświetlaj więcej tego powiadomienia ponownie EncFS Położenia szyfrowanych danych Nie udało się utworzyć katalogów, sprawdź swoje uprawnienia Nie udało się wykonać Nie można pobrać listy aplikacji używających tego sejfu Nie udało się otworzyć: %1 Wymuś zamknięcie  Ogólne Jeśli ograniczysz ten sejf tylko do pewnych aktywności, to będzie on pokazywany na aplecie tylko gdy będziesz w tych aktywnościach. Ponadto, jeśli przełączysz się do aktywności, której nie wybrałeś, to sejf zostanie samoczynnie zamknięty. Ogranicz do wskazanych aktywności: Miej na uwadze, że nie można odzyskać zapomnianego hasła. Jeśli zapomnisz hasła, to twoje dane przepadły. Punkt podpięcia Nie podano punktu podpięcia Punkt podpięcia: Następny Otwórz w przeglądarce plików Hasło: Sejf Plazmy Podaj hasło, aby otworzyć ten sejf: Poprzedni Katalog punktu podpięcia nie jest pusty, odmówiono otwarcia sejfu Podany silnik nie istnieje Ustawienia sejfu można zmienić dopiero po jego zamknięciu. Sejf jest nieznany, nie można go zamknąć. Sejf jest nieznany, nie można go zniszczyć. Urządzenie jest już zarejestrowane. Nie można utworzyć go na nowo. Katalog już zawiera zaszyfrowane dane Nie można zamknąć sejfu, aplikacja go używa Nie można zamknąć sejfu, jest używany przez %1 Nie udało się wykryć wersji Nie można wykonać działania Nieznane urządzenie Nieznany błąd, nie można utworzyć silnika. Użyj domyślnego szyfru Nazwa sej&fu: Wgrana zła wersja. Wymagana wersja to %1.%2.%3 Musisz wskazać puste katalogi dla szyfrowanej przechowalni i punktu podpięcia %1: %2 