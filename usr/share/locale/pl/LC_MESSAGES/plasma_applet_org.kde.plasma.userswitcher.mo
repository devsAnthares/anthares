��          �      <      �     �     �     �     �     �  '   �  >        L     f     �     �     �  )   �  7   �  '        B     T  �  s     \     r     z     �  
   �     �  	   �  #   �  (   �     �          ,  &   @  
   g     r      y  #   �                                       
                      	                                        Current user General Layout Lock Screen New Session Nobody logged in on that sessionUnused Show a dialog with options to logout/shutdown/restartLeave... Show both avatar and name Show full name (if available) Show login username Show only avatar Show only name Show technical information about sessions User logged in on console (X display number)on %1 (%2) User logged in on console numberTTY %1 User name display You are logged in as <b>%1</b> Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-11 03:18+0100
PO-Revision-Date: 2016-04-16 06:25+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Lokalize 2.0
 Bieżący użytkownik Ogólne Układ Zablokuj ekran Nowa sesja Nieużywana Wyjdź... Pokaż zarówno nazwę jak i awatar Pokaż pełną nazwę (jeśli dostępna) Pokaż nazwę z logowania Pokaż tylko awatar Pokaż tylko nazwę Pokaż informacje techniczne o sesjach na %1 (%2) TTY %1 Wyświetlanie nazwy użytkownika Jesteś zalogowany/a jako <b>%1</b> 