��          �   %   �      @     A  �   `  m   �  �   P  )   �  `        o     |     �     �     �      �     �     �  '        ,     >     ]     b     s  ?   �  Y   �  +     H   F  �  �     x  �   �  Z    	     {	     �	  [   �	     
  (   
     D
     X
  (   ]
     �
     �
     �
  )   �
     �
                     2  ;   ?  a   {  6   �  D                                                    	                                                               
              (c) 2003-2007 Fredrik Höglund <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) @item:inlistbox sizeResolution dependent A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Cursor Theme Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Fredrik Höglund Get new Theme Get new color schemes from the Internet Install from File NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. You have to restart the Plasma session for these changes to take effect. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-07-16 07:21+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Lokalize 2.0
 (c) 2003-2007 Fredrik Höglund <qt>Czy na pewno usunąć zestaw wskaźników <i>%1</i>?<br/> Spowoduje to usunięcie wszystkich plików wgranych przez ten zestaw.</qt> <qt>Nie można usunąć bieżącego zestawu.<br/>Trzeba najpierw wybrać inny zestaw.</qt> (Dostępne rozmiary: %1) Zależnie od rozdzielczości Zestaw o nazwie %1 już istnieje w twoim katalogu zestawów ikon. Czy chcesz go zastąpić? Potwierdzenie Ustawienia wskaźnika uległy zmienianie Zestaw wskaźników Opis Przeciągnij lub wpisz adres URL zestawu lukasz.wojnilowicz@gmail.com Fredrik Höglund Pobierz nowy zestaw Pobierz nowe zestawy kolorów z internetu Wgraj z pliku Łukasz Wojniłowicz Nazwa Czy zastąpić zestaw? Usuń zestaw Plik %1 nie jest poprawnym archiwum zestawów wskaźników. Nie można pobrać archiwum zestawu wskaźników. Proszę sprawdzić, czy adres %1 jest poprawny. Nie można znaleźć archiwum zestawu wskaźników %1. Zmiany zostaną zastosowane dopiero po ponownym uruchomieniu Plazmy. 