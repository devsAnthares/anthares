��          t      �         P     )   b  %   �  @   �     �  V   	     `     {     �     �  �  �     �  U   �  _   �  U   L     �  h   �  '   +     S  
   n     y                         	      
                             %1 is '%1 packages to update' and %2 is 'of which %1 is security updates'%1, %2 1 package to update %1 packages to update 1 security update %1 security updates First part of '%1, %2'1 package to update %1 packages to update No packages to update Second part of '%1, %2'of which 1 is security update of which %1 are security updates Security updates available System up to date Update Updates available Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-15 03:15+0100
PO-Revision-Date: 2018-01-28 06:49+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Lokalize 2.0
 %1, %2 1 pakiet do uaktualnienia %1 pakietów do uaktualnienia %1 pakietów do uaktualnienia Uaktualnienie bezpieczeństwa %1 uaktualnienie bezpieczeństwa %1 uaktualnienie bezpieczeństwa 1 pakiet do uaktualnienia %1 pakietów do uaktualnienia %1 pakietów do uaktualnienia Brak pakietów do uaktualnienia z czego 1 dotyczy bezpieczeństwa z czego %1 dotyczą bezpieczeństwa z czego %1 dotyczy bezpieczeństwa Dostępne uaktualnienie bezpieczeństwa Twój system jest aktualny Uaktualnij Dostępne uaktualnienia 