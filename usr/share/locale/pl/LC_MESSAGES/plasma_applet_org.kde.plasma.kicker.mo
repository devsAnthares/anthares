��    _                   C   	  /   M  -   }     �     �     �     �      	     	     1	     >	     J	  
   S	     ^	     g	     p	     �	  	   �	     �	     �	     �	  ,   �	  	    
     

  
   )
     4
     L
     `
     u
     �
     �
     �
     �
     �
  	   �
     �
     �
     
               !     (  	   ;     E     ]  
   r     }     �  
   �     �     �     �  
   �     �     �               $     2     @     R     h     y     �     �     �     �  	   �     �     �     �               4     Q     j     �     �     �     �  	   �     �  ,   �          !     0     @     L     S     b     t     �  #   �     �  �  �     �  
   �     �     �     �  $     &   -     T     g     o     x  
   �  	   �     �     �     �     �  	   �     �     �       3        E  &   N     u      �      �  !   �     �                 +     L     T  
   \     g     p     ~     �     �     �     �     �     �      �     �          4     N     b     {     �     �     �     �     �     �     �     �          $     7     Q      f     �     �     �     �     �     �     �     �       &     &   A  '   h     �     �     �     �     �     �  %        +     2     E     W     o     v     �     �     �  $   �     �         Q         Y   5           %       H       J   !              \   *       @   V   4   [       A   (   
       U   B   ]   ^       $   _         '   >       D   K      -                  ?      ,   O   0       R   8   6   W   2   I   =   X   E   #       N   C       T   G   M          3   "         9             +          S          F           ;   :                    &   <   L          P         7          .       /       1                     )            	   Z       @action opens a software center with the applicationManage '%1'... @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Desktop Add to Favorites Add to Panel (Widget) Align search results to bottom All Applications App name (Generic name)%1 (%2) Applications Apps & Docs Behavior Categories Computer Contacts Description (Name) Description only Documents Edit Application... Edit Applications... End session Expand search to bookmarks, files and emails Favorites Flatten menu to a single level Forget All Forget All Applications Forget All Contacts Forget All Documents Forget Application Forget Contact Forget Document Forget Recent Documents General Generic name (App name)%1 (%2) Hibernate Hide %1 Hide Application Icon: Lock Lock screen Logout Name (Description) Name only Often Used Applications Often Used Documents Often used On All Activities On The Current Activity Open with: Pin to Task Manager Places Power / Session Properties Reboot Recent Applications Recent Contacts Recent Documents Recently Used Recently used Removable Storage Remove from Favorites Restart computer Run Command... Run a command or a search query Save Session Search Search results Search... Searching for '%1' Session Show Contact Information... Show In Favorites Show applications as: Show often used applications Show often used contacts Show often used documents Show recent applications Show recent contacts Show recent documents Show: Shut Down Sort alphabetically Start a parallel session as a different user Suspend Suspend to RAM Suspend to disk Switch User System System actions Turn off computer Type to search. Unhide Applications in '%1' Unhide Applications in this Submenu Widgets Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2017-10-01 10:15+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Lokalize 2.0
 Zarządzaj '%1'... Wybierz... Wyczyść ikonę Dodaj na pulpit Dodaj do ulubionych Dodaj do panelu (element interfejsu) Wyrównaj wyniki wyszukiwania do dołu Wszystkie programy %1 (%2) Programy Aplikacje i dokumentacja Zachowanie Kategorie Komputer Kontakty Opis (nazwa) Tylko opisy Dokumenty Edytuj programy... Edytuj programy... Zakończ sesję Rozszerz wyszukiwanie na zakładki, pliki i pocztę Ulubione Spłaszcz menu do pojedynczego poziomu Zapomnij wszystko Zapomnij o wszystkich programach Zapomnij o wszystkich kontaktach Zapomnij o wszystkich dokumentach Zapomnij o programie Zapomnij o kontakcie Zapomnij o dokumencie Zapomnij o ostatnich dokumentach Ogólne %1 (%2) Zahibernuj Ukryj %1 Ukryj program Ikona: Zablokuj Zablokuj ekran Wyloguj Nazwa (opis) Tylko nazwy Najczęściej używane programy Najczęściej używane dokumenty Najczęściej używanych Na wszystkch aktywnościach Na bieżącej aktywności Otwórz za pomocą: Przypnij do paska zadań Miejsca Zasilanie / Sesja Właściwości Uruchom ponownie Ostatnie programy Ostatnie kontakty Ostatnie dokumenty Ostatnio używane Ostatnio używanych Nośniki wymienne Usuń z ulubionych Uruchom komputer ponownie Wykonaj polecenie... Wykonuje polecenie lub wyszukuje Zapisz sesję Szukaj Wyniki wyszukiwania Wyszukaj... Szukanie '%1' Sesja Pokaż szczegóły kontaktu... Pokaż w ulubionych Pokazuj programy jako: Pokaż najczęściej używane programy Pokaż najczęściej używane kontakty Pokaż najczęściej używane dokumenty Pokaż ostatnie programy Pokaż ostatnie kontakty Pokaż ostatnie dokumenty Pokaż: Wyłącz Uszereguj alfabetycznie Rozpocznij sesję innego użytkownika Uśpij Uśpienie do RAM-u Uśpienie na dysk Przełącz użytkownika System Działania systemowe Wyłącz komputer Wpisz, aby wyszukać. Pokaż programy w '%1' Pokaż ukryte programy w tym podmenu Elementy interfejsu 