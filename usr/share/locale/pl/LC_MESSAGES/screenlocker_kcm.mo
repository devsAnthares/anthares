��          �            x     y  
   �  
   �     �  .   �     �     �     �        *   )      T  ,   u  ,   �  0   �        �    #   �  	        (     0  :   7     r     ~     �     �  /   �     �            0   .     _                        
                                            	           &Lock screen on resume: Activation Appearance Error Failed to successfully test the screen locker. Immediately Keyboard shortcut: Lock Session Lock screen automatically after: Lock screen when waking up from suspension Re&quire password after locking: Spinbox suffix. Short for minutes min  mins Spinbox suffix. Short for seconds sec  secs The global keyboard shortcut to lock the screen. Wallpaper &Type: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:10+0100
PO-Revision-Date: 2018-01-28 06:50+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Lokalize 2.0
 Zab&lokuj ekran po wznowieniu pracy Aktywacja Wygląd Błąd Próba blokowania ekranu zakończyła się niepowodzeniem. Natychmiast Skrót klawiszowy: Zablokuj sesję Zablokuj ekran samoczynnie po: Zablokuj ekran po wybudzeniu ze stanu uśpienia Wymagaj hasła po zablokowaniu:  min  min  min  sek  sek  sek Globalny skrót klawiszowy do blokowania ekranu. Rodzaj &tapety: 