��    &      L  5   |      P      Q     r     ~  -   �  #   �  #   �  ?     1   S     �     �     �  H   �  L   �     F  V   N  N   �     �  
                   (     >     W  2   _  
   �     �  )   �  8   �  #      .   D  -   s  D   �  6   �  0     A   N  =   �  9   �  �  	  1   �
  
   #  C   .  1   r  	   �  	   �     �     �     �     �     �  /     &   E     l     u     }  
   �     �     �     �     �     �  
                   ,  *   J     u  +   �  7   �  +   �  &        >     F     T     X     j                  "                            
       $                      !                &                                      %      	           #                                %1 notification %1 notifications %1 of %2 %3 %1 running job %1 running jobs &Configure Event Notifications and Actions... 10 seconds ago, keep short10 s ago 30 seconds ago, keep short30 s ago A button tooltip; expands the item to show detailsShow Details A button tooltip; hides item detailsHide Details Clear Notifications Copy Copy Link Address Either just 1 dir or m of n dirs are being processed1 dir %2 of %1 dirs Either just 1 file or m of n files are being processed1 file %2 of %1 files History How much many bytes (or whether unit in the locale has been copied over total%1 of %2 Indicator that there are more urls in the notification than previews shown+%1 Information Job Failed Job Finished More Options... No new notifications. No notifications or jobs Open... Placeholder is job name, eg. 'Copying'%1 (Paused) Select All Show a history of notifications Show application and system notifications Speed and estimated time to completion%1 (%2 remaining) Track file transfers and other jobs Use custom position for the notification popup minutes ago, keep short%1 min ago %1 min ago notification was added n days ago, keep short%1 day ago %1 days ago notification was added yesterday, keep shortYesterday notification was just added, keep shortJust now placeholder is row description, such as Source or Destination%1: the job, which can be anything, failed to complete%1: Failed the job, which can be anything, has finished%1: Finished Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:07+0100
PO-Revision-Date: 2018-01-28 06:48+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Lokalize 2.0
 %1 powiadomienie %1 powiadomienia %1 powiadomień %1 z %2 %3 %1 wykonywane zadanie  %1 wykonywane zadania %1 wykonywanych zadań U&stawienia powiadomień i działań wydarzeń... 10 s temu 30 s temu Pokaż szczegóły Ukryj szczegóły Wyczyść powiadomienia Kopiuj Skopiuj adres odnośnika 1 katalog %2 z %1 katalogów %2 z %1 katalogów 1 plik %2 z %1 plików %2 z %1 plików Historia %1 z %2 +%1 Informacja Niepowodzenie zadania Ukończono zadanie Więcej możliwości... Brak nowych powiadomień Brak powiadomień i zdań Otwórz... %1 (wstrzymane) Zaznacz wszystko Pokaż historię powiadomień Pokazuj powiadomienia programów i systemu %1 (%2 do końca) Śledź przesyłanie plików i inne zadania Wskaż własne miejsce wysuwania się okna powiadomień %1 minuty temu %1 minuty temu %1 minut temu %1 dzień temu %1 dni temu %1 dni temu Wczoraj Przed chwilą %1: %1: Niepowodzenie %1: Ukończono 