��    $      <  5   \      0     1     6  )   J     t  $   �  &   �  #   �  !   �  "   !     D     T     f     z     �  J   �     �  L        [     c     k      {     �  
   �     �     �     �     �     �  "   �       )   9  <   c     �  ;   �     �  �       �     �  !   	     %	  (   3	  '   \	     �	     �	     �	     �	  
   �	     �	     �	     �	  m   
      y
  a   �
     �
  	             )     F     O     _     t     �     �     �     �     �  (   �  D   �     :     A     F     	                                                                         !       #             "                                        
           $                               100% @info:creditAuthor @info:creditCopyright 2015 Harald Sitter @info:creditHarald Sitter @labelNo Applications Playing Audio @labelNo Applications Recording Audio @labelNo Device Profiles Available @labelNo Input Devices Available @labelNo Output Devices Available @labelProfile: @titlePulseAudio @title:tabAdvanced @title:tabApplications @title:tabDevices Add virtual output device for simultaneous output on all local sound cards Advanced Output Configuration Automatically switch all running streams when a new output becomes available Capture Default Device Profiles EMAIL OF TRANSLATORSYour emails Inputs Mute audio NAME OF TRANSLATORSYour names Notification Sounds Outputs Playback Port Port is unavailable (unavailable) Port is unplugged (unplugged) Requires 'module-gconf' PulseAudio module This module allows to set up the Pulseaudio sound subsystem. label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-03 03:00+0200
PO-Revision-Date: 2017-10-14 07:29+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Lokalize 2.0
 100% Autor Copyright 2012-2014 Harald Sitter Harald Sitter Brak programów odtwarzających dźwięk Brak programów nagrywających dźwięk Brak profili urządzeń Brak urządzeń wejściowych Brak urządzeń wyjściowych Profil: PulseAudio Zaawansowane Programy Urządzenia Dodaj wirtualne urządzenie wyjścia dla jednoczesnego wyjścia na wszystkich lokalnych kartach dźwiękowych Zaawansowane ustawienia wyjścia Samoczynnie przełącz wszystkie działające strumienie, gdy nowe wyjście stanie się dostępne Przechwytywanie Domyślne Profile urządzeń lukasz.wojnilowicz@gmail.com Wejścia Wycisz dźwięk Łukasz Wojniłowicz Dźwięki powiadomień Wyjścia Odtwarzanie Złącze  (niedostępne)  (niepodłączone) Wymaga modułu 'module-gconf' PulseAudio Ten moduł umożliwia ustawienie podsystemu dźwiękowego Pulseaudio %1: %2 100% %1% 