��    L      |  e   �      p     q     u     �     �     �     �     �     �  )   �               !     7     H     e     l  	   �     �  4   �  9   �  ;     :   B     }     �     �     �     �     �     �     �  0   �      "	     C	  .   ]	     �	     �	     �	     �	     �	     �	     �	     �	     
     .
     G
     W
  
   h
     s
     x
     �
     �
     �
     �
     �
     �
  
   �
     �
     �
                    #     :     @     N     V  
   \  #   g     �     �     �  $   �     �     �     �  �       �  >   �  8     B   K     �  #   �  <   �  5     I   :  -   �  H   �  O   �  ?   K  `   �     �  h        q     �  �   �  {   A  ~   �  ~   <  (   �  u   �     Z     s  4   �  :   �  :   �     7  �   S  &   G  N   n  �   �     X  A   t  5   �     �       .     M   F  G   �  R   �  R   /  .   �  C   �  $   �          '  !   B     d  f   t  T   �     0  %   F     l     �  (   �  $   �  $   �       F   '     n  7   �     �     �  %   �  �     Y   �  .     '   A  Z   i     �     �  (   �           ,   <                 $       9       F   ?   7       I   B      J   5          A   L   .       &               !       8   G          K               H   /         '   >   %         	         D             -   :                  )          (   #   @   E   1       =   4   0                         C          *                 ;   3   "   +   2   6             
     ms &Border snap zone: &Center snap zone: &Enable hover &Focus &Moving &Titlebar Actions &Window snap zone: (c) 1997 - 2002 KWin and KControl Authors Activate Activate & Lower Activate & Pass Click Activate & Raise Activate, Raise & Pass Click Active Active screen follows &mouse Ad&vanced Alt Behavior on <em>double</em> click into the titlebar. Behavior on <em>left</em> click onto the maximize button. Behavior on <em>middle</em> click onto the maximize button. Behavior on <em>right</em> click onto the maximize button. Bernd Wuebben C&lick raises active window Cascade Centered Change Opacity Cristian Tibirna Daniel Molkentin Dela&y: Display window &geometry when moving or resizing EMAIL OF TRANSLATORSYour emails Handle mouse wheel events Hide utility windows for inactive applications Inactive Inactive Inner Window Keep Above/Below Left button: Lower Matthias Ettrich Matthias Hoelzer-Kluepfel Matthias Kalle Dalheimer Maximize (horizontal only) Maximize (vertical only) Maximize Button Maximize/Restore Maximizing Meta Middle button: Minimize Move Move to Previous/Next Desktop NAME OF TRANSLATORSYour names Nothing Operations Menu Pat Dowler Raise Raise/Lower Random Resize Right button: S&eparate screen focus Shade Shade/Unshade Shading Smart Snap Zones Snap windows onl&y when overlapping Toggle Raise & Lower Waldo Bastian Window Actio&ns Window Behavior Configuration Module Windows Wynn Wilkes Zero-Cornered Project-Id-Version: kcmkwm
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2009-01-16 22:16+0530
Last-Translator: Krishna Babu K <kkrothap@redhat.com>
Language-Team: Telugu <en@li.org>
Language: te
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=5; plural=n==1 ? 0 : n==2 ? 1 : n<7 ? 2 : n<11 ? 3 : 4;
X-Generator: KBabel 1.11.4
  మిసె (&B) సరిహద్దు స్నాప్ జోన్: (&C) కేంద్ర స్నాప్ జోన్: (&E) హోవర్‌ను చేతనముచేయుము (&F) ఫోకస్ (&M) కదులుచున్న (&T) శీర్షికపట్టీ చర్యలు (&W) విండో స్నాప్ జోన్: (c) 1997 - 2002 KWin మరియు KControl మూలకర్తలు క్రియాశీలముచేయి క్రియాశీలముచేయి & తగ్గించు క్రియాశీలముచేయి & పాస్ నొక్కు క్రియాశీలముచేయి & పెంచు క్రియాశీలముచేయి, పెంచు & పాస్ నొక్కు క్రియాశీల (&m) క్రియాశీల తెర మౌస్‌ను అనుసరిస్తుంది (&v)ఆధునికమైన ఆల్ట్ శీర్షకపట్టీ పైన <em>రెండు</em> నొక్కులు నొక్కినప్పుడు ప్రవర్తన. పెద్దదిచేసిన బటన్ పైన <em>ఎడమ</em> నొక్కు ప్రవర్తన. పెద్దదిచేసిన బటన్ పైన <em>మధ్య</em> నొక్కు ప్రవర్తన. పెద్దదిచేసిన బటన్ పైన <em>కుడి</em> నొక్కు ప్రవర్తన. బెరన్ట్ వుబెన్ (&l) రైజ్రైజ్ అయిన క్రియాశీల విండోను నొక్కుము కాస్కేడ్ సెంటర్డ్ ఓపాసిటీని మార్చుము క్రిస్టియన్ టిబిర్నా డానియల్ మాల్కెన్టిన్ (&y) ఆలస్యము: విండో జియోమెట్రీని కదుల్చుచున్నప్పుడు లేదా పునఃపరిమాణము చేయుచున్నప్పుడు ప్రదర్శించుము k.meetme@gmail.com,infyquest@gmail.com మౌస్ వీలు ఘటనలను సంభాలించుము క్రియాహీన అనువర్తనములకు వుపలభ్య విండోలను మరుగునవుంచుము క్రియాహీన క్రియాహీన అంతర్గత విండో పైకి/క్రిందకు వుంచు ఎడమ బటన్: తక్కువ మథయాస్ ఎట్ట్రిచ్ మథయాస్ హొయెల్జర్-క్లూప్ఫెల్ మాథియాస్ కెల్లే డాల్హైమర్ పెద్దదిచేయి (అడ్డముగా మాత్రమే) పెద్దదిచేయి (నిలువుగా మాత్రమే) బటన్ పెద్దదిచేయి పెద్దదిచేయి/తిరిగివుంచు పెద్దదిచేసిన మెటా మద్య బటన్: చిన్నదిచేయి కదుపు మునుపటి/తరువాతి రంగస్థలమునకు కదులుము కృష్ణబాబు కె , విజయ్ కిరణ్ కముజు ఏదీకాదు కార్యముల మెనూ పాట్ డౌలర్ పెంచు పెంచు/తగ్గించు యాధృశ్చికంగా పునఃపరిమాణము కుడి బటన్: (&e) తెర ఫోకస్‌ను వేరుచేయుము ఛాయ(షేడ్) షేడ్‌చేయి/షేడ్‌తీయి షేడింగ్ స్మార్ట్ స్నాప్ జోన్స్ (&y) విండోలు వొకదానిపై వొకటి వున్నప్పుడు మాత్రమే స్నాప్ చేయుము పెంచు & తగ్గించు అటుయిటు మార్చుము వాల్డొ బాస్టియన్ (&n) విండో చర్యలు విండో ప్రవర్తన ఆకృతీకరణ మాడ్యూల్ విండోలు విన్ వైక్స్ జోరో-కార్నర్డ్ 