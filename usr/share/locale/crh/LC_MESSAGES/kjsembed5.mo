��    )      d  ;   �      �  *   �     �     �     �          ,     F  >   L  B   �     �  9   �          %      ?     `     y     �  ;   �  -   �  )        C     \     z     �     �     �  !   �     �          1     L     h     �     �     �     �  (   �       &     &   @  �  g  -   
  %   D
      j
      �
     �
     �
  	   �
  ?   �
  C   &     j  <   �     �     �     �               =  :   S  >   �  .   �     �          0  "   G  &   j     �     �     �     �     �  &         E     f     �     �     �  $   �     �  #     &   /        %                     
       '                                                    (                                    !   )      "       $   #                    	                        &            %1 is not a function and cannot be called. %1 is not an Object type '%1' is not a valid QLayout. '%1' is not a valid QWidget. Action takes 2 args. ActionGroup takes 2 args. Alert Bad event handler: Object %1 Identifier %2 Method %3 Type: %4. Bad slot handler: Object %1 Identifier %2 Method %3 Signature: %4. Call to '%1' failed. Call to method '%1' failed, unable to get argument %2: %3 Confirm Could not construct value Could not create temporary file. Could not open file '%1' Could not open file '%1': %2 Could not read file '%1' Error encountered while processing include '%1' line %2: %3 Exception calling '%1' function from %2:%3:%4 Exception calling '%1' slot from %2:%3:%4 Failed to create Action. Failed to create ActionGroup. Failed to create Layout. Failed to create Widget. Failed to load file '%1' File %1 not found. First argument must be a QObject. Incorrect number of arguments. Must supply a filename. Must supply a layout name. Must supply a valid parent. Must supply a widget name. No classname specified No classname specified. No such method '%1'. Not enough arguments. There was an error reading the file '%1' Wrong object type. include only takes 1 argument, not %1. library only takes 1 argument, not %1. Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2009-02-12 23:29-0600
Last-Translator: Reşat SABIQ <tilde.birlik@gmail.com>
Language-Team: Qırımtatarca (Qırım Türkçesi)
Language: crh
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: KBabel 1.11.4
 %1 bir funktsiya degildir ve çağırılamaz. %1 Nesne (Object) türünden degildir '%1' keçerli bir QLayout degil. '%1' keçerli bir QWidget degil. Amel 2 delil alır. AmelZümresi 2 delil alır. İhtar Et Fena vaqia qollayıcısı: Nesne %1 Kimlik %2 Usul %3 Tür: %4. Fena közenek qollayıcısı: Nesne %1 Kimlik %2 Usul %3 İmza: %4. '%1' çağıruvı oñmadı. '%1' usulına çağıruv oñmadı, %2 delili alınamadı: %3 Teyit Et Qıymet inşa etilamadı Muvaqqat dosye icat etilamadı. '%1' dosyesi açılamadı '%1' dosyesi açılamadı: %2 Dosye oqulamadı '%1' '%1' kirsetmesini işlegende hata yaşandı; satır %2: %3 '%1' funktsiyasını %2:%3:%4 qonumından çağıruvda istisna %2:%3:%4 qonumından '%1' çağıruvda istisna Amel icat etilamadı. AmelZümresi icat etilamadı. Serim icat etilamadı. Pencereçikni icat etüv oñmadı. '%1' dosyesiniñ yüklenüvi oñmadı %1 dosyesi tapılamadı. İlk delil bir QObject olmalı. Yañlış delil sayısı. Dosye ismi temin etilmeli. Bir serim ismi temin etilmeli. Keçerli bir üst-tür temin etilmeli. Pencereçik ismi temin etilmeli. Sınıf ismi belirtilmegen Sınıf ismi belirtilmegen. '%1' kibi usul yoqtır. Delil sayısı yetersiz. Dosye oqulğanda hata yaşandı '%1' Yañlış nesne türü. kirsetme tek 1 delil ala, %1 degil. kitaphane tek 1 delilni ala, %1 degil. 