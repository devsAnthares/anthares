��          �      |      �     �     �     �                 ;  	   \     f     �  &   �     �     �     �  	          �   %  �   �  t   ?  7   �  +   �       �                   !   #  "   E     h     }     �     �  +   �     �     �          &  	   -  �   7  �   �  �   �	     
  :   "
  
   ]
                                      
   	                                                         min Act like Always Define a special behavior Don't use special settings EMAIL OF TRANSLATORSYour emails Hibernate NAME OF TRANSLATORSYour names Never shutdown the screen Never suspend or shutdown the computer PC running on AC power PC running on battery power PC running on low battery Shut down Suspend The Power Management Service appears not to be running.
This can be solved by starting or scheduling it inside "Startup and Shutdown" The activity service is not running.
It is necessary to have the activity manager running to configure activity-specific power management behavior. The activity service is running with bare functionalities.
Names and icons of the activities might not be available. This is meant to be: Act like activity %1Activity "%1" Use separate settings (advanced users only) after Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-09 03:03+0200
PO-Revision-Date: 2017-10-14 22:07+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
  min Comportarse como Sempre Definir un comportamento especial Non usar configuracións especiais mvillarino@gmail.com Hibernar Marce Villarino Non apagar nunca a pantalla Non suspender nin apagar nunca o computador PC conectado á corrente PC gastando as baterías PC con pouca batería Apagar Suspender Semella que o servizo de xestión da enerxía non se está a executar.
Isto pode arranxarse iniciando ou planificándoo en «Arrinque e apagado» O servizo da actividade non está dispoñíbel. É preciso que o xestor de actividades estea a executarse para configurar a xestión da enerxía específica de cada unha das actividades. Este servizo das actividades está aexecutarse coa mínima funcionalidade.
Os nomes e iconas das actividades poden non estar dispoñíbeis. Actividade «%1» Usar configuracións por separado (só usuarios avanzados) despois de 