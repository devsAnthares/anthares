��    =        S   �      8  ;   9     u     �     �     �  6   �  A   
     L     U  $   Y  
   ~     �  #   �     �     �     �     �     �  7        >     [     w  '   �     �     �  $   �  ,   �          3     C     K     ]     y     �     �     �     �     �  �   �  9   �	  "   �	     �	     �	     
     *
     <
     R
     c
  %   u
     �
     �
     �
     �
     �
  
   �
  7        :     T     l     t  �  �  G   �  '   �  "   �       &   &     M     [     y     �  '   �  	   �  "   �  @   �          %     D     T     o  /   }  +   �  %   �     �  5        >     Z  +   l  3   �     �     �     �            !     ?     K     X     l     �     �  �   �  W   �  /   �  
     1         L  "   m  *   �  $   �     �  $   �  
     &   (     O     o     t  
   z  !   �     �     �     �     �     !          3      0      (                 1      "          =   .   6                                         	      :          5       ,   #   &   7       ;   '             -   
             8                 *           %           <   4   9                )   /   $          2             +       

Choose the previous strip to go to the last cached strip. &Create Comic Book Archive... &Save Comic As... &Strip Number: *.cbz|Comic Book Archive (Zip) @option:check Context menu of comic image&Actual Size @option:check Context menu of comic imageStore current &Position Advanced All An error happened for identifier %1. Appearance Archiving comic failed Automatically update comic plugins: Cache Check for new comic strips: Comic Comic cache: Configure... Could not create the archive at the specified location. Create %1 Comic Book Archive Creating Comic Book Archive Destination: Display error when getting comic failed Download Comics Error Handling Failed adding a file to the archive. Failed creating the file with identifier %1. From beginning to ... From end to ... General Get New Comics... Getting comic strip failed: Go to Strip Information Jump to &current Strip Jump to &first Strip Jump to Strip ... Manual range Maybe there is no Internet connection.
Maybe the comic plugin is broken.
Another reason might be that there is no comic for this day/number/string, so choosing a different one might work. Middle-click on the comic to show it at its original size No zip file is existing, aborting. Range: Show arrows only on mouse over Show comic URL Show comic author Show comic identifier Show comic title Strip identifier: The range of comic strips to archive. Update Visit the comic website Visit the shop &website an abbreviation for Number# %1 days dd.MM.yyyy here strip means comic strip&Next Tab with a new Strip in a range: from toFrom: in a range: from toTo: minutes strips per comic Project-Id-Version: plasma_applet_comic
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-07-18 03:20+0200
PO-Revision-Date: 2017-11-05 09:59+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 

Escolla ir á banda anterior para acceder á última banda da caché. &Crear un arquivo de banda deseñada… &Gardar a banda deseñada como.… &Número da banda: *.cbz|Arquivo de banda deseñada (zip) Tamaño &real Almacenar a &posición actual Avanzado Todas Produciuse un erro co identificador %1. Aparencia Arquivar a banda deseñada fallou. Actualizar automaticamente os complementos de bandas deseñadas: Caché Comprobar se hai novas bandas: Banda deseñada Caché da banda deseñada: Configurar… Non se puido crear o arquivo no lugar indicado. Crear un arquivo de banda deseñada para %1 Creando un arquivo de banda deseñada Destino: Mostrar o erro ao descargar a banda deseñada fallou. Descargar bandas deseñadas Xestión de erros Engadir un dos ficheiros ao arquivo fallou. A creación do ficheiro co identificador %1 fallou. Desde o principio ata… Desde o final ata… Xeral Obter bandas deseñadas novas… A obtención da banda fallou: Ir á banda Información Ir á banda a&ctual Ir á &primeira banda Ir á banda… Intervalo manual Pode que non teña acceso a internet.
Pode que o complemento da banda deseñada xa non funcione.
Tamén pode ser que non haxa unha banda para este día, número ou texto; en tal caso, debería poder escoller unha banda distinta. Prema a banda deseñada co botón central do rato para mostrala co seu tamaño orixinal Non existe ningún ficheiro zip, interrompendo. Intervalo: Mostrar as &frechas só ao pasar o rato por riba: Mostrar o URL da banda deseñada Mostrar o autor da banda deseñada Mostrar o identificador da banda deseñada Mostrar o título da banda deseñada Identificador da banda: O intervalo de bandas para arquivar. Actualizar Visitar o sitio web da banda deseñada Visitar o sitio &web de compras # %1 días dd/MM/yyyy &Seguinte lapela cunha nova banda Desde: Ata: minutos bandas por banda deseñada 