��    8      �  O   �      �  A   �          2  /   I  8   y     �     �     �     �     �                      $   ,  	   Q     [  !   q  3   �  	   �     �      �  $   �       *   /     Z  	   _  5   i     �     �  
   �     �     �  	   �          !     @  5   O  3   �  6   �     �  ,   �  /   )	  	   Y	     c	     z	     �	     �	  O   �	  Z   �	  b   J
  	   �
  
   �
  P   �
       �  #  7        Q     g  5   |  $   �     �     �     �  !        .     F     ^     d     k  +   x  
   �     �  '   �  2   �          -      6  )   W     �     �     �     �  A   �     �       
   -     8     E     T     a     �     �  C   �  D   �  ?   7     w  !   �     �  	   �     �     �     �     �  ^     q   u  V   �     >     O  O   _     �                                            ,   	          #       )   $                                         +   (   &   1            7   4                         6           8   
   *   .             /       %      3   !           0   2      "            -       '           5        %1 is an external application and has been automatically launched (c) 2009, Ben Cooksley (c) 2017, Marco Martin <i>Contains 1 item</i> <i>Contains %1 items</i> A search yelded no resultsNo items matching your search About %1 About Active Module About Active View About System Settings All Settings Apply Settings Author Back Ben Cooksley Central configuration center by KDE. Configure Configure your system Contains 1 item Contains %1 items Determines whether detailed tooltips should be used Developer Dialog EMAIL OF TRANSLATORSYour emails Expand the first level automatically Frequently used: General config for System SettingsGeneral Help Icon View Internal module representation, internal module model Internal name for the view used Keyboard Shortcut: %1 Maintainer Marco Martin Mathias Soeken Most Used Most used module number %1 NAME OF TRANSLATORSYour names No views found Provides a categorized icons view of control modules. Provides a categorized sidebar for control modules. Provides a classic tree-based view of control modules. Relaunch %1 Reset all current changes to previous values Search through a list of control modulesSearch Search... Show detailed tooltips Sidebar Sidebar View System Settings System Settings was unable to find any views, and hence has nothing to display. System Settings was unable to find any views, and hence nothing is available to configure. The settings of the current module have changed.
Do you want to apply the changes or discard them? Tree View View Style Welcome to "System Settings", a central place to configure your computer system. Will Stephenson Project-Id-Version: systemsettings
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:08+0100
PO-Revision-Date: 2018-01-19 19:55+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 %1 é un aplicativo externo e iniciouse automaticamente © 2009, Ben Cooksley © 2017 Marco Martin <i>Contén 1 elemento</i> <i>Contén %1 elementos</i> Ningún elemento coincide coa busca. Sobre %1 Sobre o módulo activo Sobre a vista activa Sobre a Configuración do Sistema A toda a configuración Aplica a configuración Autor Atrás Ben Cooksley Centro principal de configuración por KDE. Configurar Configurar o sistema Contén 1 elemento Contén %1 elementos Determina se deben empregarse axudiñas detalladas Desenvolvedor Diálogo mvillarino@users.sourceforge.net Expandir automaticamente o primeiro nivel Usados frecuentemente: Xeral Axuda Vista con iconas Representación interna dos módulos, modelo interno dos módulos Nome interno do visor empregado Atallo de teclado: %1 Mantenedor Marco Martin Mathias Soeken Máis usados Módulo máis usado número %1. Marce Villarino Non se atopou ningunha vista Fornece unha vista de iconas dos módulos de control categorizadas. Fornece unha barra lateral categorizada para os módulos de control. Fornece unha vista en árbore clásica dos módulos de control. Iniciar %1 de novo Desfacer todos os cambios actuais Buscar Buscar… Mostrar axudas detalladas Barra lateral Vista da barra lateral Configuración do sistema A configuración do sistema non puido atopar ningunha vista, polo que non hai ren que mostrar. A configuración do sistema non puido atopar ningunha vista, e polo tanto non se dispón de nada para configurar. Cambiouse a configuración do módulo activo.
Quere aplicar os cambios ou descartalos? Vista en árbore Estilo da vista Benvido á «Configuración do Sistema», o lugar onde configurar o computador. Will Stephenson 