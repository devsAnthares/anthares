��    *      l  ;   �      �     �     �  )   �               /     H     ^  #   ~     �  
   �     �     �  %   �  +        E     Z     m  @   z     �     �     �     �               '     ,     9     ?     _     e  .   m     �  F   �  (   �  	   '  	   1  	   ;  '   E  7   m  "   �  �  �     �	     �	  %   �	     �	     �	  
   �	     �	     �	     
     &
     <
     H
     c
  !   x
  C   �
     �
     �
       P        p     �     �     �      �     �     �     �     �  "        (     /  6   ;     r  M   �  &   �                    )     1     =                   $                                 %                      *                                               	   #   )   "             &           
                        '             (   !    &Do not remember @actionWalk through activities @actionWalk through activities (Reverse) @action:buttonApply @action:buttonCancel @action:buttonChange... @action:buttonCreate @title:windowActivity Settings @title:windowCreate a New Activity @title:windowDelete Activity Activities Activity information Activity switching Are you sure you want to delete '%1'? Blacklist all applications not on this list Clear recent history Create activity... Description: Error loading the QML files. Check your installation.
Missing %1 For a&ll applications Forget a day Forget everything Forget the last hour Forget the last two hours General Icon Keep history Name: O&nly for specific applications Other Privacy Private - do not track usage for this activity Remember opened documents: Remember the current virtual desktop for each activity (needs restart) Shortcut for switching to this activity: Shortcuts Switching Wallpaper for in 'keep history for 5 months'for  unit of time. months to keep the history month  months unlimited number of monthsforever Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-04-21 07:31+0100
Last-Translator: Adrián Chaves Fernández (Gallaecio) <adriyetichaves@gmail.com>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 &Non lembrar Navegar polas actividades Navegar polas actividades (ao revés) Aplicar Cancelar Cambiar… Crear Configuración da actividade Crear unha actividade Eliminar a actividade Actividades Información da actividade Cambio de actividade Seguro que quere eliminar «%1»? Pór na lista negra todos os aplicativos que non estean nesta lista Limpar o historial recente Crear unha actividade… Descrición: Produciuse un erro ao cargar os ficheiro de QML. Revise a instalación.
Falta %1 En &calquera aplicativo Esquecer un día Esquecer todo Esquecer a última hora Esquecer as últimas dúas horas Xeral Icona Manter o historial Nome: &Só para aplicativos específicos Outros Privacidade Privada. Non facer seguimento do uso desta actividade. Lembrar os documentos abertos: Lembrar o escritorio virtual actual para cada actividade (require reiniciar). Atallo para cambiar a esta actividade: Atallos Cambio Fondo de escritorio durante  mes  meses para sempre 