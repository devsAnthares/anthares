��    	      d      �       �      �      �   -        <  -   V  #   �  #   �     �  �  �     �     �  .   �     '  .   =     l     q  
   v                                   	           Current time is %1 Displays the current date Displays the current date in a given timezone Displays the current time Displays the current time in a given timezone Note this is a KRunner keyworddate Note this is a KRunner keywordtime Today's date is %1 Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2015-03-15 14:56+0100
Last-Translator: Adrián Chaves Fernández <adriyetichaves@gmail.com>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 A hora actual é %1 Mostra a data actual. Mostra a data actual no fuso horario indicado. Mostra a hora actual. Mostra a hora actual no fuso horario indicado. data hora Hoxe é %1 