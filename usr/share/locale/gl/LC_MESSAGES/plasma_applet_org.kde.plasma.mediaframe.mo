��            )   �      �     �     �     �     �     �     �     �  
           )   (     R     V     \     c     v     �     �     �  3   �     �       <   #  5   `  8   �  8   �                    /  �  1     �     �               *     ?     R     l     �  ;   �     �     �     �     �  +   �  +   "     N     c  1   o     �  )   �  e   �  M   L	  :   �	  :   �	     
     
     -
     ?
                                                  
                                                                               	        Add files... Add folder... Background frame Change picture every Choose a folder Choose files Configure plasmoid... Fill mode: General Left click image opens in external viewer Pad Paths Paths: Pause on mouseover Preserve aspect crop Preserve aspect fit Randomize items Stretch The image is duplicated horizontally and vertically The image is not transformed The image is scaled to fit The image is scaled uniformly to fill, cropping if necessary The image is scaled uniformly to fit without cropping The image is stretched horizontally and tiled vertically The image is stretched vertically and tiled horizontally Tile Tile horizontally Tile vertically s Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-16 03:31+0100
PO-Revision-Date: 2018-01-18 21:49+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Engadir ficheiros… Engadir un cartafol… Marco do fondo Cambiar a imaxe cada Escoller un cartafol Escoller ficheiros Configurar o plasmoide… Modo de enchemento: Xeral Premer a imaxe co botón esquerdo ábrea nun visor externo. Centrar Rutas Rutas: Pausa ao pasar o rato por riba Axustar mantendo as proporcións e cortando Axustar mantendo as proporcións sen cortar Elementos aleatorios Alongamento Duplicar a imaxe horizontalmente e verticalmente. A imaxe non se transforma. Cambiar o tamaño da imaxe para axustala. Cámbiase o tamaño da imaxe de maneira uniforme para axustala, e recortala en caso de ser necesario. Cámbiase o tamaño da imaxe de maneira uniforme para axustala sen recortala. Alongar a imaxe horizontalmente e duplicala verticalmente. Alongar a imaxe verticalmente e duplicala horizontalmente. Teselado Teselado horizontal Teselado vertical s 