��          �      \      �     �     �  	   �  $   �          ,     C     ^     j  	   y  
   �     �     �     �     �     �  "   �  	   �  '   �  �  &     �     �     �  (     )   -  %   W     }     �     �     �     �     �     �  	   �     �       #   '     K  .   \                      
            	                                                                 / Authentication Base Dir: Could not create the new request:
%1 Could not get reviews list Could not set metadata Could not upload the patch Destination JSON error: %1 Password: Repository Repository: Request Error: %1 Server: Update Review: Update review User name in the specified service Username: Where this project was checked out from Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:19+0100
PO-Revision-Date: 2015-10-24 11:39+0100
Last-Translator: Adrián Chaves Fernández (Gallaecio) <adriyetichaves@gmail.com>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 / Autenticación Directorio base: Non se puido crear a solicitude nova:
%1 Non se puido obter a lista das revisións Non se puideron definir os metadatos. Non se puido enviar o parche Destino Erro de JSON: %1 Contrasinal: Repositorio Repositorio: Erro de solicitude: %1 Servidor: Actualizar a revisón: Actualizar a revisón Nome de usuario no servizo indicado Nome de usuario: De onde se descargou (check out) este proxecto 