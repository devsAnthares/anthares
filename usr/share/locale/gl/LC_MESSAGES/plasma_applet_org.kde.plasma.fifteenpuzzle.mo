��          �            x  
   y  	   �     �     �  3   �     �     �                    %     *     F  B   Y     �    �  	   �     �     �     �  :   �     3     D     `     n  
   �     �  -   �     �  	   �     �     	                                                  
                        Appearance Browse... Choose an image Fifteen Puzzle Image Files (*.png *.jpg *.jpeg *.bmp *.svg *.svgz) Number color Path to custom image Piece color Show numerals Shuffle Size Solve by arranging in order Solved! Try again. The time since the puzzle started, in minutes and secondsTime: %1 Use custom image Project-Id-Version: plasma_applet_fifteenPuzzle
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-01-12 03:59+0100
PO-Revision-Date: 2017-07-29 11:06+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Aparencia Examinar… Escoller unha imaxe Quebracabezas de quince pezas Ficheiros de imaxe (*.png *.jpg *.jpeg *.bmp *.svg *.svgz) Cor dos números Ruta da imaxe personalizada Cor das pezas Mostrar os números Desordenar Tamaño Ordene as pezas para resolver o quebracabezas Resolveuno! Inténteo de novo. Tempo: %1 Usar unha imaxe personalizada 