��    
      l      �       �   
   �      �        .   0     _     t     �  (   �  8   �  �       �  %   �     �  7   �  !   7     Y     b     k     |        
                             	        Disk Quota No quota restrictions found. Please install 'quota' Quota tool not found.

Please install 'quota'. Running quota failed e.g.: 12 GiB of 20 GiB%1 of %2 e.g.: 8 GiB free%1 free example: Quota: 83% usedQuota: %1% used usage of quota, e.g.: '/home/bla: 38% used'%1: %2% used Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2017-11-05 09:58+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Cota de espazo Non se atoparon restricións de cota. Instale «quota» Non se atopou a ferramenta de cota.

Instale «quota». A execución de «quota» fallou. %1 de %2 %1 libre Cota: %1% en uso %1: %2% usado 