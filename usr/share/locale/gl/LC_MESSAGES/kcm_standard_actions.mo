��          L      |       �       �      �   #   �        �      �  �     �     �  !        .  �   K                                         EMAIL OF TRANSLATORSYour emails NAME OF TRANSLATORSYour names Standard Actions successfully saved Standard Shortcuts The changes have been saved. Please note that:<ul><li>Applications need to be restarted to see the changes.</li>    <li>This change could introduce shortcut conflicts in some applications.</li></ul> Project-Id-Version: kcm_standard_actions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2017-07-28 09:58+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 mvillarino@kde-espana.es Marce Villarino As accións estándar gardáronse Atallos de teclado estándar Gardáronse os cambios. Lembre que:<ul><li>Os aplicativos deben reiniciarse para ver os cambios.</li> <li>Este cambio pode introducir conflitos de atallos nalgúns aplicativos.</li></ul> 