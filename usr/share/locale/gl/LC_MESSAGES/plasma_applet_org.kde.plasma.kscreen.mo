��    
      l      �       �   L   �   +   >  #   j     �  P   �  	   �  (     I   -  K   w  �  �      s  
   �  ,   �  $   �     �     
          3     F                                      	   
    %1 is name of the newly connected displayA new display %1 has been detected Disables the newly connected screenDisable Failed to connect to KScreen daemon Failed to load root object Makes the newly conencted screen a clone of the primary oneClone Primary Output No Action Opens KScreen KCMAdvanced Configuration Places the newly connected screen left of the existing oneExtend to Left Places the newly connected screen right of the existing oneExtend to Right Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2015-03-15 14:36+0100
Last-Translator: Adrián Chaves Fernández <adriyetichaves@gmail.com>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 Detectouse unha nova pantalla %1 Desactivar Non se puido conectar co servizo de KScreen. Non se puido cargar o obxecto raíz. Clonar a saída primaria Ningunha acción Configuración avanzada Situar á esquerda Situar á dereita 