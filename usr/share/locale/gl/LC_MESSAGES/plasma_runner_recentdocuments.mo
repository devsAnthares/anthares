��          4      L       `   :   a      �   �  �   L   �     �                    Looks for documents recently used with names matching :q:. Open Containing Folder Project-Id-Version: krunner_recentdocuments
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-12-17 03:48+0100
PO-Revision-Date: 2017-10-17 19:45+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Busca documentos empregados recentemente que teñan nomes que casen con :q:. Abrir o cartafol contedor 