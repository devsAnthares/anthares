��          �   %   �      P  �  Q          )  6   9  -   p     �     �     �     �  (   �  d        x     �     �     �     �     �     �                7  "   X     {     �     �  �  �  �  �     w
     �
  ;   �
  (   �
               +     B  1   V  ^   �     �     �               !     (     .     4  	   A  	   K     U     \     o     w                               	                                                                      
                        <p>You have chosen to open another desktop session.<br />The current session will be hidden and a new login screen will be displayed.<br />An F-key is assigned to each session; F%1 is usually assigned to the first session, F%2 to the second session and so on. You can switch between sessions by pressing Ctrl, Alt and the appropriate F-key at the same time. Additionally, the KDE Panel and Desktop menus have actions for switching between sessions.</p> Lists all sessions Lock the screen Locks the current sessions and starts the screen saver Logs out, exiting the current desktop session New Session Reboots the computer Restart the computer Shutdown the computer Starts a new session as a different user Switches to the active session for the user :q:, or lists all active sessions if :q: is not provided Turns off the computer User sessionssessions Warning - New Session lock screen commandlock log out log out commandLogout log out commandlogout new session restart computer commandreboot restart computer commandrestart shutdown computer commandshutdown switch user switch user commandswitch switch user commandswitch :q: Project-Id-Version: krunner_sessions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2018-02-01 20:07+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 <p>Escolleu abrir outra sesión de escritorio.<br />A sesión actual ha agocharse e mostrarase unha nova pantalla de acceso.<br />Hai unha tecla de función asignada a cada sesión: F%1 polo xeral asígnase á primeira sesión, F%2 á segunda, e así en diante. Poderá cambiar de sesión premendo Ctrl, Alt, e a tecla de función axeitada á vez. Ademais, o panel de KDE e os menús de escritorio teñen accións para saltar entre sesións.</p> Listar todas as sesións Trancar a pantalla Tranca as sesións actuais e inicia o protector da pantalla Sae, pechando esta sesión do escritorio Nova sesión Reinicia o computador Reiniciar o computador Apagar o computador Inicia unha sesión nova como un usuario distinto Vai á sesión activa do usuario :q:, ou lista todas as sesións activas de non fornecerse :q: Apaga o computador sesións Aviso de nova sesión trancar saída Saír saír nova sesión reiniciar reiniciar apagar cambiar de usuario cambiar cambiar a :q: 