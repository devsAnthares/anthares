��    7      �  I   �      �     �     �     �     �     �     �               $     -     5     H     T      `     �     �     �     �     �     �     �     �                     )     7  	   C  	   M     W     d     j     �     �     �     �     �     �     �     �     �     �  @   �     7     @     \     c     j     r     �     �     �  4   �     �  �  �     �	     �	     �	  #   �	     �	  
   �	     �	     �	     �	     
     
     
     
     .
     ?
     H
  $   \
     �
     �
     �
     �
     �
     �
     �
     �
               (     9  
   F     Q     X     r  
   v     �     �     �  
   �     �     �     �     �  P   �  	   !  #   +     O     X     Z     b     �     �     �     �     �     &   0      7                  #                
          	   '   (   +                       6              3      1                   ,   2                  "       *          $      4   )               %      5                     !          /   .   -       % %1 is value, %2 is unit%1 %2 %1: Application Energy Consumption Battery Capacity Charge Percentage Charge state Charging Current Degree Celsius°C Details: %1 Discharging EMAIL OF TRANSLATORSYour emails Energy Energy Consumption Energy Consumption Statistics Environment Full design Fully charged Has power supply Kai Uwe Broulik Last 12 hours Last 2 hours Last 24 hours Last 48 hours Last 7 days Last full Last hour Manufacturer Model NAME OF TRANSLATORSYour names No Not charging PID: %1 Path: %1 Rechargeable Refresh Serial Number Shorthand for WattsW System Temperature This type of history is currently not available for this device. Timespan Timespan of data to display Vendor VoltV Voltage Wakeups per second: %1 (%2%) WattW Watt-hoursWh Yes current power draw from the battery in WConsumption literal percent sign% Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2017-07-25 11:00+0100
Last-Translator: Adrián Chaves <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 % %1 %2 %1: Consumo enerxético dos aplicativos Batería Capacidade Porcentaxe de carga Estado da carga Cargándose Corrente °C Detalles: %1 Descargándose adrian@chaves.io Enerxía Consumo enerxético Estatísticas de consumo enerxético Ambiente Deseño completo Carga completa Conectado á corrente Kai Uwe Broulik Últimas 12 horas Últimas 2 horas Últimas 24 horas Últimas 48 horas Últimos 7 días Última completa Última hora Fabricante Modelo Adrián Chaves Fernández Non Sen cargar PID: %1 Ruta: %1 Recargábel Actualizar Número de serie W Sistema Temperatura Este tipo de historial non está dispoñíbel actualmente para este dispositivo. Duración Duración dos datos que se mostran. Vendedor V Voltaxe Sacudidas por segundo: %1 (%2%) W Wh Si Consumo % 