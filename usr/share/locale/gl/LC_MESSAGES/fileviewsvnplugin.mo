��          �   %   �      p     q  +   �  .   �  6   �  *     #   D  &   h  /   �  2   �  :   �  0   -  3   ^  ;   �  K   �  -     $   H  '   m     �     �     �     �     �  #        1     O     c     |  �  �     q  -   y  )   �  /   �  &   	     (	     H	  -   g	  )   �	  6   �	  .   �	  -   %
  3   S
  d   �
  .   �
       !   ;     ]     k     |     �     �     �     �     �     �                                                         	   
                                                                    @action:buttonCommit @info:statusAdded files to SVN repository. @info:statusAdding files to SVN repository... @info:statusAdding of files to SVN repository failed. @info:statusCommit of SVN changes failed. @info:statusCommitted SVN changes. @info:statusCommitting SVN changes... @info:statusRemoved files from SVN repository. @info:statusRemoving files from SVN repository... @info:statusRemoving of files from SVN repository failed. @info:statusReverted files from SVN repository. @info:statusReverting files from SVN repository... @info:statusReverting of files from SVN repository failed. @info:statusSVN status update failed. Disabling Option "Show SVN Updates". @info:statusUpdate of SVN repository failed. @info:statusUpdated SVN repository. @info:statusUpdating SVN repository... @item:inmenuSVN Add @item:inmenuSVN Commit... @item:inmenuSVN Delete @item:inmenuSVN Revert @item:inmenuSVN Update @item:inmenuShow Local SVN Changes @item:inmenuShow SVN Updates @labelDescription: @title:windowSVN Commit Show updates Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2017-11-05 09:46+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Remitir Engadíronse os ficheiros ao repositorio SVN. Engadindo ficheiros ao repositorio SVN… Engadir ficheiros ao repositorio de SVN fallou. A remisión dos cambios de SVN fallou. Remitíronse os cambios de SVN. Remitindo os cambios de SVN… Retiráronse os ficheiros do repositorio SVN. Retirando ficheiros do repositorio SVN… A retirada dos ficheiros do repositorio de SVN fallou. Revertéronse os ficheiros do repositorio SVN. Revertendo os ficheiros do repositorio SVN… Reverter os ficheiros do repositorio de SVN fallou. A actualización do estado de SVN fallou. Desactivando a opción «Mostrar actualizacións de SVN». A actualización do repositorio de SVN fallou. Actualizouse o repositorio SVN. Actualizando o repositorio SVN… Engadir (SVN) Remitir (SVN)… Eliminar (SVN) Reverter (SVN) Actualizar (SVN) Mostrar os cambios locais (SVN) Mostrar actualizacións (SVN) Descrición: Remisión a SVN Mostrar actualizacións 