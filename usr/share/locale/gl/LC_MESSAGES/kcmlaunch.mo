��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5    K     O  /   T  �  �  �  <	  `   �
     0     D     U  *   e     �     �  /   �  "   �                                          
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2017-02-02 21:28+0100
Last-Translator: Adrián Chaves Fernández (Gallaecio) <adriyetichaves@gmail.com>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
  seg Tempo de e&xpiración da indicación do inicio: <H1>Notificación na barra de tarefas</H1>
Pode activar un segundo método de notificación de inicio que
é usado pola barra de tarefas onde aparece un botón cun disco
en rotación, simbolizando que o aplicativo iniciado está cargándose.
Pode ocorrer que algúns aplicativos non reciban esta
notificación de inicio. Neste caso, o botón desaparecerá
despois do tempo estabelecido en «Tempo de expiración da indicación de inicio». <h1>Cursor ocupado</h1>
KDE ofrece un cursor ocupado para notificar o inicio dun aplicativo.
Para activar o cursor ocupado, seleccione no despregábel o tipo de información visual.
Pode ocorrer que algúns aplicativos non reciban esta notificación
de inicio. Neste caso, o cursor deixa de pestanexar despois de transcorrido
o tempo que figura na sección «Tempo de espera da indicación de inicio». <h1>Notificación de inicio</h1> Aquí pode configurar a notificación de inicio dun aplicativo. Cursor pestanexante Cursor elástico Cursor ocu&pado Activar notificación na barra de &tarefas Sen cursor de ocupado Cursor ocupado pasivo Tempo de e&xpiración da indicación de inicio: &Notificación na barra de tarefas 