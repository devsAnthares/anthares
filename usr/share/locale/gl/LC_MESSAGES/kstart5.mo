��            )   �      �  0   �  .   �  s  �  X   e     �     �  *   �           )  H   <     �  &   �  S   �                ,     M     l     �  8   �     �     �  /     -   1  S   _  *   �  *   �  �   	  �  �  /   �
  9   �
  �  �
  f   �     �       (     ?   B     �  ?   �     �  2   �  W        h     y  "   �      �  +   �     �  F        \  $   m  (   �  *   �  |   �  1   c  2   �  �   �              
                                        	                                                                                    (C) 1997-2000 Matthias Ettrich (ettrich@kde.org) A regular expression matching the window title A string matching the window class (WM_CLASS property)
The window class can be found out by running
'xprop | grep WM_CLASS' and clicking on a window
(use either both parts separated by a space or only the right part).
NOTE: If you specify neither window title nor window class,
then the very first window to appear will be taken;
omitting both options is NOT recommended. Alternative to <command>: desktop file to start. D-Bus service will be printed to stdout Command to execute David Faure Desktop on which to make the window appear EMAIL OF TRANSLATORSYour emails Iconify the window Jump to the window even if it is started on a 
different virtual desktop KStart Make the window appear on all desktops Make the window appear on the desktop that was active
when starting the application Matthias Ettrich Maximize the window Maximize the window horizontally Maximize the window vertically NAME OF TRANSLATORSYour names No command specified Optional URL to pass <desktopfile>, when using --service Richard J. Moore Show window fullscreen The window does not get an entry in the taskbar The window does not get an entry on the pager The window type: Normal, Desktop, Dock, Toolbar, 
Menu, Dialog, TopMenu or Override Try to keep the window above other windows Try to keep the window below other windows Utility to launch applications with special window properties 
such as iconified, maximized, a certain virtual desktop, a special decoration
and so on. Project-Id-Version: kstart
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-01-07 03:59+0100
PO-Revision-Date: 2017-08-09 22:55+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <proxecto@trasno.net>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 © 1997-2000 Matthias Ettrich (ettrich@kde.org) Unha expresión regular que coincida co título da xanela Unha cadea coincidente coa clase da xanela (propiedade WM_CLASS)
A clase da xanela pode atoparse executando
«xprop | grep WM_CLASS» e premendo nunha xanela
(use tanto ambas as dúas partes separadas por un espazo ou
só a parte da dereita).
NOTA: Se non especifica nin un título para a xanela nin unha
clase de xanela, entón tomarase a primeira xanela en aparecer;
non se recomenda omitir ambas as opcións. Alternativa a <command>: o ficheiro de escritorio para iniciar. O servizo D-Bus imprimirase en stdout. A orde a executar David Faure O escritorio onde debe aparecer a xanela xabigf@gmx.net,
jba@pobox.com,
mvillarino@users.sourceforge.net Iconificar a xanela Ir á xanela mesmo de iniciarse nun
escritorio virtual distinto KStart Facer que a xanela apareza en todos os escritorios Facer que a xanela apareza no escritorio que estea activo
cando se execute o aplicativo Matthias Ettrich Maximizar a xanela Maximizar a xanela horizontalmente Maximizar a xanela verticalmente Xabi García, Jesús Bravo, Marce Villarino Non se indicou unha orde Un URL opcional a pasar a <desktopfile> cando se emprega con --service Richard J. Moore Mostrar a xanela a pantalla completa A xanela non aparece na barra de tarefas A xanela non ten unha entrada no paxinador O tipo de xanela: Normal, Escritorio, Acoplada, Barra de ferramentas,
Menú, Diálogo, Menú de escritorio ou Sobreposición Intentar manter a xanela por riba doutras xanelas Intentar manter a xanela por baixo doutras xanelas Utilidade para iniciar aplicativos con xanelas con propiedades especiais,
como iconificada, maximizada, nun determinado escritorio virtual,
cunha decoración especial, e outras. 