��    	      d      �       �   
   �      �      �   	              "     C  "   ]     �  	   �     �     �     �     �  /   �  )      1   *                             	                 Appearance Colors: Display seconds Draw grid Show inactive LEDs: Use custom color for active LEDs Use custom color for grid Use custom color for inactive LEDs Project-Id-Version: plasma_applet_binaryclock
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-31 05:47+0100
PO-Revision-Date: 2017-07-29 11:06+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Aparencia Cores: Mostrar os segundos. Mostrar a grade Mostrar os LED apagados: usar unha cor personalizada para os LED acesos. Usar unha cor personalizada para a grade. Usar unha cor personalizada para os LED apagados. 