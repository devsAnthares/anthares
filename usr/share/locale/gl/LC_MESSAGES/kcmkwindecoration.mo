��            )   �      �  !   �  "   �  '   �  ,     #   ;  &   _  !   �  &   �  '   �     �                 V   $  1   {     �  *   �     �        
     
   "     -     6     ;     D     T     [     a     g    p     |     �     �     �     �     �     �  
   �  
   �     �     �            K     5   i     �  7   �  "   �     	     '	     6	  	   F	     P	  	   V	     `	     x	     	     �	     �	                                                                          
                                                    	           @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Borders @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large Application menu Border si&ze: Buttons Close Close by double clicking:
 To open the menu, keep the button pressed until it appears. Close windows by double clicking &the menu button Context help Drag buttons between here and the titlebar Drop here to remove button Get New Decorations... Keep above Keep below Maximize Menu Minimize On all desktops Search Shade Theme Titlebar Project-Id-Version: kcmkwindecoration
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-04-14 02:49+0200
PO-Revision-Date: 2015-11-12 07:24+0100
Last-Translator: Adrián Chaves Fernández (Gallaecio) <adriyetichaves@gmail.com>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Enorme Grande Sen moldura Sen moldura lateral Normal Sobredimensionado Pequerrecho Descomunal Moi grande Menú de aplicativos Tamaño da moldura: Botóns Pechar Pechar cun duplo-clic:
 Para abrir o menú, prema o botón ata que apareza. &Pechar as xanelas cun dobre clic no botón do menú. Axuda contextual Arrastrar botóns entre esta zona e a barra do título. Solte aquí para retirar o botón. Obter novas decoracións… Manter en riba Manter en baixo Maximizar Menú Minimizar En todos os escritorios Buscar Recoller Tema Barra do título 