��          �      L      �     �     �     �     �          )     1     9     P     h     t  K   �     �     �     �     �            �     #   �     �       )        C  
   _  
   j     u  (   �     �     �     �     �  
   �     �          *     1                                                  
   	                                               Change the barcode type Clear history Clipboard Contents Clipboard history is empty. Clipboard is empty Code 39 Code 93 Configure Clipboard... Creating barcode failed Data Matrix Edit contents Indicator that there are more urls in the clipboard than previews shown+%1 Invoke action QR Code Remove from history Return to Clipboard Search Show barcode Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-10 03:07+0200
PO-Revision-Date: 2017-11-05 09:59+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Cambiar o tipo de código de barras Eliminar o historial Contido do portapapeis O historial do portapapeis está baleiro. O portapapeis está baleiro Código 39 Código 93 Configurar o portapapeis… A creación do código de barras fallou. Matriz de datos Editar o contido +%1 Invocar unha acción Código QR Retirar do historial Volver ao portapapeis Buscar Mostrar o código de barras 