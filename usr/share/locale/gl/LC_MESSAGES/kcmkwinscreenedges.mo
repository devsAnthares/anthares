��    %      D  5   l      @     A     E     G  	   Y     c     |     �     �     �     �     �     �            S   ,  w   �     �  M         [     |  ?   �     �  	   �     �     
     #  %   2     X     e  F   �  #   �     �  ]     R   f     �     �  �  �     �	     �	     �	  	   �	     �	     
     -
     ;
     G
      b
     �
  %   �
     �
     �
  ^   �
  x   G     �  F   �           :  B   M     �     �     �     �     �  &   �          .  W   :  '   �     �  c   �  ?   :     z     �               !      "          #   %                                                                            $                          	                
                            ms % %1 - All Desktops %1 - Cube %1 - Current Application %1 - Current Desktop %1 - Cylinder %1 - Sphere &Reactivation delay: &Switch desktop on edge: Activation &delay: Active Screen Corners and Edges Activity Manager Always Enabled Amount of time required after triggering an action until the next trigger can occur Amount of time required for the mouse cursor to be pushed against the edge of the screen before the action is triggered Application Launcher Change desktop when the mouse cursor is pushed against the edge of the screen EMAIL OF TRANSLATORSYour emails Lock Screen Maximize windows by dragging them to the top edge of the screen NAME OF TRANSLATORSYour names No Action Only When Moving Windows Open krunnerRun Command Other Settings Quarter tiling triggered in the outer Show Desktop Switch desktop on edgeDisabled Tile windows by dragging them to the left or right edges of the screen Toggle alternative window switching Toggle window switching Trigger an action by pushing the mouse cursor against the corresponding screen edge or corner Trigger an action by swiping from the screen edge towards the center of the screen Window Management of the screen Project-Id-Version: kcmkwinscreenedges
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-30 03:13+0100
PO-Revision-Date: 2018-01-18 21:52+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
  ms % %1 - Todos os escritorios %1 - Cubo %1 - O aplicativo actual %1 - Escritorio actual %1 - Cilindro %1 - Esfera &Retardo na reactivación: &Cambiar de escritorio na marxe: &Retardo na activación: Bordos e esquinas activos da pantalla Xestor de actividades Activado sempre A cantidade de tempo requirida tras desencadear unha acción ata se poida volver desencadearse A cantidade de tempo durante o que o rato debe empuxarse conta o bordo da pantalla antes de que se desencadee a acción. Iniciador de aplicativos Cambia o escritorio cando se empurro o rato contra a marxe da pantalla mvillarino@kde-espana.org Trancar a pantalla Maximizar as xanelas arrastrándoas ao bordo superior da pantalla. Marce Villarino Ningunha acción Só cando se movan xanelas Executar unha orde Outros parámetros A disposición en cuartos actívase no Mostrar o escritorio Desactivado Dispor as xanelas en mosaico arrastrándoas aos bordos esquerdo ou dereito da pantalla. Conmutar o cambio alternativo de xanela Conmutar o cambio de xanela Para desencadear unha acción leve o cursor do rato ao bordo ou esquina de pantalla correspondente. Para provocar unha acción pase do bordo da pantalla ao centro. Xestión das xanelas exterior da pantalla 