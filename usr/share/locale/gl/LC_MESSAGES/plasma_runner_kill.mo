��          �            h     i     r     �  	   �  *   �     �  "   �                 ;   +     g     z       �  �     k     x     �  
   �  $   �  &   �  ;        A     P     _  F   k     �     �     �     	                 
                                                          &Sort by &Trigger word: &Use trigger word CPU usage It is not sure, that this will take effect Kill Applications Config Process ID: %1
Running as user: %2 Send SIGKILL Send SIGTERM Terminate %1 Terminate running applications whose names match the query. inverted CPU usage kill nothing Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2012-08-07 08:13+0200
Last-Translator: Marce Villarino <mvillarino@kde-espana.es>
Language-Team: Galician <proxecto@trasno.net>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.4
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 &Ordenar por &Palabra disparadora: &Utilizar a palabra disparadora Uso da CPU Non é seguro que isto sexa efectivo Matar a configuración dos aplicativos Identificador do proceso: %1
En execución como usuario: %2 Enviar SIGKILL Enviar SIGTERM Terminar %1 Terminar os aplicativos en execución cuxo nome coincida coa consulta. Uso da CPU invertido matar nada 