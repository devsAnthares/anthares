��          t      �         P     )   b  %   �  @   �     �  V   	     `     {     �     �  �  �     O  6   V  =   �  6   �  (     Z   +  /   �     �  
   �  "   �                         	      
                             %1 is '%1 packages to update' and %2 is 'of which %1 is security updates'%1, %2 1 package to update %1 packages to update 1 security update %1 security updates First part of '%1, %2'1 package to update %1 packages to update No packages to update Second part of '%1, %2'of which 1 is security update of which %1 are security updates Security updates available System up to date Update Updates available Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-15 03:15+0100
PO-Revision-Date: 2018-01-18 21:57+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 %1, %2 1 paquete para actualizar %1 paquetes para actualizar. Actualización de seguridade %1 actualizacións de seguranza. 1 paquete para actualizar %1 paquetes para actualizar. Non hai ningún paquete para actualizar. dos que 1 é unha actualización de seguranza dos que %1 son actualizacións de seguranza. Hai dispoñíbeis actualizacións de seguranza. O sistema está ao día. Actualizar Hai actualizacións dispoñíbeis. 