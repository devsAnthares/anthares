��          �            x     y  
   �  
   �     �  .   �     �     �     �        *   )      T  ,   u  ,   �  0   �        �    "   �     �  	   �     �  @   �     9     H     [  /   n  7   �  )   �  
      
     2        I                        
                                            	           &Lock screen on resume: Activation Appearance Error Failed to successfully test the screen locker. Immediately Keyboard shortcut: Lock Session Lock screen automatically after: Lock screen when waking up from suspension Re&quire password after locking: Spinbox suffix. Short for minutes min  mins Spinbox suffix. Short for seconds sec  secs The global keyboard shortcut to lock the screen. Wallpaper &Type: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:10+0100
PO-Revision-Date: 2018-01-18 22:07+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 B&loquear a pantalla ao continuar: Activación Aparencia Erro Non se puido probar satisfactoriamente o bloqueador da pantalla. Inmediatamente Atallo de teclado: Bloquear a sesión Bloquear a pantalla automaticamente despois de: Bloquea a pantalla cando se recupere dunha suspensión. Esixir o contrasinal despois de bloquear:  min  mins  seg  segs Atallo de teclado global para bloquear a pantalla. &Tipo de fondo de escritorio: 