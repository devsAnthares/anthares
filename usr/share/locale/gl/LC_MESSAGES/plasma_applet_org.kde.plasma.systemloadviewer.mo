��    &      L  5   |      P     Q     l     y     �     �     �     �     �     �     �     �     �  &   �               #     ,     3     ?     M     U     ]     d     s     �     �     �     �     �     �     �     �     �     �  
   �                         "     .     F  	   M     W     [     i     z     �     �     �  0   �     �  
   �  	                  %     4     :     B     J     _     r     �     �     �     �  #   �  "   �     	     	     	  !   :	     \	     e	                           	      #      $                                                    &                                          !          %                            "   
    Abbreviation for secondss Application: Average clock: %1 MHz Bar Buffers: CPU CPU %1 CPU monitor CPU%1: %2% @ %3 Mhz CPU: %1% CPUs separately Cache Cache Dirty, Writeback: %1 MiB, %2 MiB Cache monitor Cached: Circular Colors Compact Bar Dirty memory: General IOWait: Memory Memory monitor Memory: %1/%2 MiB Monitor type: Nice: Set colors manually Show: Swap Swap monitor Swap: %1/%2 MiB Sys: System load Update interval: Used swap: User: Writeback memory: Project-Id-Version: plasma_applet_systemloadviewer
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-29 03:31+0200
PO-Revision-Date: 2017-10-27 08:00+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 s Aplicativo: Media do reloxo: %1 MHz Barras Búferes: CPU Procesador %1 Vixilante da CPU CPU%1: %2% @ %3 Mhz CPU: %1% Cada CPU por separado Caché Caché sucia, caché retroactiva: %1 MiB, %2 MiB Vixilante da caché Na caché: Círculos Cores Barras compactas Memoria sucia: Xeral IOWait: Memoria Vixilante da memoria Memoria: %1/%2 MiB Tipo de vixilante: Prioridade: Definir as cores manualmente Mostrar: Swap Vixilante da memoria de intercambio Memoria de intercambio: %1%/%2 MiB Sistema: Carga do sistema Intervalo de actualización: Memoria de intercambio empregada: Usuario: Memoria retroactiva: 