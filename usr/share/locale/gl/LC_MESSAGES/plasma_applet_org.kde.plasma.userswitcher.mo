��          �      <      �     �     �     �     �     �  '   �  >        L     f     �     �     �  )   �  7   �  '        B     T  �  s          +     1     >     R  	   _     i     r  0   �     �     �     �  0     
   2     =     D  "   c                                       
                      	                                        Current user General Layout Lock Screen New Session Nobody logged in on that sessionUnused Show a dialog with options to logout/shutdown/restartLeave... Show both avatar and name Show full name (if available) Show login username Show only avatar Show only name Show technical information about sessions User logged in on console (X display number)on %1 (%2) User logged in on console numberTTY %1 User name display You are logged in as <b>%1</b> Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-11 03:18+0100
PO-Revision-Date: 2017-09-09 12:36+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Usuario actual Xeral Disposición Bloquear a pantalla Nova sesión Non usado Saír… Mostrar o nome e a imaxe. Mostrar o nome completo (se está dispoñíbel). Mostrar o nome de usuario. Mostrar só a imaxe. Mostrar só o nome. Mostrar información técnica sobre as sesións. en %1 (%2) TTY %1 Aparencia dos nomes de usuario Está identificado como <b>%1</b>. 