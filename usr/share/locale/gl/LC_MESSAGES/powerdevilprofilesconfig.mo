��    	      d      �       �       �           !  
   -     8     G  �   `  �   (  �  �     P     e  +   u  "   �  #   �  !   �  �   
  �   �     	                                         EMAIL OF TRANSLATORSYour emails NAME OF TRANSLATORSYour names On AC Power On Battery On Low Battery Restore Default Profiles The KDE Power Management System will now generate a set of defaults based on your computer's capabilities. This will also erase all existing modifications you made. Are you sure you want to continue? The Power Management Service appears not to be running.
This can be solved by starting or scheduling it inside "Startup and Shutdown" Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-09 03:03+0200
PO-Revision-Date: 2014-05-26 04:47+0200
Last-Translator: Marce Villarino <mvillarino@gmail.com>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 mvillarino@gmail.com Marce Villarino Cando se está conectado á rede eléctrica Cando se está a gastar a batería Cando queda pouca carga na batería Restaurar os perfís predefinidos O Sistema de xestión da enerxía de KDE vai xerar valores predeterminados baseándose nas posibilidades do computador. Isto ha borrar todas as modificacións que fixese vostede. Seguro que quere continuar? Semella que o servizo de xestión da enerxía non se está a executar.
Isto só se pode arranxar iniciando ou planificándoo en «Arrinque e apagado» 