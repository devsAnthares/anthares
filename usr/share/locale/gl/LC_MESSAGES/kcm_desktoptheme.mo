��          �      �       H     I     a      m     �     �     �  
   �     �  &   �          .     L  1   b  �  �     �     �     �     �     �               )  ,   8     e     �     �  9   �         	                                    
                    Configure Desktop Theme David Rosca EMAIL OF TRANSLATORSYour emails Get New Themes... Install from File... NAME OF TRANSLATORSYour names Open Theme Remove Theme Theme Files (*.zip *.tar.gz *.tar.bz2) Theme installation failed. Theme installed successfully. Theme removal failed. This module lets you configure the desktop theme. Project-Id-Version: kcm_desktopthemedetails
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-22 05:21+0100
PO-Revision-Date: 2018-01-18 21:49+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Configurar o tema do escritorio David Rosca mvillarino@kde-espana.es Obter temas novos… Instalar desde un ficheiro… Marce Villarino Abrir un tema Retirar o tema Ficheiros de tema (*.zip *.tar.gz *.tar.bz2) Fallou a instalación do tema. O tema instalouse. A eliminación do tema fallou. Este módulo permítelle configurar o tema do escritorio. 