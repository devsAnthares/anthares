��          �   %   �      @     A  !   Y      {     �      �     �     �  +        =     N     [  (   z     �  ,   �  7   �     !  7   :     r  ]   �  1   �  	   "     ,  "   ?  5   b  �  �  "   A  ,   d  ,   �  $   �     �  !   �  #     "   :     ]     o     |  9   �     �  ;   �  @    	     a	  M   w	  !   �	  �   �	  6   q
     �
     �
  -   �
  ,   �
                    
         	                                                                                                 Cannot start initramfs. Cannot start update-alternatives. Configure Plymouth Splash Screen Download New Splash Screens EMAIL OF TRANSLATORSYour emails Get New Boot Splash Screens... Initramfs failed to run. Initramfs returned with error condition %1. Install a theme. Marco Martin NAME OF TRANSLATORSYour names No theme specified in helper parameters. Plymouth theme installer Select a global splash screen for the system The theme to install, must be an existing archive file. Theme %1 does not exist. Theme corrupted: .plymouth file not found inside theme. Theme folder %1 does not exist. This module lets you configure the look of the whole workspace with some ready to go presets. Unable to authenticate/execute the action: %1, %2 Uninstall Uninstall a theme. update-alternatives failed to run. update-alternatives returned with error condition %1. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-28 03:05+0200
PO-Revision-Date: 2017-11-01 14:45+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Non se pode iniciar «initramfs». Non se pode iniciar «update-alternatives». Configurar a pantalla de benvida de Plymouth Descargar novas pantallas de benvida adrian@chaves.io Obter novas pantallas de carga… «initramfs» non puido executarse. «initramfs» rematou cun erro %1. Instalar un tema. Marco Martin Adrian Chaves Non se indicou ningún tema nos parámetros do asistente. Instalador de temas de Plymouth Seleccionar unha pantalla de benvida global para o sistema. O tema para instalar, debe ser un ficheiro de arquivo existente. O tema %1 non existe. O tema está corrupto: non se atopou o ficheiro «.plymouth» dentro do tema. O cartafol de tema %1 non existe. Este módulo permítelle configurar a aparencia do espazo de traballo completo, e fornece algunhas aparencias predefinidas que pode usar. Non foi posíbel autenticar/executar a acción: %1, %2 Desinstalar Desinstalar un tema. «update-alternatives» non puido executarse. «update-alternatives» rematou cun erro %1. 