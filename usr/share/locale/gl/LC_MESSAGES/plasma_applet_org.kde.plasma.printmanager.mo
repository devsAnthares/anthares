��          �      ,      �     �     �     �     �     �     �     �     �  .   �     .     L     \     m     �     �  H   �  �  �     �     �                ;     R     X     q  3   �  /   �     �       #      
   D     O  N   h        	                                                                  
    &Configure Printers... Active jobs only All jobs Completed jobs only Configure printer General No active jobs No jobs No printers have been configured or discovered One active job %1 active jobs One job %1 jobs Open print queue Print queue is empty Printers Search for a printer... There is one print job in the queue There are %1 print jobs in the queue Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2015-02-21 09:37+0000
PO-Revision-Date: 2015-02-21 13:48+0100
Last-Translator: Adrián Chaves Fernández <adriyetichaves@gmail.com>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 &Configurar as impresoras… Só as tarefas activas Todas as tarefas Só as tarefas completadas Configurar a impresora Xeral Non hai tarefas activas. Non hai tarefas activas. Non se configurou nin descubriu ningunha impresora. Hai unha tarefa activa. Hai %1 tarefas activas. Unha tarefa %1 tarefas Abrir a cola de impresión A cola de impresión está baleira. Impresoras Buscar unha impresora… Hai un traballo de impresión na cola. Hai %1 traballos de impresión na cola. 