��          �      �       0     1  �  H     �       &         ?     `     n     v     �     �  (   �  �  �     �  �  �     �     �  ,   �  6        :     K     S     s     �  <   �         
                 	                                (c) 2002-2006 KDE Team <h1>System Notifications</h1>Plasma allows for a great deal of control over how you will be notified when certain events occur. There are several choices as to how you are notified:<ul><li>As the application was originally designed.</li><li>With a beep or other noise.</li><li>Via a popup dialog box with additional information.</li><li>By recording the event in a logfile without any additional visual or audible alert.</li></ul> Carsten Pfeiffer Charles Samuels Disable sounds for all of these events EMAIL OF TRANSLATORSYour emails Event source: KNotify NAME OF TRANSLATORSYour names Olivier Goffart Original implementation System Notification Control Panel Module Project-Id-Version: kcmnotify
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-09 07:18+0200
PO-Revision-Date: 2018-01-19 20:30+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 © 2002-2006 O equipo de KDE <h1>Notificacións do sistema</h1>Plasma dálle moitas posibilidades de controlar como se lle avisará cando ocorran certos eventos. Hai varias posibilidades para avisalo:<ul><li>Como fose deseñado orixinalmente o aplicativo.</li><li>Cunha badalada ou outro son.</li><li>Mediante un cadro de diálogo emerxente con información adicional.</li><li>Gardando o evento nun ficheiro de rexistro sen ningunha notificación visual nin auditiva.</li></ul> Carsten Pfeiffer Charles Samuels Desactivar os sons para todos estes eventos. javierjc@mundo-r.com, mvillarino@users.sourceforge.net Orixe do evento: KNotify Javier Jardón, Marce Villarino Olivier Goffart Realización orixinal Módulo de notificacións do sistema para o panel de control 