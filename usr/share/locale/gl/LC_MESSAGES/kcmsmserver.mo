��          �      <      �     �     �     �  7  �  �  #  +   �  ^              �     �  x   �  �   %     �               ;     U  �  r     e	     	     �	  J  �	    �
  %     m   )     �      �     �  �   �  �   T     .  
   K  (   V          �                          
       	                                                                    &End current session &Restart computer &Turn off computer <h1>Session Manager</h1> You can configure the session manager here. This includes options such as whether or not the session exit (logout) should be confirmed, whether the session should be restored again when logging in and whether the computer should be automatically shut down after session exit by default. <ul>
<li><b>Restore previous session:</b> Will save all applications running on exit and restore them when they next start up</li>
<li><b>Restore manually saved session: </b> Allows the session to be saved at any time via "Save Session" in the K-Menu. This means the currently started applications will reappear when they next start up.</li>
<li><b>Start with an empty session:</b> Do not save anything. Will come up with an empty desktop on next start.</li>
</ul> Applications to be e&xcluded from sessions: Check this option if you want the session manager to display a logout confirmation dialog box. Conf&irm logout Default Leave Option General Here you can choose what should happen by default when you log out. This only has meaning, if you logged in through KDM. Here you can enter a colon or comma separated list of applications that should not be saved in sessions, and therefore will not be started when restoring a session. For example 'xterm:konsole' or 'xterm,konsole'. O&ffer shutdown options On Login Restore &manually saved session Restore &previous session Start with an empty &session Project-Id-Version: kcmsmserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2017-09-30 05:57+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 R&ematar a sesión actual &Reiniciar o computador A&pagar o computador <h1>Xestor de sesións</h1> Aquí pode configurar o xestor de sesións. Isto inclúe opcións tales como se ao pechar a sesión (saír) débese solicitar confirmación, se a sesión previa debería ser restaurada ao acceder á conta, e se o computador debe apagarse de maneira predeterminada tras pechar automaticamente a sesión. <ul>
<li><b>Restaurar a sesión anterior:</b> Gardará todos os aplicativos en execución ao saír e as restauraraos no próximo inicio.</li>
<li><b>Restaurar a sesión gardada manualmente: </b> Permítelle gardar a sesión en calquera momento a través da opción «Gardar a sesión» no menú K. Isto significa que os aplicativos en execución nese momento reaparecerán no seu próximo inicio.</li>
<li><b>Iniciar cunha sesión baleira:</b> Non garda nada. Arrincará cunha sesión baleira no próximo inicio.</li>
</ul> Aplicativos a e&xcluír das sesións: Marque esta opción se quere que o xestor de sesións mostre un cadro de diálogo de confirmación de saída. Confirmar a &saída Opción de saída predeterminada Xeral Aquí pode escoller o que debería ocorrer de maneira predeterminada cando saia. Isto só ten sentido se accede a través de KDM. Aquí pode inserir unha lista de aplicativos separados por comas ou dous puntos que non se gardarán nas sesións nin han ser iniciados cando se restaure a sesión. Por exemplo «xterm:kconsole» ou «xterm,konsole». O&frecer opcións de apagado Ao acceder Restaurar a sesión &gardada manualmente Restaurar a sesión &anterior Iniciar cunha sesión &baleira 