��    �      t  �   �      �  
   �  
   �     �     �     �     �     �                         ,     ?     K     O     ]     k     r     w     }     �     �     �     �     �     �     �     �     �     �          &     :     X     `     o     ~     �     �     �     �     �     �     �     �     �     �     �  	   �     �  
   �     �  	          	   $     .     :     K  
   R     ]     m     {     �  
   �     �     �     �  	   �     �     �     �     �     �     �     �     
     !     '  	   -     7     D     J     Z     h     u     |     �     �     �     �  
   �     �     �     �  
   �     �       	        &     .  
   3     >  	   K  
   U     `     e     j     p     u     �     �     �     �     �     �     �     �     �     �     	  	        $     )     :     F     K     S     c     u     |     �     �     �     �  	   �     �     �     �  �  �  
   �     �     �     �     �     �     �     �  	                  +     A     M     Q     ^     m     t     y          �     �  
   �     �     �     �     �                 "   )     L  $   c     �     �     �     �     �     �     �     �     �     �     �     �     �     �               )     <     L     U     ]     n          �     �     �     �     �     �     �  
             %     -  
   6     A     W     ]     a     j     s     z     �     �     �     �     �     �     �     �  
   �     
          *     :     H     O     _     g     }  
   �  	   �     �     �  
   �     �     �     �     	  
              ,     1     6     <     D     X     _     w     �     �     �     �     �     �     �               (     -     A     Q     V     c     y     �     �     �  
   �     �  	   �     �     �     �             ^           	   +                y   A       2       n   i       /       �   :       b   �   g       w             B   j   �   1   L   6   <   �   �   8       K   �   ]   E   0   Q   s              D       V      Y      %   G      !   R       X       l   3   '   >          $               ;   O       m   �   q       P   u       |   �       {          @   W   .       f   a                  T   c       (   ?       4       ~      *       [   r   �          I   )      \   d   J   7   p                  -   N            F   `           �   h   H      "   M   C       5   ,       
          k   }   e           =   x   9   z             &              �   _   �   Z       S   o   t   U                           v       #        Accessible Appendable Audio Available Content BD BD-R BD-RE Battery Blank Block Blu Ray Recordable Blu Ray Rewritable Blu Ray Rom Bus CD Recordable CD Rewritable CD Rom CD-R CD-RW Camera Camera Battery Can Change Frequency Capacity Cdrom Drive Charge Percent Charge State Charging Compact Flash DVD DVD Plus Recordable DVD Plus Recordable Duallayer DVD Plus Rewritable DVD Plus Rewritable Duallayer DVD Ram DVD Recordable DVD Rewritable DVD Rom DVD+DL DVD+DLRW DVD+R DVD+RW DVD-R DVD-RAM DVD-RW Data Description Device Device Types Disc Type Discharging Drive Type Emblems Encrypted Encrypted Container File Path File System File System Type Floppy Free Space Free Space Text Fully Charged HD DVD Recordable HD DVD Rewritable HD DVD Rom HDDVD HDDVD-R HDDVD-RW Hard Disk Hotpluggable Icon Ide Ieee1394 Ignored In Use Keyboard Battery Keyboard Mouse Battery Label Major Max Speed Memory Stick Minor Monitor Battery Mouse Battery Not Charging Number Operation result Optical Drive OpticalDisc Other PDA Battery Parent UDI Partition Table Phone Battery Platform Plugged In Portable Media Player Primary Battery Processor Product Raid Read Speed Rechargeable Removable Rewritable Sata Scsi SdMmc Size Smart Media State Storage Access Storage Drive Storage Volume Super Video CD Supported Drivers Supported Media Supported Protocols Tape Temperature Temperature Unit Timestamp Type Type Description UPS Battery UUID Unknown Unknown Battery Unknown Disc Type Unused Usage Usb Vendor Video Blu Ray Video CD Video DVD Write Speed Write Speeds Xd Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2017-10-22 13:28+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Accesíbel Poden amecérselle datos Son Contido dispoñíbel BD BD-R BD-RE Batería En branco Bloque Blu Ray regravábel Blu Ray reescribíbel Blu Ray Rom Bus CD gravábel CD regravábel CD-ROM CD-R CD-RW Cámara Batería da cámara Pode cambiar a frecuencia Capacidade Unidade de CD-ROM Porcentaxe de carga Estado da carga Cargándose Memoria Flash DVD DVD Plus regravábel DVD Plus regravábel de capa dupla DVD Plus reescribíbel DVD Plus reescribíbel de capa dupla DVD-RAM DVD gravábel DVD regravábel DVD-ROM DVD+DL DVD+DLRW DVD+R DVD+RW DVD-R DVD-RAM DVD-RW Datos Descrición Dispositivo Tipos de dispositivo Tipo de disco Estase a descargar Tipo de unidade Emblemas Cifrada Contedor cifrado Ruta ao ficheiro Sistema de ficheiros Tipo de sistema de ficheiros Disquete Espazo libre Texto do espazo libre Carga completa HD DVD regravábel HD DVD reescribíbel HD DVD-ROM HDDVD HDDVD-R HDDVD-RW Disco duro Conectábel en quente Icona IDE IEEE1394 Ignorado En uso Pila do teclado Batería de teclado e rato Etiqueta Maior Velocidade máxima Lapis de memoria Menor Batería do monitor Pila do rato Sen cargar Número Resultado da operación Unidade óptica Disco óptico Outros Batería de PDA UDI pai Táboa de particións Batería do teléfono Plataforma Enchufado Reprodutor multimedia portátil Batería principal Procesador Produto Raid Velocidade de lectura Recargábel Extraíbel Regravábel SATA SCSI SdMmc Tamaño Soporte intelixente Estado Acceso ao almacenamento Unidade de almacenaxe Volume de almacenaxe Super Vídeo CD Controladores admitidos Medios admitidos Protocolos admitidos Cinta Temperatura Unidade de temperatura Marca de tempo Tipo Descrición do tipo Batería de SAI UUID Descoñecido Batería descoñecida Tipo de disco descoñecido Sen usar Uso USB Fabricante Blu Ray de vídeo Vídeo CD DVD de vídeo Velocidade de gravación Velocidades de gravación Xd 