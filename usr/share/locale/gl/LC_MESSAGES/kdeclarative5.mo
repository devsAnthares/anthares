��          �            h  ~   i  +   �           5     T     s     �     �  �   �  g   A  0   �  .   �     	  @       Z  ~   ^  +   �  >   	  '   H  1   p     �  	   �     �  �   �  g   t  3   �  +   	     <	     O	                                                   	             
                Click on the button, then enter the shortcut like you would in the program.
Example for Ctrl+A: hold the Ctrl key and press A. Conflict with Standard Application Shortcut EMAIL OF TRANSLATORSYour emails KPackage QML application shell NAME OF TRANSLATORSYour names No shortcut definedNone Reassign Reserved Shortcut The '%1' key combination is also used for the standard action "%2" that some applications use.
Do you really want to use it as a global shortcut as well? The F12 key is reserved on Windows, so cannot be used for a global shortcut.
Please choose another one. The key you just pressed is not supported by Qt. The unique name of the application (mandatory) Unsupported Key What the user inputs now will be taken as the new shortcutInput Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-30 03:07+0100
PO-Revision-Date: 2015-03-14 06:28+0100
Last-Translator: Adrián Chaves Fernández <adriyetichaves@gmail.com>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
X-Environment: kde, development
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Prema o botón, logo insira o atallo que desexe ter no programa.
Exemplo para Ctrl+A: manteña premida a tecla Ctrl e prema A. Conflito cun atallo estándar de aplicativo xosecalvo@gmail.com, mvillarino@gmail.com, proxecto@trasno.gal Shell de aplicativos en QML de KPackage Xabier García Feal, marce villarino, Xosé Calvo Ningún Reasignar Atallo reservado A combinación de teclas «%1» tamén está asignada á acción estándar «%2», que é usada por algúns aplicativos.
Seguro que quere empregala tamén como atallo global? A tecla F12 está reservada en Windows polo que non se pode empregar como atallo global.
Escolla outro. A tecla que acaba de premer non é admitida por Qt. Nome único para o aplicativo (obrigatorio) Tecla non admitida Entrada 