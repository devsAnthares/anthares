��            )   �      �  ;   �  J   �          /  ~   7  A   �     �       )        G     X     i      q     �     �     �  
   �     �  
   �     �             "   <     _  	   y     �     �     �  �  �     �     �     �     �  {   �  >   ;     z     �  +   �     �     �     �  &   �     	     0	     @	  
   N	  6   Y	     �	     �	     �	  $   �	  *   �	     
     9
     F
     c
     w
                                      
                                                                                            	        %1 is the full user name, %2 is the user login name%1 (%2) %1 is the name of a detail about the current action provided by polkit%1: (c) 2009 Red Hat, Inc. Action: An application is attempting to perform an action that requires privileges. Authentication is required to perform this action. Another client is already authenticating, please try again later. Application: Authentication Required Authentication failure, please try again. Click to edit %1 Click to open %1 Details EMAIL OF TRANSLATORSYour emails Former maintainer Jaroslav Reznik Lukáš Tinkl Maintainer NAME OF TRANSLATORSYour names P&assword: Password for %1: Password for root: Password or swipe finger for %1: Password or swipe finger for root: Password or swipe finger: Password: PolicyKit1 KDE Agent Select User Vendor: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:22+0100
PO-Revision-Date: 2017-10-04 20:40+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 %1 (%2) %1: © 2009 Red Hat, Inc. Acción: Un aplicativo está a intentar realizar unha acción que require privilexios. Debe autenticarse para realizar esta acción. Xa está a autenticar outro aplicativo, inténteo máis tarde. Aplicativo: Debe autenticarse Fallo de autenticación, inténteo de novo. Prema para editar %1 Prema para abrir %1 Detalles mvillarino@gmail.com, adrian@chaves.io Mantedor orixinal. Jaroslav Reznik Lukáš Tinkl Mantenedor Marce Villarino, Adrián Chaves Fernández (Gallaecio) &Contrasinal: Contrasinal de %1: Contrasinal de «root»: Contrasinal ou pegada dixital de %1: Contrasinal ou pegada dixital de «root»: Contrasinal ou pegada dixital: Contrasinal: Axente de KDE para PolicyKit Escoller un usuario Fabricante: 