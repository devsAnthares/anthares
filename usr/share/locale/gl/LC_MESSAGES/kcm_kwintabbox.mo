��    "      ,  /   <      �  
   �               ,     >     J  !   V     x     �     �     �     �     �  L   �     #     +     J     Y     u     z     �     �     �     �  	   �     �     �     �  �   �  F   �     �     �     �  �  �     �     �               +     <     H     U     ]     q     �     �     �  R   �     	  0    	     Q	  ,   f	  	   �	     �	  "   �	     �	     �	  	   �	     �	     
     
     1
  v   E
  ]   �
          /     D                    !                                            	                                                           "               
                    Activities All other activities All other desktops All other screens All windows Alternative An example Desktop NameDesktop 1 Content Current activity Current application Current desktop Current screen Filter windows by Focus policy settings limit the functionality of navigating through windows. Forward Get New Window Switcher Layout Hidden windows Include "Show Desktop" icon Main Minimization Only one window per application Recently used Reverse Screens Shortcuts Show selected window Sort order: Stacking order The currently selected window will be highlighted by fading out all other windows. This option requires desktop effects to be active. The effect to replace the list window when desktop effects are active. Virtual desktops Visible windows Visualization Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2012-07-30 00:57+0200
Last-Translator: Marce Villarino <mvillarino@kde-espana.es>
Language-Team: Galician <proxecto@trasno.net>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.4
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Actividades As demais actividades Os demais escritorios As demais pantallas Todas as xanelas Alternativo Escritorio 1 Contido A actividade actual O aplicativo actual Escritorio actual A pantallas actual Filtrar as xanelas segundo A política de focalización limita a funcionalidade da navegación polas xanelas. Adiante Obter novas disposicións do selector de xanelas As xanelas agochadas Incluír a icona de «Mostrar o escritorio» Principal Minimización Só unha xanela de cada aplicativo Empregados recentemente Inverter Pantallas Atallos Mostrar a xanela escollida Modo de ordenación: Orde de amoreamento wA xanela escollida realzarase escurecendo todas as outras. Esta opción require ter activos os efectos do escritorio. O efecto co que substituír a xanela coa lista cando están activos os efectos do escritorio. Escritorios virtuais As xanelas visíbeis Visualización 