��    !      $  /   ,      �  .   �  1     9   J     �  -   �  &   �  )   �  .   #  &   R  )   y  .   �  &   �  )   �  2   #  5   V  =   �  #   �     �  !     '   /  "   W  0   z  '   �  *   �     �          7     R     j     �     �  &   �  �  �  0   �	  ,   �	  2   $
     W
  )   w
  "   �
  !   �
  +   �
  !     #   4  )   X     �      �  0   �  ,   �  6         W     w     �  +   �  #   �  1   �  "   *  $   M     r     �     �     �     �     �     �  #                                                                 	                                
                                    !                             @info:statusAdded files to Bazaar repository. @info:statusAdding files to Bazaar repository... @info:statusAdding of files to Bazaar repository failed. @info:statusBazaar Log closed. @info:statusCommit of Bazaar changes failed. @info:statusCommitted Bazaar changes. @info:statusCommitting Bazaar changes... @info:statusPull of Bazaar repository failed. @info:statusPulled Bazaar repository. @info:statusPulling Bazaar repository... @info:statusPush of Bazaar repository failed. @info:statusPushed Bazaar repository. @info:statusPushing Bazaar repository... @info:statusRemoved files from Bazaar repository. @info:statusRemoving files from Bazaar repository... @info:statusRemoving of files from Bazaar repository failed. @info:statusReview Changes failed. @info:statusReviewed Changes. @info:statusReviewing Changes... @info:statusRunning Bazaar Log failed. @info:statusRunning Bazaar Log... @info:statusUpdate of Bazaar repository failed. @info:statusUpdated Bazaar repository. @info:statusUpdating Bazaar repository... @item:inmenuBazaar Add... @item:inmenuBazaar Commit... @item:inmenuBazaar Delete @item:inmenuBazaar Log @item:inmenuBazaar Pull @item:inmenuBazaar Push @item:inmenuBazaar Update @item:inmenuShow Local Bazaar Changes Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2017-11-05 10:04+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Engadíronse os ficheiros ao repositorio Bazaar. Engadindo ficheiros ao repositorio Bazaar… Engadir ficheiros ao repositorio de Bazaar fallou. Pechouse o historial de Bazaar. A remisión dos cambios de Bazaar fallou. Remitíronse os cambios de Bazaar. Remitindo os cambios de Bazaar… A descarga do repositorio de Bazaar fallou. Descargouse o repositorio Bazaar. Descargando o repositorio Bazaar… O envío do repositorio de Bazaar fallou. Enviouse o repositorio Bazaar. Enviando o repositorio Bazaar… Retiráronse os ficheiros do repositorio Bazaar. Retirando ficheiros do repositorio Bazaar… Retirar os ficheiros do repositorio de  Bazaar fallou. A revisión dos cambios fallou. Revisáronse os cambios. Revisando os cambios… A execución do historial de Bazaar fallou. Accedendo ao historial de Bazaar… A actualización do repositorio de Bazaar fallou. Actualizouse o repositorio Bazaar. Actualizando o repositorio Bazaar… Engadir a Bazaar… Remisión de Bazaar… Eliminar de Bazaar Historial de Bazaar Descarga de Bazaar Envío de Bazaar Actualización de Bazaar Mostrar os cambios locais de Bazaar 