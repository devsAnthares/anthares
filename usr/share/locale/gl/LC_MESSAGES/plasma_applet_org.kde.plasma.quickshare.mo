��          �      <      �     �     �     �  +   �  @        L     a     i     w     }     �  
   �     �     �     �     �     �  �  
     �     �     �  2   �  Y     $   s     �     �     �     �     �     �  	   �     �            "   /     
         	                                                                                       <a href='%1'>%1</a> Close Copy Automatically: Don't show this dialog, copy automatically. Drop text or an image onto me to upload it to an online service. Error during upload. General History Size: Paste Please wait Please, try again. Sending... Share Shares for '%1' Successfully uploaded The URL was just shared Upload %1 to an online service Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-01 04:04+0100
PO-Revision-Date: 2017-10-03 11:49+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 <a href='%1'>%1</a> Pechar Copiar automaticamente: Non mostrar este diálogo, copiar automaticamente. Arrastre texto ou unha imaxe para riba de min para que a envíe a un servizo en internet. Produciuse un erro durante o envío. Xeral Servizo de historiais: Pegar Agarde Inténteo de novo.. Enviando… Compartir Comparticións de «%1» Enviouse correctamente. Compartiuse o URL Enviar %1 a un servizo en internet 