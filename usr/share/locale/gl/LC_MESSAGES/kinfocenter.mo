��          �   %   �      @     A     Z     g          �     �      �     �     �     �            W   2  #   �     �     �     �     �  $        +     J     \     p     �  �  �     �     �     �     �     �     �  ?        K     i     v     |     �  "   �     �     �     �               7  +   H     t     �     �     �                                           	                                                           
                        @title:menuMain Toolbar Clear Search Collapse All Categories Copyright 2009-2018 KDE Current Maintainer David Hubner EMAIL OF TRANSLATORSYour emails Expand All Categories Helge Deller Help button labelHelp Info Center Information Modules Information about current module located in about menuAbout Current Information Module Kaction search labelSearch Modules Main window titleKInfocenter Matthias Elter Matthias Ettrich Matthias Hoelzer-Kluepfel Module help button labelModule Help NAME OF TRANSLATORSYour names Nicolas Ternisien Previous Maintainer Search Bar Click MessageSearch Waldo Bastian Project-Id-Version: kinfocenter
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-08 05:56+0100
PO-Revision-Date: 2018-01-18 21:55+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Barra de ferramentas principal Limpar a busca Pregar todas as categorías © 2009-2018 KDE Mantenedor actual David Hubner jba@pobox.com, xabigf@gmx.net, mvillarino@users.sourceforge.net Expandir todas as categorías Helge Deller Axuda Centro de información Módulos de información Sobre este módulo de información Buscar módulos Centro de información de KDE Matthias Elter Matthias Ettrich Matthias Hoelzer-Kluepfel Axuda do módulo Jesús Bravo, Xabi García, Marce Villarino Nicolas Ternisien Antigo mantenedor Buscar Waldo Bastian 