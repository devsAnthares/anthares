��          �      �       0     1     9  
   S     ^     u     �     �     �     �     �     �     �  �  �  	   �     �  	   �     �          4  
   P     [     {     �  '   �     �     	   
                                                  (Empty) @title:windowSelect Font Appearance Configure Input Method Custom Font: Exit Input Method Hide %1 Reload Config Select Font Show Use Default Font Vertical List Project-Id-Version: kimpanel
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-07-18 03:21+0200
PO-Revision-Date: 2017-08-11 07:43+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 (baleiro) Escoller o tipo de letra Aparencia Configurar o método de entrada Tipo de letra personalizado: Saír do método de entrada Agochar %1 Cargar a configuración de novo Escoller o tipo de letra Mostrar Empregar o tipo de letra predeterminado Lista vertical 