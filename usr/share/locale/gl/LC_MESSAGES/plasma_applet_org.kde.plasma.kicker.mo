��    _                   C   	  /   M  -   }     �     �     �     �      	     	     1	     >	     J	  
   S	     ^	     g	     p	     �	  	   �	     �	     �	     �	  ,   �	  	    
     

  
   )
     4
     L
     `
     u
     �
     �
     �
     �
     �
  	   �
     �
     �
     
               !     (  	   ;     E     ]  
   r     }     �  
   �     �     �     �  
   �     �     �               $     2     @     R     h     y     �     �     �     �  	   �     �     �     �               4     Q     j     �     �     �     �  	   �     �  ,   �          !     0     @     L     S     b     t     �  #   �     �  �  �     �     �     �     �     �       '   +     S     h     p     |     �     �  
   �  	   �     �     �  
   �     �          $  1   6  	   h  $   r     �     �     �     �     �          &     ;     [     a     i  
   r     }     �     �     �     �     �  
   �      �     �          /     G  
   \     g     �     �     �  	   �     �     �     �     �               6     L     c     y     �     �     �  	   �     �     �  #   �          !  +   >  )   j  *   �     �     �     �          "     )  3   A  	   u          �     �     �     �     �     �      �  %     	   C         Q         Y   5           %       H       J   !              \   *       @   V   4   [       A   (   
       U   B   ]   ^       $   _         '   >       D   K      -                  ?      ,   O   0       R   8   6   W   2   I   =   X   E   #       N   C       T   G   M          3   "         9             +          S          F           ;   :                    &   <   L          P         7          .       /       1                     )            	   Z       @action opens a software center with the applicationManage '%1'... @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Desktop Add to Favorites Add to Panel (Widget) Align search results to bottom All Applications App name (Generic name)%1 (%2) Applications Apps & Docs Behavior Categories Computer Contacts Description (Name) Description only Documents Edit Application... Edit Applications... End session Expand search to bookmarks, files and emails Favorites Flatten menu to a single level Forget All Forget All Applications Forget All Contacts Forget All Documents Forget Application Forget Contact Forget Document Forget Recent Documents General Generic name (App name)%1 (%2) Hibernate Hide %1 Hide Application Icon: Lock Lock screen Logout Name (Description) Name only Often Used Applications Often Used Documents Often used On All Activities On The Current Activity Open with: Pin to Task Manager Places Power / Session Properties Reboot Recent Applications Recent Contacts Recent Documents Recently Used Recently used Removable Storage Remove from Favorites Restart computer Run Command... Run a command or a search query Save Session Search Search results Search... Searching for '%1' Session Show Contact Information... Show In Favorites Show applications as: Show often used applications Show often used contacts Show often used documents Show recent applications Show recent contacts Show recent documents Show: Shut Down Sort alphabetically Start a parallel session as a different user Suspend Suspend to RAM Suspend to disk Switch User System System actions Turn off computer Type to search. Unhide Applications in '%1' Unhide Applications in this Submenu Widgets Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2018-01-18 22:12+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Xestionar «%1»… Escoller… Borrar a icona Engadir ao escritorio Engadir aos favoritos Engadir ao panel (trebello) Aliñar no fondo os resultados da busca Todos os aplicativos %1 (%2) Aplicativos Aplicativos e documentos Comportamento Categorías Computador Contactos Descrición (Nome) Só a descrición Documentos Editar o aplicativo… Editar os aplicativos… Rematar a sesión Incluír na busca marcadores, ficheiros e correos Favoritos Desaniñar o menú a un único nivel Esquecer todo Esquecer todos os aplicativos Esquecer todos os contactos Esquecer todos os documentos Esquecer o aplicativo Esquecer o contacto Esquecer o documento Esquecer os documentos recentes Xeral %1 (%2) Hibernar Agochar %1 Agochar o aplicativo Icona: Bloquear Trancar a pantalla Saír Nome (descrición) Só o nome Aplicativos usados habitualmente Documentos usados habitualmente Usados habitualmente En todas as actividades Na actividade actual Abrir con: Fixar no xestor de tarefas Lugares Enerxía / sesión Propiedades Reiniciar Aplicativos recentes Contactos recentes Documentos recentes Empregados recentemente Empregados recentemente Dispositivo extraíbel Retirar dos favoritos Reiniciar o computador Executar unha orde… Executa unha orde ou unha busca Gardar a sesión Buscar Resultados da busca Buscar… Buscando «%1» Sesión Mostrar información do contacto… Mostrar nos favoritos Mostrar os aplicativos como: Mostrar os aplicativos usados habitualmente Mostrar os contactos usados habitualmente Mostrar os documentos usados habitualmente Mostrar os aplicativos recentes Mostrar os contactos recentes Mostrar documentos recentes Mostrar: Apagar Ordenar alfabeticamente Comezar unha sesión paralela cun usuario distinto. Suspender Suspender na RAM Suspender no disco Cambiar de usuario Sistema Accións do sistema Apagar o computador Escriba o que buscar. Mostrar os aplicativos de «%1» Mostrar os aplicativos deste submenú Trebellos 