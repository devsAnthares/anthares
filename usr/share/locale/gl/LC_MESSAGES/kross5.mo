��    $      <  5   \      0     1  +   E     q  4   �  &   �     �     �                     5     :     P     X  ,   u  3   �     �     �               !  '   .     V     u     {     �     �     �     �     �     �  &   �       B        b  �  y     t     z     �     �     �     �  
   �  	   �     �  >   	     D	     K	  	   f	     p	  6   �	  6   �	  (   �	  "   $
  	   G
     Q
     X
  +   e
  1   �
     �
  #   �
     �
               1     :     R  '   X     �  ;   �     �                                       
                              !                                  $      #                               	                           "             @info:creditAuthor @info:creditCopyright 2006 Sebastian Sauer @info:creditSebastian Sauer @info:shell command-line argumentThe script to run. @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: application descriptionCommand-line utility to run Kross scripts. application nameKross Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2014-05-13 22:33+0200
Last-Translator: Marce Villarino <mvillarino@kde-espana.org>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
X-Environment: kde, development
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Autor Copyright 2006 Sebastian Sauer Sebastian Sauer O script que se vai executar. Xeral Engade un script novo. Engadir… Cancelar? Comentario: xosecalvo@gmail.com, mvillarino@gmail.com, proxecto@trasno.gal Editar Editar o script escollido. Editar… Executar o script escollido. Non se puido crear un script para o intérprete «%1» Non se puido determinar o intérprete do script «%1» Non se puido cargar o intérprete «%1» Non se puido abrir o script «%1» Ficheiro: Icona: Intérprete: O nivel de seguranza do intérprete de Ruby Xabier García Feal, marce villarino, Xosé Calvo Nome: Non existe ningunha función «%1» Non existe o intérprete «%1» Retirar Retirar o script escollido. Executar O script %1 non existe. Parar Deter a execución do script escollido. Texto: Utilidade da liña de ordes para executar scripts de Kross. Kross 