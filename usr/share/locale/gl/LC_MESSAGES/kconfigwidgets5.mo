��    w      �  �   �      
     
  	   &
     0
     =
     K
     Q
     X
     i
     o
     w
     
     �
     �
     �
     �
  	   �
     �
     �
  
   �
     �
     �
       
             !     (  	   7     A  
   G  
   R     ]     i     x     ~     �     �     �     �  ;   �  -   �  #        ;     T     l  
   �     �     �  
   �     �     �     �               7     K     P  	   k      u     �     �     �  
   �     �     �               .     E     V     f     v     �     �  
   �     �  )   �     �          #     2     A     R     X     `     s  '   �     �     �     �     �     �     
           .  C   <     �  s   �           %     :     Q     f     �     �      �     �  -   �  /        4  	   =  !   G     i      �     �     �     �     �     �  �  �     �  	   �     	          ,     4     <     N  	   V     `     f  
        �     �     �     �     �     �     �     �     
            	   1     ;     B     T     a  
   h     s     �     �     �     �     �  	   �     �     �  	   �  	   �      	     *  
   :     E  
   V     a  $   i     �     �      �  $   �  '   �  &        E     ]  (   e     �  >   �     �     �          !     4     G     e     �     �     �     �     �     �       1        C     P  &   b     �     �     �     �     �       	          "   /  5   R     �     �      �     �     �  *   �     $     >  N   ^     �  �   �  %   K  #   q  $   �  )   �  "   �                :     Z  *   h  .   �     �     �  %   �  )   �  )   #     M     U     ^     f     m     o   5   l   W   	             1               R   6          J         >   u   d           V          M   T       X   r   v          G   .   Q   \       a              E   -       O               ^          b   7           +   Y   C      [       4                 H       k   B   c       ]   I   *   P   (   h   w   ;   N   3   
                @      "   S      _                   L   $      9   F      D          m   U   %       K       &                     t   Z   `   !       ,   0   g   f   p          i      2   e       n   /      q       ?      <   A   #   8          =   '   s   )               :   j           %1 &Handbook &About %1 &Actual Size &Add Bookmark &Back &Close &Configure %1... &Copy &Delete &Donate &Edit Bookmarks... &Find... &First Page &Fit to Page &Forward &Go To... &Go to Line... &Go to Page... &Last Page &Mail... &Move to Trash &New &Next Page &Open... &Paste &Previous Page &Print... &Quit &Redisplay &Rename... &Replace... &Report Bug... &Save &Save Settings &Spelling... &Undo &Up &Zoom... @action:button Goes to next tip, opposite to previous&Next @action:button Goes to previous tip&Previous @option:check&Show tips on startup @titleDid you know...?
 @title:windowConfigure @title:windowTip of the Day About &KDE C&lear Check spelling in document Clear List Close document Configure &Notifications... Configure S&hortcuts... Configure Tool&bars... Copy selection to clipboard Create new document Cu&t Cut selection to clipboard Dese&lect EMAIL OF TRANSLATORSYour emails Encodings menuAutodetect Encodings menuDefault F&ull Screen Mode Find &Next Find Pre&vious Fit to Page &Height Fit to Page &Width Go back in document Go forward in document Go to first page Go to last page Go to next page Go to previous page Go up NAME OF TRANSLATORSYour names No Entries Open &Recent Open a document which was recently opened Open an existing document Paste clipboard content Print Previe&w Print document Quit application Re&do Re&vert Redisplay document Redo last undone action Revert unsaved changes made to document Save &As... Save document Save document under a new name Select &All Select zoom level Send document by mail Show &Menubar Show &Toolbar Show Menubar<p>Shows the menubar again after it has been hidden</p> Show St&atusbar Show Statusbar<p>Shows the statusbar, which is the bar at the bottom of the window used for status information.</p> Show a print preview of document Show or hide menubar Show or hide statusbar Show or hide toolbar Switch Application &Language... Tip of the &Day Undo last action View document at its actual size What's &This? You are not allowed to save the configuration You will be asked to authenticate before saving Zoom &In Zoom &Out Zoom to fit page height in window Zoom to fit page in window Zoom to fit page width in window go back&Back go forward&Forward home page&Home show help&Help without name Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-16 03:10+0100
PO-Revision-Date: 2018-01-11 21:23+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
X-Environment: kde, development
X-Accelerator-Marker: &
X-Text-Markup: kde4
 &Manual de %1 &Sobre %1 Tamaño &real &Engadir un marcador &Atrás &Pechar &Configurar %1… &Copiar &Eliminar &Doar &Editar os marcadores… &Atopar… &Primeira páxina &Axustar á páxina Adiante &Ir a… &Ir á liña… &Ir á páxina… Última pá&xina Enviar por &correo… &Botar no lixo &Novo Páxina &seguinte &Abrir… &Pegar Páxina &anterior Im&primir… &Saír &Redebuxar &Renomear… &Substituír… Informar dun &fallo… &Gardar &Gardar a configuración &Ortografía… &Desfacer &Subir &Ampliar… &Seguinte &Anterior Mostrar os con&sellos ao iniciar Sabía que…?
 Configurar Consello do día Sobre &KDE &Limpar Comprobar a ortografía do documento Limpar a lista Pechar o documento Configurar as &notificacións… Configurar os &atallos de teclado… Configurar as &barras de ferramentas… Copiar a selección para o portapapeis Crear un novo documento Cor&tar Corta a selección e pona no portapapeis Anu&lar a escolla xosecalvo@gmail.com, mvillarino@gmail.com, proxecto@trasno.gal Detectar automaticamente Predeterminada Modo a pantalla &completa Atopar a &seguinte Atopar a &anterior Axustar á &altura da páxina Axustar á a&nchura da páxina Retroceder no documento Avanzar no documento Ir á primeira paxina Ir á última páxina Ir á páxina seguinte Ir á páxina anterior Subir Xabier García Feal, marce villarino, Xosé Calvo Sen entradas Abrir un &recente Abrir un documento aberto recentemente Abrir un documento existente Pega o contido do portapapeis Pre&visualizar o impreso Imprimir o documento Saír do aplicativo &Refacer Re&verter Redebuxar o documento Refacer a última acción desfeita Anula os cambios non gardados que fixese no documento Gardar &como… Gardar o documento Garda o documento con outro nome Escoller &todo Escoller nivel de ampliación Enviar o documento por correo electrónico Mostrar a barra de &menú Mostrar a barra de &ferramentas Mostrar barra de menú<p>Mostra de novo a barra de menú tras ser agochada</p> Mostrar a barra de &estado Mostrar a barra de estado<p>Mostra a barra de estado, que é a barra no fondo da xanela onde se mostra información de estado.</p> Mostra unha vista previa do documento Mostrar ou agochar a barra de menú Mostrar ou agochar a barra de estado Mostrar ou agochar a barra de ferramentas Cambiar o &idioma do aplicativo… Consello do &día Desfacer a última acción feita Ver o documento co tamaño real Que é &isto? Non se lle permite gardar a configuración Pediráselle que se autentique antes de gardar A&chegar Afastar Axustar a altura da páxina da xanela Axustar o tamaño da páxino ao da xanela Axustar a anchura da páxina á da xanela &Atrás A&diante &Inicio A&xuda sen nome 