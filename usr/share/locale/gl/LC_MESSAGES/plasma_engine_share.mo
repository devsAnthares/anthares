��    
      l      �       �   $   �   %     :   <     w  '   �  -   �     �       '     �  <  -      4   N  8   �  0   �  +   �  )     !   C     e  )   w         	                            
        Could not detect the file's mimetype Could not find all required functions Could not find the provider with the specified destination Error trying to execute script Invalid path for the requested provider It was not possible to read the selected file Service was not available Unknown Error You must specify a URL for this service Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2012-08-07 08:20+0200
Last-Translator: Marce Villarino <mvillarino@kde-espana.es>
Language-Team: Galician <proxecto@trasno.net>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.4
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Non se puido detectar o tipo mime do ficheiro Non se puideron atopar todas as funcións requiridas Non se puido atopar o fornecedor do destino especificado Produciuse un erro ao intentar executar o script Ruta incorrecta para o fornecedor escollido Non foi posíbel ler o ficheiro escollido O servizo non estaba dispoñíbel Erro descoñecido Debe especificar un URL para este servizo 