��    &      L  5   |      P     Q     l     y     �     �     �     �     �     �     �     �     �  &   �               #     ,     3     ?     M     U     ]     d     s     �     �     �     �     �     �     �     �     �     �  
   �            1       L     N     ^  	   y  	   �  	   �     �     �     �     �     �     �  $   �       	        !     (  	   /     9     F     M     U     \     l          �     �  	   �     �     �     �     �     �     	     	  	   )	     3	                           	      #      $                                                    &                                          !          %                            "   
    Abbreviation for secondss Application: Average clock: %1 MHz Bar Buffers: CPU CPU %1 CPU monitor CPU%1: %2% @ %3 Mhz CPU: %1% CPUs separately Cache Cache Dirty, Writeback: %1 MiB, %2 MiB Cache monitor Cached: Circular Colors Compact Bar Dirty memory: General IOWait: Memory Memory monitor Memory: %1/%2 MiB Monitor type: Nice: Set colors manually Show: Swap Swap monitor Swap: %1/%2 MiB Sys: System load Update interval: Used swap: User: Writeback memory: Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-29 03:31+0200
PO-Revision-Date: 2018-02-12 05:47-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-stable/messages/kde-workspace/plasma_applet_org.kde.plasma.systemloadviewer.pot
 s 应用程序： 平均时钟频率: %1 MHz 条形图 缓冲： 处理器 CPU %1 CPU 监视器 CPU%1：%2% @ %3 Mhz CPU: %1% 分别显示 CPU 缓存 脏缓存，写回：%1 MiB，%2 MiB 缓存监视器 缓存： 环形 颜色 紧凑条 脏内存： 常规 IOWait: 内存 内存监视器 内存：%1/%2 MiB 监视器类型: Nice： 手动设定颜色 显示： 交换空间 交换空间监视器 交换空间：%1/%2 MiB Sys: 系统负载 更新间隔： 已用交换空间： 用户： 写回内存： 