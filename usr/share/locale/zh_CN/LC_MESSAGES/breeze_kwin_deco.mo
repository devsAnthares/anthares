��    D      <  a   \      �     �     �     �  !     "   %  &   H  ,   o  #   �  &   �  !   �  &   	  '   0  "   X  #   {  !   �  "   �  '   �       +     2   <     o  
   �     �     �     �     �     �     �     �     �     	  !   	  +   *	     V	     v	      {	     �	     �	     �	     �	     �	  !   �	     
  	    
     *
     2
     R
     m
  &   �
     �
     �
     �
     �
     �
     �
     �
            $        A     R     l     ~     �     �     �  >   �         )     1     3     M     T  	   X     b     o  	   s     }     �     �     �     �     �     �     �     �  !   �  <   �          #     *     >     N     U  	   j  	   t     ~     �  	   �  !   �  *   �     �               2     ?     V     c     j     �     �     �     �     �     �     �     �          %     ,     E     I     P     ^     e  	   y      �     �     �     �     �     �               '     D   8      1                 )                  .               3   @   4       &      /   >       ?      <   ;             7       (          
                 $                  0   5       A   :   #   2         C   +                        =   '             *                 	          ,   !                             B   9   6       "       %   -     ms % &Matching window property:  @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Border @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large @item:inlistbox Button size:Large @item:inlistbox Button size:Medium @item:inlistbox Button size:None @item:inlistbox Button size:Small @item:inlistbox Button size:Very Large Add Add handle to resize windows with no border Allow resizing maximized windows from window edges Anima&tions duration: Animations B&utton size: Border size: Center Center (Full Width) Class:  Color: Decoration Options Detect Window Properties Dialog Draw a circle around close button Draw separator between Title Bar and Window Draw window background gradient Edit Edit Exception - Breeze Settings Enable animations Enable/disable this exception Exception Type General Hide window title bar Information about Selected Window Left Move Down Move Up New Exception - Breeze Settings Question - Breeze Settings Regular Expression Regular Expression syntax is incorrect Regular expression &to match:  Remove Remove selected exception? Right Shadows Si&ze: Tiny Tit&le alignment: Title:  Use window class (whole application) Use window title Warning - Breeze Settings Window Class Name Window Identification Window Property Selection Window Title Window-Specific Overrides strength of the shadow (from transparent to opaque)S&trength: Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-09 03:20+0100
PO-Revision-Date: 2018-02-12 05:47-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-stable/messages/kde-workspace/breeze_kwin_deco.pot
  毫秒 % 匹配窗口属性(&M)： 巨大 大 无边框 无侧边框 中 超特大 小 超大 很大 大 中等 无 小 很大 添加 为无边框窗口添加缩放柄 允许从窗口边缘调整已经最大化的窗口的大小 动画长度(&T)： 动画 按钮大小(&U)： 边框大小： 居中 居中(完整宽度) 分类： 颜色： 装饰选项 检测窗口属性 对话框 在关闭按钮周围绘制圆圈 在标题栏和窗口之间绘制分隔符 绘制窗口背景渐变 编辑 编辑例外 - Breeze 设置 启用动画 启用/禁用此例外 例外类型 常规 隐藏窗口标题栏 所选窗口的信息 左 下移 上移 新建例外 - Breeze 设置 问题 - Breeze 设置 正则表达式 正则表达式语法错误 匹配正则表达式(&T)： 删除 移除所选例外吗？ 右 阴影 大小(&Z)： 微小 标题排列(&L)： 标题： 使用窗口分类(整个程序) 使用窗口标题 警告 - Breeze 设置 窗口分类名 窗口标识符 窗口属性选择 窗口标题 特定窗口优先规则 强度(&T)： 