��          �   %   �      @     A  !   Y      {     �      �     �     �  +        =     N     [  (   z     �  ,   �  7   �     !  7   :     r  ]   �  1   �  	   "     ,  "   ?  5   b    �     �  #   �     �               )     E  (   _     �     �  
   �  !   �     �  '   �  6   	     J	  8   a	     �	  T   �	  "   
     2
     9
  #   I
  2   m
                    
         	                                                                                                 Cannot start initramfs. Cannot start update-alternatives. Configure Plymouth Splash Screen Download New Splash Screens EMAIL OF TRANSLATORSYour emails Get New Boot Splash Screens... Initramfs failed to run. Initramfs returned with error condition %1. Install a theme. Marco Martin NAME OF TRANSLATORSYour names No theme specified in helper parameters. Plymouth theme installer Select a global splash screen for the system The theme to install, must be an existing archive file. Theme %1 does not exist. Theme corrupted: .plymouth file not found inside theme. Theme folder %1 does not exist. This module lets you configure the look of the whole workspace with some ready to go presets. Unable to authenticate/execute the action: %1, %2 Uninstall Uninstall a theme. update-alternatives failed to run. update-alternatives returned with error condition %1. Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-28 03:05+0200
PO-Revision-Date: 2018-02-12 05:47-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-stable/messages/kde-workspace/kcm_plymouth.pot
 无法启动 initramfs。 无法启动 update-alternatives。 配置 Plymouth 欢迎界面 下载新的闪屏 kde-china@kde.org 获取新的开机闪屏... Initramfs 运行失败。 Initramfs 返回了错误状态码 %1。 安装主题。 Marco Martin KDE 中国 帮助参数中未指定主题。 Plymouth 主题安装器 为系统选择一个全局欢迎界面 要安装的主题，必须为已存的压缩文件。 主题 %1 不存在。 主题已损坏：主题中未找到 .plymouth 文件。 主题文件夹 %1 不存在。 该模块可以帮助您使用某些建议预设值配置整个工作区的外观。 无法验证/执行操作：%1, %2 卸载 卸载主题。 update-alternatives 运行失败。 update-alternatives 返回了错误状态码 %1。 