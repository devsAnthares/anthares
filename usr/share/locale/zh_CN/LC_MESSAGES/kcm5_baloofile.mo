��          �            h     i     �  #   �      �      �     �  J        [  &   y     �  T   �       *   $     O    ]     p     �  #   �     �  (   �       3        L  !   h     �  T   �     �          $                            	                                             
       Also index file content Configure File Search Copyright 2007-2010 Sebastian Trüg Do not search in these locations EMAIL OF TRANSLATORSYour emails Enable File Search File Search helps you quickly locate all your files based on their content Folder %1 is already excluded Folder's parent %1 is already excluded NAME OF TRANSLATORSYour names Not allowed to exclude root folder, please disable File Search if you do not want it Sebastian Trüg Select the folder which should be excluded Vishesh Handa Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2018-02-12 05:47-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-stable/messages/kde-workspace/kcm5_baloofile.pot
 同时索引文件内容 配置文件搜索 Copyright 2007-2010 Sebastian Trüg 搜索时忽略如下位置 kde-china@kde.org, chaofeng111@gmail.com 启用文件搜索 文件搜索帮助您根据内容快速定位文件 已经排除了文件夹 %1 已经排除了上层文件夹 %1 KDE 中国, Feng Chao 不允许排除根文件夹。如果您不想使用它的话，请禁用文件搜索 Sebastian Trüg 设置需要排除的文件夹 Vishesh Handa 