��    $      <  5   \      0     1     6  )   J     t  $   �  &   �  #   �  !   �  "   !     D     T     f     z     �  J   �     �  L        [     c     k      {     �  
   �     �     �     �     �     �  "   �       )   9  <   c     �  ;   �     �         "	     '	     .	     K	  !   Y	     {	     �	     �	     �	  	   �	  
   �	     �	     �	     
  B   	
     L
  -   _
     �
     �
     �
     �
     �
     �
  
   �
     �
     �
     �
     �
            +     3   G     {     �     �     	                                                                         !       #             "                                        
           $                               100% @info:creditAuthor @info:creditCopyright 2015 Harald Sitter @info:creditHarald Sitter @labelNo Applications Playing Audio @labelNo Applications Recording Audio @labelNo Device Profiles Available @labelNo Input Devices Available @labelNo Output Devices Available @labelProfile: @titlePulseAudio @title:tabAdvanced @title:tabApplications @title:tabDevices Add virtual output device for simultaneous output on all local sound cards Advanced Output Configuration Automatically switch all running streams when a new output becomes available Capture Default Device Profiles EMAIL OF TRANSLATORSYour emails Inputs Mute audio NAME OF TRANSLATORSYour names Notification Sounds Outputs Playback Port Port is unavailable (unavailable) Port is unplugged (unplugged) Requires 'module-gconf' PulseAudio module This module allows to set up the Pulseaudio sound subsystem. label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-03 03:00+0200
PO-Revision-Date: 2018-02-12 05:47-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-stable/messages/kde-workspace/kcm_pulseaudio.pot
 100% 作者 Copyright 2015 Harald Sitter Harald Sitter 无应用程序正在播放音频 无应用程序正在录音 无可用设备配置 无可用输入设备 无可用输出设备 配置： PulseAudio 高级 应用程序 设备 一个可以同步输出到所有本地声卡的虚拟输出设备 高级输出配置 当新输出可用时，自动切换所有流 音频抓取 默认 设备配置 kde-china@kde.org 输入 静音 KDE 中国 通知声音 输出 音频播放 端口  (不可用)  (已拔出) 需要 'module-gconf' 的 PulseAudio 模块 此模块允许设置 Pulseaudio 声音子系统。 %1: %2 100% %1% 