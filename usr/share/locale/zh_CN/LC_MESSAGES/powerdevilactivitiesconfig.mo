��          �      |      �     �     �     �                 ;  	   \     f     �  &   �     �     �     �  	          �   %  �   �  t   ?  7   �  +   �              =     E     R     Y     l     �     �  
   �     �     �     �     �                 j     X   �  R   �     6	  )   E	     o	                                      
   	                                                         min Act like Always Define a special behavior Don't use special settings EMAIL OF TRANSLATORSYour emails Hibernate NAME OF TRANSLATORSYour names Never shutdown the screen Never suspend or shutdown the computer PC running on AC power PC running on battery power PC running on low battery Shut down Suspend The Power Management Service appears not to be running.
This can be solved by starting or scheduling it inside "Startup and Shutdown" The activity service is not running.
It is necessary to have the activity manager running to configure activity-specific power management behavior. The activity service is running with bare functionalities.
Names and icons of the activities might not be available. This is meant to be: Act like activity %1Activity "%1" Use separate settings (advanced users only) after Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-09 03:03+0200
PO-Revision-Date: 2018-02-12 05:47-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-stable/messages/kde-workspace/powerdevilactivitiesconfig.pot
  分钟 行为类似 总是 定义特殊行为 不使用特殊设置 kde-china@kde.org 休眠 KDE 中国 永不关闭屏幕 永不关闭或挂起计算机 电脑使用交流电 电脑使用电池 电池电量低 关机 挂起 电源管理服务未运行。
可以启动它或者在“启动和关闭系统”中设置自动启动。 活动服务未运行。
配置活动特定的电源管理行为需要活动管理器。 活动服务仅以基础功能运行。
活动的名称和图标可能不可用。 活动“%1” 使用单独设置(适用于高级用户) 如果空闲 