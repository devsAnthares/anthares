��          �      l      �  5   �  �    s    +   |     �     �     �     �           0     G  	   a  
   k     v     �     �  ,  �  H  �     9	  7   F	    ~	     �  �   �    Q  %   S     y     �     �     �     �     �     �  	   �  
     
             -    A  
  J     U  $   \                                                             	   
                                  %1 is language name, %2 is language code name%1 (%2) %1 is the language codeThe translation files for the language with the code '%2' could not be found. The language has been removed from your configuration. If you want to add it back, please install the localization files for it and add the language again. The translation files for the languages with the codes '%2' could not be found. These languages have been removed from your configuration. If you want to add them back, please install the localization files for it and the languages again. <h1>Translations</h1>
<p>Here you can set your preferred language for translating the user interface of your applications. You can choose a single language, or a list of languages to be applied in sequence. Only language translations that are installed on your system will be listed as available. If your language is not listed then you will need to install it first.</p> <p>Click here to install more languages</p> Applying Language Settings Available &Translations: Available Languages: Configure Plasma translations EMAIL OF TRANSLATORSYour emails Install more languages Install more translations John Layt Maintainer NAME OF TRANSLATORSYour names Preferred Languages: Preferred Trans&lations: This is the list of installed KDE Plasma language translations currently being used, listed in order of preference.  <br />If a translation is not available for the first language in the list, the next language will be used. <br /> If no other translations are available then US English will be used. This is the list of installed KDE Plasma language translations not currently being used. <br />To use a language translation move it to the 'Preferred Languages' list in the order of preference.  <br />If no suitable languages are listed, then you may need to install more language packages using your usual installation method. Translations Your changes will take effect the next time you log in. Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2018-02-12 05:47-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-stable/messages/kde-workspace/kcmtranslations.pot
 %1 (%2) 无法找到代码为“%2”的语言的翻译文件。这些语言已从您的配置中移除。如果您想再次添加它们，请再次安装对应的本地化文件和语言。 <h1>翻译</h1>
<p>您可以在此选择您的首选应用程序界面翻译语言。您可以选择一个语言，或者一系列语言。只有已在系统上安装的语言将被列为可用。如果您的语言未列出您需要首先将其安装。</p> <p>点击这里安装更多语言</p> 应用语言设置 可用翻译(&T)： 可用语言： 配置 Plasma 翻译 kde-china@kde.org 安装更多语言 安装更多翻译 John Layt Maintainer KDE 中国 首选语言： 首选翻译(&L)： 这是 KDE Plasma 当前已安装并正在使用的语言的翻译，它们是按照优先级排列的。<br />如果一个翻译对首选语言不可用，那么将会尝试使用下一个语言。<br />如果没有翻译可用，那么将会使用美国英语。 这是当前 KDE 工作空间已经安装但未使用的语言翻译。<br />如果想要使用某个语言的翻译，就将它移动到偏好语言中。<br />如果没有列出合适的语言，那么您可以按您通常的安装方式安装更多的语言包。 翻译 更改将在下次登录时生效。 