��            )         �     �  "   �     �  !   �  !        4  
   J     U     p     �  !   �     �  0   �  8        ?  !   ]          �     �     �     �                '  
   /  
   :  
   E  &   P     w     �    �     �     �     �     �          0     D     K     k     x  !   �     �  3   �  0   	     7	  $   M	  !   r	     �	     �	     �	     �	     �	     
     
  	   "
     ,
  	   9
  !   C
     e
     r
                                                                                                 	                       
                 ms &Keyboard accelerators visibility: &Top arrow button type: Always Hide Keyboard Accelerators Always Show Keyboard Accelerators Anima&tions duration: Animations Bottom arrow button t&ype: Breeze Settings Center tabbar tabs Drag windows from all empty areas Drag windows from titlebar only Drag windows from titlebar, menubar and toolbars Draw a thin line to indicate focus in menus and menubars Draw focus indicator in lists Draw frame around dockable panels Draw frame around page titles Draw frame around side panels Draw slider tick marks Draw toolbar item separators Enable animations Enable extended resize handles Frames General No Buttons One Button Scrollbars Show Keyboard Accelerators When Needed Two Buttons W&indows' drag mode: Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:42+0200
PO-Revision-Date: 2018-02-12 05:47-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-stable/messages/kde-workspace/breeze_style_config.pot
  毫秒 键盘快捷键可见性(&K)： 顶部箭头按钮类型(&T)： 总是隐藏键盘快捷键 总是显示键盘快捷键 动画长度(&T)： 动画 底部箭头按钮类型(&Y)： 微风设置 居中标签栏的标签 从所有空白区域拖曳窗口 只从标题栏上拖曳窗口 从标题栏、菜单栏和工具栏上拖曳窗口 绘制细线以指示菜单和菜单栏的焦点 绘制焦点指示器 绘制可停靠面板周围的框架 绘制页面标题周围的框架 绘制侧面板周围的框架 绘制滑块刻度标记 绘制工具栏项目分隔符 启用动画 启用扩展的缩放控制 框架 常规 无按钮 一个按钮 滚动栏 在需要时显示键盘快捷键 两个按钮 窗口拖曳模式(&I)： 