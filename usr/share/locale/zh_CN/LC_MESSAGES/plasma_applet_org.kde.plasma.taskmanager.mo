��    Q      �  m   ,      �     �     �     �                      .   !  U   P     �     �      �     �  /   �  /   -     ]     i     r  
   ~     �     �  $   �     �     �     �     �     	     	  	   *	     4	  
   F	  7   Q	     �	     �	     �	     �	  	   �	     �	  !   �	     
     
  	   !
      +
     L
     Y
     r
     �
     �
     �
     �
     �
     �
     �
  (   �
       =   '  2   e     �     �  "   �     �       J     1   Y  )   �  (   �  '   �  "     4   )     ^     l     r     {     �     �     �  U   �  N   !  9   p  J   �  ,  �     "  
   3  
   >  
   I     T  
   e  
   p     {     �     �     �     �     �  '   �  '        .     5     <  	   I     S     c     v  	   �  	   �     �     �     �     �  	   �     �     �     
               0     L     P     ^  '   e     �     �     �     �     �     �     �          !     (     8     ?     O     _  !   r     �     �     �     �  	   �  	   �     �  
   �     �     �          +     J     i  -   �     �     �  	   �     �     �     �     �  
   �       	                L   )             O                      4              6   $   ?   3         B              (   K   Q               ;       .   '   D   2   -       J   >   !   
   8                       P   5         ,          7   <           F      *   @       H   1                 "          9      +   &   0           /   N          #       E   	             I   A   =   %      G   :   C                   M    &All Desktops &Close &Fullscreen &Move &New Desktop &Pin &Shade 1 = number of desktop, 2 = desktop name&%1 %2 Activities a window is currently on (apart from the current one)Also available on %1 Add To Current Activity All Activities Allow this program to be grouped Alphabetically Always arrange tasks in columns of as many rows Always arrange tasks in rows of as many columns Arrangement Behavior By Activity By Desktop By Program Name Close Window or Group Cycle through tasks with mouse wheel Do Not Group Do Not Sort Filters Forget Recent Documents General Grouping and Sorting Grouping: Highlight windows Icon size: Invalid number of new messages, overlay, keep short— Keep &Above Others Keep &Below Others Keep launchers separate Large Ma&ximize Manually Mark applications that play audio Maximum columns: Maximum rows: Mi&nimize Minimize/Restore Window or Group More Actions Move &To Current Desktop Move To &Activity Move To &Desktop Mute New Instance On %1 On All Activities On The Current Activity On middle-click: Only group when the task manager is full Open groups in popups Open or bring to the front window of media player appRestore Over 9999 new messages, overlay, keep short9,999+ Pause playbackPause Play next trackNext Track Play previous trackPrevious Track Quit media player appQuit Re&size Remove launcher button for application shown while it is not runningUnpin Show all user Places%1 more Place %1 more Places Show only tasks from the current activity Show only tasks from the current desktop Show only tasks from the current screen Show only tasks that are minimized Show progress and status information in task buttons Show tooltips Small Sorting: Start New Instance Start playbackPlay Stop playbackStop The click actionNone Toggle action for showing a launcher button while the application is not running&Pin When clicking it would toggle grouping windows of a specific appGroup/Ungroup Which activities a window is currently onAvailable on %1 Which virtual desktop a window is currently onAvailable on all activities Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2018-02-12 05:47-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-stable/messages/kde-workspace/plasma_applet_org.kde.plasma.taskmanager.pot
 全部桌面(&A) 关闭(&C) 全屏(&F) 移动(&M) 新建桌面(&N) 固定(&P) 卷起(&S) &%1 %2 同时还位于 %1 添加到当前活动 全部活动 允许分组此程序 按字母顺序 总是尽可能多行按列排列任务 总是尽可能多列按行排列任务 排列 行为 根据活动 按桌面 按程序名称 关闭窗口或组 使用鼠标滚轮循环任务 不分组 不排序 过滤 忘记最近的文档 常规 分组和排序 分组： 突出显示窗口 图标大小： — 常居顶端(&A) 常居底端(&B) 保持启动器分隔放置 大 最大化(&X) 手动 标记正在播放音频的应用程序 最大列数： 最大行数： 最小化(&N) 最小化/恢复窗口或组 更多动作 移至当前桌面(&T) 移动到活动(&A) 移至桌面(&D) 静音 启动新实例 于 %1 在全部活动 在当前活动 中键点击时： 仅当任务管理器满时分组 在弹出窗口中打开组 恢复 9,999+ 暂停 下一首 上一首 退出 大小(&S) 取消固定 另外 %1 个位置 仅显示当前活动的任务 仅显示当前桌面的任务 仅显示当前屏幕的任务 仅显示最小化的任务 在任务按钮上显示进度和状态信息 显示工具提示 小 排序： 启动新实例 播放 停止 无 固定(&P) 组合/解散 位于 %1 在全部活动中可见 