��            )   �      �     �  
   �  /   �  -   �          !  
   2     =     J     `  W   i     �  ,   �  	                  !     '     -  
   :     E     W     o     �     �     �     �  1   �       (       B  
   H  	   S     ]     j     }     �     �     �     �  T   �       3   #  	   W     a     q  	   x     �     �     �     �     �     �     �     �     	     	     1	     A	                                                                                  
                                	                       %1@%2 %2@%3 (%1) @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Favorites All Applications Appearance Applications Applications updated. Computer Drag tabs between the boxes to show/hide them, or reorder the visible tabs by dragging. Edit Applications... Expand search to bookmarks, files and emails Favorites Hidden Tabs History Icon: Leave Menu Buttons Often Used On All Activities On The Current Activity Remove from Favorites Show In Favorites Show applications by name Sort alphabetically Switch tabs on hover Type is a verb here, not a nounType to search... Visible Tabs Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2018-02-12 05:47-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-stable/messages/kde-workspace/plasma_applet_org.kde.plasma.kickoff.pot
 %1@%2 %2@%3 (%1) 选择... 清除图标 添加到收藏夹 全部应用程序 外观 程序 程序已更新。 电脑 在方框之间拖拽以显示或隐藏它们，或者拖拽排序可见标签页。 编辑应用程序... 扩展搜索范围到书签，文件和电子邮件 收藏夹 隐藏标签页 历史 图标： 离开 菜单按钮 常用 在全部活动 在当前活动 从收藏夹中删除 在收藏夹中显示 按名称显示应用程序 按字母顺序排序 悬停时切换标签 输入搜索... 可见标签页 