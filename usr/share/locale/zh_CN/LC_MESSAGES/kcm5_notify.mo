��          �      �       0     1  �  H     �       &         ?     `     n     v     �     �  (   �    �  !   �  c       |     �     �     �     �     �  
   �     �     �              
                 	                                (c) 2002-2006 KDE Team <h1>System Notifications</h1>Plasma allows for a great deal of control over how you will be notified when certain events occur. There are several choices as to how you are notified:<ul><li>As the application was originally designed.</li><li>With a beep or other noise.</li><li>Via a popup dialog box with additional information.</li><li>By recording the event in a logfile without any additional visual or audible alert.</li></ul> Carsten Pfeiffer Charles Samuels Disable sounds for all of these events EMAIL OF TRANSLATORSYour emails Event source: KNotify NAME OF TRANSLATORSYour names Olivier Goffart Original implementation System Notification Control Panel Module Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-09 07:18+0200
PO-Revision-Date: 2018-02-12 05:47-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-stable/messages/kde-workspace/kcm5_notify.pot
 (c) 2002-2006 KDE 开发组团队 <h1>系统通知</h1>Plasma 允许控制特定事件发生时您会如何收到通知。有许多选择收到通知的选择：<ul><li>按程序最初设计的方式。</li><li>滴滴声或者其他声音。</li><li>通过一个含有额外信息的弹出窗口。</li><li>记录事件到日志文件中，不显示任何视觉或声音提示。</li></ul> Carsten Pfeiffer Charles Samuels 禁用这些事件的声音 kde-china@kde.org 事件来源： KNotify KDE 中国 Olivier Goffart 初始设计 系统通知控制面板模块 