��    2      �  C   <      H     I     h     �     �     �     �     �     �  -   �  �     r   �  }     �   �  �   %  �   �  �   w  g   	  h   ~	  Y   �	  Y   A
  ]   �
     �
     �
  
             $  
   9     D  	   M     W  
   n     y  ]   �     �  I   �     H  %   a      �     �     �     �     �  H   
     S  -   q     �     �  *   �     �    �     �               1     D     Q     ^     k  !   �     �  3   �  <   �  +   1  %   ]  :   �  +   �  6   �  '   !  !   I  '   k  '   �     �     �     �     �     �     �       	             (     =     P     i  B   |     �     �     �     
       	   *  	   4  3   >     r  	   �     �  	   �  "   �     �     $                                      /       1   !                 )   	                 *   
   +      ,               (                            &       "         #      2   .                             '   0             %   -    @title:windowChange your Face @title:windowChoose Image Add user account Choose from Gallery... Clear Avatar Delete User Delete files Email Address: Enable administrator privileges for this user Error returned when the password is invalidThe password should be at least %1 character The password should be at least %1 characters Error returned when the password is invalidThe password should be more varied in letters, numbers and punctuation Error returned when the password is invalidThe password should contain a mixture of letters, numbers, spaces and punctuation Error returned when the password is invalidThe password should contain at least %1 lowercase letter The password should contain at least %1 lowercase letters Error returned when the password is invalidThe password should contain at least %1 number The password should contain at least %1 numbers Error returned when the password is invalidThe password should contain at least %1 special character (like punctuation) The password should contain at least %1 special characters (like punctuation) Error returned when the password is invalidThe password should contain at least %1 uppercase letter The password should contain at least %1 uppercase letters Error returned when the password is invalidThe password should not contain sequences like 1234 or abcd Error returned when the password is invalidThe password should not contain too many repeated characters Error returned when the password is invalidThis password can't be used, it is too simple Error returned when the password is invalidYour name should not be part of your password Error returned when the password is invalidYour username should not be part of your password John John Doe Keep files Load from file... Log in automatically MyPassword New User Password: Passwords do not match Real Name: Remove user account Returned when a more specific error message has not been foundPlease choose another password Select a new face: The username can contain only letters, numbers, score, underscore and dot The username is too long The username must start with a letter This e-mail address is incorrect This password is excellent This password is good This password is very good This password is weak This user is using the system right now, removing it will cause problems This username is already used Title for change password dialogNew Password Users Verify: What do you want to do after deleting %1 ? john.doe@example.com Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-11 03:21+0100
PO-Revision-Date: 2018-02-12 05:47-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-stable/messages/kde-workspace/user_manager.pot
 更改您的头像 选择图像 添加用户账户 从图库选择... 清除头像 删除用户 删除文件 电子邮件地址： 为此用户启用管理员权限 密码至少要 %1 个字符 密码应当包含多种字母，数字，和标点 密码应当包含字母，数字，空格和标点的混合 密码应当至少包含 %1 个小写字符 密码应当至少包含 %1 个数字 密码应当至少包含 %1 个特殊字符 (例如标点) 密码应当至少包含 %1 个大写字符 密码不应当包含像 1234 和 abcd 这样的序列 密码不应当包含太多重复字符 密码无法使用，太简单了 姓名不应当为您密码的一部分 用户名不应当为密码的一部分 John 李郎 保留文件 从文件装入... 自动登录 我的密码 新建用户 密码： 密码不匹配 真实姓名(&R)：: 删除用户账户 请选择另一个密码 选择新头像： 用户名仅可包括字母，数字，横线，下划线和点。 用户名过长 用户名必须以字母开头 电子邮件地址不正确 密码非常强 密码一般强 密码强 密码弱 此用户正在使用系统，移除会导致问题 用户名已被使用 新密码 用户 验证： 删除 %1 后您想要做什么？ john.doe@example.com 