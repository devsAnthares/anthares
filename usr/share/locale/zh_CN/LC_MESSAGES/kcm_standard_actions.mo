��          L      |       �       �      �   #   �        �        �        
             6  �   F                                         EMAIL OF TRANSLATORSYour emails NAME OF TRANSLATORSYour names Standard Actions successfully saved Standard Shortcuts The changes have been saved. Please note that:<ul><li>Applications need to be restarted to see the changes.</li>    <li>This change could introduce shortcut conflicts in some applications.</li></ul> Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2018-02-12 05:47-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-stable/messages/kde-workspace/kcm_standard_actions.pot
 kde-china@kde.org KDE 中国 成功保存标准动作 标准快捷键 更改已保存。请注意：<ul><li>应用程序需要重启以应用该更改。</li>    <li>此更改在某些应用程序中会导致快捷键冲突。</li></ul> 