��          �      l      �  3   �  $     �   :     �  4   �     �  #        =  �   E  �   �  +   �     �     �     �  1     (   3     \  �   s  $   �  (     /  F     v  	   �     �     �  9   �     �  $   	     2	  s   9	  �   �	  $   E
     j
     �
     �
     �
     �
     �
     �
     �
                                                        	   
                                            1 action for this device %1 actions for this device @info:status Free disk space%1 free Accessing is a less technical word for Mounting; translation should be short and mean 'Currently mounting this device'Accessing... All devices Click to access this device from other applications. Click to eject this disc. Click to safely remove this device. General It is currently <b>not safe</b> to remove this device: applications may be accessing it. Click the eject button to safely remove this device. It is currently <b>not safe</b> to remove this device: applications may be accessing other volumes on this device. Click the eject button on these other volumes to safely remove this device. It is currently safe to remove this device. Most Recent Device No Devices Available Non-removable devices only Open auto mounter kcmConfigure Removable Devices Open popup when new device is plugged in Removable devices only Removing is a less technical word for Unmounting; translation shoud be short and mean 'Currently unmounting this device'Removing... This device is currently accessible. This device is not currently accessible. Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2018-02-12 05:47-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-stable/messages/kde-workspace/plasma_applet_org.kde.plasma.devicenotifier.pot
 此设备有 %1 种操作 %1 空闲 访问中... 全部设备 单击这里可通过其它应用程序访问此设备。 单击这里可弹出盘片。 单击这里可安全移除设备。 常规 现在移除设备<b>不安全</b>：可能有程序还在访问此设备。单击弹出键可将其安全移除。 现在移除设备<b>不安全</b>：可能有程序正在访问此设备的其它分卷。在其它分卷上单击弹出键后可将其安全移除。 现在可以安全移除此设备。 最近使用的设备 无可用设备 只有不可移动设备 配置便携设备 在新设备插入时弹出 只有移动设备 移除中... 此设备目前可访问。 此设备目前无法访问。 