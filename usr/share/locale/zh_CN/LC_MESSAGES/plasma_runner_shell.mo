��          |      �          �   !  �   �  9   �  7   �  8     
   H     S     Z     s     �  
   �    �  x   �  �   >  3     0   P  3   �     �  	   �     �     �                   
                    	                              Check this option if the application you want to run is a text mode application. The application will then be run in a terminal emulator window. Check this option if you want to run the application with a different user id. Every process has a user id associated with it. This id code determines file access and other permissions. The password of the user is required to do this. Enter the password here for the user you specified above. Enter the user you want to run the application as here. Finds commands that match :q:, using common shell syntax Pass&word: Run %1 Run as a different &user Run in &terminal window Run in Terminal Window User&name: Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-12-17 03:48+0100
PO-Revision-Date: 2018-02-12 05:47-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-stable/messages/kde-workspace/plasma_runner_shell.pot
 如果您要在文字模式下运行此程序，请选中此项。程序将会在一个终端仿真器窗口内运行。 如果您想要程序以不同的用户 ID 运行，请选中此项。每个进程都有对应的用户 ID，此 ID 决定了相应进程的文件访问权限和其它权限。您需要为此提供那个用户的密码。 在此输入您在上面指定的用户的密码。 在此输入要以哪个用户来运行程序。 使用通用 shell 语法查找匹配 :q: 的命令 密码(&W)： 运行 %1 以不同用户运行(&U) 在终端窗口内运行(&T) 在终端窗口内运行 用户名(&N)： 