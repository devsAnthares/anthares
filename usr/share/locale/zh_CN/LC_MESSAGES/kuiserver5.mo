��          �   %   �      P     Q     b     w     �     �     �  
   �     �     �     �     �     �  /        3     Q     p     v     �     �     �     �     �     �     �         %     4     A      Q  2   r     �  %   �     �     �     �     �  	   �     �  -        <     X     w     ~     �     �     �  !   �     �     �          4                            
                                                                                             	    %1 file %1 files %1 folder %1 folders %1 of %2 processed %1 of %2 processed at %3/s %1 processed %1 processed at %2/s Appearance Behavior Cancel Clear Configure... Finished Jobs List of running file transfers/jobs (kuiserver) Move them to a different list Move them to a different list. Pause Remove them Remove them. Resume Show all jobs in a list Show all jobs in a list. Show all jobs in a tree Show all jobs in a tree. Show separate windows Show separate windows. Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2018-02-12 05:47-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-stable/messages/kde-workspace/kuiserver5.pot
 %1 个文件 %1 个文件夹 已处理了 %1 个，共 %2 个 已于 %3/s 的速度处理了 %1 个，共 %2 个 处理了 %1 已于 %2/s 的速度处理了 %1 个 外观 行为 取消 清除 配置... 已完成任务 列出所有正在运行的文件处理任务 将其移至不同的列表 将其移至不同的列表。 暂停 删除列表 删除列表。 继续 在列表中显示全部任务 在列表中显示全部任务。 以树形显示全部任务 以树形显示全部任务。 显示单独的窗口 显示单独的窗口。 