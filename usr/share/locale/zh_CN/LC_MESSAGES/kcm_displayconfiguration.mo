��    5      �  G   l      �     �     �     �     �     �     �  !   �  '        >     M  -   _     �     �     �     �     �     �     �     �           
     +  	   3     =     N     [  
   i     t     �     �  A   �                     %     2     @     L  <   Z  <   �     �  3   �       �        �     �  ;   �     	     	     *	  %   6	  b   \	    �	     �     �     �     
     (     7     F     Y     l     y  '   �     �     �     �  	   �  	   �     �     �            )   (  	   R  	   \     f     s     �  	   �     �     �     �  <   �       	        '  	   7     A     N     [     h     k  	   n     x     �  �   �     9     B  9   K     �     �     �  $   �  1   �             ,      &      3           0   #                 *      /      5       -                                       '   (   !   )   2   4             "          %                         .   +                    1                   
   	         $    %1 Hz &Disable All Outputs &Reconfigure (c), 2012-2013 Daniel Vrátil 90° Clockwise 90° Counterclockwise @title:windowDisable All Outputs @title:windowUnsupported Configuration Active profile Advanced Settings Are you sure you want to disable all outputs? Auto Break Unified Outputs Button Checkbox Combobox Configuration for displays Daniel Vrátil Display Configuration Display: EMAIL OF TRANSLATORSYour emails Enabled Group Box Identify outputs KCM Test App Laptop Screen Maintainer NAME OF TRANSLATORSYour names No Primary Output No available resolutions No kscreen backend found. Please check your kscreen installation. Normal Orientation: Primary display: Radio button Refresh rate: Resolution: Scale Display Scale multiplier, show everything at 1 times normal scale1x Scale multiplier, show everything at 2 times normal scale2x Scale: Scaling changes will come into effect after restart Screen Scaling Sorry, your configuration could not be applied.

Common reasons are that the overall screen size is too big, or you enabled more displays than supported by your GPU. Tab 1 Tab 2 Tip: Hold Ctrl while dragging a display to disable snapping Unified Outputs Unify Outputs Upside Down Warning: There are no active outputs! Your system only supports up to %1 active screen Your system only supports up to %1 active screens Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-24 03:18+0200
PO-Revision-Date: 2018-02-12 05:47-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-stable/messages/kde-workspace/kcm_displayconfiguration.pot
 %1 Hz 禁用所有输出(&D) 重新配置(&R) (c), 2012-2013 Daniel Vrátil 90° 顺时针 90° 逆时针 禁用所有输出 不支持的配置 激活配置 高级设置 您确定想要禁用所有输出吗？ 自动 取消统一输出 按钮 复选框 组合框 显示器的配置信息 Daniel Vrátil 显示器配置 显示器： kde-china@kde.org,guoyunhebrave@gmail.com 已启用 分组框 标识输出 KCM 测试程序 笔记本屏幕 维护者 KDE 中国,Guo Yunhe 无主显示输出 无可用分辨率 未找到 kscreen 后端。请检查您的 kscreen 安装。 普通 方向： 主显示器： 单选钮 刷新率： 分辨率： 缩放显示 1x 2x 缩放： 缩放将会在重启后生效 屏幕缩放 抱歉，您的设置无法应用。

通常的原因是整体显示器尺寸过大，或者您启用的显示器数量超过 GPU 支持的上限。 标签 1 标签 2 提示：拖拽显示器时按住 Ctrl 禁用自动对齐 统一输出 统一输出 上下颠倒 警告：无活动的显示输出！ 您的系统只支持最多 %1 个活动的屏幕 