��    @        Y         �     �     �     �     �     �     �  �  �  �   �  -   =	  )   k	  -   �	  +   �	  r   �	  	   b
  	   l
     v
     �
     �
     �
  
   �
     �
     �
     �
     �
      �
               4     :     G      Y     z     �  r   �  5        D     V     p     �     �     �  	   �     �     �     �  (   �                '     A     \     b  +   n  3   �     �     �     �     �  "   �  S     )   p     �  �   �    �     �     �     �     �  
   �     �  �    {   �  0        =     D     K  W   R  
   �     �     �     �     �     �  	   �             	             2     D     Q     d     q     ~     �     �     �  t   �  !   E     g     w     �     �  
   �     �     �     �     �     �     �          #     <     U  	   n  	   x  0   �  *   �     �     �  	   �     �       D     $   d     �  �   �            1           7   <          >                   $                         9   +   .   *       &   ;         '                   (   
   8   4      #              3          %      :           	   @   5   =          ?      )   ,       /       -               6   !                     "            2             0       &Amount: &Effect: &Second color: &Semi-transparent &Theme (c) 2000-2003 Geert Jansen <h1>Icons</h1>This module allows you to choose the icons for your desktop.<p>To choose an icon theme, click on its name and apply your choice by pressing the "Apply" button below. If you do not want to apply your choice you can press the "Reset" button to discard your changes.</p><p>By pressing the "Install Theme File..." button you can install your new icon theme by writing its location in the box or browsing to the location. Press the "OK" button to finish the installation.</p><p>The "Remove Theme" button will only be activated if you select a theme that you installed using this module. You are not able to remove globally installed themes here.</p><p>You can also specify effects that should be applied to the icons.</p> <qt>Are you sure you want to remove the <strong>%1</strong> icon theme?<br /><br />This will delete the files installed by this theme.</qt> <qt>Installing <strong>%1</strong> theme</qt> @label The icon rendered as activeActive @label The icon rendered as disabledDisabled @label The icon rendered by defaultDefault A problem occurred during the installation process; however, most of the themes in the archive have been installed Ad&vanced All Icons Antonio Larrosa Jimenez Co&lor: Colorize Confirmation Desaturate Description Desktop Dialogs Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Effect Parameters Enable icon animations Gamma Geert Jansen Get New Themes... Get new themes from the Internet Icons Icons Control Panel Module If you already have a theme archive locally, this button will unpack it and make it available for KDE applications Install a theme archive file you already have locally Install from File Installing icon themes... Jonathan Riddell Main Toolbar NAME OF TRANSLATORSYour names Name No Effect Panel Preview Remove Theme Remove the selected theme from your disk Set Effect... Setup Active Icon Effect Setup Default Icon Effect Setup Disabled Icon Effect Size: Small Icons The file is not a valid icon theme archive. This will remove the selected theme from your disk. To Gray To Monochrome Toolbar Torsten Rahn Unable to create a temporary file. Unable to download the icon theme archive;
please check that address %1 is correct. Unable to find the icon theme archive %1. Use of Icon You need to be connected to the Internet to use this action. A dialog will display a list of themes from the http://www.kde.org website. Clicking the Install button associated with a theme will install this theme locally. Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-08 06:27+0100
PO-Revision-Date: 2018-02-12 05:47-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-stable/messages/kde-workspace/kcm5_icons.pot
 效果强度(&A)： 特殊效果(&E)： 第二颜色(&S)： 半透明(&S) 主题(&T) (c) 2000-2003 Geert Jansen <h1>图标</h1>此模块允许您为您的桌面选择图标。<p>要选择图标主题，请单击该主题的名称再单击下面的“应用”按钮应用您的选择。如果您不想应用您的选择，您可以按“重置”按钮丢弃您的更改。</p><p>按下“安装新主题...”按钮，您可以安装您找到的新图标主题，只需在对话框中输入其位置或者浏览到该位置。单击“确定”即可完成安装。</p><p>“删除主题”按钮仅当您选择了使用此模块安装的主题后才会激活。您不能在此删除全局安装的主题。</p><p>您也可以指定应用于图标的效果。</p> <qt>您确定要删除图标主题 <strong>%1</strong> 吗？<br /><br />这会删除这个主题所安装的文件。</qt> <qt>正在安装 <strong>%1</strong> 主题</qt> 活动 禁用 默认 在安装中出现了问题；但是，归档中的大部分主题已经安装完成。 高级(&V) 全部图标 Antonio Larrosa Jimenez 颜色(&L)： 亮彩 确认 去饱和 描述 桌面 对话框 拖放或输入主题 URL kde-china@kde.org 效果参数 启用图标动画 伽玛控制 Geert Jansen 获得新主题... 从互联网上获得新主题 图标 图标控制面板模块 如果在您的本地资源中已经有一个图标主题归档，此按钮会将其解压并应用到 KDE 程序中 安装本地的主题归档文件 从文件安装 正在安装图标主题... Jonathan Riddell 主工具栏 KDE 中国 名称 无特殊效果 面板 预览 删除主题 从磁盘上删除所选主题 设置特殊效果... 设置活动图标效果 设置默认图标效果 设置禁用图标效果 大小： 小图标 文件不是一个有效的图标主题归档。 这将从磁盘上删除所选的主题。 灰度 单色 工具栏 Torsten Rahn 无法创建临时文件。 无法下载图标主题归档；
请检查地址 %1 是否正确。 无法找到图标主题归档 %1。 图标用途 您需要为此操作连接到互联网，它将从 http://www.kde.org 网站上获得一份主题列表并显示。点击安装按钮即可把相应主题安装到本地。 