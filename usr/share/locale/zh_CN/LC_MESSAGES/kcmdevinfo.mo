��    ]           �      �  !   �       	   !     +  S   4  	   �     �     �     �     �     �     �     �     �     �     	  J   $	     o	     }	     �	      �	  	   �	  
   �	     �	     �	     �	     �	     
     
     
  L   
  	   l
  	   v
  
   �
  
   �
  
   �
     �
     �
     �
     �
     �
     �
     �
     �
     	     (  	   +     5     G     S     [     i     m     }     �     �  
   �  	   �     �  
   �     �     �     �     �     �     	          +     ?     \     r     x     |     �  H   �     �     �     �     �     �       
     &        A  "   T     w     �     �     �     �       	       (  &   7     ^  	   t     ~     �     �     �  	   �     �     �     �  	   �     �     �     	       $        B     R  	   Y     c  	   u          �     �     �     �     �     �     �  $   �  	     	   !  
   +  
   6  
   A     L     S  	   a     k     {     �     �     �  
   �     �  	   �     �  	   �  	   �     �       	     	             '  	   :     D     Q     V     i     n     s     �     �     �     �     �     �     �                      )        G  	   W     a     n     ~     �  
   �     �     �     �     �     �     �     �     �     �     �           B   "       (   W   #   $   %            [   @      L   M   '   6       >   T           <   9   G   &           A      .   8   R       7       J             ?   4                   H   -                      F          5   )   N   D       ,       K             3       U   =   
       ;       O   :           +           !   Z   /      V   Y              Q         E       1       X             ]       0      P         \   I          	          C       S   2       *    
Solid Based Device Viewer Module (c) 2010 David Hubner AMD 3DNow ATI IVEC Available space out of total partition size (percent used)%1 free of %2 (%3% used) Batteries Battery Type:  Bus:  Camera Cameras Charge Status:  Charging Collapse All Compact Flash Reader Default device tooltipA Device Device Information Device Listing Whats ThisShows all the devices that are currently listed. Device Viewer Devices Discharging EMAIL OF TRANSLATORSYour emails Encrypted Expand All File System File System Type:  Fully Charged Hard Disk Drive Hotpluggable? IDE IEEE1394 Info Panel Whats ThisShows information about the currently selected device. Intel MMX Intel SSE Intel SSE2 Intel SSE3 Intel SSE4 Keyboard Keyboard + Mouse Label:  Max Speed:  Memory Stick Reader Mounted At:  Mouse Multimedia Players NAME OF TRANSLATORSYour names No No Charge No data available Not Mounted Not Set Optical Drive PDA Partition Table Primary Processor %1 Processor Number:  Processors Product:  Raid Removable? SATA SCSI SD/MMC Reader Show All Devices Show Relevant Devices Smart Media Reader Storage Drives Supported Drivers:  Supported Instruction Sets:  Supported Protocols:  UDI:  UPS USB UUID:  Udi Whats ThisShows the current device's UDI (Unique Device Identifier) Unknown Drive Unused Vendor:  Volume Space: Volume Usage:  Yes kcmdevinfo name of something is not knownUnknown no device UDINone no instruction set extensionsNone platform storage busPlatform unknown battery typeUnknown unknown deviceUnknown unknown device typeUnknown unknown storage busUnknown unknown volume usageUnknown xD Reader Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-23 03:11+0200
PO-Revision-Date: 2018-02-12 05:47-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-stable/messages/kde-workspace/kcmdevinfo.pot
 
基于 Solid 的设备查看器模块 (c) 2010 David Hubner AMD 3DNow ATI IVEC 共 %2，剩余 %1(已用 %3%) 电池 电池类型： 总线： 相机 相机 充电状态： 充电中 全部折叠 CF 读卡器 设备 设备信息 显示所有当前列出的设备。 设备查看器 设备 放电中 kde-china@kde.org 已加密 全部展开 文件系统 文件系统类型： 充电完成 硬盘驱动器 是否支持热插拔？ IDE IEEE1394 显示当前所选设备的信息。 Intel MMX Intel SSE Intel SSE2 Intel SSE3 Intel SSE4 键盘 键盘+鼠标 标签： 最高速度： 记忆棒读取器 挂载点： 鼠标 多媒体播放器 KDE 中国 否 未充电 无可用数据 未挂载 未设定 光盘驱动器 PDA 分区表 主设备 处理器 %1 处理器编号： 处理器 制造商： Raid 是否可移动？ SATA SCSI SD/MMC 读卡器 显示全部设备 显示相关设备 智能介质读取器 存储驱动器 支持驱动： 支持的指令集： 支持协议： UDI： UPS USB UUID： 显示当前设备的唯一标识符(UDI) 未知驱动器 未使用 销售商： 分卷空间： 分卷使用率： 是 kcmdevinfo 未知 无 无 平台 未知 未知 未知 未知 未知 xD 读卡器 