��    _                   C   	  /   M  -   }     �     �     �     �      	     	     1	     >	     J	  
   S	     ^	     g	     p	     �	  	   �	     �	     �	     �	  ,   �	  	    
     

  
   )
     4
     L
     `
     u
     �
     �
     �
     �
     �
  	   �
     �
     �
     
               !     (  	   ;     E     ]  
   r     }     �  
   �     �     �     �  
   �     �     �               $     2     @     R     h     y     �     �     �     �  	   �     �     �     �               4     Q     j     �     �     �     �  	   �     �  ,   �          !     0     @     L     S     b     t     �  #   �     �  '  �     �  	                  -     @     Y     u     �     �     �     �     �  	   �  	   �     �     �     �     �          (  3   5  	   i     s     �     �     �     �     �                &     <     C     K  	   R     \  	   o     y     �     �     �     �     �     �     �     �     �     �               &     6     =     D     Z     j     z     �     �     �     �     �     �     �          
  	        !     6     =     V     l     �     �     �     �     �     �  	   	            $   0     U     \     l     |     �     �     �     �  &   �  '   �              Q         Y   5           %       H       J   !              \   *       @   V   4   [       A   (   
       U   B   ]   ^       $   _         '   >       D   K      -                  ?      ,   O   0       R   8   6   W   2   I   =   X   E   #       N   C       T   G   M          3   "         9             +          S          F           ;   :                    &   <   L          P         7          .       /       1                     )            	   Z       @action opens a software center with the applicationManage '%1'... @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Desktop Add to Favorites Add to Panel (Widget) Align search results to bottom All Applications App name (Generic name)%1 (%2) Applications Apps & Docs Behavior Categories Computer Contacts Description (Name) Description only Documents Edit Application... Edit Applications... End session Expand search to bookmarks, files and emails Favorites Flatten menu to a single level Forget All Forget All Applications Forget All Contacts Forget All Documents Forget Application Forget Contact Forget Document Forget Recent Documents General Generic name (App name)%1 (%2) Hibernate Hide %1 Hide Application Icon: Lock Lock screen Logout Name (Description) Name only Often Used Applications Often Used Documents Often used On All Activities On The Current Activity Open with: Pin to Task Manager Places Power / Session Properties Reboot Recent Applications Recent Contacts Recent Documents Recently Used Recently used Removable Storage Remove from Favorites Restart computer Run Command... Run a command or a search query Save Session Search Search results Search... Searching for '%1' Session Show Contact Information... Show In Favorites Show applications as: Show often used applications Show often used contacts Show often used documents Show recent applications Show recent contacts Show recent documents Show: Shut Down Sort alphabetically Start a parallel session as a different user Suspend Suspend to RAM Suspend to disk Switch User System System actions Turn off computer Type to search. Unhide Applications in '%1' Unhide Applications in this Submenu Widgets Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2018-02-12 05:47-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-stable/messages/kde-workspace/plasma_applet_org.kde.plasma.kicker.pot
 管理“%1”... 选择... 清楚图标 添加到桌面 添加到收藏夹 添加到面板 (部件) 搜索结果和底部对齐 全部应用程序 %1 (%2) 应用程序 应用程序 & 文档 行为 类别 计算机 联系人 描述 (名称) 只显示描述 文档 编辑应用程序... 编辑应用程序... 结束会话 扩展搜索范围至书签，文件和电子邮件 收藏夹 扁平化菜单到一级 清除全部 清除所有最近应用程序 忘记所有联系人 清除所有最近文档 清除最近应用程序 忘记联系人 清除最近的文档 忘记最近的文档 常规 %1 (%2) 休眠 隐藏 %1 隐藏应用程序 图标： 锁定 锁定屏幕 注销 名称  (描述) 只显示名称 常用应用 常用文档 常用 在全部活动 在当前活动 打开方式： 固定到任务管理器 位置 电源 / 会话 属性 重启 最近使用的程序 最近联系人 最近的文档 最近使用 最近使用 可移动存储 从收藏夹中删除 重启计算机 运行命令... 运行命令或搜索查询 保存会话 搜索 搜索结果 搜索... 正在搜索“%1” 会话 显示联系人信息... 在收藏夹中显示 显示应用程序为： 显示常用应用 显示常用联系人 显示常用文档 显示最近使用的程序 显示最近联系人 显示最近的文档 显示： 关机 按字母顺序排序 以不同的用户启动并行会话 挂起 挂起到内存 挂起到磁盘 切换用户 系统 系统动作 关闭计算机 输入搜索。 取消隐藏“%1”中的应用程序 取消隐藏此子菜单的应用程序 部件 