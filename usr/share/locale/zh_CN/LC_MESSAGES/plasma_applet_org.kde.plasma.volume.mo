��    &      L  5   |      P     Q  8   S     �     �     �     �     �     �  3   �  >        N     i     y     �  m   �     �          "     2     7     ?  *   O      z     �     �  "   �     �     �          3     :     H     X     e     �  ;   �     �  '  �     	     	     0	     =	     J	     W	     ^	  	   k	     u	     |	     �	     �	     �	     �	     �	     �	     �	     �	     �	  	   �	     �	  '   
     7
     V
  	   c
     m
     z
     �
     �
     �
     �
     �
     �
     �
     �
                  #   %                                                              "          $                      
                                           &      !                	        % Accessibility data on volume sliderAdjust volume for %1 Applications Audio Muted Audio Volume Behavior Capture Devices Capture Streams Checkable switch for (un-)muting sound output.Mute Checkable switch to change the current default output.Default Decrease Microphone Volume Decrease Volume Devices General Heading for a list of ports of a device (for example built-in laptop speakers or a plug for headphones)Ports Increase Microphone Volume Increase Volume Maximum volume: Mute Mute %1 Mute Microphone No applications playing or recording audio No output or input devices found Playback Devices Playback Streams Port is unavailable (unavailable) Port is unplugged (unplugged) Raise maximum volume Show additional options for %1 Volume Volume at %1% Volume feedback Volume step: label of device items%1 (%2) label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2018-02-12 05:47-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-stable/messages/kde-workspace/plasma_applet_org.kde.plasma.volume.pot
 % 调节 %1 的音量 应用程序 音频静音 音频音量 行为 录音设备 录音流 静音 默认 减小麦克风音量 减小音量 设备 常规 端口 增大麦克风音量 增大音量 最大音量： 静音 静音 %1 麦克风静音 无应用程序播放或者录制音频 未找到输出或输入设备 播放设备 播放流  (不可用)  (已拔出) 提高最大音量 显示用于 %1 的附加选项 音量 音量为 %1% 音量反馈 音量调整步长： %1 (%2) %1: %2 100% %1% 