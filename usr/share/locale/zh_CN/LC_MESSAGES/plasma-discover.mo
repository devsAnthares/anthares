��    d      <  �   \      �     �     �     �     �     �  5   �  #   �  E   	  E   _	  >   �	     �	     �	  7   
     E
     _
     p
     �
     �
     �
     �
     �
     �
          	          4     B     W     i     n  	   u          �     �  !   �  F   �     #     @     Q  <   c     �     �  *   �     �      �            	        &     6  	   >     H     X     _      h     �  
   �     �     �     �     �  
   �  F     :   K     �     �     �     �     �     �  8   �     �       	     
   "     -     =     F     W     l     r     �     �     �     �     �     �     �  &   �     "  
   >     I     Y     y     �     �     �     �  $   �    �     �                    &  :   7  $   r  N   �  N   �  G   5     }     �  !   �     �  	   �     �     �                    ,     3     @     M  !   `     �     �     �     �     �  	   �     �     �     �  &   �  9   &     `     x     �  <   �     �  	   �  '   �  	             -  	   4  	   >     H     U  	   \     f     v     }     �     �     �  !   �     �       	        !  E   1  0   w     �  	   �     �     �     �     �  9   �     "     )  	   A     K     W     h     o       	   �     �  	   �     �     �  	   �     �     
               $     <     I     V     b  	   i     s     �     �      �         J   `   :       	   W                 [   .   E         )      (   R   M   '   O   $       7   Y   4                -                    9       8   #   S       0   P       B      
   +                         L   >              N   @      *   D      F   /   ;       5       2   ^      U               ?   6       <   V   ,       c       \   Z      G   1                  d       C   Q   &       A          _   b       =   T      !   3             a   K           ]              H      %      I   X          "    
Also available in %1 %1 %1 (%2) %1 (Default) <b>%1</b> by %2 <em>%1 out of %2 people found this review useful</em> <em>Tell us about this review!</em> <em>Useful? <a href='true'><b>Yes</b></a>/<a href='false'>No</a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'><b>No</b></a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'>No</a></em> @infoFetching updates @infoFetching... @infoIt is unknown when the last check for updates was @infoLooking for updates @infoNo updates @infoNo updates are available @infoShould check for updates @infoThe system is up to date @infoUpdates @infoUpdating... Accept Add Source... Addons Aleix Pol Gonzalez An application explorer Apply Changes Available backends:
 Available modes:
 Back Cancel Category: Checking for updates... Comment too long Comment too short Compact Mode (auto/compact/full). Could not close the application, there are tasks that need to be done. Could not find category '%1' Couldn't open %1 Delete the origin Directly open the specified application by its package name. Discard Discover Display a list of entries with a category. Extensions... Failed to remove the source '%1' Featured Help... Homepage: Improve summary Install Installed Jonathan Thomas Launch License: List all the available backends. List all the available modes. Loading... Local package file to install Make default More Information... More... No Updates Open Discover in a said mode. Modes correspond to the toolbar buttons. Open with a program that can deal with the given mimetype. Proceed Rating: Remove Resources for '%1' Review Reviewing '%1' Running as <em>root</em> is discouraged and unnecessary. Search Search in '%1'... Search... Search: %1 Search: %1 + %2 Settings Short summary... Show reviews (%1)... Size: Sorry, nothing found... Source: Specify the new source for %1 Still looking... Summary: Supports appstream: url scheme Tasks Tasks (%1%) TransactioName TransactionStatus%1 %2 Unable to find resource: %1 Update All Update Selected Update section nameUpdate (%1) Updates Version: unknown reviewer updates not selected updates selected © 2010-2016 Plasma Development Team Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:02+0100
PO-Revision-Date: 2018-02-12 05:47-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-stable/messages/kde-workspace/plasma-discover.pot
 
也在 %1 中可用 %1 %1 (%2) %1 (默认) <b>%1</b> 由 %2 <em>%2 位用户中有 %1 位觉得这条评价有用</em> <em>告诉我们这条评价！</em> <em>有用吗？ <a href='true'><b>有</b></a>/<a href='false'>没有</a></em> <em>有用吗？ <a href='true'>有</a>/<a href='false'><b>没有</b></a></em> <em>有用吗？ <a href='true'>有</a>/<a href='false'>没有</a></em> 正在获取更新 正在获取... 上次检查更新的时间未知 正在查找更新 无更新 无更新可用 应当检查更新 系统已为最新 更新 正在更新... 接受 添加源... 附加组件 Aleix Pol Gonzalez 一款应用程序的探索工具 应用更改 可用的后端：
 可用的模式：
 后退 取消 类别： 应当检查更新... 评论太长 评论太短 紧凑模式 (自动/紧凑/完整)。 无法关闭此应用程序，有需要完成的任务。 找不到分类“%1” 无法打开 %1 删除原始包 直接通过某个应用程序的软件包名打开程序。 丢弃 发现者 显示某个分类中的项目列表。 扩展... 无法删除源 "%1" 精选 帮助... 主页： 改进摘要 安装 已安装 Jonathan Thomas 启动 许可证： 列出所有可用的后端。 列出所有可用的模式。 正在载入... 要安装的本地软件包文件 设为默认 更多信息... 更多... 无可用更新 以指明的方式打开发现者。模式与工具栏按钮对应。 用能处理指定 MIME 类型的程序打开。 继续 评分： 移除 “%1”的资源 评价 评价“%1” 作为 <em>root</em> 运行是不鼓励和不必要的。 搜索 在“%1”中搜索... 搜索... 搜索：%1 搜索：%1 + %2 设置 简短摘要... 显示评价 (%1)... 大小： 抱歉，未找到... 原因： 指定 %1 的新源 仍在查找... 概要： 支持 appstream：url scheme 任务 任务 (%1%) %1 %2 无法找到资源：%1 全部更新 更新选中 更新 (%1) 更新 版本： 未知评论者 更新未选中 更新选中 © 2010-2016 Plasma 开发团队 