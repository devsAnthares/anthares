��    3      �  G   L      h  6   i  M   �  K   �     :  '   C     k     r  �   �     ;  	   R  A   \  >   �  9   �  9     9   Q  W   �  E   �     )     :      @     a  7   ~      �     �     �  X   �     X	     `	     v	  �   �	     '
     F
     L
     c
  
   s
  
   ~
     �
     �
  E  �
          &     <  w   O     �     �     �     �     �  	            #  0   :  W   k  N   �               8     ?  �   O     �               !     (     /     6     =     V     c     s     z     �  -   �     �     �     �  Q        a     h     z  {   �  
             !     7  
   P  
   [     f     j  �   n     U     p     �  ]   �     �                      	   =     G         /   +   )               ,      .                 	          &                       %   #                       '       0       3                         -       1   2                        (   
      *   !         $                           "    "Full screen repaints" can cause performance problems. "Only when cheap" only prevents tearing for full screen changes like a video. "Re-use screen content" causes severe performance problems on MESA drivers. Accurate Allow applications to block compositing Always Animation speed: Applications can set a hint to block compositing when the window is open.
 This brings performance improvements for e.g. games.
 The setting can be overruled by window-specific rules. Author: %1
License: %2 Automatic Category of Desktop Effects, used as section headerAccessibility Category of Desktop Effects, used as section headerAppearance Category of Desktop Effects, used as section headerCandy Category of Desktop Effects, used as section headerFocus Category of Desktop Effects, used as section headerTools Category of Desktop Effects, used as section headerVirtual Desktop Switching Animation Category of Desktop Effects, used as section headerWindow Management Configure filter Crisp EMAIL OF TRANSLATORSYour emails Enable compositor on startup Exclude Desktop Effects not supported by the Compositor Exclude internal Desktop Effects Full screen repaints Get New Effects... Hint: To find out or configure how to activate an effect, look at the effect's settings. Instant KWin development team Keep window thumbnails: Keeping the window thumbnail always interferes with the minimized state of windows. This can result in windows not suspending their work when minimized. NAME OF TRANSLATORSYour names Never Only for Shown Windows Only when cheap OpenGL 2.0 OpenGL 3.1 OpenGL Platform InterfaceEGL OpenGL Platform InterfaceGLX OpenGL compositing (the default) has crashed KWin in the past.
This was most likely due to a driver bug.
If you think that you have meanwhile upgraded to a stable driver,
you can reset this protection but be aware that this might result in an immediate crash!
Alternatively, you might want to use the XRender backend instead. Re-enable OpenGL detection Re-use screen content Rendering backend: Scale method "Accurate" is not supported by all hardware and can cause performance regressions and rendering artifacts. Scale method: Search Smooth Smooth (slower) Tearing prevention ("vsync"): Very slow XRender Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2018-02-12 05:47-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-stable/messages/kde-workspace/kcmkwincompositing.pot
 ”全场景重画“可能导致性能问题。 ”仅当占用资源少时“只阻止全屏幕变化带来的撕裂，例如视频。 “重用窗口内容”可能在 MESA 驱动上导致严重的性能问题。 精确 允许应用程序阻止混成 总是 动画速度： 应用程序可以设置一个提示在窗口打开时来阻止混成。
这会改善性能，例如游戏。
这个设置可以被窗口特定规则覆盖。 作者：%1
许可协议： %2 自动 辅助功能 外观 蜜罐 焦点 工具 虚拟桌面切换动画 窗口管理 配置过滤器 快速 kde-china@kde.org 启动时开启混成 不包含不被混成器支持的桌面特效 不包含内置桌面特效 全场景重画 获得新特效... 提示：要查找或配置如何激活效果，请查看相应效果的选项。 即时 KWin 开发团队 保留窗口缩略图： 总是保持窗口缩略图影响窗口的最小化状态。这可能导致窗口在最小化时不暂停他们的工作。 KDE 中国 从不 只对显示的窗口 仅当占用资源少时 OpenGL 2.0 OpenGL 3.1 EGL GLX OpenGL 混合(默认设置) 曾经导致 KWin 崩溃。
通常是驱动原因。
如果驱动升级到了稳定版本，
可以重置此保护但是记得这可能导致立即崩溃！
此外，可以考虑使用 XRender 后端。 重新启用 OpenGL 检测 重用窗口内容 渲染后端： 缩放模式”精确“并不被所有硬件支持并且可能导致性能或渲染问题。 缩放方法： 搜索 平滑 平滑(较慢) 避免撕裂 (垂直同步)： 非常慢 XRender 