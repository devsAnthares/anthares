��    `        �         (     )     1     B     Q     W     ^     j     {     �     �  /   �  -   �     �     �  
   		  
   	     	     ,	     1	     8	     @	     R	     _	     d	  
   l	     w	     �	     �	     �	  	   �	     �	  	   �	     �	     �	     �	     
     
  	   #
     -
     4
     H
  	   M
     W
     ]
     c
     h
     m
  	   v
     �
     �
     �
     �
     �
     �
     �
     �
  <   �
               /     6     =     X     ^     e     j  
   ~     �     �     �     �     �  )   �               5     :     @     M     U     ]     f  
   x     �     �  	   �     �     �     �     �     �     �     �  E   �  *   A  ,  l  
   �     �     �  
   �  
   �  
   �     �     �             	   .     8     E  	   L     V     c  	   p     z     �     �     �     �     �     �     �     �     �     �     �  	                  +     ;     B     ^     n     ~  	   �     �     �     �     �     �  	   �     �     �  	   �     �  	   �     �               !     %     ,  B   <          �     �     �     �  	   �     �     �     �     �     �     �               ,  $   <     a     w     �     �     �     �     �  	   �     �     �     �  	   �     �                 	   )     3     I     V  N   f     �     J       -      M       %   Q           ^              U       L   S   $      .       H       X          Y   8   *         <   &          '       ]   N       3                     `          #   V             /   T   G   9   @   >   =   6   2   O   C   F   ?      0      
      A   E       	      P              _                D          Z               1   R   ,   +                \           I   )   B   K      :   !   W   4          ;               (      5   "       7       [           &Delete &Empty Trash Bin &Move to Trash &Open &Paste &Properties &Refresh Desktop &Refresh View &Reload &Rename @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Align Appearance: Arrange In Arrange in Arrangement: Back Cancel Columns Configure Desktop Custom title Date Default Descending Description Deselect All Desktop Layout Enter custom title here Features: File name pattern: File type File types: Filter Folder preview popups Folders First Folders first Full path Got it Hide Files Matching Huge Icon Size Icons Large Left List Location Location: Lock in place Locked Medium More Preview Options... Name None OK Panel button: Press and hold widgets to move them and reveal their handles Preview Plugins Preview thumbnails Remove Resize Restore from trashRestore Right Rotate Rows Search file type... Select All Select Folder Selection markers Show All Files Show Files Matching Show a place: Show files linked to the current activity Show the Desktop folder Show the desktop toolbox Size Small Small Medium Sort By Sort by Sorting: Specify a folder: Text lines Tiny Title: Tool tips Tweaks Type Type a path or a URL here Unsorted Use a custom icon Widget Handling Widgets unlocked You can press and hold widgets to move them and reveal their handles. whether to use icon or list viewView mode Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:11+0100
PO-Revision-Date: 2018-02-12 05:47-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-stable/messages/kde-workspace/plasma_applet_org.kde.desktopcontainment.pot
 删除(&D) 清空回收站(&E) 移至回收站(&M) 打开(&O) 粘贴(&P) 属性(&P) 刷新桌面(&R) 刷新视图(&R) 重新载入(&R) 重命名(&R) 选择... 清除图标 对齐 外观： 布局方式 布局方式 布局： 后退 取消 列 配置桌面 自定义标题 日期 默认 降序 描述 全部取消选择 桌面布局 这里输入自定义标题 功能： 文件名模式： 文件类型 文件类型： 过滤 文件夹预览弹出窗口 文件夹优先 文件夹在前 完整路径 知道了 隐藏匹配文件 大 图标大小 图标 大 左对齐 列表 位置 位置： 就地锁定 已锁定 中等 更多预览选项... 名称 无 确定 面板按钮： 按下并按住部件将移动部件并且显示他们的控制栏 预览插件 预览缩略图 删除 缩放 恢复 右对齐 旋转 行 搜索文件类型... 全部选中 选择文件夹 选择标记 显示所有文件 显示匹配文件 显示位置： 显示与当前活动关联的文件 显示桌面文件夹 显示桌面工具栏 大小 小 中小 排序方式 排序依据 排序： 指定文件夹： 文本行数 微小 标题： 工具提示 调整 类型 在此输入路径或 URL 未排序 使用自定义图标 部件处理 部件已解锁 您可以按下并按住部件将移动部件并且显示他们的控制栏。 视图模式 