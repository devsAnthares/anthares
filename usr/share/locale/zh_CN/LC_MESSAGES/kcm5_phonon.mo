��    B      ,  Y   <      �  i   �  m        y  b   �     �     	  6        O  7   _     �     �  	   �     �  (   �  )   �  )   (     R     o  Y   u     �     �  �   �      y	  .   �	     �	  
   �	     �	     �	     
     
     !
     5
     B
     J
     c
     r
     w
     �
     �
     �
     �
     �
  	   �
  
   �
     �
     �
  	     
     
   *     5     B  	   `     j     o  
   �  �   �     (  8   8  �   q     �  8   	  ,   B  ,   o  %   �     �    �  :   �  d   (     �  H   �     �       )        9  /   F     v     }  	   �     �     �     �  !   �          $  H   +     t     �  i   �     �  '        5     <     C     P     W     d     k     x     �     �     �  	   �  
   �     �     �     �     �     �     �          
          )     0     7     >     K  	   d     n     u  	   �  �   �     !  3   .  k   b     �  /   �  '     '   3  !   [     }     $   !   %          ;   ,          B                 8   #                 7   9       +              )   ?   -   <         6                                    *   2       >                1       @       .   5   3   :   A         &      /                4              0      "             =   (   '         	   
           @info User changed Phonon backendTo apply the backend change you will have to log out and back in again. A list of Phonon Backends found on your system.  The order here determines the order Phonon will use them in. Apply Device List To... Apply the currently shown device preference list to the following other audio playback categories: Audio Hardware Setup Audio Playback Audio Playback Device Preference for the '%1' Category Audio Recording Audio Recording Device Preference for the '%1' Category Backend Colin Guthrie Connector Copyright 2006 Matthias Kretz Default Audio Playback Device Preference Default Audio Recording Device Preference Default Video Recording Device Preference Default/Unspecified Category Defer Defines the default ordering of devices which can be overridden by individual categories. Device Configuration Device Preference Devices found on your system, suitable for the selected category.  Choose the device that you wish to be used by the applications. EMAIL OF TRANSLATORSYour emails Failed to set the selected audio output device Front Center Front Left Front Left of Center Front Right Front Right of Center Hardware Independent Devices Input Levels Invalid KDE Audio Hardware Setup Matthias Kretz Mono NAME OF TRANSLATORSYour names Phonon Configuration Module Playback (%1) Prefer Profile Rear Center Rear Left Rear Right Recording (%1) Show advanced devices Side Left Side Right Sound Card Sound Device Speaker Placement and Testing Subwoofer Test Test the selected device Testing %1 The order determines the preference of the devices. If for some reason the first device cannot be used Phonon will try to use the second, and so on. Unknown Channel Use the currently shown device list for more categories. Various categories of media use cases.  For each category, you may choose what device you prefer to be used by the Phonon applications. Video Recording Video Recording Device Preference for the '%1' Category  Your backend may not support audio recording Your backend may not support video recording no preference for the selected device prefer the selected device Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2018-02-12 05:47-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-stable/messages/kde-workspace/kcm5_phonon.pot
 您必须注销并再次登录才能应用后端的修改. 列出在您系统中找到的 Phonon 后端。这里的顺序决定了 Phonon 所使用的顺序。 设备列表应用到... 对以下音频播放分类应用当前显示的设备首选项列表： 音频硬件设置 音频回放 “%1”分类下的播放设备首选项 音频抓取 “%1”分类下的音频抓取设备首选项 后端 Colin Guthrie 连接器 Copyright 2006 Matthias Kretz 默认播放设备首选项 默认录音设备首选项 默认视频抓取设备首选项 默认/未指定分类 次选 定义默认的设备排序方式，它可被个人分类设定覆盖。 设备配置 设备首选项 在您的系统中找到的适合所选种类的设备。请选择您希望应用程序使用的设备。 kde-china@kde.org 设定选中的音频输出设备失败 正前 左中 中央左前 右前 中央右前 硬件 独立设备 输入级别 无效 KDE 音频硬件设置 Matthias Kretz 单声道 KDE 中国 Phonon 配置模块 回放 (%1) 首选 配置 正后 左后 右后 抓取 (%1) 显示高级设备 左边 右边 声卡 声音设备 扬声器位置和测试 低音炮 测试 测试选中的设备 测试 %1 这个顺序决定设备的优先顺序。如果第一个设备不能正常使用，Phonon 将尝试使用第二个设备，以此类推。 未知声道 应用当前显示的设备列表到更多分类。 媒体功能的各种分类。对于每种分类您可以选择 Phonon 应用程序所要输出的设备。 视频抓取 “%1”分类下的视频抓取设备首选项 您的后端可能不支持音频抓取 您的后端可能不支持视频抓取 被选中的设备没有首选项 首选被选中的设备 