��    K      t  e   �      `     a     o     v     �  
   �     �  &   �     �     �     �  
   �           &     G  ,   Z  +   �  /   �     �     �     �  p        �     �     �     �     �     �      	     	  	   	  
   &	     1	     H	     Y	     i	     �	  	   �	     �	     �	     �	  (   �	  #   
     3
     D
     S
     k
     �
  9   �
     �
     �
     �
     �
          *     I     i     �     �  :   �     �  #        *     @     `     u     �     �     �     �               /     E  S   `  �  �  .   r     �  O   �       -   "  F   P  z   �  ;        N     _     o  Y   �     �  Z   �  j   X  4   �  �   �  W   �     �  *   �  ;    Y   N  S   �  �   �  �   �  o     3   �  4   �     �     �       o   /     �  ;   �  a   �  X   N     �  8   �  F   �  %   @  e   f  _   �  N   ,  Q   {  ]   �  Z   +  4   �  �   �  J   �  a   �  :   H  I   �  L   �  X     U   s  U   �  T      ^   t   h   �   L   <!  n   �!  E   �!  g   >"  V   �"  u   �"  t   s#  w   �#  �   `$  �   �$  S   e%  �   �%     :&  �   �&  P  D'               '   -   I      ,   #   F   3             8      1      
                  2       0   $   C             :          A   *   5      ;       K         	   >                     H                               9   /       )   "               <   .       (   +           B   !   E       %          J      7             =      @         G   4      &   ?   D   6        &All Desktops &Close &Fullscreen &Move &No Border Activate Window (%1) Caption of the window to be terminated Close Window Cristian Tibirna Daniel M. Duley Desktop %1 Disable configuration options EMAIL OF TRANSLATORSYour emails Hide Window Border Hostname on which the application is running ID of resource belonging to the application Indicate that KWin has recently crashed n times KDE window manager KWin KWin helper utility KWin is unstable.
It seems to have crashed several times in a row.
You can select another window manager to run: Keep &Above Others Keep &Below Others Keep Window Above Others Keep Window Below Others Keep Window on All Desktops Kill Window Lower Window Luboš Luňák Ma&ximize Maintainer Make Window Fullscreen Matthias Ettrich Maximize Window Maximize Window Horizontally Maximize Window Vertically Mi&nimize Minimize Window Move Window NAME OF TRANSLATORSYour names Name of the application to be terminated PID of the application to terminate Pack Window Down Pack Window Up Pack Window to the Left Pack Window to the Right Raise Window Replace already-running ICCCM2.0-compliant window manager Resize Window Setup Window Shortcut Shade Window Switch One Desktop Down Switch One Desktop Up Switch One Desktop to the Left Switch One Desktop to the Right Switch to Next Desktop Switch to Next Screen Switch to Previous Desktop This helper utility is not supposed to be called directly. Walk Through Desktop List Walk Through Desktop List (Reverse) Walk Through Desktops Walk Through Desktops (Reverse) Walk Through Windows Walk Through Windows (Reverse) Window One Desktop Down Window One Desktop Up Window One Desktop to the Left Window One Desktop to the Right Window Operations Menu Window to Next Desktop Window to Next Screen Window to Previous Desktop kwin: unable to claim manager selection, another wm running? (try using --replace)
 Project-Id-Version: kwin
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-15 03:16+0100
PO-Revision-Date: 2009-01-16 16:49+0530
Last-Translator: Amitakhya Phukan <অমিতাক্ষ ফুকন>
Language-Team: Assamese <fedora-trans-as@redhat.com>
Language: as
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 0.2
Plural-Forms: nplurals=2; plural=n != 1;
 সকলো ডেষ্কট'পলৈ (&A) বন্ধ কৰক (&C) সম্পূৰ্ণ পৰ্দ্দাত প্ৰদৰ্শন (&F) লৰাওক (&M) প্ৰান্ত নোহোৱা (&N) সংযোগক্ষেত্ৰ সক্ৰিয় কৰক (%1) বন্ধ কৰাৰ বাবে চিহ্নিত সংযোগক্ষেত্ৰৰ শিৰোনাম সংযোগক্ষেত্ৰ বন্ধ কৰক Cristian Tibirna Daniel M. Duley ডেস্কট'প %1 বিন্যাসৰ বিকল্প নিষ্ক্ৰিয় কৰা হ'ব aphukan@fedoraproject.org সংযোগক্ষেত্ৰৰ প্ৰান্ত লুকাওক কৰক অনুপ্ৰয়োগ চালনকাৰী যন্ত্ৰৰ গৃহস্থৰ নাম অনুপ্ৰয়োগৰ সম্পদৰ ID KWin এ n সংখ্যক বাৰ বিপৰ্যস্ত হোৱাৰ ইঙ্গিত প্ৰদৰ্শন কৰা হ'ব KDE সংযোগক্ষেত্ৰ পৰিচালন ব্যৱস্থা KWin KWin সহায়ক সামগ্ৰী KWin ত সমস্যা দেখা দিছে ।
একাধিক বাৰ বিপৰ্যস্ত হৈছে ।
আপুনি কোনো পৃথক সংযোগক্ষেত্ৰ পৰিচালন ব্যৱস্থা নিৰ্ব্বাচন কৰিব পাৰে: সকলো সংযোগক্ষেত্ৰৰ ওপৰত স্থাপন (&A) সকল সংযোগক্ষেত্ৰৰ তলত স্থাপন (&B) সকলো সংযোগক্ষেত্ৰৰ শীৰ্ষত এই সংযোগক্ষেত্ৰ স্থাপন কৰক অন্য সংযোগক্ষেত্ৰৰ তলত এই সংযোগক্ষেত্ৰ স্থাপন কৰক সকলো ডেষ্কট'পত সংযোগক্ষেত্ৰ প্ৰদৰ্শিত হ'ব সংযোগক্ষেত্ৰ Kill কৰক সংযোগক্ষেত্ৰ নমাওক Luboš Luňák ডাঙৰ কৰক (&x) পৰিচালক সম্পূৰ্ণ পৰ্দ্দাত সংযোগক্ষেত্ৰ প্ৰদৰ্শন Matthias Ettrich সংযোগক্ষেত্ৰ ডাঙৰ কৰক অনুভূমিক দিশত সংযোগক্ষেত্ৰ ডাঙৰ কৰক উলম্ব দিশত সংযোগক্ষেত্ৰ ডাঙৰ কৰক সৰু কৰক (&n) সংযোগক্ষেত্ৰ সৰু কৰক সংযোগক্ষেত্ৰ স্থানান্তৰণ অমিতাক্ষ ফুকন বন্ধ কৰাৰ বাবে চিহ্নিত অনুপ্ৰয়োগৰ নাম বন্ধ কৰাৰ বাবে চিহ্নিত অনুপ্ৰয়োগৰ PID তলত সংযোগক্ষেত্ৰ একত্ৰিত কৰক ওপৰত সংযোগক্ষেত্ৰ একত্ৰিত কৰক বাওঁফালে সংযোগক্ষেত্ৰ একত্ৰিত কৰক সোঁফালে সংযোগক্ষেত্ৰ একত্ৰিত কৰক সংযোগক্ষেত্ৰ উঠাওক ইতিমধ্যে চলি থকা ICCCM2.0 ৰ সৈতে সুসঙ্গত সংযোগক্ষেত্ৰ পৰিচালন ব্যৱস্থাক প্ৰতিস্থাপন কৰক সংযোগক্ষেত্ৰৰ মাপ পৰিবৰ্তন সংযোগক্ষেত্ৰৰ শৰ্ট কাট নিৰ্ধাৰণ কৰক ছায়াবৃত সংযোগক্ষেত্ৰ তলৰ ডেষ্কট'পলৈ পৰিবৰ্তন কৰক উপৰৰ ডেষ্কট'পলৈ পৰিবৰ্তন কৰক বাওঁফালৰ ডেষ্কট'পলৈ পৰিবৰ্তন কৰক সোঁফালৰ ডেষ্কট'পলৈ পৰিবৰ্তন কৰক পৰবৰ্তী ডেষ্কট'পলৈ পৰিবৰ্তন কৰক পৰবৰ্তী পৰ্দ্দালৈ পৰিবৰ্তন কৰক পূৰ্ববৰ্তী ডেষ্কট'পলৈ পৰিবৰ্তন কৰক সহায়ক সামগ্ৰীক পোনেপোনে মাতিব নালাগে । ডেষ্কট'প তালিকা পৰিদৰ্শন কৰক ডেষ্কট'প তালিকা পৰিদৰ্শন কৰক (বিপৰীত দিশে) ডেস্কট'পসমূহ পৰিদৰ্শন কৰক ডেষ্কট'পসমূহ পৰিদৰ্শন কৰক (বিপৰীত দিশে) সংযোগক্ষেত্ৰসমূহৰ পৰিদৰ্শন কৰক সংযোগক্ষেত্ৰসমূহ পৰিদৰ্শন কৰক (বিপৰীত দিশে) তলৰ ডেষ্কট'পলৈ সংযোগক্ষেত্ৰ স্থানান্তৰ কৰক ওপৰৰ ডেষ্কট'পলৈ সংযোগক্ষেত্ৰ স্থানান্তৰ কৰক বাওঁফালৰ ডেষ্কট'পলৈ সংযোগক্ষেত্ৰ স্থানান্তৰ কৰক সোঁফালৰ ডেষ্কট'পলৈ সংযোগক্ষেত্ৰ স্থানান্তৰ কৰক সংযোগক্ষেত্ৰ পৰিচালনাৰ তালিকা পৰবৰ্তি ডেষ্কট'পলৈ সংযোগক্ষেত্ৰ স্থানান্তৰ কৰক পৰবৰ্তী পৰ্দ্দালৈ সংযোগক্ষেত্ৰ স্থানান্তৰ কৰক পূৰ্ববৰ্তী ডেষ্কট'পলৈ সংযোগক্ষেত্ৰ স্থানান্তৰ কৰক kwin: পৰিচালন ব্যৱস্থা ধাৰ্য কৰোঁতে ব্যৰ্থ, এটা পৃথক সংযোগক্ষেত্ৰ পৰিচালন ব্যৱস্থা বৰ্তমানে চলিছে নেকি ? ( --replace ৰ প্ৰয়োগ চেষ্টা কৰক)
 