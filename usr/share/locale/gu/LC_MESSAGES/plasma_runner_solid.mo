��          �      L      �  m   �     /  "   <  F   _  F   �  F   �  J   4  N     R   �     !  %   2  $   X  #   }  $   �  %   �  &   �  �        �  �  �  /   J  2   z  t   �  �   "  �   	  �   �	  �   �
    �    �  2   �     �     
     $     )     <     C  2   \  8   �                                
                          	                                          Close the encrypted container; partitions inside will disappear as they had been unpluggedLock the container Eject medium Finds devices whose name match :q: Lists all devices and allows them to be mounted, unmounted or ejected. Lists all devices which can be ejected, and allows them to be ejected. Lists all devices which can be mounted, and allows them to be mounted. Lists all devices which can be unmounted, and allows them to be unmounted. Lists all encrypted devices which can be locked, and allows them to be locked. Lists all encrypted devices which can be unlocked, and allows them to be unlocked. Mount the device Note this is a KRunner keyworddevice Note this is a KRunner keywordeject Note this is a KRunner keywordlock Note this is a KRunner keywordmount Note this is a KRunner keywordunlock Note this is a KRunner keywordunmount Unlock the encrypted container; will ask for a password; partitions inside will appear as they had been plugged inUnlock the container Unmount the device Project-Id-Version: plasma_runner_solid
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-12-23 12:15+0530
Last-Translator: Sweta Kothari <swkothar@redhat.com>
Language-Team: Gujarati
Language: gu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=(n!=1);
 પાત્રને તાળુ મારો માધ્યમને બહાર કાઢો ઉપકરણોને શોધે છે જેનું નામ :q: સાથે બંધબેસે છે બધા ઉપકરણોની યાદી કરો અને માઉન્ટ, અનમાઉન્ટ, અથવા બહાર કાઢવા માટે તેઓને પરવાનગી આપે છે. બધા ઉપકરણોની યાદી કરો કે જેને બહાર કાઢી શકાય છે, અને બહાર કાઢવા માટે તેઓને પરવાનગી આપે છે. બધા ઉપકરણોની યાદી કરો કે જે માઉન્ટ કરી શકાય છે, અને માઉન્ટ કરવા માટે પરવાનગી આપે છે. બધા ઉપકરણોની યાદી કરો કે જે અનમાઉન્ટ કરી શકાય છે, અને અનમાઉન્ટ કરવા માટે પરવાનગી આપે છે. બધી એનક્રિપ્ટ થયેલ ઉપકરણોની યાદીઓ કે જેને તાળુ મારી શકાય છે અને તેઓને તાળુ મારવા માટે પરવાનગી અપાય છે. બધી એનક્રિપ્ટ થયેલ ઉપકરણોની યાદીઓ કે જેનું તાળુ ખોલી શકાય છે અને તેઓને તાળુ ખોલવા માટે પરવાનગી અપાય છે. ઉપકરણને માઉન્ટ કરો ઉપકરણ બહાર કાઢો lock માઉન્ટ unlock અનમાઉન્ટ પાત્રનું તાળુ ખોલો ઉપકરણને અનમાઉન્ટ કરો 