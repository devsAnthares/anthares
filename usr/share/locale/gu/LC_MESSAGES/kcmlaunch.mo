��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  �  K     �  J     �  \  T  @  �   �  %   N  /   t  '   �  P   �  ?     8   ]  J   �  0   �                                          
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2008-10-03 20:05+0530
Last-Translator: Pragnesh Radadiya <pg.radadia@gmail.com>
Language-Team: Gujarati <kde-i18n-doc@lists.kde.org>
Language: gu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 0.2
Plural-Forms: nplurals=2; plural=(n!=1);
 સેકન્ડ શરુઆત દેખાઇને સમયસમાપ્તિ: (&S) <H1>ટાસ્કબાર નોંધણી</H1>
 તમે શરુઆતની નોંધણી માટે બીજી પધ્ધતિ સક્ષમ કરી શકો છો જે
 ટાસ્કબાર વડે ઉપયોગ થાય છે જેમાં ગોળ ગોળ ફરતી સમયસારણી જોવા મળશે,
 જે તમને બતાવે છે કે તમે શરુ કરેલો કાર્યક્રમ લાવી રહયા છે.
 એ બની શકે છે, કેટલાક કાર્યક્રમોને શરુઆતની નોધણીની જાણ જ ના  હોય
 તેવા સંજોગોમાં, બટન કેટલાક સમય પછી અદ્રશ્ય થઈ જશે
 એ તે સમય છે કે જે તમે 'શરુઆત દેખાઇને સમયસમાપ્તિ' વિભાગમાં આપેલ છે <h1>વ્યસ્ત કર્સર</h1>
 કેડીઇ કાર્યક્રમ શરુ કરતી વખતે વ્યસ્ત કર્સરથી નોંધ કરાવે છે.
 વ્યસ્ત કર્સર સક્ષમ કરવા માટે, એક પ્રકારનો દર્શનીય પ્રતિભાવ
  કોમ્બોબોક્ષમાંથી પસંદ કરો.
 તે બની શકે છે, કેટલાક કાર્યક્રમો આ  શરુઆતની  નોધણી
 વિશે અજાણ હોય, આ સંજોગોમાં, કર્સર 'શરુઆત દેખાઇને સમયસમાપ્તિ'
 વિભાગમાં આપેલા સમય પછી ઝબકવાનુ બંધ કરી દેશે <h1>પ્રતિભાવ શરુ કરો</h1>તમે કાર્યક્રમ-શરુઆતનો પ્રતિભાવ અહીં ગોઠવી શકો છો. ઝબૂકતું કર્સર કૂદકા મારતુ કર્સર વ્યસ્ત કર્સર (&y) ટાસ્કબાર નોંધણીને સક્ષમ કરો (&t) કોઇપણ વ્યસ્ત કર્સર નહીં નિષ્કિય વ્યસ્ત કર્સર શરુઆત દેખાઇને સમયસમાપ્તિ: (&u) ટાસ્કબાર નોંધણી (&N) 