��          �            h  &   i  +   �     �  	   �     �     �     �     �     �  (        7  ,   N     {     �  �  �  h   -  |   �          )  M   B     �     �     �  5   �  ~     9   �  o   �     4  +   Q                                                                  	   
           Do you want to suspend to RAM (sleep)? Do you want to suspend to disk (hibernate)? General Hibernate Hibernate (suspend to disk) Leave Leave... Lock Lock the screen Logout, turn off or restart the computer Sleep (suspend to RAM) Start a parallel session as a different user Suspend Switch user Project-Id-Version: plasma_applet_lockout
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2010-06-18 12:59+0530
Last-Translator: Sweta Kothari <swkothar@redhat.com>
Language-Team: Gujarati
Language: gu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=(n!=1);
 શું તમે RAM ને સ્થગિત કરવા માંગો છો (સ્લીપ)? શું તમે ડિસ્કને સ્થગિત કરવા માંગો છો (હાઇબરનેટ)? સામાન્ય હાઇબરનેટ હાઇબરનેટ (ડિસ્કને સ્થગિત કરો) છોડી દો છોડી દો... તાળુ મારો સ્ક્રીનને તાળુ મારો બહાર નીકળો, બંધ કરો અથવા કમ્પ્યૂટરને પુન:શરૂ કરો સ્લીપ (RAM ને સ્થગિત કરો) બીજા વપરાશકર્તા તરીકે સમાંતર સત્ર શરુ કરો સ્થગિત કરો વપરાશકર્તા બદલો 