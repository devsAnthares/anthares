��          �      �       H  $   I  �   n     �  4   �     3  #   M  �   q  �   �  +   �     �     �       �   1  �  �     [     n     �  �   �  S   +  g     t  �  �  \	  a     <   }  T   �  P        `                          	                                   
    @info:status Free disk space%1 free Accessing is a less technical word for Mounting; translation should be short and mean 'Currently mounting this device'Accessing... All devices Click to access this device from other applications. Click to eject this disc. Click to safely remove this device. It is currently <b>not safe</b> to remove this device: applications may be accessing it. Click the eject button to safely remove this device. It is currently <b>not safe</b> to remove this device: applications may be accessing other volumes on this device. Click the eject button on these other volumes to safely remove this device. It is currently safe to remove this device. No Devices Available Non-removable devices only Removable devices only Removing is a less technical word for Unmounting; translation shoud be short and mean 'Currently unmounting this device'Removing... Project-Id-Version: plasma_applet_devicenotifier
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2008-10-23 08:59+0530
Last-Translator: Kartik Mistry <kartik.mistry@gmail.com>
Language-Team: Gujarati <kde-i18n-doc@kde.org>
Language: gu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n!=1);
 %1 મુક્ત વાપરે છે... બધાં ઉપકરણો આ ઉપકરણને બીજા કાર્યક્રમોમાંથી વાપરવા ક્લિક કરો. ડિસ્કને બહાર નીકાળવા ક્લિક કરો. આ ઉપકરણને સલામત રીતે નીકાળવા ક્લિક કરો. હાલમાં આ ઉપકરણને બહાર નીકાળવું <b>સલામત નથી</b>: કાર્યક્રમો કદાચ તેને વાપરી રહ્યા છે. આ ઉપકરણને સલામત રીતે બહાર નીકાળવા બહાર નીકાળો બટન ક્લિક કરો. હાલમાં આ ઉપકરણને બહાર નીકાળવું <b>સલામત નથી</b>: કાર્યક્રમો કદાચ આ ઉપકરણનાં બીજાં કદો વાપરી રહ્યા છે. આ બીજાં કદોને ઉપકરણ પર સલામત રીતે બહાર નીકાળવા બહાર નીકાળોબટન ક્લિક કરો. આ ઉપકરણને હાલ બહાર નીકાળવું સલામત છે. કોઈ ઉપકરણો પ્રાપ્ત નથી માત્ર દૂર ન કરી શકાય તેવા ઉપકરણો માત્ર દૂર કરી શકાય તેવા ઉપકરણો દૂર કરે છે... 