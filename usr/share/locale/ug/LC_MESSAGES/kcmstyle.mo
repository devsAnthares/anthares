��          �   %   �      p  &   q  �   �          8     P     W     `     n     {     �      �  	   �     �     �     �     �               (     =     C     I     Z  	   l     v          �  �  �  '   D  �   l     9     V  
   v  !   �     �     �     �     �  '   �          :  :   F     �     �     �     �  C   �     	     +	  ,   :	  *   g	     �	     �	  4   �	     �	                                                             	                             
                                 (c) 2002 Karol Szwed, Daniel Molkentin <h1>Style</h1>This module allows you to modify the visual appearance of user interface elements, such as the widget style and effects. @title:tab&Applications @title:tab&Fine Tuning Button Checkbox Con&figure... Configure %1 Daniel Molkentin Description: %1 EMAIL OF TRANSLATORSYour emails Group Box Karol Szwed NAME OF TRANSLATORSYour names No Text No description available. Preview Ralf Nolden Show icons in menus: Tab 1 Tab 2 Text Below Icons Text Beside Icons Text Only Toolbars Unable to Load Dialog Widget style: Project-Id-Version: kcmstyle
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-17 05:44+0100
PO-Revision-Date: 2013-09-08 07:05+0900
Last-Translator: Gheyret Kenji <gheyret@gmail.com>
Language-Team: Uyghur Computer Science Association <UKIJ@yahoogroups.com>
Language: ug
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 (c) 2002 Karol Szwed، Daniel Molkentin <h1>ئۇسلۇب</h1>بۇ بۆلەك ئارقىلىق سىز كۆرۈنمەيۈز ئېلېمېنتلىرى(ۋىجېت ۋە ئۈنۈملەر) نىڭ كۆرۈنۈشلىرىنى ئۆزگەرتەلەيسىز. پروگراممىلار(&A) ئىنچىكە تەڭشەش(&F) توپچا تاللاش كۆزنەكچىسى سەپلەش(&F)… %1 نى سەپلە Daniel Molkentin چۈشەندۈرۈش: %1 sahran.ug@gmail.com,  gheyret@gmail.com گۇرۇپپا رامكىسى Karol Szwed ئابدۇقادىر ئابلىز, غەيرەت كەنجى تېكىست يوق چۈشەندۈرۈشى يوق. ئالدىن كۆزەت Ralf Nolden تىزىملىكلەردە سىنبەلگىلەرنى كۆرسەت: بەتكۈچ 1 بەتكۈچ 2 خەت سىنبەلگىنىڭ ئاستىدا تېكىست سىنبەلگە يېنىدا تېكىستلا قورال بالداق سۆزلەشكۈنى ئوقۇغىلى بولمىدى ۋىجېت ئۇسلۇبى: 