��    *      l  ;   �      �  A   �     �  /        2     ;     O     a     w     �     �  	   �     �  3   �  	   �     �      �  $      *   E     p  	   u  5        �     �  
   �     �          $  5   3  6   i     �  ,   �  /   �     	        O   0  Z   �  b   �  	   >  
   H  P   S     �  �  �  ^   [
     �
     �
     �
  ,   �
  *   (  2   S     �     �     �  
   �     �  ]   �     C     Z  '   k  A   �     �     �     �  a     @   w  +   �     �     �  :   	  '   D  �   l  z        �  E   �  
   �  8   �  #   -  �   Q  �   �  �   �  #   0     T  �   r                 )             &      $   	   %         '                                                !      
       *                     "                                                 (                 #    %1 is an external application and has been automatically launched (c) 2009, Ben Cooksley <i>Contains 1 item</i> <i>Contains %1 items</i> About %1 About Active Module About Active View About System Settings Apply Settings Author Ben Cooksley Configure Configure your system Determines whether detailed tooltips should be used Developer Dialog EMAIL OF TRANSLATORSYour emails Expand the first level automatically General config for System SettingsGeneral Help Icon View Internal module representation, internal module model Internal name for the view used Keyboard Shortcut: %1 Maintainer Mathias Soeken NAME OF TRANSLATORSYour names No views found Provides a categorized icons view of control modules. Provides a classic tree-based view of control modules. Relaunch %1 Reset all current changes to previous values Search through a list of control modulesSearch Show detailed tooltips System Settings System Settings was unable to find any views, and hence has nothing to display. System Settings was unable to find any views, and hence nothing is available to configure. The settings of the current module have changed.
Do you want to apply the changes or discard them? Tree View View Style Welcome to "System Settings", a central place to configure your computer system. Will Stephenson Project-Id-Version: systemsettings
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:08+0100
PO-Revision-Date: 2013-09-08 07:05+0900
Last-Translator: Gheyret Kenji <gheyret@gmail.com>
Language-Team: Uyghur Computer Science Association <UKIJ@yahoogroups.com>
Language: ug
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 %1 سىرتقى پروگرامما بولۇپ ئاپتوماتىك ئىجرا قىلىنغان (c) 2009, Ben Cooksley <i>%1 تۈر بار</i> %1 ھەققىدە ئاكتىپ بۆلەكلەر ھەققىدە ئاكتىپ كۆرۈنۈش ھەققىدە سىستېما تەڭشەكلىرى ھەققىدە تەڭشەك قوللان ئاپتور Ben Cooksley سەپلە سىستېمىنى سەپلەڭ تەپسىلىي كۆرسەتمىلەر ئىشلىتىلەمدۇ يوق بەلگىلەيدۇ. ئىجادىيەتچى سۆزلەشكۈ sahran.ug@gmail.com,  gheyret@gmail.com بىرىنچى دەرىجىنى ئاپتوماتىك يايىدۇ ئادەتتىكى ياردەم سىنبەلگە كۆرۈنۈش ئىچكى بۆلەكنىڭ چۈشەندۈرۈلۈشى، ئىچكى بۆلەكنىڭ مودېلى ئىشلىتىلگەن كۆرۈنۈشنىڭ ئىچكى ئاتى. ھەرپتاختا تېزلەتمىسى: %1 مەسئۇل كىشى Mathias Soeken ئابدۇقادىر ئابلىز, غەيرەت كەنجى كۆرۈنۈشلەر تېپىلمىدى تىزگىنلەش بۆلەكلىرىنىڭ كاتېگورىيەگە ئايرىلغان، سىنبەلگىلىك كۆرۈنۈشىنى تەمىنلەيدۇ. تىزگىنلەش بۆلەكلىرىنىڭ كلاسسىك دەرەخسىمان كۆرۈنۈشىنى تەمىنلەيدۇ. قايتا ئىجرا قىل %1 ئالدىنقى قېتىمقى قىممەتلىرىگە قايتۇر ئىزدە تەپسىلىي كۆرسەتمىلەرنى كۆرسەت سىستېما تەڭشەكلىرى سىستېما تەڭشىكى ھېچقانداق كۆرۈنۈشلەرنى تاپالمىدى. شۇڭا كۆرسىتىش ئېلىپ بارغىلى بولمايدۇ. سىستېما تەڭشىكى ھېچقانداق كۆرۈنۈشلەرنى تاپالمىدى. شۇڭا سەپلەش ئېلىپ بارغىلى بولمايدۇ. نۆۋەتتىكى بۆلەكنىڭ تەڭشىكى ئۆزگەرتىلدى.
ئۆزگەرتىشنى ساقلامسىز ياكى تاشلىۋېتەمسىز؟ دەرەخسىمان كۆرۈنۈش كۆرۈنۈش ئۇسلۇبى «سىستېما تەڭشىكى»گە مەرھابا. بۇ يەر كومپيۇتېر سىستېمىڭىزنى سەپلەيدىغان مەركەزدۇر. Will Stephenson 