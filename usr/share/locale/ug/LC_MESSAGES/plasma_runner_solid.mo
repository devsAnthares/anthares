��          �      L      �  m   �     /  "   <  F   _  F   �  F   �  J   4  N     R   �     !  %   2  $   X  #   }  $   �  %   �  &   �  �        �  �  �  !   Z     |  E   �  �   �  �   �  �   !	  �   �	  �   `
  �   	  '   �     �  
   �     �     �     �       '   '  -   O                                
                          	                                          Close the encrypted container; partitions inside will disappear as they had been unpluggedLock the container Eject medium Finds devices whose name match :q: Lists all devices and allows them to be mounted, unmounted or ejected. Lists all devices which can be ejected, and allows them to be ejected. Lists all devices which can be mounted, and allows them to be mounted. Lists all devices which can be unmounted, and allows them to be unmounted. Lists all encrypted devices which can be locked, and allows them to be locked. Lists all encrypted devices which can be unlocked, and allows them to be unlocked. Mount the device Note this is a KRunner keyworddevice Note this is a KRunner keywordeject Note this is a KRunner keywordlock Note this is a KRunner keywordmount Note this is a KRunner keywordunlock Note this is a KRunner keywordunmount Unlock the encrypted container; will ask for a password; partitions inside will appear as they had been plugged inUnlock the container Unmount the device Project-Id-Version: plasma_runner_solid
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2013-09-08 07:05+0900
Last-Translator: Gheyret Kenji <gheyret@gmail.com>
Language-Team: Uyghur Computer Science Association <UKIJ@yahoogroups.com>
Language: ug
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 قاچىنى قۇلۇپلايدۇ ۋاسىتىنى قاڭقىت ئاتى :q: غا ماس كەلگەن ئۈسكۈنىنى تاپىدۇ ھەممە ئۈسكۈنە تىزىمىنى كۆرسىتىپ، ئۇلارنى يۈكلەپ، يۈك چۈشۈرۈپ ياكى قاڭقىتىشقا يول قويىدۇ. قاڭقىتالايدىغان ھەممە ئۈسكۈنە تىزىملىكىنى كۆرسىتىپ، ئۇلارنى قاڭقىتىشقا يول قويىدۇ. يۈكلىگىلى بولىدىغان ھەممە ئۈسكۈنە تىزىمىنى كۆرسىتىپ، ئۇلارنى يۈكلەشكە يول قويىدۇ. ئېگەرسىزلەيدىغان ھەممە ئۈسكۈنە تىزىمىنى كۆرسىتىپ، ئۇلارنى ئېگەرسىزلىشىڭىزگە يول قويىدۇ. قۇلۇپلانغان شىفىرلىق ئۈسكۈنە تىزىمىنىڭ ھەممىسىنى كۆرسىتىپ، ئۇلارنى قۇلۇپلاشقا يول قويىدۇ. قۇلۇپسىزلايدىغان ھەممە ئۈسكۈنىلەر تىزىمىنى كۆرسىتىپ، قۇلۇپ ئېچىشقا يول قويىدۇ. ئۈسكۈنىنى ئېگەرلەيدۇ ئۈسكۈنە چىقار قۇلۇپلا ئېگەرلە قۇلۇپسىزلا ئېگەرسىزلە قاچىنى قۇلۇپسىزلايدۇ ئۈسكۈنىنى ئېگەرسىزلەيدۇ 