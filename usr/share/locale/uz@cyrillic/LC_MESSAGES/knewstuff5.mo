��          �   %   �      P     Q  �   c  �     �   �     Y     a     e     r     z     �     �     �     �     �     �     �  	   �     �     �  
   �     �  	          	          �  &     �  I  �  T  5    �	     �
     �
     �
     �
     �
  
   �
     �
     �
  )        0     5     G     V     k  :   x     �     �     �     �     �                                                                             	                     
                                <br />Version: %1 <qt>Cannot start <i>gpg</i> and check the validity of the file. Make sure that <i>gpg</i> is installed, otherwise verification of downloaded resources will not be possible.</qt> <qt>Cannot start <i>gpg</i> and retrieve the available keys. Make sure that <i>gpg</i> is installed, otherwise verification of downloaded resources will not be possible.</qt> <qt>Cannot start <i>gpg</i> and sign the file. Make sure that <i>gpg</i> is installed, otherwise signing of the resources will not be possible.</qt> Author: BSD Description: Details Error Finish GPL Install Key used for signing: LGPL License: Opposite to BackNext Password: Reset Select Signing Key Server: %1 Title: Uninstall Update Username: Version: Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-12 03:00+0100
PO-Revision-Date: 2009-10-17 00:34+0200
Last-Translator: Mashrab Kuvatov <kmashrab@uni-bremen.de>
Language-Team: Uzbek <floss-uz-l10n@googlegroups.com>
Language: uz
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=1; plural=0;
 <br />Версия: %1 <qt><i>Gpg</i> ёрдамида файлнинг ҳақиқийлигини текшириб бўлмади. <i>Gpg</i> дастури ўрнатилганлигига ишонч ҳосил қилинг акс ҳолда, ёзиб олинган файлнинг ҳақиқийлигини текшириб бўлмайди.</qt> <qt><i>Gpg</i> ёрдамида мавжуд калитларнинг рўйхатини аниқлаб бўлмади. <i>Gpg</i> дастури ўрнатилганлигига ишонч ҳосил қилинг акс ҳолда, ёзиб олинган файлнинг ҳақиқийлигини текшириб бўлмайди.</qt> <qt>Файлга имзо қўйиш учун <i>gpg</i> дастурини ишга тушириб бўлмади. <i>Gpg</i> дастури ўрнатилганлигига ишонч ҳосил қилинг акс ҳолда, имзолашни иложи бўлмайди.</qt> Муаллиф: BSD Таърифи: Тафсилотлар Хато Тайёр GPL Ўрнатиш Имзолаш учун калитлар: LGPL Лицензия: Кейинги Махфий сўз: Тиклаш Имзо қўйиш учун калитни танланг Сервер: %1 Сарлавҳа: Олиб ташлаш Янгилаш Фойдаланувчи: Версия: 