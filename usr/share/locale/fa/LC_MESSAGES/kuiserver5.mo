��          �   %   �      P     Q     b     w     �     �     �  
   �     �     �     �     �     �  /        3     Q     p     v     �     �     �     �     �     �     �       �  %     �     �     �  =   	     G  5   \     �  
   �     �     �     �  '   �  L   �  '   F  (   n     �     �     �     �  8   �  9   	  >   C  ?   �  /   �  0   �                            
                                                                                             	    %1 file %1 files %1 folder %1 folders %1 of %2 processed %1 of %2 processed at %3/s %1 processed %1 processed at %2/s Appearance Behavior Cancel Clear Configure... Finished Jobs List of running file transfers/jobs (kuiserver) Move them to a different list Move them to a different list. Pause Remove them Remove them. Resume Show all jobs in a list Show all jobs in a list. Show all jobs in a tree Show all jobs in a tree. Show separate windows Show separate windows. Project-Id-Version: kuiserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2011-09-26 19:02+0330
Last-Translator: Mohamad Reza Mirdamadi <mohi@linuxshop.ir>
Language-Team: Farsi (Persian) <>
Language: fa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 1.2
 %1 پرونده‌ها %1 پوشه‌ها %1 از %2 پردازش شد %1 از %2 پردازش شد با سرعت %3 در ثانیه %1 پردازش شد %1 پردازش شد با سرعت %2 در ثانیه ظاهر رفتار لغو پاک کردن پیکربندی... کارهای به اتمام رسیده لیست انتقال فایل/کارهای در حال اجرا (kuiserver) انتقال به لیست متفاوت انتقال به لیست متفاوت. مکث حذف آنها پاک کردن. ازسرگیری مشاهده‌ی تمام کارها در یک لیست مشاهده‌ی تمام کارها در یک لیست. مشاهده‌ی تمام کارها به صورت درختی مشاهده‌ی تمام کارها به صورت درختی. مشاهده پنجره‌های جداگانه مشاهده پنجره‌های جداگانه. 