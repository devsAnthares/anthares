��          �      <      �     �  �   �  m   R  `   �     !     .     F     R      i     �     �     �     �     �  ?   �  Y     +   w  �  �     @  �   _  �   T  �     
   �  4   �  
   	  3   	  $   D	     i	  ;   z	     �	  #   �	     �	  p   �	  �   b
  Q   �
                                                                 
                             	       (c) 2003-2007 Fredrik Höglund <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Fredrik Höglund NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. Project-Id-Version: kcminput
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2012-01-12 16:57+0330
Last-Translator: Mohammad Reza Mirdamadi <mohi@linuxshop.ir>
Language-Team: Farsi (Persian) <>
Language: fa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=1; plural=0;
 (c) 2003-2007 Fredrik Höglund <qt>آیا مطمئن هستید که می‌خواهید چهره مکان‌نمای <i>%1</i> را حذف کنید؟ <br />این‌ کار همه پرونده‌های نصب‌شده توسط این چهره را حذف می‌کند.</qt> <qt>نمی‌توانید چهره‌ای که هم اکنون استفاده می‌کنید را حذف نمایید.<br />ابتدا باید به چهره دیگری سودهی کنید.</qt> هم اکنون، چهره‌ای با نام %1در پوشه چهره شمایل شما وجود دارد. آیا می‌خواهید با این جایگزین کنید؟ تأیید تنظیمات مکان‌نما تغییر یافت توصیف کشیدن یا تحریر نشانی وب چهره mohi@linuxshop.ir , kazemi@itland.ir Fredrik Höglund محمدرضا میردامادی , نازنین کاظمی نام چهره جای‌نوشت شود؟ حذف چهره به نظر نمی‌رسد پرونده %1یک بایگانی چهره مکان‌نمای معتبر باشد. قادر به بارگیری بایگانی چهره مکان‌نما نیست؛ لطفاً، بررسی کنید که نشانی %1 درست باشد. قادر به یافتن بایگانی چهره مکان‌نمای %1 نیست. 