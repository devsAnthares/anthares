��    B      ,  Y   <      �  i   �  m        y  b   �     �     	  6        O  7   _     �     �  	   �     �  (   �  )   �  )   (     R     o  Y   u     �     �  �   �      y	  .   �	     �	  
   �	     �	     �	     
     
     !
     5
     B
     J
     c
     r
     w
     �
     �
     �
     �
     �
  	   �
  
   �
     �
     �
  	     
     
   *     5     B  	   `     j     o  
   �  �   �     (  8   8  �   q     �  8   	  ,   B  ,   o  %   �     �  �  �  �   �  �   (  /     {   3  )   �     �  <   �     $  <   2  "   o     �     �  #   �  5   �  5     9   D  *   ~  
   �  �   �     Z     x  �   �  ?   �  C   �     	          #     ;     K     g      {     �     �  /   �     �     �  _     )   c     �  
   �     �     �     �     �     �  /   �     &     4     F     V  2   j     �  
   �  (   �     �    �       u   (  �   �     j  @   |  g   �  k   %  E   �  &   �     $   !   %          ;   ,          B                 8   #                 7   9       +              )   ?   -   <         6                                    *   2       >                1       @       .   5   3   :   A         &      /                4              0      "             =   (   '         	   
           @info User changed Phonon backendTo apply the backend change you will have to log out and back in again. A list of Phonon Backends found on your system.  The order here determines the order Phonon will use them in. Apply Device List To... Apply the currently shown device preference list to the following other audio playback categories: Audio Hardware Setup Audio Playback Audio Playback Device Preference for the '%1' Category Audio Recording Audio Recording Device Preference for the '%1' Category Backend Colin Guthrie Connector Copyright 2006 Matthias Kretz Default Audio Playback Device Preference Default Audio Recording Device Preference Default Video Recording Device Preference Default/Unspecified Category Defer Defines the default ordering of devices which can be overridden by individual categories. Device Configuration Device Preference Devices found on your system, suitable for the selected category.  Choose the device that you wish to be used by the applications. EMAIL OF TRANSLATORSYour emails Failed to set the selected audio output device Front Center Front Left Front Left of Center Front Right Front Right of Center Hardware Independent Devices Input Levels Invalid KDE Audio Hardware Setup Matthias Kretz Mono NAME OF TRANSLATORSYour names Phonon Configuration Module Playback (%1) Prefer Profile Rear Center Rear Left Rear Right Recording (%1) Show advanced devices Side Left Side Right Sound Card Sound Device Speaker Placement and Testing Subwoofer Test Test the selected device Testing %1 The order determines the preference of the devices. If for some reason the first device cannot be used Phonon will try to use the second, and so on. Unknown Channel Use the currently shown device list for more categories. Various categories of media use cases.  For each category, you may choose what device you prefer to be used by the Phonon applications. Video Recording Video Recording Device Preference for the '%1' Category  Your backend may not support audio recording Your backend may not support video recording no preference for the selected device prefer the selected device Project-Id-Version: kcm_phonon
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2012-06-22 10:12+0430
Last-Translator: Mohammad Reza Mirdamadi <mohi@linuxshop.ir>
Language-Team: Farsi (Persian) <kde-i18n-fa@kde.org>
Language: fa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.4
Plural-Forms: nplurals=1; plural=0;
 برای اعمال تغییرات پایانه پشتیبانی شما باید از این نشست خارج شده و دوباره وارد شوید. فهرستی از پایانه‌های پشتیبانی  Phonon که روی سیستمتان یافت شد.  ترتیب اینجا تعیین می‌کند ترتیب Phonon آنها را استفاده می‌کند. اعمال لیست دستگاه‌ها به... اعمال تنظیمات نشان داده شده‌ی جاری به بخش‌های پخش صدای دیگر در زیر: تنظیمات سخت‌افزار صدا پخش صدا تنظیم دستگاه پخش صدا برای بخش «%1» ضبط صدا تنظیم دستگاه ضبط صدا برای بخش «%1» پایانه‌ی پشتیبانی Colin Guthrie متصل کننده حق نشر ۲۰۰۶ Matthias Kretz تنظیم دستگاه پیش‌فرض پخش صدا تنظیم دستگاه پیش‌فرض ضبط صدا تنظیم دستگاه پیش‌فرض ضبط تصویر بخش پیش‌فرض/مشخص‌نشده تعویق اولویت پیش‌فرض دستگاه‌ها را تعریف می‌کند که می‌تواند توسط بخش‌های جداگانه بازنویسی شود پیکربندی دستگاه تنظیم دستگاه دستگاه‌هایی متناسب برای بخش‌های انتخاب شده روی سیستمتان یافت شد.  دستگاهی را انتخاب کنید که می‌خواهید توسط نرم‌افزارها استفاده شود. mohi@linuxshop.ir , kazemi@itland.ir , vahid.fazl2000@gmail.com تنتظیم دستگاه صدای انتخابی شکست خورد جلو وسط جلو جپ جلو چپ یا وسط جلو راست جلو راست یا وسط سخت‌افزار دستگاه‌های مستقل سطوح ورودی نامعتبر تنظیمات سخت‌افزار صدای KDE Matthias Kretz تک محمدرضا میردامادی , نازنین کاظمی , وحید فضل الله زاده پیمانه‌ی پیکربندی Phonon پخش (%1) ترجیح مجموعه تنظیمات عقب وسط عقب چپ عقب راست ضبط (%1) نمایش دستگاه‌های پیشرفته کنار چپ کنار راست کارت صدا دستگاه صدا قرارگیری بلندگو و امتحان آن ساب ووفر آزمون امتحان دستگاه برگزیده در حال امتحان %1 ترتیب، تنظیم دستگاههای خروجی را تعیین می‌کند. اگر به دلایلی دستگاه اول نتوانست استفاده شود، Phonon سعی در استفاده از دستگاه دوم می‌کند و به همین ترتیب. کانال ناشناخته از لیست دستگاه‌های نشان داده شده برای بخش‌های بیشتر استفاده کن. دسته های گوناگون استفاده‌های رسانه. برای هر دسته ممکن است دستگاهی که می‌خواهید خروجی از آن باشد را انتخاب کنید. ضبط تصویر تنظیم دستگاه ضبط تصویر برای بخش «%1» پایانه‌ي پشتیبانی شما ممکن است از ضبط صدا پشتیبانی نکند. پایانه‌ي پشتیبانی شما ممکن است از ضبط تصویر پشتیبانی نکند. تنظیمی برای دستگاه برگزیده وجود ندارد ترجیح دستگاه برگزیده 