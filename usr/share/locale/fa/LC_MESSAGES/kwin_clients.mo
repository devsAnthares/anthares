��          t      �                 !  �   (  �   �  Z   2     �     �     �     �     �  �  �  %   m     �  �   �    t  �   �     <     Z     _      h  0   �                     
   	                                    Animate buttons Center Check this option if the window border should be painted in the titlebar color. Otherwise it will be painted in the background color. Check this option if you want the buttons to fade in when the mouse pointer hovers over them and fade out again when it moves away. Check this option if you want the titlebar text to have a 3D look with a shadow behind it. Config Dialog Left Right Title &Alignment Use shadowed &text Project-Id-Version: kwin_clients
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-02 03:16+0100
PO-Revision-Date: 2007-06-24 11:12+0330
Last-Translator: Nazanin Kazemi <kazemi@itland.ir>
Language-Team: Persian <kde-i18n-fa@kde.org>
Language: fa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=1; plural=0;
 پویا‌نمایی دکمه‌ها مرکز اگر لبه‌های پنجره باید به رنگ میله عنوان رنگ شوند، این گزینه را علامت بزنید. در غیر این صورت، به رنگ زمینه رنگ می‌شوند. اگر می‌خواهید دکمه‌ها را هنگامی که اشاره‌گر موشی روی آنها قرار می‌گیرد محو کرده و دوباره با دور شدن اشاره‌گر پدیدار شوند، این گزینه را علامت بزنید. اگر می‌خواهید متن میله عنوان دارای یک نمای سه بعدی با سایه‌ای پشت آن باشد، این گزینه را علامت بزنید. پیکربندی محاوره چپ راست &تراز کردن عنوان‌ استفاده از &متن سایه‌دار‌ 