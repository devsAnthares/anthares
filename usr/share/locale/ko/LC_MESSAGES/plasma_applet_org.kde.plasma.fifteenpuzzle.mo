��          �            x  
   y  	   �     �     �  3   �     �     �                    %     *     F  B   Y     �  �  �     <     C     S     a  5   y  
   �     �  
   �     �     �     �  .     )   2  
   \     g     	                                                  
                        Appearance Browse... Choose an image Fifteen Puzzle Image Files (*.png *.jpg *.jpeg *.bmp *.svg *.svgz) Number color Path to custom image Piece color Show numerals Shuffle Size Solve by arranging in order Solved! Try again. The time since the puzzle started, in minutes and secondsTime: %1 Use custom image Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-01-12 03:59+0100
PO-Revision-Date: 2017-01-22 20:06+0100
Last-Translator: Shinjo Park <kde@peremen.name>
Language-Team: Korean <kde@peremen.name>
Language: ko
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 모양 찾아보기... 그림 선택 열다섯 조각 퍼즐 그림 파일 (*.png *.jpg *.jpeg *.bmp *.svg *.svgz) 숫자 색 사용자 정의 그림 경로 조각 색 숫자 보이기 섞기 크기 숫자대로 정렬해서 풀어 보십시오. 풀었습니다! 다시 해 보십시오. 시간: %1 사용자 정의 그림 사용 