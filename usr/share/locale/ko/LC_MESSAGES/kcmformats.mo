��          �   %   �      0     1     9     J     \  	   i     s     z  a   �     �  
     	             /     G     [      n      �     �  	   �     �     �     �  7   �  �  &     �     �     �     �     �     �       S        m     �     �     �     �     �     �  	   �  	   �  	                   (     4  <   <                                                   
                               	                                             %1 (%2) %1 (long format) %1 (short format) %1 - %2 (%3) &Numbers: &Time: <b>Description</b> <h1>Formats</h1>You can configure the formats used for time, dates, money and other numbers here. Co&llation and Sorting: Currenc&y: Currency: De&tailed Settings Format Settings Changed Measurement &Units: Measurement Units: Measurement comboboxImperial UK Measurement comboboxImperial US Measurement comboboxMetric No change Numbers: Re&gion: Time: Your changes will take effect the next time you log in. Project-Id-Version: kcmlocale
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-11 03:20+0100
PO-Revision-Date: 2017-12-02 23:21+0100
Last-Translator: Shinjo Park <kde@peremen.name>
Language-Team: Korean <kde@peremen.name>
Language: ko
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 %1(%2) %1(긴 형식) %1(짧은 형식) %1 - %2 (%3) 숫자(&N): 시간(&T): <b>설명</b> <h1>형식</h1>날짜, 시간, 통화, 숫자 형식을 설정할 수 있습니다. 분류 및 정렬 순서(&L): 통화(&Y): 통화: 자세한 설정(&T) 형식 설정 변경됨 측량 단위계(&U): 측량 단위계: 영국법 미국법 미터법 변화 없음 숫자: 지역(&G): 시간: 다음에 로그인할 때 변경 사항이 적용됩니다. 