��    _                   C   	  /   M  -   }     �     �     �     �      	     	     1	     >	     J	  
   S	     ^	     g	     p	     �	  	   �	     �	     �	     �	  ,   �	  	    
     

  
   )
     4
     L
     `
     u
     �
     �
     �
     �
     �
  	   �
     �
     �
     
               !     (  	   ;     E     ]  
   r     }     �  
   �     �     �     �  
   �     �     �               $     2     @     R     h     y     �     �     �     �  	   �     �     �     �               4     Q     j     �     �     �     �  	   �     �  ,   �          !     0     @     L     S     b     t     �  #   �     �  �  �     [  	   j     t     �     �     �  !   �     �          
          %     ,  	   3  	   =     G  	   W     a     h          �  ,   �  	   �     �     �          /     G     \     p     �     �     �     �     �     �     �  
   �     �                 	   /     9     W     o     �     �     �     �     �     �     �     �     �          $     2     @     N     b  #   y     �  3   �     �     �     �  	                  &     A     U  '   q  $   �  !   �     �     �          1     9     J  +   d     �  
   �     �     �  	   �     �     �  $   �  "   "  /   E     u         Q         Y   5           %       H       J   !              \   *       @   V   4   [       A   (   
       U   B   ]   ^       $   _         '   >       D   K      -                  ?      ,   O   0       R   8   6   W   2   I   =   X   E   #       N   C       T   G   M          3   "         9             +          S          F           ;   :                    &   <   L          P         7          .       /       1                     )            	   Z       @action opens a software center with the applicationManage '%1'... @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Desktop Add to Favorites Add to Panel (Widget) Align search results to bottom All Applications App name (Generic name)%1 (%2) Applications Apps & Docs Behavior Categories Computer Contacts Description (Name) Description only Documents Edit Application... Edit Applications... End session Expand search to bookmarks, files and emails Favorites Flatten menu to a single level Forget All Forget All Applications Forget All Contacts Forget All Documents Forget Application Forget Contact Forget Document Forget Recent Documents General Generic name (App name)%1 (%2) Hibernate Hide %1 Hide Application Icon: Lock Lock screen Logout Name (Description) Name only Often Used Applications Often Used Documents Often used On All Activities On The Current Activity Open with: Pin to Task Manager Places Power / Session Properties Reboot Recent Applications Recent Contacts Recent Documents Recently Used Recently used Removable Storage Remove from Favorites Restart computer Run Command... Run a command or a search query Save Session Search Search results Search... Searching for '%1' Session Show Contact Information... Show In Favorites Show applications as: Show often used applications Show often used contacts Show often used documents Show recent applications Show recent contacts Show recent documents Show: Shut Down Sort alphabetically Start a parallel session as a different user Suspend Suspend to RAM Suspend to disk Switch User System System actions Turn off computer Type to search. Unhide Applications in '%1' Unhide Applications in this Submenu Widgets Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2017-12-02 23:40+0100
Last-Translator: Shinjo Park <kde@peremen.name>
Language-Team: Korean <kde@peremen.name>
Language: ko
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 '%1' 관리... 선택... 아이콘 초기화 데스크톱에 추가 책갈피에 추가 패널에 추가(위젯) 검색 결과를 아래로 정렬 모든 프로그램 %1 (%2) 프로그램 앱과 문서 행동 분류 컴퓨터 연락처 설명 (이름) 설명만 문서 프로그램 편집... 프로그램 편집... 세션 끝내기 책갈피, 파일, 이메일에서도 찾기 책갈피 단일 단계로 메뉴 축소 모두 잊어버리기 모든 프로그램 잊기 모든 연락처 잊기 모든 문서 잊기 프로그램 잊기 연락처 잊기 문서 잊기 최근 문서 비우기 일반 %1 (%2) 최대 절전 모드 %1 숨기기 프로그램 숨기기 아이콘: 잠금 화면 잠그기 로그아웃 이름 (설명) 이름만 자주 사용한 프로그램 자주 사용한 문서 자주 사용됨 모든 활동에 현재 활동에만 다음으로 열기: 작업 관리자에 고정 위치 전원/세션 속성 다시 시작 최근 프로그램 최근 연락처 최근 문서 최근 항목 최근 항목 이동식 저장소 책갈피에서 삭제 컴퓨터를 다시 시작합니다 명령 실행... 명령을 실행하거나 검색을 시작합니다 세션 저장 찾기 찾은 결과 찾기... '%1' 검색 중 세션 연락처 정보 보기... 책갈피에 표시 프로그램 표시 형식: 자주 사용한 프로그램 보이기 자주 사용한 연락처 보이기 자주 사용한 문서 보이기 최근 프로그램 보이기 최근 연락처 보이기 최근 문서 보이기 표시: 컴퓨터 끄기 가나다순으로 정렬 다른 사용자로 새 세션 시작하기 대기 모드 RAM 절전 디스크 절전 사용자 전환 시스템 시스템 동작 컴퓨터를 끕니다 입력하는 대로 검색합니다. '%1'의 프로그램 숨김 해제 이 하위 메뉴의 프로그램 숨김 해제 위젯 