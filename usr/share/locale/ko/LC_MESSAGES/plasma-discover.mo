��    U      �  q   l      0     1     G     O  5   _  #   �  E   �  E   �  >   E     �     �  7   �     �     �     	     /	     N	     m	     {	     �	     �	     �	     �	     �	     �	     �	     �	      
     
  !   
  F   A
     �
     �
     �
  <   �
            *        A     O     W  	   _     i     y     �      �     �  
   �     �     �       
     F     :   _     �     �     �     �     �  8   �            	   $  
   .     9     I     R     c     x     �     �     �     �     �     �     �       
        (     8     X     `     q     �  $   �  �  �     K     f     m  G   �  5   �  U      U   V  N   �     �       ?   -     m     �  $   �     �     �     �                    -     @     X     m     �     �     �     �  !   �  J   �  %   :     `     z  B   �     �     �  .   �               &  	   -     7     G     N  .   ]  +   �     �  !   �     �            \   $  K   �     �     �     �     �     �  b   �     a     h  	   �  
   �     �     �     �     �     �     �     �          &     .     K     R     ^     ~     �     �     �     �      �       $           O   '          T   8      2   $          /   E   D   A   3                 =       J           C      R                0   1   ;   L       	   G   .   "   6                 P   
      7      M                 )   ,   F       @      B       S          >       +   K   Q          I              (          #   9       :   4   !      U   H                        %       N   -         <   &          ?   *           5    
Also available in %1 %1 (%2) <b>%1</b> by %2 <em>%1 out of %2 people found this review useful</em> <em>Tell us about this review!</em> <em>Useful? <a href='true'><b>Yes</b></a>/<a href='false'>No</a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'><b>No</b></a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'>No</a></em> @infoFetching updates @infoFetching... @infoIt is unknown when the last check for updates was @infoLooking for updates @infoNo updates @infoNo updates are available @infoShould check for updates @infoThe system is up to date @infoUpdates @infoUpdating... Accept Addons Aleix Pol Gonzalez An application explorer Apply Changes Available backends:
 Available modes:
 Back Cancel Checking for updates... Compact Mode (auto/compact/full). Could not close the application, there are tasks that need to be done. Could not find category '%1' Couldn't open %1 Delete the origin Directly open the specified application by its package name. Discard Discover Display a list of entries with a category. Extensions... Help... Install Installed Jonathan Thomas Launch License: List all the available backends. List all the available modes. Loading... Local package file to install More Information... More... No Updates Open Discover in a said mode. Modes correspond to the toolbar buttons. Open with a program that can deal with the given mimetype. Rating: Remove Resources for '%1' Review Reviewing '%1' Running as <em>root</em> is discouraged and unnecessary. Search Search in '%1'... Search... Search: %1 Search: %1 + %2 Settings Short summary... Show reviews (%1)... Sorry, nothing found... Source: Specify the new source for %1 Still looking... Summary: Supports appstream: url scheme Tasks Tasks (%1%) Unable to find resource: %1 Update All Update Selected Update section nameUpdate (%1) Updates unknown reviewer updates not selected updates selected © 2010-2016 Plasma Development Team Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:02+0100
PO-Revision-Date: 2017-12-02 23:48+0100
Last-Translator: Shinjo Park <kde@peremen.name>
Language-Team: Korean <kde@peremen.name>
Language: ko
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 
%1에서도 사용 가능 %1(%2) <b>%1</b>, %2 작성 <em>%2명 중 %1명이 이 리뷰를 유용한 것으로 표시함</em> <em>이 리뷰에 대해서 알려 주십시오!</em> <em>유용합니까? <a href='true'><b>예</b></a>/<a href='false'>아니요</a></em> <em>유용합니까? <a href='true'>예</a>/<a href='false'><b>아니요</b></a></em> <em>유용합니까? <a href='true'>예</a>/<a href='false'>아니요</a></em> 업데이트 가져오는 중 가져오는 중... 마지막으로 업데이트를 확인한 때를 알 수 없음 업데이트 확인 중 업데이트 없음 사용 가능한 업데이트 없음 업데이트 확인 여부 시스템이 최신 상태임 업데이트 업데이트 중... 수락 부가 기능 Aleix Pol Gonzalez 프로그램 탐색기> 변경 사항 적용 사용 가능한 백엔드:
 사용 가능한 모드:
 뒤로 취소 업데이트 검색 중... 간편 모드(auto/compact/full). 진행 중인 작업이 있어서 프로그램을 닫을 수 없습니다. 분류 '%1'을(를) 찾을 수 없음 %1을(를) 열 수 없음 원본 삭제 지정한 프로그램을 패키지 이름으로 바로 엽니다. 무시 발견 분류별로 항목 목록을 표시합니다. 확장 기능... 도움말... 설치 설치됨 Jonathan Thomas 실행 라이선스:  사용 가능한 백엔드를 표시합니다. 사용 가능한 모드를 표시합니다. 불러오는 중... 설치할 로컬 패키지 파일 자세한 정보... 더 보기... 업데이트 없음 발견을 said 모드로 시작합니다. 모드는 도구 모음 단추에 대응합니다. 지정한 MIME 형식을 처리할 수 있는 프로그램으로 엽니다. 별점: 삭제 '%1'의 자원 리뷰 '%1' 리뷰 <em>루트</em> 계정으로 실행하는 것은 권장하지 않으며 필요하지 않습니다. 검색 '%1'에서 검색 중... 찾기... 찾기: %1 찾기: %1 + %2 설정 간단한 요약... 리뷰 보기(%1)... 찾을 수 없습니다... 원본: %1의 새 원본 지정 계속 찾는 중... 요약: appstream: URL 형식 지원 작업 작업(%1%) 자원을 찾을 수 없음: %1 모두 업데이트 업데이트 선택됨 업데이트(%1) 업데이트 알 수 없는 리뷰어 업데이트 선택되지 않음 업데이트 선택됨 © 2010-2016 Plasma Development Team 