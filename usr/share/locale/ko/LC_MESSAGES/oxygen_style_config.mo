��    ?        Y         p     q     u     �  !   �  !   �     �  
   �             (   6  I   _  :   �  <   �  2   !  .   T  1   �  (   �  1   �  o        �  !   �     �  0   �     �     	     5	     L	     l	     �	     �	  ,   �	     �	     �	     �	     �	  !   �	      
     2
     A
     T
  	   c
  
   m
  
   x
     �
     �
     �
     �
  
   �
  &   �
     
          2     D     [     g     w     �     �     �     �     �     �  �  �  	   �  #   �  (   �  $   �  $        ,     D  "   T     w  "   �  W   �  L     Y   T  H   �  B   �  5   :  '   p  B   �  |   �     X  '   e  &   �  @   �  $   �  (        C  $   [  +   �  	   �     �  1   �     �       	   0     :     A     ]     n     |     �     �     �     �     �      �      �          2  +   ?  
   k     v     �     �     �  !   �  %   �          #     =     D     K     R        =   5   1                 ?           3       "   '          *             7   9         8                  <   (   ,   /                     ;      #   2   &         	      .                 :      
   -       !                         0             +   >       6      $                     %   )                4         ms &Tree expander size: &Use selection color (plain) Always Hide Keyboard Accelerators Always Show Keyboard Accelerators Animation type: Animations Bottom arrow button type: Combo box transitions Configure fading transition between tabs Configure fading transition when a combo box's selected choice is changed Configure fading transition when a label's text is changed Configure fading transition when an editor's text is changed Configure menu bars' mouseover highlight animation Configure menus' mouseover highlight animation Configure progress bars' busy indicator animation Configure progress bars' steps animation Configure toolbars' mouseover highlight animation Configure widgets' focus and mouseover highlight animation, as well as widget enabled/disabled state transition Dialog Drag windows from all empty areas Drag windows from titlebar only Drag windows from titlebar, menubar and toolbars Draw focus indicator in lists Draw toolbar item separators Draw tree branch lines Draw window background gradient Enable extended resize handles Fade Fade duration: Focus, mouseover and widget state transition Follow Mouse Follow mouse duration: Frame General Keyboard accelerators visibility: Label transitions Menu Highlight Menu bar highlight Menu highlight No button No buttons One button Oxygen Settings Progress bar animation Progress bar busy indicator Scrollbar wi&dth: Scrollbars Show Keyboard Accelerators When Needed Tab transitions Text editor transitions Toolbar highlight Top arrow button type: Two buttons Use dar&k color Use selec&tion color (subtle) Views Windows' &drag mode: px triangle sizeNormal triangle sizeSmall triangle sizeTiny Project-Id-Version: kstyle_config
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-24 03:18+0200
PO-Revision-Date: 2017-12-02 23:32+0100
Last-Translator: Shinjo Park <kde@peremen.name>
Language-Team: Korean <kde@peremen.name>
Language: ko
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 밀리초 트리 확장 삼각형 크기(&T): 단순한 선택 색상 사용하기(&U) 항상 키보드 가속기 숨기기 항상 키보드 가속기 보이기 애니메이션 종류: 애니메이션 아래쪽 화살표 단추 종류: 콤보 상자 전환 탭 전환 페이드 효과 설정 콤보 상자의 선택 항목이 변경되었을 때의 페이드 전환 효과 설정 레이블 텍스트가 변경되었을 때의 페이드 전환 효과 설정 텍스트 편집기의 텍스트가 변경되었을 때의 페이드 전환 효과 설정 메뉴 표시줄로 마우스가 지나다닐 때 애니메이션 설정 메뉴 위로 마우스가 지나다닐 때 애니메이션 설정 진행 표시줄의 진행 중 애니메이션 설정 진행 표시줄 애니메이션 설정 도구모음 위로 마우스가 지날 때 애니메이션 설정 위젯의 초점 및 마우스가 지나다닐 때 강조 애니메이션, 활성/비활성 상태 전환을 설정합니다 대화상자 모든 빈 영역으로 드래그하기 제목 표시줄로만 드래그하기 제목 및 메뉴 표시줄, 도구 모음으로 드래그하기 목록에 초점 표시기 그리기 도구 모음 항목 구분자 그리기 트리 가지 그리기 창 배경 그라데이션 그리기 확장된 크기 조절 핸들 사용하기 페이드 페이드 시간: 초점, 마우스 움직임, 위젯 상태 전환 마우스 따라가기 마우스를 따라갈 시간: 프레임 일반 키보드 가속기 표시: 레이블 전환 메뉴 강조 메뉴 표시줄 강조 메뉴 강조 단추 없음 단추 없음 단추 하나 Oxygen 설정 진행 표시줄 애니메이션 진행 표시줄 애니메이션 스크롤바 폭(&D): 스크롤바 필요할 때 키보드 가속기 보이기 탭 전환 텍스트 편집기 전환 도구 모음 강조 위쪽 화살표 단추 종류: 단추 두개 어두운 색상 사용하기(&K) 대체 선택 색상 사용하기(&T) 보기 창 드래그 모드(&D): 픽셀 일반 작게 매우 작게 