ή                ό       H     I     N    k  ^  ϋ  P   Z     «     »     Λ     Ψ     υ               5    K     Ξ      Σ    τ  §    q   3
     ₯
     Ή
     Η
  (   έ
  %        ,      K     l                                          
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2007-12-26 20:15+0900
Last-Translator: Shinjo Park <kde@peremen.name>
Language-Team: Korean <cho.sungjae@gmail.com>
Language: ko
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
  μ΄ μμ μλ¦Ό μκ° μ ν(&S): <h1>μμ νμμ€ μλ¦Ό</h1>
μμ νμμ€μ μ¬μ©ν  μμ μλ¦Όμ μ€μ ν©λλ€. μ΄κ²μ μμ νμμ€ λ¨μΆμ
λμκ°λ λͺ¨λμκ³κ° λνλλ©΄μ μ€ν μ€μμ μλ € μ€λλ€.
λͺλͺ νλ‘κ·Έλ¨μ μ΄ μμ μλ¦Όκ³Ό μ λμνμ§ μμ μλ μμ΅λλ€.
μ΄λ¬ν κ²½μ° μ»€μλ 'μμ μλ¦Ό μκ° μ ν'μ μ§μ ν μκ° λμλ§
κΉλΉ‘μλλ€. <h1>μ€ν μ€ μ»€μ</h1>
KDEμμλ νλ‘κ·Έλ¨ μμμ μλ¦¬λ μ€ν μ€ μ»€μλ₯Ό μ κ³΅ν©λλ€.
μ€ν μ€ μ»€μλ₯Ό μ¬μ©νλ €λ©΄ μ½€λ³΄ μμμμ μ¬μ©ν  μκ°μ  νΌλλ°±μ
μ’λ₯λ₯Ό μ ννμ­μμ€.
λͺλͺ νλ‘κ·Έλ¨μ μ΄ μμ μλ¦Όκ³Ό μ λμνμ§ μμ μλ μμ΅λλ€.
μ΄λ¬ν κ²½μ° μ»€μλ 'μμ μλ¦Ό μκ° μ ν'μ μ§μ ν μκ° λμλ§
κΉλΉ‘μλλ€. <h1>μ€ν νΌλλ°±</h1> μ΄ κ³³μμ νλ‘κ·Έλ¨μ΄ μ€νλ  λμ νΌλλ°±μ μ€μ ν  μ μμ΅λλ€. κΉλΉ‘μ΄λ μ»€μ νλ μ»€μ μ€ν μ€ μ»€μ(&Y) μμ νμμ€ μλ¦Ό μ¬μ©νκΈ°(&T) μ€ν μ€ μ»€μ μ¬μ©νμ§ μμ μλμ μΈ μ€ν μ€ μ»€μ μμ μλ¦Ό μκ° μ ν(&U): μμ νμμ€ μλ¦Ό(&N) 