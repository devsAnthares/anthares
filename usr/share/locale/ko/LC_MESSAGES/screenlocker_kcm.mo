��          �            h     i  
   �     �  .   �     �     �     �      �  *         9  ,   Z  ,   �  0   �     �  �  �  *   �  	   �     �  8   �     �            %   $  2   J  -   }     �     �  5   �     �                      
   	                                                       &Lock screen on resume: Activation Error Failed to successfully test the screen locker. Immediately Keyboard shortcut: Lock Session Lock screen automatically after: Lock screen when waking up from suspension Re&quire password after locking: Spinbox suffix. Short for minutes min  mins Spinbox suffix. Short for seconds sec  secs The global keyboard shortcut to lock the screen. Wallpaper &Type: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:10+0100
PO-Revision-Date: 2017-12-03 00:05+0100
Last-Translator: Shinjo Park <kde@peremen.name>
Language-Team: Korean <kde@peremen.name>
Language: ko
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 다시 시작할 때 화면 잠그기(&L): 활성화 오류 잠금 화면을 테스트하는 데 실패했습니다. 즉시 키보드 단축키: 세션 잠금 자동으로 화면을 잠글 시간: 대기 모드에서 깨어날 때 화면 잠그기 잠금 이후 암호를 물어볼 시간(&Q): 분 초 화면을 잠글 전역 키보드 단축키입니다. 배경 그림 종류(&T): 