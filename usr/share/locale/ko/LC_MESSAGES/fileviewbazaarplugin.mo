��    !      $  /   ,      �  .   �  1     9   J     �  -   �  &   �  )   �  .   #  &   R  )   y  .   �  &   �  )   �  2   #  5   V  =   �  #   �     �  !     '   /  "   W  0   z  '   �  *   �     �          7     R     j     �     �  &   �  t  �  %   S	  (   y	  9   �	     �	  3   �	     &
  "   F
  /   i
     �
  0   �
  /   �
       '   2  (   Z  .   �  3   �  +   �  $        7  &   S     z  5   �  -   �  $   �          0     A     O  
   ]     h     v  "   �                                                              	                                
                                    !                             @info:statusAdded files to Bazaar repository. @info:statusAdding files to Bazaar repository... @info:statusAdding of files to Bazaar repository failed. @info:statusBazaar Log closed. @info:statusCommit of Bazaar changes failed. @info:statusCommitted Bazaar changes. @info:statusCommitting Bazaar changes... @info:statusPull of Bazaar repository failed. @info:statusPulled Bazaar repository. @info:statusPulling Bazaar repository... @info:statusPush of Bazaar repository failed. @info:statusPushed Bazaar repository. @info:statusPushing Bazaar repository... @info:statusRemoved files from Bazaar repository. @info:statusRemoving files from Bazaar repository... @info:statusRemoving of files from Bazaar repository failed. @info:statusReview Changes failed. @info:statusReviewed Changes. @info:statusReviewing Changes... @info:statusRunning Bazaar Log failed. @info:statusRunning Bazaar Log... @info:statusUpdate of Bazaar repository failed. @info:statusUpdated Bazaar repository. @info:statusUpdating Bazaar repository... @item:inmenuBazaar Add... @item:inmenuBazaar Commit... @item:inmenuBazaar Delete @item:inmenuBazaar Log @item:inmenuBazaar Pull @item:inmenuBazaar Push @item:inmenuBazaar Update @item:inmenuShow Local Bazaar Changes Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2014-03-01 02:43+0900
Last-Translator: Shinjo Park <kde@peremen.name>
Language-Team: Korean <kde@peremen.name>
Language: ko
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 Bazaar 저장소에 파일 추가됨. Bazaar 저장소에 파일 추가 중... Bazaar 저장소에 파일을 추가할 수 없습니다. Bazaar 기록 닫힘. Bazaar 변경 사항을 커밋할 수 없습니다. Bazaar 변경 사항 커밋됨. Bazaar 변경 사항 커밋 중... Bazaar 저장소를 가져올 수 없습니다. Bazaar 저장소 가져옴. Bazaar 저장소를 가져오고 있습니다... Bazaar 저장소를 내보낼 수 없습니다. Bazaar 저장소 내보냄. Bazaar 저장소를 내보내는 중... Bazaar 저장소에서 파일 지워짐. Bazaar 저장소에서 파일 지우는 중... Bazaar 저장소에서 파일을 지울 수 없음. 변경 사항 검토가 실패했습니다. 변경 사항을 검토했습니다. 변경 사항 검토 중... Bazaar 기록을 실행할 수 없음. Bazaar 기록 실행 중... Bazaar 저장소를 업데이트할 수 없습니다. Bazaar 저장소를 업데이트했습니다. Bazaar 저장소 업데이트 중... Bazaar 추가... Bazaar 커밋... Bazaar 삭제 Bazaar 로그 Bazaar 풀 Bazaar 푸시 Bazaar 업데이트 로컬 Bazaar 변경 사항 보기 