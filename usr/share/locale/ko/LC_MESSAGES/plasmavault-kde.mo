Þ    ?        Y         p  Ø  q  }  J  M  È        
   7     B     K     j       <        Í     å  	   õ     ÿ  .     %   A     g     }                     ·     Ê     Ñ     ï     õ  4        B  9   T          ¡     ³  å   »  !   ¡  t   Ã     8     D     a     n     s  	          -   ¡     Ï  B   Ø  &     ?   B  '     )   ª  7   Ô  .     5   ;  +   q          º     Ú  ,   é          -  9   :  V   t  C   Ë      Ø      w  {       "     ¤"  
   «"  (   ¶"  5   ß"     #  E   #     b#     {#     #     #  8   ¯#  .   è#     $     5$     <$     [$     a$     |$     $  "   ¨$     Ë$     Ñ$  ?   ï$     /%  M   D%     %     ¥%     ¶%    ½%     Ã&  ¡   ß&     '  *   '     ½'     Ï'     Ö'     ñ'     ù'  8   (     G(  W   N(  +   ¦(  C   Ò(  ;   )  >   R)  J   )  7   Ü)  L   *  B   a*     ¤*     Ã*     â*  <   ø*     5+     M+  R   g+  \   º+     ,     !      	   2   ?   
   /                 +   (   *       1            )   >           8          9   $              7             .       '   6          %   <             3   ,          ;       =   &                              -           #      "          :      5      0                                   4               <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body style=" font-family:'hlv'; font-size:9pt; font-weight:400; font-style:normal;">
<p style="-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><br /></p></body></html> <b>Security notice:</b>
                             According to a security audit by Taylor Hornby (Defuse Security),
                             the current implementation of Encfs is vulnerable or potentially vulnerable
                             to multiple types of attacks.
                             For example, an attacker with read/write access
                             to encrypted data might lower the decryption complexity
                             for subsequently encrypted data without this being noticed by a legitimate user,
                             or might use timing analysis to deduce information.
                             <br /><br />
                             This means that you should not synchronize
                             the encrypted data to a cloud storage service,
                             or use it in other circumstances where the attacker
                             can frequently access the encrypted data.
                             <br /><br />
                             See <a href='http://defuse.ca/audits/encfs.htm'>defuse.ca/audits/encfs.htm</a> for more information. <b>Security notice:</b>
                             CryFS encrypts your files, so you can safely store them anywhere.
                             It works well together with cloud services like Dropbox, iCloud, OneDrive and others.
                             <br /><br />
                             Unlike some other file-system overlay solutions,
                             it does not expose the directory structure,
                             the number of files nor the file sizes
                             through the encrypted data format.
                             <br /><br />
                             One important thing to note is that,
                             while CryFS is considered safe,
                             there is no independent security audit
                             which confirms this. @title:windowCreate a New Vault Activities Backend: Can not create the mount point Can not open an unknown vault. Change Choose the encryption system you want to use for this vault: Choose the used cypher: Close the vault Configure Configure Vault... Configured backend can not be instantiated: %1 Configured backend does not exist: %1 Correct version found Create Create a New Vault... CryFS Device is already open Device is not open Dialog Do not show this notice again EncFS Encrypted data location Failed to create directories, check your permissions Failed to execute Failed to fetch the list of applications using this vault Failed to open: %1 Forcefully close  General If you limit this vault only to certain activities, it will be shown in the applet only when you are in those activities. Furthermore, when you switch to an activity it should not be available in, it will automatically be closed. Limit to the selected activities: Mind that there is no way to recover a forgotten password. If you forget the password, your data is as good as gone. Mount point Mount point is not specified Mount point: Next Open with File Manager Password: Plasma Vault Please enter the password to open this vault: Previous The mount point directory is not empty, refusing to open the vault The specified backend is not available The vault configuration can only be changed while it is closed. The vault is unknown, can not close it. The vault is unknown, can not destroy it. This device is already registered. Can not recreate it. This directory already contains encrypted data Unable to close the vault, an application is using it Unable to close the vault, it is used by %1 Unable to detect the version Unable to perform the operation Unknown device Unknown error, unable to create the backend. Use the default cipher Vaul&t name: Wrong version installed. The required version is %1.%2.%3 You need to select empty directories for the encrypted storage and for the mount point formatting the message for a command, as in encfs: not found%1: %2 Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-11 05:53+0100
PO-Revision-Date: 2017-12-10 20:42+0100
Last-Translator: Shinjo Park <kde@peremen.name>
Language-Team: Korean <kde@peremen.name>
Language: ko
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body style=" font-family:'hlv'; font-size:9pt; font-weight:400; font-style:normal;">
<p style="-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><br /></p></body></html> <b>ë³´ì ê²½ê³ :</b>
                             Defuse Securityì Taylor Hornbyì ìí ë³´ì ê°ì¬ì ìíë©´
                       íì¬ ì¬ì© ì¤ì¸ EncFSë ì¬ë¬ ì¢ë¥ì ê³µê²©ì ì·¨ì½íê±°ë ì ì¬ì ì¼ë¡ ì·¨ì½í 
                       ì ììµëë¤.
                       ìë¥¼ ë¤ì´ ìí¸íë ë°ì´í°ì ì½ê¸°/ì°ê¸° ê¶íì ê°ì§ê³  ìë ê³µê²©ìê°
                       ì ìì ì¸ ì¬ì©ììê² ë°ê²¬ëì§ ìì ìíë¡ ì°¨í ì¶ê°ë ë°ì´í°ì
                       ìí¸í ê°ëë¥¼ ë®ì¶ê±°ë, ìê° ë¶ì±ë ê³µê²©ì íµí´ì
                       ì ë³´ë¥¼ ì ì¶í  ì ììµëë¤.
                             <br /><br />
                       ìí¸íë ë°ì´í°ë¥¼ í´ë¼ì°ë ì ì¥ìì ëê¸°ííê±°ë,
                       ê³µê²©ìê° ìí¸íë ë°ì´í°ì ì ê·¼í  ì ìë ìí©ìì
                       ì¬ì©íë ê²ì ì¶ì²íì§ ììµëë¤.                             <br /><br />
                       ìì¸í ì ë³´ë¥¼ ë³´ë ¤ë©´ <a href='http://defuse.ca/audits/encfs.htm'>defuse.ca/audits/encfs.htm</a> ì¬ì´í¸ë¥¼ ì°¸ì¡°íì­ìì¤. <b>ë³´ì ê²½ê³ :</b>
                             CryFSë íì¼ì ìí¸ííë¯ë¡ ë°ì´í°ë¥¼ ë¤ë¥¸ ê³³ì ì ì¥í  ì ììµëë¤.
                             Dropbox, iCloud, OneDrive ë± í´ë¼ì°ë ìë¹ì¤ì ë°ì´í°ë¥¼ ì ì¥í´ë ë©ëë¤.
                             <br /><br />
                       íì¼ ìì¤í ì¤ë²ë ì´ ê¸°ë° ìë£¨ìê³¼ë ë¬ë¦¬
                       ëë í°ë¦¬ êµ¬ì¡°, ìí¸íë íì¼ ê°ì ë°
                       íì¼ì í¬ê¸°ë¥¼ ìí¸íë ë°ì´í° íìì íµí´ì
                       ë¸ì¶ìí¤ì§ ììµëë¤.
                             <br /><br />
                             CryFSê° ìì í ê²ì¼ë¡ ê°ì£¼ëë¤ê³  íëë¼ë
                       ëë¦½ì ì¸ ë³´ì ê°ì¬ë¥¼ íµí´ì ìì íë¤ë ì¬ì¤ì´
                       ìì§ê¹ì§ë íì¸ëì§ ìììµëë¤. ì ë¹ë° ê³µê° ë§ë¤ê¸° íë ë°±ìë: ë§ì´í¸ ì§ì ì ìì±í  ì ìì ì ì ìë ë¹ë° ê³µê°ì ì´ ì ììµëë¤. ë³ê²½ ë¹ë° ê³µê°ì ì¬ì©í  ìí¸í ìì¤íì ì ííì­ìì¤: ë¤ì ìí¸í ì¬ì©: ë¹ë° ê³µê° ë«ê¸° ì¤ì  ë¹ë° ê³µê° ì¤ì ... ì¤ì í ë°±ìëë¥¼ ì¸ì¤í´ì¤íí  ì ìì: %1 ì¤ì í ë°±ìëê° ì¡´ì¬íì§ ìì: %1 ì¬ë°ë¥¸ ë²ì ì ì°¾ìì ìì± ì ë¹ë° ê³µê° ë§ë¤ê¸°... CryFS ì¥ì¹ê° ì´ë¯¸ ì´ë ¸ì ì¥ì¹ê° ì´ë¦¬ì§ ììì ëí ìì ì´ ë©ìì§ ë¤ì ë³´ì§ ìê¸° EncFS ìí¸íë ë°ì´í° ìì¹ ëë í°ë¦¬ë¥¼ ë§ë¤ ì ìì, ê¶íì íì¸íì­ìì¤ ì¤íí  ì ìì ì´ ë¹ë° ê³µê°ì ì¬ì©íë íë¡ê·¸ë¨ ëª©ë¡ì ê°ì ¸ì¬ ì ìì ì´ ì ìì: %1 ê°ì ë¡ ë«ê¸° ì¼ë° ì´ ë¹ë° ê³µê°ì í¹ì  íëì¼ë¡ ì ííë©´ í´ë¹ íëì´ íì±íëì´ ìì ëìë§ ë¹ë° ê³µê°ì íìí©ëë¤. í´ë¹ ë¹ë° ê³µê°ì´ íì±íëì´ ìì§ ìì íëì¼ë¡ ì ííë©´ ë¹ë° ê³µê°ì ìëì¼ë¡ ë«ìµëë¤. ë¤ì íëì¼ë¡ ì í: ë¹ë° ê³µê°ì ìí¸ë¥¼ ìì´ë²ë¦¬ë©´ ë³µêµ¬í  ì ìë ë°©ë²ì´ ììµëë¤. ìí¸ë¥¼ ìì´ë²ë¦¬ë©´ í´ë¹ ë°ì´í°ì ì ê·¼í  ì ììµëë¤. ë§ì´í¸ ì§ì  ë§ì´í¸ ì§ì ì´ ì§ì ëì§ ììì ë§ì´í¸ ì§ì : ë¤ì íì¼ ê´ë¦¬ìë¡ ì´ê¸° ìí¸: Plasma ë¹ë° ê³µê° ë¹ë° ê³µê°ì ì´ë ¤ë©´ ìí¸ë¥¼ ìë ¥íì­ìì¤: ì´ì  ë§ì´í¸ ì§ì  ëë í°ë¦¬ê° ë¹ì´ ìì§ ììì ë¹ë° ê³µê°ì ì´ì§ ìì ì§ì í ë°±ìëë¥¼ ì¬ì©í  ì ìì ë¹ë° ê³µê° ì¤ì ì ë³ê²½íë ¤ë©´ ë¨¼ì  ë«ìì¼ í©ëë¤. ë¹ë° ê³µê°ì ì ì ìì´ì ë«ì ì ììµëë¤. ë¹ë° ê³µê°ì ì ì ìì´ì ì­ì í  ì ììµëë¤. ì¥ì¹ê° ì´ë¯¸ ë±ë¡ëììµëë¤. ë¤ì ìì±í  ì ììµëë¤. ëë í°ë¦¬ì ì´ë¯¸ ìí¸íë ë°ì´í°ê° ìì íë¡ê·¸ë¨ìì ë¹ë° ê³µê°ì ì¬ì©íê³  ìì´ì ë«ì ì ìì %1ìì ë¹ë° ê³µê°ì ì¬ì©íê³  ìì´ì ë«ì ì ìì ë²ì ì íì¸í  ì ìì ììì ìíí  ì ìì ì ì ìë ì¥ì¹ ì ì ìë ì¤ë¥, ë°±ìëë¥¼ ë§ë¤ ì ììµëë¤. ê¸°ë³¸ ìí¸í ì¬ì© ë¹ë° ê³µê° ì´ë¦(&T): ìëª»ë ë²ì ì´ ì¤ì¹ëììµëë¤. ë²ì  %1.%2.%3ì´(ê°) íìí©ëë¤ ìí¸íë ì ì¥ìì ë§ì´í¸ ì§ì ì ì¬ì©í  ëë í°ë¦¬ë ë¹ì´ ìì´ì¼ í¨ %1: %2 