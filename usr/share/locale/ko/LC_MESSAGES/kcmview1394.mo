ή          ¬      <      °  H  ±     ϊ     ώ               
     "     &     *     0     5  	   =     G     J     W     ]     e    l  Κ  ρ     Ό
     ΐ
     Γ
     Η
  ,   Μ
     ω
     ύ
            	             -     0     ?     F     U                                                  
                                           	        <qt>Here you can see some information about your IEEE 1394 configuration. The meaning of the columns:<ul><li><b>Name</b>: port or node name, the number can change with each bus reset</li><li><b>GUID</b>: the 64 bit GUID of the node</li><li><b>Local</b>: checked if the node is an IEEE 1394 port of your computer</li><li><b>IRM</b>: checked if the node is isochronous resource manager capable</li><li><b>CRM</b>: checked if the node is cycle master capable</li><li><b>ISO</b>: checked if the node supports isochronous transfers</li><li><b>BM</b>: checked if the node is bus manager capable</li><li><b>PM</b>: checked if the node is power management capable</li><li><b>Acc</b>: the cycle clock accuracy of the node, valid from 0 to 100</li><li><b>Speed</b>: the speed of the node</li><li><b>Vendor</b>: the vendor of the device</li></ul></qt> Acc BM CRM GUID Generate 1394 Bus Reset IRM ISO Local Name Node %1 Not ready PM Port %1:"%2" Speed Unknown Vendor Project-Id-Version: kcmview1394
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-31 02:49+0200
PO-Revision-Date: 2009-12-06 18:07+0900
Last-Translator: Shinjo Park <kde@peremen.name>
Language-Team: Korean <cho.sungjae@gmail.com>
Language: ko
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 <qt>μ¬κΈ°μμ IEEE 1394 μ€μ  μ λ³΄λ₯Ό λ³Ό μ μμ΅λλ€. κ°κ° μ΄μ μλ―Έλ λ€μκ³Ό κ°μ΅λλ€.<ul><li><b>μ΄λ¦</b>: ν¬νΈλ λΈλ μ΄λ¦. λ²μ€κ° μ΄κΈ°νλ  λλ§λ€ λ°λ μ μμ΅λλ€.</li><li><b>GUID</b>: λΈλμ 64λΉνΈ GUIDμλλ€.</li><li><b>λ‘μ»¬</b>: λΈλκ° μ΄ μ»΄ν¨ν°μ IEEE 1394 ν¬νΈμ΄λ©΄ μ νλ©λλ€.</li><li><b>IRM</b>: λΈλμμ λ±μμ± μμ κ΄λ¦¬μλ₯Ό μ¬μ©ν  μ μμΌλ©΄ μ νλ©λλ€.</li><li><b>ISO</b>: λΈλμμ λκΈ°νλ μ μ‘μ μ¬μ©ν  μ μμΌλ©΄ μ νλ©λλ€.</li><li><b>BM</b>: λΈλμμ λ²μ€ κ΄λ¦¬μλ₯Ό μ¬μ©ν  μ μμΌλ©΄ μ νλ©λλ€.</li><li><b>PM</b>: λΈλμμ μ μ κ΄λ¦¬μλ₯Ό μ¬μ©ν  μ μμΌλ©΄ μ νλ©λλ€.</li><li><br /><b>Acc</b>: 0μμ 100 μ¬μ΄μ μλ λΈλμ μ¬μ΄ν΄ ν΄λ­ μ νλμλλ€.</li><li><b>μλ</b>: λΈλμ μλμλλ€.</li><li><b>μ μ‘°μ¬</b>: μ₯μΉ μ μ‘°μ¬μλλ€.</li></ul></qt> Acc BM CRM GUID 1394 λ²μ€ μ΄κΈ°ν μ νΈ λ°μμν€κΈ° IRM ISO λ‘μ»¬ μ΄λ¦ λΈλ %1 μ€λΉλμ§ μμ PM ν¬νΈ %1:"%2" μλ μ μ μμ λ²€λ 