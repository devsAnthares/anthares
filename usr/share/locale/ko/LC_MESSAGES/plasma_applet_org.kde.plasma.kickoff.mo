��            )   �      �     �  
   �  /   �  -   �          !  
   2     =     J     `  W   i     �  ,   �  	                  !     '     -  
   :     E     W     o     �     �     �     �  1   �       �       �  
   �  	   �     �     �     �     �            	   1  �   ;     �  ,   �          ,     :  
   H  	   S     ]     k     |     �     �     �     �     �     	     !	     8	                                                                                  
                                	                       %1@%2 %2@%3 (%1) @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Favorites All Applications Appearance Applications Applications updated. Computer Drag tabs between the boxes to show/hide them, or reorder the visible tabs by dragging. Edit Applications... Expand search to bookmarks, files and emails Favorites Hidden Tabs History Icon: Leave Menu Buttons Often Used On All Activities On The Current Activity Remove from Favorites Show In Favorites Show applications by name Sort alphabetically Switch tabs on hover Type is a verb here, not a nounType to search... Visible Tabs Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2017-12-02 23:39+0100
Last-Translator: Shinjo Park <kde@peremen.name>
Language-Team: Korean <kde@peremen.name>
Language: ko
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 %1@%2 %2@%3 (%1) 선택... 아이콘 초기화 책갈피에 추가 모든 프로그램 모양 프로그램 프로그램 업데이트됨. 컴퓨터 상자에서 상자로 탭을 드래그해서 표시 및 숨김 상태를 변경하거나, 표시되는 탭의 순서를 조정하려면 드래그하십시오. 프로그램 편집... 책갈피, 파일, 이메일에서도 찾기 즐겨찾기 숨겨진 탭 과거 기록 아이콘: 떠나기 메뉴 단추 자주 사용됨 모든 활동에 현재 활동에만 책갈피에서 삭제 책갈피에 표시 프로그램 이름 순 정렬 가나다순으로 정렬 지나다닐 때 탭 전환 입력하여 찾기... 표시되는 탭 