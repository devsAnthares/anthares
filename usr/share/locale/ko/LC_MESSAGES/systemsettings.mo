��    8      �  O   �      �  A   �          2  /   I  8   y     �     �     �     �     �                      $   ,  	   Q     [  !   q  3   �  	   �     �      �  $   �       *   /     Z  	   _  5   i     �     �  
   �     �     �  	   �          !     @  5   O  3   �  6   �     �  ,   �  /   )	  	   Y	     c	     z	     �	     �	  O   �	  Z   �	  b   J
  	   �
  
   �
  P   �
       �  #  G   �               6     Q  	   l     v     �     �     �     �  	   �     �     �  "        '     .     ?  1   S  	   �     �     �  '   �     �     �  	   �     �     
  '   *     R  	   j     t     �     �  '   �     �     �  >   �  >   -  <   l     �  B   �     �  	     $        3     @     T  _   e  _   �  ~   %     �     �  ^   �     "                                            ,   	          #       )   $                                         +   (   &   1            7   4                         6           8   
   *   .             /       %      3   !           0   2      "            -       '           5        %1 is an external application and has been automatically launched (c) 2009, Ben Cooksley (c) 2017, Marco Martin <i>Contains 1 item</i> <i>Contains %1 items</i> A search yelded no resultsNo items matching your search About %1 About Active Module About Active View About System Settings All Settings Apply Settings Author Back Ben Cooksley Central configuration center by KDE. Configure Configure your system Contains 1 item Contains %1 items Determines whether detailed tooltips should be used Developer Dialog EMAIL OF TRANSLATORSYour emails Expand the first level automatically Frequently used: General config for System SettingsGeneral Help Icon View Internal module representation, internal module model Internal name for the view used Keyboard Shortcut: %1 Maintainer Marco Martin Mathias Soeken Most Used Most used module number %1 NAME OF TRANSLATORSYour names No views found Provides a categorized icons view of control modules. Provides a categorized sidebar for control modules. Provides a classic tree-based view of control modules. Relaunch %1 Reset all current changes to previous values Search through a list of control modulesSearch Search... Show detailed tooltips Sidebar Sidebar View System Settings System Settings was unable to find any views, and hence has nothing to display. System Settings was unable to find any views, and hence nothing is available to configure. The settings of the current module have changed.
Do you want to apply the changes or discard them? Tree View View Style Welcome to "System Settings", a central place to configure your computer system. Will Stephenson Project-Id-Version: systemsettings
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:08+0100
PO-Revision-Date: 2017-12-03 00:05+0100
Last-Translator: Shinjo Park <kde@peremen.name>
Language-Team: Korean <kde@peremen.name>
Language: ko
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 %1은(는) 외부 프로그램이며 자동으로 실행되었습니다 (c) 2009, Ben Cooksley (c) 2017, Marco Martin <i>항목 %1개 포함</i> 일치하는 항목 없음 %1 정보 활성화된 모듈 정보 활성화된 보기 정보 시스템 설정 정보 모든 설정 설정 적용하기 작성자 뒤로 Ben Cooksley KDE 중앙 제어 센터입니다. 설정 시스템 설정 항목 %1개 포함 자세한 정보를 사용할 지 여부입니다 개발자 대화 상자 kde@peremen.name 첫 단계를 자동으로 확장하기 자주 사용함: 일반 도움말 아이콘 보기 내부 모듈 표현 및 모델 보기에서 사용하는 내부 이름 키보드 단축키: %1 관리자 Marco Martin Mathias Soeken 자주 사용됨 가장 자주 사용되는 모듈 %1위 Shinjo Park 보기 찾을 수 없음 제어 모듈을 분류별로 아이콘으로 표시합니다. 제어 모듈을 분류별로 사이드바에 표시합니다. 제어 모듈을 고전 스타일 트리로 표시합니다. %1 다시 실행 모든 현재 변경 사항을 이전 값으로 초기화합니다 찾기 찾기... 자세한 풍선 도움말 보이기 사이드바 사이드바 보기 시스템 설정 시스템 설정에서 보기를 찾을 수 없으며, 아무것도 보여줄 수 없습니다. 시스템 설정에서 보기를 찾을 수 없으며, 아무것도 설정할 수 없습니다. 현재 모듈에 저장되지 않은 변경 사항이 있습니다.
변경 사항을 적용하거나 무시하시겠습니까? 트리 보기 보기 스타일 "시스템 설정"에서는 여러분의 시스템을 한 곳에서 설정할 수 있습니다 Will Stephenson 