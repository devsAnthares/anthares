��          �   %   �      p     q  +   �  .   �  6   �  *     #   D  &   h  /   �  2   �  :   �  0   -  3   ^  ;   �  K   �  -     $   H  '   m     �     �     �     �     �  #        1     O     c     |  �  �       /     .   O  ?   ~  9   �     �     	  .   5	  +   d	  ?   �	  .   �	  .   �	  ?   .
  l   n
  ;   �
       !   6  
   X     c  
   q     |     �  !   �     �     �  
   �     �                                                    	   
                                                                    @action:buttonCommit @info:statusAdded files to SVN repository. @info:statusAdding files to SVN repository... @info:statusAdding of files to SVN repository failed. @info:statusCommit of SVN changes failed. @info:statusCommitted SVN changes. @info:statusCommitting SVN changes... @info:statusRemoved files from SVN repository. @info:statusRemoving files from SVN repository... @info:statusRemoving of files from SVN repository failed. @info:statusReverted files from SVN repository. @info:statusReverting files from SVN repository... @info:statusReverting of files from SVN repository failed. @info:statusSVN status update failed. Disabling Option "Show SVN Updates". @info:statusUpdate of SVN repository failed. @info:statusUpdated SVN repository. @info:statusUpdating SVN repository... @item:inmenuSVN Add @item:inmenuSVN Commit... @item:inmenuSVN Delete @item:inmenuSVN Revert @item:inmenuSVN Update @item:inmenuShow Local SVN Changes @item:inmenuShow SVN Updates @labelDescription: @title:windowSVN Commit Show updates Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2017-01-22 20:41+0100
Last-Translator: Shinjo Park <kde@peremen.name>
Language-Team: Korean <kde@peremen.name>
Language: ko
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 커밋 SVN 저장소에 파일을 추가 했습니다. SVN 저장소에 파일을 추가하는 중... SVN 저장소에 파일을 추가하는 데 실패했습니다. SVN 변경 사항을 커밋하는 데 실패했습니다. SVN 변경 사항 커밋됨. SVN 변경 사항 커밋 중... SVN 저장소에서 파일을 지웠습니다. SVN 저장소에서 파일 지우는 중... SVN 저장소에서 파일을 지우는 데 실패했습니다. SVN 저장소의 파일로 되돌렸습니다. SVN 저장소의 파일로 되돌리는 중... SVN 저장소의 파일로 되돌리는 데 실패했습니다. SVN 상태 업데이트에 실패했습니다. "SVN 업데이트 보기" 옵션을 비활성화하십시오. SVN 저장소를 업데이트하는 데 실패했습니다. SVN 저장소 업데이트됨. SVN 저장소 업데이트 중... SVN 추가 SVN 커밋... SVN 삭제 SVN 되돌리기 SVN 업데이트 로컬 SVN 변경사항 보이기 SVN 업데이트 보이기 설명: SVN 커밋 업데이트 보이기 