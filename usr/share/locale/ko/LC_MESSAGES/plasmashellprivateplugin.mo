��            )   �      �     �     �     �     �     �     �            !   $     F  !   [     }     �     �     �     �     �     �     �     �     �  2     @   >  	     4   �  8   �  $   �          *  �  .  
   �     �     �  $   �       	   "  %   ,     R  *   Y     �  9   �     �  
   �     �     �  
              %     F     a     �  F   �  =   �  	   	     &	     +	  :   2	     m	     {	     
                                                             	                                                                       &Execute All Widgets Categories: Desktop Shell Scripting Console Download New Plasma Widgets Editor Executing script at %1 Filters Install Widget From Local File... Installation Failure Installing the package %1 failed. Load New Session Open Script File Output Running Runtime: %1ms Save Script File Screen lock enabled Screen saver timeout Select Plasmoid File Sets the minutes after which the screen is locked. Sets whether the screen will be locked after the specified time. Templates Toolbar Button to switch to KWin Scripting ModeKWin Toolbar Button to switch to Plasma Scripting ModePlasma Unable to load script file <b>%1</b> Uninstallable Use Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-23 03:23+0100
PO-Revision-Date: 2016-10-20 22:57+0100
Last-Translator: Shinjo Park <kde@peremen.name>
Language-Team: Korean <kde@peremen.name>
Language: ko
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 실행(&E) 모든 위젯 분류: 데스크톱 셸 스크립팅 콘솔 새 Plasma 위젯 다운로드 편집기 스크립트를 %1에서 실행하기 필터 로컬 파일에서 위젯 설치하기... 설치 실패 패키지 %1을(를) 설치하는 데 실패했습니다. 불러오기 새 세션 스크립트 파일 열기 출력 실행 중 실행 시간: %1ms 스크립트 파일 저장하기 화면 잠금 활성화됨 화면 보호기 시간 제한 Plasmoid 파일 선택 화면 보호기를 시작할 시간을 분 단위로 설정합니다. 지정한 시간 이후 화면을 잠글 지 설정합니다. 템플릿 KWin Plasma 스크립트 파일 <b>%1</b>을(를) 불러올 수 없음 삭제 가능 사용하기 