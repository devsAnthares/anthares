��    =        S   �      8  ;   9     u     �     �     �  6   �  A   
     L     U  $   Y  
   ~     �  #   �     �     �     �     �     �  7        >     [     w  '   �     �     �  $   �  ,   �          3     C     K     ]     y     �     �     �     �     �  �   �  9   �	  "   �	     �	     �	     
     *
     <
     R
     c
  %   u
     �
     �
     �
     �
     �
  
   �
  7        :     T     l     t  �  �  P     (   e     �     �  #   �     �     �            0        M  "   T  (   w     �     �     �     �  	   �  =   �  $     !   A     c  0   k     �     �  6   �  2   �     (     5     E     L     g     �     �     �     �     �     �  �   �  8   �  (        7  '   ?     g     |     �     �     �  /   �     	  !     %   8     ^     c  
   g  #   r     �     �     �  
   �     !          3      0      (                 1      "          =   .   6                                         	      :          5       ,   #   &   7       ;   '             -   
             8                 *           %           <   4   9                )   /   $          2             +       

Choose the previous strip to go to the last cached strip. &Create Comic Book Archive... &Save Comic As... &Strip Number: *.cbz|Comic Book Archive (Zip) @option:check Context menu of comic image&Actual Size @option:check Context menu of comic imageStore current &Position Advanced All An error happened for identifier %1. Appearance Archiving comic failed Automatically update comic plugins: Cache Check for new comic strips: Comic Comic cache: Configure... Could not create the archive at the specified location. Create %1 Comic Book Archive Creating Comic Book Archive Destination: Display error when getting comic failed Download Comics Error Handling Failed adding a file to the archive. Failed creating the file with identifier %1. From beginning to ... From end to ... General Get New Comics... Getting comic strip failed: Go to Strip Information Jump to &current Strip Jump to &first Strip Jump to Strip ... Manual range Maybe there is no Internet connection.
Maybe the comic plugin is broken.
Another reason might be that there is no comic for this day/number/string, so choosing a different one might work. Middle-click on the comic to show it at its original size No zip file is existing, aborting. Range: Show arrows only on mouse over Show comic URL Show comic author Show comic identifier Show comic title Strip identifier: The range of comic strips to archive. Update Visit the comic website Visit the shop &website an abbreviation for Number# %1 days dd.MM.yyyy here strip means comic strip&Next Tab with a new Strip in a range: from toFrom: in a range: from toTo: minutes strips per comic Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-07-18 03:20+0200
PO-Revision-Date: 2015-04-26 16:31+0200
Last-Translator: Shinjo Park <kde@peremen.name>
Language-Team: Korean <kde@peremen.name>
Language: ko
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 1.5
 

이전 화를 누르면 마지막으로 캐시된 화를 볼 수 있습니다. 만화책 압축 파일 만들기(&C)... 만화 저장하기(&S)... 화 번호(&S): *.cbz|만화책 압축 파일 (Zip) 실제 크기(&A) 현재 위치 저장하기(&P) 고급 모두 식별자 %1에서 오류가 발생했습니다. 모양 압축 파일을 만들 수 없음 만화 플러그인 자동 업데이트: 캐시 새 화 자동 확인: 만화 만화 캐시: 설정... 지정한 위치에 압축 파일을 만들 수 없습니다. %1 만화책 압축 파일 만들기 만화책 압축 파일 만들기 대상: 만화를 가져올 수 없을 때 오류 표시 만화 다운로드 오류 처리 압축 파일에 파일을 추가할 수 없습니다. 식별자 %1인 파일을 만들 수 없습니다. 처음부터 끝에서부터 일반 새 만화 가져오기... 만화를 가져올 수 없음: 화로 가기 정보 현재 화로 가기(&J) 첫 화로 가기(&J) 다음 화로 가기... 수동 범위 인터넷에 연결할 수 없을 수도 있습니다.
만화 플러그인이 깨졌을 수도 있습니다.
오늘/입력한 숫자/문자열에 대한 만화가 없을 수도 있으므로 다른 값을 입력해 보십시오. 가운데 단추를 누르면 원래 크기로 보이기 zip 파일이 없어서 중단합니다. 범위: 지나다닐 때만 화살표 보이기 만화 URL 보이기 만화 작성자 보이기 만화 식별자 보이기 만화 제목 보이기 화 식별자: 압축 파일로 저장할 화 범위입니다. 업데이트 만화 웹 사이트 방문하기 상점 웹 사이트 방문하기(&W) # %1 일 yyyy.MM.dd 다음 탭에 새 화 만들기(&N) 시작: 끝: 분 편당 화 