ή          ¬      <      °     ±     Ζ     Ψ  7  λ  Π  #  +   τ  ^                   €  x   ¬  Τ   %     ϊ               ;     U    r     χ     	     /	  @  D	  π  
  (   v  z             8     P     W  !  Ψ  %   ϊ        .   1     `  !                             
       	                                                                    &End current session &Restart computer &Turn off computer <h1>Session Manager</h1> You can configure the session manager here. This includes options such as whether or not the session exit (logout) should be confirmed, whether the session should be restored again when logging in and whether the computer should be automatically shut down after session exit by default. <ul>
<li><b>Restore previous session:</b> Will save all applications running on exit and restore them when they next start up</li>
<li><b>Restore manually saved session: </b> Allows the session to be saved at any time via "Save Session" in the K-Menu. This means the currently started applications will reappear when they next start up.</li>
<li><b>Start with an empty session:</b> Do not save anything. Will come up with an empty desktop on next start.</li>
</ul> Applications to be e&xcluded from sessions: Check this option if you want the session manager to display a logout confirmation dialog box. Conf&irm logout Default Leave Option General Here you can choose what should happen by default when you log out. This only has meaning, if you logged in through KDM. Here you can enter a colon or comma separated list of applications that should not be saved in sessions, and therefore will not be started when restoring a session. For example 'xterm:konsole' or 'xterm,konsole'. O&ffer shutdown options On Login Restore &manually saved session Restore &previous session Start with an empty &session Project-Id-Version: kcmsmserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2009-03-25 02:47+0900
Last-Translator: Shinjo Park <kde@peremen.name>
Language-Team: Korean <cho.sungjae@gmail.com>
Language: ko
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 νμ¬ μΈμ λλ΄κΈ°(&E) μ»΄ν¨ν° λ€μ μμ(&R) μ»΄ν¨ν° λκΈ°(&T) <h1>μΈμ κ΄λ¦¬μ</h1> μ΄ κ³³μμ μΈμ κ΄λ¦¬μλ₯Ό μ€μ ν  μ μμ΅λλ€. μ΄ λͺ¨λμμλ μΈμμμ λ‘κ·Έμμν  λ νμΈμ λ°μμ§, λ€μμ λ‘κ·ΈμΈν  λ μ μ₯ν μΈμμ λΆλ¬μ¬μ§, μΈμμ μ’λ£ν λ€μ κΈ°λ³Έμ μΌλ‘ μμ€νμ μ’λ£ν  μ§ μ€μ ν  μ μμ΅λλ€. <ul>
<li><b>μ΄μ  μΈμ λ³΅μ:</b> νμ¬ μ€νλκ³  μλ λͺ¨λ  νλ‘κ·Έλ¨μ μ μ₯νκ³  λ€μ μμ μ μ€νλ©λλ€.</li>
<li><b>μλμΌλ‘ μ μ₯λ μΈμ λ³΅μ:</b> K λ©λ΄μ "μΈμ μ μ₯"μ ν΅ν΄μ μ΄λ λλ μΈμμ μ μ₯ν  μ μλλ‘ ν©λλ€. μ΄ λ μ μ₯ν μΈμμ λ€μ μμ μ μ€νλ©λλ€.</li>
<li><b>λΉ μΈμμΌλ‘ μμ:</b> μλ¬΄κ²λ μ μ₯νμ§ μμ΅λλ€. λ€μ μμ μ λΉ μΈμμΌλ‘ μμλ©λλ€.</li>
</ul> μΈμμμ μ μΈν  νλ‘κ·Έλ¨(&X): λ§μ½ μΈμ κ΄λ¦¬μμμ λ‘κ·Έμμ νμΈ λνμμλ₯Ό μΆλ ₯νκ² νλ €λ©΄ μ΄ μ€μ μ μ ννμ­μμ€. λ‘κ·Έμμ νμΈνκΈ°(&I) κΈ°λ³Έ λ λκΈ° μ΅μ μΌλ° μ΄ κ³³μμ λ‘κ·Έμμν  λ μΌμ΄λ  μΌμ μ€μ ν  μ μμ΅λλ€. KDMμΌλ‘ λ‘κ·ΈμΈνμ λλ§ μ μ©λ©λλ€. μ΄ κ³³μ μΈμμμ μ μΈν  νλ‘κ·Έλ¨μ λͺ©λ‘μ μ½λ‘ μ΄λ μΌνλ‘ κ΅¬λΆνμ¬ μλ ₯νμ­μμ€. β©μΈμμ λ€μ μμν  λ μ΄ νλ‘κ·Έλ¨μ λ€μ μμλμ§ μμ΅λλ€. μλ₯Ό λ€μ΄ 'xterm:konsole' β©λλ 'xterm,konsole'κ°μ΄ μλ ₯νλ©΄ λ©λλ€. μμ€ν μ’λ£ μ€μ  λ³΄μ΄κΈ°(&F) λ‘κ·ΈμΈν  λ μλμΌλ‘ μ μ₯λ μΈμ λ³΅μνκΈ°(&M) μ΄μ  μΈμ λ³΅μνκΈ°(&P) λΉ μΈμμΌλ‘ μμνκΈ°(&S) 