ή          μ   %   Ό      P     Q  $   l  *        Ό     Λ     ι               (  '   @     h  (        ©  &   Β     ι  ,     _   2  !     \   ΄  )     J   ;  c     >   κ  A   )     k    n  %     9   ,  4   f           Ή     Ϊ     υ      	  (   #	  8   L	  %   	  8   «	  &   δ	  #   
  !   /
  9   Q
  x   
  '     }   ,  3   ͺ  C   ή  j   "  M     O   Ϋ     +                                                                	                                                         
    

KDE is unable to start.
 $HOME directory (%1) does not exist. $HOME directory (%1) is out of disk space. $HOME not set! Also allow remote connections Halt Without Confirmation Log Out Log Out Without Confirmation Logout canceled by '%1' No read access to $HOME directory (%1). No read access to '%1'. No write access to $HOME directory (%1). No write access to '%1'. Plasma Workspace installation problem! Reboot Without Confirmation Restores the saved user session if available Starts <wm> in case no other window manager is 
participating in the session. Default is 'kwin' Starts the session in locked mode Starts without lock screen support. Only needed if other component provides the lock screen. Temp directory (%1) is out of disk space. The following installation problem was detected
while trying to start KDE: The reliable KDE session manager that talks the standard X11R6 
session management protocol (XSMP). Writing to the $HOME directory (%2) failed with the error '%1' Writing to the temp directory (%2) failed with
    the error '%1' wm Project-Id-Version: ksmserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-05 03:18+0100
PO-Revision-Date: 2016-01-24 22:44+0100
Last-Translator: Shinjo Park <kde@peremen.name>
Language-Team: Korean <kde@peremen.name>
Language: ko
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 

KDEλ₯Ό μμν  μ μμ΅λλ€.
 $HOME λλ ν°λ¦¬(%1)μ΄(κ°) μ‘΄μ¬νμ§ μμ΅λλ€. $HOME λλ ν°λ¦¬(%1)μ κ³΅κ°μ΄ λΆμ‘±ν©λλ€. $HOMEμ΄ μ€μ λμ§ μμ! μκ²© μ μμ νμ©ν©λλ€ νμΈνμ§ μκ³  λκΈ° λ‘κ·Έμμ νμΈνμ§ μκ³  λ‘κ·Έμμ '%1'μ μν΄μ λ‘κ·Έμμ μ·¨μλ¨ $HOME λλ ν°λ¦¬(%1)μ μ½κΈ° κΆνμ΄ μμ΅λλ€. '%1'μ μ½κΈ° κΆνμ΄ μμ΅λλ€ $HOME λλ ν°λ¦¬(%1)μ μ°κΈ° κΆνμ΄ μμ΅λλ€. '%1'μ μ°κΈ° κΆνμ΄ μμ΅λλ€. Plasma μμ κ³΅κ° μ€μΉ λ¬Έμ ! νμΈνμ§ μκ³  λ€μ μμ μ¬μ© κ°λ₯ν κ²½μ° μ μ₯λ μ¬μ©μ μΈμ λ³΅κ΅¬ μΈμμμ μ΄λ ν μ°½ κ΄λ¦¬μλ μμλμ§ μμμ λ
<wm>μ μμν©λλ€. κΈ°λ³Έκ°μ 'kwin'μλλ€ μΈμμ μ κ·Ό μνλ‘ μμνκΈ° μ κΈ νλ©΄ μ§μ μμ΄ μμν©λλ€. λ€λ₯Έ κ΅¬μ± μμμμ μ κΈ νλ©΄μ μ κ³΅ν  λμλ§ νμν©λλ€. Temp λλ ν°λ¦¬(%1)μ κ³΅κ°μ΄ λΆμ‘±ν©λλ€. KDEλ₯Ό μμνλ μ€ λ€μ μ€μΉ μ€λ₯λ₯Ό
λ°κ²¬νμ΅λλ€: λ―Ώμ μ μλ KDE μΈμ κ΄λ¦¬μλ νμ€ X11R6 μΈμ κ΄λ¦¬ νλ‘ν μ½
XSMPμ ν΅μ ν©λλ€. $HOME λλ ν°λ¦¬(%2)μ κΈ°λ‘νλ μ€ μ€λ₯κ° λ°μνμ΅λλ€: '%1' Temp λλ ν°λ¦¬(%2)μ κΈ°λ‘νλ μ€ μ€λ₯κ° λ°μνμ΅λλ€: 
  '%1' wm 