��          t      �         :        L     Y     g     {     �     �  2   �  @   �    "  �  $  G   �     �     	          2     @     _  F   t  =   �  M  �                      
                      	                Ensuring that the screen gets locked before going to sleep Lock Session Screen Locker Screen lock enabled Screen locked Screen saver timeout Screen unlocked Sets the minutes after which the screen is locked. Sets whether the screen will be locked after the specified time. The screen locker is broken and unlocking is not possible anymore.
In order to unlock switch to a virtual terminal (e.g. Ctrl+Alt+F2),
log in and execute the command:

loginctl unlock-session %1

Afterwards switch back to the running session (Ctrl+Alt+F%2). Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-03 03:06+0200
PO-Revision-Date: 2017-12-02 23:26+0100
Last-Translator: Shinjo Park <kde@peremen.name>
Language-Team: Korean <kde@peremen.name>
Language: ko
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 대기 모드로 진입하기 전에 화면이 잠길 수 있도록 함 세션 잠금 화면 잠금 화면 잠금 활성화됨 화면 잠김 화면 보호기 시간 제한 화면 잠금 풀림 화면 보호기를 시작할 시간을 분 단위로 설정합니다. 지정한 시간 이후 화면을 잠글 지 설정합니다. 화면 잠금 도구에 문제가 발생했으며 화면 잠금을 해제할 수 없습니다.
화면 잠금을 해제하려면 가상 터미널로 전환하여(예: Ctrl+Alt+F2)
로그인한 다음 이 명령을 실행하십시오:

loginctl unlock-session %1

그 다음 실행 중인 세션으로 돌아오십시오.(Ctrl+Alt+F%2) 