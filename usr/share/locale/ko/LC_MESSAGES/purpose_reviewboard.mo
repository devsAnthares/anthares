��          �      \      �     �     �  	   �  $   �          ,     C     ^     j  	   y  
   �     �     �     �     �     �  "   �  	   �  '   �  �  &     �     �     �  /   �  +     '   7  (   _     �     �     �  	   �  
   �     �     �     �     �  '   �     '  &   9                      
            	                                                                 / Authentication Base Dir: Could not create the new request:
%1 Could not get reviews list Could not set metadata Could not upload the patch Destination JSON error: %1 Password: Repository Repository: Request Error: %1 Server: Update Review: Update review User name in the specified service Username: Where this project was checked out from Project-Id-Version: kdevelop
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:19+0100
PO-Revision-Date: 2015-10-17 22:48+0200
Last-Translator: Shinjo Park <kde@peremen.name>
Language-Team: Korean <kde@peremen.name>
Language: ko
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=1; plural=0;
 / 인증 기본 디렉터리: 새로운 요청을 만들 수 없습니다.
%1 리뷰 목록을 가져올 수 없습니다 메타데이터를 설정할 수 없음 패치를 업로드할 수 없습니다. 대상 JSON 오류: %1 암호: 저장소 저장소: 요청 오류: %1 서버: 리뷰 업데이트: 리뷰 업데이트 지정한 서비스의 사용자 이름 사용자 이름: 프로젝트를 체크아웃한 위치 