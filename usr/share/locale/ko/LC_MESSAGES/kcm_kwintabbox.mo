��    "      ,  /   <      �  
   �               ,     >     J  !   V     x     �     �     �     �     �  L   �     #     +     J     Y     u     z     �     �     �     �  	   �     �     �     �  �   �  F   �     �     �     �  y  �     u     |     �     �  
   �     �     �     �     �     �               -  P   H  	   �  .   �     �  )   �     
	  	   	     	     3	     K	     R	  	   Y	     c	     {	     �	  �   �	  S   6
     �
     �
  	   �
                    !                                            	                                                           "               
                    Activities All other activities All other desktops All other screens All windows Alternative An example Desktop NameDesktop 1 Content Current activity Current application Current desktop Current screen Filter windows by Focus policy settings limit the functionality of navigating through windows. Forward Get New Window Switcher Layout Hidden windows Include "Show Desktop" icon Main Minimization Only one window per application Recently used Reverse Screens Shortcuts Show selected window Sort order: Stacking order The currently selected window will be highlighted by fading out all other windows. This option requires desktop effects to be active. The effect to replace the list window when desktop effects are active. Virtual desktops Visible windows Visualization Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2012-05-19 23:05+0900
Last-Translator: Shinjo Park <kde@peremen.name>
Language-Team: Korean <cho.sungjae@gmail.com>
Language: ko
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 활동 모든 다른 활동 다른 모든 데스크톱 모든 다른 화면 모든 창 보조 데스크톱 1 내용 현재 활동 현재 프로그램 현재 데스크톱 현재 화면 다음으로 창 필터링 초점 정책 설정은 창 사이를 옮겨다니는 행동을 제한합니다. 앞으로 다른 창 전환기 레이아웃 가져오기 숨겨진 창 "데스크톱 보이기" 아이콘 포함 주 최소화 프로그램당 창 하나 최근 사용된 순서 뒤로 화면 단축키 선택한 창 보이기 정렬 순서: 쌓이는 순서 다른 모든 창을 숨겨서 현재 선택한 창을 강조합니다. 이 옵션을 사용하려면 데스크톱 효과를 활성화시켜야 합니다. 데스크톱 효과가 활성화되어 있을 때 창을 바꾸는 효과입니다. 가상 데스크톱 보이는 창 시각화 