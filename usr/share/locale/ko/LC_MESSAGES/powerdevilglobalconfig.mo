��          �      l      �     �     �     �  *         +  >   ?  9   ~     �     �  
   �      �       	        (  !   :     \  $   {  	   �     �  �   �  �  8     �     �     �  ,   �       O   ;  E   �     �     �     �       	   )     3     H     Y     y  A   �     �     �  �   �                              
                                                  	                  % &Critical level: &Low level: <b>Battery Levels                     </b> A&t critical level: Battery will be considered critical when it reaches this level Battery will be considered low when it reaches this level Configure Notifications... Critical battery level Do nothing EMAIL OF TRANSLATORSYour emails Enabled Hibernate Low battery level Low level for peripheral devices: NAME OF TRANSLATORSYour names Pause media players when suspending: Shut down Suspend The Power Management Service appears not to be running.
This can be solved by starting or scheduling it inside "Startup and Shutdown" Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-11 03:05+0200
PO-Revision-Date: 2017-12-03 00:13+0100
Last-Translator: Shinjo Park <kde@peremen.name>
Language-Team: Korean <kde@peremen.name>
Language: ko
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 % 바닥난 단계(&C): 낮은 단계(&L): <b>배터리 잔량                     </b> 바닥난 단계에서(&T): 이 수준에 도달했을 때 배터리가 바닥난 것으로 간주합니다 이 수준에 도달했을 때 배터리 부족으로 간주합니다 알림 설정... 배터리 바닥남 수준 아무것도 하지 않음 kde@peremen.name 사용함 최대 절전 모드 배터리 낮음 연결된 장치 낮은 수준: Shinjo Park 대기 모드로 들어갈 때 미디어 재생기 일시 정지: 컴퓨터 끄기 대기 모드 전원 관리 서비스가 실행 중이 아닌 것 같습니다.
"시작 및 종료" 항목에서 전원 관리 서비스를 시작하거나 예약하십시오 