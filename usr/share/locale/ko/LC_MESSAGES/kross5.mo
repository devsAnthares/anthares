��    $      <  5   \      0     1  +   E     q  4   �  &   �     �     �                     5     :     P     X  ,   u  3   �     �     �               !  '   .     V     u     {     �     �     �     �     �     �  &   �       B        b  �  y  	             9      I     j     q  	   �     �     �  &   �     �  *   �  	   	  *   	  K   D	  U   �	  2   �	  0   
     J
  
   R
     ]
  '   n
     �
     �
      �
  )   �
          	     (  .   /     ^  4   e  
   �  M   �     �                                       
                              !                                  $      #                               	                           "             @info:creditAuthor @info:creditCopyright 2006 Sebastian Sauer @info:creditSebastian Sauer @info:shell command-line argumentThe script to run. @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: application descriptionCommand-line utility to run Kross scripts. application nameKross Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2015-01-18 17:08+0900
Last-Translator: Shinjo Park <kde@peremen.name>
Language-Team: Korean <kde@peremen.name>
Language: ko
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 1.5
 작성자 Copyright 2006 Sebastian Sauer Sebastian Sauer 실행할 스크립트입니다. 일반 새 스크립트 추가. 추가... 취소하시겠습니까? 설명: cho.sungjae@gmail.com,kde@peremen.name 편집 선택한 스크립트를 편집합니다. 편집... 선택한 스크립트를 실행합니다. 인터프리터 "%1"(을)를 위한 스크립트를 만들 수 없습니다 스크립트 파일 "%1"을(를) 위한 인터프리터를 결정할 수 없습니다 인터프리터 "%1"을 불러올 수 없습니다 스크립트 파일 "%1"을 열 수 없습니다 파일: 아이콘: 인터프리터: 루비 인터프리터의 안전 등급 Cho Sung Jae,Shinjo Park 이름: 함수 "%1"이(가) 없습니다 인터프리터 "%1"이(가) 없습니다 삭제 선택한 스크립트 삭제. 실행 스크립트 파일 "%1"이(가) 없습니다. 정지 선택한 스크립트의 실행을 중지합니다. 텍스트: Kross 스크립트를 실행시키기 위한 명령행 프로그램입니다. Kross 