��    	      d      �       �      �      �   -        <  -   V  #   �  #   �     �  �  �     n      �  0   �      �  0   �     /     6     =                                   	           Current time is %1 Displays the current date Displays the current date in a given timezone Displays the current time Displays the current time in a given timezone Note this is a KRunner keyworddate Note this is a KRunner keywordtime Today's date is %1 Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2015-04-26 16:45+0200
Last-Translator: Shinjo Park <kde@peremen.name>
Language-Team: Korean <kde@peremen.name>
Language: ko
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 1.5
 현재 시간은 %1입니다 오늘 날짜를 표시합니다 지정한 시간대의 날짜를 표시합니다 현재 시간을 표시합니다 지정한 시간대의 시간을 표시합니다 날짜 시간 오늘은 %1입니다 