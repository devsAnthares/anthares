��    $      <  5   \      0     1     6  )   J     t  $   �  &   �  #   �  !   �  "   !     D     T     f     z     �  J   �     �  L        [     c     k      {     �  
   �     �     �     �     �     �  "   �       )   9  <   c     �  ;   �     �  �       �  	   �     �     �  -   �  -   	     4	     L	     a	  
   v	  
   �	     �	     �	     �	  T   �	     �	  Q   
     c
  	   j
     t
     �
     �
     �
     �
  	   �
     �
     �
     �
     �
     �
  -     Q   4     �     �     �     	                                                                         !       #             "                                        
           $                               100% @info:creditAuthor @info:creditCopyright 2015 Harald Sitter @info:creditHarald Sitter @labelNo Applications Playing Audio @labelNo Applications Recording Audio @labelNo Device Profiles Available @labelNo Input Devices Available @labelNo Output Devices Available @labelProfile: @titlePulseAudio @title:tabAdvanced @title:tabApplications @title:tabDevices Add virtual output device for simultaneous output on all local sound cards Advanced Output Configuration Automatically switch all running streams when a new output becomes available Capture Default Device Profiles EMAIL OF TRANSLATORSYour emails Inputs Mute audio NAME OF TRANSLATORSYour names Notification Sounds Outputs Playback Port Port is unavailable (unavailable) Port is unplugged (unplugged) Requires 'module-gconf' PulseAudio module This module allows to set up the Pulseaudio sound subsystem. label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-03 03:00+0200
PO-Revision-Date: 2017-12-02 23:13+0100
Last-Translator: Shinjo Park <kde@peremen.name>
Language-Team: Korean <kde@peremen.name>
Language: ko
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 100% 작성자 Copyright 2015 Harald Sitter Harald Sitter 오디오를 재생하는 프로그램 없음 오디오를 녹음하는 프로그램 없음 장치 프로필 없음 입력 장치 없음 출력 장치 없음 프로필: PulseAudio 고급 프로그램 장치 모든 로컬 사운드 카드로 동시에 출력하는 가상 출력 장치 추가 고급 출력 설정 새로운 출력을 사용할 수 있을 때 자동으로 모든 스트림 전환 캡처 기본값 장치 프로필 kde@peremen.name 입력 오디오 음소거 Shinjo Park 알림음 출력 재생 포트 (사용할 수 없음) (연결 해제됨) 'module-gconf' PulseAudio 모듈이 필요함 이 모듈에서 PulseAudio 소리 서브시스템을 설정할 수 있습니다. %1: %2 100% %1% 