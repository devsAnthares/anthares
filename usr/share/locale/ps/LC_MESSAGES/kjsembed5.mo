��    !      $  /   ,      �  *   �          -     J     g     |     �     �      �     �     �          5     N     l     �     �     �  !   �     �          #     >     Z     u     �     �     �  (   �     �  &     &   2  �  Y  Q   /     �  &   �  &   �  $   �  *   	  -   ?	  !   m	  3   �	  ,   �	  0   �	  &   !
  +   H
  9   t
  9   �
  ;   �
  1   $  '   V  >   ~  (   �  +   �  0     )   C  2   m  -   �  .   �      �  #     U   B     �  A   �  A   �                                                                       
                     !             	                                                      %1 is not a function and cannot be called. %1 is not an Object type '%1' is not a valid QLayout. '%1' is not a valid QWidget. Action takes 2 args. ActionGroup takes 2 args. Call to '%1' failed. Could not construct value Could not create temporary file. Could not open file '%1' Could not open file '%1': %2 Could not read file '%1' Failed to create Action. Failed to create ActionGroup. Failed to create Layout. Failed to create Widget. Failed to load file '%1' File %1 not found. First argument must be a QObject. Incorrect number of arguments. Must supply a filename. Must supply a layout name. Must supply a valid parent. Must supply a widget name. No classname specified No classname specified. No such method '%1'. Not enough arguments. There was an error reading the file '%1' Wrong object type. include only takes 1 argument, not %1. library only takes 1 argument, not %1. Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2008-08-16 16:01-0800
Last-Translator: Zabeeh Khan <zabeehkhan@gmail.com>
Language-Team: Pashto <pathanisation@googlegroups.com>
Language: ps
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-Language: Pashto, Pushto
X-Poedit-Country: AFGHANISTAN
 کومه کړنه نه ده او اړيکنيو ورسره نه شي کېدی. %1 يو څيز ډول نه دی %1 .سمه ک.هډوانه نه ده '%1' سم ک.کاروونی نه دی. '%1' چار ۲ ارزښتمنۍ اخلي. چارډله ۲ ارزښتمنۍ اخلي. .سره اړيکنيو پاتې راغی '%1' ارزښت نه شي جوړولی .لنډمهاله دوتنه نه شي جوړولی دوتنه نه شي پرانيستلی '%1' %2 :دوتنه نه شي پرانيستلی '%1' دوتنه نه شي لوستلی '%1' .چار جوړولو کې پاتې راغی د چارډلې په جوړولو کې پاتې راغی. .د هډوانې په جوړولو کې پاتې راغی د کارووني په جوړولو کې پاتې راغی. دوتنې لېښلو کې پاتې راغی '%1' .دوتنه ونه موندل شوه %1 ښايي چې لومړۍ ارزښتمنۍ يو ک.څيز وي. .د ارزښتمنېو ناسم شمېر .دوتنه نوم ورکول اړين دي .د هډوانې نوم ورکول اړين دي .سم پلرين ورکول اړين دی د کارووني نوم ورکول اړين دي. پاړکی نوم نه دی ورکړل شوی .پاړکی نوم نه دی ورکړل شوی .'%1' داسې لېله نشته .بشپړې ارزښمنۍ نشته دوتنې په لوستلو کې کومه ستونزه رامنځته شوه '%1' د .ناسم څيز ډول .%1 ننويستل يوازې ۱ ارزښتمنۍ اخلي، نه .%1 کتابتون يوازې ۱ ارزښتمنۍ اخلي، نه 