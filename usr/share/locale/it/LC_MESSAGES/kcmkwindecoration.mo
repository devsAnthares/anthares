��            )   �      �  !   �  "   �  '   �  ,     #   ;  &   _  !   �  &   �  '   �     �                 V   $  1   {     �  *   �     �        
     
   "     -     6     ;     D     T     [     a     g  �  p     "     )     0     =     S     [     m  
   u     �     �     �     �     �  Y   �  <   %     b  :   t  &   �     �     �     	  
   #	     .	  	   3	     =	     P	     V	     _	     d	                                                                          
                                                    	           @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Borders @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large Application menu Border si&ze: Buttons Close Close by double clicking:
 To open the menu, keep the button pressed until it appears. Close windows by double clicking &the menu button Context help Drag buttons between here and the titlebar Drop here to remove button Get New Decorations... Keep above Keep below Maximize Menu Minimize On all desktops Search Shade Theme Titlebar Project-Id-Version: kcmkwindecoration
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-04-14 02:49+0200
PO-Revision-Date: 2015-10-10 11:23+0200
Last-Translator: Vincenzo Reale <smart2128@baslug.org>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 Enorme Spesso Nessun bordo Nessun bordo laterale Normale Sovradimensionato Sottile Gigantesco Molto spesso Menu dell'applicazione Dimen&sione del bordo: Pulsanti Chiudi Chiudi con un doppio clic:
 Per aprire il menu, tieni premuto il pulsante finché appare. Chiudi le fines&tre con un doppio clic sul pulsante del menu Aiuto contestuale Trascina i pulsanti tra questo punto e la barra del titolo Rilascia qui per rimuovere il pulsante Scarica nuove decorazioni... Mantieni sopra le altre Mantieni sotto le altre Massimizza Menu Minimizza Su tutti i desktop Cerca Arrotola Tema Barra del titolo 