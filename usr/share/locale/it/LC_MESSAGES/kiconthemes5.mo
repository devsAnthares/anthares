��          �      L      �  
   �     �  >   �                  
   -     8     @     H     O  	   [     e     s     z  2   �     �     �  �  �     w     �  >   �     �     �     �  	   �     �     �          
  	        #     1     9  9   L     �     �                                   	                                                         
        &Browse... &Search: *.png *.xpm *.svg *.svgz|Icon Files (*.png *.xpm *.svg *.svgz) Actions All Applications Categories Devices Emblems Emotes Icon Source Mimetypes O&ther icons: Places S&ystem icons: Search interactively for icon names (e.g. folder). Select Icon Status Project-Id-Version: kio4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:11+0100
PO-Revision-Date: 2016-11-22 20:21+0100
Last-Translator: Luigi Toscano <luigi.toscano@tiscali.it>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 &Sfoglia... &Trova: *.png *.xpm *.svg *.svgz|File icone (*.png *.xpm *.svg *.svgz) Azioni Tutte Applicazioni Categorie Dispositivi Emblemi Faccine Sorgente icone Tipi MIME &Altre icone: Risorse Icone di &sistema: Cerca interattivamente i nomi delle icone (es. cartella). Scegli l'icona Stato 