��    *      l  ;   �      �     �     �  )   �               /     H     ^  #   ~     �  
   �     �     �  %   �  +        E     Z     m  @   z     �     �     �     �               '     ,     9     ?     _     e  .   m     �  F   �  (   �  	   '  	   1  	   ;  '   E  7   m  "   �  �  �     w	     �	     �	     �	     �	  	   �	     �	     �	     �	     
  	   
     '
     D
  '   a
  L   �
     �
     �
       W        m     �     �     �     �     �     �     �               (     .  7   ;     s  L   �  +   �               !     (     -  
   9                   $                                 %                      *                                               	   #   )   "             &           
                        '             (   !    &Do not remember @actionWalk through activities @actionWalk through activities (Reverse) @action:buttonApply @action:buttonCancel @action:buttonChange... @action:buttonCreate @title:windowActivity Settings @title:windowCreate a New Activity @title:windowDelete Activity Activities Activity information Activity switching Are you sure you want to delete '%1'? Blacklist all applications not on this list Clear recent history Create activity... Description: Error loading the QML files. Check your installation.
Missing %1 For a&ll applications Forget a day Forget everything Forget the last hour Forget the last two hours General Icon Keep history Name: O&nly for specific applications Other Privacy Private - do not track usage for this activity Remember opened documents: Remember the current virtual desktop for each activity (needs restart) Shortcut for switching to this activity: Shortcuts Switching Wallpaper for in 'keep history for 5 months'for  unit of time. months to keep the history month  months unlimited number of monthsforever Project-Id-Version: kcm_activities
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-04-22 19:40+0200
Last-Translator: Vincenzo Reale <smart2128@baslug.org>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8.7
 &Non ricordare Percorri le attività Percorri le attività (inverso) Applica Annulla Cambia... Crea Impostazioni attività Crea una nuova attività Elimina attività Attività Informazioni sulle attività Commutazione delle attività Sei sicuro di volerlo eliminare «%1»? Aggiungi alla lista nera tutte le applicazioni non presenti in questo elenco Ripulisci la cronologia recente Crea attività... Descrizione: Errore durante il caricamento dei file QML. Controlla la tua installazione.
%1 mancante Per tutte &le applicazioni Dimentica un giorno Dimentica tutto Dimentica l'ultima ora Dimentica le ultime due ore Generale Icona Mantieni la cronologia Nome: &Solo per certe applicazioni Altro Riservatezza Privato - non tracciare l'utilizzo per questa attività Ricorda i documenti aperti: Ricorda il desktop virtuale attuale per ogni attività (richiede il riavvio) Scorciatoia per passare a questa attività: Scorciatoie Commutazione Sfondo per   mese  mesi per sempre 