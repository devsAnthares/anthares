��          �            h  ~   i  +   �           5     T     s     �     �  �   �  g   A  0   �  .   �     	  @     �  Z  �     9   �     �     �            	        "  �   8  u   �  8   O  0   �     �  
   �                                                   	             
                Click on the button, then enter the shortcut like you would in the program.
Example for Ctrl+A: hold the Ctrl key and press A. Conflict with Standard Application Shortcut EMAIL OF TRANSLATORSYour emails KPackage QML application shell NAME OF TRANSLATORSYour names No shortcut definedNone Reassign Reserved Shortcut The '%1' key combination is also used for the standard action "%2" that some applications use.
Do you really want to use it as a global shortcut as well? The F12 key is reserved on Windows, so cannot be used for a global shortcut.
Please choose another one. The key you just pressed is not supported by Qt. The unique name of the application (mandatory) Unsupported Key What the user inputs now will be taken as the new shortcutInput Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-30 03:07+0100
PO-Revision-Date: 2015-03-15 13:50+0100
Last-Translator: Vincenzo Reale <smart2128@baslug.org>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 1.5
 Fai clic sul pulsante, quindi digita la scorciatoia come la vorresti nel programma.
Esempio per Ctrl+A: tieni premuto il tasto Ctrl e premi A. Conflitto con una scorciatoia standard delle applicazioni smart2128@baslug.org Shell applicazione KPackage QML Vincenzo Reale Nessuna Riassegna Scorciatoia riservata La combinazione di tasti «%1» è anche usata per l'azione standard «%2» che alcune applicazioni usano.
Vuoi veramente usarla anche come scorciatoia globale? Il tasto F12 è riservato sotto Windows, quindi non può essere usato per una scorciatoia globale.
Scegline un altro. Il tasto che hai appena premuto non è supportato da Qt. Il nome univoco dell'applicazione (obbligatorio) Tasto non supportato Immissione 