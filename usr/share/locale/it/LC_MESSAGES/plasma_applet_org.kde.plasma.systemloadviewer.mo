��    &      L  5   |      P     Q     l     y     �     �     �     �     �     �     �     �     �  &   �               #     ,     3     ?     M     U     ]     d     s     �     �     �     �     �     �     �     �     �     �  
   �            �       �     �     �                              .     >     G     Y  &   _     �  	   �  	   �     �     �     �     �  
   �     �     �            
   .     9     T     \     a     w     �     �     �     �     �     �                           	      #      $                                                    &                                          !          %                            "   
    Abbreviation for secondss Application: Average clock: %1 MHz Bar Buffers: CPU CPU %1 CPU monitor CPU%1: %2% @ %3 Mhz CPU: %1% CPUs separately Cache Cache Dirty, Writeback: %1 MiB, %2 MiB Cache monitor Cached: Circular Colors Compact Bar Dirty memory: General IOWait: Memory Memory monitor Memory: %1/%2 MiB Monitor type: Nice: Set colors manually Show: Swap Swap monitor Swap: %1/%2 MiB Sys: System load Update interval: Used swap: User: Writeback memory: Project-Id-Version: plasma_applet_systemloadviewer
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-29 03:31+0200
PO-Revision-Date: 2017-03-05 09:29+0100
Last-Translator: Vincenzo Reale <smart2128@baslug.org>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 s Applicazione: Frequenza media: %1 MHz Barra Buffer: CPU CPU%1 Indicatore della CPU CPU%1:%2,%3 MHz CPU: %1% CPU separatamente Cache Cache Dirty, Writeback: %1 MiB, %2 MiB Monitor della cache In cache: Circolare Colori Barra compatta Memoria dirty: Generale Attesa IO: Memoria Indicatore della memoria Memoria: %1/%2 MiB Tipo di indicatore Priorità: Imposta colori manualmente Mostra: Swap Indicatore della swap Swap: %1/%2 MiB Sistema: Carico di sistema Intervallo d'aggiornamento: Swap in uso: Utente: Memoria writeback 