��          �   %   �      0  *   1     \  .   s     �     �     �     �     �     �                 
   $     /     5     =     E     ]     t     �     �     �     �  �  �  -   �     �  -   �               ,     F     ]     n     �     �     �     �     �     �     �     �     �     �  !        <     N  	   Z        
                             	                                                                                          %1 Minimized Window: %1 Minimized Windows: %1 Window: %1 Windows: ...and %1 other window ...and %1 other windows Activity name Activity number Add Virtual Desktop Configure Desktops... Desktop name Desktop number Display: Does nothing General Horizontal Icons Layout: No text Only the current screen Remove Virtual Desktop Selecting current desktop: Show Activity Manager... Shows desktop The pager layoutDefault Vertical Project-Id-Version: plasma_applet_pager
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-19 03:29+0200
PO-Revision-Date: 2016-10-30 00:50+0200
Last-Translator: Vincenzo Reale <smart2128@baslug.org>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 %1 Minimized Window: %1 finestre minimizzate: %1 finestra: %1 finestre: ...e %1 altra finestra ...e %1 altre finestre Nome dell'attività Numero dell'attività Aggiungi desktop virtuale Configura i desktop... Nome del desktop Numero del desktop Schermo: Non fa nulla Generale Orizzontale Icone Disposizione: Nessun testo Solo lo schermo attuale Rimuovi desktop virtuale Selezione desktop corrente: Mostra gestore delle attività... Mostra il desktop Predefinita Verticale 