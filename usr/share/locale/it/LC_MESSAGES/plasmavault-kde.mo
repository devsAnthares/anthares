��    ?        Y         p  �  q  }  J  M  �        
   7     B     K     j     �  <   �     �     �  	   �     �  .     %   A     g     }     �     �     �     �     �     �     �     �  4        B  9   T     �     �     �  �   �  !   �  t   �     8     D     a     n     s  	   �     �  -   �     �  B   �  &     ?   B  '   �  )   �  7   �  .     5   ;  +   q     �     �     �  ,   �          -  9   :  V   t  C   �  �    �  �  �  �  r  d     �"  	   �"     �"  (   �"  )   '#     Q#  @   Z#     �#     �#  	   �#     �#  8   �#  $    $     E$     _$     d$     |$     �$     �$     �$  !   �$     �$     �$  =   %     R%  P   j%     �%     �%     �%  �   �%  "   �&  �   �&     |'  .   �'     �'  
   �'     �'  	   �'     (  /   (  
   E(  U   P(  (   �(  H   �(  5   )  8   N)  >   �)  *   �)  =   �)  .   /*      ^*  #   *     �*  1   �*     �*     	+  J   +  h   f+     �+     !      	   2   ?   
   /                 +   (   *       1            )   >           8          9   $              7             .       '   6          %   <             3   ,          ;       =   &                              -           #      "          :      5      0                                   4               <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body style=" font-family:'hlv'; font-size:9pt; font-weight:400; font-style:normal;">
<p style="-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><br /></p></body></html> <b>Security notice:</b>
                             According to a security audit by Taylor Hornby (Defuse Security),
                             the current implementation of Encfs is vulnerable or potentially vulnerable
                             to multiple types of attacks.
                             For example, an attacker with read/write access
                             to encrypted data might lower the decryption complexity
                             for subsequently encrypted data without this being noticed by a legitimate user,
                             or might use timing analysis to deduce information.
                             <br /><br />
                             This means that you should not synchronize
                             the encrypted data to a cloud storage service,
                             or use it in other circumstances where the attacker
                             can frequently access the encrypted data.
                             <br /><br />
                             See <a href='http://defuse.ca/audits/encfs.htm'>defuse.ca/audits/encfs.htm</a> for more information. <b>Security notice:</b>
                             CryFS encrypts your files, so you can safely store them anywhere.
                             It works well together with cloud services like Dropbox, iCloud, OneDrive and others.
                             <br /><br />
                             Unlike some other file-system overlay solutions,
                             it does not expose the directory structure,
                             the number of files nor the file sizes
                             through the encrypted data format.
                             <br /><br />
                             One important thing to note is that,
                             while CryFS is considered safe,
                             there is no independent security audit
                             which confirms this. @title:windowCreate a New Vault Activities Backend: Can not create the mount point Can not open an unknown vault. Change Choose the encryption system you want to use for this vault: Choose the used cypher: Close the vault Configure Configure Vault... Configured backend can not be instantiated: %1 Configured backend does not exist: %1 Correct version found Create Create a New Vault... CryFS Device is already open Device is not open Dialog Do not show this notice again EncFS Encrypted data location Failed to create directories, check your permissions Failed to execute Failed to fetch the list of applications using this vault Failed to open: %1 Forcefully close  General If you limit this vault only to certain activities, it will be shown in the applet only when you are in those activities. Furthermore, when you switch to an activity it should not be available in, it will automatically be closed. Limit to the selected activities: Mind that there is no way to recover a forgotten password. If you forget the password, your data is as good as gone. Mount point Mount point is not specified Mount point: Next Open with File Manager Password: Plasma Vault Please enter the password to open this vault: Previous The mount point directory is not empty, refusing to open the vault The specified backend is not available The vault configuration can only be changed while it is closed. The vault is unknown, can not close it. The vault is unknown, can not destroy it. This device is already registered. Can not recreate it. This directory already contains encrypted data Unable to close the vault, an application is using it Unable to close the vault, it is used by %1 Unable to detect the version Unable to perform the operation Unknown device Unknown error, unable to create the backend. Use the default cipher Vaul&t name: Wrong version installed. The required version is %1.%2.%3 You need to select empty directories for the encrypted storage and for the mount point formatting the message for a command, as in encfs: not found%1: %2 Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-11 05:53+0100
PO-Revision-Date: 2017-12-11 23:27+0100
Last-Translator: Luigi Toscano <luigi.toscano@tiscali.it>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body style=" font-family:'hlv'; font-size:9pt; font-weight:400; font-style:normal;">
<p style="-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><br /></p></body></html> <b>Avviso di sicurezza:</b>
                             In base ad un controllo di sicurezza di Taylor Hornby (Defuse Security),
                             l'implementazione attuale di Encfs è vulnerabile o potenzialmente vulnerabile
                             a vari tipi di attacco.
                             Ad esempio, un attaccante con accesso in lettura/scrittura
                             ai dati cifrati potrebbe abbassare la complessità di decifratura
                             per i dati cifrati successivamente senza che questo sia visibile ad un utente legittimo,
                             o potrebbe usare un'analisi delle tempistiche per dedurre informazioni.
                             <br /><br />
                             Questo vuol dire che non dovresti sincronizzare
                             i dati cifrati con un dispositivo di memorizzazione sul cloud,
                             o usarlo in altre circostanze dove l'attaccante
                             può accedere frequentemente ai dati cifrati.
                             <br /><br />
                             Consultare <a href='http://defuse.ca/audits/encfs.htm'>defuse.ca/audits/encfs.htm</a> per maggiori informazioni. <b>Avviso di sicurezza:</b>
                             CryFS cifra i tuoi file, così puoi memorizzarli ovunque in modo sicuro.
                             Funziona bene con servizi di cloud come Dropbox, iCloud, OneDrive ed altri.
                             <br /><br />
                             Al contrario di altre soluzioni di tipo overlay sul filesystem,
                             non espone la struttura delle cartelle,
                             il numero di file o la loro dimensione
                             attraverso il formato dei dai cifrati.
                             <br /><br />
                             Un fattore importante da considerare è che,
                             anche se CryFS è considerato sicuro,
                             non ci sono controlli di sicurezza indipendenti
                             che lo confermino. Crea un nuovo caveau Attività Motore: Impossibile creare il punto di montaggio Impossibile aprire un caveau sconosciuto. Modifica Scegli il sistema di cifratura che vuoi usare per questo caveau: Scegli il cifrario da usare: Chiudi il caveau Configura Configura caveau... Impossibile creare un'istanza del motore configurato: %1 Il motore configurato non esiste: %1 Trovata versione corretta Crea Crea un nuovo caveau... CryFS Il dispositivo è già aperto Il dispositivo non è aperto Finestra di dialogo Non mostrare più questa notifica EncFS Posizione dei dati cifrati Creazione delle cartelle non riuscita, controllare i permessi Esecuzione non riuscita Scaricamento non riuscito dell'elenco delle applicazioni che usano questo caveau Impossibile aprire: %1 Chiudi forzatamente General Se limiti questo caveau solo ad alcune attività, sarà mostrato nell'applet solo quando ti trovi in quelle attività. Inoltre, quando passi ad un'attività in cui non dovrebbe essere disponibile, sarà chiuso automaticamente. Limita alle attività selezionate: Si presti attenzione al fatto che non c'è modo di recuperare una password dimenticata. Se dimentichi la password, i dati sono perduti. Punto di montaggio Il punto di montaggio non è stato specificato Punto di montaggio: Successivo Apri con il gestore dei file Password: Caveau di Plasma Inserisci la password per aprire questo caveau: Precedente La cartella punto di montaggio non è vuota, l'apertura del caveau è stata declinata Il motore specificato non è disponibile La configurazione del caveau può essere cambiata solo quando è chiuso. Il caveau è sconosciuto, non è possibile chiuderlo. Il caveau è sconosciuto, non è possibile distruggerlo. Il dispositivo è già registrato. Non è possibile ricrearlo. La cartella contiene già dei dati cifrati Impossibile chiudere il caveau, un'applicazione lo sta usando Impossibile chiudere il caveau, è usato da %1 Impossibile rilevare la versione Impossibile effettuare l'operazione Dispositivo sconosciuto Errore sconosciuto, impossibile creare il motore. Usa il cifrario predefinito Nome del cave&au: È installata una versione non corretta. La versione richiesta è %1.%2.%3 Devi selezionare delle cartelle vuote per lo spazio di archiviazione cifrato e per il punto di montaggio %1: %2 