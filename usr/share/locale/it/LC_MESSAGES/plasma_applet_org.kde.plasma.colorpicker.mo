��          |      �          %   !     G     U     c     u     �     �  
   �     �     �  $   �  �  �  -   �     �     �     �          1     :     S     a     r  (   �     
   	                                                   Automatically copy color to clipboard Clear History Color Options Copy to Clipboard Default color format: General Open Color Dialog Pick Color Pick a color Show history When pressing the keyboard shortcut: Project-Id-Version: plasma_applet_kolourpicker
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-12 03:03+0200
PO-Revision-Date: 2016-12-16 22:02+0100
Last-Translator: Vincenzo Reale <smart2128@baslug.org>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 Copia automaticamente il colore negli appunti Cancella cronologia Opzioni di colore Copia negli appunti Formato di colore predefinito: Generale Apri finestra dei colori Scegli colore Scegli un colore Mostra la cronologia Quando premi la scorciatoia da tastiera: 