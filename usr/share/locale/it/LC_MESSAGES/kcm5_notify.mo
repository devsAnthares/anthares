��          �      �       0     1  �  H     �       &         ?     `     n     v     �     �  (   �  �  �     �  �  �     ]     n  *   ~     �     �     �     �     �     	  :   !         
                 	                                (c) 2002-2006 KDE Team <h1>System Notifications</h1>Plasma allows for a great deal of control over how you will be notified when certain events occur. There are several choices as to how you are notified:<ul><li>As the application was originally designed.</li><li>With a beep or other noise.</li><li>Via a popup dialog box with additional information.</li><li>By recording the event in a logfile without any additional visual or audible alert.</li></ul> Carsten Pfeiffer Charles Samuels Disable sounds for all of these events EMAIL OF TRANSLATORSYour emails Event source: KNotify NAME OF TRANSLATORSYour names Olivier Goffart Original implementation System Notification Control Panel Module Project-Id-Version: kcmnotify
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-09 07:18+0200
PO-Revision-Date: 2017-09-12 21:10+0100
Last-Translator: Vincenzo Reale <smart2128@baslug.org>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 (c) 2002-2006 Il Team di KDE <h1>Notifiche di sistema</h1>Plasma fornisce molto controllo sulla modalità di notifica al verificarsi di determinati eventi. Queste sono le scelte possibili:<ul><li>Come l'applicazione è stata originariamente progettata.</li><li>Con un bip o un altro suono.</li><li>Con una finestra a comparsa con informazioni aggiuntive.</li><li>Registrando l'evento in un file di log senza ulteriori segnalazioni visive o acustiche.</li></ul> Carsten Pfeiffer Charles Samuels Disabilita i suoni per tutti questi eventi smart2128@baslug.org, Sorgente dell'evento: KNotify Vincenzo Reale,Andrea Rizzi Olivier Goffart Realizzazione originale Modulo del pannello di controllo per gli avvisi di sistema 