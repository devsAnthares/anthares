��    w      �  �   �      
     
  	   &
     0
     =
     K
     Q
     X
     i
     o
     w
     
     �
     �
     �
     �
  	   �
     �
     �
  
   �
     �
     �
       
             !     (  	   7     A  
   G  
   R     ]     i     x     ~     �     �     �     �  ;   �  -   �  #        ;     T     l  
   �     �     �  
   �     �     �     �               7     K     P  	   k      u     �     �     �  
   �     �     �               .     E     V     f     v     �     �  
   �     �  )   �     �          #     2     A     R     X     `     s  '   �     �     �     �     �     �     
           .  C   <     �  s   �           %     :     Q     f     �     �      �     �  -   �  /        4  	   =  !   G     i      �     �     �     �     �     �  �  �     �     �     �     �  	   �     �     �            
        (  	   @     J     X     l  	   t     ~     �     �     �     �     �     �     �     �     �  
          
        $     1     A     W     ^     r     �     �     �     �     �      �     �  	   �     �             $   )     N     ]     n     �  &   �      �     �       !        -     :     S     m     y     �     �      �  #   �     �          )     ?     U     p  	   �  I   �     �     �  %   �        "   <     _     t     �     �     �     �     �  3   �          &  !   6     X  %   i     �     �      �  [   �     @  �   Z  +   �  #   
  #   .  *   R  &   }     �     �  2   �       0     3   F     z     �  +   �  /   �  .   �  	   "     ,     4     :  
   A     o   5   l   W   	             1               R   6          J         >   u   d           V          M   T       X   r   v          G   .   Q   \       a              E   -       O               ^          b   7           +   Y   C      [       4                 H       k   B   c       ]   I   *   P   (   h   w   ;   N   3   
                @      "   S      _                   L   $      9   F      D          m   U   %       K       &                     t   Z   `   !       ,   0   g   f   p          i      2   e       n   /      q       ?      <   A   #   8          =   '   s   )               :   j           %1 &Handbook &About %1 &Actual Size &Add Bookmark &Back &Close &Configure %1... &Copy &Delete &Donate &Edit Bookmarks... &Find... &First Page &Fit to Page &Forward &Go To... &Go to Line... &Go to Page... &Last Page &Mail... &Move to Trash &New &Next Page &Open... &Paste &Previous Page &Print... &Quit &Redisplay &Rename... &Replace... &Report Bug... &Save &Save Settings &Spelling... &Undo &Up &Zoom... @action:button Goes to next tip, opposite to previous&Next @action:button Goes to previous tip&Previous @option:check&Show tips on startup @titleDid you know...?
 @title:windowConfigure @title:windowTip of the Day About &KDE C&lear Check spelling in document Clear List Close document Configure &Notifications... Configure S&hortcuts... Configure Tool&bars... Copy selection to clipboard Create new document Cu&t Cut selection to clipboard Dese&lect EMAIL OF TRANSLATORSYour emails Encodings menuAutodetect Encodings menuDefault F&ull Screen Mode Find &Next Find Pre&vious Fit to Page &Height Fit to Page &Width Go back in document Go forward in document Go to first page Go to last page Go to next page Go to previous page Go up NAME OF TRANSLATORSYour names No Entries Open &Recent Open a document which was recently opened Open an existing document Paste clipboard content Print Previe&w Print document Quit application Re&do Re&vert Redisplay document Redo last undone action Revert unsaved changes made to document Save &As... Save document Save document under a new name Select &All Select zoom level Send document by mail Show &Menubar Show &Toolbar Show Menubar<p>Shows the menubar again after it has been hidden</p> Show St&atusbar Show Statusbar<p>Shows the statusbar, which is the bar at the bottom of the window used for status information.</p> Show a print preview of document Show or hide menubar Show or hide statusbar Show or hide toolbar Switch Application &Language... Tip of the &Day Undo last action View document at its actual size What's &This? You are not allowed to save the configuration You will be asked to authenticate before saving Zoom &In Zoom &Out Zoom to fit page height in window Zoom to fit page in window Zoom to fit page width in window go back&Back go forward&Forward home page&Home show help&Help without name Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-16 03:10+0100
PO-Revision-Date: 2017-08-14 15:21+0200
Last-Translator: Vincenzo Reale <smart2128@baslug.org>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 &Manuale di %1 Informazioni &su %1 Dimensione re&ale &Aggiungi un segnalibro &Indietro &Chiudi &Configura %1... &Copia Eli&mina &Donazione &Modifica segnalibri... Tro&va... &Prima pagina &Adatta alla pagina &Avanti &Vai a... Vai alla &riga... Vai alla &pagina... &Ultima pagina S&pedisci... &Cestina &Nuovo Pagina &successiva &Apri... &Incolla Pagina &precedente Stam&pa... &Esci &Ridisegna &Rinomina... S&ostituisci... Segnala un e&rrore... &Salva Salva imp&ostazioni &Ortografia... &Annulla &Su In&grandimento... &Successivo &Precedente Mo&stra i suggerimenti all'avvio Sapevi che...?
 Configura Suggerimento del giorno Informazioni su &KDE Pu&lisci Controlla l'ortografia nel documento Pulisci elenco Chiudi documento Configura le &notifiche... Configura le scorcia&toie... Configura le &barre degli strumenti... Copia la selezione negli appunti Crea nuovo documento &Taglia Taglia la selezione negli appunti Dese&leziona smart2128@baslug.org,,,, Riconoscimento automatico Predefinita Modalità a t&utto schermo Trova &successivo Trova &precedente Adatta all'&altezza della pagina Adatta alla &larghezza della pagina Indietro nel documento Avanti nel documento Vai alla prima pagina Vai all'ultima pagina Vai alla pagina successiva Vai alla pagina precedente Vai in su Vincenzo Reale,Federico Zenith,Dario Panico,Nicola Ruggero,Federico Cozzi Nessuna voce Apri &recente Apri un documento recentemente aperto Apri un documento esistente Incolla il contenuto degli appunti Ante&prima di stampa Stampa documento Esci dall'applicazione Ri&fai Ann&ulla Ridisegna il documento Rifai l'ultima azione annullata Annulla le modifiche non salvate fatte al documento Salva co&me... Salva documento Salva documento con un nuovo nome Seleziona t&utto Seleziona il livello di ingrandimento Invia documento per posta Mostra la barra dei &menu Mostra la barra degli &strumenti Mostra la barra dei menu<p>Mostra di nuovo la barra dei menu dopo che è stata nascosta</p> Mostra la barra di st&ato Mostra la barra di stato<p>Mostra la barra di stato, cioè quella in fondo alla finestra che indica lo stato dell'applicazione.</p> Mostra un'anteprima di stampa del documento Mostra o nascondi la barra dei menu Mostra o nascondi la barra di stato Mostra o nascondi la barra degli strumenti Cambia la &lingua dell'applicazione... &Suggerimento del giorno Annulla l'ultima azione Mostra il documento nelle sue dimensioni effettive Che &cos'è? Non hai il permesso di salvare la configurazione Ti sarà richiesto di autenticarti prima di salvare &Ingrandisci &Rimpicciolisci Adatta l'altezza della pagina alla finestra Adatta le dimensioni della pagina alla finestra Adatta la larghezza della pagina alla finestra &Indietro &Avanti &Home &Aiuto senza nome 