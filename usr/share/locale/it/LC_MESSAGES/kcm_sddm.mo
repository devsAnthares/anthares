��    ,      |  ;   �      �  (   �     �  �   �     �     �     �     �     �     �     �     �     �               %     1      J     k     s     �     �     �     �     �     �     �               /     4     I     Y     l     y     �     �      �  7   �                     1     6  �  <     �     �     �     	     &	     /	     6	     J	     R	     d	  #   l	     �	     �	     �	     �	     �	     �	      
     	
     
     7
     H
     Z
     x
  &   �
     �
     �
     �
     �
     �
     �
          0     =     X  	   u  (     >   �     �     �                '     !                 %   +   ,      "   #                               &                  	                           )                             *      '      $      (                                
    %1 is the name of a session%1 (Wayland) ... @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) @title:windowSelect Image Advanced Author Auto &Login Background: Clear Image Commands Could not decompress archive Cursor Theme: Customize theme Default Description Download New SDDM Themes EMAIL OF TRANSLATORSYour emails General Get New Theme Halt Command: Install From File Install a theme. Invalid theme package Load from file... Login screen using the SDDM Maximum UID: Minimum UID: NAME OF TRANSLATORSYour names Name No preview available Reboot Command: Relogin after quit Remove Theme SDDM KDE Config SDDM theme installer Session: The default cursor theme in SDDM The theme to install, must be an existing archive file. Theme Unable to install theme Uninstall a theme. User User: Project-Id-Version: kcm_sddm
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2017-10-19 00:08+0100
Last-Translator: Vincenzo Reale <smart2128@baslug.org>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 %1 (Wayland) ... (Dimensioni disponibili: %1) Seleziona immagine Avanzate Autore Accesso auto&matico Sfondo: Cancella immagine Comandi Impossibile decomprimere l'archivio Tema di puntatori: Personalizza il tema Predefinito Descrizione Scarica nuovi temi di SDDM smart2128@baslug.org Generale Ottieni un nuovo tema Comando di spegnimento: Installa da file Installa un tema. Pacchetto dei temi non valido Carica da file... Schermata di accesso che utilizza SDDM UID massimo: UID minimo: Vincenzo Reale Nome Nessuna anteprima disponibile Comando di riavvio: Accedi nuovamente dopo l'uscita Rimuovi tema Configurazione SDDM di KDE Installatore di temi di SDDM Sessione: Il tema di puntatori predefinito di SDDM Il tema da installare, deve essere un file archivio esistente. Tema Impossibile installare il tema Disinstalla un tema. Utente Utente: 