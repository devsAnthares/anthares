��    %      D  5   l      @     A     E     G  	   Y     c     |     �     �     �     �     �     �            S   ,  w   �     �  M         [     |  ?   �     �  	   �     �     
     #  %   2     X     e  F   �  #   �     �  ]     R   f     �     �  �  �     �	     �	     �	  	   �	     �	     �	     �	  
   �	     �	     
     .
  #   D
     h
     �
  a   �
  b   �
     V  T   p     �     �  E   �     0     N     ]     |     �  #   �     �     �  K   �  "   *     M  ~   d  I   �     -  &   ?               !      "          #   %                                                                            $                          	                
                            ms % %1 - All Desktops %1 - Cube %1 - Current Application %1 - Current Desktop %1 - Cylinder %1 - Sphere &Reactivation delay: &Switch desktop on edge: Activation &delay: Active Screen Corners and Edges Activity Manager Always Enabled Amount of time required after triggering an action until the next trigger can occur Amount of time required for the mouse cursor to be pushed against the edge of the screen before the action is triggered Application Launcher Change desktop when the mouse cursor is pushed against the edge of the screen EMAIL OF TRANSLATORSYour emails Lock Screen Maximize windows by dragging them to the top edge of the screen NAME OF TRANSLATORSYour names No Action Only When Moving Windows Open krunnerRun Command Other Settings Quarter tiling triggered in the outer Show Desktop Switch desktop on edgeDisabled Tile windows by dragging them to the left or right edges of the screen Toggle alternative window switching Toggle window switching Trigger an action by pushing the mouse cursor against the corresponding screen edge or corner Trigger an action by swiping from the screen edge towards the center of the screen Window Management of the screen Project-Id-Version: kcmkwinscreenedges
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-30 03:13+0100
PO-Revision-Date: 2018-01-03 09:40+0100
Last-Translator: Vincenzo Reale <smart2128@baslug.org>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
  ms % %1 - Tutti i desktop %1 - Cubo %1 - Applicazione corrente %1 - Desktop corrente %1 - Cilindro %1 - Sfera Ritardo &riattivazione: &Cambia desktop sul bordo: Ritar&do attivazione: Angoli e bordi dello schermo attivi Gestore delle attività Sempre abilitato Tempo che deve trascorrere dopo l'innesco di un'azione prima che possa partire una seconda azione Tempo che il puntatore del mouse deve sostare sul bordo dello schermo prima di innescare un'azione Avviatore di applicazioni Cambia il desktop quando il puntatore del mouse viene spinto sul bordo dello schermo smart2128@baslug.org, Blocca schermo Massimizza le finestre trascinandole al bordo superiore dello schermo Vincenzo Reale,Nicola Ruggero Nessuna azione Solo quando sposti le finestre Esegui comando Altre impostazioni Affiancatura a quattro attivata nel Mostra desktop Disabilitato Affianca le finestre trascinandole al bordo destro o sinistro dello schermo Attiva scambiafinestre alternativo Attiva scambiafinestre Per innescare un'azione spostando il puntatore del mouse contro il bordo o l'angolo dello schermo nella direzione dell'azione. Per innescare un'azione scivolano dal bordo dello schermo verso il centro Gestione finestra della parte più esterna dello schermo 