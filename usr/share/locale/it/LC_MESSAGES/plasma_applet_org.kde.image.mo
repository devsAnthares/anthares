��            )   �      �     �     �     �     �     �     �     �     �  0        3     G     ]     c     o     w     �  
   �     �     �     �     �     �               (     A     I     a     m  �  s     *     3     R     g     |     �     �     �  4   �     �     �     �                    7     O     ]     u     �     �     �     �     �     �     �          "     0                                            
                                                                                      	       %1 by %2 Add Custom Wallpaper Add Folder... Add Image... Background: Blur Centered Change every: Directory with the wallpaper to show slides from Download Wallpapers Get New Wallpapers... Hours Image Files Minutes Next Wallpaper Image Open Containing Folder Open Image Open Wallpaper Image Positioning: Recommended wallpaper file Remove wallpaper Restore wallpaper Scaled Scaled and Cropped Scaled, Keep Proportions Seconds Select Background Color Solid color Tiled Project-Id-Version: plasma_wallpaper_image
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:39+0100
PO-Revision-Date: 2017-11-26 21:27+0100
Last-Translator: Vincenzo Reale <smart2128@baslug.org>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 %1 di %2 Aggiungi sfondo personalizzato Aggiungi cartella... Aggiungi immagine... Sfondo: Sfuoca Centrata Cambia ogni: Cartella con gli sfondi da mostrare come diapositive Scarica sfondi Ottieni nuovi sfondi... Ore File di immagine Minuti Immagine di sfondo successiva Apri cartella superiore Apri immagine Apri immagine di sfondo Posizionamento: File di sfondo consigliato Rimuovi sfondo Ripristina sfondo Scalata Scalata e ritagliata Scalata, mantiene proporzioni Secondi Seleziona il colore di sfondo Colore solido Ripetuta 