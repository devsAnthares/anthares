��          �            x     y  
   �  
   �     �  .   �     �     �     �        *   )      T  ,   u  ,   �  0   �        �        �     �     �     �  ,   �          *     C  %   S  .   y  $   �  	   �  	   �  ;   �                             
                                            	           &Lock screen on resume: Activation Appearance Error Failed to successfully test the screen locker. Immediately Keyboard shortcut: Lock Session Lock screen automatically after: Lock screen when waking up from suspension Re&quire password after locking: Spinbox suffix. Short for minutes min  mins Spinbox suffix. Short for seconds sec  secs The global keyboard shortcut to lock the screen. Wallpaper &Type: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:10+0100
PO-Revision-Date: 2018-01-12 00:00+0100
Last-Translator: Vincenzo Reale <smart2128@baslug.org>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 B&locca lo schermo alla ripresa: Attivazione Aspetto Errore Prova della schermata di blocco non riuscita Immediatamente Scorciatoia da tastiera: Blocco sessione Blocco dello schermo automatico dopo: Blocca lo schermo al ritorno dalla sospensione Richiedi la password dopo il blocco:  min  min  sec  sec La scorciatoia da tastiera globale per bloccare lo schermo. &Tipo di sfondo: 