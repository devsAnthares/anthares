��    Q      �  m   ,      �     �     �     �                      .   !  U   P     �     �      �     �  /   �  /   -     ]     i     r  
   ~     �     �  $   �     �     �     �     �     	     	  	   *	     4	  
   F	  7   Q	     �	     �	     �	     �	  	   �	     �	  !   �	     
     
  	   !
      +
     L
     Y
     r
     �
     �
     �
     �
     �
     �
     �
  (   �
       =   '  2   e     �     �  "   �     �       J     1   Y  )   �  (   �  '   �  "     4   )     ^     l     r     {     �     �     �  U   �  N   !  9   p  J   �  �  �     �     �     �     �     �     �  
   �     �               ;  /   N     ~  D   �  D   �          %     3     A     T     r  2   �     �     �     �     �           	     &     6     L     d     h     �     �     �     �     �  +   �            
   .  &   9     `     m     �     �     �     �     �     �     �     �  :     $   T  
   y     �     �     �     �     �     �     �  !   �  2   �  /   '  1   W  0   �  J   �               !     .  	   B     L     R     Z     c     t  !   �        L   )             O                      4              6   $   ?   3         B              (   K   Q               ;       .   '   D   2   -       J   >   !   
   8                       P   5         ,          7   <           F      *   @       H   1                 "          9      +   &   0           /   N          #       E   	             I   A   =   %      G   :   C                   M    &All Desktops &Close &Fullscreen &Move &New Desktop &Pin &Shade 1 = number of desktop, 2 = desktop name&%1 %2 Activities a window is currently on (apart from the current one)Also available on %1 Add To Current Activity All Activities Allow this program to be grouped Alphabetically Always arrange tasks in columns of as many rows Always arrange tasks in rows of as many columns Arrangement Behavior By Activity By Desktop By Program Name Close Window or Group Cycle through tasks with mouse wheel Do Not Group Do Not Sort Filters Forget Recent Documents General Grouping and Sorting Grouping: Highlight windows Icon size: Invalid number of new messages, overlay, keep short— Keep &Above Others Keep &Below Others Keep launchers separate Large Ma&ximize Manually Mark applications that play audio Maximum columns: Maximum rows: Mi&nimize Minimize/Restore Window or Group More Actions Move &To Current Desktop Move To &Activity Move To &Desktop Mute New Instance On %1 On All Activities On The Current Activity On middle-click: Only group when the task manager is full Open groups in popups Open or bring to the front window of media player appRestore Over 9999 new messages, overlay, keep short9,999+ Pause playbackPause Play next trackNext Track Play previous trackPrevious Track Quit media player appQuit Re&size Remove launcher button for application shown while it is not runningUnpin Show all user Places%1 more Place %1 more Places Show only tasks from the current activity Show only tasks from the current desktop Show only tasks from the current screen Show only tasks that are minimized Show progress and status information in task buttons Show tooltips Small Sorting: Start New Instance Start playbackPlay Stop playbackStop The click actionNone Toggle action for showing a launcher button while the application is not running&Pin When clicking it would toggle grouping windows of a specific appGroup/Ungroup Which activities a window is currently onAvailable on %1 Which virtual desktop a window is currently onAvailable on all activities Project-Id-Version: plasma_applet_tasks
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2018-01-16 20:18+0100
Last-Translator: Vincenzo Reale <smart2128@baslug.org>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 Tutti &i desktop &Chiudi Scher&mo intero S&posta &Nuovo desktop A&ppunta Om&breggia &%1 %2 Disponibile anche su %1 Aggiungi all'attività attuale Tutte le attività Consenti il raggruppamento per questo programma Alfabeticamente Disponi sempre le attività in colonne con lo stesso numero di righe Disponi sempre le attività in righe con lo stesso numero di colonne Disposizione Comportamento Per attività In base al desktop in base al nome del programma Chiudi finestra o gruppo Usa la rotella del mouse per scorrere le attività Non raggruppare Non ordinare Filtri Dimentica i documenti recenti Generale Raggruppamento e ordinamento Raggruppamento: Evidenzia le finestre Dimensione delle icone: — M&antieni sopra le altre Mantieni so&tto le altre Mantieni i lanciatori separati Grande Ma&ssimizza Manualmente Marca le applicazioni che riproducono audio Num. massimo colonne: Num. massimo righe: Mi&nimizza Minimizza/Ripristina finestra o gruppo Altre azioni Sposta al desktop a&ttuale Sposta su &attività Sposta al &desktop Silenzia Nuova istanza Su %1 In tutte le attività Nell'attività attuale Al clic con il tasto centrale: Raggruppa solo quando la barra delle applicazioni è piena Apri i gruppi in finestre a comparsa Ripristina 9,999+ Pausa Traccia successiva Traccia precedente Esci Ridimen&siona Rimuovi %1 altra risorsa %1 altre risorse Mostra solo le applicazioni dell'attività attuale Mostra solo le applicazioni del desktop attuale Mostra solo le applicazioni dello schermo attuale Mostra solo le applicazioni che sono minimizzate Mostra le informazioni di stato e avanzamento nei pulsanti delle attività Mostra suggerimenti Piccola Ordinamento: Avvia nuova istanza Riproduci Ferma Nessuna A&ppunta Raggruppa/Separa Disponibile su %1 Disponibile su tutte le attività 