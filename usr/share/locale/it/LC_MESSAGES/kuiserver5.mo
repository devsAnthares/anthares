��          �   %   �      P     Q     b     w     �     �     �  
   �     �     �     �     �     �  /        3     Q     p     v     �     �     �     �     �     �     �       �  %     �     �     �     �          "     6     >     L     T     \     i  D   {     �     �     �  	   �  
          $     %   @  $   f  %   �     �     �                            
                                                                                             	    %1 file %1 files %1 folder %1 folders %1 of %2 processed %1 of %2 processed at %3/s %1 processed %1 processed at %2/s Appearance Behavior Cancel Clear Configure... Finished Jobs List of running file transfers/jobs (kuiserver) Move them to a different list Move them to a different list. Pause Remove them Remove them. Resume Show all jobs in a list Show all jobs in a list. Show all jobs in a tree Show all jobs in a tree. Show separate windows Show separate windows. Project-Id-Version: kuiserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-05-30 22:00+0200
Last-Translator: Federico Zenith <federico.zenith@member.fsf.org>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 %1 file %1 file %1 cartella %1 cartelle %1 di %2 elaborati %1 di %2 elaborati a %3/s %1 elaborati %1 elaborati a %2/s Aspetto Comportamento Annulla Pulisci Configura... Processi conclusi Elenco di processi e trasferimenti di file in esecuzione (kuiserver) Spostali in un'altra lista Spostali in un'altra lista. Pausa Rimuovili Rimuovili. Riprendi Mostra tutti i processi in una lista Mostra tutti i processi in una lista. Mostra tutti i processi in un albero Mostra tutti i processi in un albero. Mostra finestre separate Mostra finestre separate. 