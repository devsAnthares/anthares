��            )   �      �  ;   �  J   �          /  ~   7  A   �     �       )        G     X     i      q     �     �     �  
   �     �  
   �     �             "   <     _  	   y     �     �     �  �  �     U     ]     a     x  �   �  H        K     Y  0   r     �     �     �     �     �     	     	     -	     :	  
   V	     a	     r	     �	      �	     �	  	   �	     �	      
  
   
                                      
                                                                                            	        %1 is the full user name, %2 is the user login name%1 (%2) %1 is the name of a detail about the current action provided by polkit%1: (c) 2009 Red Hat, Inc. Action: An application is attempting to perform an action that requires privileges. Authentication is required to perform this action. Another client is already authenticating, please try again later. Application: Authentication Required Authentication failure, please try again. Click to edit %1 Click to open %1 Details EMAIL OF TRANSLATORSYour emails Former maintainer Jaroslav Reznik Lukáš Tinkl Maintainer NAME OF TRANSLATORSYour names P&assword: Password for %1: Password for root: Password or swipe finger for %1: Password or swipe finger for root: Password or swipe finger: Password: PolicyKit1 KDE Agent Select User Vendor: Project-Id-Version: polkit-kde-authentication-agent-1
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:22+0100
PO-Revision-Date: 2015-02-01 12:45+0100
Last-Translator: Pino Toscano <toscano.pino@tiscali.it>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 %1 (%2) %1: (c) 2009 Red Hat, Inc. Azione: Un'applicazione sta tentando di effettuare un'azione che richiede privilegi. Per compiere l'azione è richiesta l'autenticazione. Un altro client si sta già autenticando, per favore riprova più tardi. Applicazione: Autenticazione richiesta Autenticazione non riuscita, per favore riprova. Fai clic per modificare %1 Fai clic per aprire %1 Dettagli toscano.pino@tiscali.it, Responsabile precedente Jaroslav Reznik Lukáš Tinkl Responsabile Pino Toscano,Nicola Ruggero P&assword: Password per %1: Password per root: Password o sfiora dito per %1: Password o sfiora dito per root: Password o sfiora dito: Password: Agente KDE di PolicyKit1 Seleziona utente Fornitore: 