��          �      |      �     �  2        B     b       #   �      �     �  %   �       
   &     1     >     ]  �   }       ]   (     �  p   �  :        P  �  \  !   �  D     !   d     �  %   �  $   �     �       +     (   E     n     {     �  ,   �  �   �     �	  q   �	  (   
  �   F
  H   �
                                                                           
                	           Apply a look and feel package Command line tool to apply look and feel packages. Configure Look and Feel details Copyright 2017, Marco Martin Cursor Settings Changed Download New Look And Feel Packages EMAIL OF TRANSLATORSYour emails Get New Looks... List available Look and feel packages Look and feel tool Maintainer Marco Martin NAME OF TRANSLATORSYour names Reset the Plasma Desktop layout Select an overall theme for your workspace (including plasma theme, color scheme, mouse cursor, window and desktop switcher, splash screen, lock screen etc.) Show Preview This module lets you configure the look of the whole workspace with some ready to go presets. Use Desktop Layout from theme Warning: your Plasma Desktop layout will be lost and reset to the default layout provided by the selected theme. You have to restart KDE for cursor changes to take effect. packagename Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-27 05:46+0100
PO-Revision-Date: 2017-12-22 08:29+0100
Last-Translator: Vincenzo Reale <smart2128@baslug.org>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 Applica un pacchetto dell'aspetto Strumento da riga di comando per applicare i pacchetti dell'aspetto. Configura i dettagli dell'aspetto Copyright 2017, Marco Martin Impostazioni dei puntatori modificate Scarica nuovi pacchetti dell'aspetto smart2128@baslug.org Scarica nuovi temi... Elenca i pacchetti dell'aspetto disponibili Strumento di configurazione dell'aspetto Responsabile Marco Martin Vincenzo Reale Ripristina la disposizione di Plasma Desktop Seleziona un tema completo dell'aspetto per il tuo spazio di lavoro (inclusi tema di plasma, schema di colori, puntatori del mouse, scambiatore di finestre e desktop, schermata iniziale, schermata di blocco, ecc.) Mostra anteprima Questo modulo ti consente di configurare l'aspetto complessivo dello spazio di lavoro con alcune preimpostazioni. Usa la disposizione del desktop dal tema Avviso: la tua disposizione di Plasma Desktop sarà perso e ripristinata alla disposizione predefinita fornita dal tema selezionata. Devi riavviare KDE affinché le modifiche dei puntatori abbiano effetto. nomepacchetto 