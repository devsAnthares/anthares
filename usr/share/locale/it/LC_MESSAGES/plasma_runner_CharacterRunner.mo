��    
      l      �       �      �            	               .  I   3     }  	   �  �  �     7     O     a     g  '   n     �  F   �     �     �               
   	                           &Trigger word: Add Item Alias Alias: Character Runner Config Code Creates Characters from :q: if it is a hexadecimal code or defined alias. Delete Item Hex Code: Project-Id-Version: plasma_runner_CharacterRunner
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2010-08-06 08:29+0200
Last-Translator: Vincenzo Reale <smart2128@baslug.org>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Parola di a&ttivazione: Aggiungi elemento Alias Alias: Configurazione dell'esecutore caratteri Codice Crea caratteri da :q: se è un codice esadecimale o un alias definito. Elimina elemento Codice esadecimale: 