��            )   �      �     �     �     �     �     �     �     �  
           )   (     R     V     \     c     v     �     �     �  3   �     �       <   #  5   `  8   �  8   �                    /  �  1     �     
          1     F     Z     h          �  B   �  
   �     �  	   �  *     "   .     Q     i     z  7   �     �  -   �  R   	  6   Y	  B   �	  B   �	     
     
     8
     O
                                                  
                                                                               	        Add files... Add folder... Background frame Change picture every Choose a folder Choose files Configure plasmoid... Fill mode: General Left click image opens in external viewer Pad Paths Paths: Pause on mouseover Preserve aspect crop Preserve aspect fit Randomize items Stretch The image is duplicated horizontally and vertically The image is not transformed The image is scaled to fit The image is scaled uniformly to fill, cropping if necessary The image is scaled uniformly to fit without cropping The image is stretched horizontally and tiled vertically The image is stretched vertically and tiled horizontally Tile Tile horizontally Tile vertically s Project-Id-Version: plasma_applet_org.kde.plasma.mediaframe
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-16 03:31+0100
PO-Revision-Date: 2017-11-26 21:26+0100
Last-Translator: Vincenzo Reale <smart2128@baslug.org>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 Aggiungi file... Aggiungi cartella... Cornice di sfondo Cambia immagine ogni Scegli una cartella Scegli i file Configura plasmoide... Modalità di riempimento: Generale Il clic con il tasto sinistro apre l'immagine in un visore esterno Spaziatura Percorsi Percorsi: Pausa al passaggio del puntatore del mouse Ritaglio mantenendo le proporzioni Preserva le proporzioni Elementi casuali Allunga L'immagine è duplicata orizzontalmente e verticalmente L'immagine non è trasformata L'immagine viene ridimensionata per adattarsi L'immagine è riscalata in modo uniforme per adattarsi, con ritaglio se necessario L'immagine è riscalata in modo uniforme senza ritagli L'immagine è allungata orizzontalmente e affiancata verticalmente L'immagine è allungata verticalmente e affiancata orizzontalmente Affianca Affianca orizzontalmente Affianca verticalmente s 