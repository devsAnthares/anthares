��    2      �  C   <      H     I     M     R  .   a  5   �     �     �     �     �  	   �     �          '     6  1   J     |     �     �     �  
   �     �  '   �     �     �     �          !  '   (     P     _     f     n     �  5   �     �     �     �     �     �  $   �  A   #  �   e     K     R  C   c  '   �     �     �     �  �  	     �
     �
     �
     �
     �
     �
     �
             	   /     9     Y     r       J   �     �     �               /     B     b     o     r     {     �     �  ,   �     �     �                 )  2   0     c     x     �     �     �     �     �     �     �     �     �                  &                  $   0       .                      %                ,         )   2          /   "                        &   1      '       +   !   	         -                               
         (                    *            #       %1% Back Battery at %1% Button to change keyboard layoutSwitch layout Button to show/hide virtual keyboardVirtual Keyboard Cancel Caps Lock is on Close Close Search Configure Configure Search Plugins Desktop Session: %1 Different User Keyboard Layout: %1 Logging out in 1 second Logging out in %1 seconds Login Login Failed Login as different user Logout Next track No media playing Nobody logged in on that sessionUnused OK Password Play or Pause media Previous track Reboot Reboot in 1 second Reboot in %1 seconds Recent Queries Remove Restart Show media controls: Shutdown Shutting down in 1 second Shutting down in %1 seconds Start New Session Suspend Switch Switch Session Switch User Textfield placeholder textSearch... Textfield placeholder text, query specific KRunnerSearch '%1'... This is the first text the user sees while starting in the splash screen, should be translated as something short, is a form that can be seen on a product. Plasma is the project name so shouldn't be translated.Plasma made by KDE Unlock Unlocking failed User logged in on console (X display number)on TTY %1 (Display %2) User logged in on console numberTTY %1 Username Username (location)%1 (%2) in category recent queries Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-09 03:21+0100
PO-Revision-Date: 2018-02-10 07:43+0100
Last-Translator: Vincenzo Reale <smart2128@baslug.org>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 %1% Indietro Batteria al %1% Cambia disposizione Tastiera virtuale Annulla Il blocco maiuscole è attivo Chiudi Chiudi ricerca Configura Configura estensioni di ricerca Sessione del desktop: %1 Altro utente Mappatura della tastiera: %1 Chiusura della sessione in 1 secondo Chiusura della sessione in %1 secondi Accesso Accesso non riuscito Accedi con un altro utente Chiudi sessione Traccia successiva Nessun supporto in riproduzione Inutilizzata OK Password Riproduci o sospendi supporto Traccia precedente Riavvia Riavvia tra 1 secondo Riavvia tra %1 secondi Interrogazioni recenti Rimuovi Riavvia Mostra i controlli multimediali: Spegni Spegnimento in 1 secondo Spegnimento in %1 secondi Avvia nuova sessione Sospendi Cambia Cambia sessione Cambia utente Cerca... Cerca «%1»... Plasma creato da KDE Sblocca Sblocco non riuscito su TTY %1 (%2) TTY %1 Nome utente %1 (%2) nella categoria interrogazioni recenti 