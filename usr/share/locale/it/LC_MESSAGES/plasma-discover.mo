��    d      <  �   \      �     �     �     �     �     �  5   �  #   �  E   	  E   _	  >   �	     �	     �	  7   
     E
     _
     p
     �
     �
     �
     �
     �
     �
          	          4     B     W     i     n  	   u          �     �  !   �  F   �     #     @     Q  <   c     �     �  *   �     �      �            	        &     6  	   >     H     X     _      h     �  
   �     �     �     �     �  
   �  F     :   K     �     �     �     �     �     �  8   �     �       	     
   "     -     =     F     W     l     r     �     �     �     �     �     �     �  &   �     "  
   >     I     Y     y     �     �     �     �  $   �  �  �     �     �     �     �     �  ?   �  .     D   H  D   �  =   �          -  _   B     �     �  %   �  &   �           9     G     a     i     {     �     �     �     �     �     �     �  
              -     C  ,   Y  E   �  '   �     �     
  D        a     h  +   o     �  *   �     �     �     �     �       
        )     9     ?  "   H      k     �  &   �     �     �     �        e     @   {     �     �     �     �  
   �     �  A        N     T     g     p     |     �     �     �     �  )   �     �          %  
   @     K  	   j     t     �  "   �     �     �     �     �  	   �     �          /  -   I         J   `   :       	   W                 [   .   E         )      (   R   M   '   O   $       7   Y   4                -                    9       8   #   S       0   P       B      
   +                         L   >              N   @      *   D      F   /   ;       5       2   ^      U               ?   6       <   V   ,       c       \   Z      G   1                  d       C   Q   &       A          _   b       =   T      !   3             a   K           ]              H      %      I   X          "    
Also available in %1 %1 %1 (%2) %1 (Default) <b>%1</b> by %2 <em>%1 out of %2 people found this review useful</em> <em>Tell us about this review!</em> <em>Useful? <a href='true'><b>Yes</b></a>/<a href='false'>No</a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'><b>No</b></a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'>No</a></em> @infoFetching updates @infoFetching... @infoIt is unknown when the last check for updates was @infoLooking for updates @infoNo updates @infoNo updates are available @infoShould check for updates @infoThe system is up to date @infoUpdates @infoUpdating... Accept Add Source... Addons Aleix Pol Gonzalez An application explorer Apply Changes Available backends:
 Available modes:
 Back Cancel Category: Checking for updates... Comment too long Comment too short Compact Mode (auto/compact/full). Could not close the application, there are tasks that need to be done. Could not find category '%1' Couldn't open %1 Delete the origin Directly open the specified application by its package name. Discard Discover Display a list of entries with a category. Extensions... Failed to remove the source '%1' Featured Help... Homepage: Improve summary Install Installed Jonathan Thomas Launch License: List all the available backends. List all the available modes. Loading... Local package file to install Make default More Information... More... No Updates Open Discover in a said mode. Modes correspond to the toolbar buttons. Open with a program that can deal with the given mimetype. Proceed Rating: Remove Resources for '%1' Review Reviewing '%1' Running as <em>root</em> is discouraged and unnecessary. Search Search in '%1'... Search... Search: %1 Search: %1 + %2 Settings Short summary... Show reviews (%1)... Size: Sorry, nothing found... Source: Specify the new source for %1 Still looking... Summary: Supports appstream: url scheme Tasks Tasks (%1%) TransactioName TransactionStatus%1 %2 Unable to find resource: %1 Update All Update Selected Update section nameUpdate (%1) Updates Version: unknown reviewer updates not selected updates selected © 2010-2016 Plasma Development Team Project-Id-Version: muon-discover
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:02+0100
PO-Revision-Date: 2018-01-16 20:20+0100
Last-Translator: Vincenzo Reale <smart2128@baslug.org>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 
Disponibile anche su %1 %1 %1 (%2) %1 (Predefinito) <b>%1</b> di %2 <em>%1 persone su %2 hanno trovato utile questa recensione</em> <em>Fai un commento su questa recensione!</em> <em>Utile? <a href='true'><b>Sì</b></a>/<a href='false'>No</a></em> <em>Utile? <a href='true'>Sì</a>/<a href='false'><b>No</b></a></em> <em>Utile? <a href='true'>Sì</a>/<a href='false'>No</a></em> Recupero degli aggiornamenti Recupero in corso... Non è noto quando l'ultima verifica della disponibilità di aggiornamenti sia stata effettuata Ricerca degli aggiornamenti Nessun aggiornamento Non ci sono aggiornamenti disponibili Dovresti controllare gli aggiornamenti Il sistema è aggiornato Aggiornamenti Aggiornamento in corso... Accetta Aggiungi fonte... Aggiunte Aleix Pol Gonzalez Esploratore di applicazioni Applica le modifiche Motori disponibili:
 Modi disponibili:
 Indietro Annulla Categoria: Controllo degli aggiornamenti... Commento troppo lungo Commento troppo breve Modalità compatta (auto/compatta/completa). Impossibile chiudere l'applicazione, ci sono attività da completare. Impossibile trovare la categoria «%1» Impossibile aprire %1 Elimina l'origine Apri direttamente l'applicazione selezionata per nome del pacchetto. Scarta Scopri Mostra un elenco di voci con una categoria. Estensioni... Rimozione della fonte non riuscita: «%1» In primo piano Aiuto... Pagina principale: Migliora il riepilogo Installa Installato Jonathan Thomas Avvia Licenza: Elenca tutti i motori disponibili. Elenca tutti i modi disponibili. Caricamento in corso... File di pacchetto locale da installare Rendi predefinito Ulteriori informazioni... Altro... Nessun aggiornamento Apri Discover in uno dei modi indicati. I modi corrispondono ai pulsanti della barra degli strumenti. Apri con un programma in grado di gestire il tipo mime indicato. Procedi Valutazione: Rimuovi Risorse per «%1» Recensisci Recensione di «%1» L'esecuzione come <em>root</em> è sconsigliata e non necessaria. Cerca Cerca in «%1»... Cerca... Cerca: %1» Cerca: %1 + %2 Impostazioni Riepilogo breve... Mostra le recensioni (%1)... Dimensione: Spiacenti, non è stato trovato niente... Fonte: Specifica la nuova fonte per %1 Ricerca ancora in corso... Riepilogo: Supporta appstream: url scheme Attività Attività (%1%) %1 %2 Impossibile trovare la risorsa: %1 Aggiorna tutto Aggiorna selezionati Aggiornamento (%1) Aggiornamenti Versione: revisore sconosciuto aggiornamenti non selezionati aggiornamenti selezionati © 2010-2016 La squadra di sviluppo di Plasma 