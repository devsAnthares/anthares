��          �      <      �     �  <   �  
   �     	          &     3     ?  
   G     R     c     q     }     �     �  
   �     �  �  �     �  X   �     �               )     E     V     _     q     �     �     �     �     �     �                                                        
   	                                                Add Launcher... Add launchers by Drag and Drop or by using the context menu. Appearance Arrangement Edit Launcher... Enable popup Enter title General Hide icons Maximum columns: Maximum rows: Quicklaunch Remove Launcher Show hidden icons Show launcher names Show title Title Project-Id-Version: plasma_applet_org.kde.plasma.quicklaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-02-26 04:09+0100
PO-Revision-Date: 2016-01-23 20:29+0100
Last-Translator: Vincenzo Reale <smart2128@baslug.org>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 Aggiungi lanciatore... Aggiungi i lanciatori con il trascinamento e rilascio o utilizzando il menu contestuale. Aspetto Disposizione Modifica lanciatore... Abilita finestra a comparsa Digita il titolo Generale Nascondi le icone Num. massimo colonne: Num. massimo righe: Avvio rapido Rimuovi lanciatore Mostra le icone nascoste Mostra nomi dei lanciatori Mostra il titolo Titolo 