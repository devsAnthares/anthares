��          �   %   �      P  �  Q          )  6   9  -   p     �     �     �     �  (   �  d        x     �     �     �     �     �     �                7  "   X     {     �     �  �  �  �  x     C
     \
  3   n
  ?   �
     �
     �
            ,   ,  f   Y     �     �     �     �     �          "     2     A     I     Q     X     f     m                               	                                                                      
                        <p>You have chosen to open another desktop session.<br />The current session will be hidden and a new login screen will be displayed.<br />An F-key is assigned to each session; F%1 is usually assigned to the first session, F%2 to the second session and so on. You can switch between sessions by pressing Ctrl, Alt and the appropriate F-key at the same time. Additionally, the KDE Panel and Desktop menus have actions for switching between sessions.</p> Lists all sessions Lock the screen Locks the current sessions and starts the screen saver Logs out, exiting the current desktop session New Session Reboots the computer Restart the computer Shutdown the computer Starts a new session as a different user Switches to the active session for the user :q:, or lists all active sessions if :q: is not provided Turns off the computer User sessionssessions Warning - New Session lock screen commandlock log out log out commandLogout log out commandlogout new session restart computer commandreboot restart computer commandrestart shutdown computer commandshutdown switch user switch user commandswitch switch user commandswitch :q: Project-Id-Version: plasma_runner_sessions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2014-12-13 10:00+0100
Last-Translator: Vincenzo Reale <smart2128@baslug.org>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 1.5
 <p>Hai scelto di aprire una nuova sessione di desktop.<br />La sessione attuale sarà nascosta e sarà visualizzata una nuova schermata d'accesso.<br />A ogni sessione viene assegnato un tasto F: di solito F%1 è assegnato alla prima sessione, F%2 alla seconda e così via. Puoi cambiare sessione premendo contemporaneamente Ctrl, Alt e il tasto F appropriato. Inoltre, il pannello di KDE e i menu del desktop hanno delle azioni per cambiare la sessione.</p> Elenca tutte le sessioni Blocca lo schermo Blocca le sessioni correnti e avvia il salvaschermo Chiude la sessione, uscendo dalla sessione corrente del desktop Nuova sessione Riavvia il computer Riavvia il computer Spegni il computer Avvia una nuova sessione con un altro utente Passa alla sessione attiva dell'utente :q:, o elenca tutte le sessioni attive se :q: non viene fornito Spegne il computer sessioni Attenzione - Nuova sessione Blocca Chiudi la sessione Chiudi sessione Chiudi sessione Nuova sessione Riavvia Riavvia Spegni Cambia utente Cambia passa a :q: 