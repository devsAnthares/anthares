��    	      d      �       �      �      �   -        <  -   V  #   �  #   �     �  �  �     �     �  1   �     �  /        @     E     I                                   	           Current time is %1 Displays the current date Displays the current date in a given timezone Displays the current time Displays the current time in a given timezone Note this is a KRunner keyworddate Note this is a KRunner keywordtime Today's date is %1 Project-Id-Version: plasma_runner_datetime
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2015-02-01 22:51+0100
Last-Translator: Vincenzo Reale <smart2128@baslug.org>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 1.5
 L'ora attuale è %1 Visualizza la data attuale Visualizza la data attuale in un dato fuso orario Visualizza l'ora attuale Visualizza l'ora attuale in un dato fuso orario data ora La data odierna è %1 