��          �      �       H     I     a      m     �     �     �  
   �     �  &   �          .     L  1   b  �  �     G     e     q     �     �     �  	   �     �  '   �  $   �     #      B  =   c         	                                    
                    Configure Desktop Theme David Rosca EMAIL OF TRANSLATORSYour emails Get New Themes... Install from File... NAME OF TRANSLATORSYour names Open Theme Remove Theme Theme Files (*.zip *.tar.gz *.tar.bz2) Theme installation failed. Theme installed successfully. Theme removal failed. This module lets you configure the desktop theme. Project-Id-Version: kcm_desktopthemedetails
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-22 05:21+0100
PO-Revision-Date: 2017-10-20 15:52+0100
Last-Translator: Paolo Zamponi <zapaolo@email.it>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 Configura il tema del desktop David Rosca smart2128@baslug.org Ottieni nuovi temi... Installa da file... Vincenzo Reale Apri tema Rimuovi tema File di tema (*.zip *.tar.gz *.tar.bz2) Installazione del tema non riuscita. Tema installato correttamente. Rimozione del tema non riuscita. Questo modulo ti permette di configurare il tema del desktop. 