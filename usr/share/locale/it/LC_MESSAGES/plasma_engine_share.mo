��    
      l      �       �   $   �   %     :   <     w  '   �  -   �     �       '     �  <  %   �  /     @   7  *   x  .   �  2   �          %  +   8         	                            
        Could not detect the file's mimetype Could not find all required functions Could not find the provider with the specified destination Error trying to execute script Invalid path for the requested provider It was not possible to read the selected file Service was not available Unknown Error You must specify a URL for this service Project-Id-Version: plasma_engine_share
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2011-01-06 07:40+0100
Last-Translator: Federico Zenith <federico.zenith@member.fsf.org>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Impossibile rilevare il tipo del file Impossibile trovare tutte le funzioni richieste Impossibile trovare il fornitore con la destinazione specificata Errore nel tentativo di eseguire lo script Percorso non valido per il fornitore richiesto Non è stato possibile leggere il file selezionato Il servizio non era disponibile Errore sconosciuto Devi specificare un URL per questo servizio 