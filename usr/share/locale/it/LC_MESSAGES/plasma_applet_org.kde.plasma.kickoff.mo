��            )   �      �     �  
   �  /   �  -   �          !  
   2     =     J     `  W   i     �  ,   �  	                  !     '     -  
   :     E     W     o     �     �     �     �  1   �       �       �  
   �  	   �     �     �          '     /     <     U  i   ^     �  9   �  	        %  
   5     @     G     L     ^     p     �     �     �     �     �  $   �     	     5	                                                                                  
                                	                       %1@%2 %2@%3 (%1) @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Favorites All Applications Appearance Applications Applications updated. Computer Drag tabs between the boxes to show/hide them, or reorder the visible tabs by dragging. Edit Applications... Expand search to bookmarks, files and emails Favorites Hidden Tabs History Icon: Leave Menu Buttons Often Used On All Activities On The Current Activity Remove from Favorites Show In Favorites Show applications by name Sort alphabetically Switch tabs on hover Type is a verb here, not a nounType to search... Visible Tabs Project-Id-Version: plasma_applet_kickoff
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2017-09-09 17:55+0100
Last-Translator: Vincenzo Reale <smart2128@baslug.org>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 %1@%2 %2@%3 (%1) Scegli... Ripristina icona Aggiungi ai preferiti Tutte le applicazioni Aspetto Applicazioni Applicazioni aggiornate. Computer Trascina le schede tra i riquadri per mostrarle/nasconderle, o riordina le schede visibili trascinandole. Modifica applicazioni... Espandi la ricerca a segnalibri, file e messaggi di posta Preferiti Schede nascoste Cronologia Icona: Esci Pulsanti dei menu Utilizzati spesso In tutte le attività Nell'attività attuale Rimuovi dai preferiti Mostra nei preferiti Mostra applicazioni per nome In ordine alfabetico Cambia scheda al passaggio del mouse Digita per cercare... Schede visibili 