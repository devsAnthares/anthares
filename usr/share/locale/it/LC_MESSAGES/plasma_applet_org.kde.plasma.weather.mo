��    O      �  k         �     �  ^   �  H     
   f     q     �     �     �     �  '   �     �  "   �          4     K  
   T     _     x     �  	   �     �     �     �     �     �     �  "   �     	      	  	   8	     B	  !   I	     k	  !   �	  ?   �	     �	     �	     �	     
     
     (
     <
  ;   H
  &   �
      �
  %   �
     �
          &     ?  >   X     �     �  '   �  +   �  !   !     C     c     t     �     �     �     �     �     �     �               +     >     P     b     s     �     �     �     �  3   �  �       �     �     �     �     �                         (     ;     K     [     g     �  
   �     �     �     �  
   �  
   �     �     �     �            )        D     K  
   M     X  '   ^     �  ,   �  	   �     �     �     �     �     �          	          '     6     I     ^     j     r     y     �     �     �     �     �     �     �     �     �     �     �     �                                                              $     (     1     7     S     /       ?       $         K   M   !   )      @                                 2   E       '               1          6              O   <      H   J      8       7      >       =   0   *          .      ,   :      N   D   G   C   3   	   #   +   4                F          %   A                   5       &   ;              9   
           -                      (   "   L      B   I             min %1 is the weather condition, %2 is the temperature,  both come from the weather provider%1 %2 A weather station location and the weather service it comes from%1 (%2) Appearance Beaufort scale bft Celsius °C Degree, unit symbol° Details Fahrenheit °F Forecast period timeframe1 Day %1 Days Hectopascals hPa High & Low temperatureH: %1 L: %2 High temperatureHigh: %1 Inches of Mercury inHg Kelvin K Kilometers Kilometers per Hour km/h Kilopascals kPa Knots kt Location: Low temperatureLow: %1 Meters per Second m/s Miles Miles per Hour mph Millibars mbar N/A No weather stations found for '%1' Notices Percent, measure unit% Pressure: Search Select weather services providers Short for no data available- Show temperature in compact mode: Shown when you have not set a weather providerPlease Configure Temperature: Units Update every: Visibility: Weather Station Wind conditionCalm Wind speed: certain weather condition (probability percentage)%1 (%2%) content of water in airHumidity: %1%2 distance, unitVisibility: %1 %2 ground temperature, unitDewpoint: %1 humidex, unitHumidex: %1 pressure tendencyfalling pressure tendencyrising pressure tendencysteady pressure tendency, rising/falling/steadyPressure Tendency: %1 pressure, unitPressure: %1 %2 temperature, unit%1%2 visibility from distanceVisibility: %1 weather services provider name (id)%1 (%2) weather warningsWarnings Issued: weather watchesWatches Issued: wind directionE wind directionENE wind directionESE wind directionN wind directionNE wind directionNNE wind directionNNW wind directionNW wind directionS wind directionSE wind directionSSE wind directionSSW wind directionSW wind directionVR wind directionW wind directionWNW wind directionWSW wind direction, speed%1 %2 %3 wind speedCalm windchill, unitWindchill: %1 winds exceeding wind speed brieflyWind Gust: %1 %2 Project-Id-Version: plasma_applet_weather
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-28 05:50+0100
PO-Revision-Date: 2018-01-16 20:19+0100
Last-Translator: Vincenzo Reale <smart2128@baslug.org>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
  min %1 %2 %1 (%2) Aspetto Scala di Beaufort bft Celsius °C ° Dettagli Fahrenheit °F 1 giorno %1 giorni Hectopascal hPa Max: %1 Min: %2 Massima: %1 Pollici di mercurio inHg Kelvin K Chilometri Chilometri all'ora km/h Kilopascal kPa Nodi kt Località: Minima: %1 Metri al secondo m/s Miglia Miglia all'ora mph Millibar mbar N/D Nessuna stazione meteo trovata per «%1» Avvisi % Pressione: Cerca Seleziona i fornitori dei servizi meteo - Mostra la temperatura in modalità compatta: Configura Temperatura: Unità Aggiornamento ogni: Visibilità: Stazione meteo Calma Velocità del vento: %1 (%2%) Umidità: %1%2 Visibilità: %1 %2 Punto di rugiada: %1 Humidex: %1 ribasso rialzo stabile Tendenza della pressione: %1 Pressione: %1 %2 %1%2 Visibilità: %1 %1 (%2) Avvertimenti emessi: Allerte emesse: E ENE ESE N NE NNE NNO NO S SE SSE SSO SO VR O ONO OSO %1 %2 %3 Calma Raffreddamento da vento: %1 Raffica: %1 %2 