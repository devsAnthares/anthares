��          �   %   �      @     A  !   Y      {     �      �     �     �  +        =     N     [  (   z     �  ,   �  7   �     !  7   :     r  ]   �  1   �  	   "     ,  "   ?  5   b  �  �     4  *   S  (   ~     �     �  "   �     �  4        P     b     o  2   }  ,   �  6   �  =   	     R	  ?   i	  #   �	  q   �	  1   ?
     q
     }
  (   �
  =   �
                    
         	                                                                                                 Cannot start initramfs. Cannot start update-alternatives. Configure Plymouth Splash Screen Download New Splash Screens EMAIL OF TRANSLATORSYour emails Get New Boot Splash Screens... Initramfs failed to run. Initramfs returned with error condition %1. Install a theme. Marco Martin NAME OF TRANSLATORSYour names No theme specified in helper parameters. Plymouth theme installer Select a global splash screen for the system The theme to install, must be an existing archive file. Theme %1 does not exist. Theme corrupted: .plymouth file not found inside theme. Theme folder %1 does not exist. This module lets you configure the look of the whole workspace with some ready to go presets. Unable to authenticate/execute the action: %1, %2 Uninstall Uninstall a theme. update-alternatives failed to run. update-alternatives returned with error condition %1. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-28 03:05+0200
PO-Revision-Date: 2017-05-09 09:59+0100
Last-Translator: Paolo Zamponi <zapaolo@email.it>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 Impossibile avviare initramfs. Non riesco ad avviare update-alternatives. Configura la schermata di avvio Plymouth Scarica nuove schermate d'avvio zapaolo@email.it Ottieni nuove schermate d'avvio... Impossibile eseguire Initramfs. Initramfs restituito con la condizione di errore %1. Installa un tema. Marco Martin Paolo Zamponi Nessun tema specificato nei parametri di supporto. Programma di installazione del tema Plymouth Seleziona una schermata d'avvio globale per il sistema Il tema da installare deve essere un file archivio esistente. Il tema %1 non esiste. Tema corrotto: file .plymouth non trovato all'interno del tema. La cartella del tema %1 non esiste. Questo modulo ti consente di configurare l'aspetto complessivo dello spazio di lavoro con alcune preimpostazioni. Impossibile autenticare/eseguire l'azione: %1, %2 Disinstalla Disinstalla un tema. Impossibile avviare update-alternatives. update-alternatives ha restituito la condizione di errore %1. 