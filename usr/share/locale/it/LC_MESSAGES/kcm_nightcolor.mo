��          �      �           	               4  	   I     S      c  "   �      �     �     �  	   �     �               )  
   9     D     S     a     g     {  �  �           #     +     J  
   d     o     �  (   �  .   �  
   �  	   �     �     
       !   (     J  
   _     j     x     �     �  	   �                              	                                     
                                      K (HH:MM) (In minutes - min. 1, max. 600) Activate Night Color Automatic Detect location EMAIL OF TRANSLATORSYour emails Error: Morning not before evening. Error: Transition time overlaps. Latitude Location Longitude NAME OF TRANSLATORSYour names Night Color Night Color Temperature:  Operation mode: Roman Gilg Sunrise begins Sunset begins Times Transition duration and ends Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-12 03:09+0100
PO-Revision-Date: 2018-01-19 09:37+0100
Last-Translator: Paolo Zamponi <zapaolo@email.it>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
  K (OO:MM) (In minuti - min. 1, max. 600) Attiva il colore notturno Automatica Rileva posizione zapaolo@email.it Errore: la mattina non prima della sera. Errore: il tempo di transizione si sovrappone. Latitudine Posizione Longitudine Paolo Zamponi Colore notturno Temperatura del colore notturno:  Modalità operativa: Roman Gilg L'alba inizia Il tramonto inizia Orari Durata della transizione e termina 