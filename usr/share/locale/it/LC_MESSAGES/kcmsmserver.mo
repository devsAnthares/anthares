��          �      <      �     �     �     �  7  �  �  #  +   �  ^              �     �  x   �  �   %     �               ;     U  �  r     	     -	     B	  
  V	    a
  *   u  b   �               4  �   =  �   �      �     �  +   �  "        8                          
       	                                                                    &End current session &Restart computer &Turn off computer <h1>Session Manager</h1> You can configure the session manager here. This includes options such as whether or not the session exit (logout) should be confirmed, whether the session should be restored again when logging in and whether the computer should be automatically shut down after session exit by default. <ul>
<li><b>Restore previous session:</b> Will save all applications running on exit and restore them when they next start up</li>
<li><b>Restore manually saved session: </b> Allows the session to be saved at any time via "Save Session" in the K-Menu. This means the currently started applications will reappear when they next start up.</li>
<li><b>Start with an empty session:</b> Do not save anything. Will come up with an empty desktop on next start.</li>
</ul> Applications to be e&xcluded from sessions: Check this option if you want the session manager to display a logout confirmation dialog box. Conf&irm logout Default Leave Option General Here you can choose what should happen by default when you log out. This only has meaning, if you logged in through KDM. Here you can enter a colon or comma separated list of applications that should not be saved in sessions, and therefore will not be started when restoring a session. For example 'xterm:konsole' or 'xterm,konsole'. O&ffer shutdown options On Login Restore &manually saved session Restore &previous session Start with an empty &session Project-Id-Version: kcmsmserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-01-29 08:48+0100
Last-Translator: Federico Zenith <federico.zenith@member.fsf.org>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 &Termina la sessione corrente &Riavvia il computer &Spegni il computer <h1>Gestione sessione</h1>Qui puoi configurare il gestore delle sessioni. Queste sono alcune opzioni: confermare o no l'uscita dalla sessione (logout), se la sessione deve essere ripristinata al successivo accesso e se il computer debba essere spento quando si esce. <ul>
<li><b> Ripristina la precedente sessione:</b> salva tutte le applicazioni in esecuzione all'uscita e le ripristina all'avvio successivo.</li>
<li><b>Ripristina la sessione salvata manualmente:</b> permette di salvare la sessione in un qualunque momento con la voce «Salva sessione» del menu K. Questo significa che le applicazioni avviate al momento del salvataggio saranno riavviate all'avvio successivo.</li>
<li><b>Inizia con una sessione vuota:</b> non salva niente. Al prossimo avvio il desktop sarà vuoto.</li>
</ul> Applicazioni da escl&udere dalle sessioni: Marca questa opzione se vuoi che il gestore delle sessioni chieda conferma al momento dell'uscita. Conferma l'&uscita Opzione di uscita predefinita Generale Qui puoi scegliere cosa deve succedere (come azione predefinita) quando si chiude la sessione. Questo ha senso solo se accedi tramite KDM. Qui puoi inserire un elenco di applicazioni, separate da due punti o virgole, che non devono essere salvate nelle sessioni e che quindi non saranno riavviate quando si ripristina una sessione. Ad esempio «xterm:konsole» o «xterm,xconsole». O&ffri le opzioni di spegnimento All'accesso Ripristina una sessione salvata manualmente Ripristina la sessione &precedente Avvia una &sessione vuota 