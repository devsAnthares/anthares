��    `        �         (     )     1     B     Q     W     ^     j     {     �     �  /   �  -   �     �     �  
   		  
   	     	     ,	     1	     8	     @	     R	     _	     d	  
   l	     w	     �	     �	     �	  	   �	     �	  	   �	     �	     �	     �	     
     
  	   #
     -
     4
     H
  	   M
     W
     ]
     c
     h
     m
  	   v
     �
     �
     �
     �
     �
     �
     �
     �
  <   �
               /     6     =     X     ^     e     j  
   ~     �     �     �     �     �  )   �               5     :     @     M     U     ]     f  
   x     �     �  	   �     �     �     �     �     �     �     �  E   �  *   A  �  l               )     2     8     A     M     b     u  	   ~  	   �     �     �     �  
   �  
   �     �     �     �     �     �                    (     4     @     R  #   k     �     �     �     �     �  /   �     	          -     ?     H     e     l     �     �     �     �  	   �  
   �     �     �     �     �     �     �             I        `     x     �     �  
   �     �     �     �     �     �     �     �          )     D  -   X     �      �  
   �     �     �  
   �  
   �     �               -     5     =     J     V     [     {      �     �     �  P   �     $     J       -      M       %   Q           ^              U       L   S   $      .       H       X          Y   8   *         <   &          '       ]   N       3                     `          #   V             /   T   G   9   @   >   =   6   2   O   C   F   ?      0      
      A   E       	      P              _                D          Z               1   R   ,   +                \           I   )   B   K      :   !   W   4          ;               (      5   "       7       [           &Delete &Empty Trash Bin &Move to Trash &Open &Paste &Properties &Refresh Desktop &Refresh View &Reload &Rename @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Align Appearance: Arrange In Arrange in Arrangement: Back Cancel Columns Configure Desktop Custom title Date Default Descending Description Deselect All Desktop Layout Enter custom title here Features: File name pattern: File type File types: Filter Folder preview popups Folders First Folders first Full path Got it Hide Files Matching Huge Icon Size Icons Large Left List Location Location: Lock in place Locked Medium More Preview Options... Name None OK Panel button: Press and hold widgets to move them and reveal their handles Preview Plugins Preview thumbnails Remove Resize Restore from trashRestore Right Rotate Rows Search file type... Select All Select Folder Selection markers Show All Files Show Files Matching Show a place: Show files linked to the current activity Show the Desktop folder Show the desktop toolbox Size Small Small Medium Sort By Sort by Sorting: Specify a folder: Text lines Tiny Title: Tool tips Tweaks Type Type a path or a URL here Unsorted Use a custom icon Widget Handling Widgets unlocked You can press and hold widgets to move them and reveal their handles. whether to use icon or list viewView mode Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:11+0100
PO-Revision-Date: 2017-11-26 21:26+0100
Last-Translator: Vincenzo Reale <smart2128@baslug.org>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 &Elimina Svuota il c&estino &Cestina &Apri &Incolla &Proprietà Aggior&na il desktop Aggio&rna la vista Ricarica &Rinomina Scegli... Cancella icona Allinea Aspetto: Disponi in Disponi in Disposizione: Indietro Annulla Colonne Configura desktop Titolo personalizzato Data Predefinito Decrescente Descrizione Deseleziona tutto Disposizione del desktop Digita qui un titolo personalizzato Funzionalità: Schema di nome del file: Tipo di file Tipi di file: Filtro Finestre a comparsa di anteprima delle cartelle Prima le cartelle Prima le cartelle Percorso completo Ricevuto Nascondi file corrispondenti Enorme Dimensioni delle icone Icone Grande Sinistra Elenco Posizione Posizione: Blocca sul posto Bloccato Media Altre opzioni di anteprima... Nome Nessuno OK Pulsante pannello: Tieni premuto sugli oggetti per spostarli e far apparire le loro maniglie Estensioni di anteprima Miniature di anteprima Rimuovi Ridimensiona Ripristina Destra Ruota Righe Cerca il tipo di file... Seleziona tutto Seleziona cartella Marcatori di selezione Mostra tutti i file Mostra file corrispondenti Mostra una risorsa: Mostra i file collegati all'attività attuale Mostra la cartella Desktop Mostra gli strumenti del desktop Dimensione Piccola Medio piccola Ordina per Ordina per Ordinamento: Specifica una cartella: Righe di testo Piccola Titolo: Suggerimenti Regolazioni Tipo Digita qui un percorso o un URL Nessun ordine Utilizza un'icona personalizzata Gestione degli oggetti Oggetti sbloccati Puoi tenere premuto sugli oggetti per spostarli e far apparire le loro maniglie. Modalità di visualizzazione 