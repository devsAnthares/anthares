��    _                   C   	  /   M  -   }     �     �     �     �      	     	     1	     >	     J	  
   S	     ^	     g	     p	     �	  	   �	     �	     �	     �	  ,   �	  	    
     

  
   )
     4
     L
     `
     u
     �
     �
     �
     �
     �
  	   �
     �
     �
     
               !     (  	   ;     E     ]  
   r     }     �  
   �     �     �     �  
   �     �     �               $     2     @     R     h     y     �     �     �     �  	   �     �     �     �               4     Q     j     �     �     �     �  	   �     �  ,   �          !     0     @     L     S     b     t     �  #   �     �  �  �     �  	   �     �     �     �     �  *        ,     B     J     W     p  	   ~     �     �     �     �  	   �     �     �     �  9     	   H  (   R     {     �     �     �     �     �          "     >     G     O     [     g     }     �     �     �     �     �     �     �               -  	   D  %   N     t     |  
   �     �     �     �     �     �     �          %     ;     O     a     �     �     �     �     �     �  "   �     �       %   !  #   G  "   k     �     �     �     �     �     �  1   �     0     <     L     ^     l     t     �     �  (   �  2   �     	         Q         Y   5           %       H       J   !              \   *       @   V   4   [       A   (   
       U   B   ]   ^       $   _         '   >       D   K      -                  ?      ,   O   0       R   8   6   W   2   I   =   X   E   #       N   C       T   G   M          3   "         9             +          S          F           ;   :                    &   <   L          P         7          .       /       1                     )            	   Z       @action opens a software center with the applicationManage '%1'... @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Desktop Add to Favorites Add to Panel (Widget) Align search results to bottom All Applications App name (Generic name)%1 (%2) Applications Apps & Docs Behavior Categories Computer Contacts Description (Name) Description only Documents Edit Application... Edit Applications... End session Expand search to bookmarks, files and emails Favorites Flatten menu to a single level Forget All Forget All Applications Forget All Contacts Forget All Documents Forget Application Forget Contact Forget Document Forget Recent Documents General Generic name (App name)%1 (%2) Hibernate Hide %1 Hide Application Icon: Lock Lock screen Logout Name (Description) Name only Often Used Applications Often Used Documents Often used On All Activities On The Current Activity Open with: Pin to Task Manager Places Power / Session Properties Reboot Recent Applications Recent Contacts Recent Documents Recently Used Recently used Removable Storage Remove from Favorites Restart computer Run Command... Run a command or a search query Save Session Search Search results Search... Searching for '%1' Session Show Contact Information... Show In Favorites Show applications as: Show often used applications Show often used contacts Show often used documents Show recent applications Show recent contacts Show recent documents Show: Shut Down Sort alphabetically Start a parallel session as a different user Suspend Suspend to RAM Suspend to disk Switch User System System actions Turn off computer Type to search. Unhide Applications in '%1' Unhide Applications in this Submenu Widgets Project-Id-Version: plasma_applet_org.kde.homerun
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2017-09-09 17:54+0100
Last-Translator: Vincenzo Reale <smart2128@baslug.org>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 Gestisci «%1»... Scegli... Ripristina icona Aggiungi al desktop Aggiungi ai preferiti Aggiungi al pannello (Oggetto) Allinea i risultati della ricerca in basso Tutte le applicazioni %1 (%2) Applicazioni Applicazioni e documenti Comportamento Categorie Computer Contatti Descrizione (Nome) Solo la descrizione Documenti Modifica applicazione... Modifica applicazioni... Termina sessione Espandi la ricerca a segnalibri, file e messaggi di posta Preferiti Appiattisci il menu a un singolo livello Dimenticare tutto Dimentica tutte le applicazioni Dimentica tutti i contatti Dimentica tutti i documenti Dimentica applicazione Dimentica contatto Dimentica documento Dimentica documenti recenti Generale %1 (%2) Ibernazione Nascondi %1 Nascondi applicazioni Icona: Blocca Blocca schermo Chiudi sessione Nome (Descrizione) Solo il nome Applicazioni utilizzate spesso Documenti utilizzati spesso Utilizzati spesso In tutte le attività Nell'attività attuale Apri con: Appunta al gestore delle applicazioni Risorse Alimentazione / Sessione Proprietà Riavvia Applicazioni recenti Contatti recenti Documenti recenti Utilizzati di recente Utilizzati di recente Archiviazione rimovibile Rimuovi dai preferiti Riavvia il computer Esegui comando... Esegui un comando o una ricerca Salva sessione Cerca Risultati di ricerca Cerca... Ricerca di «%1» Sessione Mostra informazioni di contatto... Mostra nei preferiti Mostra applicazioni come: Mostra applicazioni utilizzate spesso Mostra i contatti utilizzati spesso Mostra documenti utilizzati spesso Mostra applicazioni recenti Mostra i contatti recenti Mostra documenti recenti Mostra: Spegni Ordine alfabetico Avvia una sessione parallela come un altro utente Sospensione Sospendi in RAM Sospendi su disco Cambia utente Sistema Azioni di sistema Spegni il computer Digita per cercare. Mostra nuovamente applicazioni in «%1» Mostra nuovamente applicazioni in questo sottomenu Oggetti 