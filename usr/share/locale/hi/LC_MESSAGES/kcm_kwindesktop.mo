��          D      l       �      �   
   	       *      �  K  R       f     �  h   �                          <h1>Multiple Desktops</h1>In this module, you can configure how many virtual desktops you want and how these should be labeled. Desktop %1 Desktop %1: Here you can enter the name for desktop %1 Project-Id-Version: kcm_kwindesktop
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-06 05:35+0100
PO-Revision-Date: 2008-01-31 11:54+0530
Last-Translator: Ravishankar Shrivastava <raviratlami@yahoo.com>
Language-Team: Hindi <indlinux-hindi@lists.sourceforge.net>
Language: hi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=(n!=1);
 <h1>बहु डेस्कटॉप</h1>इस मॉड्यूल में आप कॉन्फ़िगर कर सकते हैं कि आप कितने आभासी डेस्कटॉप चाहते हैं तथा उनमें किस प्रकार लेबल लगाए जाएँ. डेस्कटॉप %1 डेस्कटॉप %1: यहाँ आप डेस्कटॉप %1 के लिए नाम भर सकते हैं 