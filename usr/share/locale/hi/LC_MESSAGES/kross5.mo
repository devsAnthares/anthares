��            )   �      �  &   �     �     �     �     �      �               .     6  ,   S  3   �     �     �     �     �     �  '        4     S     Y     o     �     �     �     �     �  &   �     �  �  �     �  9   �     �          #     <     S  O   f     �  ;   �  w     �   �  X   	  W   r	     �	     �	  "   �	  U   
  4   l
  
   �
  8   �
  D   �
     *  >   :     y  h   �     �  R        U                                       
                      	                                                                            @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2012-06-25 15:27+0530
Last-Translator: G Karunakar <karunakar@indlinux.org>
Language-Team: Hindi <kde-i18n-doc@kde.org>
Language: hi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n!=1);
X-Generator: Lokalize 1.2
 सामान्य नया स्क्रिप्ट जोड़ें. जोड़ें... रद्द करें? टिप्पणीः karunakar@indlinux.org संपादन चयनित स्क्रिप्ट संपादित करें. संपादन... चयनित स्क्रिप्ट चलाएँ इंटरप्रेटर "%1" के लिए स्क्रिप्ट बनाने में असफल स्क्रिप्ट फ़ाइल "%1" के लिए इंटरप्रेटर का पता लगाने में असफल इंटरप्रेटर "%1" को लोड करने में असफल स्क्रिप्ट फ़ाइल "%1" खोलने में असफल फ़ाइलः प्रतीकः इन्टरप्रेटर: रूबी इंटरप्रेटर का सुरक्षा स्तर करुणाकर गुंटुपल्ली नाम: ऐसा कोई फंक्शन नहीं "%1" ऐसा कोई इंटरप्रेटर नहीं "%1" हटाएँ चयनित स्क्रिप्ट हटायें चलाएँ स्क्रिप्ट फ़ाइल "%1" अस्तित्व में नहीं है. रूकें चयनित स्क्रिप्ट का चलाना रोकें पाठः 