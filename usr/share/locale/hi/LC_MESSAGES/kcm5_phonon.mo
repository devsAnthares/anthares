��    B      ,  Y   <      �  i   �  m        y  b   �     �     	  6        O  7   _     �     �  	   �     �  (   �  )   �  )   (     R     o  Y   u     �     �  �   �      y	  .   �	     �	  
   �	     �	     �	     
     
     !
     5
     B
     J
     c
     r
     w
     �
     �
     �
     �
     �
  	   �
  
   �
     �
     �
  	     
     
   *     5     B  	   `     j     o  
   �  �   �     (  8   8  �   q     �  8   	  ,   B  ,   o  %   �     �  �  �  �   �  <  *  F   g  �   �  8   c  %   �  q   �  .   4  z   c     �  "   �       L   -  a   z  j   �  m   G  D   �     �  �   
  4   �  4     1  C     u  ]   �     �       '        D  '   [     �  (   �     �     �  E   �  (   5     ^  S   k  J   �     
     %     8      T     u     �  #   �  5   �  #        '     A     a  C   �     �     �  9   �  3     @  S  "   �  �   �  P  <  1   �   ~   �   �   >!  �   �!  Z   M"  9   �"     $   !   %          ;   ,          B                 8   #                 7   9       +              )   ?   -   <         6                                    *   2       >                1       @       .   5   3   :   A         &      /                4              0      "             =   (   '         	   
           @info User changed Phonon backendTo apply the backend change you will have to log out and back in again. A list of Phonon Backends found on your system.  The order here determines the order Phonon will use them in. Apply Device List To... Apply the currently shown device preference list to the following other audio playback categories: Audio Hardware Setup Audio Playback Audio Playback Device Preference for the '%1' Category Audio Recording Audio Recording Device Preference for the '%1' Category Backend Colin Guthrie Connector Copyright 2006 Matthias Kretz Default Audio Playback Device Preference Default Audio Recording Device Preference Default Video Recording Device Preference Default/Unspecified Category Defer Defines the default ordering of devices which can be overridden by individual categories. Device Configuration Device Preference Devices found on your system, suitable for the selected category.  Choose the device that you wish to be used by the applications. EMAIL OF TRANSLATORSYour emails Failed to set the selected audio output device Front Center Front Left Front Left of Center Front Right Front Right of Center Hardware Independent Devices Input Levels Invalid KDE Audio Hardware Setup Matthias Kretz Mono NAME OF TRANSLATORSYour names Phonon Configuration Module Playback (%1) Prefer Profile Rear Center Rear Left Rear Right Recording (%1) Show advanced devices Side Left Side Right Sound Card Sound Device Speaker Placement and Testing Subwoofer Test Test the selected device Testing %1 The order determines the preference of the devices. If for some reason the first device cannot be used Phonon will try to use the second, and so on. Unknown Channel Use the currently shown device list for more categories. Various categories of media use cases.  For each category, you may choose what device you prefer to be used by the Phonon applications. Video Recording Video Recording Device Preference for the '%1' Category  Your backend may not support audio recording Your backend may not support video recording no preference for the selected device prefer the selected device Project-Id-Version: kcm_phonon
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2012-07-10 20:14+0530
Last-Translator: G Karunakar <karunakar@indlinux.org>
Language-Team: Hindi <indlinux-hindi@lists.sourceforge.net>
Language: hi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=2; plural=(n!=1);
 बैकेण्ड बदलाव लगाने के लिए आपको लागउट कर लॉगिन करना होगा. आपके तंत्र में प्राप्त फोनॉन बैकएण्ड की सूची. अनुक्रम यह निर्धारित करता है कि फोनॉन उन्हें किस अनुक्रम में प्रयोग करेगा. उपकरण सूची यहां लागू करें... वर्तमान उपकरण पसंद सूची को यह अन्य ध्वनि प्लेबैक श्रेणियों पर लगाएँ: ध्वनि हार्डवेयर सेटप ध्वनि प्लेबैक ध्वनि प्लेबैक उपकरण वरीयता श्रेणि '%1' के लिए ध्वनि रेकॉर्डिंग ध्वनि रिकार्डिंग उपकरण वरीयता श्रेणि '%1' के लिए बैकएण्ड कोलिन गूत्री कनेक्टर सर्वाधिकार 2006 मैथियास कर्त्ज डिफ़ॉल्ट ध्वनि प्लेबैक उपकरण वरीयता डिफ़ॉल्ट ध्वनि रेकॉर्डिंग उपकरण वरीयता डिफ़ॉल्ट विडियो रेकॉर्डिंग उपकरण वरीयता डिफ़ॉल्ट/अनिर्दिष्ट वर्ग टालें दर्शाता है उपकरणों का क्रम जो व्यक्तिक श्रेणियों द्वारा खारिज किया जा सकता है. उपकरण कॉन्फ़िगरेशन उपकरण प्राथमिकताएँ आपके तंत्र में प्राप्त उपकरण, चयनित श्रेणि के लिए योग्य. वह उपकरण चुनें जिसे आप अनुप्रयोगों में उपयोग करना चाहते हैं. raviratlami@aol.in, चयनित ध्वनि उपकरण सेट करने में विफल आगे बीच में आगे बाएँ बीच से आगे बाएँ आगे दाएँ बीच से आगे दाएँ हार्डवेयर स्वतंत्र उपकरण ईनपुट सतर अवैध कडीई ध्वनि हार्डवेयर सेटप मैथियास कर्त्ज मोनो रविशंकर श्रीवास्तव, जी. करूणाकर फोनॉन कॉन्फ़िगरेशन मॉड्यूल प्लेबैक (%1) वरीयता प्रोफ़ाइल पीछे बीच में पीछे बाएँ पीछे दाएँ रेकॉर्डिंग (%1) विकसित उपकरण दिखाएँ बाजू में बाएँ बाजू दाएँ ध्वनि कार्ड ध्वनि उपकरण स्पीकर स्थान चुनाव व जाँच सबवूफर जांच चयनित उपकरण जाँच करें जांच किया जा रहा है%1 यह क्रम उपकरणों की पसंद दर्शाता है, अगर किसी कारण पहला उपकरण उपयोग में नहीं हो सकता तो फ़ोनोन दूसरे का उपयोग करेगा, इत्यादि. अज्ञात चैनेल और श्रेणियोँ के लिए वर्तमान उपकरण सूची उपयोग करें. मिडिया उपयोग की विभिन्न श्रेणियाँ. प्रत्येक श्रेणी के लिए आप चुन सकते हैं कि किस उपकरण को फ़ोनोन अनुप्रयोगों में प्रयोग करना है. विडियो रेकॉर्डिंग विडियो रिकार्डिंग उपकरण वरीयता श्रेणि '%1' के लिए  यह बैकेंड शायद ध्वनि रिकार्डिंग समर्थित नहीं करता यह बैकेंड शायद विडियो रिकार्डिंग समर्थित नहीं करता चयनित उपकरण के लिए कोई वरीयता नहीं चयनित उपकरण पसंद करें 