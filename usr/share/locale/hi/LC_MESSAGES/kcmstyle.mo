��          �   %   �      P  &   Q  �   x     �                    &     3     D      T  	   u  c        �     �                9     A     N     Z     `  	   f  C   p  j   �       �  5  [   �  P  R  	   �     �  !   �  #   �  +   	  1   8	     j	     �	     �	  	  �	  2   �
  %   �
  S     9   h  !   �     �  %   �                 �   9    �  A   �                                                                          
                  	                                 (c) 2002 Karol Szwed, Daniel Molkentin <h1>Style</h1>This module allows you to modify the visual appearance of user interface elements, such as the widget style and effects. Button Checkbox Combobox Con&figure... Configure %1 Daniel Molkentin Description: %1 EMAIL OF TRANSLATORSYour emails Group Box If you enable this option, KDE Applications will show small icons alongside some important buttons. KDE Style Module Karol Szwed NAME OF TRANSLATORSYour names No description available. Preview Radio button Ralf Nolden Tab 1 Tab 2 Text Only There was an error loading the configuration dialog for this style. This area shows a preview of the currently selected style without having to apply it to the whole desktop. Unable to Load Dialog Project-Id-Version: kcmstyle
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-17 05:44+0100
PO-Revision-Date: 2007-10-23 13:35+0530
Last-Translator: Ravishankar Shrivastava <raviratlami@yahoo.com>
Language-Team: Hindi <indlinux-hindi@lists.sourceforge.net>
Language: hi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=(n!=1);
 (c) 2002 केरॉल स्वेद, डेनियल मॉलकेंटाइन <h1>शैली</h1>यह मॉड्यूल आपको उपयोक्ता इंटरफेस तत्वों के प्रद्रशित शक्ल-सूरत जैसे कि विजेट शैली तथा प्रभाव को परिवर्धित करने देता है. बटन चेकबॉक्स कॉम्बोबाक्स कॉन्फ़िगर... (&f) कॉन्फ़िगर करें %1 डेनियल मॉल्केनटिन वर्णनः %1 raviratlami@aol.in, समूह बक्सा यदि यह विकल्प चुना जाता है, तो केडीई अनुप्रयोग कुछ महत्वपूर्ण बटनों के बाज़ू में छोटे प्रतीक दिखाएगा. केडीई शैली मॉड्यूल केरोल स्ज़वेड रविशंकर श्रीवास्तव, जी. करूणाकर कोई वर्णन उपलब्ध नहीं पूर्वावलोकन रेडियो बटन राल्फ नॉल्देन टैब 1 टैब 2 सिर्फ पाठ इस शैली के लिए कॉन्फ़िगरेशन संवाद लोड करने में त्रुटि हुई. यह क्षेत्र मौज़ूदा चुने गए शैलियों का पूर्वावलोकन प्रदर्शित करता है, इसे पूरे डेस्कटॉप में लागू किए बगैर. संवाद लोड करने में अक्षम. 