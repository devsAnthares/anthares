��             +         �     �     �     �  +   �      !     B  ;   X     �     �     �     �      �       9   -     g  3   o  	   �     �  )   �  %   �  5     *   E     p     �     �     �     �  +   �  (   �     (  3   H  �  |  J     4   N     �  t   �  e   	  ;   t	  �   �	  4   E
  K   z
  [   �
  o   "     �  D   �  �   �     �  �   �     f  7   �  r   �  \   0  �   �  v   m  S   �  >   8     w  "   �  D   �  h   �  `   X  _   �  o                                                                   
   	                                                                             %1 theme already exists Add Emoticon Add... Choose the type of emoticon theme to create Could Not Install Emoticon Theme Create a new emoticon Create a new emoticon by assigning it an icon and some text Delete emoticon Design a new emoticon theme Do you want to remove %1 too? Drag or Type Emoticon Theme URL EMAIL OF TRANSLATORSYour emails Edit Emoticon Edit the selected emoticon to change its icon or its text Edit... Emoticon themes must be installed from local files. Emoticons Emoticons Manager Enter the name of the new emoticon theme: Get new icon themes from the Internet Install a theme archive file you already have locally Modify the selected emoticon icon or text  NAME OF TRANSLATORSYour names New Emoticon Theme Remove Remove Theme Remove the selected emoticon Remove the selected emoticon from your disk Remove the selected theme from your disk Require spaces around emoticons This will remove the selected theme from your disk. Project-Id-Version: kcm_emoticons
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-04 06:03+0100
PO-Revision-Date: 2010-12-29 03:08+0530
Last-Translator: G Karunakar
Language-Team: Hindi <en@li.org>
Language: hi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=(n!=1);
 प्रसंग %1 पहले से ही मौज़ूद है मनोभावचिह्न जोड़ें जोड़ें... मनोभावचिह्न प्रसंग बनाने हेतु क़िस्म चुनें इमोटिकॉन प्रसंग संस्थापित नहीं कर सका नया मनोभावचिह्न बनाएं कुछ पाठ तथा एक प्रतीक आबंटित कर एक नया मनोभावचिह्न बनाएँ मनोभावचिह्न मिटाएं नया मनोभावचिह्न डिजाइन करें क्या आप वाकई %1 को भी मिटाना चाहेंगे? इमोटिकॉन प्रसंग यूआरएल ड्रैग या टाइप करें raviratlami@aol.in, मनोभावचिह्न संपादित करें इसके प्रतीक या इसके पाठ को बदलने के लिए चयनित मनोभावचिह्न को संपादित करें संपादन... इमोटिकॉन प्रसंगों को स्थानीय फ़ाइलों से संस्थापित करना होगा. इमोटिकॉन्स मनोभावचिह्न प्रबंधक नए मनोभावचिह्न प्रसंग का नाम प्रविष्ट करें इंटरनेट से नया प्रसंग प्राप्त करें प्रसंग अभिलेख फ़ाइल जो आपके पास पहले से ही स्थानीय रूप से मौजूद है उसे संस्थापित करें चुने गए पाठ या मनोभावचिह्न को परिवर्धित करें रविशंकर श्रीवास्तव, जी. करूणाकर नया मनोभावचिह्न प्रसंग हटाएँ प्रसंग हटाएँ चयनित मनोभावचिह्न हटायें अपने डिस्क से चयनित मनोभावचिह्न मिटाएं अपने डिस्क से चयनित प्रसंग को मिटाएं मनोभावचिह्नों के चारों ओर जगह चाहिए यह चयनित प्रसंग को आपके डिस्क से मिटा देगा. 