��    `        �         (     )     1     B     Q     W     ^     j     {     �     �  /   �  -   �     �     �  
   		  
   	     	     ,	     1	     8	     @	     R	     _	     d	  
   l	     w	     �	     �	     �	  	   �	     �	  	   �	     �	     �	     �	     
     
  	   #
     -
     4
     H
  	   M
     W
     ]
     c
     h
     m
  	   v
     �
     �
     �
     �
     �
     �
     �
     �
  <   �
               /     6     =     X     ^     e     j  
   ~     �     �     �     �     �  )   �               5     :     @     M     U     ]     f  
   x     �     �  	   �     �     �     �     �     �     �     �  E   �  *   A  P  l     �     �     �     �     �  	   �               $  
   4  
   ?     J     Z     c     k     x  	   �     �     �     �     �     �     �     �  
   �     �     �     �          "     /  	   D     N     ^     e     �     �     �     �     �     �     �     �     �     �     �       	             +     7     ?     W     [     c     j  C   z     �     �     �     �                              3     ?     N     ]     r     �  3   �     �     �  	   �     	                 	   +     5     G  	   U     _  
   g     r     ~     �     �     �     �     �  K   �     .     J       -      M       %   Q           ^              U       L   S   $      .       H       X          Y   8   *         <   &          '       ]   N       3                     `          #   V             /   T   G   9   @   >   =   6   2   O   C   F   ?      0      
      A   E       	      P              _                D          Z               1   R   ,   +                \           I   )   B   K      :   !   W   4          ;               (      5   "       7       [           &Delete &Empty Trash Bin &Move to Trash &Open &Paste &Properties &Refresh Desktop &Refresh View &Reload &Rename @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Align Appearance: Arrange In Arrange in Arrangement: Back Cancel Columns Configure Desktop Custom title Date Default Descending Description Deselect All Desktop Layout Enter custom title here Features: File name pattern: File type File types: Filter Folder preview popups Folders First Folders first Full path Got it Hide Files Matching Huge Icon Size Icons Large Left List Location Location: Lock in place Locked Medium More Preview Options... Name None OK Panel button: Press and hold widgets to move them and reveal their handles Preview Plugins Preview thumbnails Remove Resize Restore from trashRestore Right Rotate Rows Search file type... Select All Select Folder Selection markers Show All Files Show Files Matching Show a place: Show files linked to the current activity Show the Desktop folder Show the desktop toolbox Size Small Small Medium Sort By Sort by Sorting: Specify a folder: Text lines Tiny Title: Tool tips Tweaks Type Type a path or a URL here Unsorted Use a custom icon Widget Handling Widgets unlocked You can press and hold widgets to move them and reveal their handles. whether to use icon or list viewView mode Project-Id-Version: plasma_applet_org.kde.desktopcontainment
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:11+0100
PO-Revision-Date: 2017-12-15 12:38+0100
Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>
Language-Team: Serbian <kde-i18n-sr@kde.org>
Language: sr@ijekavianlatin
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
 &Obriši &Isprazni smeće Premesti u &smeće &Otvori &Nalepi &Svojstva &Osveži površ &Osveži prikaz &Učitaj ponovo &Preimenuj Izaberi... Očisti ikonicu Poravnaj Izgled: Rasporedi po Raspored po Raspored: Nazad Odustani kolonama Podesi površ poseban naslov datum podrazumevan Opadajuće opis Poništi izbor Raspored površi Unesite ovde poseban naslov Mogućnosti: Obrazac imena fajla: tip fajla Tipovi fajlova: Filter Iskakači pregleda fascikle prvo fascikle Prvo fascikle puna putanja Razumem Sakrij poklopljene fajlove ogromne Veličina ikonica Ikonice velike lijevo Spisak Lokacija Lokacija: Zaključaj na mestu Zaključano srednje Još opcija pregleda... ime nikakav U redu Panelsko dugme: Pritisnite i držite vidžete da ih pomerate i otkrijete im ručke. Priključci za pregled Sličice za pregled Ukloni Promeni veličinu Vrati desno Okreni vrstama Potraži tip fajla... Izaberi sve Izbor fascikle Markeri izbora Prikaži sve fajlove Prikaži poklopljene fajlove Mjesto: Prikaži fajlove povezana sa trenutnom aktivnošću Prikaži fasciklu površi Prikaži alatnicu površi veličina male male Poređaj po Poređaj po Ređanje: Posebna fascikla: Redovi teksta sićušne Naslov: Oblačići Štelovanja tip Unesite putanju ili URL nepoređano Posebna ikonica Upravljanje vidžetima Vidžeti otključani Možete pritisnuti i držati vidžete da ih pomerate i otkrijete im ručke. Režim prikaza 