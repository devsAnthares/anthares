��    
      l      �       �   P   �   )   B  %   l  @   �     �  V   �     @     [     m  5       �  a   �  \     a   {     �  �   �     �     �     �            	               
                  %1 is '%1 packages to update' and %2 is 'of which %1 is security updates'%1, %2 1 package to update %1 packages to update 1 security update %1 security updates First part of '%1, %2'1 package to update %1 packages to update No packages to update Second part of '%1, %2'of which 1 is security update of which %1 are security updates Security updates available System up to date Updates available Project-Id-Version: muon-notifier
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-15 03:15+0100
PO-Revision-Date: 2015-11-14 16:11+0100
Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>
Language-Team: Serbian <kde-i18n-sr@kde.org>
Language: sr@ijekavianlatin
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
 %1, %2 %1 paket za ažuriranje %1 paketa za ažuriranje %1 paketa za ažuriranje %1 paket za ažuriranje %1 bezbjednosna dopuna %1 bezbjednosne dopune %1 bezbjednosnih dopuna %1 bezbjednosna dopuna %1 paket za ažuriranje %1 paketa za ažuriranje %1 paketa za ažuriranje %1 paket za ažuriranje Nema paketa za ažuriranje od čega je %1 dopuna bezbjednosna od čega su %1 dopuna bezbjednosne od čega je %1 dopuna bezbjednosnih od čega je %1 dopuna bezbjednosna Dostupne su bezbjednosne dopune Sistem je ažuran Dostupne su dopune 