��    b      ,  �   <      H     I     _     b     j     w  5   �  #   �  E   �  E   '	  >   m	     �	     �	  7   �	     
     '
     8
     W
     v
     �
     �
     �
     �
     �
     �
     �
     �
          #     (  	   /     9     Q     b  !   t  F   �     �     �       <        Z     b  *   k     �      �     �  	   �     �     �  	   �     �     	                :  
   X     c     �     �     �  
   �  F   �  :   �     7     ?     G     N     a     h  8   w     �     �  	   �  
   �     �     �     �               #     ;     C     a     r     {     �     �  &   �     �  
   �     �     
     *     2     ;     L     a  $   r  ]  �  6   �     ,     /     7     J  �   [  ,   �  E     E   T  >   �     �     �  .   �     )     >     J     `     w     �     �     �     �     �     �     �     �     �                    '     <     T  /   j  <   �  %   �     �       0   &     W  	   ^  $   h     �  !   �  	   �     �     �  
   �     �                    &     E     c     p     �     �     �     �  L   �  4        N     V     ]     d  
   x     �  ;   �     �  2   �  	             %     7     @     L  
   g     r     �     �     �  	   �  !   �     �     �     �     �          (     ;     G     N     W     j     }  #   �     K      >   P             M              -      +   H       ?   1      ^   J       Q         [   ;   3   5          6                              &       (           _                 S          G      *   L   Z       X   2      C               V   :   B       #   b   N   `       =       @              9   D   R   F      E       0   ]          a   7   %   A              8          
   <   W   	          I       $   .       )   '   O      U       \   "   4      Y   T   /          ,   !    
Also available in %1 %1 %1 (%2) %1 (Default) <b>%1</b> by %2 <em>%1 out of %2 people found this review useful</em> <em>Tell us about this review!</em> <em>Useful? <a href='true'><b>Yes</b></a>/<a href='false'>No</a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'><b>No</b></a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'>No</a></em> @infoFetching updates @infoFetching... @infoIt is unknown when the last check for updates was @infoLooking for updates @infoNo updates @infoNo updates are available @infoShould check for updates @infoThe system is up to date @infoUpdates @infoUpdating... Accept Addons Aleix Pol Gonzalez An application explorer Apply Changes Available backends:
 Available modes:
 Back Cancel Category: Checking for updates... Comment too long Comment too short Compact Mode (auto/compact/full). Could not close the application, there are tasks that need to be done. Could not find category '%1' Couldn't open %1 Delete the origin Directly open the specified application by its package name. Discard Discover Display a list of entries with a category. Extensions... Failed to remove the source '%1' Help... Homepage: Improve summary Install Installed Jonathan Thomas Launch License: List all the available backends. List all the available modes. Loading... Local package file to install Make default More Information... More... No Updates Open Discover in a said mode. Modes correspond to the toolbar buttons. Open with a program that can deal with the given mimetype. Proceed Rating: Remove Resources for '%1' Review Reviewing '%1' Running as <em>root</em> is discouraged and unnecessary. Search Search in '%1'... Search... Search: %1 Search: %1 + %2 Settings Short summary... Show reviews (%1)... Size: Sorry, nothing found... Source: Specify the new source for %1 Still looking... Summary: Supports appstream: url scheme Tasks Tasks (%1%) TransactioName TransactionStatus%1 %2 Unable to find resource: %1 Update All Update Selected Update section nameUpdate (%1) Updates Version: unknown reviewer updates not selected updates selected © 2010-2016 Plasma Development Team Project-Id-Version: plasma-discover
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-09 06:04+0100
PO-Revision-Date: 2018-01-06 09:21+0100
Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>
Language-Team: Serbian <kde-i18n-sr@kde.org>
Language: sr@ijekavianlatin
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
X-Associated-UI-Catalogs: libdiscover
 
Takođe dostupno u %1|/|
Takođe dostupno u $[lok %1] %1 %1 (%2) %1 (podrazumevano) <b>%1</b> — %2 <em>%1 od %2 osoba smatra ovu recenziju korisnom</em>|/|<em>%1 od %2 $[množ ^2 osobe osobe osoba] smatra ovu recenziju korisnom</em> <em>Recite nam nešto o ovoj recenziji!</em> <em>Korisno? <a href='true'><b>Da</b></a>/<a href='false'>Ne</a></em> <em>Korisno? <a href='true'>Da</a>/<a href='false'><b>Ne</b></a></em> <em>Korisno? <a href='true'>Da</a>/<a href='false'>Ne</a></em> Dobavljam dopune... Dobavljam... Ne zna se kad je bila poslednja provera dopuna Proveravam dopune... Nema dopuna Nema dostupnih dopuna Treba proveriti dopune Sistem je ažuran Dopune Ažuriram... Prihvati Dodaci Aleks Pol Gonzalez Istraživač programa Primeni izmene Dostupne pozadine:
 Dostupni režimi:
 Nazad Odustani Kategorija: Proveravam dopune... Komentar je predugačak Komentar je prekratak Sažeti režim (jedno od: auto, compact, full). Ne mogu da zatvorim program, ima još nedovršenih zadataka. Ne mogu da nađem kategoriju „%1“ Ne mogu da otvorim %1 Obriši izvorište Neposredno otvori zadati program po imenu paketa Odbaci Otkrivač Prikaži spisak unosa sa kategorijom Proširenja... Ne mogu da uklonim izvor „%1“ Pomoć... Domaća stranica: Poboljšaj sažetak Instaliraj Instalirano Džonatan Tomas Pokreni Licenca: Nabroji sve dostupne pozadine. Nabroji sve dostupne režime. Učitavam... Lokalni paket za instaliranje. Učini podrazumevanim Više podataka... Više... Nema dopuna Otvori Otkrivač u datom režimu. Režimi odgovaraju dugmadima trake alatki. Otvori programom koji ume da rukuje datim MIME tipom Nastavi Ocena: Ukloni Resursi za „%1“ Recenziraj Recenzija „%1“ Izvršavanje pod korenom nije preporučljivo niti potrebno. Pretraga Traži u „%1“...|/|Traži u „$[lok %1]“... Traži... Pretraga: %1 Pretraga: %1 + %2 Postavke Sažetak... Prikaži recenzije (%1)... Veličina: Ništa nije nađeno. Izvor: Zadajte novi izvor za %1 Još tražim... Sažetak: Podrška za URL šemu appstream:. Zadaci Zadaci (%1%) %1 %2 Ne mogu da nađem resurs: %1 Ažuriraj sve Ažuriraj izabrano Dopuna (%1) Dopune Izdanje: nepoznat recenzent Preskočene dopune Izabrane dopune © 2010–2016, razvojni tim Plasme 