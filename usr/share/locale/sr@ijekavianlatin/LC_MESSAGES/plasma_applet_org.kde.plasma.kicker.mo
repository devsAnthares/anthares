��    _                   C   	  /   M  -   }     �     �     �     �      	     	     1	     >	     J	  
   S	     ^	     g	     p	     �	  	   �	     �	     �	     �	  ,   �	  	    
     

  
   )
     4
     L
     `
     u
     �
     �
     �
     �
     �
  	   �
     �
     �
     
               !     (  	   ;     E     ]  
   r     }     �  
   �     �     �     �  
   �     �     �               $     2     @     R     h     y     �     �     �     �  	   �     �     �     �               4     Q     j     �     �     �     �  	   �     �  ,   �          !     0     @     L     S     b     t     �  #   �     �  K  �  4     
   M     X     h     x     �      �     �     �     �     �  
   �  
               
   "  	   -  	   7     A     R     d  0   p     �     �     �     �     �               )     :     L     g     n     v     �     �     �  
   �     �  	   �  
   �     �  n   �  r   b     �     �     �          $     C     J     [     d  M   l  N   �  Q   	     [     c     w     �     �     �  !   �     �     �       	        #     7     >     [     q  $   �  $   �  %   �     �          ,  	   G     Q     W  +   i  	   �     �     �     �     �     �     �     �  >     )   [     �         Q         Y   5           %       H       J   !              \   *       @   V   4   [       A   (   
       U   B   ]   ^       $   _         '   >       D   K      -                  ?      ,   O   0       R   8   6   W   2   I   =   X   E   #       N   C       T   G   M          3   "         9             +          S          F           ;   :                    &   <   L          P         7          .       /       1                     )            	   Z       @action opens a software center with the applicationManage '%1'... @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Desktop Add to Favorites Add to Panel (Widget) Align search results to bottom All Applications App name (Generic name)%1 (%2) Applications Apps & Docs Behavior Categories Computer Contacts Description (Name) Description only Documents Edit Application... Edit Applications... End session Expand search to bookmarks, files and emails Favorites Flatten menu to a single level Forget All Forget All Applications Forget All Contacts Forget All Documents Forget Application Forget Contact Forget Document Forget Recent Documents General Generic name (App name)%1 (%2) Hibernate Hide %1 Hide Application Icon: Lock Lock screen Logout Name (Description) Name only Often Used Applications Often Used Documents Often used On All Activities On The Current Activity Open with: Pin to Task Manager Places Power / Session Properties Reboot Recent Applications Recent Contacts Recent Documents Recently Used Recently used Removable Storage Remove from Favorites Restart computer Run Command... Run a command or a search query Save Session Search Search results Search... Searching for '%1' Session Show Contact Information... Show In Favorites Show applications as: Show often used applications Show often used contacts Show often used documents Show recent applications Show recent contacts Show recent documents Show: Shut Down Sort alphabetically Start a parallel session as a different user Suspend Suspend to RAM Suspend to disk Switch User System System actions Turn off computer Type to search. Unhide Applications in '%1' Unhide Applications in this Submenu Widgets Project-Id-Version: plasma_applet_org.kde.plasma.kicker
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-23 05:56+0100
PO-Revision-Date: 2017-09-25 19:53+0200
Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>
Language-Team: Serbian <kde-i18n-sr@kde.org>
Language: sr@ijekavianlatin
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
 Upravljaj „%1“...|/|Upravljaj „$[nom %1]“... Izaberi... Očisti ikonicu Dodaj na površ Dodaj u omiljene Dodaj na panel (vidžet) Ravnaj rezultate pretrage uz dno Svi programi %1 (%2) Programi Programi i dokumentacija Ponašanje Kategorije Računar Kontakti opis (ime) samo opis Dokumenti Uredi program... Uredi programe... Kraj sesije Pretražuj i obeleživače, fajlove i e‑poštu Omiljeno Meni spljošten na jedan nivo Zaboravi sve Zaboravi sve programe Zaboravi sve kontakte Zaboravi sve dokumente Zaboravi program Zaboravi kontakt Zaboravi dokument Zaboravi nedavne dokumente Opšte %1 (%2) U hibernaciju Sakrij %1|/|Sakrij $[aku %1] Sakrij program Ikonica: Zaključaj Zaključaj ekran Odjavi me ime (opis) samo ime Često korišćeni programi|/|$[svojstva gen 'često korišćenih programa' aku 'često korišćene programe'] Često korišćeni dokumenti|/|$[svojstva gen 'često korišćenih dokumenata' aku 'često korišćene dokumente'] Često korišćeno Na svim aktivnostima Na tekućoj aktivnosti Otvori pomoću: Prikači na menadžer zadataka Mjesta Napajanje/sesija Svojstva Resetuj Nedavni programi|/|$[svojstva gen 'nedavnih programa' aku 'nedavne programe'] Nedavni kontakti|/|$[svojstva gen 'nedavnih kontakata' aku 'nedavne kontakte'] Nedavni dokumenti|/|$[svojstva gen 'nedavnih dokumenata' aku 'nedavne dokumente'] Nedavno Nedavno korišćeno Uklonjivo skladište Ukloni iz omiljenih Ponovo pokreni računar Izvrši naredbu... Izvrši naredbu ili upit pretrage Sačuvaj sesiju Pretraga Rezultati pretrage Traži... Tražim „%1“... Sesija Prikaži podatke kontakta... Prikaži u omiljenima Prikazuj programe kao: Prikaži često korišćene programe Prikaži često korišćene kontakte Prikaži često korišćene dokumente Prikaži nedavne programe Prikaži nedavne kontakte Prikaži nedavne dokumente Prikaži: Ugasi Poređaj abecedno Pokreni naporedu sesiju za drugog korisnika Suspenduj Suspenduj u memoriju Suspenduj na disk Prebaci korisnika Sistem Sistemske radnje Ugasi računar Unesite nešto za traženje. Otkrij programe u „%1“|/|Otkrij programe u „$[lok %1]“ Otkrij skrivene programe u ovom podmeniju Vidžeti 