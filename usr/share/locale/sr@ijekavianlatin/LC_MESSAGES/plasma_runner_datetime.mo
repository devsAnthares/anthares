��    	      d      �       �      �      �   -        <  -   V  #   �  #   �     �  >  �          5  2   N     �  4   �     �     �     �                                   	           Current time is %1 Displays the current date Displays the current date in a given timezone Displays the current time Displays the current time in a given timezone Note this is a KRunner keyworddate Note this is a KRunner keywordtime Today's date is %1 Project-Id-Version: plasma_runner_datetime
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2015-01-31 12:56+0100
Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>
Language-Team: Serbian <kde-i18n-sr@kde.org>
Language: sr@ijekavianlatin
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
 Trenutno vrijeme je %1 Prikazuje trenutni datum Prikazuje trenutni datum u zadatoj vremenskoj zoni Prikazuje trenutno vrijeme Prikazuje trenutno vrijeme u zadatoj vremenskoj zoni datum vrijeme Današnji datum je %1 