��          �   %   �      @     A  !   Y      {     �      �     �     �  +        =     N     [  (   z     �  ,   �  7   �     !  7   :     r  ]   �  1   �  	   "     ,  "   ?  5   b  4  �     �  (   �  %         ;     \  &   p  !   �     �     �     �     �  *   	     -	  )   J	  :   t	     �	  1   �	     �	  d   
  1   w
     �
     �
  +   �
  &   �
                    
         	                                                                                                 Cannot start initramfs. Cannot start update-alternatives. Configure Plymouth Splash Screen Download New Splash Screens EMAIL OF TRANSLATORSYour emails Get New Boot Splash Screens... Initramfs failed to run. Initramfs returned with error condition %1. Install a theme. Marco Martin NAME OF TRANSLATORSYour names No theme specified in helper parameters. Plymouth theme installer Select a global splash screen for the system The theme to install, must be an existing archive file. Theme %1 does not exist. Theme corrupted: .plymouth file not found inside theme. Theme folder %1 does not exist. This module lets you configure the look of the whole workspace with some ready to go presets. Unable to authenticate/execute the action: %1, %2 Uninstall Uninstall a theme. update-alternatives failed to run. update-alternatives returned with error condition %1. Project-Id-Version: kcm_plymouth
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-28 03:05+0200
PO-Revision-Date: 2017-05-07 21:01+0200
Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>
Language-Team: Serbian <kde-i18n-sr@kde.org>
Language: sr@ijekavianlatin
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
 Ne mogu da pokrenem initramfs. Ne mogu da pokrenem update-alternatives. Podešavanje uvodnog ekrana Plymoutha Preuzimanje novih uvodnih ekrana caslav.ilic@gmx.net Dobavi nove uvodne ekrane podizanja... initramfs ne može da se izvrši. initramfs vraća grešku %1. Instaliraj temu. Marko Martin Časlav Ilić Nije zadata tema u parametrima pomoćnika. Instalator Plymouthovih tema Izaberite globalni uvodni ekran za sistem Tema za instaliranje, mora da bude postojeći fajl arhive. Tema %1 ne postoji. Tema iskvarena: nema .plymouth fajla unutar teme. Fascikla teme %1 ne postoji. U ovom modulu možete da podesite izgled celog radnog prostora, uz nekoliko spremnih predefinisanih. Ne mogu da autentifikujem/izvršim radnju: %1, %2 Deinstaliraj Deinstaliraj temu. update-alternatives ne može da se izvrši. update-alternatives vraća grešku %1. 