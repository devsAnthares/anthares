��          �      L      �  &   �  +   �       @     	   ]     g     �     �     �     �  (   �     �     �  ,   �               +     7  P  ;  ,   �  1   �     �     �     �          %  
   -  
   8     C  ,   T     �     �  .   �  	   �     �     �     �        
                          	                                                                  Do you want to suspend to RAM (sleep)? Do you want to suspend to disk (hibernate)? General Heading for list of actions (leave, lock, shutdown, ...)Actions Hibernate Hibernate (suspend to disk) Leave Leave... Lock Lock the screen Logout, turn off or restart the computer No Sleep (suspend to RAM) Start a parallel session as a different user Suspend Switch User Switch user Yes Project-Id-Version: plasma_applet_org.kde.plasma.lock_logout
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2015-07-12 20:23+0200
Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>
Language-Team: Serbian <kde-i18n-sr@kde.org>
Language: sr@ijekavianlatin
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
 Želite li da suspendujete u RAM (spavanje)? Želite li da suspendujete na disk (hibernacija)? Opšte Radnje U hibernaciju U hibernaciju (susp. na disk) Napusti Napusti... Zaključaj Zaključaj ekran Odjavi me, ugasi ili ponovo pokreni računar Ne Na spavanje (susp. u RAM) Započnite paralelnu sesiju kao drugi korisnik Suspenduj Prebaci korisnika Prebaci korisnika Da 