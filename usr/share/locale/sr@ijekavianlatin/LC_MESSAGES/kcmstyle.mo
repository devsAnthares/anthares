��    )      d  ;   �      �  &   �  �   �  e  G     �     �     �     �     �     �               #      3  	   T  �   ^  \   '  c   �     �     �          "     A     I     c     k     x  !   �     �     �     �     �     �     �  	   	  C   	  j   P	  E   �	     
     

      
  0  .
  &   _  �   �  `    	   p     z     �     �     �  
   �     �     �     �     �     �  �     T   �  U     E   t  
   �     �     �  
   �     �               %     1     P     g  
   |  
   �     �     �  
   �  <   �  N     0   Q     �     �     �               
       "      $                      #                                  '   )   &                     !                     (                                             %   	                   (c) 2002 Karol Szwed, Daniel Molkentin <h1>Style</h1>This module allows you to modify the visual appearance of user interface elements, such as the widget style and effects. <p><b>No Text:</b> Shows only icons on toolbar buttons. Best option for low resolutions.</p><p><b>Text Only: </b>Shows only text on toolbar buttons.</p><p><b>Text Beside Icons: </b> Shows icons and text on toolbar buttons. Text is aligned beside the icon.</p><b>Text Below Icons: </b> Shows icons and text on toolbar buttons. Text is aligned below the icon. @title:tab&Applications @title:tab&Fine Tuning Button Checkbox Combobox Con&figure... Configure %1 Daniel Molkentin Description: %1 EMAIL OF TRANSLATORSYour emails Group Box Here you can choose from a list of predefined widget styles (e.g. the way buttons are drawn) which may or may not be combined with a theme (additional information like a marble texture or a gradient). If you enable this option, KDE Applications will show small icons alongside most menu items. If you enable this option, KDE Applications will show small icons alongside some important buttons. KDE Style Module Karol Szwed Main &toolbar text location: NAME OF TRANSLATORSYour names No Text No description available. Preview Radio button Ralf Nolden Secondary toolbar text &location: Show icons in b&uttons: Show icons in menus: Tab 1 Tab 2 Text Below Icons Text Beside Icons Text Only There was an error loading the configuration dialog for this style. This area shows a preview of the currently selected style without having to apply it to the whole desktop. This page allows you to choose details about the widget style options Toolbars Unable to Load Dialog Widget style: Project-Id-Version: kcmstyle
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-17 05:44+0100
PO-Revision-Date: 2017-12-17 18:00+0100
Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>
Language-Team: Serbian <kde-i18n-sr@kde.org>
Language: sr@ijekavianlatin
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
 © 2002, Karol Sved, Danijel Molkentin <h1>Stil</h1><p>U ovom modulu možete mijenjati vizuelni izgled elemenata korisničkog sučelja, kao što su stil vidžeta i efekti.</p> <p><i>bez teksta</i>: samo ikonice na traci alatki; najbolja opcija za niske rezolucije ekrana</p><p><i>samo tekst</i>: samo tekst na traci alatki</p><p><i>tekst pored ikonica</i>: i ikonice i tekst na traci alatki; tekst je poravnat pored ikonice</p><p><i>tekst ispod ikonica</i>: i ikonice i tekst na traci alatki; tekst je poravnat ispod ikonice</p> &Programi &Fino štelovanje Dugme Kućica Spisak &Podesi... Podesi %1|/|Podesi $[aku %1] Danijel Molkentin Opis: %1 caslav.ilic@gmx.net Okvir grupe Birajte sa spiska predefinisanih stilova vidžeta (način na koji se npr. dugmad iscrtava), koji mogu ili ne moraju biti kombinovani sa temom (dodatne informacije kao što su teksture ili preliv). Ako uključite ovo, KDE programi će kraj većine stavki menija davati male ikonice. Ako uključite ovo, KDE programi će na nekim važnijim dugmadima imati male ikonice. Kontrolni modul za stil|/|$[svojstva dat 'Kontrolnom modulu za stil'] Karol Sved &Lokacija teksta glavne trake: Časlav Ilić bez teksta Opis nije dostupan. Pregled Radio dugme Ralf Nolden Lokacija &teksta drugih traka: &Ikonice na dugmadima: &Ikonice u menijima: Jezičak 1 Jezičak 2 tekst ispod ikonica tekst pored ikonica samo tekst Greška pri učitavanju dijaloga za podešavanje ovog stila. Pregled trenutno izabranog stila, prije nego što se primijeni na celu površ. Na ovoj stranici birate opcije za stil vidžeta. Trake alatki Ne mogu da učitam dijalog Stil vidžeta: 