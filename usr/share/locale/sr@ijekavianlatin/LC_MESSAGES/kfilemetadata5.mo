��    H      \  a   �            !     5     D     R     f     s     �     �     �     �     �     �     �     �          "     0     B     P     ]     m     }     �     �     �     �     �     �     �               &     3     @     \     s     �      �     �     �     
	     %	     =	  #   \	     �	     �	     �	  "   �	     
  $   !
     F
     c
     
     �
     �
  3   �
       $   (     M  &   f  <   �  !   �  8   �  0   %  !   V  C   x  X   �  P     R   f      �  +   �  6       =     N  	   U  
   _     j     p     v     �     �  
   �     �     �     �     �     �     �     �     �                         (     6     C     L     [     g     o     t     z     �     �     �     �     �     �     �     �          !     2     ?     W     u     �     �     �     �     	     %     8     I     i          �     �  
   �     �     �     �     �     �     �  
                  5     T     l     r        -      0   G   C   4                      +   ,                 "      '   	          <             ;   9   3              *   A   2   %      )   :   =                           @   5                   H   &   B   $             /   
      ?       E       D   6                 F          #   !   1                  8       7       .      (       >       @labelAlbum Artist @labelArchive @labelArtist @labelAspect Ratio @labelAudio @labelAuthor @labelBitrate @labelChannels @labelComment @labelComposer @labelCopyright @labelCreation Date @labelDocument @labelDocument Generated By @labelDuration @labelFolder @labelFrame Rate @labelHeight @labelImage @labelKeywords @labelLanguage @labelLyricist @labelPage Count @labelPresentation @labelPublisher @labelRelease Year @labelSample Rate @labelSpreadsheet @labelSubject @labelText @labelTitle @labelVideo @labelWidth @label EXIFImage Date Time @label EXIFImage Make @label EXIFImage Model @label EXIFImage Orientation @label EXIFPhoto Aperture Value @label EXIFPhoto Exposure Bias @label EXIFPhoto Exposure Time @label EXIFPhoto F Number @label EXIFPhoto Flash @label EXIFPhoto Focal Length @label EXIFPhoto Focal Length 35mm @label EXIFPhoto GPS Altitude @label EXIFPhoto GPS Latitude @label EXIFPhoto GPS Longitude @label EXIFPhoto ISO Speed Rating @label EXIFPhoto Metering Mode @label EXIFPhoto Original Date Time @label EXIFPhoto Saturation @label EXIFPhoto Sharpness @label EXIFPhoto White Balance @label EXIFPhoto X Dimension @label EXIFPhoto Y Dimension @label date of template creation8Template Creation @label music albumAlbum @label music disc numberDisc Number @label music genreGenre @label music track numberTrack Number @label number of fuzzy translated stringsDraft Translations @label number of linesLine Count @label number of translatable stringsTranslatable Units @label number of translated stringsTranslations @label number of wordsWord Count @label the URL a file was originally downloded fromDownloaded From @label the message ID of an email this file was attached toE-Mail Attachment Message ID @label the sender of an email this file was attached toE-Mail Attachment Sender @label the subject of an email this file was attached toE-Mail Attachment Subject @label translation authorAuthor @label translations last updateLast Update Project-Id-Version: kfilemetadata5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:36+0200
PO-Revision-Date: 2017-10-30 23:08+0100
Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>
Language-Team: Serbian <kde-i18n-sr@kde.org>
Language: sr@ijekavianlatin
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
 izvođač albuma arhiva izvođač proporcija audio autor bitski protok kanala komentar kompozitor autorska prava datum stvaranja dokument generator dokumenta trajanje fascikla broj kadrova visina slika ključne reči jezik tekstopisac broj stranica prezentacija izdavač godina izdanja uzorkovanje tablica tema tekst naslov video širina datum-vreme (slika) marka (slika) model (slika) orijentacija (slika) vrednost blende (slika) otklon ekspozicije (slika) vreme ekspozicije (slika) f‑broj (slika) blic (slika) fokusna dužina (slika) fokusna dužina 35 mm (slika) GPS nadmorska visina (slika) GPS geografska širina (slika) GPS geografska dužina (slika) ISO razredi brzine (slika) mjerenje osvjetljenja (slika) izvorni datum-vreme (slika) zasićenje (slika) oštrina (slika) uravnotežavanje bijele (slika) x‑veličina (slika) u‑veličina (slika) stvaranje šablona album broj diska žanr broj numere nacrti prevoda broj redova prevodive jedinice prevodi broj reči preuzeto sa ID poruke priloga e‑pošte pošiljalac priloga e‑pošte tema priloga e‑pošte autor poslednja izmena 