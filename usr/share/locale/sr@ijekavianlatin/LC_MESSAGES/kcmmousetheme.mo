��          �   %   �      @     A  �   `  m   �  �   P  )   �  `        o     |     �     �     �      �     �     �  '        ,     >     ]     b     s  ?   �  Y   �  +     H   F  5  �     �  w   �  d   ]	     �	     �	  X   �	     L
  #   T
     x
     �
     �
     �
     �
     �
  #   �
               )     -     >  8   J  T   �  0   �  F   	                                                 	                                                               
              (c) 2003-2007 Fredrik Höglund <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) @item:inlistbox sizeResolution dependent A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Cursor Theme Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Fredrik Höglund Get new Theme Get new color schemes from the Internet Install from File NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. You have to restart the Plasma session for these changes to take effect. Project-Id-Version: kcmmousetheme
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-07-23 22:40+0200
Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>
Language-Team: Serbian <kde-i18n-sr@kde.org>
Language: sr@ijekavianlatin
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
 © 2003–2007, Fredrik Heglund <qt>Želite li zaista da uklonite temu pokazivača <i>%1</i>? Ovo će obrisati sve fajlove instalirane ovom temom.</qt> <qt>Ne možete obrisati temu koju trenutno koristite. Prvo morate prebaciti na neku drugu temu.</qt> (dostupne veličine: %1) nezavisno od rezolucije Tema ikonica po imenu %1 već postoji u fascikli tema. Želite li da je zamijenite ovom? Potvrda Postavke pokazivača su izmijenjene Tema pokazivača Opis Prevucite ili upišite URL teme caslav.ilic@gmx.net Fredrik Heglund Dobavi novu temu Dobavi novu šemu boja sa Interneta Instaliraj iz fajla Časlav Ilić Ime Prebrisati temu? Ukloni temu Fajl %1 ne izgleda kao pravilna arhiva teme pokazivača. Ne mogu da preuzmem arhivu teme pokazivača. Provjerite da li je adresa %1 ispravna. Ne mogu da pronađem arhivu teme pokazivača %1. Morate ponovo pokrenuti sesiju Plasme da bi izmjene stupile u dejstvo. 