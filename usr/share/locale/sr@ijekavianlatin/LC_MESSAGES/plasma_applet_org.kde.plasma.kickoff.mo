��            )   �      �     �  
   �  /   �  -   �          !  
   2     =     J     `  W   i     �  ,   �  	                  !     '     -  
   :     E     W     o     �     �     �     �  1   �       L       f  
   l     w     �     �     �     �     �     �     �  n   �     O  8   a     �     �  	   �     �     �     �     �     �     	     	     1	     G	     b	     t	     �	     �	                                                                                  
                                	                       %1@%2 %2@%3 (%1) @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Favorites All Applications Appearance Applications Applications updated. Computer Drag tabs between the boxes to show/hide them, or reorder the visible tabs by dragging. Edit Applications... Expand search to bookmarks, files and emails Favorites Hidden Tabs History Icon: Leave Menu Buttons Often Used On All Activities On The Current Activity Remove from Favorites Show In Favorites Show applications by name Sort alphabetically Switch tabs on hover Type is a verb here, not a nounType to search... Visible Tabs Project-Id-Version: plasma_applet_org.kde.plasma.kickoff
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2017-09-25 19:53+0200
Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>
Language-Team: Serbian <kde-i18n-sr@kde.org>
Language: sr@ijekavianlatin
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
 %1@%2 %2@%3 (%1) Izaberite... Očisti ikonicu Dodaj u omiljene Svi programi Izgled Programi Programi ažurirani. Računar Prevlačite jezičke između okvira da ih prikažete ili sakrijete, ili menjajte redosled vidljivih jezičaka. Uredi programe... Proširi pretragu na obeleživače, fajlove i e‑poštu Omiljeno Skriveni jezičci Istorijat Ikonica: Napusti Dugmad menija Često korišćeno Na svim aktivnostima Na tekućoj aktivnosti Ukloni iz omiljenih Prikaži u omiljenima Prikaži programe po imenu Poređaj abecedno Prebacuj jezičke pri lebdenju Kucajte za pretragu... Vidljivi jezičci 