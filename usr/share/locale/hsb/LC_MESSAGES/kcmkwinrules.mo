��    %      D  5   l      @  "   A     d     �     �     �     �     �     �     �  !   �      �          	          )  
   2     =     F     T  -   b     �     �  &   �     �     �  :   �     "     )     1     ?  *   E     p  .     B   �     �  -     �  6  !        :     U     c     j     s     �     �     �     �  '   �     �     �     	     	     	     *	     8	     G	  4   V	     �	     �	  0   �	     �	     �	  +   �	      
     &
     5
  	   G
  ,   Q
     ~
  !   �
  :   �
     �
  4                 #                 "                                                                              %   	                                                   !      $   
       (c) 2004 KWin and KControl Authors Application settings for %1 C&lear Class: Default Desktop Dialog Window Dock (panel) Edit... Information About Selected Window Internal setting for remembering KWin KWin helper utility Lubos Lunak Machine: Move &Down Move &Up Normal Window Override Type Remember settings separately for every window Role: Shortcut Show internal settings for remembering Splash Screen Standalone Menubar This helper utility is not supposed to be called directly. Title: Toolbar Torn-Off Menu Type: Unknown - will be treated as Normal Window Utility Window WId of the window for special window settings. Whether the settings should affect all windows of the application. Window settings for %1 Window-Specific Settings Configuration Module Project-Id-Version: desktop_kdebase
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2008-11-21 23:13+0100
Last-Translator: Eduard Werner <edi.werner@gmx.de>
Language-Team: en_US <kde-i18n-doc@lists.kde.org>
Language: hsb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KAider 0.1
Plural-Forms: nplurals=4; plural=n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3;
 (c) 2004 KWin a awtorojo KControl Pogramowe nastajenja za %1 &Wuprózdnić Klasa: Standard Dźěłowy powjerch Dialogowe wokno Panel Wobdźěłać... Informacija wo wubranym woknje Nutřkowne nastajenja za spomjatkowanje KWin KWin pomocnik Lubos Lunak Mašina: &Dele sunyć &Horje sunyć Normalne wokno Přerjadowanje Nastajenja za kóžde wokno separatnje spomjatkować Róla: Skrótšenka Nutřkowne nastajenja za spomjatkowanje pokazać Powitančko Samostatny menijowy pas Tutón pomocnik nima so direktnje zwołać. Titl: Nastrojowy pas Wottorhnjeny meni Družina: Njeznate - wobhladuje so jako normalne wokno  Wokno za pomocnika Wid wokna za specielne nastajenja Hač so nastajenja na wšitke wokna aplikacije wuskutkuja. Woknowe nastajenja %1 Připrawjenski modul za specifiske nastajenja woknow 