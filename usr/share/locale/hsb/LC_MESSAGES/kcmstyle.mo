��          |      �             !     (     1     ?     L  �   \  c   %     �     �  	   �  j   �  �        �     �               "  �   2  Y   �     E     ^     j  >   w                            	             
                  Button Checkbox Con&figure... Configure %1 Description: %1 Here you can choose from a list of predefined widget styles (e.g. the way buttons are drawn) which may or may not be combined with a theme (additional information like a marble texture or a gradient). If you enable this option, KDE Applications will show small icons alongside some important buttons. No description available. Preview Text Only This area shows a preview of the currently selected style without having to apply it to the whole desktop. Project-Id-Version: kcmstyle
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-17 05:44+0100
PO-Revision-Date: 2005-02-25 17:27+0100
Last-Translator: Bianka Šwejdźic <hertn@gmx.de>
Language-Team: Hornjoserbšćina
Language: hsb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.3
Plural-Forms: nplurals=4; plural=n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3;
 Knefl Městno za křižik &Zarjadować... %1 zarjadować Wopisowanje: %1 Tu móžeće z lisćiny předdefinowanych elementowych stilow (na př. wašnje, kak so knefle pokazuja) wuzwolić. Tute hodźa so wuhotować (marmorowa struktura abo běžny přechod). Tuta opcija zmóžnja, zo so małe symbole na najwažnišich kneflach KDE-programa jewja. Nimam žane wopisowanje. Předhladka Jenož tekst Tu widźiće předhladku stila, kotryž je so runje wuzwolił. 