��          �      L      �     �     �  �   �  T   }  )   �  (   �  0   %  $   V  &   {  &   �  %   �  ?   �  F   /     v     �     �     �     �  �  �     �     �  �   �  Y   J     �  
   �     �  
   �     �  	   �     �     �               5     F     U     h                                              
                    	                                  Dim screen by half Dim screen totally Lists screen brightness options or sets it to the brightness defined by :q:; e.g. screen brightness 50 would dim the screen to 50% maximum brightness Lists system suspend (e.g. sleep, hibernate) options and allows them to be activated Note this is a KRunner keyworddim screen Note this is a KRunner keywordhibernate Note this is a KRunner keywordscreen brightness Note this is a KRunner keywordsleep Note this is a KRunner keywordsuspend Note this is a KRunner keywordto disk Note this is a KRunner keywordto ram Note this is a KRunner keyword; %1 is a parameterdim screen %1 Note this is a KRunner keyword; %1 is a parameterscreen brightness %1 Set Brightness to %1 Suspend to Disk Suspend to RAM Suspends the system to RAM Suspends the system to disk Project-Id-Version: plasma_runner_powerdevil
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-26 03:33+0200
PO-Revision-Date: 2010-07-14 15:12+0100
Last-Translator: Berend Ytsma <berendy@gmail.com>
Language-Team: Frysk <kde-i18n-fry@kde.org>
Language: fy
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=n != 1;
 skerm foar 50% dimme skem totaal dimme Skerm dim opsjes opsomme of ynstelle yn de helderheid opsjes :q:; bgl skerm helderheid 50 sil it skerm dime eni 50% fan de maksimale helderheid Systeem ûnderbrek opsjes(bgl. sliep, wachtstân) opsomme en iepen stelle foar aktivaasje skerm dimme sliepstân skerm helderheid wachtstân ûnderbrekke nei skiif nei ûnthâld skerm dimme %1 skerm helderheid %1 Set helderheid nei %1 Skiif-sliepstân RAM-sliepstân Sliepstân nei RAM Sliepstân nei skiif 