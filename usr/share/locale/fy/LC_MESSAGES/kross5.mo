��            )   �      �  &   �     �     �     �     �      �               .     6  ,   S  3   �     �     �     �     �     �  '        4     S     Y     o     �     �     �     �     �  &   �     �  �  �     �     �     �  	   �     �     �     �     �          &  /   F  3   v     �  !   �     �  
   �     �  %   	     /     <     C  #   c  
   �     �     �     �     �  .   �     
	                                       
                      	                                                                            @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2010-07-22 09:48+0100
Last-Translator: Berend Ytsma <berendy@gmail.com>
Language-Team: nl <kde-i18n-doc@lists.kde.org>
Language: fy
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KAider 0.1
Plural-Forms: nplurals=2; plural=n != 1;
 Algemien In nij skript taheakje Taheakje... Ofbrekke? Taljochting: Berendy@gmail.com Bewurkje Selektearre skript bewurkje. Bewurkje... It selektearre skript útfiere. Koe skript net oanmeitsje foar interpreter "%1" Koe interpreter foar skripttriem "%1" net beskiede. Koe interpreter "%1" net lade Koe skript-triem "%1" net iepenje Triem: Byldkaike: Interpreter: Feiligensnivo fan de Ruby-interpreter Berend Ytsma Namme: Dizze funksje bestiet net: "%1" Dizze interpreter bestiet net: "%1" Fuortsmite Selektearre skript fuortsmite Utfiere Skripttriem %1 bestiet net. Stopje It útfieren fan it selektearre skript stopje. Tekst: 