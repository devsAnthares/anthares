��    	      d      �       �      �      �           !     >     V  ,   r  c   �  �       �     �     �     �     	  '   )  +   Q  Z   }                                    	          Also allow remote connections Halt Without Confirmation Log Out Log Out Without Confirmation Logout canceled by '%1' Reboot Without Confirmation Restores the saved user session if available The reliable KDE session manager that talks the standard X11R6 
session management protocol (XSMP). Project-Id-Version: ksmserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-05 03:18+0100
PO-Revision-Date: 2010-07-16 11:51+0100
Last-Translator: Berend Ytsma <berendy@gmail.com>
Language-Team: Frysk <kde-i18n-fry@kde.org>
Language: fy
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=n != 1;
 Ek eksterne ferbiningen tastean Ophâlde sûnder befêstiging Utlogge Utlogge sûnder befêstiging Ofmelding ôfbrutsen troch '%1' Op 'e nij opstarte sûnder befêstiging Herstelt, as beskikber is, de foarige sesje De betroubere KDE-sesjebehearder die it standert X11R6-
sesjebehearprotokol (XSMP) brûkt. 