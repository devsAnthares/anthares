��    *      l  ;   �      �  A   �     �  /        2     ;     O     a     w     �     �  	   �     �  3   �  	   �     �      �  $      *   E     p  	   u  5        �     �  
   �     �          $  5   3  6   i     �  ,   �  /   �     	        O   0  Z   �  b   �  	   >  
   H  P   S     �  �  �  7   \
     �
  /   �
     �
      �
          $     ?     S     \     i     w  8   �     �     �     �  %   �            	   "  4   ,  !   a     �     �     �     �     �  F   �  9        O  8   e     �     �     �  C   �  W     Q   q     �     �  ]   �     >            )             &      $   	   %         '                                                !      
       *                     "                                                 (                 #    %1 is an external application and has been automatically launched (c) 2009, Ben Cooksley <i>Contains 1 item</i> <i>Contains %1 items</i> About %1 About Active Module About Active View About System Settings Apply Settings Author Ben Cooksley Configure Configure your system Determines whether detailed tooltips should be used Developer Dialog EMAIL OF TRANSLATORSYour emails Expand the first level automatically General config for System SettingsGeneral Help Icon View Internal module representation, internal module model Internal name for the view used Keyboard Shortcut: %1 Maintainer Mathias Soeken NAME OF TRANSLATORSYour names No views found Provides a categorized icons view of control modules. Provides a classic tree-based view of control modules. Relaunch %1 Reset all current changes to previous values Search through a list of control modulesSearch Show detailed tooltips System Settings System Settings was unable to find any views, and hence has nothing to display. System Settings was unable to find any views, and hence nothing is available to configure. The settings of the current module have changed.
Do you want to apply the changes or discard them? Tree View View Style Welcome to "System Settings", a central place to configure your computer system. Will Stephenson Project-Id-Version: systemsettings
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:08+0100
PO-Revision-Date: 2010-07-14 10:56+0100
Last-Translator: Berend Ytsma <berendy@gmail.com>
Language-Team: Frysk <kde-i18n-fry@kde.org>
Language: fy
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: KBabel 1.11.4
 %1 is in ekstern programma en wurdt automatysk útfierd (c) 2009, Ben Cooksley <i>Befettet 1 item</i> <i>Befettet %1 items</i> Ynfo oer %1 Ynformaasje oer de aktive module Aktive werjefte ynformaasje Oer de systeem ynstellings Ynstellings tapasse Skriuwer Ben Cooksley Konfigurearje Jo systeem ynstelle Jout oan of der detailearre arktippen brûk moatte wurde Untwikkelder Dialooch berendy@bigfoot.com It earste nivo automatysk útfâldzje Algemien Help Byldkaike Ynterne module werpresintaasje, ynterne module model Ynterne namme foar dizze werjefte Fluchtoets: %1 Underhâlder Mathias Soeken Berend Ytsma Gjin wergaven fûn Ferskaft in katogorisearre byldkaikewerjefte fan bestjoeringsmodullen. Ferskaft in klasike beamstruktuer fan kontrôle moldules. %1 op 'e nij útfiere Alle lêste feroarings weromsette nei de foarige wearden Sykje Detailearre arktip sjen litte Systeemynstellings Systeem ynstellings koe gjin wergaven fine, en der is neat te sjen. Systeem ynstellings koe gjin wergaven fine, en der is neat foar it ynstellen beskikber. De ynstelling fan de aktive module is feroare.
Wolle jo it tapasse of se negeare? Beamstrucktuer Werjefte styl Wolkom by "Systeem ynstellings", in sintraal plak foar it ynstellen fan jo kompjûtersysteem. Will Stephenson 