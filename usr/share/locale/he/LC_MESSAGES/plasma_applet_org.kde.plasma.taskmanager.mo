��    O      �  k         �     �     �     �     �     �     �     �  .   �  U   (     ~     �      �     �  /   �  /        5     A     J  
   V     a     q  $   �     �     �     �     �     �     �  	   	     	  
   	     )	     <	     O	     g	  	   m	     w	  !   �	     �	     �	  	   �	      �	     �	     �	     
     $
     5
     :
     G
     M
     _
     w
  (   �
     �
  =   �
            "   5     X     s  J   {  1   �  )   �  (   "  '   K  "   s  4   �     �     �     �     �     �          "  U   8  N   �  9   �  J     �  b  !   %  	   G     Q     `     h  	   �     �     �     �  &   �     �  '   �       F   0  F   w     �     �     �     �       .     -   K     y     �     �  "   �     �     �     �     �               4  $   Q     v  	     
   �  +   �     �     �  	   �  *        2  -   F     t      �     �     �     �     �     �       .   -  -   \     �     �     �     �     �     �     �  -   �  :   "  A   ]  2   �  +   �  J   �     I     h  	   o     y     �     �     �     �     �     �  "   �     %   N         @   A       >   +   "       ,       F   /   B   O           9      ;      $                      G          3          K         ?                        2   L   *      !      H   =                   <              '               :       -            .   I   J   6   C           M       1          #   0      4       7       (   	   
           8               D   5   )   &   E    &All Desktops &Close &Fullscreen &Move &New Desktop &Pin &Shade 1 = number of desktop, 2 = desktop name&%1 %2 Activities a window is currently on (apart from the current one)Also available on %1 Add To Current Activity All Activities Allow this program to be grouped Alphabetically Always arrange tasks in columns of as many rows Always arrange tasks in rows of as many columns Arrangement Behavior By Activity By Desktop By Program Name Close Window or Group Cycle through tasks with mouse wheel Do Not Group Do Not Sort Filters Forget Recent Documents General Grouping and Sorting Grouping: Highlight windows Icon size: Keep &Above Others Keep &Below Others Keep launchers separate Large Ma&ximize Manually Mark applications that play audio Maximum columns: Maximum rows: Mi&nimize Minimize/Restore Window or Group More Actions Move &To Current Desktop Move To &Activity Move To &Desktop Mute New Instance On %1 On All Activities On The Current Activity On middle-click: Only group when the task manager is full Open groups in popups Open or bring to the front window of media player appRestore Pause playbackPause Play next trackNext Track Play previous trackPrevious Track Quit media player appQuit Re&size Remove launcher button for application shown while it is not runningUnpin Show all user Places%1 more Place %1 more Places Show only tasks from the current activity Show only tasks from the current desktop Show only tasks from the current screen Show only tasks that are minimized Show progress and status information in task buttons Show tooltips Small Sorting: Start New Instance Start playbackPlay Stop playbackStop The click actionNone Toggle action for showing a launcher button while the application is not running&Pin When clicking it would toggle grouping windows of a specific appGroup/Ungroup Which activities a window is currently onAvailable on %1 Which virtual desktop a window is currently onAvailable on all activities Project-Id-Version: plasma_applet_org.kde.plasma.taskmanager
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2017-05-23 01:00-0400
Last-Translator: Elkana Bardugo <ttv200@gmail.com>
Language-Team: Hebrew <kde-i18n-doc@kde.org>
Language: he
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Zanata 3.9.6
Plural-Forms: nplurals=2; plural=n != 1;
 &כל שולחנות העבודה &סגור &מסך מלא ה&זז שולחן עבודה חדש &הצמד האפל &%1 %2 זמין גם ב %1 הוסף לפעילות הנוכחית כל הפעילויות אפשר ליישום זה להתקבץ לפי סדר הא"ב תמיד ארגן משימות בעמודות של הרבה שורות תמיד ארגן משימות בשורות של הרבה עמודות סדר התנהגות לפי פעילות לפי שולחן עבודה לפי שם יישום סגור חלון או קבוצת חלונות עבור בין משימות ע"י גלילה אל תקבץ אל תמיין מסננים נקה מסמכים אחרונים כללי קיבוץ ומיון קיבוץ: הדגש חלונות גודל סמל: שמור מעל אחרים שמור מתחת אחרים שמור משגרים מופרדים גדול &הגדל ידנית סמן יישומים המנגנים שמע מקסימום עמודות: מספר שורות מירבי: &מזער מזער/שחזר חלון או קבוצה עוד פעולות העבר לשולחן עבודה הנוכחי העבר ל&פעילות העבר לשולחן עבודה השתק פתח מופע חדש ב־%1 בכל הפעילויות בפעילות הנוכחית בלחיצה על הגלגלת: קבץ רק ששורת המשימות מלאה פתח קבוצה בחלונות קופצים שחזר השהה הרצועה הבאה הרצועה הקודמת צא שנה &גודל בטל הצמדה עוד מקום אחד עוד %1 מקומות הראה רק משימות מהפעילות הנוכחית הראה רק משימות משולחן העבודה הנוכחי הראה רק משימות מהמסך הנוכחי הראה רק משימות ממוזערות הצג מידע על התקדמות ופרטים בלחצני משימות הצג חלוניות מידע קטן מיון: פתח חלון חדש נגן עצור כלום & קבץ/בטל קיבוץ זמין ב %1 זמין בכל הפעילויות 