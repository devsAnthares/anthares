��          �      �           	          &     -     4  
   =     H     Q     Y     i  >   w     �     �     �     �  
   �     �     �     �            U   %  �  {     E     Q     b     i  
   r     }     �  
   �     �  
   �  3   �     �               !     5  	   I     S     \     t     �  �   �                       	                                                
                                 %1 is running %1 not running &Reset &Start Advanced Appearance Command: Display Execute command Notifications Remaining time left: %1 second Remaining time left: %1 seconds Run command S&top Show notification Show seconds Show title Text: Timer Timer finished Timer is running Title: Use mouse wheel to change digits or choose from predefined timers in the context menu Project-Id-Version: plasma_applet_org.kde.plasma.timer
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2017-05-16 06:56-0400
Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>
Language-Team: Hebrew <kde-i18n-doc@kde.org>
Language: he
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Zanata 3.9.6
Plural-Forms: nplurals=2; plural=n != 1;
 %1 פועל %1 לא פועל אפס התחל מתקדם מראה פקודה: תצוגה ביצוע פקודה התראה שניה אחת נותרה נותרו %1 שניות הרץ פקודה עצור הראה התראה הראה שניות הראה כותרת טקסט: טימר טיימר הסתיים טיימר פועל כותרת: השתמש בגלגלת כדי לשנות את הספרות או כדי לבחור זמן מוגדר מראש בתפריט ההקשר 