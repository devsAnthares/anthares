��    )      d  ;   �      �     �  
   �  N   �       	   &     0  G   9     �     �     �     �     �     �     �     �  	   �     �     �               ,  
   ?  	   J     T     i     v  !   �  :   �  E   �  F   7  I   ~  C   �  H     E   U  !   �  
   �     �     �     �     �  �       �	     
  x   
  %   �
     �
     �
  !   �
     �
     �
               :     O     ^     g     {      �     �  
   �     �  #   �          )  #   =     a  0   u  3   �  
   �     �     �     �               *  
   7     B     T     m     �  %   �                       "   &   #                    '   $                	              )                   %   !           
                                 (                                                   Adjust Date and Time... Appearance At least one time zone needs to be enabled. 'Local' was enabled automatically. Available Calendar Plugins Bold text Calendar Checkbox label; means 24h clock format, without am/pmUse 24-hour Clock City Comment Date format: Display time zone as: Font style: ISO Date Information Italic text Long Date No events for this day No events for today Region Search Time Zones Set Time Format... Short Date Show date Show local time zone Show seconds Show week numbers in Calendar Switch time zone with mouse wheel This is a city associated with particular time zoneZurich This is a continent/area associated with a particular timezoneAfrica This is a continent/area associated with a particular timezoneAmerica This is a continent/area associated with a particular timezoneAntarctica This is a continent/area associated with a particular timezoneAsia This is a continent/area associated with a particular timezoneAustralia This is a continent/area associated with a particular timezoneEurope This means "Local Timezone"Local Time Zones Time zone city Time zone code Use default fontDefault Your system time zone Project-Id-Version: plasma_applet_org.kde.plasma.digitalclock
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-05 03:18+0100
PO-Revision-Date: 2017-05-16 06:55-0400
Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>
Language-Team: Hebrew <kde-i18n-doc@kde.org>
Language: he
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Zanata 3.9.6
 הגדר שעה ותאריך... מראה לפחות איזור זמן אחד צריך להיות מאופשר. "מקומי" היה מאופשר אוטומטית. תוספי לוח שנה זמינים כיתוב מודגש לוח שנה השתמש בשעון 24 שעות עיר הערה פורמט תאריך: הראה איזור זמן כ: עיצוב גופן: תאריך ISO מידע כיתוב מוטה תאריך ארוך אין אירועים להיום אין אירועים היום מדינה חפש איזור זמן הגדר פורמטי איזור... תאריך קצר הראה תאריך הרא איזור זמן מקומי הראה שניות הראה מספרי שבועות בלוח שנה החלף איזור זמן עם גלגל העכבר ציריך אפריקה אמריקה אנטרטיקה אסיה אוסטרליה אירופה מקומי איזור זמן עיר איזור זמן קוד איזור זמן ברירת־מחדל איזור הזמן של המערכת 