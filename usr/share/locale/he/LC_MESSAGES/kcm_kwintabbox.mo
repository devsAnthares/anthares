��    "      ,  /   <      �  
   �               ,     >     J  !   V     x     �     �     �     �     �  L   �     #     +     J     Y     u     z     �     �     �     �  	   �     �     �     �  �   �  F   �     �     �     �  �  �     �  $   �  -   �          4     H     [     s     |     �      �     �     �  Y   �  
   Q	  =   \	     �	  .   �	     �	  
   �	  &   �	      
     <
  
   E
     P
     _
  	   x
     �
  �   �
  n   P     �     �  
   �                    !                                            	                                                           "               
                    Activities All other activities All other desktops All other screens All windows Alternative An example Desktop NameDesktop 1 Content Current activity Current application Current desktop Current screen Filter windows by Focus policy settings limit the functionality of navigating through windows. Forward Get New Window Switcher Layout Hidden windows Include "Show Desktop" icon Main Minimization Only one window per application Recently used Reverse Screens Shortcuts Show selected window Sort order: Stacking order The currently selected window will be highlighted by fading out all other windows. This option requires desktop effects to be active. The effect to replace the list window when desktop effects are active. Virtual desktops Visible windows Visualization Project-Id-Version: kcm_kwintabbox
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2017-05-16 06:52-0400
Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>
Language-Team: Hebrew <kde-i18n-doc@kde.org>
Language: he
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Zanata 3.9.6
Plural-Forms: nplurals=2; plural=n != 1;
 פעילויות כל הפעילויות האחרות כל שולחנות העבודה האחרים כל המסכים האחרים כל החלונות אלטרנטיבי שולחן עבודה 1 תוכן פעילות הנוכחית יישום נוכחי שולחן עבודה נוכחי מסך נוכחי סנן באמצעות הגדרות מיקוד מגבילות את יכולות ניווט דרך חלונות.  קדימה קבל סידורים של החלפת חלונות חדשים חלונות מוסתרים כלול סמל "הצג שולחן עבודה" ראשי מזעור רק חלון אחד לכל יישום בשימוש לאחרונה הפוך מסכים קיצורים הצג חלון מבחר מיון: מסודר בערמות החלון הנבחר הנוכחי יואר באמצעות הדהיית כל שאר החלונות. אפשרות זו דרושת אפקטים בשולחן עבודה מופעלים. האפקט להחלפת רשימת החלונות, כאשר אפקטים בשולחן עבודה פועלים. שולחנות העבודה החלונות הגלויים תצוגה 