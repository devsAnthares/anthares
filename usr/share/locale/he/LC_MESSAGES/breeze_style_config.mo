��            )   �      �  "   �     �  !   �  !   �        
   6     A     \     l  !        �  0   �  8   �     +  !   I     k     �     �     �     �     �            
     
   &  
   1  &   <     c     o  �  �  -     (   I  9   r  4   �     �     �  (        4  5   H  :   ~  6   �  ^   �  ]   O	  -   �	  G   �	  6   #
  8   Z
  &   �
  ?   �
     �
  B        W     d     m     �     �  C   �     �                                                            
                                                                  	                &Keyboard accelerators visibility: &Top arrow button type: Always Hide Keyboard Accelerators Always Show Keyboard Accelerators Anima&tions duration: Animations Bottom arrow button t&ype: Breeze Settings Center tabbar tabs Drag windows from all empty areas Drag windows from titlebar only Drag windows from titlebar, menubar and toolbars Draw a thin line to indicate focus in menus and menubars Draw focus indicator in lists Draw frame around dockable panels Draw frame around page titles Draw frame around side panels Draw slider tick marks Draw toolbar item separators Enable animations Enable extended resize handles Frames General No Buttons One Button Scrollbars Show Keyboard Accelerators When Needed Two Buttons W&indows' drag mode: Project-Id-Version: breeze_style_config
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:42+0200
PO-Revision-Date: 2017-05-23 06:19-0400
Last-Translator: Elkana Bardugo <ttv200@gmail.com>
Language-Team: Hebrew
Language: he
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Zanata 3.9.6
Plural-Forms: nplurals=2; plural=n != 1;
 נראות סמני קיצורי מ&קלדת: סוג כפתור גלילה &עליון תמיד הסתר את סימני קיצורי מקלדת תמיד הראה סימני קיצורי מקלדת משך אנימציות& אנימציות סוג כפתור גלילה &תחתון הגדרות Breeze מרכז כרטיסיות בסרגל כרטיסיות דרוג חלונות מכל האיזורים הריקים דרוג חלונות משורת הכותרת בלבד דרוג חלונות משורת הכותרת, שורות תפריטים וסרגלי גלים צייר קו דק לפריטים בפוקוס בתפריטים ובשורות תפריטים הראה מחוון מיקוד ברשימות ציור מסגרת מסביב ללוחות הניתנים לעגינה ציור מסגרת מסביב לכותרות דפים ציור מסגרת מסביב ללוחות צדדיים צייר סמן על פס הגלילה הצג מפרידים בין פריטים בסרגלי כלים אפשר אנימציות אפשר הרחבה של אזור אחיזה לשינוי גודל מסגרות כללי ללא כפתורים כפתור אחד סרגלי גלילה הראה את  סימני קיצורי מקלדת מתי שצריך שני כפתורים מצב &גרירת חלונות 