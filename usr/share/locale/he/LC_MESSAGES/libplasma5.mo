��    E      D  a   l      �     �  (   �  +   &  +   R  ,   ~     �     �     �     �       $     &   5     \     l  	   �     �     �  	   �  
   �     �     �     �  	   �     �               2     >     L     U     Z     a     i     r       "   �     �  
   �     �  
   �  	   �     �     �     �     	     	     !	     /	     D	     V	     i	  
   �	     �	     �	     �	  !   �	     �	     �	     �	     �	     �	  	   
  	   
     
     .
     >
     F
     X
  �  t
     &     6     F     Y     h     |     �     �     �     �     �     �               /     D     c     j     |     �     �     �  
   �  "   �               2     H     ^     k     r          �     �  
   �     �  
   �     �     �     �     �  
             %     5     K     ]     o     �     �     �     �     �     �     �     �          .     5  $   C     h     |     �     �     �     �     �  
   �     !          /      ;                     A              7   5                    -      	   "      @      2      :          6   &   3      9   .   >       0                      8                           4   (      D       ?   <       
   %   $                        )             +       B         C   1      '   *       =   E       ,           #        %1 Settings %1 is the name of the applet%1 Settings %1 is the name of the applet%1 Settings... %1 is the name of the appletRemove this %1 %1 is the name of the containment%1 Options A desktop has been removed. A panel has been removed. Accessibility Activity Settings Add Widgets... Agenda listview section titleEvents Agenda listview section titleHolidays Alternatives... Application Launchers Astronomy Background image for panels Cancel Clipboard Data Files Date and Time Desktop Removed Development Tools Education Environment and Weather Examples Executable Scripts File System Fun and Games Graphics Icon Images Install Language Lock Widgets Mapping Means 'Other calendar items'Other Miscellaneous Multimedia Next Decade Next Month Next Year OK Open with %1 Panel Removed Previous Decade Previous Month Previous Year Remove this Activity Remove this Panel Remove this Widget Reset calendar to todayToday Screenshot System Information Tasks Tests The widget "%1" has been removed. Translations Undo Unknown Unlock Widgets User Interface Utilities Wallpaper Widget Removed Widget Settings Widgets Windows and Tasks misc categoryMiscellaneous Project-Id-Version: libplasma5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-10 02:58+0100
PO-Revision-Date: 2017-05-16 06:50-0400
Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>
Language-Team: Hebrew <kde-i18n-doc@kde.org>
Language: he
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Zanata 3.9.6
 הגדרות %1 הגדרות %1 הגדרות %1... הסר את %1 אפשרויות %1 שולחן עבודה הוסר. לוח הוסר. נגישות הגדרות פעילות הוסף יישומון... אירועים חגים חלופות... משגר יישומים אסטרונומיה תמונת רקע ללוחות בטל לוח העתקה קובצי נתונים זמן ותאריך שולחן עבודה הוסר כלי פיתוח חינוך אווירה ומזג האוויר דוגמאות סקריפט בר־הרצה מערכת קבצים כיף ומשחקים גרפיקה סמל תמונות התקן שפה נעל יישומונים מיפוי אחר שונות מדיה העשור הבא חודש הבא שנה הבאה אישור פתח באמצעות %1 לוח הוסר העשור הקודם חודש קודם שנה קודמת הסר פעילות זו הסר לוח זה הסר יישומון זה היום צילום מסך מידע מערכת משימות בדיקות היישומון "%1" הוסר. תרגומים בטל לא ידוע בטל נעילת יישומונים ממשק משתמש כלים רקע יישומון הוסר הגדרות יישומון יישומונים חלונות ומשימות שונות 