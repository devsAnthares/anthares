��          �      �       0     1  ,   J     w  O   �     G      _     �  =   �     �  H   �  !   @     b  �         1   2  �   d  d   $     �     �     �  S   �       B   2     u     �        
                                        	          (c) 2016 Kai Uwe Broulik A brief one-line summary of the notification A comma-separated list of user IDs this notification should be sent to. If omitted, the notification will be sent to all users. A tool that emits a notification for all users by sending it on the system DBus Broadcast Notifications EMAIL OF TRANSLATORSYour emails Icon for the notification Keep the notification in the history until the user closes it NAME OF TRANSLATORSYour names Name of the application that should be associated with this notification The actual notification body text Timeout for the notification Project-Id-Version: kbroadcastnotification
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-11 05:52+0100
PO-Revision-Date: 2017-05-16 11:30-0400
Last-Translator: Elkana Bardugo <ttv200@gmail.com>
Language-Team: Hebrew
Language: he
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Zanata 3.9.6
Plural-Forms: nplurals=2; plural=n != 1;
 (c) 2016 Kai Uwe Broulik תקציר של שורה אחת של ההתראה רשימת מזהיי משתמשים (user ID), מופרדת בפסיקים, של המשתמשים אליהם תשלח ההתראה. אם ריק, ההתראה תשלח לכל המשתמשים כלי לשליחת התראות לכל המשתמשים באמצעות שליחה למערכת DBus הודעות תפוצה ttv200@gmail.com סמל ההתראה שמור את ההתראה בהיסטוריה עד שהמשתמש סוגר אותה אלקנה ברדוגו שם היישום שצריך להיות קשור להתראה זו גוף ההתראה הגבלת זמן להתראה 