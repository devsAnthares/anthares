��          �   %   �      `     a  
   g  /   r  -   �     �     �  
   �     �     
        W   )     �  ,   �  	   �     �     �     �     �     �  
   �               5     I  1   ^     �  �  �     \  
   b  	   m     w     �     �     �     �  "   �     �  �   �     �  Y   �          -     K     \  
   d     o  &   �     �  ,   �     �  $   	     +	     G	                     
                                                                         	                              %1@%2 %2@%3 (%1) @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Favorites All Applications Appearance Applications Applications updated. Computer Drag tabs between the boxes to show/hide them, or reorder the visible tabs by dragging. Edit Applications... Expand search to bookmarks, files and emails Favorites Hidden Tabs History Icon: Leave Menu Buttons Often Used Remove from Favorites Show applications by name Sort alphabetically Switch tabs on hover Type is a verb here, not a nounType to search... Visible Tabs Project-Id-Version: plasma_applet_org.kde.plasma.kickoff
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2017-05-22 05:18-0400
Last-Translator: Elkana Bardugo <ttv200@gmail.com>
Language-Team: Hebrew <kde-i18n-doc@kde.org>
Language: he
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Zanata 3.9.6
 %1@%2 %2@%3 (%1) בחר... נקה סמל הוסף למועדפים כל היישומים מראה יישומים היישומים מעודכנים. מחשב גרור כרטיסיות בין התיבות כדי להציג או להסתיר אותן, או סדר מחדש את הכרטיסיות הגלויות על ידי גרירה. ערוך יישומים... הרחב חיפוש ל־סמניות, קבצים והודעות דואר אלקטרוני מועדפים כרטיסיות נסתרות היסטוריה סמל: יציאה כפתורי תפריט בשימוש לעיתים קרובות הסר ממועדפים הראה יישומים על ידי שמות סדר לפי שם החלף כרטיסיה בריחוף הקלד כדי לחפש... כרטיסיות גלויות 