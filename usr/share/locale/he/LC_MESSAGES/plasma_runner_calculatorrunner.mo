��          4      L       `   m   a   R   �   �  "  u   �  _   ^                    Calculates the value of :q: when :q: is made up of numbers and mathematical symbols such as +, -, /, * and ^. The exchange rates could not be updated. The following error has been reported: %1 Project-Id-Version: plasma_runner_calculatorrunner
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-28 05:09+0200
PO-Revision-Date: 2017-05-16 06:57-0400
Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>
Language-Team: Hebrew <kde-i18n-doc@kde.org>
Language: he
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Zanata 3.9.6
 מחשב את הערך של :q: כאשר :q: מכיל מספרים וסימני חשבון כגון +,-,*,/ וגם ^. אין אפשרות לעדכן את שער החליפין. דווחה השגיאה הבאה: %1 