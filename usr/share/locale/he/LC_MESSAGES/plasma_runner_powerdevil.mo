��          �      L      �     �     �  �   �  T   }  )   �  (   �  0   %  $   V  &   {  &   �  %   �  ?   �  F   /     v     �     �     �     �  �  �      �  '   �  �   �  p   �               &  
   :     E     N     ^     p     �  "   �     �     �  ,   �  %   *	                                              
                    	                                  Dim screen by half Dim screen totally Lists screen brightness options or sets it to the brightness defined by :q:; e.g. screen brightness 50 would dim the screen to 50% maximum brightness Lists system suspend (e.g. sleep, hibernate) options and allows them to be activated Note this is a KRunner keyworddim screen Note this is a KRunner keywordhibernate Note this is a KRunner keywordscreen brightness Note this is a KRunner keywordsleep Note this is a KRunner keywordsuspend Note this is a KRunner keywordto disk Note this is a KRunner keywordto ram Note this is a KRunner keyword; %1 is a parameterdim screen %1 Note this is a KRunner keyword; %1 is a parameterscreen brightness %1 Set Brightness to %1 Suspend to Disk Suspend to RAM Suspends the system to RAM Suspends the system to disk Project-Id-Version: plasma_runner_powerdevil
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-26 03:33+0200
PO-Revision-Date: 2017-05-16 06:57-0400
Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>
Language-Team: Hebrew <kde-i18n-doc@kde.org>
Language: he
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Zanata 3.9.6
Plural-Forms: nplurals=2; plural=n != 1;
 הורד בהירות חלקית הורד את הבהירות לגמרי מציג את האפרויות החשכת המסך או קובע את הבהירות אל :q: , למשל בהירות של 50 תהפוך את המסך לחצי מוחשך. מציג את אפשרויות ההשהיה (למשל שינה, השהייה) ומאפשר להפעיל אותם הורד בהירות שינה בהירות מסך המתנה השהה אל הדיסק אל הזכרון מסך מוחשך אל %1 בהירות מסך היא %1 קבע את הבהירות אל %1 השהייה לכונן השהייה לזכרון משהה את המערכת על הזכרון משהה את המערכת לכונן 