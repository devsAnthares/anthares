��          �      <      �  5   �  �  �  +   �               8     M      k     �     �  	   �  
   �     �     �            7   ,  �  d       �  #  8   �     	     0	     H	     ^	     y	     �	     �	  	   �	  
   �	     �	     �	     
     
  ;   "
                                                                 	               
                     %1 is language name, %2 is language code name%1 (%2) %1 is the language codeThe translation files for the language with the code '%2' could not be found. The language has been removed from your configuration. If you want to add it back, please install the localization files for it and add the language again. The translation files for the languages with the codes '%2' could not be found. These languages have been removed from your configuration. If you want to add them back, please install the localization files for it and the languages again. <p>Click here to install more languages</p> Applying Language Settings Available &Translations: Available Languages: Configure Plasma translations EMAIL OF TRANSLATORSYour emails Install more languages Install more translations John Layt Maintainer NAME OF TRANSLATORSYour names Preferred Languages: Preferred Trans&lations: Translations Your changes will take effect the next time you log in. Project-Id-Version: kcmtranslations
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2017-05-16 06:53-0400
Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>
Language-Team: Hebrew <kde-i18n-doc@kde.org>
Language: he
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Zanata 3.9.6
 %1 (%2) קובצי התרגום לקוד השפה '%2' לא נמצאו. השפה הוסרה מההגדרות שלך. אם אתה רוצה את שפה זו חזרה, אנא התקן את קבצי התרגום שוב. קובצי התרגום לשפות בעלות הקודים '%2' לא נמצאו. השפות הוסרו מההגדרות שלך. אם אתה רוצה את שפות אלו חזרה, אנא התקן את קבצי התרגום שוב. <p>לחץ כאן כדי להתקין עוד שפות</p> מחיל הגדרות שפה שפות זמיניות שפות זמינות הגדר שפת מערכת ttv200@gmail.com התקן עוד שפות התקן עוד שפות John Layt מתחזק אלקנה ברדוגו שפות מועדפות שפות מועדפות שפות השינויים יחולו בפעם הבאה שתתחבר. 