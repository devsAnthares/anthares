��          �   %   �      P     Q     b     w     �     �     �  
   �     �     �     �     �     �  /        3     Q     p     v     �     �     �     �     �     �     �       �  %     �     �       :   1     l  .   |     �     �  
   �     �     �     �  L   �  '   L  (   t     �     �     �     �  ,   �  -   �  &   +  '   R     z     �                            
                                                                                             	    %1 file %1 files %1 folder %1 folders %1 of %2 processed %1 of %2 processed at %3/s %1 processed %1 processed at %2/s Appearance Behavior Cancel Clear Configure... Finished Jobs List of running file transfers/jobs (kuiserver) Move them to a different list Move them to a different list. Pause Remove them Remove them. Resume Show all jobs in a list Show all jobs in a list. Show all jobs in a tree Show all jobs in a tree. Show separate windows Show separate windows. Project-Id-Version: kuiserver5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2017-05-16 06:54-0400
Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>
Language-Team: עברית <kde-i18n-doc@kde.org>
Language: he
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Zanata 3.9.6
 קובץ %1 %1 קבצים תיקייה %1 %1 תיקיות עוּבָד %1 מתוך %2 עוּבָד %1 מתוך %2 במהירות %3 לשנייה %1 עוּבָד עוּבָד %1 במהירות %2 לשנייה מראה התנהגות ביטול נקה הגדרה... משימות שהושלמו רשימת העברות הקבצים/משימות שפועלות (kuiserver) העבר אותן לרשימה אחרת העבר אותן לרשימה אחרת. השהה הסר אותן הסר אותן. המשך הצג את כל הרשימות ברשימה הצג את כל העבודות ברשימה. הצג את כל המשימות בעץ הצג את כל העבודות בעץ. הצג חלונות שונים הצג חלונות שונים. 