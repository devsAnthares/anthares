��          �      L      �     �     �     �     �          )     1     9     P     h     t  K   �     �     �     �     �            �        �            +   :     f  	        �     �  "   �     �     �     �       	        !     =  
   Z     e                                                  
   	                                               Change the barcode type Clear history Clipboard Contents Clipboard history is empty. Clipboard is empty Code 39 Code 93 Configure Clipboard... Creating barcode failed Data Matrix Edit contents Indicator that there are more urls in the clipboard than previews shown+%1 Invoke action QR Code Remove from history Return to Clipboard Search Show barcode Project-Id-Version: plasma_applet_org.kde.plasma.clipboard
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-10 03:07+0200
PO-Revision-Date: 2017-05-16 06:55-0400
Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>
Language-Team: Hebrew <kde-i18n-doc@kde.org>
Language: he
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Zanata 3.9.6
 שנה סוג ברקוד נקה היסטוריה תוכן לוח העתקה היסטורית לוח העתקה ריקה לוח העתקה ריק קוד 39 'וד 93 הגדר לוח העתקה... יצירת הברקוד נכשלה מטריקס נתונים ערוך תוכן +%1 עורר פעולה קוד QR הסר מההיסטוריה חזור ללוח העתקה חיפוש הראה ברקוד 