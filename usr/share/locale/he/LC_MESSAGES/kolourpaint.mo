��    �      �  K  |      �     �     �     �     �     �     �     �     �  	   �     �                              %     2     :     L     R     [     c     j     w     ~     �     �     �  	   �     �     �  	   �     �     �  
   �     �  	   �  
   �                         -  	   :  
   D     O     \     h  �   p  C        O     S  	   Y     c  	   l     v     ~     �     �  
   �     �  
   �     �     �     �     �     �     �  6   �     ,      D  +   e     �     �  	   �     �     �     �     �     �     
  H        e     z  (   �  0   �  7   �     $  
   *  	   5  "   ?     b       0   �     �     �     �               (     7  1   T     �  
   �      �     �     �     �     �     �     �          3     8      J     k  
   {     �     �     �     �     �     �     �     �     �  %   �  )        H     g     �     �     �  A   �        &   ?     f     k  
   x     �     �     �     �  	   �  	   �     �     �     �     �     �  	             $     2     C  	   J     T     X  	   f     p     �     �     �     �  A   �                 +   .   2      a   A   x      �      �      �      �      �      �      
!     !  	   !     "!     9!     O!     g!     u!     �!     �!     �!     �!     �!     �!  (   "     -"     D"     Z"  �   g"     
#  
   #  
   #     )#     9#     E#     U#     Z#     `#     t#     �#     �#     �#     �#     �#     �#     �#     �#     �#     �#  
   
$     $     &$     3$     D$  
   Q$     \$     l$     �$     �$     �$     �$     �$     �$     �$     �$     %     %  <   +%  k   h%  S   �%     (&     1&     =&  	   R&     \&  C   c&     �&     �&  �  �&     c(  )   e(     �(     �(     �(     �(     �(     �(     �(     �(     �(     �(     �(  
   �(  	   �(     �(     )     )     ')  
   3)     >)  	   G)     Q)     ^)  !   j)     �)     �)     �)  
   �)     �)  	   �)     �)     �)     *     *  	   %*     /*  
   ;*     F*     R*     b*     f*     �*     �*     �*     �*     �*     �*  �   �*  N   �+     �+     �+     �+     ,     .,  
   B,     M,  
   V,  
   a,     l,     {,     �,     �,     �,      �,     �,     �,  
   -  V   -  !   n-  '   �-  B   �-     �-      .     :.     N.     d.  
   v.     �.     �.     �.  q   �.  )   9/  -   c/  K   �/  N   �/  d   ,0  
   �0  
   �0  
   �0  4   �0  3   �0     1  Q   61  "   �1     �1  1   �1  "   �1     2     (2  $   @2  E   e2  	   �2     �2  4   �2     �2     3     3     3     53     O3      k3     �3     �3     �3     �3  
   �3     �3     �3  	   �3  
   4     4     .4     74      K4  !   l4  4   �4  <   �4  .    5  /   /5  )   _5  *   �5  @   �5  d   �5  0   Z6  2   �6     �6     �6     �6  3   �6     #7     57     >7  
   K7     V7     b7     z7     �7     �7     �7     �7     �7     �7  1   8     :8     B8     K8     T8     h8  !   v8  -   �8     �8     �8     �8  E   9     X9     `9     9  E   �9  !   �9  f   �9     ^:     s:     �:     �:     �:     �:  
   �:     �:  
   �:     �:     ;     &;     >;     M;     `;     s;     �;     �;  !   �;     �;  (   <     1<  %   F<     l<  �   �<     `=     m=     �=  #   �=     �=     �=     �=  
   �=  (   �=  &   #>     J>  
   `>     k>  
   �>     �>     �>     �>  #   �>     �>     �>     ?     -?     I?     Z?     v?     �?     �?     �?     �?     �?     @     @  (   -@     V@     n@      �@     �@     �@  K   �@  �   &A  t   �A     CB     UB     ^B     xB  	   �B  l   �B  
   �B     
C     5       �   �       /          �   �                     �   �          �   �       V   �      �           �   �   �      L      3   P                 �   �   �   �   r   &   �   �   m   `   �   �         N   G   #   
   �       �   ?   g   �   �   �   �   �   }   Z   :   �   z   2   8              I   �           ]   �       �          �   W       ,   1           k   �   �   c   �   d   S   \       U               �   �      f       v       �           �   �      �      �   �   �   F      �   �   �   p   �               ^   �   b           �   �   �   (   �          M           %   0   '       �   �   ~   �   �   o   �       �           �   t   �   �   �       |       �   �       �   �   	   �   �   h       �   �   �   �   �   9   n               O   +           e   Y   x   )   �   �   K   w       �   X       H   J      $           @      �       q   -   s   .          l   E   �               �       a   ;   �      �       B       �       u   �   �   �   �   �          �   �      �   R   =      �   �   6   �       {       A       �   �   4   7   Q   �          y       "   *       �   �   i   �   �   �   D   �   �   �                      [   >   �   C   �   �       �   �       �          �   !   <   �   T       �   _   �       �           j   �   �                       % %1 more item %1 more items %1 x %2 %1% %1,%2 %1,%2 - %3,%4 %1:  %1: %2 %1: Smear %1bpp %1x%2 %1x%2 %3 &All &Amount: &Blue &Brightness: &Colors &Delete Selection &Edit &Effect: &Gamma: &Green &Horizontal: &Image &Invert Colors &Monochrome &More Effects... &New: &Percent: &Preview &Red &Redo: %1 &Reset &Resize &Rotate... &Scale &Undo: %1 &Vertical: &View 180 d&egrees 1x1 24-&bit color 24-bit Color 256 Color 256 co&lor 270 de&grees 90 &degrees <empty> <qt><p>The <b>%1</b> format may not be able to preserve all of the image's color information.</p><p>Are you sure you want to save in this format?</p></qt> A document called "%1" already exists.
Do you want to overwrite it? All Angle Autocr&op Autocrop Backslash Balance Blue Bold Brush C&hannels: C&lear C&lockwise C&opy to File... C&ustom: Cannot Paste Channels Chief Investigator Circle Click or drag to erase pixels of the foreground color. Click or drag to erase. Click or drag to spray graffiti. Click to draw dots or drag to draw strokes. Click to fill a region. Click to select a color. Color Box Color Eraser Color Picker Colors Connected Lines Convert &to: Cou&nterclockwise Could not open "%1" - unsupported image format.
The file may be corrupt. Could not open "%1". Could not save as "%1". Could not save image - failed to upload. Could not save image - insufficient information. Could not save image - unable to create temporary file. Curve Dimensions Direction Drag out the start and end points. Drag to draw the first line. Drag to draw. Draw using brushes of different shapes and sizes Draws connected lines Draws curves Draws dots and freehand strokes Draws ellipses and circles Draws lines Draws polygons Draws rectangles and squares Draws rectangles and squares with rounded corners E&nable E&xport... EMAIL OF TRANSLATORSYour emails Ellipse Entire Image Eraser Exact Match Fill with Background Color Fill with Foreground Color Fills regions in the image Flip Flip horizontally Flip horizontally and vertically Flip vertically Flood Fill Font Family Green Height: Icons InputMethod Support Invert Invert Colors Keep &aspect ratio Left click to cancel. Left click to change cursor position. Left drag the handle to resize the image. Left drag to create selection. Left drag to create text box. Left drag to move selection. Left drag to move text box. Left drag to scale selection. Left drag to set the last control point or right click to finish. Let go of all the mouse buttons. Lets you select a color from the image Line Main Toolbar Monochrome NAME OF TRANSLATORSYour names No Fill None Opaque Operation Original: Paste &From File... Paste in &New Window Pen Polygon Preview Quali&ty: R&esize / Scale... R&esize Image R&esize Text Box Re&set Rectangle Red Reduce Colors Reduce To Reduce to &Grayscale Reduce to Grayscale Reloa&d Remove Internal B&order Remove Internal Border Replaces pixels of the foreground color with the background color Rese&t Reset &All Values Resize Resize Image: Let go of all the mouse buttons. Right click to cancel. Right drag to set the last control point or left click to finish. Rotat&e Image Rotat&e Selection Rotate Rounded Rectangle S&kew... S&mooth Scale Scale Scan... Selection Selection (Elliptical) Selection (Free-Form) Selection (Rectangular) Selection: %1 Selection: Create Selection: Delete Selection: Deselect Selection: Move Selection: Scale Selection: Smooth Scale Selection: Transparency Color Selection: Transparency Color Similarity Selection: Transparent Set Zoom Level to %1% Set as Image Setting the zoom level to a value that is not a multiple of 100% results in imprecise editing and redraw glitches.
Do you really want to set to zoom level to %1%? Settings Show &Grid Show &Path Show T&humbnail Sk&ew Image Sk&ew Selection Skew Slash Smooth Scal&e Image Smooth Scal&e Selection Smooth Scale Spraycan Sprays graffiti Square Strike Through Text Text Box Text Toolbar Text: Background Color Text: Backspace Text: Bold Text: Create Box Text: Delete Text: Delete Box Text: Finish Text: Font Text: Font Size Text: Foreground Color Text: Italic Text: Move Box Text: New Line Text: Paste Text: Resize Box Text: Strike Through Text: Swap Colors Text: Transparent Background Text: Underline Text: Write The document "%1" has been modified.
Do you want to save it? The document "%1" has been modified.
Reloading will lose all changes since you last saved it.
Are you sure? The document "%1" has been modified.
Reloading will lose all changes.
Are you sure? Tool Box Transparent URL: %1
Mimetype: %2 Underline Width: You must save this image before sending it.
Do you want to save it? degrees x Project-Id-Version: kolourpaint
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-07-16 03:17+0200
PO-Revision-Date: 2017-05-16 07:02-0400
Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>
Language-Team: Hebrew <kde-i18n-he@kde.org>
Language: he
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Zanata 3.9.6
Plural-Forms: nplurals=2; plural=n != 1;
 % %1 עוד עצמים עוד %1 עצמים %1 x %2 %1% %1,%2 %1,%2 - %3,%4 %1: %1: %2 %1: משטח %1bpp %1x%2 %1x%2 %3 &הכל &כמות: &כחול &בהירות: &צבעים &מחק בחירה &עריכה &אפקט: &גמא: &ירוק &אופקי: &תמונה &הפוך צבעים (נגטיב) &מונוכרום &עוד אפקטים... &חדש &אחוז: &תצוגה מקדימה &אדום &בצע שוב: %1 &אפס &שנה גודל &סובב... &הגדל &בטל: %1 &אנכי: &תצוגה 1&80 מעלות 1x1 24 &סיביות של צבע 24 סיביות של צבע 256 צבעים 256 צ&בעים 2&70 מעלות 90 &מעלות <ריק> <qt> <p>הפורמט <b>%1</b> עלול לא להכיל את כל המידע אודות צבעי התמונה. </p> <p>האם ברצונך לשמור בפורמט הזה?</p></qt> מסמך בשם "%1" כבר קיים.
האם ברצונך לשכתב אותו? הכל הטיה &חיתוך אוטומטי חיתוך אוטומטי לוכסן הפוך איזון כחול מודגש מברשת ע&רוצים: &נקה &בכיוון השעון ה&דבק לקובץ... &מותאם אישית: אין אפשרות להדביק ערוצים בודק ראשי עיגול לחץ או גרור בשביל למחוק פיקסלים מתוך צבעי החזית לחץ או גרור למחיקה לחץ או גרור לציור קיר. לחץ כדי לצייר נקודות וקווים חופשיים. לחץ ומלא אזורים. לחץ כדי לבחור צבע. תיבת צבעים מחיקת צבעים בחירת צבע צבעים קווים מחוברים המר &אל: &נגד כיוון השעון אין אפשרות לפתוח את "%1" - פורמט התמונה לא נתמך.
הקובץ כנראה פגום. אין אפשרות לפתוח את "%1". אין אפשרות לשמור בתור "%1". אין אפשרות לשמור את התמונה - השליחה נכשלה. אין אפשרות לשמור את התמונה - אין מספיק מידע. אין אפשרות לשמור את התמונה - אין אפשרות ליצור קובץ זמני. עקומה ממדים כיוון גרור את נקודות ההתחלה והסיום גרור כדי לצייר את הקו הראשון גרור כדי לצייר ציור בעזרת מברשת של צורות שונות בגדלים שונים צייר קווים מחוברים צייר עקומה צייר נקודות וקווים חופשיים צייר אליפסה ועיגול צייר קווים צייר פוליגון צייר מלבנים וריבעים מצייר מלבנים וריבעים עם פינות מעוגלות &אפשר יי&צא... Garry@Nunex.co.il, koala@linux.net, weliad@gmail.com אליפסה כל התמונה מחק התאמה מדויקת מלא עם צבע רקע מלא עם צבע חזית מלא אזורים בתמונה הפוך הפוך אופקי הפוך אופקי ואנכי הפוך אנכי מילוי סוג כתב ירוק גובה: סמלים תמיכה בשיטת קלט הפוך הפוך צבעים שמור על &יחס נוכחי לחצן שמאלי לביטול. לחצן שמאלי לשינוי מיקום הסמן גרור את הידית לשינוי גודל התמונה. גרור כדי ליצור תחום בחירה גרור כדי ליצור קופסת טקסט. גרור להזזת תחום הבחירה גרור להזזת קופסת הטקסט. גרורה שמאלית כדי לשנות גודל הבחירה. גרירה שמאלית נקודת השליטה האחרונה או לחציה ימנית לסיום נא לשחרר את כל לחצני העכבר. מאפשר לבחור צבע מתוך התמונה קו סרגל כלים ראשי מונוכרום גארי לכמן, Koala, ליעד ויינברגר ללא מילוי כלום מעורפל פעולה מקורי: הדבק &מקובץ... הדבק בחלון &חדש עט פוליגון תצוגה מקדימה אי&כות: &שנה גודל/הגדל... &שנה גודל תמונה &שינוי הגודל של קופסת הטקסט &אפס מלבן אדום הקטן צבעים הקטן אל המרה ל&גווניי אפור המרת הצבעים לגווניי אפור &טען מחדש הסר גבול פ&נימי הסר מסגרת פנימית החלף פיקסלים של צבע החזית באלה של הרקע &אפס אפס &את כל הערכים שנה גודל שינוי גודל: נא לשחרר את כל לחצני העכבר. לחצן ימיני לביטול. גרירה ימנית לנקודת השליטה האחרונה או לחציה שמאלית לסיום &סובב תמונה סיבוב &בחירה סובב מלבן מעוגל &הנטה... &הגדלה עדינה הגדלה סרוק... בחירה בחירה (אליפסה) בחירת (מצב חופשי) בחירה (מלבני) בחירה: %1 בחירה: צור בחירה: מחק בחירה: בטל בחירה בחירה: הזזה בחירה: שינוי גודל בחירה: הגדלה עדינה בחירה: צבע שקיפות בחירה: שקיפות צבע דומה בחירה: שקוף קבע רמת התקרבות אל %1% קבע בתור תמונה קביעת רמת התקריב לרמה שאינה כפולה של 100% מסתכמת בעריכה לא מדויקת וגלישות בציור מחדש.
האם ברצונך לקבוע את רמת התקריב ל־%1%? הגדרות הצג קווי &מתאר &הצג נתיב הצג &תצוגות מקדימות &הגדל תמונה הגדל &בחירה נטוי לוכסן הגדלה &עדינה של התמונה &הגדלה עדינה של בחירה הגדלה עדינה ספריי ציור קיר (גרפיטי) מרובע קו חותך טקסט קופסת טקסט סרגל כלים עבור טקסט טקסט: צבע רקע טקסט: מחיקה אות טקסט: הדגשה טקסט: צור קופסה טקסט: מחק טקסט: מחק קופסה טקסט: סיום טקסט: כתב טקסט: גודל כתב טקסט: צבע חזית טקסט: כתב נטוי טקסט: הזז קופסה טקסט: קו חדש טקסט: הדבק טקסט: שינוי גודל קופסה טקסט: קו חותך טקסט: החלף צבעים טקסט: צבע רקע שקוף טקסט: קו תחתון טקסט: כתוב המסמך "%1" נערך.
האם ברצונך לשמור את השינוי? המסמך "%1" שונה.
טעינה מחדש תגרום לאיבוד כל השינויים שנעשו עד השמירה האחרונה שלך.
האם אתה בטוח? המסמך "%1" שונה.
טעינה מחדש תגרום לאיבוד כל השינויים.
האם אתה בטוח? ארגז כלים שקוף כתובת: %1
סוג: %2 קו תחתון רוחב: אתה חייב לשמור את התמונה לפני השליחה.
האם ברצונך לשמור אותה? מעלות x 