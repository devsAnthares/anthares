��    *      l  ;   �      �     �     �  )   �               /     H     ^  #   ~     �  
   �     �     �  %   �  +        E     Z     m  @   z     �     �     �     �               '     ,     9     ?     _     e  .   m     �  F   �  (   �  	   '  	   1  	   ;  '   E  7   m  "   �  �  �     r	      �	  4   �	     �	     �	  	   �	     �	     �	     
     .
     B
     S
     i
     �
  \   �
     �
          #  K   /     {     �     �  #   �  +   �                    .     4  
   P     [  D   h  #   �  m   �  0   ?     p     �     �     �     �  
   �                   $                                 %                      *                                               	   #   )   "             &           
                        '             (   !    &Do not remember @actionWalk through activities @actionWalk through activities (Reverse) @action:buttonApply @action:buttonCancel @action:buttonChange... @action:buttonCreate @title:windowActivity Settings @title:windowCreate a New Activity @title:windowDelete Activity Activities Activity information Activity switching Are you sure you want to delete '%1'? Blacklist all applications not on this list Clear recent history Create activity... Description: Error loading the QML files. Check your installation.
Missing %1 For a&ll applications Forget a day Forget everything Forget the last hour Forget the last two hours General Icon Keep history Name: O&nly for specific applications Other Privacy Private - do not track usage for this activity Remember opened documents: Remember the current virtual desktop for each activity (needs restart) Shortcut for switching to this activity: Shortcuts Switching Wallpaper for in 'keep history for 5 months'for  unit of time. months to keep the history month  months unlimited number of monthsforever Project-Id-Version: kcm_activities5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2017-05-16 11:33-0400
Last-Translator: Elkana Bardugo <ttv200@gmail.com>
Language-Team: Hebrew <kde-i18n-doc@kde.org>
Language: he
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Zanata 3.9.6
 אל תזכור עבור בין פעילויות עבור בין פעילויות (בסדר הפוך) החל בטל שנה... צור הגדרות פעילות צור פעילות חדשה מחק פעילות פעילויות מידע פעילות החלפת פעילות למחוק את '%1'? הוסף לרשימה השחורה את כל היישומים שאינם ברשימה הזו נקה היסטוריה צור פעילות... תיאור: נכשל בטעינת קובצי QML. בדוק את ההתקנה.
%1 חסר לכל הישומים שכח את היום שכח הכל שכח את השעה האחרונה שכח את השעתיים האחרונות כללי סמל שמור היסטוריה שם: לישומים נבחרים אחרים פרטיות פרטי – אל תעקוב אחר השימוש בפעילות זו זכור מסמכים פתוחים: זכור את השולחן העבודה הוירטואלי לכל פעילות (דורש הפעלה מחדש) קיצור דרך להחלפת פעילויות: קיצורי דרך החלף תמונת רקע של חודש חודשים לתמיד 