��    
      l      �       �   $   �   %     :   <     w  '   �  -   �     �       '     �  <  i   �  f   K  c   �  l     �   �  T     H   `  B   �  S   �         	                            
        Could not detect the file's mimetype Could not find all required functions Could not find the provider with the specified destination Error trying to execute script Invalid path for the requested provider It was not possible to read the selected file Service was not available Unknown Error You must specify a URL for this service Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-11-27 11:55+0700
Last-Translator: Phuwanat Sakornsakolpat <narachai@gmail.com>
Language-Team: Thai <thai-l10n@googlegroups.com>
Language: th
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=1; plural=0;
 ไม่สามารถตรวจหาชนิดแฟ้ม MIME ของแฟ้มได้ ไม่พบคุณสมบัติที่ต้องการใช้ทั้งหมด ไม่พบผู้ให้บริการในปลายทางที่ระบุ เกิดข้อผิดพลาดขณะสั่งให้สคริปต์ทำงาน ที่ตั้งไม่ถูกต้องสำหรับผู้ให้บริการที่ร้องข้อ ไม่สามารถอ่านแฟ้มที่เลือกได้ ไม่สามารถใช้งานบริการได้ ข้อผิดพลาดที่ไม่รู้จัก คุณต้องระบุ URL สำหรับบริการนี้ 