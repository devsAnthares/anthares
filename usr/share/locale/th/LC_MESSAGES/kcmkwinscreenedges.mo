��          �            x     y     }     �     �     �  S   �  w   !  M   �     �  	   �     �          %     2     R  �  d  "     k   *  S   �  S   �  *   >  �   i  �   =  �        �  '   �  i   	  .   �	  0   �	  $   �	  3   
                                              	          
                        ms &Reactivation delay: &Switch desktop on edge: Activation &delay: Always Enabled Amount of time required after triggering an action until the next trigger can occur Amount of time required for the mouse cursor to be pushed against the edge of the screen before the action is triggered Change desktop when the mouse cursor is pushed against the edge of the screen Lock Screen No Action Only When Moving Windows Other Settings Show Desktop Switch desktop on edgeDisabled Window Management Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-30 03:13+0100
PO-Revision-Date: 2010-03-25 11:46+0700
Last-Translator: Thanomsub Noppaburana <donga.nb@gmail.com>
Language-Team: Thai <thai-l10n@googlegroups.com>
Language: th
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=1; plural=0;
  มิลลิวินาที หน่วงเวลา&ก่อนการเรียกใช้งานอีกครั้ง: &สลับพื้นที่ทำงานเมื่อแตะขอบ: &หน่วงเวลาก่อนการเรียกใช้งาน: เปิดใช้งานเสมอ ช่วงเวลารอนับตั้งแต่เริ่มการกระทำหลังสุดไปแล้ว ก่อนจะเริ่มการกระทำถัดไป ช่วงเวลานับตั้งแต่ที่เลื่อนเมาส์ไปยังตำแหน่งขอบจอ ก่อนจะเริ่มการกระทำ เปลี่ยนพื้นที่ทำงานเมื่อเคลื่อนเคอร์เซอร์เมาส์มาจนถึงขอบของหน้าจอ ล็อคหน้าจอ ไม่มีการกระทำ เฉพาะเมื่อมีการย้ายหน้าต่างเท่านั้น การตั้งค่าอื่น ๆ แสดงพื้นที่ทำงาน ปิดการใช้งาน การจัดการหน้าต่าง 