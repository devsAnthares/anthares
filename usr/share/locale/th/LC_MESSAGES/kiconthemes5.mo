��          �      <      �  
   �     �  >   �            
        $     ,     4     ;  	   G     Q     _     f  2   u     �     �  �  �     d     x  O   �     �     �     	     "  '   8  -   `  $   �  -   �  "   �       &        E     �     �                   
                                                                          	        &Browse... &Search: *.png *.xpm *.svg *.svgz|Icon Files (*.png *.xpm *.svg *.svgz) Actions Applications Categories Devices Emblems Emotes Icon Source Mimetypes O&ther icons: Places S&ystem icons: Search interactively for icon names (e.g. folder). Select Icon Status Project-Id-Version: kio4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:11+0100
PO-Revision-Date: 2010-12-31 00:33+0700
Last-Translator: Phuwanat Sakornsakolpat <narachai@gmail.com>
Language-Team: Thai <thai-l10n@googlegroups.com>
Language: th
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=1; plural=0;
 เลือ&ก... &ค้นหา: *.png *.xpm *.svg *.svgz|แฟ้มไอคอน (*.png *.xpm *.svg *.svgz) การกระทำ โปรแกรม หมวดหมู่ อุปกรณ์ ป้ายสัญลักษณ์ ไอคอนสื่ออารมณ์ ตำแหน่งไอคอน ประเภท Mime ของแฟ้ม ไ&อคอนอื่น ๆ : ที่หลัก ๆ ไอคอนของ&ระบบ: ค้นหาแบบโต้ตอบสำหรับชื่อไอคอน (เช่น โฟลเดอร์) เลือกไอคอน สถานะ 