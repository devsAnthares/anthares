��          �      L      �  m   �     /  "   <  F   _  F   �  F   �  J   4  N     R   �     !  %   2  $   X  #   }  $   �  %   �  &   �  �        �  �  �  9   Q  *   �  ]   �        	  �   -
      �   ,  �   *  '        A  *   W     �     �     �  -   �  B   �  B   )                                
                          	                                          Close the encrypted container; partitions inside will disappear as they had been unpluggedLock the container Eject medium Finds devices whose name match :q: Lists all devices and allows them to be mounted, unmounted or ejected. Lists all devices which can be ejected, and allows them to be ejected. Lists all devices which can be mounted, and allows them to be mounted. Lists all devices which can be unmounted, and allows them to be unmounted. Lists all encrypted devices which can be locked, and allows them to be locked. Lists all encrypted devices which can be unlocked, and allows them to be unlocked. Mount the device Note this is a KRunner keyworddevice Note this is a KRunner keywordeject Note this is a KRunner keywordlock Note this is a KRunner keywordmount Note this is a KRunner keywordunlock Note this is a KRunner keywordunmount Unlock the encrypted container; will ask for a password; partitions inside will appear as they had been plugged inUnlock the container Unmount the device Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-12-17 21:17+0700
Last-Translator: Thanomsub Noppaburana <donga.nb@gmail.com>
Language-Team: Thai <thai-l10n@googlegroups.com>
Language: th
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.1
Plural-Forms: nplurals=1; plural=0;
 ล็อคส่วนบรรจุข้อมูล เด้งเอาสื่อออก หาอุปกรณ์ต่าง ๆ ที่เข้าเงื่อนไข :q: เรียกดูรายการอุปกรณ์ทั้งหมดและอนุญาตให้มันสามารถเมานท์, ยกเลิกการเมานท์ หรือเด้งสื่อออกได้ เรียกดูรายการอุปกรณ์ทั้งหมดที่สามารถจะเด้งสื่อออกได้ และอนุญาตให้มันสามารถเด้งเอาสื่อออกได้ เรียกดูรายการอุปกรณ์ทั้งหมดที่สามารถจะถูกเมานท์ได้ และอนุญาตให้มันสามารถเมานท์ได้ เรียกดูรายการอุปกรณ์ทั้งหมดที่สามารถจะยกเลิกเมานท์ได้ และอนุญาตให้มันสามารถยกเลิกเมานท์ได้ เรียกดูรายการอุปกรณ์ทั้งหมดที่สามารถจะถูกล็อคได้ และอนุญาตให้มันสามารถทำการถูกล็อคได้ เรียกดูรายการอุปกรณ์ทั้งหมดที่สามารถจะปลดล็อคได้ และอนุญาตให้มันสามารถปลดล็อคได้ เมานท์อุปกรณ์ อุปกรณ์ เด้งเอาสื่อออก ล็อค เมานท์ ปลดล็อค ยกเลิกการเมานท์ ปลดล็อคส่วนบรรจุข้อมูล ยกเลิกการเมานท์อุปกรณ์ 