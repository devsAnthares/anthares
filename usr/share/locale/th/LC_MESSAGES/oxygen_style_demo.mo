��    }        �   �      �
  	   �
     �
     �
     �
     �
     �
     �
     �
  
   �
     �
     �
     �
  
                  &  7   .     f     n     {     �  
   �  
   �  	   �     �     �  
   �     �  ,   �  /        O     U  
   \     g     p  
   |     �  
   �     �     �     �     �  
   �     �     �     �     �     �               !     (     .     2     5     :     B     N     V     g     o     {     �     �     �     �     �     �     �     �     �     �                     ;     G     Z  B   q     �     �  /   �  =     #   V  *   z     �     �     �     �     �     �  
   �     �             	        $     5  
   D     O     \  
   n  
   y  	   �     �     �  
   �     �     �     �     �     �     �     �            	        "     +     <     M     V  
   [     f  �  o          >     K     j     �     �  $   �     �  6   �       !   >     `  !        �  ?   �     �  �        �  -   �       '   #     K     g     }  '   �  E   �     �  	     	   $  	   .     8     E  !   R  '   t  -   �     �     �  '   �  3   #     W     d     y  '   �     �     �     �     �  U        e     r     �     �  	   �     �     �     �     �     �  1     $   A     f     y  	   �  	   �  $   �     �  <   �  K   '     s      �  )   �     �     �  )     G   1     y  6   �  ?   �  �     !   �  B   �  �      �   �  c   9  f   �  X         ]      y      �   	   �   %   �   !   �   3   �      !!  0   .!  -   _!  -   �!  .   �!  .   �!      "  )   :"     d"     �"     �"  )   �"  G   �"     !#  B   ?#     �#     �#     �#  -   �#  *   �#     *$     1$     J$     ]$  '   m$     �$  '   �$     �$     �$     %     %             4           _         ^         s                '   )   }       a       <       l   f   z          =   .   d   M       %   J      7   A   C   q      $          i       >   g   F           o   K   t      (   8       x   @      Z   *   B       k   n       e   p      Q             &   E      \   6   ?   9       v   W              :                   /   y   b   R   !       j   5   c      h   N   L   X   +   O   G   H       1   P       S   U   -   T   w   r   |           #   u          I               D   
                            [       	   `   ,           V   3   Y      {   ;       "   m      2   ]            0        Benchmark Bottom Bottom to Top Bottom-left Bottom-right Buttons Cascade Center Checkboxes Description Dialog Document mode Down Arrow East Editable combobox Editors Emulates user interaction with widgets for benchmarking Enabled Example text First Column First Description First Item First Page First Row First Subitem First Subitem Description First item Flat Flat frame. No frame is actually drawn.Flat Flat group box. No frame is actually drawnFlat Frame Frames Grab mouse GroupBox Hide tabbar Horizontal Huge (48x48) Icons Only Input Widgets Large Large (32x32) Left  Left Arrow Left to Right Lists Medium (22x22) Modules Multi-line text editor: New New Row Normal North Off On Open Options Oxygen Demo Partial Password editor: Preview Pushbuttons Radiobuttons Raised Right Right Arrow Right to Left Right to left layout Run Simulation Save Second Column Second Description Second Item Second Page Second Subitem Second Subitem Description Second item Select Next Window Select Previous Window Select below the modules for which you want to run the simulation: Show Corner Buttons Shows the appearance of buttons Shows the appearance of lists, trees and tables Shows the appearance of sliders, progress bars and scrollbars Shows the appearance of tab widgets Shows the appearance of text input widgets Single line text editor: Sliders Small Small (16x16) South Spinbox: Tab Widget Tab Widgets Tabs Text Alongside Icons Text Only Text Under Icons Text and icon: Text only: Third Column Third Description Third Item Third Page Third Row Third Subitem Third Subitem Description Third item This is a sample text Tile Title Title: Toolbox Toolbuttons Top Top to Bottom Top-left Top-right Up Arrow Use flat buttons Use flat widgets Vertical West Wrap words password Project-Id-Version: kstyle_config
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-16 03:34+0200
PO-Revision-Date: 2010-12-17 20:18+0700
Last-Translator: Thanomsub Noppaburana <donga.nb@gmail.com>
Language-Team: Thai <thai-l10n@googlegroups.com>
Language: th
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.1
Plural-Forms: nplurals=1; plural=0;
 เบนช์มาร์ก ล่าง ล่างขึ้นบน ซ้ายล่าง ล่างขวา ปุ่มต่าง ๆ เรียงลดหลั่น กลาง กล่องกาเครื่องหมาย รายละเอียด กล่องโต้ตอบ โหมดเอกสาร ปุ่มลูกศรลง ตะวันออก กล่องคอมโบแบบแก้ไขได้ ตัวแก้ไข จำลองการใช้งานของผู้ใช้กับวิดเจ็ต เพื่อวัดค่าเบนช์มาร์ก เปิดใช้งาน ข้อความตัวอย่าง คอลัมน์แรก รายละเอียดแรก รายการแรก หน้าแรก แถวแรก รายการย่อยแรก รายละเอียดรายการย่อยแรก รายการแรก แบน แบน แบน เฟรม เฟรม ดักจับเมาส์ กล่องจัดกลุ่ม ซ่อนแถบแสดงแท็บ แนวนอน ใหญ่มาก (48x48) ไอคอนเท่านั้น วิดเจ็ตป้อนข้อมูล ใหญ่ ใหญ่ (32x32) ซ้าย  ปุ่มลูกศรซ้าย ซ้ายไปขวา ช่องรายการ กลาง (22x22) มอดูล ตัวแก้ไขข้อความแบบหลายบรรทัด: ใหม่ แถวใหม่ ปกติ เหนือ ปิด เปิด เปิด ตัวเลือก สาธิต Oxygen บางส่วน ตัวแก้ไขรหัสผ่าน: แสดงตัวอย่าง ปุ่มกด ปุ่มวิทยุ นูน ขวา ปุ่มลูกศรขวา ขวาไปซ้าย เค้าโครงแบบขวาไปซ้าย เรียกใช้การจำลองสถานการณ์ บันทึก คอลัมน์ที่ 2 รายละเอียดที่ 2 รายการที่ 2 หน้าที่ 2 รายการย่อยที่ 2 รายละเอียดรายการย่อยที่ 2 รายการที่ 2 เลือกหน้าต่างถัดไป เลือกหน้าต่างก่อนหน้า เลือกทางด้านล่างมอดูลว่าต้องการจะให้ทำการจำลองสถานการณ์ใด: แสดงปุ่มมุม แสดงรูปลักษณ์ของปุ่มกด แสดงรูปลักษณ์ของช่องรายการ, ผังต้นไม้ และตาราง แสดงรูปลักษณ์ของแถบเลื่อน, แถบความคืบหน้า และแถบลาก แสดงรูปลักษณ์ของวิดเจ็ตประเภทแท็บ แสดงรูปลักษณ์ของวิดเจ็ตป้อนข้อความ ตัวแก้ไขข้อความแบบบรรทัดเดียว: แถบเลื่อน เล็ก เล็ก (16x16) ใต้ กล่องปรับเลข: แท็บวิดเจ็ต วิดเจ็ตประเภทแท็บ แท็บ ข้อความข้างไอคอน ข้อความเท่านั้น ข้อความใต้ไอคอน ข้อความและไอคอน: ข้อความเท่านั้น: คอลัมน์ที่ 3 รายละเอียดที่ 3 รายการที่ 3 หน้าที่ 3 แถวที่ 3 รายการย่อยที่ 3 รายละเอียดรายการย่อยที่ 3 รายการที่ 3 นี่เป็นข้อความตัวอย่าง ปูเรียง หัวเรื่อง หัวเรื่อง: กล่องเครื่องมือ ปุ่มเครื่องมือ บน บนลงล่าง ซ้ายบน ขวาบน ปุ่มลูกศรขึ้น ใช้ปุ่มแบน ใช้วิดเจ็ตแบน แนวตั้ง ตะวันตก ตัดคำ รหัสผ่าน 