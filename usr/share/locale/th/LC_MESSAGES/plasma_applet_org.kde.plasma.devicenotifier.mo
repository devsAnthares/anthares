��          �            h  $   i  �   �       4        S  #   m  �   �  �     +   �     
          :  �   Q  $   �  �  �  '   �  0   �  *     |   >  E   �  o     u  q  �  �	  �   �  Y   @  s   �  j     $   y  {   �                                               
           	                      @info:status Free disk space%1 free Accessing is a less technical word for Mounting; translation should be short and mean 'Currently mounting this device'Accessing... All devices Click to access this device from other applications. Click to eject this disc. Click to safely remove this device. It is currently <b>not safe</b> to remove this device: applications may be accessing it. Click the eject button to safely remove this device. It is currently <b>not safe</b> to remove this device: applications may be accessing other volumes on this device. Click the eject button on these other volumes to safely remove this device. It is currently safe to remove this device. No Devices Available Non-removable devices only Removable devices only Removing is a less technical word for Unmounting; translation shoud be short and mean 'Currently unmounting this device'Removing... This device is currently accessible. Project-Id-Version: plasma_applet_devicenotifier
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2010-12-17 16:09+0700
Last-Translator: Thanomsub Noppaburana <donga.nb@gmail.com>
Language-Team: Thai <thai-l10n@googlegroups.com>
Language: th
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 1.1
 เนื้อที่ว่าง %1 กำลังเข้าใช้งาน... อุปกรณ์ทั้งหมด คลิกเพื่อเข้าถึงอุปกรณ์นี้จากโปรแกรมต่าง ๆ คลิกเพื่อดันดิสก์นี้ออก คลิกเพื่อถอดอุปกรณ์นี้ออกอย่างปลอดภัย การถอดอุปกรณ์นี้ออก<b>ไม่ปลอดภัย</b> เนื่องจากอาจมีบางโปรแกรมใช้งานมันอยู่ กรุณาคลิกที่ปุ่มเอาออก เพื่อถอดอุปกรณ์นี้ออกอย่างปลอดภัย การถอดอุปกรณ์นี้ออก<b>ไม่ปลอดภัย</b> เนื่องจากอาจมีบางโปรแกรมกำลังใช้งานบางโวลุมของอุปกรณ์นี้อยู่ กรุณาคลิกปุ่มเอาออก บนโวลุมอื่น เพื่อถอดอุปกรณ์นี้ออกอย่างปลอดภัย คุณสามารถเอาอุปกรณ์นี้ออกได้อย่างปลอดภัยแล้ว ยังไม่มีอุปกรณ์ใด ๆ ให้เลือกใช้ อุปกรณ์ที่ไม่สามารถถอด/เสียบได้เท่านั้น อุปกรณ์ที่สามารถถอด/เสียบได้เท่านั้น กำลังถอดออก... อุปกรณ์นี้สามารถเข้าใช้งานได้แล้วในตอนนี้ 