��          �      �       0  "   1     T     d  �   k  �   �  Z   u     �     �     �     �     �       �  #     �  ?   �  !       A  M  R  �   �  K   q  '   �     �     �  C   	  :   X	     	                                
                     @item:inlistbox Button size:Large Animate buttons Center Check this option if the window border should be painted in the titlebar color. Otherwise it will be painted in the background color. Check this option if you want the buttons to fade in when the mouse pointer hovers over them and fade out again when it moves away. Check this option if you want the titlebar text to have a 3D look with a shadow behind it. Colored window border Config Dialog Left Right Title &Alignment Use shadowed &text Project-Id-Version: kwin_clients
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-02 03:16+0100
PO-Revision-Date: 2010-07-07 15:12+0700
Last-Translator: Thanomsub Noppaburana <donga.nb@gmail.com>
Language-Team: Thai <thai-l10n@googlegroups.com>
Language: th
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.1
Plural-Forms: nplurals=1; plural=0;
 ใหญ่ แสดงปุ่มแบบเคลื่อนไหว จัดกึ่งกลาง เปิดใช้ตัวเลือกนี้ หากต้องการให้วาดกรอบหน้าต่าง
โดยใช้สีเดียวกันกับสีของแถบหัวเรื่องหน้าต่าง เปิดใช้ตัวเลือกนี้ หากต้องการให้ปุ่มมีการปรับความเลือน/ชัด
เมื่อมีการเลื่อนตัวชี้ของเมาส์มาอยู่เหนือปุ่มเหล่านั้น เปิดใช้ตัวเลือกนี้ หากต้องการให้ข้อความในแถบหัวเรื่องหน้าต่างมีเงาด้วย เติมสีให้กรอบหน้าต่างด้วย กล่องปรับแต่ง ด้านซ้าย ด้านขวา การจัดตำแหน่งหัวเ&รื่อง แสดง&ข้อความแบบมีเงา 