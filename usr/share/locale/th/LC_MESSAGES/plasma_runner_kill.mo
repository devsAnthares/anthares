��          �            h     i     r     �  	   �  *   �     �  "   �                 ;   +     g     z       �  �  (   *     S  %   q  ?   �  ]   �  ?   5  {   u  #   �  #     0   9  �   j  T   
  	   _     i     	                 
                                                          &Sort by &Trigger word: &Use trigger word CPU usage It is not sure, that this will take effect Kill Applications Config Process ID: %1
Running as user: %2 Send SIGKILL Send SIGTERM Terminate %1 Terminate running applications whose names match the query. inverted CPU usage kill nothing Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-02-11 23:33+0700
Last-Translator: Thanomsub Noppaburana <donga.nb@gmail.com>
Language-Team: Thai <thai-l10n@googlegroups.com>
Language: th
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=1; plural=0;
 &เรียงลำดับตาม &คำกระตุ้น: ใ&ช้คำกระตุ้น การใช้ตัวประมวลผลกลาง ไม่แน่ใจว่ามันจะได้รับผลหรือไม่ ปรับแต่งการฆ่าโปรแกรม โพรเซสหมายเลข: %1
กำลังทำงานโดยสิทธิ์ผู้ใช้: %2 ส่งสัญญาณ SIGKILL ส่งสัญญาณ SIGTERM ยุติการทำงานของ %1 ยุติโปรแกรมที่ทำงานอยู่ที่มีชื่อเข้าเงื่อนไขกับการค้น กลับค่าการใช้ตัวประมวลผลกลาง ฆ่า ไม่ทำอะไร 