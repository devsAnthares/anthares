��    %      D  5   l      @  m   A     �     �     �  	   �     �          "  Y   (     �      �     �  
   �     �     �     �                    !     @     \     c     k  	   w  
   �     �  	   �  
   �  
   �     �  	   �     �     �  %   �     %  �  @  Z  �  I   H	  3   �	     �	  '   �	  ;   �	  O   8
  $   �
  �   �
  0   �  %   �  -   �  0   	  B   :  -   }  ?   �     �            d   #  7   �  !   �     �  -   �  0   &  -   W  ]   �  0   �  -     $   B  K   g  !   �     �  3   �  o     N   �        $                                                           
                  #                    %                                            	   !       "                    A list of Phonon Backends found on your system.  The order here determines the order Phonon will use them in. Apply Device List To... Backend Colin Guthrie Connector Copyright 2006 Matthias Kretz Default/Unspecified Category Defer Defines the default ordering of devices which can be overridden by individual categories. Device Preference EMAIL OF TRANSLATORSYour emails Front Center Front Left Front Left of Center Front Right Front Right of Center Hardware Matthias Kretz Mono NAME OF TRANSLATORSYour names Phonon Configuration Module Prefer Profile Rear Center Rear Left Rear Right Show advanced devices Side Left Side Right Sound Card Speaker Placement and Testing Subwoofer Test Unknown Channel no preference for the selected device prefer the selected device Project-Id-Version: kcm_phonon
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-12-17 00:32+0700
Last-Translator: Thanomsub Noppaburana <donga.nb@gmail.com>
Language-Team: Thai <thai-l10n@googlegroups.com>
Language: th
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.1
Plural-Forms: nplurals=1; plural=0;
 รายการโปรแกรมเบื้องหลังสำหรับ Phonon บนระบบของคุณ  โดยลำดับที่ตรงนี้ จะเป็นการบอกถึงลำดับของอุปกรณ์ที่ Phonon จะเลือกใช้ก่อนหลัง ปรับใช้รายการอุปกรณ์กับ ... โปรแกรมเบื้องหลัง Colin Guthrie ส่วนเชื่อมต่อ สงวนลิขสิทธิ์ 2006 Matthias Kretz หมวดหมู่ปริยาย/ไม่มีการระบุ อนุโลมให้ใช้ กำหนดลำดับของอุปกรณ์ปริยาย ซึ่งสามารถนำไปบังคับใช้แยกกับแต่ละหมวดหมู่ได้ อุปกรณ์ที่ควรใช้ drrider@gmail.com, donga.nb@gmail.com ตรงกลางด้านหน้า ด้านหน้าฝั่งซ้าย ด้านหน้าซ้ายของตรงกลาง ด้านหน้าฝั่งขวา ด้านหน้าขวาของตรงกลาง ฮาร์ดแวร์ Matthias Kretz โมโน สหชาติ อนุกูลกิจ, ถนอมทรัพย์ นพบูรณ์ มอดูลการปรับแต่ง Phonon ควรเลือกใช้ โพรไฟล์ ด้านหลังตรงกลาง ด้านหลังฝั่งซ้าย ด้านหลังฝั่งขวา แสดงส่วนควบคุมขั้นสูงของอุปกรณ์ ด้านข้างฝั่งซ้าย ด้านข้างฝั่งขวา แผงวงจรเสียง การจัดวางลำโพงและการทดสอบ ซับวูฟเฟอร์ ทดสอบ ไม่ทราบช่องสัญญาณ ไม่มีการปรับแต่งสำหรับอุปกรณ์ที่เลือก กำหนดใช้งานอุปกรณ์ที่เลือก 