��          �            h  &   i  +   �     �  	   �     �     �     �     �     �  (        7  ,   N     {     �  �  �  �   2  �        �     �  K   �  	   >     H     U     b  �   �  l     �   �  '     '   5                                                                  	   
           Do you want to suspend to RAM (sleep)? Do you want to suspend to disk (hibernate)? General Hibernate Hibernate (suspend to disk) Leave Leave... Lock Lock the screen Logout, turn off or restart the computer Sleep (suspend to RAM) Start a parallel session as a different user Suspend Switch user Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2010-12-17 16:12+0700
Last-Translator: Thanomsub Noppaburana <donga.nb@gmail.com>
Language-Team: Thai <thai-l10n@googlegroups.com>
Language: th
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.1
Plural-Forms: nplurals=1; plural=0;
 คุณต้องการจะปิดพักเครื่องโดยเก็บสถานะไว้ในหน่วยความจำ (สถานะหลับ) หรือไม่ ? คุณต้องการจะปิดพักเครื่องโดยเก็บสถานะไว้ในดิสก์ (สถานะจำศีล) หรือไม่ ? ทั่วไป จำศีล จำศีล (เก็บสถานะไว้ในดิสก์) ออก ออก... ล็อค ล็อคหน้าจอ ออกจากระบบ, ปิดเครื่อง หรือเริ่มระบบคอมพิวเตอร์ใหม่ พักเครื่อง (เก็บสถานะไว้ในหน่วยความจำ) เริ่มวาระงานคู่ขนานโดยเข้าใช้เป็นผู้ใช้งานอื่น ปิดพักเครื่อง สลับผู้ใช้งาน 