��          �      |      �  J   �     <     S  ~   [  A   �       )   )     S     d      u     �  
   �     �     �     �      �  "        8  	   R     \     h  �  p       >        X  ]  r  �   �     �     �  -   A	  *   o	     �	     �	     �	  4   �	  %   
  '   .
  [   V
  ]   �
  O        `  !   z     �                                          	                                               
        %1 is the name of a detail about the current action provided by polkit%1: (c) 2009 Red Hat, Inc. Action: An application is attempting to perform an action that requires privileges. Authentication is required to perform this action. Another client is already authenticating, please try again later. Application: Authentication failure, please try again. Click to edit %1 Click to open %1 EMAIL OF TRANSLATORSYour emails Jaroslav Reznik Maintainer NAME OF TRANSLATORSYour names Password for %1: Password for root: Password or swipe finger for %1: Password or swipe finger for root: Password or swipe finger: Password: Select User Vendor: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:22+0100
PO-Revision-Date: 2010-12-11 16:14+0700
Last-Translator: Phuwanat Sakornsakolpat <narachai@gmail.com>
Language-Team: Thai <thai-l10n@googlegroups.com>
Language: th
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=1; plural=0;
 %1: สงวนลิขสิทธิ์ (c) 2009 Red Hat, Inc. การกระทำ: โปรแกรมได้พยายามจะทำการกระทำที่ต้องการสิทธิ์ในการทำงานเพิ่มเติม จึงจะต้องทำการตรวจสอบสิทธิ์เพื่อใช้ในการทำการกระทำนี้ มีไคลเอนต์อื่นที่กำลังทำการตรวจสอบสิทธิ์อยู่ โปรดลองใหม่อีกครั้งในภายหลัง โปรแกรม: การตรวจสอบสิทธิ์ล้มเหลว โปรดลองใหม่อีกครั้ง คลิกเพื่อแก้ไข %1 คลิกเพื่อเปิด %1 donga.nb@gmail.com Jaroslav Reznik ผู้ดูแล ถนอมทรัพย์ นพบูรณ์ รหัสผ่านของ %1: รหัสผ่านของ root: รหัสผ่านหรืออ่านลายนิ้วมือของ %1: รหัสผ่านหรืออ่านลายนิ้วมือของ root: รหัสผ่านหรืออ่านลายนิ้วมือ: รหัสผ่าน: เลือกผู้ใช้ ผู้ผลิต: 