��          �   %   �      P     Q     Y     j     y          �     �     �     �     �     �     �     �     �                 
   %     0     ?     S     a     y          �  �  �     f  %   n  %   �     �  
   �     �  U   �  C   F     �  "   �  6   �  +     "   -  E   P     �     �  l   �  $      -   E  E   s  "   �  H   �     %	  %   2	  M   X	                                                 
                                                                  	          &Delete &Empty Trash Bin &Move to Trash &Open &Paste &Properties &Refresh Desktop &Refresh View &Reload &Rename Deselect All File name pattern: File types: Hide Files Matching Icons Large More Preview Options... Select All Show All Files Show Files Matching Show a place: Show the Desktop folder Small Specify a folder: Type a path or a URL here Project-Id-Version: plasma_applet_folderview
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:11+0100
PO-Revision-Date: 2010-07-07 15:16+0700
Last-Translator: Thanomsub Noppaburana <donga.nb@gmail.com>
Language-Team: Thai <thai-l10n@googlegroups.com>
Language: th
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.1
Plural-Forms: nplurals=1; plural=0;
 &ลบ &เทถังขยะทิ้ง &ทิ้งลงถังขยะ เ&ปิด &วาง คุณ&สมบัติ &ปรับปรุงพื้นที่ทำงานอีกครั้ง &ปรับปรุงมุมมองอีกครั้ง เ&รียกใหม่ เ&ปลี่ยนชื่อ ยกเลิกเลือกทั้งหมด รูปแบบชื่อแฟ้ม: ชนิดของแฟ้ม: ซ่อนแฟ้มที่เข้าเงื่อนไข ไอคอน ใหญ่ ตัวเลือกเพิ่มเติมของการแสดงตัวอย่าง... เลือกทั้งหมด แสดงแฟ้มทั้งหมด แสดงแฟ้มที่เข้าเงื่อนไข แสดงสถานที่: แสดงโฟลเดอร์พื้นที่ทำงาน เล็ก ระบุโฟลเดอร์: ป้อนพาธหรือที่อยู่ URL ที่นี่ 