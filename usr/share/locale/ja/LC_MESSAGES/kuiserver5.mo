��          �   %   �      P     Q     b     w     �     �     �  
   �     �     �     �     �     �  /        3     Q     p     v     �     �     �     �     �     �     �       �  %     �          1     F     d     t     �     �     �  	   �  	   �     �  =   �          ,     E     R     Y     `  $   g  $   �  '   �  '   �  !     !   #                            
                                                                                             	    %1 file %1 files %1 folder %1 folders %1 of %2 processed %1 of %2 processed at %3/s %1 processed %1 processed at %2/s Appearance Behavior Cancel Clear Configure... Finished Jobs List of running file transfers/jobs (kuiserver) Move them to a different list Move them to a different list. Pause Remove them Remove them. Resume Show all jobs in a list Show all jobs in a list. Show all jobs in a tree Show all jobs in a tree. Show separate windows Show separate windows. Project-Id-Version: kuiserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-09-25 16:47-0700
Last-Translator: Fumiaki Okushi <okushi@kde.gr.jp>
Language-Team: Japanese <kde-jp@kde.org>
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Generator: Lokalize 1.1
 %1 ファイル %1 ファイル %1 フォルダ %1 フォルダ %1 / %2 処理済み %1 / %2 処理済み (%3/秒) %1 処理済み %1 処理済み (%2/秒) 外観 挙動 キャンセル クリア 設定... 完了したジョブ 実行中のファイル転送/ジョブの一覧 (kuiserver) 別のリストに移動 別のリストに移動 一時停止 削除 削除 再開 すべてのジョブを一覧表示 すべてのジョブを一覧表示 すべてのジョブをツリー表示 すべてのジョブをツリー表示 個別のウィンドウを表示 個別のウィンドウを表示 