��          �      L      �     �     �  �   �  T   }  )   �  (   �  0   %  $   V  &   {  &   �  %   �  ?   �  F   /     v     �     �     �     �  �  �  0   �  *   �  �     x   �     s     �     �     �     �     �     �  "   �     	  .   ,	     [	     z	  &   �	  -   �	                                              
                    	                                  Dim screen by half Dim screen totally Lists screen brightness options or sets it to the brightness defined by :q:; e.g. screen brightness 50 would dim the screen to 50% maximum brightness Lists system suspend (e.g. sleep, hibernate) options and allows them to be activated Note this is a KRunner keyworddim screen Note this is a KRunner keywordhibernate Note this is a KRunner keywordscreen brightness Note this is a KRunner keywordsleep Note this is a KRunner keywordsuspend Note this is a KRunner keywordto disk Note this is a KRunner keywordto ram Note this is a KRunner keyword; %1 is a parameterdim screen %1 Note this is a KRunner keyword; %1 is a parameterscreen brightness %1 Set Brightness to %1 Suspend to Disk Suspend to RAM Suspends the system to RAM Suspends the system to disk Project-Id-Version: krunner_powerdevil
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-26 03:33+0200
PO-Revision-Date: 2010-07-11 15:18-0700
Last-Translator: Fumiaki Okushi <okushi@kde.gr.jp>
Language-Team: Japanese <kde-jp@kde.org>
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Generator: Lokalize 1.0
 ディスプレイの明るさを半分にする ディスプレイを完全に暗くする スクリーン明るさオプションをリスト、または :q:; によって定義される明るさにセットします。例 スクリーン明るさを50にすると最大明るさの50%にスクリーンを暗くします すべてのシステム停止オプション(例 スリープ、ハイバネート)をリストし、有効化します スクリーンを暗く ハイバネート スクリーン明るさ スリープ サスペンド ディスクへ RAM へ スクリーン %1 を暗くする スクリーン明るさ %1 ディスプレイの明るさを %1 にする ディスクへサスペンド RAM へサスペンド システムを RAM へサスペンド システムをディスクへサスペンド 