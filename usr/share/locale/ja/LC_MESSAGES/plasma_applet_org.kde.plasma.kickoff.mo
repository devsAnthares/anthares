��          �      <      �     �  
   �  /   �  -   �        
   1     <     I     _     h  	   }     �     �     �     �     �  1   �  �  �     �  
   �  	   �     �  $        5     <  (   U     ~  $   �     �     �     �     �  0   �  0        D                      
                                             	                                  %1@%2 %2@%3 (%1) @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon All Applications Appearance Applications Applications updated. Computer Edit Applications... Favorites History Icon: Leave Show applications by name Switch tabs on hover Type is a verb here, not a nounType to search... Project-Id-Version: plasma_applet_kickoff
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2015-04-18 17:27-0700
Last-Translator: Fumiaki Okushi <fumiaki.okushi@gmail.com>
Language-Team: Japanese <kde-jp@kde.org>
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Generator: Lokalize 1.5
 %1@%2 %2@%3 (%1) 選択... アイコンをクリア すべてのアプリケーション 外観 アプリケーション 更新されたアプリケーション. コンピュータ アプリケーションを編集... お気に入り 履歴 アイコン: 終了 アプリケーションを名前で表示する マウスオーバーでタブを切り替える タイプして検索... 