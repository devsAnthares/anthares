��          �            h  m   i     �  "   �  F     F   N  F   �  J   �     '  %   8  $   ^  $   �  &   �  �   �     W  �  j     (     A  >   `  �   �  r   "  l   �  x        {     �     �     �     �  !   �  !   �           	                                                             
        Close the encrypted container; partitions inside will disappear as they had been unpluggedLock the container Eject medium Finds devices whose name match :q: Lists all devices and allows them to be mounted, unmounted or ejected. Lists all devices which can be ejected, and allows them to be ejected. Lists all devices which can be mounted, and allows them to be mounted. Lists all devices which can be unmounted, and allows them to be unmounted. Mount the device Note this is a KRunner keyworddevice Note this is a KRunner keywordeject Note this is a KRunner keywordmount Note this is a KRunner keywordunmount Unlock the encrypted container; will ask for a password; partitions inside will appear as they had been plugged inUnlock the container Unmount the device Project-Id-Version: plasma_runner_solid
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-01-09 23:17+0900
Last-Translator: Yukiko Bando <ybando@k6.dion.ne.jp>
Language-Team: Japanese <kde-jp@kde.org>
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Text-Markup: kde4
 コンテナをロック メディアをイジェクト 名前が :q: にマッチするデバイスを見つけます すべてのデバイスを一覧表示し、マウント/アンマウントしたりイジェクトできるようにします。 イジェクト可能なすべてのデバイスを一覧表示し、イジェクトできるようにします。 マウント可能なすべてのデバイスを一覧表示し、マウントできるようにします。 アンマウント可能なすべてのデバイスを一覧表示し、アンマウントできるようにします。 デバイスをマウント device eject mount unmount コンテナのロックを解除 デバイスをアンマウント 