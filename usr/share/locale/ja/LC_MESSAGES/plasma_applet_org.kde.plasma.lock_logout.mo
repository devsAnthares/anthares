��          �            h  &   i  +   �     �  	   �     �     �     �     �     �  (        7  ,   N     {     �  �  �  3   g  6   �     �     �  $   �       	     	   (     2  ?   N  !   �  Z   �                                                                            	   
           Do you want to suspend to RAM (sleep)? Do you want to suspend to disk (hibernate)? General Hibernate Hibernate (suspend to disk) Leave Leave... Lock Lock the screen Logout, turn off or restart the computer Sleep (suspend to RAM) Start a parallel session as a different user Suspend Switch user Project-Id-Version: plasma_applet_lockout
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2010-09-25 16:58-0700
Last-Translator: Fumiaki Okushi <okushi@kde.gr.jp>
Language-Team: Japanese <kde-jp@kde.org>
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Generator: Lokalize 1.1
 コンピュータをサスペンドしますか？ コンピュータをハイバネートしますか？ 全般 ハイバネート 作業状態をディスクに保存 席を外す 終了... ロック スクリーンをロック ログアウト、コンピュータを停止または再起動 作業状態をメモリに保存 現在のセッションと並行して、別のユーザとしてセッションを開始 サスペンド ユーザの切り替え 