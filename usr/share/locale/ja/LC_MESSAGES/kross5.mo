��    $      <  5   \      0     1  +   E     q  4   �  &   �     �     �                     5     :     P     X  ,   u  3   �     �     �               !  '   .     V     u     {     �     �     �     �     �     �  &   �       B        b  �  y     L     S     r     �     �  -   �  	   �     �     �  g   
	     r	  0   y	  	   �	  0   �	  X   �	  d   >
  =   �
  @   �
     "     0     >  )   R  K   |     �  $   �  6   �     ,  0   3     d  =   k     �  9   �     �  Z   �     S                                       
                              !                                  $      #                               	                           "             @info:creditAuthor @info:creditCopyright 2006 Sebastian Sauer @info:creditSebastian Sauer @info:shell command-line argumentThe script to run. @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: application descriptionCommand-line utility to run Kross scripts. application nameKross Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2015-04-19 16:22-0700
Last-Translator: Fumiaki Okushi <fumiaki.okushi@gmail.com>
Language-Team: Japanese <kde-jp@kde.org>
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Generator: Lokalize 1.1
 作者 Copyright 2006 Sebastian Sauer Sebastian Sauer 実行するスクリプト 一般 新しいスクリプトを追加します。 追加... キャンセルしますか？ コメント: kom@kde.gr.jp,shinobo@leo.bekkoame.ne.jp,toyohiro@ksmplus.com,md81@bird.email.ne.jp,tsuno@ngy.1st.ne.jp 編集 選択したスクリプトを編集します。 編集... 選択したスクリプトを実行します。 インタプリタ “%1” のためのスクリプトを作成できませんでした スクリプトファイル ‘%1’ のためのインタプリタを判断できませんでした インタプリタ “%1” を起動できませんでした スクリプトファイル ‘%1’ を開けませんでした ファイル: アイコン: インタプリタ: Ruby インタプリタの安全レベル Taiki Komoda,Noboru Sinohara,Toyohiro Asukai,Kurose Shushi,Shinichi Tsunoda 名前: %1 という関数はありません “%1” というインタプリタはありません 削除 選択したスクリプトを削除します。 実行 スクリプトファイル ‘%1’ は存在しません。 停止 選択したスクリプトの実行を停止します。 テキスト: Kross スクリプトを実行するためのコマンドラインユーティリティ。 Kross 