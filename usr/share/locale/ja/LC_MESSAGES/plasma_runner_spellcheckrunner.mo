��    	      d      �       �      �      �           "     *  Q   ?     �     �  �  �     n      �  +   �  	   �     �     �                                      	                  &Require trigger word &Trigger word: Checks the spelling of :q:. Correct Spell Check Settings Spelling checking runner syntax, first word is trigger word, e.g.  "spell".%1:q: Suggested words: %1 spell Project-Id-Version: krunner_spellcheckrunner
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2009-12-17 23:30+0900
Last-Translator: Yukiko Bando <ybando@k6.dion.ne.jp>
Language-Team: Japanese <kde-jp@kde.org>
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Text-Markup: kde4
 トリガーを使う(&R) トリガーにする単語(&T): :q: のスペルをチェックします。 正しい スペルチェックの設定 %1:q: 修正候補: %1 spell 