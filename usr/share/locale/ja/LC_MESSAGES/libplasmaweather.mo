��          <      \       p      q   *   �   /   �   �  �   2   �  C   �  ?                      Cannot find '%1' using %2. Connection to %1 weather server timed out. Weather information retrieval for %1 timed out. Project-Id-Version: libplasmaweather
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-13 03:17+0100
PO-Revision-Date: 2010-07-24 18:21-0700
Last-Translator: Fumiaki Okushi <okushi@kde.gr.jp>
Language-Team: Japanese <kde-jp@kde.org>
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Text-Markup: kde4
 %2 を使って「%1」が見つかりません。 天候サーバ %1 への接続がタイムアウトしました。 %1 の気象情報の取得がタイムアウトしました。 