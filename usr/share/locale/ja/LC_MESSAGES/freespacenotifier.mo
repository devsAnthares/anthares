��    	      d      �       �      �   V   �      =  '   [  5   �  6   �  D   �     5  �  T     9     >  <   Q  3   �  <   �  '   �     '  7   .                      	                         MiB Allows the user to configure the warning notification being shownConfigure Warning... Enable low disk space warning Is the free space notification enabled. Minimum free space before user starts being notified. Opens a file manager like dolphinOpen File Manager... The settings dialog main page name, as in 'general settings'General Warn when free space is below: Project-Id-Version: kded_susefreespacenotifier
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2015-05-22 22:08-0700
Last-Translator: Fumiaki Okushi <fumiaki.okushi@gmail.com>
Language-Team: Japanese <kde-jp@kde.org>
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Generator: Lokalize 1.1
  MiB 警告の設定... ディスクスペースが少なくなったら警告する 空き領域の通知が有効になりました。 ユーザが通知され始める前の最小の空き領域 ファイルマネージャを開く... 一般 空き領域が以下の量になったら警告する: 