��    ;      �  O   �           	          )     H     h  
   q     |     �     �     �     �  ,   �          !     9     M     b     u     �     �     �     �  	   �     �     �     �     �            	   "  
   ,     7  
   G     R     Y     m     }     �     �     �     �  	   �     �     �     �          &     ;  	   Q  ,   [     �     �     �     �     �     �     �  #   �  �  	     �
       $   (     M     U     \     i     y  $   �  $   �     �  K   �     ;  -   Z     �  -   �  !   �     �  !   
  *   ,     W     ^     f     y  !   �  	   �     �     �     �     �  +   �     $     ;  	   K  *   U     �     �     �  $   �     �       	        #     3  (   O  3   x  $   �  !   �     �  B   	     L  -   \  0   �     �     �  !   �  ,     ?   0         9   *   /                 '            )                 (      :            0       ,   5      &                          +   $   .          4   3      
            ;      #   8       "                 1   	   %                       6   -             !      2       7            Add to Desktop Add to Favorites Align search results to bottom App name (Generic name)%1 (%2) Behavior Categories Description (Name) Description only Edit Application... Edit Applications... End session Expand search to bookmarks, files and emails Flatten menu to a single level Forget All Applications Forget All Contacts Forget All Documents Forget Application Forget Contact Forget Document Forget Recent Documents General Generic name (App name)%1 (%2) Hibernate Hide %1 Hide Application Lock Lock screen Logout Name (Description) Name only Open with: Power / Session Properties Reboot Recent Applications Recent Contacts Recent Documents Remove from Favorites Restart computer Save Session Search Search... Session Show Contact Information... Show applications as: Show recent applications Show recent contacts Show recent documents Shut Down Start a parallel session as a different user Suspend Suspend to RAM Suspend to disk Switch User System Turn off computer Unhide Applications in '%1' Unhide Applications in this Submenu Project-Id-Version: plasma_applet_kicker
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2017-03-28 22:28-0700
Last-Translator: Fumiaki Okushi <fumiaki.okushi@gmail.com>
Language-Team: Japanese <kde-jp@kde.org>
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Generator: Lokalize 1.5
 デスクトップに追加 お気に入りに追加 検索結果を下部に配置する %1 (%2) 挙動 カテゴリ 説明 (名前) 説明のみ アプリケーションを編集... アプリケーションを編集... セッションを終了する 検索対象をブックマークとファイル、メールに拡大する 単一階層にフラット化 すべてのアプリケーションを削除 すべての連絡先を削除 すべての文書を履歴から削除する アプリケーションを削除 連絡先を削除 文書を履歴から削除する 最近使用した文書から削除する 全般 %1 (%2) ハイバネート %1 を隠す アプリケーションを隠す ロック 画面をロックする ログアウト 名前 (説明) 名前のみ 以下のアプリケーションで開く: 電源/セッション プロパティ 再起動 最近使用したアプリケーション 最近使用したの連絡先 最近使用した文書 お気に入りから削除 コンピュータを再起動する セッションを保存 検索 検索... セッション 連絡先情報を表示... アプリケーションの表示方法: 最近使用したアプリケーションを表示 最近使用した連絡先を表示 最近使用した文書を表示 シャットダウン 別のユーザとして並行するセッションを開始する サスペンド 状態をメモリに保持して待機する 状態をディスクに退避して待機する ユーザの切り替え システム コンピュータを停止する '%1' のアプリケーションを再表示 このサブメニューのアプリケーションを再表示 