��          �   %   �      @     A     _     q  6   �  A   �  
   �       #        ?     [     a     n     }     �     �     �     �     �     �     �     	          (     @  �  `  (     (   E     n     �  "   �     �  3   �  C   �  1   B     t     {     �     �  :   �     �        %     %   -     S     s     �  $   �  .   �     �                	                       
                                                                                       &Create Comic Book Archive... &Save Comic As... &Strip Number: @option:check Context menu of comic image&Actual Size @option:check Context menu of comic imageStore current &Position Appearance Archiving comic failed Automatically update comic plugins: Check for new comic strips: Comic Comic cache: Error Handling General Getting comic strip failed: Go to Strip Information Jump to &current Strip Jump to &first Strip Jump to Strip ... Strip identifier: Update Visit the comic website Visit the shop &website an abbreviation for Number# %1 Project-Id-Version: plasma_applet_comic
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-07-18 03:20+0200
PO-Revision-Date: 2012-01-29 14:29-0800
Last-Translator: Fumiaki Okushi <okushi@kde.gr.jp>
Language-Team: Japanese <kde-jp@kde.org>
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Text-Markup: kde4
 漫画本アーカイブを作成(&C)... 名前を付けて漫画を保存(&S)... ストリップ番号(&S): 実際のサイズ(&A) 現在の位置を記憶する(&P) 外観 コミックのアーカイブに失敗しました 自動的にコミックプラグインをアップデートする: 新しいコミック・ストリップを確認: 漫画 漫画のキャッシュ: エラー処理 全般 コミックストリップの取得に失敗しました: ストリップに移動 情報 現在のストリップに移動(&C) 最初のストリップに移動(&F) ストリップに移動(&J)... 識別子を削除: アップデート 漫画のウェブサイトを訪問 ショップのウェブサイトを訪問(&W) No %1 