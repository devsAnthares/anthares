��          4      L       `   $   a   /   �   �  �   F   ~  N   �                    Finds Konsole sessions matching :q:. Lists all the Konsole sessions in your account. Project-Id-Version: plasma_runner_konsolesessions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2009-07-28 21:23+0900
Last-Translator: Yukiko Bando <ybando@k6.dion.ne.jp>
Language-Team: Japanese <kde-jp@kde.org>
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Text-Markup: kde4
 :q: にマッチする Konsole のセッションを見つけます。 あなたの Konsole のすべてのセッションを一覧表示します。 