��          �   %   �      p     q     ~     �     �     �     �     �     �  )   �               !     4     I     ]     m  3   u     �     �  <   �  5     8   T  8   �     �     �     �     �  �  �     �     �  
   �     �     �     �     �       -        A     I     R  .   h     �     �  
   �  &   �     �       C   '  <   k  7   �  7   �  	   	     "	     8	     N	                        
                                                	                                                   Add files... Add folder... Background frame Change picture every Choose a folder Choose files Configure plasmoid... General Left click image opens in external viewer Pad Paths Pause on mouseover Preserve aspect crop Preserve aspect fit Randomize items Stretch The image is duplicated horizontally and vertically The image is not transformed The image is scaled to fit The image is scaled uniformly to fill, cropping if necessary The image is scaled uniformly to fit without cropping The image is stretched horizontally and tiled vertically The image is stretched vertically and tiled horizontally Tile Tile horizontally Tile vertically s Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-16 03:31+0100
PO-Revision-Date: 2016-08-18 19:41+0300
Last-Translator: Marek Laane <qiilaq69@gmail.com>
Language-Team: Estonian <kde-i18n-doc@kde.org>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 Lisa failid... Lisa kataloog... Taustaraam Piltide vahetamise sagedus Kataloogi valimine Failide valimine Seadista plasmoidi... Üldine Vasakklõpsuga avaneb pilt välises näitajas Polster Asukohad Paus hiirekursori all Proportsioon säilitatakse, kärpimisvõimalus Proportsioon säilitatakse Elemendid juhuslikult Venitamine Pilti korratakse rõht- ja püstsuunas Pilti ei muudeta Pilt skaleeritakse sobivaks Pilt skaleeritakse ühetaoliselt täitmiseks, vajadusel kärbitakse Pilt skaleeritakse ühetaoliselt täitmiseks ilma kärpimata Pilti venitatakse rõhtsuunas ja paanitakse püstsuunas Pilti venitatakse püstsuunas ja paanitakse rõhtsuunas Paanidena Paanidena rõhtsuunas Paanidena püstsuunas s 