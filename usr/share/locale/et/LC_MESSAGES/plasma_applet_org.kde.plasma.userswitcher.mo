��          �      <      �     �     �     �     �     �  '   �  >        L     f     �     �     �  )   �  7   �  '        B     T  �  s          !     )     2  
   A  
   L  	   W  "   a  $   �  $   �     �     �  $   �     $     6     =  '   U                                       
                      	                                        Current user General Layout Lock Screen New Session Nobody logged in on that sessionUnused Show a dialog with options to logout/shutdown/restartLeave... Show both avatar and name Show full name (if available) Show login username Show only avatar Show only name Show technical information about sessions User logged in on console (X display number)on %1 (%2) User logged in on console numberTTY %1 User name display You are logged in as <b>%1</b> Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-11 03:18+0100
PO-Revision-Date: 2016-07-27 17:50+0300
Last-Translator: Marek Laane <qiilaq69@gmail.com>
Language-Team: Estonian <kde-i18n-doc@kde.org>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 Aktiivne kasutaja Üldine Paigutus Lukusta ekraan Uus seanss Kasutamata Välju... Nii avatari kui ka nime näitamine Täisnime näitamine (kui võimalik) Kasutaja sisselogimisnime näitamine Ainult avatari näitamine Ainult nime näitamine Seansside tehnilise teabe näitamine konsoolis %1 (%2) TTY %1 Kasutajanime näitamine Oled sisse logitud kasutajana <b>%1</b> 