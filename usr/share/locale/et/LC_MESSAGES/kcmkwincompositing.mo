��    2      �  C   <      H  6   I  M   �  K   �       '   #     K     R  �   c       	   2  A   <  >   ~  9   �  9   �  9   1  W   k  E   �     	                 A  7   ^      �     �  X   �     %	     -	     C	  �   [	     �	     
     
     0
  
   @
  
   K
     V
     t
  E  �
     �     �     	  w        �     �     �     �     �  	   �     �  �  �  A   �  s   �  X   O     �  (   �     �     �  �   �     �  
   �  
   �     �                 0   !  
   R     ]     q     w  #   �  8   �  ,   �       E   2  
   x     �      �  �   �     Q     ]     j     �  
   �  
   �     �     �  ;  �  '        *     E  s   b     �     �     �     �          %     3                            (          	           ,   #          0      -       *                       '             
   .   $   2                !   +                "            &       %           1      /                   )            "Full screen repaints" can cause performance problems. "Only when cheap" only prevents tearing for full screen changes like a video. "Re-use screen content" causes severe performance problems on MESA drivers. Accurate Allow applications to block compositing Always Animation speed: Applications can set a hint to block compositing when the window is open.
 This brings performance improvements for e.g. games.
 The setting can be overruled by window-specific rules. Author: %1
License: %2 Automatic Category of Desktop Effects, used as section headerAccessibility Category of Desktop Effects, used as section headerAppearance Category of Desktop Effects, used as section headerCandy Category of Desktop Effects, used as section headerFocus Category of Desktop Effects, used as section headerTools Category of Desktop Effects, used as section headerVirtual Desktop Switching Animation Category of Desktop Effects, used as section headerWindow Management Configure filter Crisp EMAIL OF TRANSLATORSYour emails Enable compositor on startup Exclude Desktop Effects not supported by the Compositor Exclude internal Desktop Effects Full screen repaints Hint: To find out or configure how to activate an effect, look at the effect's settings. Instant KWin development team Keep window thumbnails: Keeping the window thumbnail always interferes with the minimized state of windows. This can result in windows not suspending their work when minimized. NAME OF TRANSLATORSYour names Never Only for Shown Windows Only when cheap OpenGL 2.0 OpenGL 3.1 OpenGL Platform InterfaceEGL OpenGL Platform InterfaceGLX OpenGL compositing (the default) has crashed KWin in the past.
This was most likely due to a driver bug.
If you think that you have meanwhile upgraded to a stable driver,
you can reset this protection but be aware that this might result in an immediate crash!
Alternatively, you might want to use the XRender backend instead. Re-enable OpenGL detection Re-use screen content Rendering backend: Scale method "Accurate" is not supported by all hardware and can cause performance regressions and rendering artifacts. Scale method: Search Smooth Smooth (slower) Tearing prevention ("vsync"): Very slow XRender Project-Id-Version: kcmkwincompositing
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2016-12-09 00:21+0200
Last-Translator: Marek Laane <qiilaq69@gmail.com>
Language-Team: Estonian <kde-et@linux.ee>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 "Täisekraani ülejoonistamine" võib tekitada jõudlusprobleeme. "Ainult vähese koormuse korral" takistab ainult rebestusi täisekraani muutumise puhul, näiteks video esitamisel. "Ekraanisisu taaskasutamine" tekitab MESA draiverite korral tõsiseid jõudlusprobleeme. Täpne Rakendused võivad komposiiti blokeerida Alati Animatsiooni kiirus: Rakendused võivad anda teada, et komposiit tuleb akna avamisel blokeerida.
See parandab teatavatel juhtudel, näiteks mängudes, tublisti jõudlust.
Seda määratlust võib tühistada aknaspetsiifilise määratlusega. Autor: %1
Litsents: %2 Automaatne Hõlbustus Välimus Silmailu Fookus Tööriistad Virtuaalsete töölaudade vahetamise animatsioon Aknahaldus Filtri seadistamine Terav qiilaq69@gmail.com Komposiitori lubamine käivitamisel Komposiitori toetuseta töölauaefektid jäetakse välja Sissemised töölauaefektid jäetakse välja Täisekraani ülejoonistamine Vihje: mõistmaks, kuidas efekti aktiveerida, uuri efekti seadistusi. Otsekohene KWini arendusmeeskond Akende pisipiltide säilitamine: Akna pisipildi alati säilitamine läheb vastuollu akende minimeeritud olekuga. See võib tähendada, et aken ei peata tööd, kui see minimeeritakse. Marek Laane Mitte kunagi Ainult kuvatavate akende puhul Ainult vähese koormuse korral OpenGL 2.0 OpenGL 3.1 EGL GLX OpenGL komposiit (vaikimisi) on varem tekitanud KWini krahhe.
Tõenäoliselt on tegemist draiveri veaga.
Kui arvad, et oled vahepeal paigaldanud stabiilse draiveri,
võid selle kaitse maha võtta, aga arvesta, et see võib kaasa tuua kohese krahhi!
Teine võimalus on kasutada selle asemel XRenderi taustaprogrammi. Lülita OpenGL-i tuvastamine taas sisse Ekraanisisu taaskasutamine Renderdamise taustaprogramm: Kõik riistvara ei toeta skaleerimisviisi "täpne" ning see võib põhjustada jõudlusprobleeme ja renderdamisvigu. Skaleerimisviis: Otsing Sujuv Sujuv (aeglasem) Rebestuse vältimine ("vsync"): Väga aeglane XRender 