��    B      ,  Y   <      �  i   �  m        y  b   �     �     	  6        O  7   _     �     �  	   �     �  (   �  )   �  )   (     R     o  Y   u     �     �  �   �      y	  .   �	     �	  
   �	     �	     �	     
     
     !
     5
     B
     J
     c
     r
     w
     �
     �
     �
     �
     �
  	   �
  
   �
     �
     �
  	     
     
   *     5     B  	   `     j     o  
   �  �   �     (  8   8  �   q     �  8   	  ,   B  ,   o  %   �     �  �  �  K   y  ~   �     D  n   c     �     �  9   �     9  8   K     �     �     �  !   �  -   �  ,   �  -   &      T     u  [   �     �     �  o        w  .   �  
   �     �     �     �     �  	                  -     4     R     a     f     r     �     �     �     �     �     �     �     �     �       	     	   )     3  	   S     ]     b     {  �   �       T   #  ~   x     �  9   
  8   D  8   }     �     �     $   !   %          ;   ,          B                 8   #                 7   9       +              )   ?   -   <         6                                    *   2       >                1       @       .   5   3   :   A         &      /                4              0      "             =   (   '         	   
           @info User changed Phonon backendTo apply the backend change you will have to log out and back in again. A list of Phonon Backends found on your system.  The order here determines the order Phonon will use them in. Apply Device List To... Apply the currently shown device preference list to the following other audio playback categories: Audio Hardware Setup Audio Playback Audio Playback Device Preference for the '%1' Category Audio Recording Audio Recording Device Preference for the '%1' Category Backend Colin Guthrie Connector Copyright 2006 Matthias Kretz Default Audio Playback Device Preference Default Audio Recording Device Preference Default Video Recording Device Preference Default/Unspecified Category Defer Defines the default ordering of devices which can be overridden by individual categories. Device Configuration Device Preference Devices found on your system, suitable for the selected category.  Choose the device that you wish to be used by the applications. EMAIL OF TRANSLATORSYour emails Failed to set the selected audio output device Front Center Front Left Front Left of Center Front Right Front Right of Center Hardware Independent Devices Input Levels Invalid KDE Audio Hardware Setup Matthias Kretz Mono NAME OF TRANSLATORSYour names Phonon Configuration Module Playback (%1) Prefer Profile Rear Center Rear Left Rear Right Recording (%1) Show advanced devices Side Left Side Right Sound Card Sound Device Speaker Placement and Testing Subwoofer Test Test the selected device Testing %1 The order determines the preference of the devices. If for some reason the first device cannot be used Phonon will try to use the second, and so on. Unknown Channel Use the currently shown device list for more categories. Various categories of media use cases.  For each category, you may choose what device you prefer to be used by the Phonon applications. Video Recording Video Recording Device Preference for the '%1' Category  Your backend may not support audio recording Your backend may not support video recording no preference for the selected device prefer the selected device Project-Id-Version: kcm_phonon
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2012-05-14 18:58+0300
Last-Translator: Marek Laane <bald@smail.ee>
Language-Team: Estonian <kde-et@linux.ee>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.4
Plural-Forms: nplurals=2; plural=n != 1;
 Taustaprogrammi muutmise rakendamiseks tuleb välja ja uuesti sisse logida. Sinu süsteemis leitud Phononi taustaprogrammide nimekiri. Nende järjekord määrab järjekorra, millega Phonon neid kasutab. Seadmenimekirja rakendamine... Parajasti näidatavate seadme-eelistuste nimekirja rakendamine järgmistele heli taasesitamise kategooriatele: Heliriistvara seadistused Heli taasesitamine Eelistatud heli taasesitamise seade kategooria '%1' jaoks Heli salvestamine Eelistatud heli salvestamise seade kategooria '%1' jaoks Taustaprogramm Colin Guthrie Pistmik Autoriõigus 2006: Matthias Kretz Vaikimisi eelistatud heli taasesitamise seade Vaikimisi eelistatud heli salvestamise seade Vaikimisi eelistatud video salvestamise seade Vaikimisi/määramata kategooria Tagasilükkamine Määrab seadmete vaikimisi järjekorra, mille saab tühistada konkreetsete kategooriatega. Seadme seadistamine Eelistatud seade Sinu süsteemis leitud seadmed, mis sobivad valitud kategooriaga. Vali seade, mida rakendused peaksid kasutama. bald@starman.ee Valitud heli väljundseadme määramine nurjus Keskel ees Vasakul ees Keskelt vasakul ees Paremal ees Keskelt paremal ees Riistvara Sõltumatud seadmed Sisendtasemed Vigane KDE heliriistvara seadistused Matthias Kretz Mono Marek Laane Phononi seadistamismoodul Taasesitus (%1) Eelistus Profiil Keskel taga Vasakul taga Paremal taga Salvestamine (%1) Muude seadmete näitamine Vasakul küljel Paremal küljel Helikaart Heliseade Kõlarite paigutus ja testimine Subwoofer Test Valitud seadme testimine %1 testimine Järjekord määrab seadmete eelistuse. Kui mingil põhjusel ei saa Phonon kasutada esimest seadet, püütakse kasutada teist ja nii edasi. Tundmatu kanal Parajasti näidatavate seadmete nimekirja kasutamine muude kategooriate nägemiseks. Erinevad kasutamiskategooriad. Iga kategooria puhul saab valida, millist seadet peaks Phononi rakendused eelistatult kasutama. Video salvestamine Eelistatud video salvestamise seade kategooria '%1' jaoks Sinu taustaprogramm ei pruugi heli salvestamiset toetada Sinu taustaprogramm ei pruugi video salvestamist toetada valitud seadmel puudub eelistus valitud seadme eelistamine 