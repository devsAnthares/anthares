��    	      d      �       �      �      �   -        <  -   V  #   �  #   �     �  �  �     v     �  5   �     �  4   �     4     =     F                                   	           Current time is %1 Displays the current date Displays the current date in a given timezone Displays the current time Displays the current time in a given timezone Note this is a KRunner keyworddate Note this is a KRunner keywordtime Today's date is %1 Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2016-01-11 17:20+0200
Last-Translator: Marek Laane <qiilaq69@gmail.com>
Language-Team: Estonian <kde-et@linux.ee>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 Praegune kellaaeg on %1 Praeguse kuupäeva näitamine Praeguse kuupäeva näitamine määratud ajavööndis Praeguse kellaaja näitamine Praeguse kellaaja näitamine määratud ajavööndis kuupäev kellaaeg Tänane kuupäev on %1 