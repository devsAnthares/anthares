��          �   %   �      p     q  +   �  .   �  6   �  *     #   D  &   h  /   �  2   �  :   �  0   -  3   ^  ;   �  K   �  -     $   H  '   m     �     �     �     �     �  #        1     O     c     |  �  �           ,  !   I  &   k  &   �     �  !   �     �  #   	  (   <	     e	  "   �	  '   �	  H   �	     
     5
     N
     g
     v
     �
     �
     �
      �
     �
  
   �
                                                               	   
                                                                    @action:buttonCommit @info:statusAdded files to SVN repository. @info:statusAdding files to SVN repository... @info:statusAdding of files to SVN repository failed. @info:statusCommit of SVN changes failed. @info:statusCommitted SVN changes. @info:statusCommitting SVN changes... @info:statusRemoved files from SVN repository. @info:statusRemoving files from SVN repository... @info:statusRemoving of files from SVN repository failed. @info:statusReverted files from SVN repository. @info:statusReverting files from SVN repository... @info:statusReverting of files from SVN repository failed. @info:statusSVN status update failed. Disabling Option "Show SVN Updates". @info:statusUpdate of SVN repository failed. @info:statusUpdated SVN repository. @info:statusUpdating SVN repository... @item:inmenuSVN Add @item:inmenuSVN Commit... @item:inmenuSVN Delete @item:inmenuSVN Revert @item:inmenuSVN Update @item:inmenuShow Local SVN Changes @item:inmenuShow SVN Updates @labelDescription: @title:windowSVN Commit Show updates Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2016-01-14 16:42+0200
Last-Translator: Marek Laane <qiilaq69@gmail.com>
Language-Team: Estonian <kde-et@linux.ee>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 Kanna sisse Failid lisati SVN-hoidlasse. Failide lisamine SVN-hoidlasse... Failide lisamine SVN-hoidlasse nurjus. SVN-i muudatuste sissekandmine nurjus. SVN-i muudatused kanti sisse. SVN-i muudatuste sissekandmine... Failid eemaldati SVN-hoidlast. Failide eemaldamine SVN-hoidlast... Failide eemaldamine SVN-hoidlast nurjus. Failid taastati SVN-hoidlast. Failide taastamine SVN-hoidlast... Failide taastamine SVN-hoidlast nurjus. SVN-i oleku uuendamine nurjus. Valik "Näita SVN-i uuendusi" keelatakse. SVN-hoidla uuendamine nurjus. SVN-hoidla on uuendatud. SVN-hoidla uuendamine... SVN-i lisamine SVN-i sissekanne... SVN-i kustutamine SVN-i taastamine SVN-i uuendamine Näita kohalikke SVN-i muudatusi Näita SVN-i uuendusi Kirjeldus: SVN-i sissekanne Uuenduste näitamine 