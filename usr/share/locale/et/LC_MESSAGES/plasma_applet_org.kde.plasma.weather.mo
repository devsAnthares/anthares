��    K      t  e   �      `     a  ^   f  H   �          !     -     D     L  '   [     �  "   �     �     �     �  
   �     �          %  	   .     8     P     f     l          �  "   �     �     �  	   �     �     �  ?   	     D	     Q	     W	     e	     q	     �	     �	  ;   �	  &   �	      
  %   %
     K
     e
     
     �
  >   �
     �
       '   &  !   N     p     �     �     �     �     �     �     �          "     3     E     X     k     }     �     �     �     �     �     �  3     �  G     �     �     �               (     +     7     F     X     j     x     �     �     �     �     �  
   �     �     �     �               )     :  &   <  	   c     m  
   o     z     �     �     �     �     �  
   �     �     �     �     �     �     �               ,     3     ;     D     X     i     n  
   |     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     '   I   8                            >   5   B           .   6   !      +       -   4       2         "       C         %       9       0   7   @      )         
   H       :         G              F   *      /   =               &                            D   #      A         K   ,       1      (   $          ?   J       <      3      	      ;       E                   min %1 is the weather condition, %2 is the temperature,  both come from the weather provider%1 %2 A weather station location and the weather service it comes from%1 (%2) Beaufort scale bft Celsius °C Degree, unit symbol° Details Fahrenheit °F Forecast period timeframe1 Day %1 Days Hectopascals hPa High & Low temperatureH: %1 L: %2 High temperatureHigh: %1 Inches of Mercury inHg Kelvin K Kilometers Kilometers per Hour km/h Kilopascals kPa Knots kt Location: Low temperatureLow: %1 Meters per Second m/s Miles Miles per Hour mph Millibars mbar N/A No weather stations found for '%1' Notices Percent, measure unit% Pressure: Search Short for no data available- Shown when you have not set a weather providerPlease Configure Temperature: Units Update every: Visibility: Weather Station Wind conditionCalm Wind speed: certain weather condition (probability percentage)%1 (%2%) content of water in airHumidity: %1%2 distance, unitVisibility: %1 %2 ground temperature, unitDewpoint: %1 humidex, unitHumidex: %1 pressure tendencyfalling pressure tendencyrising pressure tendencysteady pressure tendency, rising/falling/steadyPressure Tendency: %1 pressure, unitPressure: %1 %2 temperature, unit%1%2 visibility from distanceVisibility: %1 weather warningsWarnings Issued: weather watchesWatches Issued: wind directionE wind directionENE wind directionESE wind directionN wind directionNE wind directionNNE wind directionNNW wind directionNW wind directionS wind directionSE wind directionSSE wind directionSSW wind directionSW wind directionVR wind directionW wind directionWNW wind directionWSW wind direction, speed%1 %2 %3 wind speedCalm windchill, unitWindchill: %1 winds exceeding wind speed brieflyWind Gust: %1 %2 Project-Id-Version: plasma_applet_weather
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-28 05:50+0100
PO-Revision-Date: 2016-07-27 18:06+0300
Last-Translator: Marek Laane <qiilaq69@gmail.com>
Language-Team: Estonian <kde-et@linux.ee>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
  min %1 %2 %1 (%2) Beaufort'i skaala bft Celsius °C ° Üksikasjad Fahrenheit °F 1 päev %1 päeva Hektopaskalid hPa Ma: %1 Mi: %2 Maksimum: %1 Elavhõbeda tollid inHg Kelvin K kilomeetrit Kilomeetrid tunnis km/h Kilopaskalid kPa Sõlmed kt Asukoht: Miinimum: %1 Meetrid sekundis m/s miili Miilid tunnis mph Millibaarid mbar - "%1" puhul ei leitud ühtegi ilmajaama Märkused % Õhurõhk: Otsing - Palun seadista Temperatuur: Ühikud Uuendamise intervall: Nähtavus: Ilmajaam Jahe Tuulekiirus: %1 (%2%) Niiskus: %1%2 Nähtavus: %1 %2 Kastepunkt: %1 Niiskusindeks: %1 langev tõusev ühtlane Õhurõhu suund: %1 Õhurõhk: %1 %2 %1%2 Nähtavus: %1 Hoiatused: Tähelepanu juhtimised: E ENE ESE N NE NNE NNW NW S SE SSE SSW SW Muutlik W WNW WSW %1 %2 %3 Nõrk Tuulejahutus: %1 Tuule kiirus hooti: %1 %2 