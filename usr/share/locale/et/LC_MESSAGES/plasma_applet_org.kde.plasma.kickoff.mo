��          �      l      �     �  
   �  /   �  -   "     P     a  
   r     }     �     �     �  ,   �  	   �     �     �          	          9  1   N  �  �       
   "     -     5     C     X     i  
   r     }     �     �  <   �     �     �     �  
          !   (  2   J     }                                                           	      
                                 %1@%2 %2@%3 (%1) @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Favorites All Applications Appearance Applications Applications updated. Computer Edit Applications... Expand search to bookmarks, files and emails Favorites History Icon: Leave Remove from Favorites Show applications by name Switch tabs on hover Type is a verb here, not a nounType to search... Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2016-07-27 17:49+0300
Last-Translator: Marek Laane <qiilaq69@gmail.com>
Language-Team: Estonian <kde-i18n-doc@kde.org>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 %1@%2 %2@%3 (%1) Vali... Puhasta ikoon Lisa lemmikute sekka Kõik rakendused Välimus Rakendused Rakendused on uuendatud. Arvuti Muuda rakendusi... Otsingu laiendamine järjehoidjatele, failidele ja kirjadele Lemmikud Ajalugu Ikoon: Väljumine Eemalda lemmikute seast Rakenduste näitamine nime järgi Kaartide vahetamine hiirekursori kaardile viimisel Kirjuta otsimiseks... 