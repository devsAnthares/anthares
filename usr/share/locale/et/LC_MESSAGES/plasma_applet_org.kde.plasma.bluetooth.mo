��    )      d  ;   �      �     �     �     �     �     �     �     �  	   �     �          %     :     G     _     v     ~  
   �  
   �     �     �     �     �     �     �     �     �               .  U   C  Z   �  O   �  D   D     �     �     �  	   �  	   �     �     �  �  �     e     m     |     �     �  	   �     �  	   �     �     �     	     	     &	     ?	     W	     _	     r	     ~	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
     !
     =
     Y
     o
  (   �
  	   �
  	   �
  
   �
  
   �
     �
  	   �
     �
                                             %                   	         (          &   !          '                          )   #          "                          $          
                            Adapter Add New Device Add New Device... Address Audio Audio device Available devices Bluetooth Bluetooth is Disabled Bluetooth is disabled Bluetooth is offline Browse Files Configure &Bluetooth... Configure Bluetooth... Connect Connected devices Connecting Disconnect Disconnecting Enable Bluetooth File transfer Input Input device Network No No Adapters Available No Devices Found No adapters available No connected devices Notification when the connection failed due to FailedConnection to the device failed Notification when the connection failed due to Failed:HostIsDownThe device is unreachable Notification when the connection failed due to NotReadyThe device is not ready Number of connected devices%1 connected device %1 connected devices Other device Paired Remote Name Send File Send file Trusted Yes Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:01+0100
PO-Revision-Date: 2016-01-11 12:03+0200
Last-Translator: Marek Laane <qiilaq69@gmail.com>
Language-Team: Estonian <kde-i18n-doc@kde.org>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 Adapter Lisa uus seade Lisa uus seade... Aadress Heli Heliseade Saadaolevad seadmed Bluetooth Bluetooth on välja lülitatud Bluetooth on välja lülitatud Bluetooth ei ole võrgus Sirvi faile Seadista &Bluetoothi ... Seadista Bluetoothi ... Ühenda Ühendatud seadmed Ühendumine Katkesta ühendus Ühenduse katkestamine Lülita Bluetooth sisse Failiedastus Sisend Sisendseade Võrk Ei Adapterid puuduvad Seadmeid ei leitud Adapterid puuduvad Ühendatud seadmed puuduvad Ühendumine seadmega nurjus Seade ei ole saadaval Seade ei ole valmis %1 ühendatud seade %1 ühendatud seadet Muu seade Paardunud Võrgunimi Saada fail Faili saatmine Usaldatav Jah 