��          �            h  ~   i  +   �           5     T     s     �     �  �   �  g   A  0   �  .   �     	  @     �  Z  o   �  *   n     �     �     �     �     �     �  �   �  q   �  -     &   6     ]     m                                                   	             
                Click on the button, then enter the shortcut like you would in the program.
Example for Ctrl+A: hold the Ctrl key and press A. Conflict with Standard Application Shortcut EMAIL OF TRANSLATORSYour emails KPackage QML application shell NAME OF TRANSLATORSYour names No shortcut definedNone Reassign Reserved Shortcut The '%1' key combination is also used for the standard action "%2" that some applications use.
Do you really want to use it as a global shortcut as well? The F12 key is reserved on Windows, so cannot be used for a global shortcut.
Please choose another one. The key you just pressed is not supported by Qt. The unique name of the application (mandatory) Unsupported Key What the user inputs now will be taken as the new shortcutInput Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-30 03:07+0100
PO-Revision-Date: 2016-09-09 00:55+0300
Last-Translator: Marek Laane <qiilaq69@gmail.com>
Language-Team: Estonian <kde-i18n-doc@kde.org>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 Klõpsa nupule ja sisesta meelepärane kiirklahv.
Näide Ctrl+A kohta: hoia all Ctrl-klahvi ja vajuta klahvi A. Konflikt rakenduse standardse kiirklahviga bald@smail.ee KPackage'i QML rakenduse kest Marek Laane Puudub Asenda Reserveeritud kiirklahv Klahvikombinatsioon '%1' on juba kasutusel standardseks toiminguks "%2", mida kasutavad mitmed rakendused.
Kas soovid seda kasutada ka globaalse kiirklahvina? Klahv F12 on reserveeritud Windowsile ja seda ei saa kasutada globaalse kiirklahvina.
Palun vali mõni muu klahv. Qt ei toeta klahvi, mida sa praegu vajutasid. Rakenduse unikaalne nimi (kohustuslik) Toetamata klahv Sisend 