��          �      �       0     1     Q  #   i      �     �     �     �  �   �  ]   �     �  p     :   v  �  �  #   M     q  %   �     �     �     �     �  �   �  f   �       y   $  <   �                
                      	                    Configure Look and Feel details Cursor Settings Changed Download New Look And Feel Packages EMAIL OF TRANSLATORSYour emails Get New Looks... Marco Martin NAME OF TRANSLATORSYour names Select an overall theme for your workspace (including plasma theme, color scheme, mouse cursor, window and desktop switcher, splash screen, lock screen etc.) This module lets you configure the look of the whole workspace with some ready to go presets. Use Desktop Layout from theme Warning: your Plasma Desktop layout will be lost and reset to the default layout provided by the selected theme. You have to restart KDE for cursor changes to take effect. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-27 05:46+0100
PO-Revision-Date: 2016-09-08 04:03+0300
Last-Translator: Marek Laane <qiilaq69@gmail.com>
Language-Team: Estonian <kde-i18n-doc@kde.org>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 Välimuse üksikasjade seadistamine Kursoriseadistusi on muudetud Uute välismuspakettide allalaadimine qiilaq69@gmail.com Hangi uusi välimusi ... Marco Martin Marek Laane Siin saab valida oma töötsooni üldise teema (sealhulgas Plasma teema, värviskeem, hiirekursor, akende ja töölaua vahetaja, tiitelkuva, lukustusekraani jms.) See moodul võimaldab seadistada terve töötsooni välimust, pakkudes ka mõningaid valmisseadistusi. Kasuta teema töölauapaigutust Hoiatus: sinu Plasma töölaua paigutus läheb kaotsi, see lähtestatakse valitud teema pakutavale vaikimisi paigutusele. Kursorimuudatuste rakendamiseks tuleb KDE uuesti käivitada. 