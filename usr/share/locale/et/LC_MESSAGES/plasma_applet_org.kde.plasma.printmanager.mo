��          �      ,      �     �     �     �     �     �     �     �     �  .   �     .     L     \     m     �     �  H   �  �  �     �     �     �     �     �     �     �  
     2     '   K     s     �     �  	   �     �  @   �        	                                                                  
    &Configure Printers... Active jobs only All jobs Completed jobs only Configure printer General No active jobs No jobs No printers have been configured or discovered One active job %1 active jobs One job %1 jobs Open print queue Print queue is empty Printers Search for a printer... There is one print job in the queue There are %1 print jobs in the queue Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2015-02-21 09:37+0000
PO-Revision-Date: 2016-01-14 13:32+0200
Last-Translator: Marek Laane <qiilaq69@gmail.com>
Language-Team: Estonian <kde-et@linux.ee>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 &Seadista printereid... Ainult aktiivsed tööd Kõik tööd Ainult lõpetatud tööd Printeri seadistamine Üldine Aktiivseid töid pole Töid pole Ühtegi printerit pole seadistatud või tuvastatud Üks aktiivne töö %1 aktiivset tööd Üks töö %1 tööd Ava trükijärjekord Trükijärjekord on tühi Printerid Printeri otsimine... Järjekorras on üks trükitöö Järjekorras on %1 trükitööd 