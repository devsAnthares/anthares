��          �      \      �  *   �     �  .        B     P     `     t     �     �     �     �     �     �     �     �     �               5  �  C  ,   �       (   2     [     i     y     �     �     �     �     �     �     �     �               7     U     s                	                                          
                                         %1 Minimized Window: %1 Minimized Windows: %1 Window: %1 Windows: ...and %1 other window ...and %1 other windows Activity name Activity number Add Virtual Desktop Configure Desktops... Desktop name Desktop number Display: Does nothing General Icons No text Only the current screen Remove Virtual Desktop Selecting current desktop: Show Activity Manager... Shows desktop Project-Id-Version: plasma_applet_pager
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-19 03:29+0200
PO-Revision-Date: 2016-09-11 06:19+0300
Last-Translator: Marek Laane <qiilaq69@gmail.com>
Language-Team: Estonian <kde-i18n-doc@kde.org>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 %1 minimeeritud aken: %1 minimeeritud akent: %1 aken: %1 akent: ... ja veel %1 aken ... ja veel %1 akent Tegevuse nimi Tegevuse number Lisa virtuaalne töölaud Seadista töölaudu... Töölaua nimi Töölaua number Näitamine: Ei tehta midagi Üldine Ikoonid Tekst puudub Ainult aktiivne ekraan Eemalda virtuaalne töölaud Aktiivse töölaua valimisel: Näita tegevuste haldurit ... Töölaua näitamine 