��    E      D  a   l      �     �     �  5   	  #   ?  E   c  E   �  >   �     .  7   E     }     �     �     �     �     �                    ,     D     R     g     y     ~  !   �  F   �     �  <    	     =	     E	  *   N	     y	     �	     �	  	   �	     �	     �	     �	  
   �	     �	  
   �	  F   �	  :   ;
     v
     ~
     �
     �
     �
  8   �
     �
  	   �
  
                  '     8     V     _     ~     �     �  
   �     �     �     �     �       $     �  :     �     �  <   �     /  R   N  R   �  K   �     @  :   U     �     �     �     �  	   �     �     �          	          8     K     j     �     �  2   �  =   �        7        N     U  '   a     �     �     �     �     �     �  *   �     �  	          X     B   s     �     �     �     �     �  W   �     D     ]  
   e     p     �     �     �     �     �  
   �     �     �          "     1  	   @     J     \  $   m        E                               =      <          8   	                           #       %      7           /       0      :       9         1       >       !   3          "      D   +              A      ;          B       2   @   ?   $          '              ,   .   (   )   *      
      -   4              C           5      6   &                  %1 (%2) <b>%1</b> by %2 <em>%1 out of %2 people found this review useful</em> <em>Tell us about this review!</em> <em>Useful? <a href='true'><b>Yes</b></a>/<a href='false'>No</a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'><b>No</b></a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'>No</a></em> @infoFetching updates @infoIt is unknown when the last check for updates was @infoNo updates @infoNo updates are available @infoShould check for updates @infoThe system is up to date @infoUpdates @infoUpdating... Accept Addons Aleix Pol Gonzalez An application explorer Apply Changes Available backends:
 Available modes:
 Back Cancel Compact Mode (auto/compact/full). Could not close the application, there are tasks that need to be done. Delete the origin Directly open the specified application by its package name. Discard Discover Display a list of entries with a category. Extensions... Help... Install Installed Jonathan Thomas Launch List all the available modes. Loading... More... No Updates Open Discover in a said mode. Modes correspond to the toolbar buttons. Open with a program that can deal with the given mimetype. Rating: Remove Resources for '%1' Review Reviewing '%1' Running as <em>root</em> is discouraged and unnecessary. Search in '%1'... Search... Search: %1 Search: %1 + %2 Settings Short summary... Specify the new source for %1 Summary: Supports appstream: url scheme Tasks Tasks (%1%) Unable to find resource: %1 Update All Update Selected Update section nameUpdate (%1) Updates updates not selected updates selected © 2010-2016 Plasma Development Team Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:02+0100
PO-Revision-Date: 2016-12-09 00:40+0200
Last-Translator: Marek Laane <qiilaq69@gmail.com>
Language-Team: Estonian <kde-i18n-doc@kde.org>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 %1 (%2) <b>%1</b>, autor %2 <em>%2 inimesest %1 pidas(id) seda arvustust kasulikuks</em> <em>Hinda seda arvustust!</em> <em>Kas see oli kasulik? <a href='true'><b>Jah</b></a>/<a href='false'>Ei</a></em> <em>Kas see oli kasulik? <a href='true'>Jah</a>/<a href='false'><b>Ei</b></a></em> <em>Kas see oli kasulik? <a href='true'>Jah</a>/<a href='false'>Ei</a></em> Uuenduste tõmbamine Pole teada, millal viimati uuenduste olemasolu kontrolliti Uuendusi pole Uuendusi pole Tuleks kontrollida uuendusi Süsteem on täiesti värske Uuendused Uuendamine... Nõus Lisad Aleix Pol Gonzalez Avastusretk rakenduste seas Rakenda muudatused Saadaolevad taustaprogrammid:
 Saadaolevad režiimid:
 Tagasi Loobu Kompaktne režiim (automaatne/kompaktne/täielik). Rakendust ei saanud sulgeda, enne peavad tööd tehtud olema. Algupära kustutamine Määratud rakenduse otsene avamine paketi nime järgi. Unusta Avastusretk Kirjete loendi kuvamine kategooriatega. Laiendid... Abi... Paigalda Paigaldatud Jonathan Thomas Käivita Kõigi saadaolevate režiimide loetlemine. Laadimine... Rohkem... Uuendusi pole Avastusretke avamine määratud režiimis. Režiimid vastavad tööriistariba nuppudele. Avamine programmiga, mis suudab käidelda määratud MIME tüüpi. Hinnang: Eemalda "%1" ressursid Arvusta "%1" arvustus Käivitamine administraatorina (<em>root</em>) on äärmiselt ebasoovitatav ja tarbetu. Otsi kategoorias "%1"... Otsi... Otsing: %1 Otsing: %1 + %2 Seadistused Lühikokkuvõte %1 uue allika määramine Kokkuvõte: Appstreami URL-i skeemi toetus Ülesanded Ülesanded (%1%) Ressurssi ei leitud: %1 Uuenda kõik Uuenda valitud Uuendused (%1) Uuendused uuendust valimata uuendust valitud © 2010-2016: Plasma arendusmeeskond 