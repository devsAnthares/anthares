��    *      l  ;   �      �     �     �  )   �               /     H     ^  #   ~     �  
   �     �     �  %   �  +        E     Z     m  @   z     �     �     �     �               '     ,     9     ?     _     e  .   m     �  F   �  (   �  	   '  	   1  	   ;  '   E  7   m  "   �  �  �     d	     v	      �	     �	     �	     �	     �	     �	     �	     �	  	   
     
     
     3
  :   O
     �
     �
  
   �
  D   �
               '     4     H     c     k     q     �     �     �  
   �  1   �  #   �  S     )   f     �     �  
   �     �  
   �     �                   $                                 %                      *                                               	   #   )   "             &           
                        '             (   !    &Do not remember @actionWalk through activities @actionWalk through activities (Reverse) @action:buttonApply @action:buttonCancel @action:buttonChange... @action:buttonCreate @title:windowActivity Settings @title:windowCreate a New Activity @title:windowDelete Activity Activities Activity information Activity switching Are you sure you want to delete '%1'? Blacklist all applications not on this list Clear recent history Create activity... Description: Error loading the QML files. Check your installation.
Missing %1 For a&ll applications Forget a day Forget everything Forget the last hour Forget the last two hours General Icon Keep history Name: O&nly for specific applications Other Privacy Private - do not track usage for this activity Remember opened documents: Remember the current virtual desktop for each activity (needs restart) Shortcut for switching to this activity: Shortcuts Switching Wallpaper for in 'keep history for 5 months'for  unit of time. months to keep the history month  months unlimited number of monthsforever Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-08-19 23:13+0300
Last-Translator: Marek Laane <qiilaq69@gmail.com>
Language-Team: Estonian <kde-i18n-doc@kde.org>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 Ei &jäeta meelde Keri läbi tegevuste Keri läbi tegevuste (teistpidi) Rakenda Loobu Muuda... Loo Tegevuse seadistused Uue tegevuse loomine Tegevuse kustutamine Tegevused Tegevuse teave Tegevuse vahetamine Kas tõesti kustutada "%1"? Kõik loendis puuduvad rakendused pannakse musta nimekirja Puhasta viimase aja ajalugu Loo tegevus... Kirjeldus: Tõrge QML-failide laadimisel. Palun kontrolli paigaldust.
Puudub %1 Kõi&gis rakendustes Unusta päev Unusta kõik Unusta viimane tund Unusta viimased kaks tundi Üldine Ikoon Ajalugu hoitakse alles Nimi: A&inult määratud rakendustes Muu Privaatsus Privaatne - selle tegevuse kasutamist ei jälgita Avatud dokumendid jäetakse meelde: Iga tegevuse aktiivne virtuaalne töölaud jäetakse meelde (nõuab taaskäivitust) Sellele tegevusele lülitumise kiirklahv: Kiirklahvid Lülitumine Taustapilt    kuu  kuud igavesti 