��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  �  K     �  #   �  �    �  �  n   s
     �
     �
     �
  !        3     K  #   f     �                                          
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2003-09-19 21:35+0300
Last-Translator: Marek Laane <bald@online.ee>
Language-Team: Estonian <kde-et@linux.ee>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.0.2
Plural-Forms: nplurals=2; plural=n != 1;
  sek. Käivitami&se indikaatori aegumine: <H1>Tegumiriba märguanne</H1>
Sa võid kasutada ka käivitamise tagasiside teist meetodit, mida
antakse tegumiribal, kuhu ilmub nupp koos keerleva kettaga, mis
näitab, et sinu poolt käivitatud rakendus käivitub.
Võib juhtuda, et mõni rakendus pole sellest tagasiside andmisest
teadlik, sel juhul nupp kaob pärast väljal "Käivitamise indikaatori
aegumine" toodud aja möödumist. <h1>Hõivatud kursor</h1>
KDE pakub võimalust kasutada rakenduste käivitamisel kasutajale
tagasiside andmiseks hõivatud kursorit. Selle kasutamiseks lülita sisse
valik "Hõivatud kursori kasutamine". Kui soovid, et see kursor ka
vilguks, lülita sisse ka valik "Vilkumise kasutamine". Võib juhtuda,
et mõni rakendus pole sellest tagasiside andmisest teadlik, sel juhul
lõpetab kursor vilkumise pärast väljal "Käivitamise indikaatori
 aegumine" toodud aja möödumist. <h1>Käivitamise tagasiside</h1> Siin saad seadistada rakenduste käivitamisel kasutajale antavat tagasisidet. Vilkuv kursor Põrkav kursor &Hõivatud kursor T&egumiriba märguande kasutamine Hõivatud kursor puudub Passiivne hõivatud kursor &Käivitamise indikaatori aegumine: &Tegumiriba märguanne 