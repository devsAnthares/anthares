��          �   %   �      @     A  �   `  m   �  �   P  )   �  `        o     |     �     �     �      �     �     �  '        ,     >     ]     b     s  ?   �  Y   �  +     H   F  �  �     3     S  p   �     D	     _	  J   w	     �	     �	     �	  	   �	      
  #   
     C
     T
  #   d
     �
     �
     �
     �
     �
  6   �
  Y     "   q  ?   �                                                 	                                                               
              (c) 2003-2007 Fredrik Höglund <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) @item:inlistbox sizeResolution dependent A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Cursor Theme Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Fredrik Höglund Get new Theme Get new color schemes from the Internet Install from File NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. You have to restart the Plasma session for these changes to take effect. Project-Id-Version: kcminput
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-08-20 00:27+0300
Last-Translator: Marek Laane <qiilaq69@gmail.com>
Language-Team: Estonian <kde-i18n-doc@kde.org>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 (c) 2003-2007: Fredrik Höglund <qt>Kas ikka kindlasti eemaldada kursoriteema <strong>%1</strong>?<br />See kustutab kõik selle teema paigaldatud failid.</qt> <qt>Teemat, mida parajasti kasutad, ei saa kustutada.<br />Eelnevalt tuleb lülituda mõnele muule teemale.</qt> (Saadaolevad suurused: %1) Lahutusvõimest sõltuv Teema nimega %1 on ikooniteemade kataloogis juba olemas. Kas asendada see? Kinnitus Kursoriseadistusi on muudetud Kursoriteema Kirjeldus Lohista või kirjuta teema URL hasso@estpak.ee, qiilaq69@gmail.com Fredrik Höglund Hangi uus teema Hangi uusi värviskeeme internetist Paigalda failist Hasso Tepper, Marek Laane Nimi Kas kirjutada teema üle? Eemalda teema Fail %1 ei paista olevat korralik kursoriteema arhiiv. Kursoriteema arhiivi allalaadimine nurjus. Palun kontrolli, kas aadress %1 on ikka õige. Kursoriteema arhiivi %1 ei leitud. Muudatuste rakendamiseks tuleb Plasma seanss uuesti käivitada. 