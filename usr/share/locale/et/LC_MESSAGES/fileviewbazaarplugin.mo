��    !      $  /   ,      �  .   �  1     9   J     �  -   �  &   �  )   �  .   #  &   R  )   y  .   �  &   �  )   �  2   #  5   V  =   �  #   �     �  !     '   /  "   W  0   z  '   �  *   �     �          7     R     j     �     �  &   �  �  �      p	  %   �	  *   �	     �	  (   �	     $
  #   D
  !   h
     �
     �
  &   �
  !   �
  !     "   /  '   R  ,   z      �     �     �  !   �       !   <     ^     {     �     �     �     �     �     �       "   !                                                              	                                
                                    !                             @info:statusAdded files to Bazaar repository. @info:statusAdding files to Bazaar repository... @info:statusAdding of files to Bazaar repository failed. @info:statusBazaar Log closed. @info:statusCommit of Bazaar changes failed. @info:statusCommitted Bazaar changes. @info:statusCommitting Bazaar changes... @info:statusPull of Bazaar repository failed. @info:statusPulled Bazaar repository. @info:statusPulling Bazaar repository... @info:statusPush of Bazaar repository failed. @info:statusPushed Bazaar repository. @info:statusPushing Bazaar repository... @info:statusRemoved files from Bazaar repository. @info:statusRemoving files from Bazaar repository... @info:statusRemoving of files from Bazaar repository failed. @info:statusReview Changes failed. @info:statusReviewed Changes. @info:statusReviewing Changes... @info:statusRunning Bazaar Log failed. @info:statusRunning Bazaar Log... @info:statusUpdate of Bazaar repository failed. @info:statusUpdated Bazaar repository. @info:statusUpdating Bazaar repository... @item:inmenuBazaar Add... @item:inmenuBazaar Commit... @item:inmenuBazaar Delete @item:inmenuBazaar Log @item:inmenuBazaar Pull @item:inmenuBazaar Push @item:inmenuBazaar Update @item:inmenuShow Local Bazaar Changes Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2011-12-22 05:14+0200
Last-Translator: Marek Laane <bald@smail.ee>
Language-Team: Estonian <kde-et@linux.ee>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=2; plural=n != 1;
 Failid lisati Bazaari hoidlasse. Failide lisamine Bazaari hoidlasse... Failide lisamine Bazaari hoidlasse nurjus. Bazaari logi on suletud. Bazaari muudatuste sissekandmine nurjus. Bazaari muudatused kanti sisse. Bazaari muudatuste sissekandmine... Bazaari hoidla tõmbamine nurjus. Bazaari hoidla on tõmmatud. Bazaari hoidla tõmbamine... Bazaari hoidla üleslükkamine nurjus. Bazaari hoidla on üles lükatud. Bazaari hoidla üleslükkamine... Failid eemaldati Bazaari hoidlast. Failide eemaldamine Bazaari hoidlast... Failide eemaldamine Bazaari hoidlast nurjus. Muudatuste ülevaatamine nurjus. Muudatused on üle vaadatud. Muudatuste vaatamine... Bazaari logi käivitamine nurjus. Bazaari logi käivitamine... Bazaari hoidla uuendamine nurjus. Bazaari hoidla on uuendatud. Bazaari hoidla uuendamine... Bazaari lisamine... Bazaari sissekandmine... Bazaari kustutamine Bazaari logi Bazaari tõmbamine Bazaari üleslükkamine Bazaari uuendus Näita kohalikke Bazaari muudatusi 