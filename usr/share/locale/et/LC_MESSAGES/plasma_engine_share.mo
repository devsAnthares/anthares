��    
      l      �       �   $   �   %     :   <     w  '   �  -   �     �       '     �  <  %   �  &   �  (        D     _          �     �  !   �         	                            
        Could not detect the file's mimetype Could not find all required functions Could not find the provider with the specified destination Error trying to execute script Invalid path for the requested provider It was not possible to read the selected file Service was not available Unknown Error You must specify a URL for this service Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-10-05 03:20+0300
Last-Translator: Marek Laane <bald@smail.ee>
Language-Team: Estonian <kde-et@linux.ee>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.1
Plural-Forms: nplurals=2; plural=n != 1;
 Faili MIME tüübi määramine nurjus Kõigi vajalikke funktsioone ei leitud Määratud sihtkohaga pakkujat ei leitud Viga skripti käivitamisel Soovitud pakkuja vigane asukoht Valitud faili lugemine nurjus Teenus ei ole saadaval Tundmatu viga Teenuse jaoks tuleb määrata URL 