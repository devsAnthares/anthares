��          �   %   �      P     Q     b     w     �     �     �  
   �     �     �     �     �     �  /        3     Q     p     v     �     �     �     �     �     �     �       �  %     �     �     �  #   �     #     2     P  
   Y     d     j     r     ~  5   �     �     �     �                 $   $  %   I     o      �     �     �                            
                                                                                             	    %1 file %1 files %1 folder %1 folders %1 of %2 processed %1 of %2 processed at %3/s %1 processed %1 processed at %2/s Appearance Behavior Cancel Clear Configure... Finished Jobs List of running file transfers/jobs (kuiserver) Move them to a different list Move them to a different list. Pause Remove them Remove them. Resume Show all jobs in a list Show all jobs in a list. Show all jobs in a tree Show all jobs in a tree. Show separate windows Show separate windows. Project-Id-Version: kuiserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-06-01 01:01+0300
Last-Translator: Marek Laane <bald@smail.ee>
Language-Team: Estonian <kde-et@linux.ee>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=2; plural=n != 1;
 %1 fail %1 faili %1 kataloog %1 kataloogi %1 %2-st töödeldud %1 %2-st töödeldud kiirusega %3/s %1 töödeldud %1 töödeldud kiirusega %2/s Välimus Käitumine Loobu Puhasta Seadista... Lõpetatud tööd Käimas failiülekannete/tööde nimekiri (kuiserver) Liigutamine teise nimekirja Liigutamine teise nimekirja. Paus Eemaldamine Eemaldamine. Jätka Kõigi tööde näitamine nimekirjas Kõigi tööde näitamine nimekirjas. Kõigi tööde näitamine puuna Kõigi tööde näitamine puuna. Eraldi akende näitamine Eraldi akende näitamine. 