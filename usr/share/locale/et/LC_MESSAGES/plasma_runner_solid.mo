��          �      L      �  m   �     /  "   <  F   _  F   �  F   �  J   4  N     R   �     !  %   2  $   X  #   }  $   �  %   �  &   �  �        �  �  �     @     R  )   h  _   �  Z   �  V   M  T   �  V   �  `   P	     �	     �	  	   �	     �	     �	     �	     �	      
     
                                
                          	                                          Close the encrypted container; partitions inside will disappear as they had been unpluggedLock the container Eject medium Finds devices whose name match :q: Lists all devices and allows them to be mounted, unmounted or ejected. Lists all devices which can be ejected, and allows them to be ejected. Lists all devices which can be mounted, and allows them to be mounted. Lists all devices which can be unmounted, and allows them to be unmounted. Lists all encrypted devices which can be locked, and allows them to be locked. Lists all encrypted devices which can be unlocked, and allows them to be unlocked. Mount the device Note this is a KRunner keyworddevice Note this is a KRunner keywordeject Note this is a KRunner keywordlock Note this is a KRunner keywordmount Note this is a KRunner keywordunlock Note this is a KRunner keywordunmount Unlock the encrypted container; will ask for a password; partitions inside will appear as they had been plugged inUnlock the container Unmount the device Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-11-16 01:09+0200
Last-Translator: Marek Laane <bald@smail.ee>
Language-Team: Estonian <kde-et@linux.ee>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.1
Plural-Forms: nplurals=2; plural=n != 1;
 Lukusta konteiner Väljasta andmekandja Seadmete leidmine nime põhjal :q: järgi Kõigi seadmete näitamine ning nende ühendamise, lahutamise või väljastamise võimaldamine. Kõigi seadmete näitamine, mida saab väljastada, ning nende väljastamise võimaldamine. Kõigi seadmete näitamine, mida saab ühendada, ning nende ühendamise võimaldamine. Kõigi seadmete näitamine, mida saab lahutada, ning nende lahutamise võimaldamine. Kõigi seadmete näitamine, mida saab lukustada, ning nende lukustamine võimaldamine. Kõigi seadmete näitamine, mille lukustust saab eemaldada, ning lahtilukustamise võimaldamine. Ühenda seade seade väljasta lukustamine ühenda lukustuse eemaldamine lahuta Lukusta konteiner lahti Lahuta seade 