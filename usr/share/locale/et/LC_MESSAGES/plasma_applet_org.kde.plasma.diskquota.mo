��    
      l      �       �   
   �      �        .   0     _     t     �  (   �  8   �  �       �     �     �  5   �     !     ;     C     K     `        
                             	        Disk Quota No quota restrictions found. Please install 'quota' Quota tool not found.

Please install 'quota'. Running quota failed e.g.: 12 GiB of 20 GiB%1 of %2 e.g.: 8 GiB free%1 free example: Quota: 83% usedQuota: %1% used usage of quota, e.g.: '/home/bla: 38% used'%1: %2% used Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2016-12-09 00:26+0200
Last-Translator: Marek Laane <qiilaq69@gmail.com>
Language-Team: Estonian <kde-i18n-doc@kde.org>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Kettakvoodid Kvoodipiiranguid ei leitud. Palun paigalda "quota" Kvooditööriista ei leitud.

Palun paigalda "quota". Quota käivitamine nurjus %1 / %2 %1 vaba Kvoot: %1% kasutatud %1: %2% kasutatud 