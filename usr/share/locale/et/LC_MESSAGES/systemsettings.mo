��    +      t  ;   �      �  A   �     �  /        B     K     _     q     �     �     �     �  	   �     �  3   �  	                $   =  *   b     �  	   �  5   �     �     �  
             "     A  5   P  6   �     �  ,   �  /   �     &     =  O   M  Z   �  b   �  	   [  
   e  P   p     �  �  �  3   v
     �
  6   �
     �
          $  !   ?     a     s     �     �     �     �  3   �     �     �       !        8     @     D  4   P     �     �     �     �     �     �  8   �  0   !     R  A   e     �  !   �     �  X   �  Z   ?  R   �     �  
   �  V        X            *             '      %   
   &         (                                                "             	                     #                                !                 )         +       $    %1 is an external application and has been automatically launched (c) 2009, Ben Cooksley <i>Contains 1 item</i> <i>Contains %1 items</i> About %1 About Active Module About Active View About System Settings All Settings Apply Settings Author Ben Cooksley Configure Configure your system Determines whether detailed tooltips should be used Developer Dialog EMAIL OF TRANSLATORSYour emails Expand the first level automatically General config for System SettingsGeneral Help Icon View Internal module representation, internal module model Internal name for the view used Keyboard Shortcut: %1 Maintainer Mathias Soeken NAME OF TRANSLATORSYour names No views found Provides a categorized icons view of control modules. Provides a classic tree-based view of control modules. Relaunch %1 Reset all current changes to previous values Search through a list of control modulesSearch Show detailed tooltips System Settings System Settings was unable to find any views, and hence has nothing to display. System Settings was unable to find any views, and hence nothing is available to configure. The settings of the current module have changed.
Do you want to apply the changes or discard them? Tree View View Style Welcome to "System Settings", a central place to configure your computer system. Will Stephenson Project-Id-Version: systemsettings
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:08+0100
PO-Revision-Date: 2016-01-11 21:30+0200
Last-Translator: Marek Laane <qiilaq69@gmail.com>
Language-Team: Estonian <kde-et@linux.ee>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 %1 on väline rakendus, mis käivitati automaatselt (c) 2009: Ben Cooksley <i>Sisaldab 1 elementi</i> <i>Sisaldab %1 elementi</i> Teave %1 kohta Teave aktiivse mooduli kohta Teave aktiivse vaate kohta Teave Süsteemi seadistuste kohta Kõik seadistused Rakenda seadistused Autor Ben Cooksley Seadistamine Süsteemi seadistamine Määrab, kas kasutada üksikasjalikke kohtspikreid Arendaja Dialoog qiilaq69@gmail.com Esimese taseme automaatne avamine Üldine Abi Ikoonivaade Sisemine moodulite esindamine, sisemine moodulimudel Kasutatava vaate seesmine nimi Kiirklahv: %1 Hooldaja Mathias Soeken Marek Laane Vaateid ei leitud Juhtimiskeskuse moodulite kategooriatesse jagatud vaade. Juhtimiskeskuse moodulite klassikaline puuvaade. Käivita %1 uuesti Kõigi tehtud muudatuste lähtestamine varasematele väärtustele Otsing Üksikasjalike vihjete näitamine Süsteemi seadistused Süsteemi seadistused ei leidnud ühtegi vaadet ja seepärast ei saa ka midagi näidata. Süsteemi seadistused ei leidnud ühtegi vaadet ja seepärast ei saa ka midagi seadistada. Aktiivse mooduli seadistusi on muudetud.
Kas muudatused rakendada või tühistada? Puuvaade Vaatestiil Tere tulemast kasutama Süsteemi seadistusi, sinu arvuti keskset seadistamiskeskkonda. Will Stephenson 