��    >        S   �      H     I  !   e  "   �  &   �  ,   �  #   �  &   "  !   I  &   k  '   �  "   �  #   �  "     '   $     L     _  +   c  
   �     �     �     �     �     �     �  U   �  7   J     �     �     �     �      �     �     �     	     	  !   &	     H	  	   M	     W	     _	     	     �	  &   �	     �	     �	     �	     
     
     #
     5
  4   =
  $   r
     �
     �
     �
     �
     �
            &   )     P  �  j       
        &  	   +     5     D     M     ]     e  
   u     �     �     �  
   �     �     �  3   �     �                    8     ?     U  ^   ]  ?   �     �          (     0  %   6     \     |     �     �     �     �     �     �     �             #   0     T     r     z     �     �     �     �  4   �  '   �          6     T     d     w     �     �  '   �  )   �         #          1   &                  	   <          6       =   0   2          +                                 '   ;              5   -             /   !      *               3   )                    
          $             ,   9   8   :           %       "      (       >   4      7      .       &Matching window property:  @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Border @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large @item:inlistbox Button size:Large @item:inlistbox Button size:Normal @item:inlistbox Button size:Small @item:inlistbox Button size:Very Large Active Window Glow Add Add handle to resize windows with no border Animations B&utton size: Border size: Button mouseover transition Center Center (Full Width) Class:  Configure fading between window shadow and glow when window's active state is changed Configure window buttons' mouseover highlight animation Decoration Options Detect Window Properties Dialog Edit Edit Exception - Oxygen Settings Enable/disable this exception Exception Type General Hide window title bar Information about Selected Window Left Move Down Move Up New Exception - Oxygen Settings Question - Oxygen Settings Regular Expression Regular Expression syntax is incorrect Regular expression &to match:  Remove Remove selected exception? Right Shadows Tit&le alignment: Title:  Use the same colors for title bar and window content Use window class (whole application) Use window title Warning - Oxygen Settings Window Class Name Window Drop-Down Shadow Window Identification Window Property Selection Window Title Window active state change transitions Window-Specific Overrides Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-13 05:20+0100
PO-Revision-Date: 2016-07-27 18:18+0300
Last-Translator: Marek Laane <qiilaq69@gmail.com>
Language-Team: Estonian <kde-i18n-doc@kde.org>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 &Sobiv akna omadus:  Hiiglaslik Suur Piireteta Külgpiireteta Tavaline Ülemõõduline Tilluke Superhiiglaslik Väga suur Suur Tavaline Väike Väga suur Aktiivse akna helendamine Lisa Pideme lisamine piireteta akende suuruse muutmiseks Animatsioonid N&upu suurus: Piirde suurus: Nupu hiirealune üleminek Keskel Keskel (täislaiuses) Klass:  Hääbumise seadistamine akna varjamise ja esiletõstmise vahel akna aktiivse oleku muutumisel Aknanuppude hiirealuse esiletõstmise animatsiooni seadistamine Dekoratsiooni valikud Tuvasta akna omadused Dialoog Muuda Erandi muutmine - Oxygeni seadistused Selle erandi lubamine/keelamine Erandi tüüp Üldine Akna tiitliriba peitmine Valitud akna teave Vasakul Liiguta alla Liiguta üles Uus erand - Oxygeni seadistused Küsimus - Oxygeni seadistused Regulaaravaldis Regulaaravaldise süntaks on vigane So&bivus regulaaravaldisega:  Eemalda Kas eemaldada valitud erand? Paremal Varjud Tiitli joo&ndus: Tiitel:  Tiitliribal ja akna sisus samade värvide kasutamine Kasutatakse akna klassi (kogu rakendus) Kasutatakse akna tiitlit Hoiatus - Oxygeni seadistused Aknaklassi nimi Akna heidetav vari Akna identifikaator Akna omaduse valik Akna tiitel Akna aktiivse oleku muutuse üleminekud Akende spetsiifilised tühistavad sätted 