��          �            h     i     r     �  	   �  *   �     �  "   �                 ;   +     g     z       �  �          -     >     Y     e     �  2   �     �     �     �  C        E     [  	   `     	                 
                                                          &Sort by &Trigger word: &Use trigger word CPU usage It is not sure, that this will take effect Kill Applications Config Process ID: %1
Running as user: %2 Send SIGKILL Send SIGTERM Terminate %1 Terminate running applications whose names match the query. inverted CPU usage kill nothing Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2009-10-28 07:32+0200
Last-Translator: Marek Laane <bald@smail.ee>
Language-Team: Estonian <kde-et@linux.ee>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=2; plural=n != 1;
 &Sorteerimise alus: Käivi&tussõna: Käivit&ussõna kasutamine CPU koormus Pole kindel, kas see toimib Rakenduste tapmise seadistamine Protsessi ID: %1
Käivitatud kasutaja %2 õigustes Saada SIGKILL Saada SIGTERM Lõpeta %1 töö Päringule vastava nimega töötavate rakenduste töö lõpetamine. CPU koormus teistpidi tapa ei midagi 