��    (      \  5   �      p     q     z     �     �     �     �  -   �  r   �  	   g  	   q     {     �     �  
   �     �     �      �     �     �                !     @  	   E     O     U     ]     j     x     �     �     �     �  +   �            S     )   h     �  �  �     P     e     z      �  
   �  F   �  S   	  �   `	     E
     Y
     m
     v
     �
  !   �
     �
  U   �
     $  4   =     r       N   �     �  	   �  #   �           6  &   U  8   |  T   �  T   
  ]   _  
   �     �  `   �     @     M  �   `  b   C  )   �                            #         $             %             &             !      
                   '                              "   (          	                                &Amount: &Effect: &Second color: &Semi-transparent &Theme (c) 2000-2003 Geert Jansen <qt>Installing <strong>%1</strong> theme</qt> A problem occurred during the installation process; however, most of the themes in the archive have been installed Ad&vanced All Icons Co&lor: Colorize Confirmation Desaturate Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Effect Parameters Gamma Icons Icons Control Panel Module NAME OF TRANSLATORSYour names Name No Effect Panel Preview Remove Theme Set Effect... Setup Active Icon Effect Setup Default Icon Effect Setup Disabled Icon Effect Size: Small Icons The file is not a valid icon theme archive. To Gray Toolbar Unable to download the icon theme archive;
please check that address %1 is correct. Unable to find the icon theme archive %1. Use of Icon Project-Id-Version: kcmicons
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-08 06:27+0100
PO-Revision-Date: 2006-01-04 13:30-0600
Last-Translator: KUSHAL DAS <programmerkd@yahoo.co.in>
Language-Team: Bengali <kde-translation@bengalinux.org>
Language: bn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.9.1
Plural-Forms: nplurals=2; plural=n != 1;
 &পরিমাণ: &এফেক্ট: দ্বিতী&য় রঙ: &অর্ধ স্বচ্ছ থি&ম (c) ২০০০-২০০৩ গ্রিট জ্যানসেন <qt><strong>%1</strong> থীম ইনস্টল করা হচ্ছে</qt> ইনস্টলেশন চলাকালীন একটি সমস্যা ঘটেছে; তবে আর্কাইভের অধিকাংশ থীম সফলভাবে ইনস্টল হয়েছে &অগ্রসর সব আইকন &রঙ: রঙীন অনুমোদন ডিস্যাচুরেট বর্ণনা থীম ইউ-আর-এল লিখুন অথবা টেনে আনুন programmerkd@yahoo.co.in এফেক্ট প্যারামিটার গামা আইকন আইকন নিয়ন্ত্রণ প্যানেল মডিউল কুশল দাস নাম কোন এফেক্ট নয় প্যানেল প্রাকদর্শন থীম সরিয়ে ফেলো এফেক্ট নির্ধারণ করো... সক্রিয় আইকন এফেক্ট ব্যবস্থাপনা ডিফল্ট আইকন এফেক্ট ব্যবস্থাপনা নিষ্ক্রিয় আইকন এফেক্ট ব্যবস্থাপনা মাপ: ছোট আইকন ফাইলটি একটি বৈধ আইকন থীম আর্কাইভ নয়। ধূসর টুলবার আইকন থীম আর্কাইভটি ডাউনলোড করতে অক্ষম;
অনুগ্রহ করে %1 ঠিকানাটি সঠিক কিনা পরীক্ষা করুন। আইকন থীম আর্কাইভ %1 খুঁজে পাওয়া যায়নি। আইকনের ব্যবহার  