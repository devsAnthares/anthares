��    w      �  �   �      
     
     
     "
     1
     5
     9
     A
     J
  	   X
     b
  
   k
     v
     ~
     �
  5   �
  %   �
     �
       
             /     8  	   G     Q     e     �  
   �     �     �     �  	   �  
   �                     ,     9     E     R     r     y     �     �     �     �     �     �     �           '     0     E     [     r  "   �     �     �      �     �            
     
   (     3     Q     k     n     �     �     �     �     �     �     �     �     �     �     �     �     	                    $     +     /  
   <     G     S     `     f     t     �     �     �     �     �     �     �     �     �     �     �  
   �                 	        )  	   2     <     L     Y  0   e  
   �     �     �     �  �  �     r      u  -   �     �     �     �     �     �        #   (     L     ^     w     {     ~     �     �  /   �  !   �  (        ;  9   N     �  9   �  X   �  N   :     �  C   �  J   �  J   +     v     �     �     �  "   �  "   �  "     "   <  U   _     �  F   �  @     6   V  M   �  $   �  "      1   #  "   U     x     �  5   �  C   �  L   "  '   o     �     �     �     �  :   �     "     /  '   E     m  >   �  J   �       /   !  %   Q     w     �     �     �     �     �     �     �  (        0  ?   L     �     �     �     �     �     �     	     %     >  %   W     }     �  Q   �  &   �  5   $  %   Z  	   �     �     �     �     �     �     �     �        &   -      T   "   a   "   �   "   �   "   �   '   �      !     .!  {   A!     �!     �!     �!  2   	"        e   
      H   c       v   l   M   '      *   b   Y         X   :                  p   t   =      "   8   ^      D      3         V   R   Q   [   9   1       T   >   F   #   ]   5   \   $   W       O          K   (   E      4      7   A   S   ?   I           m   6      0   Z          g                           &       o   s   ,   -              w   n          %       r   u      B       G               f   N   L   k   j          +   `       @   h   U               q       i   d   /      P   ;   _   a           C   .          !       J          	                   <   )   2                                        %  msec  pixel  pixels  px  ° &Color: &Height: &Layout mode: &Opacity: &Radius: &Strength: &Width: -90 90 @title:group actions when clicking on desktopDesktop @title:tab Advanced SettingsAdvanced @title:tab Basic SettingsBasic Activate window Activation Additional Options Advanced Animate switch Animation Animation duration: Animation on tab box close Animation on tab box open Appearance Apply effect to &groups Apply effect to &panels Apply effect to the desk&top Automatic Background Background color: Bottom Bottom Left Bottom Right Bottom-Left Bottom-Right Bring window to current desktop Center Clear All Mouse Marks Clear Last Mouse Mark Clear Mouse Marks Close after mouse dragging Custom Desktop Cube Desktop Cylinder Desktop Sphere Desktop name alignment:Disabled Dialogs: Display desktop name Display window &icons Display window &titles Dropdown menus: Duration of flip animationDefault Duration of rotationDefault Duration of zoomDefault EMAIL OF TRANSLATORSYour emails Enable &advanced mode Far Faster Fill &gaps Filter:
%1 General Translucency Settings Ignore &minimized windows In Inactive windows: Inside Graph KWin Layout mode: Left Less Light Menus: Middle button: More NAME OF TRANSLATORSYour names Natural Natural Layout Settings Near Nicer Nowhere Opacity Opaque Out Popup menus: Reflection Reflections Regular Grid Right Right button: Send window to all desktops Show &panels Show Desktop Grid Show desktop Size Sphere Strong Tab 1 Tab 2 Text Text alpha: Text color: Text font: Text position: Top Top Left Top Right Top-Left Top-Right Torn-off menus: Translucency Transparent Use this effect for walking through the desktops Wallpaper: Windows Zoom Zoom &duration: Project-Id-Version: kwin
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-02 03:18+0100
PO-Revision-Date: 2010-06-01 10:25-0700
Last-Translator: Deepayan Sarkar <deepayan.sarkar@gmail.com>
Language-Team: Bengali <kde-translation@bengalinux.org>
Language: bn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=2; plural=n != 1;
  %  মিলি সেকণ্ড  পিক্সেল  পিক্সেল  px  ° &রঙ: &উচ্চতা: লে-&আউট মোড: স্বচ্ছতা (&স): ব্যাসার্ধ (&ব): &শক্তি: প্রস্থ [&W]: -90 90 ডেস্কটপ অগ্রসর প্রাথমিক উইণ্ডো সক্রিয় করো অ্যাকটিভেশন অতিরিক্ত অপশনস অগ্রসর বদল করা অ্যানিমেট করো অ্যানিমেশন অ্যানিমেশন-এর সময়কাল: ট্যাব বাক্স বন্ধ করতে অ্যানিমেশন ট্যাব বাক্স খুলতে অ্যানিমেশন চেহারা এফেক্ট গ্রুপ-এ প্রয়োগ করো এফেক্ট প্যানে&ল-এ প্রয়োগ করো এফেক্ট ডেস্ক&টপ-এ প্রয়োগ করো স্বয়ংক্রিয় পটভূমি পটভূমির রং: নীচে নীচে-বাঁদিকে নীচে-ডানদিকে নীচে-বাঁদিকে নীচে-ডানদিকে উইণ্ডো সক্রিয় ডেস্কটপে নিয়ে এসো মাঝামাঝি সমস্ত মাউস চিহ্ন ফাঁকা করো শেষ মাউস চিহ্ন ফাঁকা করো মাউস চিহ্ন ফাঁকা করো মাউস ড্র্যাগ করার পর বন্ধ করো স্বনির্বাচিত ডেস্কটপ কিউব ডেস্কটপ সিলিণ্ডার ডেস্কটপ গোলক নিষ্ক্রিয় ডায়ালগ: ডেস্কটপের নাম দেখাও উইণ্ডো &আইকন প্রদর্শন করো উইণ্ডো শিরোনা&ম প্রদর্শন করো ড্রপ-ডাউন মেনু: ডিফল্ট ডিফল্ট ডিফল্ট anirban@bengalinux.org &অগ্রসর মোড সক্রিয় করো দূরে দ্রুততর ফাঁক &ভর্তি করো ফিল্টার:
%1 সাধারণ স্বচ্ছতা সেটিংস মিনিমাই&জ করা উইণ্ডো বাদ দাও ভিতরে নিষ্ক্রিয় উইণ্ডো: গ্রাফের ভেতরে কে-উইন লে-আউট মোড: বাঁদিকে কম অল্প মেনু: মাঝের বাটন: বেশী অনির্বাণ মিত্র স্বাভাবিক স্বাভাবিক লে-আউট সেটিংস কাছে দেখতে ভাল কোথাও না স্বচ্ছতা অস্বচ্ছ বাইরে পপ-আপ মেনু: প্রতিফলন প্রতিফলন রেগুলার গ্রিড ডানদিকে ডান বাটন: উইণ্ডোটি সমস্ত ডেস্কটপে পাঠাও প্যানে&ল দেখাও ডেস্কটপ গ্রিড দেখাও ডেস্কটপ দেখাও মাপ গোলক বেশী ট্যাব ১ ট্যাব ২ লেখা লেখার আলফা: লেখার রং: লেখার ফন্ট: লেখার অবস্থান: উপরে উপরে-বাঁদিকে উপরে-ডানদিকে উপরে-বাঁদিকে উপরে-ডানদিকে আলাদা করা মেনু: স্বচ্ছতা স্বচ্ছ ডেস্কটপ পরিদর্শন করতে এই এফেক্টটি ব্যবহার করো ওয়ালপেপার: উইণ্ডো ছোট/বড় করো  ছোট/বড় করার &সময়কাল: 