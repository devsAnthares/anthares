��          �      ,      �     �  /   �     �     �     �     �            I   ,     v  "   z     �  6   �  *   �          )  �  6     �  {     =   ~     �  Y   �  j   +  (   �  !   �  �   �     �  m   �  ^   7  �   �  t   9	  3   �	  3   �	                                                           
      	              to  A non-proportional font (i.e. typewriter font). Ad&just All Fonts... BGR Click to change all fonts Configure Anti-Alias Settings Configure... E&xclude range: Hinting is a process used to enhance the quality of fonts at small sizes. RGB Used by menu bars and popup menus. Used by the window titlebar. Used for normal text (e.g. button labels, list items). Used to display text beside toolbar icons. Vertical BGR Vertical RGB Project-Id-Version: kcmfonts
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-24 05:54+0100
PO-Revision-Date: 2004-10-18 22:55-0500
Last-Translator: Khandakar Mujahidul Islam <suzan@bengalinux.org>
Language-Team: Bengali <kde-translation@bengalinux.org>
Language: bn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.3.1
Plural-Forms: nplurals=2; plural=n != 1;
  থেকে  একটি অ-সমানুপাতিক ফন্ট (যেমন, টাইপরাইটার ফন্ট)। সকল ফন্ট &পরিবর্তন করো... বি-জি-আর সকল ফন্ট পরিবর্তন করতে ক্লিক করুন অ্যান্টি-এলিয়াস সেটিংস্‌ কন্‌ফিগার করো কন্‌ফিগার করো... সীমার বা&ইরে: হিন্টিং হচ্ছে একটি প্রক্রিয়া যার মাধ্যমে ছোট আকারের ফন্টের মান বৃদ্ধি করা যায়। আর-জি-বি মেনু বার এবং পপ্‌-আপ মেনু কর্তৃত ব্যবহৃত। উইন্ডো শিরোনাম বার কর্তৃক ব্যবহৃত। সাধারণ লেখার জন্য ব্যবহৃত (যেমন, বাটনের লেবেল, তালিকার আইটেম)। টুলবার আইকনগুলোর পাশে লেখা দেখাতে ব্যবহৃত। লম্বালম্বি বি-জি-আর লম্বালম্বি আর-জি-বি 