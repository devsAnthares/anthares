��            )   �      �  &   �     �     �     �     �      �               .     6  ,   S  3   �     �     �     �     �     �  '        4     S     Y     o     �     �     �     �     �  &   �     �  �  �     �  L   �     !     8     S     j     �  ]   �  "   �  J     �   i  �   �  V   �	  O   �	     =
     K
  (   Y
  g   �
  "   �
  
     F     O   _     �  G   �       :   $     _  J   o     �                                       
                      	                                                                            @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2012-07-16 14:27+0530
Last-Translator: Deepayan Sarkar <deepayan.sarkar@gmail.com>
Language-Team: American English <kde-translation@bengalinux.org>
Language: en_US
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.4
 সাধারণ একটি নতুন স্ক্রিপ্ট যোগ করো। যোগ করো... বাতিল করব? মন্তব্য: deepayan.sarkar@gmail.com সম্পাদনা নির্বাচিত স্ক্রিপ্ট সম্পাদনা করো। সম্পাদন করো... নির্বাচিত স্ক্রিপ্ট চালাও। ইন্টারপ্রিটার "%1"-এর জন্য স্ক্রিপ্ট তৈরি করতে ব্যর্থ স্ক্রিপ্ট-ফাইল "%1"-এর জন্য ইন্টারপ্রিটার নির্ধারণ করতে ব্যর্থ ইন্টারপ্রিটার "%1" লোড করতে ব্যর্থ স্ক্রিপ্ট-ফাইল "%1"খুলতে ব্যর্থ ফাইল: আইকন: ইন্টারপ্রিটার: রুবি ইন্টারপ্রিটার-এর নিরাপত্তার স্তর দীপায়ন সরকার নাম: "%1" নামে কোনো ক্রিয়া (function) নেই তেমন কোনও ইন্টারপ্রিটার "%1"নেই সরিয়ে ফেলো নির্বাচিত স্ক্রিপ্ট সরাও। চালাও স্ক্রিপ্ট-ফাইল "%1" নেই। থামাও নির্বাচিত স্ক্রিপ্ট থামাও। লেখা: 