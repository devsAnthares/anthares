��          �            x  �   y  m   �  `   i     �     �     �     �           3     R     W     h  ?   u  Y   �  +     �  ;  %  �       �        	  5   !	     W	  Y   g	     �	  S   �	  
   )
  7   4
     l
  �   �
  �      ]   �        	                                                  
                     <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. Project-Id-Version: kcminput
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2009-02-10 12:28+0530
Last-Translator: Ravishankar Shrivastava <raviratlami@aol.in>
Language-Team: Hindi <kde-i18n-doc@lists.kde.org>
Language: hi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 0.2
Plural-Forms: nplurals=2; plural=(n!=1);
 <qt>का आप मन सही मं संकेतक प्रसंग<strong>%1</strong> ल मिटाना चाहथो ?<br />ये एखर प्रसंग द्वारा इनस्टाल सब्बो फाइल मन ल मिटा देही.</qt> <qt>आप ए प्रसंग जऊन ला अभी परयोग करत हो मिटा नइ सकव.<br />पहिली आप मन ल दुसरा प्रसंग मं स्विच करना पड़ही.</qt> एक प्रसंग %1 नाम से चिनहा प्रसंग फोल्डर मं पहिली से ही मिलत हे. का आप मन ओला एखर से बदलना चाहथो ? पुस्टिकरन संकेतक सेटिंग बदलिस वर्नन प्रसंग यूआरएल खींचलाव या टाइप करव raviratlami@aol.in, रविसंकर सिरीवास्तव, जी. करूनाकर नाम  प्रसंग मेटाय के लिखव? प्रसंग हटाव फाइल %1 वैध संकेतक प्रसंग अभिलेखागार जइसन प्रतीत नइ होथे. संकेतक प्रसंग अभिलेख डाउनलोड करे मं अक्छम; किरपा करके, जांचव कि पता %1 सही हे. संकेतक प्रसंग अभिलेख %1 पाय मं अक्छम. 