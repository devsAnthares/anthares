��          �   %   �      p  &   q  �   �          7     >     G     P     ^     k     |      �  	   �  c   �          ,     8     W     q     y     �     �     �  	   �  C   �  j   �     W     m  �  {  [   .  =  �  *   �  	   �     �  !   	      9	  %   Z	  1   �	     �	     �	     �	  �   �	  2   �
  "      S   C  4   �     �     �  %        (     4     @  |   Z  �   �  5   �                                                                  	                             
                                 (c) 2002 Karol Szwed, Daniel Molkentin <h1>Style</h1>This module allows you to modify the visual appearance of user interface elements, such as the widget style and effects. @title:tab&Fine Tuning Button Checkbox Combobox Con&figure... Configure %1 Daniel Molkentin Description: %1 EMAIL OF TRANSLATORSYour emails Group Box If you enable this option, KDE Applications will show small icons alongside some important buttons. KDE Style Module Karol Szwed NAME OF TRANSLATORSYour names No description available. Preview Radio button Ralf Nolden Tab 1 Tab 2 Text Only There was an error loading the configuration dialog for this style. This area shows a preview of the currently selected style without having to apply it to the whole desktop. Unable to Load Dialog Widget style: Project-Id-Version: kcmstyle
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-17 05:44+0100
PO-Revision-Date: 2009-02-09 18:12+0530
Last-Translator: Ravishankar Shrivastava <raviratlami@aol.in>
Language-Team: Hindi <kde-i18n-doc@lists.kde.org>
Language: hi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 0.2
Plural-Forms: nplurals=2; plural=(n!=1);
 (c) 2002 केराल स्वेद, डेनियल मालकेंटाइन <h1>सैली</h1>ये माड्यूल आप मन ल कमइया इंटरफेस तत्वों के प्रद्रसित सक्ल-सूरत जइसन कि विजेट सैली अउ प्रभाव ल परिवर्धित करन देथे . फाइन ट्यूनिंग (&F) बटन सही-डब्बा काम्बोबाक्स कान्फिगर... (&f) कान्फिगर करव %1 डेनियल माल्केनटिन वर्ननः %1 raviratlami@aol.in, समूह डब्बा यदि आप मन ये विकल्प चुनथो , तहां केडीई अनुपरयोग कुछ जरूरी बटन मन के बाजू मं छोटे चिनहा देखायगा. केडीई सैली माड्यूल केरोल स्जवेड रविसंकर सिरीवास्तव, जी. करूनाकर कोई वर्नन नइ मिलत हे प्रिव्यू रेडियो बटन राल्फ नाल्देन टैब 1 टैब 2 सिरिफ पाठ ये सैली बर कान्फिगरेसन गोठ लोड करे मं गलती होइस. ये छेत्र अभी हाल के चुने गे सैली के प्रिव्यू प्रदर्सित करथे, एला पूरा डेस्कटाप मं लागू करे बिना. गोठ लोड करे मं अक्छम. विजेट सैली: 