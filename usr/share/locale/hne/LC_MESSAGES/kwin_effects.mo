��    x      �  �   �      (
     )
     ,
     2
     6
     >
     G
     U
  	   c
     m
  	   v
     �
  
   �
     �
     �
  %   �
     �
  
   �
     �
               '     ;     V  
   p     {     �     �  	   �  
   �     �     �     �                 
   (     3     8     ?     U     k     }     �     �  ,   �  -   �          $     1     :     O     e     {     �  D   �     �     �            
     
        %     3     Q     k     }     �     �     �     �     �     �     �     �     �     �     �     �                            
   (     3     ?     L     R  #   e     �  	   �     �     �     �     �     �     �     �  
   �     �     �     	  %   (  (   N  #   w     �     �  	   �     �  	   �     �     �     �  0   �  
        *     >     T     Y  �  i           #     2     5     E     ]  2   z  '   �     �     �               <     W     p  	   �     �  +   �     �  ,   �  #     J   ?  C   �  	   �  J   �  J   #  C   n     �     �  3   �          *      H     i      �     �     �     �  F   �  F   (  6   o  A   �  2   �       p   1  `   �  8        <  
   \  2   g  3   �  4   �  7        ;  �   Q  2     >   ?  	   ~  	   �  )   �     �     �  S   �  `   G  &   �  #   �     �            2        R     f  &   s     �  ;   �  	   �     �       "   -     P     l     y  #   �  $   �  $   �  "   �            '   b   H   8   �      �      !     !     !     *!  	   6!     @!     [!     r!     �!  <   �!  S   �!  z   ;"  �   �"  a   8#  	   �#     �#     �#     �#     �#  *   $  *   ?$  *   j$  x   �$     %  <   $%  M   a%  	   �%     �%     J   `   	      (       H       l   I   $             V      E   U   A   _   4          p   t   ;         ^       ,   ?      2         a   N   M   .              P   <              5   m   !   T   u   K          9   \   C      3   w   7   @   O   =   8   /   j   h   6      o   %      Y   b       i                
   #           s   *   +              x   S          "   W                      D           B   F   &       k   e           )           >   c   Q               q       d   R   1      L   :   [   ]         G   -   n          g   Z                              r   '   0       v   f   X                        %  msec  px &Color: &Height: &Layout mode: &Move factor: &Opacity: &Radius: &Spacing: &Stiffness: &Strength: &Width: &Wobbliness @title:tab Advanced SettingsAdvanced @title:tab Basic SettingsBasic Activation Additional Options Advanced Animate switch Animation duration: Animation on tab box close Animation on tab box open Appearance Apply effect to &groups Apply effect to &panels Apply effect to the desk&top Automatic Background Background color: Bottom Bottom Left Bottom Right Bottom-Left Bottom-Right Cap color: Caps Center Clear All Mouse Marks Clear Last Mouse Mark Clear Mouse Marks Close after mouse dragging Combobox popups: Custom Define how far away the object should appear Define how far away the windows should appear Desktop &name alignment: Desktop Cube Dialogs: Display desktop name Display image on caps Display window &icons Display window &titles Dra&g: Draw with the mouse by holding Shift+Meta keys and moving the mouse. Dropdown menus: Enable &advanced mode Far Faster Fill &gaps Filter:
%1 Flexible Grid General Translucency Settings Ignore &minimized windows Inactive windows: Inside Graph Layout mode: Left Less Maximum &width: Menus: More Moving windows: Natural Natural Layout Settings Near Nicer Nowhere Opacity Opaque Pager Plane Popup menus: Reflection Reflections Regular Grid Right Rotation duration: Set menu translucency independently Show Desktop Grid Show caps Size Sphere Tab 1 Tab 2 Text Text alpha: Text color: Text font: Text position: Toggle Invert Effect Toggle Invert Effect on Window Toggle Present Windows (All desktops) Toggle Present Windows (Current desktop) Toggle Thumbnail for Current Window Top Top Left Top Right Top-Left Top-Right Torn-off menus: Translucency Transparent Use this effect for walking through the desktops Wallpaper: Wo&bble when moving Wobble when &resizing Zoom Zoom &duration: Project-Id-Version: kwin_effects
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-02 03:18+0100
PO-Revision-Date: 2009-01-29 14:19+0530
Last-Translator: Ravishankar Shrivastava <raviratlami@aol.in>
Language-Team: Hindi <kde-i18n-doc@lists.kde.org>
Language: hi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 0.2
Plural-Forms: nplurals=2; plural=(n!=1);
  % मि.से. px रंग: (&C) ऊंचाईः (&H) खाका मोड (&L): खिसकाय के फैक्टर: (&M) अपारदर्सिता: (&O) त्रिज्या: (&R) जगह: (&S) कड़कपन: (&S) सामर्थ्य (&S): चौड़ाईः (&W) डगमग-पन (&W) विस्तृत मूल एक्टिवेसन अतिरिक्त विकल्प विस्तृत एनीमेट स्विच करव एनीमेसन अवधि: टैब डब्बा बंद करे मं एनीमेसन टैब डब्बा खोले मं एनीमेसन रूप प्रभाव मन ल समूह मन मं लगाव (&g) प्रभाव मन ल पैनल मन मं लगाव (&p) प्रभाव ल डेस्कटाप मं लगाव अपने अपन पिछोत अंगना पिछोत अंगना के रंगः नीचे निचला डेरी  निचला जेवनी  निचला डेरी  निचला जेवनी  कैप रंग: कैप्स बीच में सब्बो मुसुवा चिनहा साफ करव आखिरी मुसुवा चिनहा साफ करव मुसुवा चिनहा साफ करव माउस खींचे के बाद बंद करव काम्बोबाक्स पापअप: मनमाफिक परिभासित करव कि कतका दूर मं आब्जेक्ट दिखही परिभासित करव कि विंडो कतका दूर दिखही डेस्कटाप नाम जमावट: (&n) डेस्कटाप घन गोठ: डेस्कटाप नाम देखाव कैप्स मं फोटू देखाव विंडो चिनहा देखाव (&i) विंडो सीर्सक देखाव (&t) खींचव: (&g) सिफ्ट + मेटा कुंजी ल दबाए रख कर अउ मुसुवा ल खिसकाकर आप मन ड्राइंग कर सकथो  ड्रापडाउन मेन्यूः  विस्तृत मोड सक्छम करव (&a) दूर तेज खाली जगह ल भरव (&g) फिल्टर:
%1 लचीला ग्रिड सामान्य अल्पपारदर्सिता सेटिंग सबसे छोटे करे गे विंडो ल अनदेखा करव (&m) असक्रिय विंडो: ग्राफ के भीतर खाका मोड: डेरी कम सबसे अधिक चौड़ाई (&W): मेन्यू: अधिक विंडो खिसकाना: प्राकृतिक प्राकृतिक खाका सेटिंग पास थोड़ा नाइस कहीं नइ अपारदर्सिता: अपारदर्सी पेजर समतल पापअप मेन्यू: रेफ्लेक्सन्स रेफ्लेक्सन्स नियमित ग्रिड जेवनी रोटेसन अवधि: मेन्यू अल्पपारदर्सिता अलग से सेट करव डेस्कटाप ग्रिड देखाव कैप्स देखाव आकार गोला टैब 1 टैब 2 पाठ पाठ अल्फा: पाठ रंगः पाठ फोंटः पाठ स्थिति: इनवर्ट प्रभाव टागल करव विंडो मं इनवर्ट प्रभाव टागल करव अभी हाल के के विंडोज ल टागल करव (सब्बो डेस्कटाप) अभी हाल के के विंडोज ल टागल करव (अभी वाले डेस्कटाप) अभी हाल के विंडो बर छोटेफोटू टागल करव ऊपर ऊपरी डेरी  ऊपरी जेवनी  ऊपरी डेरी ऊपरी जेवनी टार्न-आफ मेन्यू: अल्पपारदर्सिता अल्पपारदर्सिता डेस्कटाप के आरपार चले मं ए प्रभाव के उपयोग करव वालपेपर जब खिसकाय जाए, डगमगाव (&b) जब नवा आकार बनाय जाए, डगमगाव (&r) जूम जूम अवधिः (&d) 