��    c      4  �   L      p  
   q     |     �     �     �     �  
   �     �  	   �     �     		  "   	     2	     C	     Q	     ^	     z	  	   �	     �	     �	     �	     �	  +   �	     �	     �	     �	     �	     
     
      
     @
     E
     S
     q
  O   y
     �
     �
     �
     �
     �
     �
  !         7     X     ]     q     }     �     �     �  
   �     �     �     �     �     �  
     
        '     0     O     \     c     q     �     �     �     �  -   �     �     �     �     �  &        -     ;     G     M     [     n  :   ~     �     �     �     �     �     �     �  *        /     =  .   L  B   {     �     �     �     �  -      �  .  ,   �       0   0  5   a  '   �  .   �     �     	       $   3     X  M   j     �  *   �  (   �  5         S     t     �     �     �     �  [   �     H     ^     n     �  )   �     �     �     �  5     X   H     �  �   �  %   c     �     �  @   �     �  W     I   c  A   �     �  3         4     M     i     p  +   �     �  %   �     �  U     <   a  <   �  *   �          "  S   ;  ,   �     �  %   �  &   �  "        B  2   X     �  j   �     
           =     R  \   h  -   �  !   �       +   +  0   W  7   �  f   �     '     ;  &   X  )        �  &   �     �  l   �  (   i  (   �  v   �  �   2   !   �   "   �   ,   �   (   (!  p   Q!     1       Y   ?   A      9   L       :             <          ]          0           [   P   Q   ^              2   +   &   T   5   K       6   `   I   F                  M      C       >             3              \       8   ;           H   4      R      a       J   N   '   .   (      /   *                                   )   c      	   D           b   #   Z          W   !   $       O      B   ,       =      @                   
             "   U          E      7      S   G   _   X          -   V   %        &Closeable &Desktop &Detect Window Properties &Focus stealing prevention &Fullscreen &Machine (hostname): &Modify... &New... &Position &Single Shortcut &Size (c) 2004 KWin and KControl Authors 0123456789-+,xX: Accept &focus All Desktops Application settings for %1 Apply Initially Apply Now C&lear Cascade Centered Class: Consult the documentation for more details. Default Delete Desktop Dialog Window Do Not Affect Dock (panel) EMAIL OF TRANSLATORSYour emails Edit Edit Shortcut Edit Window-Specific Settings Edit... Enable this checkbox to alter this window property for the specified window(s). Exact Match Extreme Force Force Temporarily High Ignore requested &geometry Information About Selected Window Internal setting for remembering KWin KWin helper utility Keep &above Keep &below Low Lubos Lunak M&aximum size M&inimized M&inimum size Machine: Match w&hole window class Maximized &horizontally Maximized &vertically Maximizing Move &Down Move &Up NAME OF TRANSLATORSYour names No Placement Normal Normal Window On Main Window Override Type Random Regular Expression Remember Remember settings separately for every window Role: Settings for %1 Sh&aded Shortcut Show internal settings for remembering Skip &taskbar Skip pa&ger Smart Splash Screen Standalone Menubar Substring Match This helper utility is not supposed to be called directly. Title: Toolbar Top-Left Corner Torn-Off Menu Type: Under Mouse Unimportant Unknown - will be treated as Normal Window Unnamed entry Utility Window WId of the window for special window settings. Whether the settings should affect all windows of the application. Window &type Window &types: Window settings for %1 Window t&itle: Window-Specific Settings Configuration Module Project-Id-Version: kcmkwinrules
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2009-02-09 18:46+0530
Last-Translator: Ravishankar Shrivastava <raviratlami@aol.in>
Language-Team: Hindi <kde-i18n-doc@lists.kde.org>
Language: hi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 0.2
Plural-Forms: nplurals=2; plural=(n!=1);
 बन्द करे जा सके (&C) डेस्कटाप (&D) विंडो गुन पता लगाव फोकस हटाय बर रोकना (&F) पूरा स्क्रीन (&F) मसीन (होस्ट-नाम): (&M) सुधारव... (&M) नवा... (&N) स्थिति (&P) एकल सार्टकट (&S) आकार (&S) (c) 2004 के-विन अउ के-कन्ट्रोल लेखक 0123456789-+,xX: फोकस स्वीकारव (&f) सब्बो डेस्कटाप %1 बर अनुपरयोग सेटिंग सुरू मं लगाव अभी लगाव साफ करव (&l) एक के बाद एक बीचोंबीच वर्गः अतिरिक्त विवरन मन बर कागद मन ल देखव डिफाल्ट मेटाव डेस्कटाप गोठ विंडो प्रभावित नइ करव डाक करव (फलक) raviratlami@aol.in, संपादन सार्टकट संपादित करव विंडो-विसिस्ट सेटिंग संपादित करव संपादन... निरधारित विंडो के विंडो-गुन मन ल बदले बर एखर चेक-डब्बा ल सक्छम करव. सटीक जोड़ीदार बहुत जादा बाध्य करव अस्थायी रूप से बाध्य करव जादा निवेदित ज्यामिति के अनदेखा करव (&g) चुने गे विंडो परिचय जानकारी याद रखे बर अंदर के सेटिंग के-विन के-विन मदद यूटिलिटी ऊपर रखव (&A) नीचे रखव (&B) कम लुबास लुनाक सबसे अधिक आकार (&a) सबसे कम (&i) सबसे कम आकार (&i) मसीनः पूरा विंडो वर्ग के जोड़ी मिलाव (&h) आड़ा मं सबसे बड़ा करव (&h) खड़ा मं सबसे बड़ा करव (&v) सबसे बड़े करत हे नीचे जाव (&D) ऊपर जाव (&U) रविसंकर सिरीवास्तव, जी. करूनाकर कोई प्लेसमेंट नइ सामान्य सामान्य विंडो मुख्य विंडो मं ओवरराइड टाइप बेतरतीब रेगुलर एक्सप्रेसन. याद रखव हर एक विंडो बर सेटिंग मन ल अलग-अलग याद रखव भूमिकाः %1 बर सेटिंग छइंहा (&a) सार्टकट याद रखे बर अंदर के सेटिंग मन ल देखाव कामपट्टी छोड़व (&t) पेजर छोड़व (&g) स्मार्ट स्प्लैस स्क्रीन अलग-थलग मेनू-पट्टी सबस्ट्रिंग जोड़ीदार ये मदद यूटिलिटी सीधे ही बलाही नइ जा सके. सीर्सक: औजार पट्टी ऊपरी डेरी कोना टार्न-ओफ मेन्यू किसिमः मुसुवा के नीचे महत्वहीन अग्यात - सामान्य विंडो के रूप मं माना जाही अनाम प्रविस्टि यूटिलिटी विंडो विसिस्ट विंडो सेटिंग बर विंडो के डबल्यूआईडी  सेटिंग के अनुपरयोग के सब्बो विंडो मं प्रभाव डालही विंडो किसम (&t) विंडो किसम (&t): %1 बर विंडो सेटिंग विंडो सीर्सक (&i): विंडो-विसिस्ट सेटिंग कान्फिगरेसन माड्यूल 