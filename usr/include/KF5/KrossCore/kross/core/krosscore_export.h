
#ifndef KROSSCORE_EXPORT_H
#define KROSSCORE_EXPORT_H

#ifdef KROSSCORE_STATIC_DEFINE
#  define KROSSCORE_EXPORT
#  define KROSSCORE_NO_EXPORT
#else
#  ifndef KROSSCORE_EXPORT
#    ifdef KF5KrossCore_EXPORTS
        /* We are building this library */
#      define KROSSCORE_EXPORT __attribute__((visibility("default")))
#    else
        /* We are using this library */
#      define KROSSCORE_EXPORT __attribute__((visibility("default")))
#    endif
#  endif

#  ifndef KROSSCORE_NO_EXPORT
#    define KROSSCORE_NO_EXPORT __attribute__((visibility("hidden")))
#  endif
#endif

#ifndef KROSSCORE_DEPRECATED
#  define KROSSCORE_DEPRECATED __attribute__ ((__deprecated__))
#endif

#ifndef KROSSCORE_DEPRECATED_EXPORT
#  define KROSSCORE_DEPRECATED_EXPORT KROSSCORE_EXPORT KROSSCORE_DEPRECATED
#endif

#ifndef KROSSCORE_DEPRECATED_NO_EXPORT
#  define KROSSCORE_DEPRECATED_NO_EXPORT KROSSCORE_NO_EXPORT KROSSCORE_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef KROSSCORE_NO_DEPRECATED
#    define KROSSCORE_NO_DEPRECATED
#  endif
#endif

#endif
