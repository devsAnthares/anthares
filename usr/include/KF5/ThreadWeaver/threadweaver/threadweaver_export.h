
#ifndef THREADWEAVER_EXPORT_H
#define THREADWEAVER_EXPORT_H

#ifdef THREADWEAVER_STATIC_DEFINE
#  define THREADWEAVER_EXPORT
#  define THREADWEAVER_NO_EXPORT
#else
#  ifndef THREADWEAVER_EXPORT
#    ifdef KF5ThreadWeaver_EXPORTS
        /* We are building this library */
#      define THREADWEAVER_EXPORT __attribute__((visibility("default")))
#    else
        /* We are using this library */
#      define THREADWEAVER_EXPORT __attribute__((visibility("default")))
#    endif
#  endif

#  ifndef THREADWEAVER_NO_EXPORT
#    define THREADWEAVER_NO_EXPORT __attribute__((visibility("hidden")))
#  endif
#endif

#ifndef THREADWEAVER_DEPRECATED
#  define THREADWEAVER_DEPRECATED __attribute__ ((__deprecated__))
#endif

#ifndef THREADWEAVER_DEPRECATED_EXPORT
#  define THREADWEAVER_DEPRECATED_EXPORT THREADWEAVER_EXPORT THREADWEAVER_DEPRECATED
#endif

#ifndef THREADWEAVER_DEPRECATED_NO_EXPORT
#  define THREADWEAVER_DEPRECATED_NO_EXPORT THREADWEAVER_NO_EXPORT THREADWEAVER_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef THREADWEAVER_NO_DEPRECATED
#    define THREADWEAVER_NO_DEPRECATED
#  endif
#endif

#endif
