
#ifndef KCMUTILS_EXPORT_H
#define KCMUTILS_EXPORT_H

#ifdef KCMUTILS_STATIC_DEFINE
#  define KCMUTILS_EXPORT
#  define KCMUTILS_NO_EXPORT
#else
#  ifndef KCMUTILS_EXPORT
#    ifdef KF5KCMUtils_EXPORTS
        /* We are building this library */
#      define KCMUTILS_EXPORT __attribute__((visibility("default")))
#    else
        /* We are using this library */
#      define KCMUTILS_EXPORT __attribute__((visibility("default")))
#    endif
#  endif

#  ifndef KCMUTILS_NO_EXPORT
#    define KCMUTILS_NO_EXPORT __attribute__((visibility("hidden")))
#  endif
#endif

#ifndef KCMUTILS_DEPRECATED
#  define KCMUTILS_DEPRECATED __attribute__ ((__deprecated__))
#endif

#ifndef KCMUTILS_DEPRECATED_EXPORT
#  define KCMUTILS_DEPRECATED_EXPORT KCMUTILS_EXPORT KCMUTILS_DEPRECATED
#endif

#ifndef KCMUTILS_DEPRECATED_NO_EXPORT
#  define KCMUTILS_DEPRECATED_NO_EXPORT KCMUTILS_NO_EXPORT KCMUTILS_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef KCMUTILS_NO_DEPRECATED
#    define KCMUTILS_NO_DEPRECATED
#  endif
#endif

#endif
