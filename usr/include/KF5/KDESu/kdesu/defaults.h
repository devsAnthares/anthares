/*
 * This file is part of the KDE project, module kdesu.
 * Copyright (C) 1999,2000 Geert Jansen <jansen@kde.org>
 *
 * This is free software; you can use this library under the GNU Library
 * General Public License, version 2. See the file "COPYING.LIB" for the
 * exact licensing terms.
 */

#ifndef KDESUDEFAULTS_H
#define KDESUDEFAULTS_H

namespace KDESu
{

const int defTimeout = 120 * 60;
const int defEchoMode = 0;
const bool defKeep = false;

}

#endif
