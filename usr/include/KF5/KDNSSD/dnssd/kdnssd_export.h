
#ifndef KDNSSD_EXPORT_H
#define KDNSSD_EXPORT_H

#ifdef KDNSSD_STATIC_DEFINE
#  define KDNSSD_EXPORT
#  define KDNSSD_NO_EXPORT
#else
#  ifndef KDNSSD_EXPORT
#    ifdef KF5DNSSD_EXPORTS
        /* We are building this library */
#      define KDNSSD_EXPORT __attribute__((visibility("default")))
#    else
        /* We are using this library */
#      define KDNSSD_EXPORT __attribute__((visibility("default")))
#    endif
#  endif

#  ifndef KDNSSD_NO_EXPORT
#    define KDNSSD_NO_EXPORT __attribute__((visibility("hidden")))
#  endif
#endif

#ifndef KDNSSD_DEPRECATED
#  define KDNSSD_DEPRECATED __attribute__ ((__deprecated__))
#endif

#ifndef KDNSSD_DEPRECATED_EXPORT
#  define KDNSSD_DEPRECATED_EXPORT KDNSSD_EXPORT KDNSSD_DEPRECATED
#endif

#ifndef KDNSSD_DEPRECATED_NO_EXPORT
#  define KDNSSD_DEPRECATED_NO_EXPORT KDNSSD_NO_EXPORT KDNSSD_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef KDNSSD_NO_DEPRECATED
#    define KDNSSD_NO_DEPRECATED
#  endif
#endif

#endif
