
#ifndef KMEDIAPLAYER_EXPORT_H
#define KMEDIAPLAYER_EXPORT_H

#ifdef KMEDIAPLAYER_STATIC_DEFINE
#  define KMEDIAPLAYER_EXPORT
#  define KMEDIAPLAYER_NO_EXPORT
#else
#  ifndef KMEDIAPLAYER_EXPORT
#    ifdef KF5MediaPlayer_EXPORTS
        /* We are building this library */
#      define KMEDIAPLAYER_EXPORT __attribute__((visibility("default")))
#    else
        /* We are using this library */
#      define KMEDIAPLAYER_EXPORT __attribute__((visibility("default")))
#    endif
#  endif

#  ifndef KMEDIAPLAYER_NO_EXPORT
#    define KMEDIAPLAYER_NO_EXPORT __attribute__((visibility("hidden")))
#  endif
#endif

#ifndef KMEDIAPLAYER_DEPRECATED
#  define KMEDIAPLAYER_DEPRECATED __attribute__ ((__deprecated__))
#endif

#ifndef KMEDIAPLAYER_DEPRECATED_EXPORT
#  define KMEDIAPLAYER_DEPRECATED_EXPORT KMEDIAPLAYER_EXPORT KMEDIAPLAYER_DEPRECATED
#endif

#ifndef KMEDIAPLAYER_DEPRECATED_NO_EXPORT
#  define KMEDIAPLAYER_DEPRECATED_NO_EXPORT KMEDIAPLAYER_NO_EXPORT KMEDIAPLAYER_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef KMEDIAPLAYER_NO_DEPRECATED
#    define KMEDIAPLAYER_NO_DEPRECATED
#  endif
#endif

#endif
