
#ifndef KFILEMETADATA_EXPORT_H
#define KFILEMETADATA_EXPORT_H

#ifdef KFILEMETADATA_STATIC_DEFINE
#  define KFILEMETADATA_EXPORT
#  define KFILEMETADATA_NO_EXPORT
#else
#  ifndef KFILEMETADATA_EXPORT
#    ifdef KF5FileMetaData_EXPORTS
        /* We are building this library */
#      define KFILEMETADATA_EXPORT __attribute__((visibility("default")))
#    else
        /* We are using this library */
#      define KFILEMETADATA_EXPORT __attribute__((visibility("default")))
#    endif
#  endif

#  ifndef KFILEMETADATA_NO_EXPORT
#    define KFILEMETADATA_NO_EXPORT __attribute__((visibility("hidden")))
#  endif
#endif

#ifndef KFILEMETADATA_DEPRECATED
#  define KFILEMETADATA_DEPRECATED __attribute__ ((__deprecated__))
#endif

#ifndef KFILEMETADATA_DEPRECATED_EXPORT
#  define KFILEMETADATA_DEPRECATED_EXPORT KFILEMETADATA_EXPORT KFILEMETADATA_DEPRECATED
#endif

#ifndef KFILEMETADATA_DEPRECATED_NO_EXPORT
#  define KFILEMETADATA_DEPRECATED_NO_EXPORT KFILEMETADATA_NO_EXPORT KFILEMETADATA_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef KFILEMETADATA_NO_DEPRECATED
#    define KFILEMETADATA_NO_DEPRECATED
#  endif
#endif

#endif
