
#ifndef SOLID_EXPORT_H
#define SOLID_EXPORT_H

#ifdef SOLID_STATIC_DEFINE
#  define SOLID_EXPORT
#  define SOLID_NO_EXPORT
#else
#  ifndef SOLID_EXPORT
#    ifdef KF5Solid_EXPORTS
        /* We are building this library */
#      define SOLID_EXPORT __attribute__((visibility("default")))
#    else
        /* We are using this library */
#      define SOLID_EXPORT __attribute__((visibility("default")))
#    endif
#  endif

#  ifndef SOLID_NO_EXPORT
#    define SOLID_NO_EXPORT __attribute__((visibility("hidden")))
#  endif
#endif

#ifndef SOLID_DEPRECATED
#  define SOLID_DEPRECATED __attribute__ ((__deprecated__))
#endif

#ifndef SOLID_DEPRECATED_EXPORT
#  define SOLID_DEPRECATED_EXPORT SOLID_EXPORT SOLID_DEPRECATED
#endif

#ifndef SOLID_DEPRECATED_NO_EXPORT
#  define SOLID_DEPRECATED_NO_EXPORT SOLID_NO_EXPORT SOLID_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef SOLID_NO_DEPRECATED
#    define SOLID_NO_DEPRECATED
#  endif
#endif

#endif
