// This file was generated by ecm_setup_version(): DO NOT EDIT!

#ifndef KSERVICE_VERSION_H
#define KSERVICE_VERSION_H

#define KSERVICE_VERSION_STRING "5.43.0"
#define KSERVICE_VERSION_MAJOR 5
#define KSERVICE_VERSION_MINOR 43
#define KSERVICE_VERSION_PATCH 0
#define KSERVICE_VERSION ((5<<16)|(43<<8)|(0))

#endif
