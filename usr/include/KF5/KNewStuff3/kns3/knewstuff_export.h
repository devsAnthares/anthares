
#ifndef KNEWSTUFF_EXPORT_H
#define KNEWSTUFF_EXPORT_H

#ifdef KNEWSTUFF_STATIC_DEFINE
#  define KNEWSTUFF_EXPORT
#  define KNEWSTUFF_NO_EXPORT
#else
#  ifndef KNEWSTUFF_EXPORT
#    ifdef KF5NewStuff_EXPORTS
        /* We are building this library */
#      define KNEWSTUFF_EXPORT __attribute__((visibility("default")))
#    else
        /* We are using this library */
#      define KNEWSTUFF_EXPORT __attribute__((visibility("default")))
#    endif
#  endif

#  ifndef KNEWSTUFF_NO_EXPORT
#    define KNEWSTUFF_NO_EXPORT __attribute__((visibility("hidden")))
#  endif
#endif

#ifndef KNEWSTUFF_DEPRECATED
#  define KNEWSTUFF_DEPRECATED __attribute__ ((__deprecated__))
#endif

#ifndef KNEWSTUFF_DEPRECATED_EXPORT
#  define KNEWSTUFF_DEPRECATED_EXPORT KNEWSTUFF_EXPORT KNEWSTUFF_DEPRECATED
#endif

#ifndef KNEWSTUFF_DEPRECATED_NO_EXPORT
#  define KNEWSTUFF_DEPRECATED_NO_EXPORT KNEWSTUFF_NO_EXPORT KNEWSTUFF_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef KNEWSTUFF_NO_DEPRECATED
#    define KNEWSTUFF_NO_DEPRECATED
#  endif
#endif

#endif
