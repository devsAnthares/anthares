
#ifndef KROSSUI_EXPORT_H
#define KROSSUI_EXPORT_H

#ifdef KROSSUI_STATIC_DEFINE
#  define KROSSUI_EXPORT
#  define KROSSUI_NO_EXPORT
#else
#  ifndef KROSSUI_EXPORT
#    ifdef KF5KrossUi_EXPORTS
        /* We are building this library */
#      define KROSSUI_EXPORT __attribute__((visibility("default")))
#    else
        /* We are using this library */
#      define KROSSUI_EXPORT __attribute__((visibility("default")))
#    endif
#  endif

#  ifndef KROSSUI_NO_EXPORT
#    define KROSSUI_NO_EXPORT __attribute__((visibility("hidden")))
#  endif
#endif

#ifndef KROSSUI_DEPRECATED
#  define KROSSUI_DEPRECATED __attribute__ ((__deprecated__))
#endif

#ifndef KROSSUI_DEPRECATED_EXPORT
#  define KROSSUI_DEPRECATED_EXPORT KROSSUI_EXPORT KROSSUI_DEPRECATED
#endif

#ifndef KROSSUI_DEPRECATED_NO_EXPORT
#  define KROSSUI_DEPRECATED_NO_EXPORT KROSSUI_NO_EXPORT KROSSUI_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef KROSSUI_NO_DEPRECATED
#    define KROSSUI_NO_DEPRECATED
#  endif
#endif

#endif
