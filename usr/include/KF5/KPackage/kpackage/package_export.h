
#ifndef PACKAGE_EXPORT_H
#define PACKAGE_EXPORT_H

#ifdef PACKAGE_STATIC_DEFINE
#  define PACKAGE_EXPORT
#  define PACKAGE_NO_EXPORT
#else
#  ifndef PACKAGE_EXPORT
#    ifdef KF5Package_EXPORTS
        /* We are building this library */
#      define PACKAGE_EXPORT __attribute__((visibility("default")))
#    else
        /* We are using this library */
#      define PACKAGE_EXPORT __attribute__((visibility("default")))
#    endif
#  endif

#  ifndef PACKAGE_NO_EXPORT
#    define PACKAGE_NO_EXPORT __attribute__((visibility("hidden")))
#  endif
#endif

#ifndef PACKAGE_DEPRECATED
#  define PACKAGE_DEPRECATED __attribute__ ((__deprecated__))
#endif

#ifndef PACKAGE_DEPRECATED_EXPORT
#  define PACKAGE_DEPRECATED_EXPORT PACKAGE_EXPORT PACKAGE_DEPRECATED
#endif

#ifndef PACKAGE_DEPRECATED_NO_EXPORT
#  define PACKAGE_DEPRECATED_NO_EXPORT PACKAGE_NO_EXPORT PACKAGE_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef PACKAGE_NO_DEPRECATED
#    define PACKAGE_NO_DEPRECATED
#  endif
#endif

#endif
