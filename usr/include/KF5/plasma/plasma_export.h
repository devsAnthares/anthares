
#ifndef PLASMA_EXPORT_H
#define PLASMA_EXPORT_H

#ifdef PLASMA_STATIC_DEFINE
#  define PLASMA_EXPORT
#  define PLASMA_NO_EXPORT
#else
#  ifndef PLASMA_EXPORT
#    ifdef KF5Plasma_EXPORTS
        /* We are building this library */
#      define PLASMA_EXPORT __attribute__((visibility("default")))
#    else
        /* We are using this library */
#      define PLASMA_EXPORT __attribute__((visibility("default")))
#    endif
#  endif

#  ifndef PLASMA_NO_EXPORT
#    define PLASMA_NO_EXPORT __attribute__((visibility("hidden")))
#  endif
#endif

#ifndef PLASMA_DEPRECATED
#  define PLASMA_DEPRECATED __attribute__ ((__deprecated__))
#endif

#ifndef PLASMA_DEPRECATED_EXPORT
#  define PLASMA_DEPRECATED_EXPORT PLASMA_EXPORT PLASMA_DEPRECATED
#endif

#ifndef PLASMA_DEPRECATED_NO_EXPORT
#  define PLASMA_DEPRECATED_NO_EXPORT PLASMA_NO_EXPORT PLASMA_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef PLASMA_NO_DEPRECATED
#    define PLASMA_NO_DEPRECATED
#  endif
#endif

#endif
