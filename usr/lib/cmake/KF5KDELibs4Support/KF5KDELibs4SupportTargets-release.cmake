#----------------------------------------------------------------
# Generated CMake target import file for configuration "Release".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "KF5::kf5-config" for configuration "Release"
set_property(TARGET KF5::kf5-config APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(KF5::kf5-config PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/kf5-config"
  )

list(APPEND _IMPORT_CHECK_TARGETS KF5::kf5-config )
list(APPEND _IMPORT_CHECK_FILES_FOR_KF5::kf5-config "${_IMPORT_PREFIX}/bin/kf5-config" )

# Import target "KF5::KDELibs4Support" for configuration "Release"
set_property(TARGET KF5::KDELibs4Support APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(KF5::KDELibs4Support PROPERTIES
  IMPORTED_LINK_DEPENDENT_LIBRARIES_RELEASE "KF5::GlobalAccel;Qt5::Svg;Qt5::Test;Qt5::X11Extras"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libKF5KDELibs4Support.so.5.43.0"
  IMPORTED_SONAME_RELEASE "libKF5KDELibs4Support.so.5"
  )

list(APPEND _IMPORT_CHECK_TARGETS KF5::KDELibs4Support )
list(APPEND _IMPORT_CHECK_FILES_FOR_KF5::KDELibs4Support "${_IMPORT_PREFIX}/lib/libKF5KDELibs4Support.so.5.43.0" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
