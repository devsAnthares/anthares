QT.KGuiAddons.VERSION = 5.43.0
QT.KGuiAddons.MAJOR_VERSION = 5
QT.KGuiAddons.MINOR_VERSION = 43
QT.KGuiAddons.PATCH_VERSION = 0
QT.KGuiAddons.name = KF5GuiAddons
QT.KGuiAddons.defines = 
QT.KGuiAddons.includes = /usr/include/KF5/KGuiAddons
QT.KGuiAddons.private_includes =
QT.KGuiAddons.libs = /usr/lib
QT.KGuiAddons.depends = gui
