QT.KWallet.VERSION = 5.43.0
QT.KWallet.MAJOR_VERSION = 5
QT.KWallet.MINOR_VERSION = 43
QT.KWallet.PATCH_VERSION = 0
QT.KWallet.name = KF5Wallet
QT.KWallet.defines = 
QT.KWallet.includes = /usr/include/KF5/KWallet
QT.KWallet.private_includes =
QT.KWallet.libs = /usr/lib
QT.KWallet.depends = widgets
