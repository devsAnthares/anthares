QT.KIOCore.VERSION = 5.43.0
QT.KIOCore.MAJOR_VERSION = 5
QT.KIOCore.MINOR_VERSION = 43
QT.KIOCore.PATCH_VERSION = 0
QT.KIOCore.name = KF5KIOCore
QT.KIOCore.defines = 
QT.KIOCore.includes = /usr/include/KF5/KIOCore
QT.KIOCore.private_includes =
QT.KIOCore.libs = /usr/lib
QT.KIOCore.depends = KCoreAddons KService
